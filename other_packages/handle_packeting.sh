#!/bin/sh
#USAGE: give folder names which you want packet and send to builder

#Change these:
maintainer="Aapo Rantalainen <aapo.rantalainen@gmail.com>"
BUILDER_USERNAME=aapo

#use same version than jammo-bin
temp=( `head ../debian/changelog -n1` )
#second word is version number but it has parenthesis ()
ver_tmp=${temp[1]}

#remove first and last character
version=${ver_tmp:1:${#ver_tmp}-2}

date="`date -R`"

for folder in "$@"
do
  cd $folder
  cd debian
  mv changelog changelog_original

  temp=( `head changelog_original -n1` )

  #package name is first word in changelog
  package=${temp[0]}

  echo $package  \("$version"\) unstable\; urgency=low >changelog
  echo '' >>changelog
  echo "  * Version bump" >>changelog
  echo '' >>changelog
  echo ' -- '"$maintainer"'  '"$date"  >>changelog
  echo '' >>changelog
  cat changelog_original >> changelog
  rm changelog_original
  cd ..

  dpkg-buildpackage -rfakeroot -sa -S -i -us -uc
  cd ..
done


#send all just created packages and remove them
time rcp *.tar.gz *.changes *.dsc $BUILDER_USERNAME@drop.maemo.org:/var/www/extras-devel/incoming-builder/fremantle/

rm *.tar.gz *.changes *.dsc
