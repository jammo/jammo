#!/bin/sh
#Use this when there are new jammo-*-data -package or new jammo-bin -package.

#If there are need to only add language specific files, use package MINOR versions for that.
#Language specific files means updated "-dif" -files.


#generate language files
msgfmt ../po/fi.po -o jammo7-12-fi/jammo.mo
#msgfmt ../po/de.po -o jammo7-12-de/jammo.mo

./handle_packeting.sh jammo3-6-de jammo3-6-en jammo3-6-fi jammo7-12-de  jammo7-12-en  jammo7-12-fi

#remove generated files:
rm jammo7-12-fi/jammo.mo
#rm jammo7-12-de/jammo.mo



