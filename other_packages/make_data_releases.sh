#!/bin/sh
#Use this when NEW DATA RELEASE is needed
#it means new or changed
#  -images
#  -musical material
#  -JSON-files


##jammo3-6-data
#This is pointing to jammo-data -git
cp -r ../../../jammo-data/data/jammo3-6_data/* jammo3-6-data/
cp -r ../data/jammo3_6/ jammo3-6-data/jsons
cp ../org.umsic.jammo3_6.desktop jammo3-6-data/
cp ../org.umsic.jammo3_6.service jammo3-6-data/

./handle_packeting.sh jammo3-6-data

rm jammo3-6-data/org.umsic.jammo3_6.desktop
rm jammo3-6-data/org.umsic.jammo3_6.service
rm jammo3-6-data/*png
rm jammo3-6-data/*.wav
rm jammo3-6-data/*.ogg
rm -rf jammo3-6-data/jsons
rm -rf jammo3-6-data/buttons
rm -rf jammo3-6-data/password
rm -rf jammo3-6-data/songs
rm -rf jammo3-6-data/themes

###################

##jammo7-12-data
#This is pointing to jammo-data -git
cp -r ../../../jammo-data/data/jammo7-12_data/* jammo7-12-data/
cp -r ../data/jammo7_12/ jammo7-12-data/jsons
cp ../org.umsic.jammo7_12.desktop jammo7-12-data/
cp ../org.umsic.jammo7_12.service jammo7-12-data/

./handle_packeting.sh jammo7-12-data

rm jammo7-12-data/org.umsic.jammo7_12.service
rm jammo7-12-data/org.umsic.jammo7_12.desktop
rm -rf jammo7-12-data/jsons
rm -rf jammo7-12-data/backingtracks
rm -rf jammo7-12-data/drumkit_acoustic
rm -rf jammo7-12-data/midieditor
rm -rf jammo7-12-data/sequencer
rm -rf jammo7-12-data/wheel_game
rm -rf jammo7-12-data/communitymenu
rm -rf jammo7-12-data/gamesmenu
rm -rf jammo7-12-data/startmenu
rm -rf jammo7-12-data/drumkit
rm -rf jammo7-12-data/jammer
rm -rf jammo7-12-data/keyboard
rm -rf jammo7-12-data/virtual-instruments
