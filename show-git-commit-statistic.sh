#! /bin/bash

# show-git-commit-statistics.sh
#
# (c) 2011 Henrik Hedberg <henrik.hedberg@oulu.fi_
#
# Shows the amount of inserted and removed lines for each commit.

IFS=$'\n'
for f in `git log "--format=format:%H %ct %cn"`; do
	commit=`echo $f|cut -d " " -f 1`
	time=`echo $f|cut -d " " -f 2`
	author=`echo $f |cut -d " " -f 3-`
	log=`git log --shortstat $commit^..$commit|grep "files changed"`
	plus=`echo $log|cut -d " " -f 5`
	minus=`echo $log|cut -d " " -f 7`
	
	echo "$time,$author,$plus,$minus"
done
