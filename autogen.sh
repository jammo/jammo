#!/bin/sh

touch NEWS README AUTHORS ChangeLog

gtkdocize --copy || exit 1

aclocal
automake -ac
autoconf

echo
echo "Run './configure; make; sudo make install' to compile and install"
echo "Run 'jammo' to start JamMo"

exit 0
