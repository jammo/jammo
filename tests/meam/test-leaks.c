#include <gst/gst.h>

#include <meam/jammo-meam.h>
#include <meam/jammo-sequencer.h>
#include <meam/jammo-editing-track.h>
#include <meam/jammo-recording-track.h>
#include <meam/jammo-sample.h>

#define AUDIO_FILENAME "/opt/jammo/songs/easy/boat/vocal_fi.ogg"

#if 1
/*
static void on_sample_stopped(JammoSample* sample, gpointer user_data) {
	GMainLoop* main_loop;
	
	main_loop = (GMainLoop*)user_data;
	g_main_loop_quit(main_loop);
}
*/
int main(int argc, char** argv) {
	GMainLoop* main_loop;
	JammoSample* sample;
	
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	main_loop = g_main_loop_new (NULL, FALSE);
	
/*
	sequencer = jammo_sequencer_new();
	jammo_sequencer_set_pitch(sequencer, "F#");
	jammo_sequencer_set_tempo(sequencer, 90);
	g_signal_connect(sequencer, "stopped", G_CALLBACK(on_sequencer_stopped), main_loop);

	track1 = jammo_editing_track_new();
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(track1));
	track2 = jammo_editing_track_new();
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(track2));
*/	
	sample = jammo_sample_new_from_file(AUDIO_FILENAME);
	g_signal_connect_swapped(sample, "notify::duration", G_CALLBACK(g_main_loop_quit), main_loop);
	//jammo_sample_play(sample);
	//g_signal_connect(sample, "stopped", G_CALLBACK(on_sample_stopped), main_loop);

	g_main_loop_run(main_loop);

	g_object_unref(sample);
	g_main_loop_unref(main_loop);
	
	jammo_meam_cleanup();

	return 0;
}
#else

static gboolean preroll_bus_callback(GstBus* bus, GstMessage* message, gpointer data) {
	gboolean cleanup = FALSE;
	GstElement* pipeline;
	
	pipeline = GST_ELEMENT(data);
	
	if (GST_MESSAGE_TYPE(message) == GST_MESSAGE_ASYNC_DONE ||
	    GST_MESSAGE_TYPE(message) == GST_MESSAGE_EOS ||
	    GST_MESSAGE_TYPE(message) == GST_MESSAGE_ERROR) {
		gst_element_set_state(pipeline, GST_STATE_NULL);
		gst_object_unref(pipeline);

		cleanup = TRUE;
	}
	
	return !cleanup;
}

static void on_new_decoded_pad(GstElement* element, GstPad* pad, gboolean last, gpointer data) {
	GstElement* sink;
	GstPad* sinkpad;
	GstCaps* caps;

	sink = GST_ELEMENT(data);
	
	sinkpad = gst_element_get_pad(sink, "sink");
	if (!GST_PAD_IS_LINKED(sinkpad)) {
		caps = gst_pad_get_caps(pad);
		if (g_strrstr(gst_structure_get_name(gst_caps_get_structure(caps, 0)), "audio")) {
			gst_pad_link(pad, sinkpad);
		}
		gst_caps_unref(caps);
	}
	g_object_unref(sinkpad);
}

int main(int argc, char** argv) {	
	GstElement* pipeline;
	GstBus* bus;
	GstElement* filesrc;
	GstElement* decodebin;
	GstElement* sink;
	
	gst_init(&argc, &argv);

	pipeline = gst_element_factory_make("pipeline", NULL);
	bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
	gst_bus_add_watch(bus, preroll_bus_callback, pipeline);
	gst_object_unref(bus);

	filesrc = gst_element_factory_make("filesrc", NULL);
	g_object_set(filesrc, "location", AUDIO_FILENAME, NULL);
	decodebin = gst_element_factory_make("decodebin", NULL);
	sink = gst_element_factory_make("fakesink", NULL);
	g_signal_connect(decodebin, "new-decoded-pad", G_CALLBACK(on_new_decoded_pad), sink);

	gst_bin_add_many(GST_BIN(pipeline), filesrc, decodebin, sink, NULL);

	g_object_unref(pipeline);
	
	gst_deinit();

	return 0;
}

#endif
