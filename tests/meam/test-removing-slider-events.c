/*
 * test-removing-slider-events
 *
 * This file is part of JamMo.
 * Testing removing jammo-slider-events from jammo-slider-track
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther
 */


#include <meam/jammo-meam.h>
#include <meam/jammo-slider-track.h>
#include <meam/jammo-slider-event.h>
#include <glib-object.h>
#include <ctype.h>
#include <string.h>


int main(int argc, char** argv) {
	JammoSequencer* sequencer;
	JammoSliderTrack* slider_track;
	
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	


	sequencer = jammo_sequencer_new();
	slider_track = jammo_slider_track_new(JAMMO_SLIDER_TYPE_KARPLUS);
	jammo_slider_track_set_recording(slider_track,FALSE);
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(slider_track));

	//Testing removing.
	printf("Removing from empty\n");
	jammo_slider_track_remove_event(slider_track, 152.0, JAMMO_SLIDER_EVENT_ON, 600000000);
	jammo_slider_track_dump_events(slider_track);

	// adding a whole lot of notes
	jammo_slider_track_add_event(slider_track, 152.0, JAMMO_SLIDER_EVENT_ON, 600000000);
	jammo_slider_track_add_event(slider_track, 152.1, JAMMO_SLIDER_EVENT_MOTION, 600000000);
	jammo_slider_track_add_event(slider_track, 158.0, JAMMO_SLIDER_EVENT_MOTION, 610000000);
	jammo_slider_track_add_event(slider_track, 168.0, JAMMO_SLIDER_EVENT_OFF, 620000000);
	jammo_slider_track_add_event(slider_track, 173.0, JAMMO_SLIDER_EVENT_ON, 630000000);
	jammo_slider_track_add_event(slider_track, 178.0, JAMMO_SLIDER_EVENT_MOTION, 640000000);
	jammo_slider_track_add_event(slider_track, 179.0, JAMMO_SLIDER_EVENT_MOTION, 650000000);
	jammo_slider_track_add_event(slider_track, 179.1, JAMMO_SLIDER_EVENT_MOTION, 650000001);
	jammo_slider_track_add_event(slider_track, 172.1, JAMMO_SLIDER_EVENT_MOTION, 660000000);
	jammo_slider_track_add_event(slider_track, 172.0, JAMMO_SLIDER_EVENT_OFF, 660000000);
	jammo_slider_track_dump_events(slider_track);

	// removing something not in list
	printf("removing something not in list\n");
	jammo_slider_track_remove_event(slider_track, 100.0, JAMMO_SLIDER_EVENT_ON, 630000000);
	jammo_slider_track_dump_events(slider_track);

	// removing of and on
	printf("removing first off and second on\n");
	jammo_slider_track_remove_event(slider_track, 168.0, JAMMO_SLIDER_EVENT_OFF, 620000000);
	jammo_slider_track_remove_event(slider_track, 173.0, JAMMO_SLIDER_EVENT_ON, 630000000);
	jammo_slider_track_dump_events(slider_track);

	return 0;
}

