/*
 * test-instrumet
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen 
 */


#include <meam/jammo-meam.h>
//#include <meam/jammo-playing-track.h>
#include <meam/jammo-instrument-track.h>
#include <meam/jammo-midi.h>
#include <chum/jammo-chum.h>
#include <tangle.h>

#include <tangle.h>
#include <glib-object.h>
#include <ctype.h>
#include <string.h>
#include <clutter/clutter.h>


static gboolean on_stop_button_clicked(TangleButton* button, gpointer user_data) {
	jammo_sequencer_stop(JAMMO_SEQUENCER(user_data));
	return FALSE;
}

static gboolean on_play_button_clicked(TangleButton* button, gpointer user_data) {
	jammo_sequencer_play(JAMMO_SEQUENCER(user_data));
	return FALSE;
}

int main(int argc, char** argv) {
	JammoSequencer* sequencer;
	JammoInstrumentTrack* instrument_track;
	ClutterActor* stage;
	ClutterActor* play_button;
	ClutterActor* stop_button;
	
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	jammo_chum_init(&argc, &argv);


	stage = clutter_stage_get_default();
	//clutter_stage_set_fullscreen(CLUTTER_STAGE(stage), TRUE);
	clutter_actor_set_size(stage, 800.0, 480.0);


	sequencer = jammo_sequencer_new();
	instrument_track =  jammo_instrument_track_new(JAMMO_INSTRUMENT_TYPE_UD);
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(instrument_track));
	jammo_instrument_track_load_to_track_from_file(instrument_track,"notes.txt");

	play_button = tangle_button_new();

	clutter_actor_set_position(play_button, 100.0, 400.0);
	tangle_widget_add_after(TANGLE_WIDGET(play_button), tangle_texture_new("test_image_100x74.jpg"), NULL, NULL, NULL);
	g_signal_connect(play_button, "clicked", G_CALLBACK(on_play_button_clicked), sequencer);
	clutter_container_add(CLUTTER_CONTAINER(stage), play_button, NULL);

	stop_button = tangle_button_new();

	clutter_actor_set_position(stop_button, 300.0, 400.0);
	tangle_widget_add_after(TANGLE_WIDGET(stop_button), tangle_texture_new("test_image_100x74.jpg"), NULL, NULL, NULL);
	g_signal_connect(stop_button, "clicked", G_CALLBACK(on_stop_button_clicked), sequencer);
	clutter_container_add(CLUTTER_CONTAINER(stage), stop_button, NULL);



	clutter_actor_show_all(stage);
	clutter_main();

	return 0;
}

