
#include <meam/jammo-meam.h>
#include <meam/jammo-sequencer.h>
#include <meam/jammo-backing-track.h>
#include <meam/jammo-recording-track.h>
#include <meam/jammo-sample.h>

/* 
   install jammo-data. 
     backing_track is: /opt/jammo/songs/easy/boat/vocal_fi.ogg

    recording is saved into /tmp/recording.wav
     or (N900) /home/user/.jammo/recording.wav

To run: ./test-recording
*/

//N900 has very limited /tmp space.
#ifdef N900
#define RECORDING_FILENAME "/home/user/MyDocs/recording.wav"
#else
#define RECORDING_FILENAME "/tmp/recording.wav"
#endif

static void on_sample_stopped(JammoSample* sample, gpointer user_data) {
	GMainLoop* main_loop;
	
	main_loop = (GMainLoop*)user_data;
	g_main_loop_quit(main_loop);
}

static void on_sequencer_stopped(JammoSequencer* sequencer, gpointer user_data) {
	JammoSample* sample;
	
	g_print("Playing...\n");
	sample = jammo_sample_new_from_file(RECORDING_FILENAME);
	g_signal_connect(sample, "stopped", G_CALLBACK(on_sample_stopped), user_data);
	jammo_sample_play(sample);
}

/*
static void on_pitch_detected(JammoTrack* track, gfloat frequency, gpointer user_data) {
	g_print("Frequency: %f\n", frequency);
}
*/

static void on_duration_notify(GObject* object, GParamSpec* param_spec, gpointer user_data) {
	guint64 duration;
	
	g_object_get(object, "duration", &duration, NULL);
	printf("Duration: %lu ms\n", (gulong)(duration / 1000000));
}

int main(int argc, char** argv) {
	GMainLoop* main_loop;
	JammoSequencer* sequencer;
	JammoBackingTrack* backing_track;
	JammoRecordingTrack* recording_track;
	
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	main_loop = g_main_loop_new (NULL, FALSE);

	sequencer = jammo_sequencer_new();
	g_signal_connect(sequencer, "stopped", G_CALLBACK(on_sequencer_stopped), main_loop);

	backing_track = jammo_backing_track_new("/opt/jammo/songs/easy/boat/vocal_fi.ogg");
	g_signal_connect(backing_track, "notify::duration", G_CALLBACK(on_duration_notify), NULL);
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(backing_track));
	
	/* recording_track = jammo_recording_track_new_with_pitch_detect(RECORDING_FILENAME); */
	recording_track = jammo_recording_track_new(RECORDING_FILENAME);
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(recording_track));
	/* g_signal_connect(recording_track, "pitch-detected", G_CALLBACK(on_pitch_detected), NULL); */

	g_print("Recording...\n");

	jammo_sequencer_play(JAMMO_SEQUENCER(sequencer));

	g_main_loop_run(main_loop);

	return 0;
}
