#include <tangle.h>
#include <meam/jammo-meam.h>
#include <meam/jammo-sequencer.h>
#include <meam/jammo-backing-track.h>

int main(int argc, char** argv) {
	GMainLoop* main_loop;
	const gchar* filename;
	GError* error = NULL;
	ClutterScript* script;
	JammoSequencer* sequencer;
	
	tangle_init(&argc, &argv);
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");

	g_print("%ld %ld\n", (long)JAMMO_TYPE_SEQUENCER, (long)JAMMO_TYPE_BACKING_TRACK);

	main_loop = g_main_loop_new (NULL, FALSE);

	if (argc > 1) {
		filename = argv[1];
	} else {
		filename = "test-script.json";
	}

	script = clutter_script_new();
	if (!clutter_script_load_from_file(script, filename, &error)) {
		g_critical("Error when loading '%s': %s", filename, error->message);
	} else if (!(sequencer = JAMMO_SEQUENCER(clutter_script_get_object(script, "the-sequencer")))) {
		g_critical("Could not find an object named 'the-sequencer' from '%s'.", filename);
	} else {
		jammo_sequencer_play(sequencer);
	}

	g_main_loop_run(main_loop);
	
	return 0;
}
