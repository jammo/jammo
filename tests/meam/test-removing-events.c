/*
 * test-removing-events
 *
 * This file is part of JamMo.
 * Testing removing jammo-midi-events from jammo-instrument-track
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen 
 */


#include <meam/jammo-meam.h>
#include <meam/jammo-instrument-track.h>
#include <meam/jammo-midi.h>
#include <glib-object.h>
#include <ctype.h>
#include <string.h>


int main(int argc, char** argv) {
	JammoSequencer* sequencer;
	JammoInstrumentTrack* instrument_track;
	
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	


	sequencer = jammo_sequencer_new();
	instrument_track = jammo_instrument_track_new(2); //0= flute, 1=drumkit ,2=ud
	jammo_instrument_track_set_realtime(instrument_track,TRUE);
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(instrument_track));

	//Not recording mode!
	jammo_instrument_track_set_realtime(instrument_track,FALSE);

	//Testing removing.
	//Case A) 
	printf("Removing from empty\n");
	jammo_instrument_track_remove_event(instrument_track, 52, 600000000LLU);
	jammo_instrument_track_dump_notes(instrument_track);

	//Case B) 
	printf("Removing something not in list\n");
	jammo_instrument_track_add_event(instrument_track, 48,  JAMMOMIDI_NOTE_ON, 1500000000LLU);
	jammo_instrument_track_add_event(instrument_track, 48, JAMMOMIDI_NOTE_OFF, 3100000000LLU);
	jammo_instrument_track_remove_event(instrument_track, 52, 600000000LLU);
	printf("There should be start+stop still in list\n");
	jammo_instrument_track_dump_notes(instrument_track);
	
	//Case C)
	printf("Removing trivial\n");
	jammo_instrument_track_remove_event(instrument_track, 48, 1500000000LLU);
	printf("should be empty list\n");
	jammo_instrument_track_dump_notes(instrument_track);
	
	//Case D)
	printf("\nRemoving start which do not have stop\n");
	jammo_instrument_track_add_event(instrument_track, 48,  JAMMOMIDI_NOTE_ON, 1500000000LLU);
	jammo_instrument_track_remove_event(instrument_track, 48, 1500000000LLU);
	printf("should be empty list\n");
	jammo_instrument_track_dump_notes(instrument_track);
	
	//Case E)
	printf("\nRemoving start which do not have stop, but there are another start-stop\n");
	jammo_instrument_track_add_event(instrument_track,48,  JAMMOMIDI_NOTE_ON, 1500000000LLU);
	jammo_instrument_track_add_event(instrument_track,48,  JAMMOMIDI_NOTE_ON, 3000000000LLU);
	jammo_instrument_track_add_event(instrument_track,48, JAMMOMIDI_NOTE_OFF, 4000000000LLU);
	jammo_instrument_track_remove_event(instrument_track,48, 1500000000LLU);
	printf("should be start30 and stop40\n");
	jammo_instrument_track_dump_notes(instrument_track);
	
	//Case F)
	printf("\nRemoving start-stop and before there are start which do not have stop\n");
	jammo_instrument_track_add_event(instrument_track,48,  JAMMOMIDI_NOTE_ON, 1500000000LLU);
	jammo_instrument_track_remove_event(instrument_track,48, 3000000000LLU);
	printf("should be only start15\n");
	jammo_instrument_track_dump_notes(instrument_track);
	
	return 0;
}

