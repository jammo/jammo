
#include <meam/jammo-meam.h>
#include <meam/jammo-sequencer.h>
#include <meam/jammo-metronome-track.h>

int main(int argc, char** argv) {
	GMainLoop* main_loop;
	JammoSequencer* sequencer;
	JammoMetronomeTrack* metronome_track;
	
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	main_loop = g_main_loop_new (NULL, FALSE);

	sequencer = jammo_sequencer_new();


	metronome_track = jammo_metronome_track_new();
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(metronome_track));
	jammo_metronome_track_set_tempo(metronome_track,180.0);
	jammo_metronome_track_set_time_signature_beats(metronome_track,6);
	jammo_metronome_track_set_time_signature_note_value(metronome_track,8);
	jammo_metronome_track_set_accent(metronome_track,TRUE);

	g_print("Playing...\n");

	jammo_sequencer_play(JAMMO_SEQUENCER(sequencer));

	g_main_loop_run(main_loop);

	return 0;
}
