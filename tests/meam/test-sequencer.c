
#include <meam/jammo-meam.h>
#include <meam/jammo-sequencer.h>
#include <meam/jammo-editing-track.h>
#include <meam/jammo-recording-track.h>
#include <meam/jammo-sample.h>

/* 
   To run:
   - without recording: ./test-sequencer
   - with recording: ./test-sequencer something
   => recording is saved into /tmp/testi.wav
      (in N900 do this beforehand: ln -s /home/user/MyDocs/testi.wav /tmp/testi.wav)

   This test _requires_ three audio files:
   - /tmp/sample1.wav (any length)
   - /tmp/sample2.wav (shorter than 2 seconds)
   - /tmp/sample3.wav (shorter than 3 seconds)
*/

#define RECORDING_FILENAME "/tmp/recording.wav"

static void on_sample_stopped(JammoSample* sample, gpointer user_data) {
	GMainLoop* main_loop;
	
	main_loop = (GMainLoop*)user_data;
	g_main_loop_quit(main_loop);
}

static void on_sequencer_stopped(JammoSequencer* sequencer, gpointer user_data) {
	JammoSample* sample;
	
	g_print("Playing...\n");
	sample = jammo_sample_new_from_file(RECORDING_FILENAME);
	g_signal_connect(sample, "stopped", G_CALLBACK(on_sample_stopped), user_data);
	jammo_sample_play(sample);
}

static void on_pitch_detected(JammoTrack* track, gfloat frequency, gpointer user_data) {
	g_print("Frequency: %f\n", frequency);
}

static gboolean do_panning(gpointer user_data) {
	static gfloat panning = 0;
	JammoPlayingTrack* playing_track;

	playing_track = JAMMO_PLAYING_TRACK(user_data);

	panning += 0.1;
	if (panning > 3.0) {
		panning = -1.0;
	}
	jammo_playing_track_set_panning(playing_track, panning < 1.0 ? panning : 2.0 - panning);

	return TRUE;
}

int main(int argc, char** argv) {
	GMainLoop* main_loop;
	JammoSequencer* sequencer;
	JammoEditingTrack* track1;
	JammoEditingTrack* track2;
	JammoRecordingTrack* track3;
	JammoSample* sample1;
	JammoSample* sample2;
	JammoSample* sample3;
	
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	main_loop = g_main_loop_new (NULL, FALSE);

	sequencer = jammo_sequencer_new();
	jammo_sequencer_set_pitch(sequencer, "F#");
	jammo_sequencer_set_tempo(sequencer, 90);
	g_signal_connect(sequencer, "stopped", G_CALLBACK(on_sequencer_stopped), main_loop);

	track1 = jammo_editing_track_new();
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(track1));
	track2 = jammo_editing_track_new();
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(track2));
	if (argc > 1) {
		track3 = jammo_recording_track_new_with_pitch_detect(RECORDING_FILENAME);
		jammo_sequencer_add_track(sequencer, JAMMO_TRACK(track3));
		g_signal_connect(track3, "pitch-detected", G_CALLBACK(on_pitch_detected), NULL);
		g_print("Recording...\n");
	}
	
	sample1 = jammo_sample_new_from_file("/tmp/sample1.wav");
	jammo_editing_track_add_sample(track1, sample1, 7000000000LL);
	sample2 = jammo_sample_new_from_file("/tmp/sample2.wav");
	jammo_editing_track_add_sample(track2, sample2, 5000000000LL);
	sample3 = jammo_sample_new_from_file("/tmp/sample3.wav");
	jammo_editing_track_add_sample(track2, sample3, 0);

	jammo_playing_track_set_echo_delay(JAMMO_PLAYING_TRACK(track2), 175000000);
	jammo_playing_track_set_echo_intensity(JAMMO_PLAYING_TRACK(track2), 0.4);
	jammo_playing_track_set_echo_feedback(JAMMO_PLAYING_TRACK(track2), 0.5);

	jammo_playing_track_set_panning_enabled(JAMMO_PLAYING_TRACK(track1), TRUE);
	jammo_playing_track_set_panning_enabled(JAMMO_PLAYING_TRACK(track2), TRUE);

	jammo_sequencer_play(JAMMO_SEQUENCER(sequencer));

	g_timeout_add(150, do_panning, track1);
	g_timeout_add(50, do_panning, track2);
	
	g_main_loop_run(main_loop);

	return 0;
}
