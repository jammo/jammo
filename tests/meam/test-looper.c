
#include <meam/jammo-meam.h>
#include <meam/jammo-sequencer.h>
#include <meam/jammo-editing-track.h>

#include "../../src/configure.h"
JammoEditingTrack* editing_track;
JammoSample *sample3;

static gboolean cb(gpointer user_data) {
	JammoSequencer* sequencer;

	sequencer = JAMMO_SEQUENCER(user_data);
	g_print("\nStop and remove last sample. Start again\n");
	jammo_sequencer_stop(sequencer);
	jammo_editing_track_remove_sample(editing_track, sample3);
	jammo_sequencer_play(sequencer);
	//jammo_sequencer_set_position(sequencer, 10000000000LLU);

	return FALSE;
}

#define file1 AUDIO_DIR "/wheel_game/Me_FolkMetalAccordion1_$t_$p_44_2.ogg"
#define file2 AUDIO_DIR "/wheel_game/Me_strings05_$t_$p_44_2.ogg"
#define file3 AUDIO_DIR "/wheel_game/Me_flute13_$t_$p_44_2.ogg"

int main(int argc, char** argv) {
	GMainLoop* main_loop;
	JammoSequencer* sequencer;
	JammoSample * sample1, *sample2;
	
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	main_loop = g_main_loop_new (NULL, FALSE);

	sequencer = jammo_sequencer_new();


	editing_track = jammo_editing_track_new();
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(editing_track));
	jammo_sequencer_set_pitch(sequencer, "A");
	jammo_sequencer_set_tempo(sequencer, 130);
	sample1=jammo_sample_new_from_file(file1);
	sample2=jammo_sample_new_from_file(file2);
	sample3=jammo_sample_new_from_file(file3);
	jammo_editing_track_add_sample(editing_track, sample1, 0);
	jammo_editing_track_add_sample(editing_track, sample2, 7000000000LLU);
	jammo_editing_track_add_sample(editing_track, sample3, 14000000000LLU);

	g_print("Playing files 0s, 7s and 14s\n");

	jammo_sequencer_play(JAMMO_SEQUENCER(sequencer));

	g_timeout_add(15000, cb, sequencer);

	g_main_loop_run(main_loop);

	return 0;
}
