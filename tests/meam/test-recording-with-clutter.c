
#include <meam/jammo-meam.h>
#include <meam/jammo-sequencer.h>
#include <meam/jammo-backing-track.h>
#include <meam/jammo-recording-track.h>
#include <meam/jammo-sample.h>
#include <clutter/clutter.h>

/* 
   install jammo-data. 
     backing_track is: /opt/jammo/themes/rock_city/backing_track.wav

    recording is saved into /tmp/recording.wav
     or (N900) /home/user/.jammo/recording.wav

To run: ./test-recording
*/

//N900 has very limited /tmp space.
#ifdef N900
#define RECORDING_FILENAME "/home/user/MyDocs/recording.wav"
#else
#define RECORDING_FILENAME "/tmp/recording.wav"
#endif

static void on_sequencer_stopped(JammoSequencer* sequencer, gpointer user_data) {
	JammoSample* sample;
	
	g_print("Playing...\n");
	sample = jammo_sample_new_from_file(RECORDING_FILENAME);
	g_signal_connect_swapped(sample, "stopped", G_CALLBACK(clutter_main_quit), NULL);
	jammo_sample_play(sample);
}

static void on_pitch_detected(JammoTrack* track, gfloat frequency, gpointer user_data) {
	g_print("Frequency: %f\n", frequency);
}

int main(int argc, char** argv) {
	JammoSequencer* sequencer;
	JammoBackingTrack* backing_track;
	JammoRecordingTrack* recording_track;
	ClutterActor* stage;
	ClutterActor* texture;
	ClutterAnimation* animation;
	
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	clutter_init(&argc, &argv);

	sequencer = jammo_sequencer_new();
	g_signal_connect(sequencer, "stopped", G_CALLBACK(on_sequencer_stopped), NULL);

	backing_track = jammo_backing_track_new("/opt/jammo/themes/city/110/backing_track.wav");
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(backing_track));
	
	recording_track = jammo_recording_track_new_with_pitch_detect(RECORDING_FILENAME);
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(recording_track));
	g_signal_connect(recording_track, "pitch-detected", G_CALLBACK(on_pitch_detected), NULL);

	stage = clutter_stage_get_default();
	
	texture = clutter_texture_new_from_file("/opt/jammo/jammo.png", NULL);
	clutter_container_add_actor(CLUTTER_CONTAINER(stage), texture);
	animation = clutter_actor_animate(texture, CLUTTER_LINEAR, 20000, "scale-x", 10.0, NULL);
	clutter_animation_set_loop(animation, TRUE);
	
	clutter_actor_show_all(stage);

	jammo_sequencer_play(JAMMO_SEQUENCER(sequencer));

	clutter_main();

	return 0;
}
