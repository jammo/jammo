
#include <meam/jammo-meam.h>
#include <meam/jammo-sample.h>

/* 
   To run:
   - ./test-sample-play <filename>
*/

static void on_stopped(JammoSample* sample, gpointer user_data) {
	GMainLoop* main_loop;
	
	main_loop = (GMainLoop*)user_data;
	g_main_loop_quit(main_loop);
}

int main(int argc, char** argv) {
	GMainLoop* main_loop;
	JammoSample* sample;
	
	if (argc != 2) {

		return 1;
	}
	
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	main_loop = g_main_loop_new (NULL, FALSE);

	sample = jammo_sample_new_from_file(argv[1]);
	g_signal_connect(sample, "stopped", G_CALLBACK(on_stopped), main_loop);
	jammo_sample_play(sample);
	g_object_unref(sample);
	
	g_main_loop_run(main_loop);

	return 0;
}
