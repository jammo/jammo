#include <chum/jammo-chum.h>
#include <tangle.h>

static ClutterActor* create_button(ClutterContainer* container) {
	ClutterActor* button;
	
	button = tangle_button_new_selectable_with_background_actors(
		tangle_texture_new("button_texture_normal.png"),
		tangle_texture_new("button_texture_interactive.png"),
		tangle_texture_new("button_texture_selected.png"));
	clutter_container_add_actor(container, button);

	return button;
}

static gboolean on_clicked(TangleButton* button, gpointer user_data) {
	ClutterActor* actor;
	
	actor = CLUTTER_ACTOR(user_data);

	clutter_actor_set_reactive(actor, FALSE);
	
	if (tangle_button_get_selected(button)) {
		jammo_chum_disable_all_actors(actor);
	} else {
		jammo_chum_enable_all_actors(actor);
	}

	clutter_actor_set_reactive(actor, TRUE);
	
	return FALSE;
}

int main(int argc, char** argv) {
	ClutterActor* stage;
	ClutterActor* widget;
	ClutterActor* button1;
	ClutterActor* button2;
	ClutterActor* button3;
	
	jammo_chum_init(&argc, &argv);
	
	stage = clutter_stage_get_default();
	widget = tangle_widget_new();
	tangle_widget_set_layout(TANGLE_WIDGET(widget), tangle_box_layout_new());
	clutter_container_add_actor(CLUTTER_CONTAINER(stage), widget);
	
	button1 = create_button(CLUTTER_CONTAINER(widget));
	button2 = create_button(CLUTTER_CONTAINER(widget));
	g_signal_connect(button1, "clicked", G_CALLBACK(on_clicked), button2);
	g_signal_connect(button2, "clicked", G_CALLBACK(on_clicked), button1);
	
	button3 = create_button(CLUTTER_CONTAINER(stage));
	clutter_actor_set_position(button3, 50.0, 100.0);
	g_signal_connect(button3, "clicked", G_CALLBACK(on_clicked), widget);
	
	
	clutter_actor_show_all(stage);
	
	clutter_main();
	
	return 0;
}
