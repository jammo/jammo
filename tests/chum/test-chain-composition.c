/*
 * test-chain-composition.c
 *
 * This file is part of JamMo.
 *
 * This is for testing chain composition. Currently it only creates an object
 * and gives it a few parameters
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther
 */

#include "../../src/chum/jammo-collaboration-chain-composition.h"
#include "../../src/meam/jammo-backing-track.h"

#include "../../src/meam/jammo-meam.h"
#include "../../src/chum/jammo-chum.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


int main (int argc, char **argv) {

  jammo_meam_init (&argc, &argv, "jammo_test_duration_cache");
  jammo_chum_init (&argc, &argv);

	JammoCollaborationChainComposition* collaboration_chain_composition = jammo_collaboration_chain_composition_new();

// ******** Testing JammoCollaborationGame functionality ********************
	char* mix_location = "test_location.ogg";
	g_object_set(G_OBJECT(collaboration_chain_composition),"mix-location",mix_location, NULL);

	// set backing track location
	char * btrack_location = "/opt/jammo/songs/easy/boat/comping.ogg";
	g_object_set(G_OBJECT(collaboration_chain_composition),"backing-track-location",btrack_location, NULL);

	printf("starting\n");
	jammo_collaboration_chain_composition_start(collaboration_chain_composition);
	jammo_collaboration_game_teacher_exit(JAMMO_COLLABORATION_GAME(collaboration_chain_composition));
	jammo_collaboration_game_create_song_file(JAMMO_COLLABORATION_GAME(collaboration_chain_composition));

	g_object_unref(collaboration_chain_composition);

	printf("everything done\n");

	return 0;
}

