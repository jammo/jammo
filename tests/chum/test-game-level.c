#include <meam/jammo-meam.h>
#include <chum/jammo-chum.h>
#include <chum/jammo-sample-button.h>
#include <chum/jammo-game-task.h>
#include <chum/jammo-game-level.h>
#include <chum/jammo-game-level-view.h>
#include <glib.h>
#include <clutter/clutter.h>

#define FILENAME "test-game-level.json"

static ClutterScript* script;

ClutterActor* jammo_get_actor_by_id(const char* id) {
	ClutterActor* actor = NULL;

	if (!(actor = CLUTTER_ACTOR(clutter_script_get_object(script, id)))) {
		g_warning("Actor '%s' not found.", id);
	}
	
	return actor;
}

void on_sample_button_listened(JammoGameTask* task, JammoSampleButton* sample_button) {
	jammo_game_task_set_act_completed(task, clutter_get_script_id(G_OBJECT(sample_button)), TRUE);
}

void on_game_started(JammoGameTask* task) {
	g_print("MENTOR: Try to play different loops.\n");
}

void on_act_completed(JammoGameTask* task, const gchar* act_name, guint acts_completed) {
	gint acts_left;
	
	acts_left = (gint)jammo_game_task_get_acts_to_complete(task) - (gint)acts_completed;
	if (acts_left > 0) {
		g_print("MENTOR: Good! Listen %u more loops.\n", acts_left);
	}
}

void on_act_redone(JammoGameTask* task, const gchar* act_name, guint acts_completed) {
	g_print("MENTOR: You had already listened this loop. Try another!\n");
}

void on_task_completed(JammoGameTask* task) {
	g_print("MENTOR: Excellent! Now you have accomplished this task. Please, proceed.\n");
}

gboolean on_start_button_clicked(JammoGameLevel* game_level, TangleButton* button) {
	const gchar* name;
	
	name = clutter_actor_get_name(CLUTTER_ACTOR(button));
	jammo_game_level_start_task(game_level, name);
	
	return FALSE;
}

gboolean on_stop_button_clicked(JammoGameLevel* game_level, TangleButton* button) {
	tangle_actor_hide_animated(TANGLE_ACTOR(jammo_get_actor_by_id("loops-view")));
	tangle_actor_show(TANGLE_ACTOR(jammo_get_actor_by_id("buttons-view")));
	
	return FALSE;
}

void on_game_level_completed(JammoGameLevel* game_level) {
	g_print("MENTOR: Now you have gone through both game levels! Feel free to do whatever you want now on...\n");
}

int main(int argc, char** argv) {
	ClutterActor* stage;
	GError* error = NULL;
	ClutterActor* actor;
	
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	jammo_chum_init(&argc, &argv);
	
	stage = clutter_stage_get_default();
	g_object_set(G_OBJECT(stage), "width", 800.0, "height", 480.0, NULL);

	g_print("%ld %ld %ld %ld %p %p %p\n", JAMMO_TYPE_SAMPLE_BUTTON, JAMMO_TYPE_GAME_TASK, JAMMO_TYPE_GAME_LEVEL, JAMMO_TYPE_GAME_LEVEL_VIEW, on_sample_button_listened, on_start_button_clicked, on_stop_button_clicked);
	
	script = clutter_script_new();
	if (!clutter_script_load_from_file(script, FILENAME, &error)) {
		g_critical("Error when loading '%s': %s", FILENAME, error->message);
	} else if (!(actor = CLUTTER_ACTOR(clutter_script_get_object(script, "main")))) {
		g_critical("Could not find an object named 'main' from '%s'.", FILENAME);
	} else {
		clutter_container_add_actor(CLUTTER_CONTAINER(stage), actor);
	}

	clutter_actor_show_all(stage);
	
	clutter_main();
	
	return 0;
}
