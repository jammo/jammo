#include <clutter/clutter.h>
#include <tangle.h>
#include <chum/jammo-chum.h>
#include <chum/jammo-mentor.h>
#include <chum/jammo-mentor-activity.h>
#include <meam/jammo-meam.h>

gboolean on_start_activity_timeout(gpointer user_data) {
	TangleActivity* activity;
	
	activity = TANGLE_ACTIVITY(user_data);
	tangle_activity_activate(activity);

	return FALSE;
}

int main(int argc, char** argv) {
	ClutterActor* stage;
	const gchar* filename;
	GError* error = NULL;
	ClutterScript* script;
	ClutterActor* actor;
	TangleActivity* activity;
	
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	jammo_chum_init(&argc, &argv);

	g_print("%ld %ld\n", (long)JAMMO_TYPE_MENTOR, (long)JAMMO_TYPE_MENTOR_ACTIVITY);
	
	stage = clutter_stage_get_default();
	g_object_set(G_OBJECT(stage), "width", 800.0, "height", 480.0, NULL);

	if (argc > 1) {
		filename = argv[1];
	} else {
		filename = "test-mentor-activity.json";
	}

	script = clutter_script_new();
	if (!clutter_script_load_from_file(script, filename, &error)) {
		g_critical("Error when loading '%s': %s", filename, error->message);
	} else if (!(actor = CLUTTER_ACTOR(clutter_script_get_object(script, "main")))) {
		g_critical("Could not find an object named 'main' from '%s'.", filename);
	} else {
		clutter_container_add_actor(CLUTTER_CONTAINER(stage), actor);
	}
	
	if (!(activity = TANGLE_ACTIVITY(clutter_script_get_object(script, "activity")))) {
		g_warning("Could not find an object named 'activity' from '%s'.", filename);
	} else {
		g_timeout_add(2000, on_start_activity_timeout, activity);
	}

	clutter_actor_show(stage);
	
	clutter_main();

	clutter_actor_destroy(stage);
	g_object_unref(script);
	
	return 0;
}
