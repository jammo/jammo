/*
 * jammo-collaboration-chain-composition.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther <mikko.gynther@lut.fi>
 */
 
#ifndef __JAMMO_COLLABORATION_CHAIN_COMPOSITION_H__
#define __JAMMO_COLLABORATION_CHAIN_COMPOSITION_H__

#include <glib.h>
#include <glib-object.h>
#include "jammo-collaboration-game.h"

#define JAMMO_TYPE_COLLABORATION_CHAIN_COMPOSITION (jammo_collaboration_chain_composition_get_type ())
#define JAMMO_COLLABORATION_CHAIN_COMPOSITION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_COLLABORATION_CHAIN_COMPOSITION, JammoCollaborationChainComposition))
#define JAMMO_IS_COLLABORATION_CHAIN_COMPOSITION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_COLLABORATION_CHAIN_COMPOSITION))
#define JAMMO_COLLABORATION_CHAIN_COMPOSITION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_COLLABORATION_CHAIN_COMPOSITION, JammoCollaborationChainCompositionClass))
#define JAMMO_IS_COLLABORATION_CHAIN_COMPOSITION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_COLLABORATION_CHAIN_COMPOSITION))
#define JAMMO_COLLABORATION_CHAIN_COMPOSITION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_COLLABORATION_CHAIN_COMPOSITION, JammoCollaborationChainCompositionClass))

typedef struct _JammoCollaborationChainCompositionPrivate JammoCollaborationChainCompositionPrivate;

typedef struct _JammoCollaborationChainComposition {
	JammoCollaborationGame parent_instance;
	JammoCollaborationChainCompositionPrivate* priv;
} JammoCollaborationChainComposition;

typedef struct _JammoCollaborationChainCompositionClass {
	JammoCollaborationGameClass parent_class;
} JammoCollaborationChainCompositionClass;

GType jammo_collaboration_chain_composition_get_type(void);

JammoCollaborationChainComposition* jammo_collaboration_chain_composition_new();

int jammo_collaboration_chain_composition_start(JammoCollaborationChainComposition * collaboration_chain_composition);

#endif
