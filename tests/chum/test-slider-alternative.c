/*
 * test-slider
 *
 * This file is part of JamMo.
 *
 * This is for testing slider virtual instrument playing.
 *
 * (c) 2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Aapo Rantalainen, Mikko Gynther
 */

#include <glib-object.h>
#include <tangle.h>
#include <meam/jammo-meam.h>
#include <meam/jammo-slider-track.h>
#include <chum/jammo-chum.h>
#include <math.h>

gboolean playing=FALSE;
static JammoSliderTrack *slider_track;
JammoSequencer *sequencer;
gfloat previous_freq=0.0;

// range in octaves
#define RANGE 3.0
#define OCTAVE_ON_SCREEN (799.0/RANGE)
#define BASE_FREQUENCY 130.81

#define TIMEOUT 1000000000

GstClockTime time_now=0;
GstClockTime time_of_previous_event=0;

int turn_slider_off() {

	if (playing) {
		time_now=jammo_sequencer_get_position(sequencer);
		if (time_now - time_of_previous_event > TIMEOUT) {
			playing=FALSE;
			jammo_slider_track_set_off_realtime(slider_track, previous_freq);
		}
	}

	return 1;
}

static void slider_motion(ClutterActor *actor, ClutterEvent *event, gpointer data){
	gfloat x_release, y_release;
	gfloat freq;
	clutter_event_get_coords (event, &x_release, &y_release);
	freq = BASE_FREQUENCY*pow(2.0, x_release/OCTAVE_ON_SCREEN);
	//jammo_slider_track_set_frequency_realtime(slider_track, freq);
}


static void slider_press(ClutterActor *actor, ClutterEvent *event, gpointer data){
	gfloat x_release, y_release;
	gfloat freq;
	clutter_event_get_coords (event, &x_release, &y_release);
	freq = BASE_FREQUENCY*pow(2.0, x_release/OCTAVE_ON_SCREEN);
	if (!playing) {
		jammo_slider_track_set_on_realtime(slider_track, freq);
		playing=TRUE;
	}
	else
		jammo_slider_track_set_frequency_realtime(slider_track, freq);
		
	time_of_previous_event=jammo_sequencer_get_position(sequencer);
	previous_freq=freq;
}


static void slider_release(ClutterActor *actor, ClutterEvent *event, gpointer data){
	gfloat x_release, y_release;
	gfloat freq;
	clutter_event_get_coords (event, &x_release, &y_release);
	freq = BASE_FREQUENCY*pow(2.0, x_release/OCTAVE_ON_SCREEN);
	//jammo_slider_track_set_off_realtime(slider_track, freq);
	time_of_previous_event=jammo_sequencer_get_position(sequencer);
	previous_freq=freq;
}


static ClutterActor* make_slider ()
{
  ClutterColor b_color = { 0, 255, 255, 255 };
  ClutterActor *box = clutter_rectangle_new_with_color (&b_color);
  clutter_actor_set_size (box, 800, 480);
  clutter_actor_show (box);

  clutter_actor_set_reactive (box, TRUE);
  g_signal_connect (box, "button-press-event", G_CALLBACK (slider_press),NULL);      
  g_signal_connect (box, "button-release-event", G_CALLBACK (slider_release),NULL);
  g_signal_connect (box, "motion-event", G_CALLBACK (slider_motion),NULL);

  return box;
}


int main (int argc, char **argv)
{
  ClutterActor *stage;

  jammo_meam_init (&argc, &argv, "jammo_test_duration_cache");
  jammo_chum_init (&argc, &argv);

  stage = clutter_stage_get_default ();
	#ifdef N900
	clutter_stage_set_fullscreen(CLUTTER_STAGE(stage), TRUE);
	#else
	clutter_actor_set_size(stage, 800.0, 480.0);
	#endif

	// add a callback for turning slider off
	g_timeout_add_full(G_PRIORITY_DEFAULT_IDLE,500,(GSourceFunc)turn_slider_off, NULL, NULL);

  sequencer = jammo_sequencer_new ();
  slider_track = jammo_slider_track_new (JAMMO_SLIDER_TYPE_FM_MODULATION);
  jammo_slider_track_set_recording(slider_track, TRUE);
  jammo_sequencer_add_track (sequencer, JAMMO_TRACK (slider_track));


  clutter_container_add (CLUTTER_CONTAINER (stage), make_slider(), NULL);

  jammo_sequencer_play (JAMMO_SEQUENCER (sequencer));
  clutter_actor_show_all (stage);
  clutter_main ();

  return 0;
}
