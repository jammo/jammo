/*
 * test-pair-composition.c
 *
 * This file is part of JamMo.
 *
 * This is for testing pair composition. Currently it only creates an object
 * and gives it a few parameters
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther
 */

#include "../../src/chum/3-6gui/jammo-collaboration-pair-composition.h"
#include "../../src/meam/jammo-backing-track.h"

#include "../../src/meam/jammo-meam.h"
#include "../../src/chum/jammo-chum.h"
#include "../../src/chum/jammo-editing-track-view.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


int main (int argc, char **argv) {

  jammo_meam_init (&argc, &argv, "jammo_test_duration_cache");
  jammo_chum_init (&argc, &argv);

	JammoCollaborationPairComposition* collaboration_pair_composition = jammo_collaboration_pair_composition_new();

// ******** Testing JammoCollaborationGame functionality ********************
	char* mix_location = "test_location.ogg";
	g_object_set(G_OBJECT(collaboration_pair_composition),"mix-location",mix_location, NULL);

// ******** Testing JammoCollaborationPairComposition functionality ********************
	// set backing track location
	char * btrack_location = "/opt/jammo/songs/easy/boat/comping.ogg";
	g_object_set(G_OBJECT(collaboration_pair_composition),"backing-track-location",btrack_location, NULL);

	// test setting and getting track views
	// garbage parameters, not meant to be used
	JammoEditingTrack * track1 = jammo_editing_track_new();
	JammoTrackView * track_view1 = JAMMO_TRACK_VIEW(jammo_track_view_new(track1, 3, 1000000,1.0,1.0));
	g_object_set(G_OBJECT(collaboration_pair_composition),"own-track-view",track_view1, NULL);
	JammoTrackView * track_view2;
	g_object_get(collaboration_pair_composition, "own-track-view", &track_view2, NULL);
	if (track_view1!=NULL && track_view1==track_view2) {
		printf("set and get own track view\n");
	}
	else {
		printf("error setting and getting own track view\n");
	}

	JammoEditingTrack * track2 = jammo_editing_track_new();
	JammoTrackView * track_view3 = JAMMO_TRACK_VIEW(jammo_track_view_new(track2, 3, 1000000,1.0,1.0));
	g_object_set(G_OBJECT(collaboration_pair_composition),"peer-track-view",track_view3, NULL);
	JammoTrackView * track_view4;
	g_object_get(collaboration_pair_composition, "peer-track-view", &track_view4, NULL);
	if (track_view3!=NULL && track_view3==track_view4) {
		printf("set and get track peer view\n");
	}
	else {
		printf("error setting and getting peer track view\n");
	}

	// TODO give a proper socket if this test code is used for actual testing.
	gint socket1=0;
	g_object_set(G_OBJECT(collaboration_pair_composition),"socket", socket1, NULL);
	gint socket2;
	g_object_get(collaboration_pair_composition, "socket", &socket2, NULL);
	if (socket1==socket2) {
		printf("socket get and set successfully\n");
	}
	else {
		printf("problem in getting and setting socket\n");
	}

	// can not start. it requires gems
	//printf("starting\n");
	//jammo_collaboration_pair_composition_start(collaboration_pair_composition, FALSE, 0, 0);
	
	jammo_collaboration_game_teacher_exit(JAMMO_COLLABORATION_GAME(collaboration_pair_composition));
	jammo_collaboration_game_create_song_file(JAMMO_COLLABORATION_GAME(collaboration_pair_composition));

	g_object_unref(collaboration_pair_composition);

	printf("everything done\n");

	return 0;
}

