/*
 * test-slider2
 *
 * This file is part of JamMo.
 *
 * This is for testing slider virtual instrument playing, storing events in sampler, get and set event lists and playback from sampler.
 *
 * (c) 2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Aapo Rantalainen, Mikko Gynther
 */

#include <glib-object.h>
#include <tangle.h>
#include <meam/jammo-meam.h>
#include <meam/jammo-slider-track.h>
#include <chum/jammo-chum.h>

#include <meam/jammo-backing-track.h>
#include <math.h>

JammoSequencer *sequencer;
static JammoSliderTrack *slider_track;

// range in octaves
#define RANGE 2.0
#define OCTAVE_ON_SCREEN (799.0/RANGE)
#define BASE_FREQUENCY 130.81


static gboolean on_play_button_clicked(TangleButton* button, gpointer user_data) {
	printf("starting play!\n");
	//jammo_sequencer_play(JAMMO_SEQUENCER(user_data));


	//Make AD-HOC sequencer and track to listen realtime track.
	GList* list= jammo_slider_track_get_event_list(JAMMO_SLIDER_TRACK(slider_track));
	jammo_slider_event_glist_to_file(list, "slider_events.txt");

	GList * list2 = jammo_slider_event_file_to_glist("slider_events.txt");	
	JammoSequencer* seq= jammo_sequencer_new();
	JammoSliderTrack*  playback_slider_track = jammo_slider_track_new (JAMMO_SLIDER_TYPE_KARPLUS);

  jammo_slider_track_set_recording (playback_slider_track, FALSE);
  jammo_slider_track_set_live (playback_slider_track, FALSE);
  jammo_sequencer_add_track (seq, JAMMO_TRACK (playback_slider_track));
	
	jammo_slider_track_set_event_list(playback_slider_track, list2);

	jammo_sequencer_play(JAMMO_SEQUENCER(seq));

	
	return FALSE;
}

static gboolean on_clear_button_clicked(TangleButton* button, gpointer user_data) {
	printf("clears events of track\n");
	jammo_slider_track_clear_events(slider_track);
	return FALSE;
}

static gboolean on_dump_button_clicked(TangleButton* button, gpointer user_data) {
	printf("dumps events of track\n");
	jammo_slider_track_dump_events(slider_track);
	return FALSE;
}

static void slider_motion(ClutterActor *actor, ClutterEvent *event, gpointer data){
gfloat x_release, y_release;
gfloat freq;
clutter_event_get_coords (event, &x_release, &y_release);
printf("move: X=%d\n",(gint)x_release);
freq = BASE_FREQUENCY*pow(2.0, x_release/OCTAVE_ON_SCREEN);
jammo_slider_track_set_frequency_realtime(slider_track, freq);
}


static void slider_press(ClutterActor *actor, ClutterEvent *event, gpointer data){
gfloat x_release, y_release;
gfloat freq;
clutter_event_get_coords (event, &x_release, &y_release);
printf("Press: X=%d\n",(gint)x_release);
freq = BASE_FREQUENCY*pow(2.0, x_release/OCTAVE_ON_SCREEN);
jammo_slider_track_set_on_realtime(slider_track, freq);
}


static void slider_release(ClutterActor *actor, ClutterEvent *event, gpointer data){
gfloat x_release, y_release;
gfloat freq;
clutter_event_get_coords (event, &x_release, &y_release);
printf("Release: X=%d\n",(gint)x_release);
freq = BASE_FREQUENCY*pow(2.0, x_release/OCTAVE_ON_SCREEN);
jammo_slider_track_set_off_realtime(slider_track, freq);
}


static ClutterActor* make_slider ()
{
  ClutterColor b_color = { 0, 255, 255, 255 };
  ClutterActor *box = clutter_rectangle_new_with_color (&b_color);
  clutter_actor_set_size (box, 800, 480);
  clutter_actor_show (box);

  clutter_actor_set_reactive (box, TRUE);
  g_signal_connect (box, "button-press-event", G_CALLBACK (slider_press),NULL);      
  g_signal_connect (box, "button-release-event", G_CALLBACK (slider_release),NULL);
  g_signal_connect (box, "motion-event", G_CALLBACK (slider_motion),NULL);

  return box;
}


int main (int argc, char **argv)
{

  ClutterActor *stage;

  jammo_meam_init (&argc, &argv, "jammo_test_duration_cache");
  jammo_chum_init (&argc, &argv);


  stage = clutter_stage_get_default ();
  //clutter_stage_set_fullscreen(CLUTTER_STAGE(stage), TRUE);
  clutter_actor_set_size (stage, 800.0, 480.0);


  sequencer = jammo_sequencer_new ();
  slider_track = jammo_slider_track_new (JAMMO_SLIDER_TYPE_KARPLUS);
	jammo_slider_track_set_recording(slider_track, TRUE);
  jammo_sequencer_add_track (sequencer, JAMMO_TRACK (slider_track));

  clutter_container_add (CLUTTER_CONTAINER (stage), make_slider(), NULL);

	//Play-button for starting sequencer
	ClutterActor* play_button = tangle_button_new();
	clutter_actor_set_position(play_button, 100.0, 0.0);
	tangle_widget_add_after(TANGLE_WIDGET(play_button), tangle_texture_new("test_image_100x74.jpg"), NULL, NULL, NULL);
	g_signal_connect(play_button, "clicked", G_CALLBACK(on_play_button_clicked), sequencer);
	clutter_container_add(CLUTTER_CONTAINER(stage), play_button, NULL);
	
	//button for clearing events
	ClutterActor* clear_button = tangle_button_new();
	clutter_actor_set_position(clear_button, 300.0, 0.0);
	tangle_widget_add_after(TANGLE_WIDGET(clear_button), tangle_texture_new("test_image_100x74.jpg"), NULL, NULL, NULL);
	g_signal_connect(clear_button, "clicked", G_CALLBACK(on_clear_button_clicked), sequencer);
	clutter_container_add(CLUTTER_CONTAINER(stage), clear_button, NULL);

  jammo_sequencer_play (JAMMO_SEQUENCER (sequencer));
  
  //button for dumping events
	ClutterActor* dump_button = tangle_button_new();
	clutter_actor_set_position(dump_button, 500.0, 0.0);
	tangle_widget_add_after(TANGLE_WIDGET(dump_button), tangle_texture_new("test_image_100x74.jpg"), NULL, NULL, NULL);
	g_signal_connect(dump_button, "clicked", G_CALLBACK(on_dump_button_clicked), sequencer);
	clutter_container_add(CLUTTER_CONTAINER(stage), dump_button, NULL);

  jammo_sequencer_play (JAMMO_SEQUENCER (sequencer));
  clutter_actor_show_all (stage);
  clutter_main ();

	// get list from slider track
	printf("events in slider's list\n");
	GList * list = jammo_slider_track_get_event_list(slider_track);
	GList * temp;
	JammoSliderEvent * event;
	for (temp=list;temp;temp=temp->next) {
		event=temp->data;
		printf(" freq: %f, type: %d, timestamp: %llu\n", event->freq, event->type, (unsigned long long)event->timestamp);
	}

  return 0;
}
