/*
 * test-group-composition.c
 *
 * This file is part of JamMo.
 *
 * This is for testing group composition. Currently it only creates an object
 * and gives it a few parameters
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther
 */

#include "../../src/chum/jammo-collaboration-group-composition.h"
#include "../../src/meam/jammo-playing-track.h"
#include "../../src/meam/jammo-editing-track.h"
#include "../../src/meam/jammo-backing-track.h"

#include "../../src/meam/jammo-meam.h"
#include "../../src/chum/jammo-chum.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main (int argc, char **argv) {

  jammo_meam_init (&argc, &argv, "jammo_test_duration_cache");
  jammo_chum_init (&argc, &argv);

	JammoCollaborationGroupComposition* collaboration_group_composition = jammo_collaboration_group_composition_new();

// ******** Testing JammoCollaborationGame functionality ********************
	char* mix_location = "test_location.ogg";
	g_object_set(G_OBJECT(collaboration_group_composition),"mix-location",mix_location, NULL);

	// set backing track location
	char * btrack_location = "/opt/jammo/songs/easy/boat/comping.ogg";
	g_object_set(G_OBJECT(collaboration_group_composition),"backing-track-location",btrack_location, NULL);

// ******** Testing JammoCollaborationGroupComposition *********************

	// set a backing track, this is just a test. not the way jammo_group_composition is supposed to be used
	g_object_get(collaboration_group_composition,"backing-track-location",&btrack_location, NULL);
	JammoBackingTrack * btrack = jammo_backing_track_new(btrack_location);
	g_object_set(G_OBJECT(collaboration_group_composition),"backing-track",btrack, NULL);
	JammoBackingTrack * btrack2;
	g_object_get(collaboration_group_composition,"backing-track",&btrack2, NULL);
	if (btrack==btrack2) {
		printf("backing track set and get successfully\n");
	}
	else {
		printf("problem with backing track set and get\n");
	}

	jammo_collaboration_group_composition_set_number_of_players(collaboration_group_composition,4);

// ******** Start **********************************************************

	// can not start. it requires gems
	//printf("starting\n");
	//jammo_collaboration_group_composition_start(collaboration_group_composition);

	printf("calling jammo_collaboration_game_teacher_exit\n");
	jammo_collaboration_game_teacher_exit(JAMMO_COLLABORATION_GAME(collaboration_group_composition));
	jammo_collaboration_game_create_song_file(JAMMO_COLLABORATION_GAME(collaboration_group_composition));

	g_object_unref(collaboration_group_composition);

	printf("everything done\n");

	return 0;
}

