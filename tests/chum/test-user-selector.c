#include <chum/jammo-chum.h>
#include <gems/gems_utils.h>
#include <chum/7-12gui/community_utilities.h>
#include <configure.h>
#include <tangle.h>
#include <stdio.h>

#define A_WIDTH 100
#define A_HEIGHT 160
#define BORDER 10

gchar** list = NULL;
guint listsize = 0;
guint listpos = 0;
//static Avatar avtr = {0,0,0,0};

void change_username(ClutterText* text)
{
	clutter_text_set_text(text,list[listpos]);
	
	//community_get_avatar(&avtr,260);
	
}

static gboolean left_clicked(ClutterActor* actor, ClutterEvent* event, gpointer user_data)
{
	if(list)
	{
		if ((listpos > 0) && (listsize > 0))	listpos--;
		else if (listpos == 0) listpos = listsize - 1;
		
		change_username((ClutterText*)user_data);
	}
	return TRUE;
}

static gboolean right_clicked(ClutterActor* actor, ClutterEvent* event, gpointer user_data)
{
	if(list)
	{
		if(listpos < listsize-1) listpos++;
		else listpos = 0;
		change_username((ClutterText*)user_data);
	}
	return TRUE;
}

static gboolean avatar_clicked(ClutterActor* actor, ClutterEvent* event, gpointer user_data)
{
	gchar* login = g_strdup_printf("Logging as \"%s\"",list[listpos]);
	clutter_text_set_text((ClutterText*)user_data,login);
	return TRUE;
}

int main(int argc, char* argv[])
{
	configure_init_directories(FALSE);
	
	ClutterColor stage_color = { 0x00, 0x00, 0x00, 0xff }; /* Black */
	ClutterColor actor_color_left = { 0x88, 0x88, 0x88, 0x99 };
	ClutterColor actor_color_right = { 0x55, 0x55, 0x55, 0x99 };
	ClutterColor actor_color = { 0xff, 0xff, 0xfa, 0x99 };
  
	jammo_chum_init(&argc, &argv);
  
	/* Get the stage and set its size and color: */
	ClutterActor *stage = clutter_stage_get_default ();
	clutter_actor_set_size (stage, 800, 480);
	clutter_stage_set_color (CLUTTER_STAGE (stage), &stage_color);
  
	// left box
	ClutterActor* left = clutter_rectangle_new_with_color(&actor_color_left);
	clutter_actor_set_size(left,A_WIDTH,A_HEIGHT);
	clutter_actor_set_position(left,clutter_actor_get_width(stage)/4-A_WIDTH,clutter_actor_get_height(stage)/3);
	clutter_container_add_actor(CLUTTER_CONTAINER(stage),left);
	clutter_actor_show(left);
	clutter_actor_set_reactive (left, TRUE);
  
	// right box
	ClutterActor* right = clutter_rectangle_new_with_color(&actor_color_right);
	clutter_actor_set_size(right,A_WIDTH,A_HEIGHT);
	clutter_actor_set_position(right,clutter_actor_get_width(stage)*3/4,clutter_actor_get_height(stage)/3);
	clutter_container_add_actor(CLUTTER_CONTAINER(stage),right);
	clutter_actor_show(right);
	clutter_actor_set_reactive (right, TRUE);

	ClutterActor* text = clutter_text_new_full("Sans 18","text",&actor_color);
	clutter_actor_set_size(text,380,80);
	clutter_actor_set_position(text,clutter_actor_get_width(stage)/4+BORDER,400);
	clutter_container_add_actor(CLUTTER_CONTAINER(stage),text);
	clutter_actor_show(text);
  
	ClutterActor* avatar = clutter_rectangle_new_with_color(&actor_color);
	clutter_actor_set_size(avatar,380,320);
	clutter_actor_set_position(avatar,clutter_actor_get_width(stage)/4+BORDER,80);
	clutter_container_add_actor(CLUTTER_CONTAINER(stage),avatar);
	clutter_actor_show(avatar);
	clutter_actor_set_reactive (avatar, TRUE);
	g_signal_connect(left,"button-press-event",G_CALLBACK(left_clicked),text);
	g_signal_connect(right,"button-press-event",G_CALLBACK(right_clicked),text);
	g_signal_connect(avatar,"button-press-event",G_CALLBACK(avatar_clicked),text);
  
	clutter_actor_show_all(stage);
	
	list = gems_list_saved_profiles(&listsize);
	
	if(list) change_username((ClutterText*)text);
	
	clutter_main();
	
	gems_free_list_saved_profiles(list);
	
 	return 0;
}
