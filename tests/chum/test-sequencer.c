/*
 * test-sequencer
 *
 * This file is part of JamMo.
 *
 * Most left image is sample-button (can be dragged to track)
 * Button on top, removes sample from slot 5. (there are two slots in same grid)
 * Bottom left adds new sample to slot 1.
 * Bottom right is play button.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 *          Aapo Rantalainen
 */

/**********
 * Known bugs: Timeline is not adjusted to anything (including sample durations).
 */

#include <meam/jammo-meam.h>
#include <meam/jammo-playing-track.h>
#include <chum/jammo-chum.h>
#include <chum/jammo-sample-button.h>
#include <chum/jammo-editing-track-view.h>
#include <chum/jammo-cursor.h>
#include <tangle.h>

static const ClutterColor red = { 255, 0, 0, 128 };

static gboolean create_new_sample(TangleButton* button, gpointer track_view) {
	ClutterActor* sample_button;
	sample_button = jammo_sample_button_new_from_files("test_image_100x74.jpg", "/opt/jammo/themes/animal/110/animalw_110_vibraslap_2.wav");
	jammo_editing_track_view_add_jammo_sample_button(JAMMO_EDITING_TRACK_VIEW(track_view), JAMMO_SAMPLE_BUTTON(sample_button), 1);
	
	return FALSE;
}


static gboolean remove_sample(TangleButton* button, gpointer track_view) {
	jammo_editing_track_view_remove_jammo_sample_button_from_slot(JAMMO_EDITING_TRACK_VIEW(track_view),  5); //slot_number
	return FALSE;
}


static gboolean on_play_button_clicked(TangleButton* button, gpointer user_data) {
	jammo_sequencer_play(JAMMO_SEQUENCER(user_data));

	return FALSE;
}

static gboolean on_dropping_incompatible_sample(JammoEditingTrackView* track_view, JammoSampleButton* sample_button, gpointer user_data) {
	g_print("Dropping incompatible sample!\n");

	return TRUE; /* Try: FALSE */
}

int main(int argc, char** argv) {
	ClutterActor* stage;
	ClutterActor* box;
	ClutterActor* sample_button;
	JammoSequencer* sequencer;
	JammoEditingTrack* backing_track;
	JammoSample* sample;
	JammoEditingTrack* track;
	ClutterActor* track_view;
	ClutterActor* texture;
	ClutterActor* cursor;
	ClutterActor* play_button;
	ClutterActor* button_for_creating;
	ClutterActor* button_for_removing;
	
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	jammo_chum_init(&argc, &argv);

	stage = clutter_stage_get_default();
	//clutter_stage_set_fullscreen(CLUTTER_STAGE(stage), TRUE);
	clutter_actor_set_size(stage, 800.0, 480.0);

	box = tangle_widget_new();
	clutter_actor_set_size(box, 800.0, 480.0);
	clutter_container_add(CLUTTER_CONTAINER(stage), box, NULL);

	sample_button = jammo_sample_button_new_with_type_from_files(JAMMO_SAMPLE_MELODICAL, "test_image_100x74.jpg", "/opt/jammo/themes/animal/110/animalw_110_pluck_2.wav");
	jammo_sample_button_set_text(JAMMO_SAMPLE_BUTTON(sample_button), "4");
	clutter_actor_set_position(sample_button, 100.0, 100.0);
	clutter_container_add_actor(CLUTTER_CONTAINER(box), sample_button);
	
	sequencer = jammo_sequencer_new();
	
	backing_track = jammo_editing_track_new();
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(backing_track));
	sample = jammo_sample_new_from_file("/opt/jammo/themes/animal/110/backing_track.wav");
	jammo_editing_track_add_sample(backing_track, sample, 0);

	track = jammo_editing_track_new();
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(track));
	
	track_view = jammo_editing_track_view_new(track, 25, 2181818182LL, 50.0, 80.0);
	jammo_editing_track_view_set_sample_type(JAMMO_EDITING_TRACK_VIEW(track_view), JAMMO_SAMPLE_HARMONICAL /* try: JAMMO_SAMPLE_MELODICAL*/);
	g_signal_connect(track_view, "dropping-incompatible-sample", G_CALLBACK(on_dropping_incompatible_sample), NULL);
	g_object_set(track_view, "line-every-nth-slot", 2, 
	             "disabled-slots-begin", 1,
		     "disabled-slots-end", 1, NULL);
	clutter_actor_set_position(track_view, 0.0, 200.0);
	clutter_container_add_actor(CLUTTER_CONTAINER(box), track_view);
	
	texture = clutter_rectangle_new_with_color(&red);
	clutter_actor_set_size(texture, 10.0, 78.0);
	cursor = jammo_cursor_new(sequencer, texture);
	tangle_actor_set_depth_position(TANGLE_ACTOR(cursor), 1);
	clutter_actor_set_position(cursor, 0.0, 200.0);
	clutter_actor_set_size(cursor, 1250.0, 80.0);
	clutter_container_add_actor(CLUTTER_CONTAINER(box), cursor);
	
	play_button = tangle_button_new();
	
#ifdef animation_enabled
	ClutterAnimation* animation;
	animation = clutter_actor_animate (play_button, CLUTTER_LINEAR, 60000, "scale-y", 0.1,  NULL);
#endif
	
	clutter_actor_set_position(play_button, 700.0, 400.0);
	tangle_widget_add_after(TANGLE_WIDGET(play_button), tangle_texture_new("test_image_100x74.jpg"), NULL, NULL, NULL);
	g_signal_connect(play_button, "clicked", G_CALLBACK(on_play_button_clicked), sequencer);
	clutter_container_add(CLUTTER_CONTAINER(box), play_button, NULL);
	
	button_for_creating = tangle_button_new();
	clutter_actor_set_position(button_for_creating, 200.0, 400.0);
	tangle_widget_add_after(TANGLE_WIDGET(button_for_creating), tangle_texture_new("test_image_100x74.jpg"), NULL, NULL, NULL);
	g_signal_connect(button_for_creating, "clicked", G_CALLBACK(create_new_sample), track_view);
	clutter_container_add(CLUTTER_CONTAINER(box), button_for_creating, NULL);
	
	button_for_removing = tangle_button_new();
	clutter_actor_set_position(button_for_removing, 200.0, 0.0);
	tangle_widget_add_after(TANGLE_WIDGET(button_for_removing), tangle_texture_new("test_image_100x74.jpg"), NULL, NULL, NULL);
	g_signal_connect(button_for_removing, "clicked", G_CALLBACK(remove_sample), track_view);
	clutter_container_add(CLUTTER_CONTAINER(box), button_for_removing, NULL);

	clutter_actor_show_all(stage);
	clutter_main();
	
	return 0;
}
