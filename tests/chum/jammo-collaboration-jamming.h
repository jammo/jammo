/*
 * jammo-collaboration-jamming.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther <mikko.gynther@lut.fi>
 */
 
#ifndef __JAMMO_COLLABORATION_JAMMING_H__
#define __JAMMO_COLLABORATION_JAMMING_H__

#include <glib.h>
#include <glib-object.h>
#include "jammo-collaboration-game.h"

#define JAMMO_TYPE_COLLABORATION_JAMMING (jammo_collaboration_jamming_get_type ())
#define JAMMO_COLLABORATION_JAMMING(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_COLLABORATION_JAMMING, JammoCollaborationJamming))
#define JAMMO_IS_COLLABORATION_JAMMING(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_COLLABORATION_JAMMING))
#define JAMMO_COLLABORATION_JAMMING_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_COLLABORATION_JAMMING, JammoCollaborationJammingClass))
#define JAMMO_IS_COLLABORATION_JAMMING_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_COLLABORATION_JAMMING))
#define JAMMO_COLLABORATION_JAMMING_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_COLLABORATION_JAMMING, JammoCollaborationJammingClass))

// message types in synchronization
enum {
	JAMMINGSYNC_INIT = 1,
	JAMMINGSYNC_RTT,
	JAMMINGSYNC_RTT_REPLY,
	JAMMINGSYNC_START,
	JAMMINGSYNC_START_OK,
	JAMMINGSYNC_RTT_AVERAGE,
	JAMMINGSYNC_ERROR
};

// states in synchronization
enum {
	JAMMINGSYNCSTATE_WAIT_FOR_INIT = 1,
	JAMMINGSYNCSTATE_CHECK_RTT,
	JAMMINGSYNCSTATE_WAIT_FOR_START_OK,
	JAMMINGSYNCSTATE_MONITOR_RTT
};

// states in jamming
enum {
	JAMMINGSTATE_INIT = 1,
	JAMMINGSTATE_STARTED,
	JAMMINGSTATE_FINISHED,
	JAMMINGSTATE_GENERATING_MIX,
	JAMMINGSTATE_DONE_GENERATING,
	JAMMINGSTATE_ERROR
};

// positions in event buffer
enum {
	JAMMING_TYPE = 0,
	JAMMING_NOTE = 1,
	JAMMING_TIMESTAMP = 2
};

// types of midi events
#define JAMMINGMIDI_NOTE_ON 0
#define JAMMINGMIDI_NOTE_OFF 1
// empty events are used to show that connection is working in real-time
#define JAMMINGMIDI_EMPTY 2
// resync is only used in communications to request a new synchronization
// it is never stored or sent to sampler
#define JAMMINGMIDI_RESYNC 3

// times to monitor rtt before and after start command
#define JAMMING_RTT_COUNT 5
// maximum acceptable rtt in us
#define JAMMING_RTT_THRESHOLD 5000
// number of times to try synchronization
#define JAMMING_MAXSYNCHRONIZATION 5
// threshold for ending jamming
// the time that is waited after backing track has ended
#define JAMMING_ENDTHRESHOLD 100000000
// after LATESTSYNCPOINT resynchronization will not be done
// 2 means half of backing track duration, 3 means one third from the beginning
// 4 means one fourth from the beginning etc.
#define JAMMING_LATESTSYNCPOINT 2

// sending midi stream
// the length of one event in bytes
#define JAMMING_EVENTLEN 10
// the number of events to buffer
#define JAMMING_EVENTBUFFERSIZE 50
// send interval in nanoseconds
#define JAMMING_SEND_INTERVAL 5000000
// allow skipping empty events for 100 ms
#define JAMMING_ALLOWSKIP 100000000/JAMMING_SEND_INTERVAL

// receiving midi stream
// time in ns that note-on events are valid, 50 ms
#define JAMMING_LATETHRESHOLD 50000000
// time in ns that connection is considered real-time if no events are received
// 150 ms
#define JAMMING_NOTEHANGDELAY 150000000
// stop after DELAYQUIT ns if no events are received
#define JAMMING_DELAYQUIT 5000000000LLU
// threshold in ns for seeking, 15 ms
#define JAMMING_EARLYTHRESHOLD 15000000
// threshold for resynchronization
#define JAMMING_SYNCTHRESHOLD 10

// volume level for backing track
#define JAMMING_BACKING_TRACK_VOLUME 0.35
// fade in length in ns
#define JAMMING_FADE_IN_LENGTH 2000000000

// minimum time of successfull jamming that leads to generating a mix
// there is no point to generate anything if jamming was not successfull at all
#define JAMMING_MINTIMEFORGENERATINGMIX 5000000000LLU

typedef struct _JammoCollaborationJammingPrivate JammoCollaborationJammingPrivate;

typedef struct _JammoCollaborationJamming {
	JammoCollaborationGame parent_instance;
	JammoCollaborationJammingPrivate* priv;
} JammoCollaborationJamming;

typedef struct _JammoCollaborationJammingClass {
	JammoCollaborationGameClass parent_class;
} JammoCollaborationJammingClass;

GType jammo_collaboration_jamming_get_type(void);

JammoCollaborationJamming* jammo_collaboration_jamming_new();

int jammo_collaboration_jamming_get_socket(JammoCollaborationJamming * collaboration_jamming);

void jammo_collaboration_jamming_set_socket(JammoCollaborationJamming * collaboration_jamming, int sockfd);

gboolean jammo_collaboration_jamming_get_master(JammoCollaborationJamming * collaboration_jamming);

void jammo_collaboration_jamming_set_master(JammoCollaborationJamming * collaboration_jamming, gboolean master);

int jammo_collaboration_jamming_get_own_instrument(JammoCollaborationJamming * collaboration_jamming);

void jammo_collaboration_jamming_set_own_instrument(JammoCollaborationJamming * collaboration_jamming, int instrument);

int jammo_collaboration_jamming_get_peer_instrument(JammoCollaborationJamming * collaboration_jamming);

void jammo_collaboration_jamming_set_peer_instrument(JammoCollaborationJamming * collaboration_jamming, int instrument);


char * jammo_collaboration_jamming_get_own_midi_location(JammoCollaborationJamming * collaboration_jamming);

void jammo_collaboration_jamming_set_own_midi_location(JammoCollaborationJamming * collaboration_jamming, char * location);

char * jammo_collaboration_jamming_get_peer_midi_location(JammoCollaborationJamming * collaboration_jamming);

void jammo_collaboration_jamming_set_peer_midi_location(JammoCollaborationJamming * collaboration_jamming, char * location);

int jammo_collaboration_jamming_start(JammoCollaborationJamming * collaboration_jamming);

#endif
