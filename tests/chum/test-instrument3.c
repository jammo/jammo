/*
 * test-instrumet3
 *
 * This file is part of JamMo.
 *
 * Loading and saving midis
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen 
 */


#include <meam/jammo-meam.h>
#include <meam/jammo-instrument-track.h>
#include <meam/jammo-midi.h>
#include <chum/jammo-chum.h>
#include <tangle.h>

#include <tangle.h>
#include <glib-object.h>
#include <ctype.h>
#include <string.h>
#include <clutter/clutter.h>




static JammoInstrumentTrack *instrument_track;

static void
instrument_click (ClutterActor * actor, ClutterEvent * event, gpointer data)
{
  int octave = GPOINTER_TO_INT (data) / 100;
  int note = GPOINTER_TO_INT (data) - 100 * octave;
  char notename = note+octave*12;

  jammo_instrument_track_note_on_realtime (instrument_track, notename);
}


static ClutterActor *
make_instrument_grid (int type)
{
  //regarding what instrument we are using, make boxes. FIXME this is very ad-hoc.
  int min_j, max_j;
  if (type == 0)
    {
      min_j = 3;
      max_j = 7;
    }
  else if (type == 1)
    {
      min_j = 1;
      max_j = 2;
    }
  else if (type == 2)
    {
      min_j = 2;
      max_j = 6;
    }
  else
    {
      min_j = 0;
      max_j = 8;
    }
  ClutterColor transparency = { 255, 255, 255, 0 };
  ClutterColor dark_color = { 25, 25, 12, 255 };
  ClutterActor *container = clutter_group_new ();
  int i;
  int j;
  // width / notes = 800/12 =66
  // height / octaves = 480/8 = 60
  for (i = 0; i < 12; i++)
    {
      for (j = min_j; j < max_j; j++)
	{
	  if (type == 0 && j == 6 && i > 0)	//Flute doesn't have these notes
	    continue;
	  if ((type == 1 && i == 1) || (type == 1 && i == 3))	//Drumkit doesn't have these notes 
	    continue;
	  if (type == 2 && j == 5 && i > 0)	//Ud doesn't have these notes
	    continue;
	  ClutterActor *box =
	    clutter_rectangle_new_with_color (&transparency);
	  clutter_rectangle_set_border_color (CLUTTER_RECTANGLE (box),
					      &dark_color);
	  clutter_rectangle_set_border_width (CLUTTER_RECTANGLE (box), 1);
	  clutter_actor_set_size (box, 66, 60);
	  clutter_actor_set_position (box, 66 * i, 60 * j);
	  clutter_actor_show (box);
	  clutter_container_add_actor (CLUTTER_CONTAINER (container), box);
	  clutter_actor_set_reactive (box, TRUE);
	  g_signal_connect (box, "button-press-event",
			    G_CALLBACK (instrument_click),
			    GINT_TO_POINTER (j * 100 + i));
	}
    }
  return container;
}

static gboolean on_play_button_clicked(TangleButton* button, gpointer track) {
	//jammo_sequencer_play(JAMMO_SEQUENCER(user_data));
	jammo_instrument_track_dump_notes(JAMMO_INSTRUMENT_TRACK(track));
	return FALSE;
}

static gboolean on_play2_button_clicked(TangleButton* button, gpointer user_data) {
	printf("load notes from file and adds them to track\n");
	jammo_instrument_track_load_to_track_from_file(instrument_track,"../../notes");
	return FALSE;
}

static gboolean on_play3_button_clicked(TangleButton* button, gpointer user_data) {
	printf("clears events of track\n");
	jammo_instrument_track_clear_events(instrument_track);
	return FALSE;
}

int
main (int argc, char **argv)
{
	printf("left is load button\t right is save button\n");
  JammoSequencer *sequencer;
  ClutterActor *stage;

  jammo_meam_init (&argc, &argv, "jammo_test_duration_cache");
  jammo_chum_init (&argc, &argv);


  stage = clutter_stage_get_default ();
  //clutter_stage_set_fullscreen(CLUTTER_STAGE(stage), TRUE);
  clutter_actor_set_size (stage, 800.0, 480.0);


  int type = 2;  //0= flute, 1=drumkit ,2=ud
  sequencer = jammo_sequencer_new ();
  instrument_track = jammo_instrument_track_new (type);
  //instrument_track = midi_parser_load_from_file("../../notes");

  jammo_instrument_track_set_realtime (instrument_track, TRUE);
  jammo_sequencer_add_track (sequencer, JAMMO_TRACK (instrument_track));


  clutter_container_add (CLUTTER_CONTAINER (stage),
  make_instrument_grid (type), NULL);

  jammo_sequencer_play (JAMMO_SEQUENCER (sequencer));

	ClutterActor* play_button;
	play_button = tangle_button_new();
	clutter_actor_set_position(play_button, 700.0, 400.0);
	tangle_widget_add_after(TANGLE_WIDGET(play_button), tangle_texture_new("test_image_100x74.jpg"), NULL, NULL, NULL);
	g_signal_connect(play_button, "clicked", G_CALLBACK(on_play_button_clicked), instrument_track);
	clutter_container_add(CLUTTER_CONTAINER(stage), play_button, NULL);
	
	ClutterActor* play2_button;
	play2_button = tangle_button_new();
	clutter_actor_set_position(play2_button, 500.0, 400.0);
	tangle_widget_add_after(TANGLE_WIDGET(play2_button), tangle_texture_new("test_image_100x74.jpg"), NULL, NULL, NULL);
	g_signal_connect(play2_button, "clicked", G_CALLBACK(on_play2_button_clicked), sequencer);
	clutter_container_add(CLUTTER_CONTAINER(stage), play2_button, NULL);
	
	ClutterActor* play3_button;
	play3_button = tangle_button_new();
	clutter_actor_set_position(play3_button, 300.0, 400.0);
	tangle_widget_add_after(TANGLE_WIDGET(play3_button), tangle_texture_new("test_image_100x74.jpg"), NULL, NULL, NULL);
	g_signal_connect(play3_button, "clicked", G_CALLBACK(on_play3_button_clicked), sequencer);
	clutter_container_add(CLUTTER_CONTAINER(stage), play3_button, NULL);
	
	
  clutter_actor_show_all (stage);
  clutter_main ();

  return 0;
}
