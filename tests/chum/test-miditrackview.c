/*
 * test-miditrackview.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */

/**********

 */

//Optional backing_track
//#define USE_BACKING_TRACK

#include <meam/jammo-meam.h>
#include <meam/jammo-playing-track.h>
#include <meam/jammo-instrument-track.h>
#include <meam/jammo-midi.h>
#include <chum/jammo-chum.h>
#include <chum/jammo-note-button.h>
#include <chum/jammo-miditrack-view.h>
#include <chum/jammo-cursor.h>
#include <tangle.h>



#include <meam/jammo-editing-track.h>


static const ClutterColor red = { 255, 0, 0, 128 };



static gboolean on_play_button_clicked(TangleButton* button, gpointer user_data) {
	jammo_sequencer_play(JAMMO_SEQUENCER(user_data));

	return FALSE;
}


static gboolean on_button2_clicked(TangleButton* button, gpointer user_data) {
	//Will resize total area of miditrack to (500,100).
	jammo_miditrack_view_zoom_to_fit(JAMMO_MIDITRACK_VIEW(user_data),500.0, 100.0);
	return FALSE;
}

static void on_duration_notify(GObject* object, GParamSpec* pspec, gpointer user_data) {

	ClutterActor* stage;
	ClutterActor* box;
	ClutterActor* note_button;
	JammoSequencer* sequencer;


	JammoInstrumentTrack* track;
	ClutterActor* track_view;
	ClutterActor* texture;
	ClutterActor* cursor;
	ClutterActor* play_button, *button2;

	JammoSample* sample;
	sample = JAMMO_SAMPLE(object);


	sequencer = JAMMO_SEQUENCER(user_data);
	// The duration of backing track is used to set fixed duration for other tracks
	guint64 duration;
	duration = jammo_sample_get_duration(sample);
	g_print("Duration: %" G_GUINT64_FORMAT "\n", duration);


	//Instrument track
	track = jammo_instrument_track_new(2); //2=UD
	jammo_instrument_track_set_realtime(track, FALSE);
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(track));




	stage = clutter_stage_get_default();
	#ifdef N900
	clutter_stage_set_fullscreen(CLUTTER_STAGE(stage), TRUE);
	#else
	clutter_actor_set_size(stage, 800.0, 480.0);
	#endif

	//Layout for button_container (box_for_buttons)
	TangleLayout* layout = tangle_box_layout_new();
	//tangle_layout_set_direction(layout, TANGLE_LAYOUT_DIRECTION_RTL);

	ClutterActor* box_for_buttons = tangle_widget_new();
	tangle_widget_set_layout(TANGLE_WIDGET(box_for_buttons), layout);
	clutter_container_add_actor(CLUTTER_CONTAINER(stage), box_for_buttons);

	//Notes are currently added like jammo-sample-buttons (dragging from somewhere)
	note_button = jammo_note_button_new_without_events();
	clutter_actor_set_position(note_button, 0.0, 0.0);
	clutter_actor_set_size(note_button,64.0, 64.0);
	clutter_container_add_actor(CLUTTER_CONTAINER(box_for_buttons), note_button);

	//Play-button for starting sequencer
	play_button = tangle_button_new();
	clutter_actor_set_position(play_button, 100.0, 0.0);
	tangle_widget_add_after(TANGLE_WIDGET(play_button), tangle_texture_new("test_image_100x74.jpg"), NULL, NULL, NULL);
	g_signal_connect(play_button, "clicked", G_CALLBACK(on_play_button_clicked), sequencer);
	clutter_container_add(CLUTTER_CONTAINER(box_for_buttons), play_button, NULL);


	//container area for track and cursor
	box = tangle_widget_new();
	clutter_actor_set_position(box, 0.0, 80.0);
	clutter_actor_set_size(box, 800.0, 400.0);
	clutter_container_add(CLUTTER_CONTAINER(stage), box, NULL);


	//Duration is ~54sec
	int number_of_slots = 54*4; //How many slize we want? Now one grid is ~1/4 seconds.

	//JammoInstrumentTrack* miditrack, guint n_slots, guint64 slot_duration, gfloat slot_width, gfloat slot_height, guint h_note, guint l_note) 
	//Trac 25=number of slots, duration_of_slot = 0,25s, grid_Size 64*64, notes from high..low
	track_view = jammo_miditrack_view_new(track, number_of_slots, (duration/number_of_slots), 64.0, 64.0,56,50);
	clutter_actor_set_position(track_view, 1.0, 1.0); //unless leftmost and topmost borders are not visible
	clutter_container_add_actor(CLUTTER_CONTAINER(box), track_view);



	//zoom-button
	button2 = tangle_button_new();
	clutter_actor_set_position(button2, 300.0, 0.0);
	tangle_widget_add_after(TANGLE_WIDGET(button2), tangle_texture_new("test_image_100x74.jpg"), NULL, NULL, NULL);
	g_signal_connect(button2, "clicked", G_CALLBACK(on_button2_clicked), track_view);
	clutter_container_add(CLUTTER_CONTAINER(box_for_buttons), button2, NULL);



	//Cursor
	texture = clutter_rectangle_new_with_color(&red);
	clutter_actor_set_size(texture, 10.0, 78.0);
	cursor = jammo_cursor_new(sequencer, texture);
	tangle_actor_set_depth_position(TANGLE_ACTOR(cursor), 1);
	clutter_actor_set_position(cursor, 0.0, 200.0);
	clutter_actor_set_size(cursor, 64.0*number_of_slots, 180.0); //width=grid_width*slot_numbers, height (not used) = grid_height*(high-low)
	clutter_container_add_actor(CLUTTER_CONTAINER(box), cursor);



	//Add one note
	JammoMidiEvent* start_event = malloc(sizeof(JammoMidiEvent));
		start_event->note = 53;
		start_event->timestamp = 300000000LL;
		start_event->type = JAMMOMIDI_NOTE_ON;

	JammoMidiEvent* stop_event = malloc(sizeof(JammoMidiEvent));
		stop_event->note = 53;
		stop_event->timestamp = 350000000LL;
		stop_event->type = JAMMOMIDI_NOTE_OFF;

	jammo_miditrack_view_add_note(JAMMO_MIDITRACK_VIEW(track_view),start_event,stop_event);



	//Add events from file
	GList* events = jammomidi_file_to_glist("../../notes.txt");
	jammo_miditrack_view_add_event_list(JAMMO_MIDITRACK_VIEW(track_view),events);



	clutter_actor_show_all(stage);
	g_signal_handlers_disconnect_by_func(object, G_CALLBACK(on_duration_notify), box);
}


int main(int argc, char** argv) {
	JammoSequencer* sequencer;


	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	jammo_chum_init(&argc, &argv);


	sequencer = jammo_sequencer_new();

	JammoEditingTrack* backing_track;
	JammoSample* sample;
	backing_track = jammo_editing_track_new();
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(backing_track));
	sample = jammo_sample_new_from_file("/opt/jammo/themes/animal/110/backing_track.wav");
	jammo_editing_track_add_sample(backing_track, sample, 0);

	g_signal_connect(sample, "notify::duration", G_CALLBACK(on_duration_notify), sequencer);

	clutter_main();

	return 0;
}
