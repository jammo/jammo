/*
 * test-mentor.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include <meam/jammo-meam.h>
#include <chum/jammo-chum.h>
#include <chum/jammo-mentor.h>

static void on_spoken2(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer user_data) {
	jammo_mentor_speak_and_highlight(mentor, "test_sound.wav", 250.0, 248.0);
}

static void on_spoken1(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer user_data) {
	jammo_mentor_highlight_with_callback(mentor, 50.0, 100.0, 3000, on_spoken2, NULL);
}

static gboolean activate_mentor(gpointer user_data) {
	JammoMentor* mentor;
	
	mentor = JAMMO_MENTOR(user_data);
	jammo_mentor_speak_with_callback(mentor, "test_sound.wav", on_spoken1, NULL);

	return FALSE;
}

int main(int argc, char** argv) {
	ClutterActor* stage;
	const ClutterColor red = { 255, 0, 0, 255 };
	ClutterActor* rectangle;
	ClutterActor* texture;
	ClutterActor* mentor;
	
	jammo_meam_init(&argc, &argv, "jammo_test_duration_cache");
	jammo_chum_init(&argc, &argv);
	
	stage = clutter_stage_get_default();
	//clutter_stage_set_fullscreen(CLUTTER_STAGE(stage), TRUE);
	clutter_actor_set_size(stage, 800.0, 480.0);

	rectangle = clutter_rectangle_new_with_color(&red);
	clutter_actor_set_size(rectangle, 3.0, 3.0);
	clutter_actor_set_position(rectangle, 49.0, 99.0);
	clutter_container_add_actor(CLUTTER_CONTAINER(stage), rectangle);

	texture = tangle_texture_new("test_image_100x74.jpg");
	g_assert(texture != NULL);
	
	mentor = jammo_mentor_new(texture, 1.0, 2.0);
	jammo_mentor_set_hotspot(JAMMO_MENTOR(mentor), 100.0, 74.0);
	jammo_mentor_set_idle_speech(JAMMO_MENTOR(mentor), "test_sound.wav");
	
	clutter_container_add_actor(CLUTTER_CONTAINER(stage), mentor);
	
	g_timeout_add(2000, activate_mentor, mentor);
	
	clutter_actor_show_all(stage);
	clutter_main();
	
	return 0;
}
