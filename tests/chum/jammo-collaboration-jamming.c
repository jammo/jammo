/*
 * jammo-collaboration-jamming.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther <mikko.gynther@lut.fi>
 */

#include "jammo-collaboration-game.h"
#include "jammo-collaboration-jamming.h"

G_DEFINE_TYPE(JammoCollaborationJamming, jammo_collaboration_jamming, JAMMO_TYPE_COLLABORATION_GAME);

#include "../meam/jammo-sequencer.h"
#include "../meam/jammo-instrument-track.h"
#include "../meam/jammo-backing-track.h"
#include "../meam/jammo-midi.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#include <clutter/clutter.h>

// globals
int * gp_jamming_state;
JammoCollaborationJamming * g_collaboration_jamming;

// function prototypes
static int slavesynchronize(gpointer data);
static int mastersynchronize(gpointer data);
static ClutterActor * make_instrument_grid (int type);
static void add_event_to_buffer(JammoCollaborationJamming* collaboration_jamming, char type,
		char note, guint64 timestamp);
static void press (ClutterActor * actor, ClutterEvent * event, gpointer data);
static void release (ClutterActor * actor, ClutterEvent * event, gpointer data);
static int send_midi(gpointer data);
static int receive_midi(gpointer data);
void jammo_collaboration_jamming_on_sequencer_stopped_recording(JammoSequencer* sequencer, gpointer data);
int store_jamming_data(JammoCollaborationJamming* collaboration_jamming);
void exit_jamming(JammoCollaborationJamming * collaboration_jamming);
static void clean_up(JammoCollaborationJamming * collaboration_jamming);

enum {
	PROP_0/*,
	PROP_ADD_PROPERTIES_HERE*/
};

struct _JammoCollaborationJammingPrivate {
	// these have to be filled before calling jamming function
	// tcp socket for communication with peer
	int sockfd;
	gboolean master;
	gint own_instrument;
	gint peer_instrument;
	char * own_midi_location;
	char * peer_midi_location;

	// following are used automatically and do not need to be filled
	// sequencer and instrument tracks
	JammoInstrumentTrack *own_instrument_track;
	JammoInstrumentTrack *peer_instrument_track;

	// a buffer for events that are to be sent
	JammoMidiEvent eventbuffer[JAMMING_EVENTBUFFERSIZE];

	// master or slave in synchronization
	int syncmaster;
	// state of jamming
	int jamming_state;
	// number of sent events
	guint64 sent_events;
	// average rtt is used in synchronizing while playing, in us
	//guint64 rttaverage;
	// next event to send
	int currentevent;
	// latest event written to buffer
	int latestevent;
	// time in ns of last received event, used to stop jamming if it is not possible
	// due to a bad connection. also used in turning possible hanging notes of in stored list
	guint64 time_of_last_received_event;
	// time for last sent event is needed for turning one's own hanging notes of in stored list
	guint64 time_of_last_sent_event;
	// duration of backing track to determine when to stop jamming
	guint64 backing_track_duration;
	// midi event lists for storing finished jamming
	GList* own_event_list;
	GList* peer_event_list;
	// allnotesoff variable tells if all peer notes are turned off
	// due to not receiving events in NOTEHANGDELAY ns
	gboolean allnotesoff;
	// number of failed synchronizations is used to cancel jamming
	// if synchronization does not seem to success
	int syncfails;
};


JammoCollaborationJamming* jammo_collaboration_jamming_new() {
	printf("new\n");
	return JAMMO_COLLABORATION_JAMMING(g_object_new(JAMMO_TYPE_COLLABORATION_JAMMING, NULL));
}


static void jammo_collaboration_jamming_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoCollaborationJamming * collaboration_jamming;
	
	collaboration_jamming = JAMMO_COLLABORATION_JAMMING(object);

	
	switch (prop_id) {
		/*case PROP_BACKING_TRACK_FILENAME:
			g_assert(!collaboration_game->priv->filename);
			collaboration_game->priv->filename = g_strdup(g_value_get_string(value));
			break;*/
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_collaboration_jamming_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
	JammoCollaborationJamming * collaboration_jamming;
	
	collaboration_jamming = JAMMO_COLLABORATION_JAMMING(object);

	switch (prop_id) {
		/*case PROP_FILENAME:
			g_value_set_string(value, backing_track->priv->filename);
			break;*/
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

// implementations for base class functions

// implementation for teacher exit
void jammo_collaboration_jamming_teacher_exit(JammoCollaborationGame * collaboration_game) {
	JammoCollaborationJamming * jamming;
	jamming = JAMMO_COLLABORATION_JAMMING(collaboration_game);
	// TODO implement teacher exit
	printf("jammo_collaboration_jamming_teacher_exit called\n");
}

// create_song_file
// return values 0 success, 1 can not open file
int jammo_collaboration_jamming_create_song_file(JammoCollaborationGame * collaboration_game) {
	JammoCollaborationJamming * jamming;
	jamming = JAMMO_COLLABORATION_JAMMING(collaboration_game);
	// TODO implement
	printf("jammo_collaboration_jamming_create_song_file called\n");

	return 0;
}

static GObject* jammo_collaboration_jamming_constructor(GType type, guint n_properties, GObjectConstructParam* properties) {
	GObject* object;
	JammoCollaborationJamming * collaboration_jamming;
	
	object = G_OBJECT_CLASS(jammo_collaboration_jamming_parent_class)->constructor(type, n_properties, properties);

	collaboration_jamming = JAMMO_COLLABORATION_JAMMING(object);

	printf("constructor\n");
	return object;
}


static void jammo_collaboration_jamming_finalize(GObject* object) {
	JammoCollaborationJamming* collaboration_jamming;
	
	collaboration_jamming = JAMMO_COLLABORATION_JAMMING(object);

	G_OBJECT_CLASS(jammo_collaboration_jamming_parent_class)->finalize(object);
}

static void jammo_collaboration_jamming_dispose(GObject* object) {
	G_OBJECT_CLASS(jammo_collaboration_jamming_parent_class)->dispose(object);
}

static void jammo_collaboration_jamming_class_init(JammoCollaborationJammingClass* collaboration_jamming_class) {
	printf("class init\n");
	GObjectClass* gobject_class = G_OBJECT_CLASS(collaboration_jamming_class);
	JammoCollaborationGameClass* collaboration_game_class = JAMMO_COLLABORATION_GAME_CLASS(collaboration_jamming_class);

	gobject_class->constructor = jammo_collaboration_jamming_constructor;
	gobject_class->finalize = jammo_collaboration_jamming_finalize;
	gobject_class->dispose = jammo_collaboration_jamming_dispose;
	gobject_class->set_property = jammo_collaboration_jamming_set_property;
	gobject_class->get_property = jammo_collaboration_jamming_get_property;

	collaboration_game_class->teacher_exit = jammo_collaboration_jamming_teacher_exit;
	collaboration_game_class->create_song_file = jammo_collaboration_jamming_create_song_file;

	g_type_class_add_private(gobject_class, sizeof(JammoCollaborationJammingPrivate));
}

static void jammo_collaboration_jamming_init(JammoCollaborationJamming* collaboration_jamming) {
	collaboration_jamming->priv = G_TYPE_INSTANCE_GET_PRIVATE(collaboration_jamming, JAMMO_TYPE_COLLABORATION_JAMMING, JammoCollaborationJammingPrivate);
}

// get and set functions

int jammo_collaboration_jamming_get_socket(JammoCollaborationJamming * collaboration_jamming) {
	return collaboration_jamming->priv->sockfd;
}

void jammo_collaboration_jamming_set_socket(JammoCollaborationJamming * collaboration_jamming, int sockfd) {
	collaboration_jamming->priv->sockfd=sockfd;
}

gboolean jammo_collaboration_jamming_get_master(JammoCollaborationJamming * collaboration_jamming) {
	return collaboration_jamming->priv->master;
}

void jammo_collaboration_jamming_set_master(JammoCollaborationJamming * collaboration_jamming, gboolean master) {
	collaboration_jamming->priv->master=master;
}

int jammo_collaboration_jamming_get_own_instrument(JammoCollaborationJamming * collaboration_jamming) {
	return collaboration_jamming->priv->own_instrument;
}

void jammo_collaboration_jamming_set_own_instrument(JammoCollaborationJamming * collaboration_jamming, int instrument) {
	collaboration_jamming->priv->own_instrument=instrument;
}

int jammo_collaboration_jamming_get_peer_instrument(JammoCollaborationJamming * collaboration_jamming) {
	return collaboration_jamming->priv->peer_instrument;
}

void jammo_collaboration_jamming_set_peer_instrument(JammoCollaborationJamming * collaboration_jamming, int instrument) {
	collaboration_jamming->priv->peer_instrument=instrument;
}

char * jammo_collaboration_jamming_get_own_midi_location(JammoCollaborationJamming * collaboration_jamming) {
	return collaboration_jamming->priv->own_midi_location;
}

void jammo_collaboration_jamming_set_own_midi_location(JammoCollaborationJamming * collaboration_jamming, char * location) {
	collaboration_jamming->priv->own_midi_location=location;
}

char * jammo_collaboration_jamming_get_peer_midi_location(JammoCollaborationJamming * collaboration_jamming) {
	return collaboration_jamming->priv->peer_midi_location;
}

void jammo_collaboration_jamming_set_peer_midi_location(JammoCollaborationJamming * collaboration_jamming, char * location) {
	collaboration_jamming->priv->peer_midi_location=location;
}

// functions needed in jamming

// signal handler for SIGPIPE
// if the other end does not close connection properly we do not want to crash
static void signalhandler( int signal_parameter ) {
  *gp_jamming_state=JAMMINGSTATE_ERROR;
}

// this callback receives backing track duration from sequencer
static void on_duration_notify(GObject* object, GParamSpec* param_spec, gpointer user_data) {
	guint64 duration;

	JammoCollaborationJamming * collaboration_jamming = (JammoCollaborationJamming*)user_data;
	
	g_object_get(object, "duration", &duration, NULL);
	printf("Duration: %lu ms\n", (gulong)(duration / 1000000));
	collaboration_jamming->priv->backing_track_duration=duration;
}


// synchronization function for slave
// sends init, replies to rtt messages, counts the time specified by master
static int slavesynchronize(gpointer data) {
	char buffer[JAMMING_EVENTLEN+1];
	struct timeval tv_select, tv_clock, tv_countstart;
	struct timezone tz;
	int rcvbytes, counttime=0, timeprev=-1, timeleft=1000, seconds, secondsprev;
	fd_set rfds;
	int rttaverage=0;
	static JammoCollaborationJamming * collaboration_jamming;

	collaboration_jamming = (JammoCollaborationJamming *)data;

	memset(buffer, 0, JAMMING_EVENTLEN+1);

	// due to synchronization a fade in is needed, volume will be increased during playback
	JammoBackingTrack * btrack;
	g_object_get(collaboration_jamming,"backing-track",&btrack, NULL);
	jammo_playing_track_set_volume(JAMMO_PLAYING_TRACK(btrack), 0.0);

	printf("synchronizing, please wait\n");

	// send init message
	buffer[0]=JAMMINGSYNC_INIT;
	if (send(collaboration_jamming->priv->sockfd, buffer, JAMMING_EVENTLEN, 0)==-1) {
		// count failures and do not attempt to synchronize forever
		(collaboration_jamming->priv->syncfails)++;
		if (collaboration_jamming->priv->syncfails >= JAMMING_MAXSYNCHRONIZATION) {
			printf("synchronize failed for too many times\n");
			collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
			return 0;
		}
		return 1;
	}
	fsync(collaboration_jamming->priv->sockfd);

	// loop until it is time to start
	while (1) {

		// if already counting for start
		if (timeprev>=0) {
			// check current time
			timeprev=timeleft;
			secondsprev=timeprev/1000000;
			gettimeofday(&tv_clock, &tz);
			// calculate remaining time in us
			timeleft=(tv_countstart.tv_sec + counttime - tv_clock.tv_sec) * 1000000
				+ tv_countstart.tv_usec - tv_clock.tv_usec;
			seconds=timeleft/1000000;

			// if time to start
			if (timeleft<=10)
				break;
		}

		// receive a message
		// timeout with select
		tv_select.tv_sec = 0;
		tv_select.tv_usec = 10;
		FD_ZERO(&rfds);
		FD_SET(collaboration_jamming->priv->sockfd, &rfds);

		if (select((collaboration_jamming->priv->sockfd)+1, &rfds, NULL, NULL, &tv_select)==-1) {
			perror("select");
			// count failures and do not attempt to synchronize forever
			(collaboration_jamming->priv->syncfails)++;
			if (collaboration_jamming->priv->syncfails >= JAMMING_MAXSYNCHRONIZATION) {
				printf("synchronize failed for too many times\n");
				collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
				return 0;
			}
			return 1;
		}

		// get_message if any
		if (FD_ISSET(collaboration_jamming->priv->sockfd, &rfds)) {
			//printf("got something\n");
			if ((rcvbytes = recv(collaboration_jamming->priv->sockfd, buffer, JAMMING_EVENTLEN, 0)) == -1) {
					perror("recv");
				// count failures and do not attempt to synchronize forever
				(collaboration_jamming->priv->syncfails)++;
				if (collaboration_jamming->priv->syncfails >= JAMMING_MAXSYNCHRONIZATION) {
					printf("synchronize failed for too many times\n");
					collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
					return 0;
				}
				return 1;
			}

			if (buffer[0]==JAMMINGSYNC_RTT) {
				// send back JAMMINGSYNC_RTT_REPLY
				buffer[0]=JAMMINGSYNC_RTT_REPLY;
				if (send(collaboration_jamming->priv->sockfd, buffer, JAMMING_EVENTLEN, 0)==-1) {
					// count failures and do not attempt to synchronize forever
					(collaboration_jamming->priv->syncfails)++;
					if (collaboration_jamming->priv->syncfails >= JAMMING_MAXSYNCHRONIZATION) {
						printf("synchronize failed for too many times\n");
						collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
						return 0;
					}
					return 1;
				}
				fsync(collaboration_jamming->priv->sockfd);
			}
			else if (buffer[0]==JAMMINGSYNC_START) {
				// get time
				gettimeofday(&tv_countstart, &tz);
				counttime=(int)buffer[1];
				buffer[1]=0;
				// send back JAMMINGSYNC_START_OK
				buffer[0]=JAMMINGSYNC_START_OK;
				if (send(collaboration_jamming->priv->sockfd, buffer, JAMMING_EVENTLEN, 0)==-1) {
					// count failures and do not attempt to synchronize forever
					(collaboration_jamming->priv->syncfails)++;
					if (collaboration_jamming->priv->syncfails >= JAMMING_MAXSYNCHRONIZATION) {
						printf("synchronize failed for too many times\n");
						collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
						return 0;
					}
					return 1;
				}
				fsync(collaboration_jamming->priv->sockfd);
				// start counting
				timeprev=0;
				gettimeofday(&tv_clock, &tz);
				// calculate remaining time in us
				timeleft=(tv_countstart.tv_sec + counttime - tv_clock.tv_sec) * 1000000
					+ tv_countstart.tv_usec - tv_clock.tv_usec;
			}
			else if (buffer[0]==JAMMINGSYNC_RTT_AVERAGE) {
				rttaverage=ntohl(*(int*)(&buffer[1]));
			}
			else {
				// garbage received
				// count failures and do not attempt to synchronize forever
				(collaboration_jamming->priv->syncfails)++;
				if (collaboration_jamming->priv->syncfails >= JAMMING_MAXSYNCHRONIZATION) {
					printf("synchronize failed for too many times\n");
					collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
					return 0;
				}
				return 1;
			}
		}
	}

	JammoSequencer * sequencer;
	g_object_get(collaboration_jamming, "sequencer", &sequencer, NULL);
  jammo_sequencer_play (sequencer);
	collaboration_jamming->priv->jamming_state=JAMMINGSTATE_STARTED;
	printf("done\n");
	return 0;
}

static int mastersynchronize(gpointer data) {
	char buffer[JAMMING_EVENTLEN+1];
	struct timeval tv_select, tv_recvd, tv_countstart, tv_sent, tv_clock;
	struct timezone tz;
	int rcvbytes, counttime=0, timeprev, timeleft, seconds, secondsprev, goodrttcount=0, rttcount=0, rttaverage32=0;
	int start_rtt=0;
	fd_set rfds;
	int state=JAMMINGSYNCSTATE_WAIT_FOR_INIT;
	guint64 rttaverage64=0;
	static JammoCollaborationJamming * collaboration_jamming;

	collaboration_jamming = (JammoCollaborationJamming *)data;

	// due to synchronization a fade in is needed, volume will be increased during playback
	JammoBackingTrack * btrack;
	g_object_get(collaboration_jamming, "backing-track", &btrack, NULL);
	jammo_playing_track_set_volume(JAMMO_PLAYING_TRACK(btrack), 0.0);

	printf("synchronizing, please wait\n");

	memset(buffer, 0, JAMMING_EVENTLEN+1);

	while (1) {
		// receive a message
		// timeout with select
		tv_select.tv_sec = 5;
		tv_select.tv_usec = 0;
		FD_ZERO(&rfds);
		FD_SET(collaboration_jamming->priv->sockfd, &rfds);

		if (select((collaboration_jamming->priv->sockfd)+1, &rfds, NULL, NULL, &tv_select)==-1) {
			perror("select");
			printf("synchronization failed\n");
			collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
			return 0;
		}

		// get_message if any
		if (FD_ISSET(collaboration_jamming->priv->sockfd, &rfds)) {
			//printf("got something\n");
			if ((rcvbytes = recv(collaboration_jamming->priv->sockfd, buffer, JAMMING_EVENTLEN, 0)) == -1) {
					perror("recv");
				printf("synchronization failed\n");
				collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
				return 0;
			}
			gettimeofday(&tv_recvd, &tz);
			if (buffer[0]==JAMMINGSYNC_INIT) {
				if (state==JAMMINGSYNCSTATE_WAIT_FOR_INIT) {
					state=JAMMINGSYNCSTATE_CHECK_RTT;
				}
				else {
					printf("got init in wrong state, starting again\n");
					state=JAMMINGSYNCSTATE_CHECK_RTT;
					goodrttcount=0;
					rttcount=0;
					counttime=0;
					rttaverage64=0;
					(collaboration_jamming->priv->syncfails)++;
					// count failures and do not attempt to synchronize forever
					if (collaboration_jamming->priv->syncfails >= JAMMING_MAXSYNCHRONIZATION) {
						printf("synchronize failed for too many times\n");
						collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
						return 0;
					}
				}
				buffer[0]=JAMMINGSYNC_RTT;
				if (send(collaboration_jamming->priv->sockfd, buffer, JAMMING_EVENTLEN, 0)==-1) {
					printf("synchronization failed\n");
					collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
					return 0;
				}
				fsync(collaboration_jamming->priv->sockfd);
				gettimeofday(&tv_sent, &tz);
			}
			if (buffer[0]==JAMMINGSYNC_RTT_REPLY) {
				if (state==JAMMINGSYNCSTATE_CHECK_RTT || state==JAMMINGSYNCSTATE_MONITOR_RTT) {
					// if the second has changed
					if (tv_recvd.tv_sec != tv_sent.tv_sec) {
						tv_recvd.tv_usec+=1000000;
					}
					// if good enough rtt for x times in a row
					if ((tv_recvd.tv_sec - tv_sent.tv_sec)*1000000+ (tv_recvd.tv_usec - tv_sent.tv_usec)<JAMMING_RTT_THRESHOLD) {
						goodrttcount++;
						rttcount++;
						rttaverage64+=(tv_recvd.tv_sec - tv_sent.tv_sec)*1000000+ (tv_recvd.tv_usec - tv_sent.tv_usec);
					}
					else {
						printf("too long rtt. starting all over again\n");
						state=JAMMINGSYNCSTATE_CHECK_RTT;
						goodrttcount=0;
						rttcount=0;
						rttaverage64=0;
						(collaboration_jamming->priv->syncfails)++;
						// count failures and do not attempt to synchronize forever
						if (collaboration_jamming->priv->syncfails >= JAMMING_MAXSYNCHRONIZATION) {
							printf("synchronize failed for too many times\n");
							collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
							return 0;
						}

					}
					if (goodrttcount>JAMMING_RTT_COUNT) {
						if (state==JAMMINGSYNCSTATE_CHECK_RTT) {
							state=JAMMINGSYNCSTATE_WAIT_FOR_START_OK;
							buffer[0]=JAMMINGSYNC_START;
							counttime=1;
							buffer[1]=counttime;
							if (send(collaboration_jamming->priv->sockfd, buffer, JAMMING_EVENTLEN, 0)==-1) {
								printf("synchronization failed\n");
								collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
								return 0;
							}
							fsync(collaboration_jamming->priv->sockfd);
							gettimeofday(&tv_countstart, &tz);
						}
						else {
							// synchronization done
							break;
						}
					}
					else {
						// send rtt
						buffer[0]=JAMMINGSYNC_RTT;
						if (send(collaboration_jamming->priv->sockfd, buffer, JAMMING_EVENTLEN, 0)==-1) {
							printf("synchronization failed\n");
							collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
							return 0;
						}
						fsync(collaboration_jamming->priv->sockfd);
						gettimeofday(&tv_sent, &tz);
					}
				}
			}
			if (buffer[0]==JAMMINGSYNC_START_OK) {
				if (state==JAMMINGSYNCSTATE_WAIT_FOR_START_OK) {
					rttcount++;
					if ((tv_recvd.tv_sec - tv_countstart.tv_sec)*1000000+ (tv_recvd.tv_usec -
											tv_countstart.tv_usec)<JAMMING_RTT_THRESHOLD) {
						rttaverage64+=(tv_recvd.tv_sec - tv_countstart.tv_sec)*1000000+ (tv_recvd.tv_usec -
												tv_countstart.tv_usec);
						start_rtt= (tv_recvd.tv_sec - tv_countstart.tv_sec)*1000000+ (tv_recvd.tv_usec -
												tv_countstart.tv_usec);
						state=JAMMINGSYNCSTATE_MONITOR_RTT;
					}
					else {
						printf("too long rtt. starting all over again\n");
						state=JAMMINGSYNCSTATE_CHECK_RTT;
						goodrttcount=0;
						rttcount=0;
						rttaverage64=0;
						// count failures and do not attempt to synchronize forever
						if (collaboration_jamming->priv->syncfails >= JAMMING_MAXSYNCHRONIZATION) {
							printf("synchronize failed for too many times\n");
							collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
							return 0;
						}
					}
				}
				else {
					// start ok in wrong state
					printf("got init in wrong state, starting again\n");
					state=JAMMINGSYNCSTATE_CHECK_RTT;
					goodrttcount=0;
					rttcount=0;
					counttime=0;
					rttaverage64=0;
					// count failures and do not attempt to synchronize forever
					if (collaboration_jamming->priv->syncfails >= JAMMING_MAXSYNCHRONIZATION) {
						printf("synchronize failed for too many times\n");
						collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
						return 0;
					}
				}
				// send rtt
				buffer[0]=JAMMINGSYNC_RTT;
				if (send(collaboration_jamming->priv->sockfd, buffer, JAMMING_EVENTLEN, 0)==-1) {
					printf("synchronization failed\n");
					collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
					return 0;
				}
				fsync(collaboration_jamming->priv->sockfd);
				gettimeofday(&tv_sent, &tz);
			}
		}
	}

	//printf("good enough rtt\n");
	if (rttcount) {
		rttaverage32=rttaverage64/rttcount;
	}

	//printf("average rtt: %d us, rtt measured %d times\n", rttaverage, rttcount);

	// send average rtt to slave
	buffer[0]=JAMMINGSYNC_RTT_AVERAGE;
	*(int*)&buffer[1]=htonl(rttaverage32);
	if (send(collaboration_jamming->priv->sockfd, buffer, JAMMING_EVENTLEN, 0)==-1) {
		printf("synchronization failed\n");
		collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
		return 0;
	}
	fsync(collaboration_jamming->priv->sockfd);

	// rtt of start message is used in delaying the start of server
	tv_countstart.tv_usec += start_rtt/2;
	if (tv_countstart.tv_usec>999999) {
		tv_countstart.tv_sec+=1;
		tv_countstart.tv_usec-=1000000;
	}

	// print if second changes
	timeprev=0;
	gettimeofday(&tv_clock, &tz);
	// use microseconds in comparison
	timeleft=(tv_countstart.tv_sec + counttime - tv_clock.tv_sec) * 1000000
		+ tv_countstart.tv_usec - tv_clock.tv_usec;

	while (1) {
		timeprev=timeleft;
		secondsprev=timeprev/1000000;
		gettimeofday(&tv_clock, &tz);
		timeleft=(tv_countstart.tv_sec + counttime - tv_clock.tv_sec) * 1000000
			+ tv_countstart.tv_usec - tv_clock.tv_usec;
		seconds=timeleft/1000000;

		if (timeleft<=10)
			break;

		usleep(10);
	}

	JammoSequencer * sequencer;
	g_object_get(collaboration_jamming, "sequencer", &sequencer, NULL);
  jammo_sequencer_play (sequencer);
	collaboration_jamming->priv->jamming_state=JAMMINGSTATE_STARTED;
	printf("done\n");
	return 0;
}

// as simple as possible ui
static ClutterActor * make_instrument_grid (int type) {

  ClutterColor transparency = { 255, 255, 255, 0 };
	ClutterColor finish= { 100, 100, 100, 255 };
  ClutterColor dark_color = { 25, 25, 12, 255 };
  ClutterActor *container = clutter_group_new ();
  int i;
  int j;
  // width / notes = 800/12 =66
  // height / octaves = 480/8 = 60
	for (i = 0; i < 12; i++) {
		for (j = 0; j < 8; j++) {
			ClutterActor *box;
			if (i==0 && j==0) {
				box=clutter_rectangle_new_with_color (&finish);
			}
			else {
				box=clutter_rectangle_new_with_color (&transparency);
			}
			clutter_rectangle_set_border_color (CLUTTER_RECTANGLE (box),
				&dark_color);
			clutter_rectangle_set_border_width (CLUTTER_RECTANGLE (box), 1);
			clutter_actor_set_size (box, 66, 60);
			clutter_actor_set_position (box, 66 * i, 60 * j);
			clutter_actor_show (box);
			clutter_container_add_actor (CLUTTER_CONTAINER (container), box);
			clutter_actor_set_reactive (box, TRUE);
			g_signal_connect (box, "button-press-event",
				G_CALLBACK (press), GINT_TO_POINTER (j * 100 + i));
			g_signal_connect (box, "button-release-event",
				G_CALLBACK (release), GINT_TO_POINTER (j * 100 + i));
		}
	}
  return container;
}

// a buffer is used to store multiple events that happen in a very short period of time
// events are sent as a constant stream of single events so in some cases (e.g. chords) buffering is needed
static void add_event_to_buffer(JammoCollaborationJamming * collaboration_jamming, char type, char note, guint64 timestamp) {
	static int i;
	if ( (collaboration_jamming->priv->currentevent) - (collaboration_jamming->priv->latestevent)==1 || (collaboration_jamming->priv->currentevent)-(collaboration_jamming->priv->latestevent)==-JAMMING_EVENTBUFFERSIZE) {
		printf("event does not fit to buffer, oldest event from buffer was overwritten\n");
		printf("latest: %d current: %d\n", collaboration_jamming->priv->latestevent, collaboration_jamming->priv->currentevent);
	}

	i=(collaboration_jamming->priv->latestevent)+1;
	if (i==JAMMING_EVENTBUFFERSIZE) {
		i=0;
	}

	// place event in buffer
	collaboration_jamming->priv->eventbuffer[i].type = type;
	collaboration_jamming->priv->eventbuffer[i].note = note;
	collaboration_jamming->priv->eventbuffer[i].timestamp = timestamp;

	collaboration_jamming->priv->latestevent=i;

	// store also own events for saving a file
	// own events could be asked from sampler but then the events would be different
	// on peer device due to sampler latency
	jammomidi_store_event_to_glist(&(collaboration_jamming->priv->own_event_list), note, type, timestamp);

	collaboration_jamming->priv->time_of_last_sent_event=timestamp;
}

// press callback for ui, note on is passed forward
// it is possible to get a stuck local and remote note by not releasing in the same box as pressed
static void press (ClutterActor * actor, ClutterEvent * event, gpointer data) {
  int octave = GPOINTER_TO_INT (data) / 100;
  int note = GPOINTER_TO_INT (data) - 100 * octave;
  char midinote = note+octave*12;

	if (g_collaboration_jamming->priv->jamming_state==JAMMINGSTATE_STARTED) {
		// end button
		if (octave==0 && note==0) {
			printf("ending\n");
			g_collaboration_jamming->priv->jamming_state=JAMMINGSTATE_FINISHED;
		}
		else {
			// notes
			jammo_instrument_track_note_on_realtime (g_collaboration_jamming->priv->own_instrument_track,
				midinote);

			JammoSequencer * sequencer;
			g_object_get(g_collaboration_jamming, "sequencer", &sequencer, NULL);
			add_event_to_buffer(g_collaboration_jamming, JAMMINGMIDI_NOTE_ON, midinote, jammo_sequencer_get_position(sequencer));
		}
	}
}

// release callback for ui, note off is passed forward
// it is possible to get a stuck local and remote note by not releasing in the same box as pressed
static void release (ClutterActor * actor, ClutterEvent * event, gpointer data) {
  int octave = GPOINTER_TO_INT (data) / 100;
  int note = GPOINTER_TO_INT (data) - 100 * octave;
  char midinote = note+octave*12;

	if (g_collaboration_jamming->priv->jamming_state==JAMMINGSTATE_STARTED) {
		jammo_instrument_track_note_off_realtime (g_collaboration_jamming->priv->own_instrument_track, midinote);

	JammoSequencer * sequencer;
	g_object_get(g_collaboration_jamming, "sequencer", &sequencer, NULL);

		add_event_to_buffer(g_collaboration_jamming, JAMMINGMIDI_NOTE_OFF, midinote, jammo_sequencer_get_position(sequencer));
	}
}

// this function is called when sending or receiving fails
static void handle_communication_error(JammoCollaborationJamming * collaboration_jamming) {
	// when state is JAMMINGSTATE_ERROR midi is not sent or received anymore
	collaboration_jamming->priv->jamming_state=JAMMINGSTATE_ERROR;
	// if jamming should continue alone sequencer should not be stopped
	JammoSequencer * sequencer;
	g_object_get(collaboration_jamming, "sequencer", &sequencer, NULL);
	jammo_sequencer_stop(sequencer);
}

// callback function for sending midi events to peer
static int send_midi(gpointer data) {
	static guint64 clock_local;
	static char buffer[JAMMING_EVENTLEN+1];
	static int skipped, if_send;
	static JammoCollaborationJamming * collaboration_jamming;

	collaboration_jamming = (JammoCollaborationJamming *)data;

	if (collaboration_jamming->priv->sockfd<0) {
		handle_communication_error(collaboration_jamming);
		return 1;
	}

	// do nothing if synchronization is not done yet
	if (collaboration_jamming->priv->jamming_state==JAMMINGSTATE_INIT) {
		skipped=0;
		return 1;
	}

	// there has been an error in receiving midi data
	if (collaboration_jamming->priv->jamming_state==JAMMINGSTATE_ERROR) {
		return 0;
	}

	// time is needed in deciding whether to send anything yet or not
	// and also for timestamp
	JammoSequencer * sequencer;
	g_object_get(collaboration_jamming, "sequencer", &sequencer, NULL);
	clock_local = jammo_sequencer_get_position(sequencer);
	// events are sent as a constant stream if enough cpu time is given
	if (clock_local > collaboration_jamming->priv->time_of_last_sent_event+JAMMING_SEND_INTERVAL) {
		// send a new event from event buffer if there are any
		if (collaboration_jamming->priv->currentevent!=collaboration_jamming->priv->latestevent) {
			(collaboration_jamming->priv->currentevent)++;
			if (collaboration_jamming->priv->currentevent==JAMMING_EVENTBUFFERSIZE) {
				collaboration_jamming->priv->currentevent=0;
			}
			buffer[JAMMING_TYPE]=collaboration_jamming->priv->eventbuffer[collaboration_jamming->priv->currentevent].type;
			buffer[JAMMING_NOTE]=collaboration_jamming->priv->eventbuffer[collaboration_jamming->priv->currentevent].note;
			*(unsigned long long int*)&buffer[JAMMING_TIMESTAMP]=GUINT64_TO_BE(collaboration_jamming->priv->eventbuffer[collaboration_jamming->priv->currentevent].timestamp);
			if_send=1;
			skipped=0;
		}
		// no new events, send empty event which is used for monitoring the stream
		else {
			skipped++;
			if_send=0;
			buffer[JAMMING_TYPE]=JAMMINGMIDI_EMPTY;
			// type, note and octave are already 0 but timestamp needs to be updated
			// from host to network byte order
			clock_local=GUINT64_TO_BE(clock_local);
			// pack to buffer
			*(unsigned long long int*)&buffer[JAMMING_TIMESTAMP]=clock_local;
		}

		if (if_send || skipped>JAMMING_ALLOWSKIP) {
			// send
			if (send(collaboration_jamming->priv->sockfd, buffer, JAMMING_EVENTLEN, 0)==-1) {
				printf("sending midi event failed\n");
				handle_communication_error(collaboration_jamming);
				return 1;
			}
			(collaboration_jamming->priv->sent_events)++;
			if_send=0;
			memset(buffer, 0, JAMMING_EVENTLEN+1);
			buffer[JAMMING_TYPE]=JAMMINGMIDI_EMPTY;
			skipped=0;
		}
	}

	if (collaboration_jamming->priv->jamming_state==JAMMINGSTATE_FINISHED) {
		printf("finished sending\n");
		return 0;
	}

	return 1;
}

// callback function for receiving midi events from peer
static int receive_midi(gpointer data) {
	static char buffer[JAMMING_EVENTLEN+1];
	static struct timeval tv_select;
	static fd_set rfds;
	static int rcvbytes;
	static guint64 timestamp, clock_local;
	static int i, early_events, backing_track_fade_in_done;
	static gfloat fade_in;
	static JammoCollaborationJamming * collaboration_jamming;

	collaboration_jamming = (JammoCollaborationJamming *)data;

	if (collaboration_jamming->priv->sockfd<0) {
		handle_communication_error(collaboration_jamming);
	}

	// do nothing if synchronization is not done
	if (collaboration_jamming->priv->jamming_state==JAMMINGSTATE_INIT) {
		early_events=0;
		backing_track_fade_in_done=0;
		fade_in=0.0;
		return 1;
	}

	// clock is needed in checking if backing track has finished and fade in
	JammoSequencer * sequencer;
	g_object_get(collaboration_jamming, "sequencer", &sequencer, NULL);
	clock_local=jammo_sequencer_get_position(sequencer);

	if (collaboration_jamming->priv->jamming_state==JAMMINGSTATE_ERROR) {
		printf("error in jamming\n");
		jammo_sequencer_stop(JAMMO_SEQUENCER (sequencer));
		if (collaboration_jamming->priv->time_of_last_received_event > JAMMING_MINTIMEFORGENERATINGMIX) {
			// something was received
			g_timeout_add_full(G_PRIORITY_LOW,1000,(GSourceFunc)store_jamming_data, collaboration_jamming,NULL);
		}
		else {
			// short jamming, no need to save
			printf("too short jamming for saving\n");
			exit_jamming(collaboration_jamming);
		}
		return 0;
	}

	// check if backing track has finished and all events have been received
	// if so, end jamming
	if (clock_local > collaboration_jamming->priv->backing_track_duration + JAMMING_ENDTHRESHOLD && collaboration_jamming->priv->time_of_last_received_event >= collaboration_jamming->priv->backing_track_duration) {
		collaboration_jamming->priv->jamming_state=JAMMINGSTATE_FINISHED;
	}

	if (collaboration_jamming->priv->jamming_state==JAMMINGSTATE_FINISHED) {
		printf("finished receiving\n");
		//jammo_sequencer_stop(JAMMO_SEQUENCER (collaboration_jamming->priv->sequencer));
		g_timeout_add_full(G_PRIORITY_LOW,1000,(GSourceFunc)store_jamming_data, collaboration_jamming,NULL);
		return 0;
	}

	// backing track needs to fade in because failed synchronization causes a few notes to play
	// if synchronization needs to be done many times short playback can be quite annoying
	if (!backing_track_fade_in_done) {
		if (clock_local<JAMMING_FADE_IN_LENGTH) {
			fade_in=1.0-(float)(JAMMING_FADE_IN_LENGTH-clock_local)/JAMMING_FADE_IN_LENGTH;
		}
		else {
			fade_in=1.0;
			backing_track_fade_in_done=1;
		}
		JammoBackingTrack * btrack;
		g_object_get(collaboration_jamming, "backing_track", &btrack, NULL);
		jammo_playing_track_set_volume(JAMMO_PLAYING_TRACK(btrack), fade_in*JAMMING_BACKING_TRACK_VOLUME);
	}

	// timeout with select
	tv_select.tv_sec = 0;
	tv_select.tv_usec = 1000;
	FD_ZERO(&rfds);
	FD_SET(collaboration_jamming->priv->sockfd, &rfds);

	if (select((collaboration_jamming->priv->sockfd)+1, &rfds, NULL, NULL, &tv_select)==-1) {
		perror("select");
		handle_communication_error(collaboration_jamming);
		return 1;
	}

	// received something
	if (FD_ISSET(collaboration_jamming->priv->sockfd, &rfds)) {
		// read a full event at a time
		if (collaboration_jamming->priv->jamming_state==JAMMINGSTATE_STARTED) {
			if ((rcvbytes = recv(collaboration_jamming->priv->sockfd, buffer, JAMMING_EVENTLEN, 0)) == -1) {
					perror("recv");
					printf("receiving a midi event failed\n");
					handle_communication_error(collaboration_jamming);
					return 1;
			}
		}
		else {
			handle_communication_error(collaboration_jamming);
			return 1;
		}
		if (rcvbytes==0) {// peer has closed socket
			handle_communication_error(collaboration_jamming);		
			return 1;
		}

		timestamp=*(unsigned long long int*)&buffer[JAMMING_TIMESTAMP];

		// from network byte order to host
		timestamp = GUINT64_FROM_BE(timestamp);
		// current time
		clock_local = jammo_sequencer_get_position(sequencer);
		collaboration_jamming->priv->time_of_last_received_event=clock_local;
		collaboration_jamming->priv->allnotesoff=FALSE;

		// check if peer requested synchronization
		if (buffer[JAMMING_TYPE]==JAMMINGMIDI_RESYNC) {
			collaboration_jamming->priv->jamming_state=JAMMINGSTATE_INIT;
			printf("peer requested synchronizing again and starting from the beginning\n");
			jammo_sequencer_stop(sequencer);
			collaboration_jamming->priv->sent_events=0;
			collaboration_jamming->priv->time_of_last_received_event=0;
			collaboration_jamming->priv->time_of_last_sent_event=0;
			// delete already recorded events
			jammomidi_free_glist(&(collaboration_jamming->priv->peer_event_list));
			jammomidi_free_glist(&(collaboration_jamming->priv->own_event_list));
			g_timeout_add_full(G_PRIORITY_DEFAULT,100,(GSourceFunc)slavesynchronize, collaboration_jamming,NULL);	
		}

		// check if event is early
		if (timestamp - JAMMING_EARLYTHRESHOLD > clock_local) {
			early_events++;
			// too many early events on the first half of the song
			// synchronize and start from the beginning again
			if (early_events>JAMMING_SYNCTHRESHOLD && clock_local < collaboration_jamming->priv->backing_track_duration / JAMMING_LATESTSYNCPOINT) {
				collaboration_jamming->priv->jamming_state=JAMMINGSTATE_INIT;
				printf("too many early events. synchronizing again and starting from the beginning\n");
				jammo_sequencer_stop(sequencer);
				// inform peer of synchronization
				buffer[JAMMING_TYPE]=JAMMINGMIDI_RESYNC;
				if (send(collaboration_jamming->priv->sockfd, buffer, JAMMING_EVENTLEN, 0)==-1) {
					printf("sending resync message failed\n");
					handle_communication_error(collaboration_jamming);
					return 1;
				}
				collaboration_jamming->priv->sent_events=0;
				collaboration_jamming->priv->time_of_last_received_event=0;
				collaboration_jamming->priv->time_of_last_sent_event=0;
				// delete already recorded events
				jammomidi_free_glist(&(collaboration_jamming->priv->peer_event_list));
				jammomidi_free_glist(&(collaboration_jamming->priv->own_event_list));
				g_timeout_add_full(G_PRIORITY_DEFAULT,100,(GSourceFunc)mastersynchronize, collaboration_jamming,NULL);		
			}
			// seeking could be done here but it seems that it is not feasible
			// seeking either seeks too much or not at all which causes
			// jamming to seek all the time or a seeking loop on both devices
		}

		// buffer[JAMMING_TYPE]=on/off, buffer[JAMMING_NOTE]=notename
		if (buffer[JAMMING_TYPE]==JAMMINGMIDI_NOTE_ON) {
			// too late note on events are not passed forward
			// it is not necessary to check if received is the latest event for that note
			// because tcp handles ordering of data

			if (clock_local <= timestamp + JAMMING_LATETHRESHOLD) {
				jammo_instrument_track_note_on_realtime (collaboration_jamming->priv->peer_instrument_track,
					buffer[JAMMING_NOTE]);
			}
			else {
				printf("note-on received late and not passed to sampler\n");
				printf("diff: %" G_GUINT64_FORMAT ", threshold: %d\n", (clock_local-timestamp)/1000000,
					JAMMING_LATETHRESHOLD/1000000); 
			}
			// all note-on events are stored because jamming needs to be saved
			jammomidi_store_event_to_glist(&(collaboration_jamming->priv->peer_event_list), buffer[JAMMING_NOTE], buffer[JAMMING_TYPE], timestamp);
		}
		// note off events are always passed forward
		// it is not necessary to check if received is the latest event for that note
		// because tcp handles ordering of data
		else if (buffer[JAMMING_TYPE]==JAMMINGMIDI_NOTE_OFF) {
			jammo_instrument_track_note_off_realtime (collaboration_jamming->priv->peer_instrument_track,
				buffer[JAMMING_NOTE]);
			jammomidi_store_event_to_glist(&(collaboration_jamming->priv->peer_event_list), buffer[JAMMING_NOTE], buffer[JAMMING_TYPE], timestamp);
		}
		else {
			//printf("empty event received\n");
			;
		}
	}

	// received nothing
	else {
		clock_local = jammo_sequencer_get_position(sequencer);
		// stop receiving if too long delay 
		if (clock_local > 0 && clock_local - (collaboration_jamming->priv->time_of_last_received_event) >JAMMING_DELAYQUIT) {
			printf("delayquit reached\n");
			printf("clock: %" G_GUINT64_FORMAT ", last event:%" G_GUINT64_FORMAT "\n", clock_local, collaboration_jamming->priv->time_of_last_received_event);
			handle_communication_error(collaboration_jamming);
			return 1;
		}
		// turn all notes off if no events have been received in NOTEHANGDELAY ms
		// this is done to avoid hung (stuck) notes
		else if (clock_local- (collaboration_jamming->priv->time_of_last_received_event) >JAMMING_NOTEHANGDELAY && (!(collaboration_jamming->priv->allnotesoff))) {
			collaboration_jamming->priv->allnotesoff=TRUE;
			// turn all notes off
			for (i=0;i<JAMMOMIDI_NUMBER_OF_NOTES;i++) {
				jammo_instrument_track_note_off_realtime (collaboration_jamming->priv->peer_instrument_track, i);
			}
			printf("all notes turned off to avoid note hang\n");
		}
	}
	return 1;
}

void jammo_collaboration_jamming_on_sequencer_stopped_recording(JammoSequencer* sequencer, gpointer data) {

	JammoCollaborationJamming * collaboration_jamming = (JammoCollaborationJamming *)data;

	printf("sequencer stopped, state %d\n", collaboration_jamming->priv->jamming_state);

	if (collaboration_jamming->priv->jamming_state==JAMMINGSTATE_GENERATING_MIX) {
		collaboration_jamming->priv->jamming_state=JAMMINGSTATE_DONE_GENERATING;
	}
	else if (collaboration_jamming->priv->jamming_state==JAMMINGSTATE_INIT || collaboration_jamming->priv->jamming_state==JAMMINGSTATE_ERROR) {
		;
	}
	else {
		collaboration_jamming->priv->jamming_state=JAMMINGSTATE_FINISHED;
	}
}

int create_mix_of_jamming(gpointer data) {
	static JammoCollaborationJamming * collaboration_jamming;

	collaboration_jamming = (JammoCollaborationJamming *)data;

	if (collaboration_jamming->priv->jamming_state==JAMMINGSTATE_FINISHED || collaboration_jamming->priv->jamming_state==JAMMINGSTATE_ERROR) {

		// free objects before creating new ones
		clean_up(collaboration_jamming);

		// create a sequencer for file creation
		JammoSequencer * sequencer= jammo_sequencer_new ();
		g_object_set(collaboration_jamming, "sequencer", sequencer,NULL);
		g_signal_connect(sequencer, "stopped", G_CALLBACK(jammo_collaboration_jamming_on_sequencer_stopped_recording),collaboration_jamming);

		// backing track
		gchar * btrack_location;
		g_object_get(collaboration_jamming, "backing-track-location", &btrack_location, NULL);
		JammoBackingTrack * btrack = jammo_backing_track_new(btrack_location);
		g_object_set(collaboration_jamming, "backing-track", btrack, NULL);
		jammo_sequencer_add_track(sequencer, JAMMO_TRACK(btrack));
		jammo_playing_track_set_volume(JAMMO_PLAYING_TRACK(btrack), JAMMING_BACKING_TRACK_VOLUME);

		// create new tracks for file creation to eliminate all but local ui latencies
		collaboration_jamming->priv->own_instrument_track = jammo_instrument_track_new (collaboration_jamming->priv->own_instrument);
		jammo_sequencer_add_track (sequencer, JAMMO_TRACK (collaboration_jamming->priv->own_instrument_track));

		collaboration_jamming->priv->peer_instrument_track = jammo_instrument_track_new(collaboration_jamming->priv->peer_instrument);
		jammo_sequencer_add_track (sequencer, JAMMO_TRACK (collaboration_jamming->priv->peer_instrument_track));

		// add own events to new track and use stored timestamps
		jammo_instrument_track_set_event_list(collaboration_jamming->priv->own_instrument_track, collaboration_jamming->priv->own_event_list);

		// add received events to new track and use received timestamps
		jammo_instrument_track_set_event_list(collaboration_jamming->priv->peer_instrument_track, collaboration_jamming->priv->peer_event_list);

		// change sequencer to file mode
		gchar * mix_location;
		g_object_get(collaboration_jamming, "mix-location", &mix_location, NULL);
		if (jammo_sequencer_set_output_filename(sequencer, mix_location)) {
			printf("sequencer changed to file mode successfully\n");
		}

		// playback will now create a file
		collaboration_jamming->priv->jamming_state=JAMMINGSTATE_GENERATING_MIX;
		jammo_sequencer_play(sequencer);
	}

	if (collaboration_jamming->priv->jamming_state==JAMMINGSTATE_GENERATING_MIX) {
		JammoSequencer * sequencer;
		g_object_get(collaboration_jamming, "sequencer", &sequencer,NULL);
		printf("generating mix, position: %" G_GUINT64_FORMAT "\n", jammo_sequencer_get_position(sequencer));
		return 1;
	}
	else if (collaboration_jamming->priv->jamming_state==JAMMINGSTATE_DONE_GENERATING){
		// mix is generated and everything is now done
		printf("mix generated\n");
		exit_jamming(collaboration_jamming);
		return 0;
	}

	// in some other state, should not come here
	return 0;
}

int store_jamming_data(JammoCollaborationJamming * collaboration_jamming) {
	int i;
	// jamming done

	JammoSequencer * sequencer;
	g_object_get(collaboration_jamming, "sequencer", &sequencer, NULL);

	// just in case turn all peer notes off and put note-off events in list as well
	for (i=0;i<JAMMOMIDI_NUMBER_OF_NOTES;i++) {
		jammo_instrument_track_note_off_realtime (collaboration_jamming->priv->own_instrument_track, i);
		jammo_instrument_track_note_off_realtime (collaboration_jamming->priv->peer_instrument_track, i);
		// time is the ending time of jamming
		// it may end before the end of backing track
		jammomidi_store_event_to_glist(&(collaboration_jamming->priv->peer_event_list), i, JAMMINGMIDI_NOTE_OFF, (collaboration_jamming->priv->time_of_last_received_event)+1);
		jammomidi_store_event_to_glist(&(collaboration_jamming->priv->own_event_list), i, JAMMINGMIDI_NOTE_OFF, (collaboration_jamming->priv->time_of_last_sent_event)+1);
	}

	printf("finished jamming, saving jamming\n");
	printf("time of last event: %" G_GUINT64_FORMAT "\n", collaboration_jamming->priv->time_of_last_received_event);
	jammo_sequencer_stop(sequencer);

	// store own midi events to a file
	if (collaboration_jamming->priv->own_midi_location!=NULL) {
		jammomidi_glist_to_file(collaboration_jamming->priv->own_event_list, collaboration_jamming->priv->own_midi_location);
	}

	// store peer midi events
	if (collaboration_jamming->priv->peer_midi_location!=NULL) {
		jammomidi_glist_to_file(collaboration_jamming->priv->peer_event_list, collaboration_jamming->priv->peer_midi_location);
	}

	// create a mix of jamming
	gchar * mix_location;
	g_object_get(collaboration_jamming, "mix-location", &mix_location, NULL);
	if (mix_location!=NULL) {
		g_timeout_add_full(G_PRIORITY_LOW,100,(GSourceFunc)create_mix_of_jamming, collaboration_jamming,NULL);
	}
	return 0;
}

// function for exiting jamming
// this can be used to call main menu or something else
// for now it just quits main loop
void exit_jamming(JammoCollaborationJamming * collaboration_jamming) {
	// unref sequencer and tracks
	clean_up(collaboration_jamming);
	// free memory of event lists
	jammomidi_free_glist(&(collaboration_jamming->priv->peer_event_list));
	jammomidi_free_glist(&(collaboration_jamming->priv->own_event_list));
	clutter_main_quit();
}

int jammo_collaboration_jamming_start(JammoCollaborationJamming * collaboration_jamming) {

	ClutterActor *stage;
	// init variables needed in jamming
	collaboration_jamming->priv->currentevent=0;
	collaboration_jamming->priv->latestevent=0;
	collaboration_jamming->priv->sent_events=0;
	collaboration_jamming->priv->jamming_state=JAMMINGSTATE_INIT;
	collaboration_jamming->priv->time_of_last_received_event=0;
	collaboration_jamming->priv->time_of_last_sent_event=0;
	collaboration_jamming->priv->syncmaster=collaboration_jamming->priv->master;
	collaboration_jamming->priv->peer_event_list=NULL;
	collaboration_jamming->priv->own_event_list=NULL;
	collaboration_jamming->priv->backing_track_duration = 0;
	collaboration_jamming->priv->syncfails=0;

	if(collaboration_jamming->priv->sockfd<=0) {
		return 1;
	}

	// signal handler for catching broken pipe
	// a global pointer for state is needed
	gp_jamming_state=&(collaboration_jamming->priv->jamming_state);
	signal(SIGPIPE, signalhandler);

	// clutter callbacks use their data already for other purposes
	// they can use the following global
	g_collaboration_jamming=collaboration_jamming;

	// init graphics
  stage = clutter_stage_get_default ();
  //clutter_stage_set_fullscreen(CLUTTER_STAGE(stage), TRUE);
  clutter_actor_set_size (stage, 800.0, 480.0);

	JammoSequencer * sequencer = jammo_sequencer_new ();
	g_object_set(collaboration_jamming, "sequencer", sequencer, NULL);
	// when stopped for some reason we want to store the jamming
	g_signal_connect(sequencer, "stopped", G_CALLBACK(jammo_collaboration_jamming_on_sequencer_stopped_recording),collaboration_jamming);

	// own instrument
  collaboration_jamming->priv->own_instrument_track = jammo_instrument_track_new (collaboration_jamming->priv->own_instrument);
  jammo_instrument_track_set_realtime (collaboration_jamming->priv->own_instrument_track, TRUE);
  jammo_sequencer_add_track (sequencer, JAMMO_TRACK (collaboration_jamming->priv->own_instrument_track));

  clutter_container_add (CLUTTER_CONTAINER (stage),
			 make_instrument_grid (collaboration_jamming->priv->own_instrument), NULL);

	// peer instrument
  collaboration_jamming->priv->peer_instrument_track = jammo_instrument_track_new (collaboration_jamming->priv->peer_instrument);
  jammo_instrument_track_set_realtime (collaboration_jamming->priv->peer_instrument_track, TRUE);
  jammo_sequencer_add_track (sequencer, JAMMO_TRACK (collaboration_jamming->priv->peer_instrument_track));

	// backing track
	gchar * btrack_location;
	g_object_get(collaboration_jamming, "backing-track-location", &btrack_location,NULL);
	JammoBackingTrack * btrack = jammo_backing_track_new(btrack_location);
	g_object_set(collaboration_jamming, "backing-track", btrack, NULL);
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(btrack));
	// sequencer returns 0 if duration is asked so a duration notification is needed
	g_signal_connect(btrack, "notify::duration", G_CALLBACK(on_duration_notify), collaboration_jamming);

	// callbacks for receiving and sending midi
	g_timeout_add_full(G_PRIORITY_HIGH_IDLE,4,(GSourceFunc)receive_midi,collaboration_jamming,NULL);
	g_timeout_add_full(G_PRIORITY_HIGH_IDLE,4,(GSourceFunc)send_midi,collaboration_jamming,NULL);

	// synchronization
	if (collaboration_jamming->priv->syncmaster)
		g_timeout_add_full(G_PRIORITY_DEFAULT,100,(GSourceFunc)mastersynchronize,collaboration_jamming,NULL);		
	else
		g_timeout_add_full(G_PRIORITY_DEFAULT,100,(GSourceFunc)slavesynchronize,collaboration_jamming,NULL);

	// start
	printf("starting jamming\n");
  clutter_actor_show_all (stage);
  clutter_main ();

	printf("jamming finished\n");

  return 0;
}

// unrefs sequencer and tracks which should cause deletion of those objects
static void clean_up(JammoCollaborationJamming * collaboration_jamming) {
	JammoSequencer * sequencer;
	g_object_get(collaboration_jamming,"sequencer",&sequencer, NULL);

	// unref sequencer. this unrefs the tracks also and should delete all objects
	g_object_unref(sequencer);
	g_object_set(G_OBJECT(collaboration_jamming),"sequencer",NULL, NULL);
	collaboration_jamming->priv->own_instrument_track=NULL;
	collaboration_jamming->priv->peer_instrument_track=NULL;
}


