/*
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */


#include <meam/jammo-meam.h>
#include <meam/jammo-editing-track.h>
#include <meam/jammo-sample.h>
#include <chum/jammo-chum.h>
#include <tangle.h>

#include <tangle.h>
#include <glib-object.h>
#include <ctype.h>
#include <string.h>
#include <clutter/clutter.h>


static JammoEditingTrack* static_editing_track;
static JammoSample* static_sample;
static JammoSequencer* static_sequencer;

static void pressed (ClutterActor * actor, ClutterEvent * event, gpointer data) {
	JammoSample* new_sample = jammo_sample_new_from_existing(static_sample);
	jammo_editing_track_add_sample(static_editing_track, new_sample, jammo_sequencer_get_position(static_sequencer));
}


static void play_pressed (ClutterActor * actor, ClutterEvent * event, gpointer data) {
	jammo_sequencer_stop(static_sequencer);
	jammo_sequencer_play(static_sequencer);
}




static ClutterColor dark_color = { 25, 25, 12, 255 };
int main (int argc, char **argv) {

  ClutterActor *stage;

  jammo_meam_init (&argc, &argv, "jammo_test_duration_cache");
  jammo_chum_init (&argc, &argv);


  stage = clutter_stage_get_default ();
  //clutter_stage_set_fullscreen(CLUTTER_STAGE(stage), TRUE);
  clutter_actor_set_size (stage, 800.0, 480.0);


  static_sequencer = jammo_sequencer_new ();
  static_editing_track = jammo_editing_track_new_fixed_duration (68010648440); //68s.
  jammo_sequencer_add_track (static_sequencer, JAMMO_TRACK (static_editing_track));

  static_sample= jammo_sample_new_from_file_with_sequencer("/opt/jammo/themes/animal/110/animalw_110_vibraslap_2.wav", static_sequencer);

  ClutterActor *box = clutter_rectangle_new_with_color (&dark_color);

  clutter_actor_set_size (box, 66, 60);
  clutter_actor_set_position (box, 66, 60);
  clutter_actor_show (box);
  clutter_container_add_actor (CLUTTER_CONTAINER (stage), box);
  clutter_actor_set_reactive (box, TRUE);
  g_signal_connect (box, "button-press-event",G_CALLBACK (pressed),NULL);



ClutterActor *play = clutter_rectangle_new_with_color (&dark_color);

clutter_actor_set_size (play, 66, 60);
clutter_actor_set_position (play, 500, 60);
clutter_actor_show (play);
clutter_container_add_actor (CLUTTER_CONTAINER (stage), play);
clutter_actor_set_reactive (play, TRUE);
g_signal_connect (play, "button-press-event",G_CALLBACK (play_pressed),NULL);


  jammo_sequencer_play (JAMMO_SEQUENCER (static_sequencer));
  clutter_actor_show_all (stage);
  clutter_main ();

  return 0;
}
