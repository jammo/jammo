/*
 * test-jamming
 *
 * This file is part of JamMo.
 *
 * This is for testing real-time pair jamming.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen, Mikko Gynther
 */

#include "../../src/chum/jammo-collaboration-jamming.h"
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/socket.h>

#include "../../src/meam/jammo-meam.h"
#include "../../src/chum/jammo-chum.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int new_socket (int *sockfd, char * server , char * port_number, int bind_y, struct addrinfo ** p);

int main (int argc, char **argv) {
	struct addrinfo * p;
	size_t addr_len;
	struct sockaddr their_addr;
	int master=0;
	int listen_socket;
	int socket_local;
	int own_instrument=0, peer_instrument=0;
  int tcpnodelay_flag = 1;

	memset(&their_addr, 0, sizeof(struct sockaddr));
	addr_len = sizeof(struct sockaddr);

  jammo_meam_init (&argc, &argv, "jammo_test_duration_cache");
  jammo_chum_init (&argc, &argv);

	if (argc<2){
		printf("usage: %s master <port> or\n", argv[0]);
		printf("%s slave <host> <port>\n", argv[0]);
		exit(1);
	}
	if (strcmp(argv[1], "master")==0) {
		master=1;
		if (new_socket(&listen_socket, NULL, argv[2], TRUE, &p)!=0) {
			exit(1);
		}

		if (listen(listen_socket, 10) == -1) {
			perror("listen");
			exit(1);
		}
	  socket_local = accept(listen_socket, (struct sockaddr *)&their_addr, (socklen_t* )&addr_len); //Casting socklen_t added by Aapo
		close(listen_socket);
		printf("got connection\n");
	}
	else if (strcmp(argv[1], "slave")==0) {
		master=0;
		if (new_socket(&socket_local, argv[2], argv[3], FALSE, &p)!=0) {
			exit(1);
		}

		if (connect(socket_local, p->ai_addr, p->ai_addrlen) == -1) {
			close(socket_local);
		  perror("connect");
		  exit(1);
		}
		printf("connected\n");
	}
	else {
		printf("usage: %s master <port> or\n", argv[0]);
		printf("%s slave <host> <port>\n", argv[0]);
		exit(1);
	}

	if (master) {
  	own_instrument = 0;
		peer_instrument = 2;
	}
	else {
		own_instrument = 2;
		peer_instrument = 0;

	}

	// turn nagle of so data will get sent instantly
  if(setsockopt(socket_local, IPPROTO_TCP, TCP_NODELAY, (char *) &tcpnodelay_flag, sizeof(int)) == -1) {
      perror("setsockopt (TCP_NODELAY)");
      exit(1);
  }
	printf("naggle off\n");

	JammoCollaborationJamming* collaboration_jamming = jammo_collaboration_jamming_new();
	jammo_collaboration_jamming_set_socket(collaboration_jamming,socket_local);
	jammo_collaboration_jamming_set_master(collaboration_jamming,master);
	jammo_collaboration_jamming_set_own_instrument(collaboration_jamming,own_instrument);
	jammo_collaboration_jamming_set_peer_instrument(collaboration_jamming,peer_instrument);

	// set backing track location
	char * btrack_location = "/opt/jammo/songs/easy/boat/comping.ogg";
	g_object_set(G_OBJECT(collaboration_jamming),"backing-track-location",btrack_location, NULL);

	// set mix location
	char * mix_location = (master==1?"jamming_out_m.ogg":"jamming_out_s.ogg");
	g_object_set(G_OBJECT(collaboration_jamming),"mix-location",mix_location, NULL);

	jammo_collaboration_jamming_set_own_midi_location(collaboration_jamming, master==1?"ownmidi_m.txt":"ownmidi_s.txt");
	jammo_collaboration_jamming_set_peer_midi_location(collaboration_jamming, master==1?"peermidi_m.txt":"peermidi_s.txt");

	printf("parameters done\n");
	jammo_collaboration_jamming_start(collaboration_jamming);

	jammo_collaboration_game_teacher_exit(JAMMO_COLLABORATION_GAME(collaboration_jamming));
	jammo_collaboration_game_create_song_file(JAMMO_COLLABORATION_GAME(collaboration_jamming));

	g_object_unref(collaboration_jamming);
	if (master) {
		sleep(2);
	}

	close(socket_local);

	return 0;
}

// this test program uses normal tcp sockets and creates them with this function
int new_socket (int *sockfd, char * server , char * port_number, int bind_y, struct addrinfo ** p) {
	int rv;
	struct addrinfo hints, *servinfo;

	/* address stuff, creating a socket, and binding are from beej's guide
	to network programming http://beej.us/guide/bgnet/  */
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET; // IP_v4 only
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	/* getaddrinfo informs user if server is not found or some other error occurs */
	if ((rv = getaddrinfo(server, port_number, &hints, &servinfo)) != 0) {
			fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
			return 1;
	}

	// loop through all the results and make a socket
	for(*p = servinfo; *p != NULL; *p = (*p)->ai_next) {
		if ((*sockfd = socket((*p)->ai_family, (*p)->ai_socktype, (*p)->ai_protocol)) == -1) {
				  perror("socket");
					continue;
			}
		if (bind_y==TRUE) {
			if (bind(*sockfd, (*p)->ai_addr, (*p)->ai_addrlen) == -1) {
			    close(*sockfd);
			    perror("bind");
			    continue;
			}
		}

			break;
	}

	if ((*p) == NULL) {
			fprintf(stderr, "failed to bind socket\n");
			return 1;
	}

	freeaddrinfo(servinfo);
	return 0;
}
