/*
 * test-slider3
 *
 * This file is part of JamMo.
 *
 * This is for testing slider virtual instrument playing, storing events in sampler, get and set event lists and playback from sampler.
 *
 * (c) 2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Aapo Rantalainen, Mikko Gynther
 */

#include <glib-object.h>
#include <tangle.h>
#include <meam/jammo-meam.h>
#include <meam/jammo-slider-track.h>
#include <chum/jammo-chum.h>

#include <meam/jammo-backing-track.h>
#include <math.h>


static JammoSliderTrack *slider_track;

// range in octaves
#define RANGE 2.0
#define OCTAVE_ON_SCREEN (799.0/RANGE)
#define BASE_FREQUENCY 130.81

static void update_marker(JammoSliderTrack* track, gfloat frequency, gpointer marker) {
		printf("frequency %f\n",frequency);
		gfloat x = OCTAVE_ON_SCREEN * log2(frequency/BASE_FREQUENCY);
		printf("x %f\n",x);
		clutter_actor_set_x(CLUTTER_ACTOR(marker),x);
}

static gboolean on_play_button_clicked(TangleButton* button, gpointer marker) {
	printf("starting play!\n");

	//Make AD-HOC sequencer and track to listen realtime track.
	GList* list= jammo_slider_track_get_event_list(JAMMO_SLIDER_TRACK(slider_track));
	jammo_slider_event_glist_to_file(list, "slider_events.txt");

	GList * list2 = jammo_slider_event_file_to_glist("slider_events.txt");	
	JammoSequencer* seq= jammo_sequencer_new();
	JammoSliderTrack*  playback_slider_track = jammo_slider_track_new (JAMMO_SLIDER_TYPE_KARPLUS);

  jammo_slider_track_set_recording (playback_slider_track, FALSE);
  jammo_slider_track_set_live (playback_slider_track, FALSE);
  jammo_sequencer_add_track (seq, JAMMO_TRACK (playback_slider_track));
	
	jammo_slider_track_set_event_list(playback_slider_track, list2);

	g_signal_connect (playback_slider_track, "report-current-frequency", G_CALLBACK (update_marker),marker);
	jammo_sequencer_play(JAMMO_SEQUENCER(seq));

	
	return FALSE;
}

static gboolean on_clear_button_clicked(TangleButton* button, gpointer user_data) {
	printf("clears events of track\n");
	jammo_slider_track_clear_events(slider_track);
	return FALSE;
}


static void slider_motion(ClutterActor *actor, ClutterEvent *event, gpointer data){
gfloat x_release, y_release;
gfloat freq;
clutter_event_get_coords (event, &x_release, &y_release);
//printf("move: X=%d\n",(gint)x_release);
freq = BASE_FREQUENCY*pow(2.0, x_release/OCTAVE_ON_SCREEN);
jammo_slider_track_set_frequency_realtime(slider_track, freq);
}


static void slider_press(ClutterActor *actor, ClutterEvent *event, gpointer data){
gfloat x_release, y_release;
gfloat freq;
clutter_event_get_coords (event, &x_release, &y_release);
//printf("Press: X=%d\n",(gint)x_release);
freq = BASE_FREQUENCY*pow(2.0, x_release/OCTAVE_ON_SCREEN);
jammo_slider_track_set_on_realtime(slider_track, freq);
}


static void slider_release(ClutterActor *actor, ClutterEvent *event, gpointer data){
gfloat x_release, y_release;
gfloat freq;
clutter_event_get_coords (event, &x_release, &y_release);
//printf("Release: X=%d\n",(gint)x_release);
freq = BASE_FREQUENCY*pow(2.0, x_release/OCTAVE_ON_SCREEN);
jammo_slider_track_set_off_realtime(slider_track, freq);
}


static ClutterActor* make_slider ()
{
  ClutterColor b_color = { 0, 255, 255, 255 };
  ClutterActor *box = clutter_rectangle_new_with_color (&b_color);
  clutter_actor_set_size (box, 800, 480);
  clutter_actor_show (box);

  clutter_actor_set_reactive (box, TRUE);
  g_signal_connect (box, "button-press-event", G_CALLBACK (slider_press),NULL);      
  g_signal_connect (box, "button-release-event", G_CALLBACK (slider_release),NULL);
  g_signal_connect (box, "motion-event", G_CALLBACK (slider_motion),NULL);

  return box;
}


int main (int argc, char **argv)
{
  ClutterActor *stage;

  jammo_meam_init (&argc, &argv, "jammo_test_duration_cache");
  jammo_chum_init (&argc, &argv);


  stage = clutter_stage_get_default ();
  //clutter_stage_set_fullscreen(CLUTTER_STAGE(stage), TRUE);
  clutter_actor_set_size (stage, 800.0, 480.0);


  JammoSequencer *sequencer = jammo_sequencer_new ();

  //Static
  slider_track = jammo_slider_track_new (JAMMO_SLIDER_TYPE_KARPLUS);
  jammo_slider_track_set_recording(slider_track, TRUE);
  jammo_sequencer_add_track (sequencer, JAMMO_TRACK (slider_track));

  clutter_container_add (CLUTTER_CONTAINER (stage), make_slider(), NULL);

	//Marker
	ClutterActor* marker = tangle_texture_new("/opt/jammo/slider_marker.png");
	clutter_actor_set_y(marker,150);
	clutter_container_add (CLUTTER_CONTAINER (stage), marker, NULL);
	clutter_actor_set_anchor_point(marker,clutter_actor_get_width(marker)/2,0.0);
	g_signal_connect (slider_track, "report-current-frequency", G_CALLBACK (update_marker),marker);


	//Grid
	ClutterActor* grid;
	int number_of_slots = 48;
	grid = tangle_grid_new(800.0 /number_of_slots, 480.0);
	clutter_actor_set_size(grid, 800.0, 480.0);
	clutter_container_add (CLUTTER_CONTAINER (stage), grid, NULL);

	//Play-button for starting sequencer
	ClutterActor* play_button = tangle_button_new();
	clutter_actor_set_position(play_button, 100.0, 0.0);
	tangle_widget_add_after(TANGLE_WIDGET(play_button), tangle_texture_new("/opt/jammo/sequencer/button_play.png"), NULL, NULL, NULL);
	g_signal_connect(play_button, "clicked", G_CALLBACK(on_play_button_clicked), marker);
	clutter_container_add(CLUTTER_CONTAINER(stage), play_button, NULL);

	//button for clearing events
	ClutterActor* clear_button = tangle_button_new();
	clutter_actor_set_position(clear_button, 300.0, 0.0);
	tangle_widget_add_after(TANGLE_WIDGET(clear_button), tangle_texture_new("/opt/jammo/cross_icon.png"), NULL, NULL, NULL);
	g_signal_connect(clear_button, "clicked", G_CALLBACK(on_clear_button_clicked), sequencer);
	clutter_container_add(CLUTTER_CONTAINER(stage), clear_button, NULL);


	clutter_actor_raise_top(marker);
  jammo_sequencer_play (JAMMO_SEQUENCER (sequencer));
  clutter_actor_show_all (stage);
  clutter_main ();


  return 0;
}
