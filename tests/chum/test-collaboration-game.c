/*
 * test-collaboration-game.c
 *
 * This file is part of JamMo.
 *
 * This is for testing collaboration game class.
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther
 */

// group composition is used because game does not have a function for creation
// it could be created differently but it is not supposed do
#include "../../src/chum/jammo-collaboration-group-composition.h"

#include "../../src/chum/jammo-editing-track-view.h"
#include "../../src/chum/jammo-miditrack-view.h"

#include "../../src/meam/jammo-backing-track.h"

#include "../../src/meam/jammo-meam.h"
#include "../../src/chum/jammo-chum.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main (int argc, char **argv) {

  jammo_meam_init (&argc, &argv, "jammo_test_duration_cache");
  jammo_chum_init (&argc, &argv);

	JammoCollaborationGroupComposition* collaboration_group_composition = jammo_collaboration_group_composition_new();

// ******** Testing JammoCollaborationGame functionality ********************
	char* mix_location = "test_location.ogg";
	printf("setting mix location to %s\n", mix_location);
	g_object_set(G_OBJECT(collaboration_group_composition),"mix-location",mix_location, NULL);

	char * mix_location2;
	g_object_get(collaboration_group_composition,"mix-location",&mix_location2, NULL);
	printf("got location %s\n", mix_location2);

	// set and get backing track location
	char * btrack_location = "/opt/jammo/songs/easy/boat/comping.ogg";
	g_object_set(G_OBJECT(collaboration_group_composition),"backing-track-location",btrack_location, NULL);
	char * btrack2;
	g_object_get(collaboration_group_composition,"backing-track-location",&btrack2, NULL);
	if (strcmp(btrack_location,btrack2)==0) {
		printf("backing track location set and get successfully\n");
	}
	else {
		printf("problem with backing track location set and get\n");
	}

	// set and get song file out location
	char * song_file_out_location = "testlocation.txt";
	g_object_set(G_OBJECT(collaboration_group_composition),"song-file-out-location",song_file_out_location, NULL);
	char * song_file_out_location2;
	g_object_get(collaboration_group_composition,"song-file-out-location",&song_file_out_location2, NULL);
	if (strcmp(song_file_out_location,song_file_out_location2)==0) {
		printf("song file out location set and get successfully\n");
	}
	else {
		printf("problem with song file out location set and get\n");
	}
	
	// set and get theme
	char * theme = "test_theme";
	g_object_set(G_OBJECT(collaboration_group_composition),"theme",theme, NULL);
	char * theme2;
	g_object_get(collaboration_group_composition,"theme",&theme2, NULL);
	if (strcmp(theme,theme2)==0) {
		printf("theme set and get successfully\n");
	}
	else {
		printf("problem with theme set and get\n");
	}
	
	// set and get variation
	int variation = 8;
	g_object_set(G_OBJECT(collaboration_group_composition),"variation",variation, NULL);
	int variation2;
	g_object_get(collaboration_group_composition,"variation",&variation2, NULL);
	if (variation==variation2) {
		printf("variation set and get successfully\n");
	}
	else {
		printf("problem with variation set and get\n");
	}
	
	// insert views to game
	printf("adding two views to game\n");
	JammoEditingTrack* track = jammo_editing_track_new();
	ClutterActor * view = jammo_editing_track_view_new(track, 20, 1000000000, 100.0, 30.0);
	guint id1 = 12345;
	jammo_collaboration_game_add_view(JAMMO_COLLABORATION_GAME(collaboration_group_composition), id1, view);
	
	JammoInstrumentTrack * miditrack = jammo_instrument_track_new(JAMMO_INSTRUMENT_TYPE_FLUTE);
	ClutterActor* view2 = jammo_miditrack_view_new(miditrack, 20, 1000000000, 100.0, 30.0, 80, 50);
	guint id2 = 54321;
	jammo_collaboration_game_add_view(JAMMO_COLLABORATION_GAME(collaboration_group_composition), id2, view2);
	
	jammo_collaboration_game_dump_views(JAMMO_COLLABORATION_GAME(collaboration_group_composition));
	printf("removing one view from game\n");
	jammo_collaboration_game_remove_view(JAMMO_COLLABORATION_GAME(collaboration_group_composition), id1);
	jammo_collaboration_game_dump_views(JAMMO_COLLABORATION_GAME(collaboration_group_composition));

	printf("calling jammo_collaboration_game_teacher_exit\n");
	jammo_collaboration_game_teacher_exit(JAMMO_COLLABORATION_GAME(collaboration_group_composition));
	jammo_collaboration_game_create_song_file(JAMMO_COLLABORATION_GAME(collaboration_group_composition));

	g_object_unref(collaboration_group_composition);

	printf("everything done\n");

	return 0;
}

