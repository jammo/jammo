/*
 * jammo-collaboration-chain-composition.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther <mikko.gynther@lut.fi>
 */

#include "jammo-collaboration-game.h"
#include "jammo-collaboration-chain-composition.h"

G_DEFINE_TYPE(JammoCollaborationChainComposition, jammo_collaboration_chain_composition, JAMMO_TYPE_COLLABORATION_GAME);

#include "../meam/jammo-sequencer.h"
#include "../meam/jammo-backing-track.h"

enum {
	PROP_0/*,
	PROP_ADD_PROPERTIES_HERE*/
};

struct _JammoCollaborationChainCompositionPrivate {
	// these have to be filled before calling chain composition start function

	// following are used automatically and do not need to be filled
	// garbage is here because struct size better not be 0
	int garbage;

};


JammoCollaborationChainComposition* jammo_collaboration_chain_composition_new() {
	return JAMMO_COLLABORATION_CHAIN_COMPOSITION(g_object_new(JAMMO_TYPE_COLLABORATION_CHAIN_COMPOSITION, NULL));
}


static void jammo_collaboration_chain_composition_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoCollaborationChainComposition * collaboration_chain_composition;
	
	collaboration_chain_composition = JAMMO_COLLABORATION_CHAIN_COMPOSITION(object);

	
	switch (prop_id) {
		/*case PROP_BACKING_TRACK_FILENAME:
			g_assert(!collaboration_composition->priv->filename);
			collaboration_composition->priv->filename = g_strdup(g_value_get_string(value));
			break;*/
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_collaboration_chain_composition_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
	JammoCollaborationChainComposition * collaboration_chain_composition;
	
	collaboration_chain_composition = JAMMO_COLLABORATION_CHAIN_COMPOSITION(object);

	switch (prop_id) {
		/*case PROP_FILENAME:
			g_value_set_string(value, backing_track->priv->filename);
			break;*/
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

// implementations for base class functions

// implementation for teacher exit
void jammo_collaboration_chain_composition_teacher_exit(JammoCollaborationGame * collaboration_game) {
	JammoCollaborationChainComposition * chain_composition;
	chain_composition = JAMMO_COLLABORATION_CHAIN_COMPOSITION(collaboration_game);
	// TODO implement teacher exit
	printf("jammo_collaboration_chain_composition_teacher_exit called\n");
}

// create_song_file
// return values 0 success, 1 can not open file
int jammo_collaboration_chain_composition_create_song_file(JammoCollaborationGame * collaboration_game) {
	JammoCollaborationChainComposition * chain_composition;
	chain_composition = JAMMO_COLLABORATION_CHAIN_COMPOSITION(collaboration_game);
	// TODO implement
	printf("jammo_collaboration_chain_composition_create_song_file called\n");

	return 0;
}

static GObject* jammo_collaboration_chain_composition_constructor(GType type, guint n_properties, GObjectConstructParam* properties) {
	GObject* object;
	JammoCollaborationChainComposition * collaboration_chain_composition;
	
	object = G_OBJECT_CLASS(jammo_collaboration_chain_composition_parent_class)->constructor(type, n_properties, properties);

	collaboration_chain_composition = JAMMO_COLLABORATION_CHAIN_COMPOSITION(object);

	return object;
}


static void jammo_collaboration_chain_composition_finalize(GObject* object) {
	JammoCollaborationChainComposition* collaboration_chain_composition;
	
	collaboration_chain_composition = JAMMO_COLLABORATION_CHAIN_COMPOSITION(object);

	G_OBJECT_CLASS(jammo_collaboration_chain_composition_parent_class)->finalize(object);
}

static void jammo_collaboration_chain_composition_dispose(GObject* object) {
	G_OBJECT_CLASS(jammo_collaboration_chain_composition_parent_class)->dispose(object);
}

static void jammo_collaboration_chain_composition_class_init(JammoCollaborationChainCompositionClass* collaboration_chain_composition_class) {
	printf("class init\n");
	GObjectClass* gobject_class = G_OBJECT_CLASS(collaboration_chain_composition_class);
	JammoCollaborationGameClass* collaboration_game_class = JAMMO_COLLABORATION_GAME_CLASS(collaboration_chain_composition_class);

	gobject_class->constructor = jammo_collaboration_chain_composition_constructor;
	gobject_class->finalize = jammo_collaboration_chain_composition_finalize;
	gobject_class->dispose = jammo_collaboration_chain_composition_dispose;
	gobject_class->set_property = jammo_collaboration_chain_composition_set_property;
	gobject_class->get_property = jammo_collaboration_chain_composition_get_property;

	collaboration_game_class->teacher_exit = jammo_collaboration_chain_composition_teacher_exit;
	collaboration_game_class->create_song_file = jammo_collaboration_chain_composition_create_song_file;

	g_type_class_add_private(gobject_class, sizeof(JammoCollaborationChainCompositionPrivate));
}

static void jammo_collaboration_chain_composition_init(JammoCollaborationChainComposition* collaboration_chain_composition) {
	collaboration_chain_composition->priv = G_TYPE_INSTANCE_GET_PRIVATE(collaboration_chain_composition, JAMMO_TYPE_COLLABORATION_CHAIN_COMPOSITION, JammoCollaborationChainCompositionPrivate);
}

// get and set functions

// function prototypes
void jammo_collaboration_chain_composition_on_sequencer_stopped_recording(JammoSequencer* sequencer, gpointer data);
int create_mix_of_chain_composition(gpointer data);
void exit_chain_composition(JammoCollaborationChainComposition * collaboration_chain_composition);
static void clean_up(JammoCollaborationChainComposition * collaboration_chain_composition);

// functions needed in chain composition

void jammo_collaboration_chain_composition_on_sequencer_stopped_recording(JammoSequencer* sequencer, gpointer data) {

	// uncomment to use object
	/*JammoCollaborationChainComposition * collaboration_chain_composition = (JammoCollaborationChainComposition *)data;*/

	// uncomment adding callback to start generating mix of chain composition
	/*g_timeout_add_full(G_PRIORITY_DEFAULT,100,(GSourceFunc)create_mix_of_chain_composition,collaboration_chain_game,NULL);*/
	
}

int create_mix_of_chain_composition(gpointer data) {
	static JammoCollaborationChainComposition * collaboration_chain_composition;

	collaboration_chain_composition = (JammoCollaborationChainComposition *)data;

	// TODO check states and do not change mode multiple times
	if (0) {
		JammoSequencer * sequencer;
		g_object_get(collaboration_chain_composition,"sequencer",&sequencer, NULL);
		char * mix_location;
		g_object_get(collaboration_chain_composition,"mix-location",&mix_location, NULL);		

		// change sequencer to file mode
		if (jammo_sequencer_set_output_filename(sequencer, mix_location)==0) {
			printf("sequencer changed to file mode successfully\n");
		}

		// playback will now create a file
		jammo_sequencer_play(sequencer);
	}

	if (1 /* stopped */){

		exit_chain_composition(collaboration_chain_composition);
		return 0;
	}

	// still generating mix file
	return 1;
}

// function for exiting chain_composition
// this can be used to call main menu or something else
void exit_chain_composition(JammoCollaborationChainComposition * collaboration_chain_composition) {
	// unref sequencer and tracks
	clean_up(collaboration_chain_composition);
}

int jammo_collaboration_chain_composition_start(JammoCollaborationChainComposition * collaboration_chain_composition) {

	// this is how to add callback functions to main loop
	/*g_timeout_add_full(G_PRIORITY_DEFAULT,100,(GSourceFunc)create_mix_of_chain_composition,collaboration_chain_game,NULL);*/

  return 0;
}

// unrefs sequencer and tracks which should cause deletion of those objects
static void clean_up(JammoCollaborationChainComposition * collaboration_chain_composition) {
	JammoSequencer * sequencer;
	g_object_get(collaboration_chain_composition,"sequencer",&sequencer, NULL);

	// unref sequencer. this unrefs the tracks also and should delete all objects
	g_object_unref(sequencer);
	g_object_set(G_OBJECT(collaboration_chain_composition),"sequencer",NULL, NULL);
}


