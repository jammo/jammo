/*
* test-fullsequencer-area.c
*
* This file is part of JamMo.
*
* Makes six editing track.
* One JammoSampleButton, pause, stop, play.
*
* (c) 2010 University of Oulu
*
* Authors: Aapo Rantalainen
*/


#include <meam/jammo-meam.h>
#include <meam/jammo-instrument-track.h>
#include <meam/jammo-midi.h>
#include <chum/jammo-chum.h>
#include <tangle.h>

#include <tangle.h>
#include <glib-object.h>
#include <ctype.h>
#include <string.h>
#include <clutter/clutter.h>

#include "chum/jammo-sample-button.h"
#include "chum/7-12gui/sequencer_track_view.h"

//These script-handlings are copied from chum/jammo.c
static ClutterScript* script;
ClutterActor* jammo_get_actor_by_id(const char* id) {
	ClutterActor* actor = NULL;

	if (!(actor = CLUTTER_ACTOR(clutter_script_get_object(script, id)))) {
		g_warning("Actor '%s' not found.", id);
	}
	return actor;
}

static void add_search_paths(ClutterScript* script) {
	const gchar* default_search_path = "/opt/jammo";
	gchar* development_search_path;
	gchar* cwd;
	const gchar* search_paths[2];

	cwd = g_get_current_dir();
	development_search_path = g_build_filename(cwd, "data", "jammo", NULL);

	search_paths[0] = development_search_path;
	search_paths[1] = default_search_path;
	clutter_script_add_search_paths(script, search_paths, G_N_ELEMENTS(search_paths));

	g_free(cwd);
	g_free(development_search_path);
}

///////////////////////////////////////////////////////

static gboolean on_play_button_clicked(TangleButton* button, gpointer user_data) {
	jammo_sequencer_play(JAMMO_SEQUENCER(user_data));
	return FALSE;
}

static gboolean on_stop_button_clicked(TangleButton* button, gpointer user_data) {
	jammo_sequencer_stop(JAMMO_SEQUENCER(user_data));
	return FALSE;
}

static gboolean on_pause_button_clicked(TangleButton* button, gpointer user_data) {
	//code for testing seeking
	guint64 pos = jammo_sequencer_get_position(JAMMO_SEQUENCER(user_data));
	jammo_sequencer_set_position(JAMMO_SEQUENCER(user_data), pos);
	

	/* Do not set position and pause at the same time!
	jammo_sequencer_pause(JAMMO_SEQUENCER(user_data)); */
	return FALSE;
}
////////////////////////////////////////////////////////

static void on_sequencer_stopped(JammoSequencer* sequencer, gpointer play_button) {
	printf("on_sequencer_stopped\n");
}

static void on_duration_notify(GObject* object, GParamSpec* pspec, gpointer user_data) {
	ClutterActor *stage;
	ClutterActor* sample_button;
	JammoSequencer* sequencer;

	stage = clutter_stage_get_default ();
	#ifdef N900
	clutter_stage_set_fullscreen(CLUTTER_STAGE(stage), TRUE);
	#else
	clutter_actor_set_size(stage, 800.0, 480.0);
	#endif


	JammoSample* sample;
	sample = JAMMO_SAMPLE(object);

	sequencer = JAMMO_SEQUENCER(user_data);
	// The duration of backing track is used to set fixed duration for other tracks
	guint64 duration;
	duration = jammo_sample_get_duration(sample);
	g_print("Duration: %" G_GUINT64_FORMAT "\n", duration);


	//Currenlty hilighting of jammosamplebutton is white, so we need background
	ClutterColor *orange = clutter_color_new(255, 150, 0, 255);
	ClutterActor* color_box = clutter_rectangle_new_with_color(orange);
	clutter_actor_set_size(color_box, 800.0,480.0);
	clutter_container_add(CLUTTER_CONTAINER(stage), color_box, NULL);
	clutter_actor_lower_bottom(color_box);


	ClutterActor* play_button;
	play_button = tangle_button_new();
	clutter_actor_set_position(play_button, 700.0, 400.0);
	tangle_widget_add_after(TANGLE_WIDGET(play_button), tangle_texture_new("/opt/jammo/buttons/play-button.png"), NULL, NULL, NULL);
	g_signal_connect(play_button, "clicked", G_CALLBACK(on_play_button_clicked), sequencer);
	clutter_container_add(CLUTTER_CONTAINER(stage), play_button, NULL);

	ClutterActor* stop_button;
	stop_button = tangle_button_new();
	clutter_actor_set_position(stop_button, 600.0, 400.0);
	tangle_widget_add_after(TANGLE_WIDGET(stop_button), tangle_texture_new("/opt/jammo/buttons/stop-button.png"), NULL, NULL, NULL);
	g_signal_connect(stop_button, "clicked", G_CALLBACK(on_stop_button_clicked), sequencer);
	clutter_container_add(CLUTTER_CONTAINER(stage), stop_button, NULL);

	ClutterActor* pause_button;
	pause_button = tangle_button_new();
	clutter_actor_set_position(pause_button, 500.0, 400.0);
	tangle_widget_add_after(TANGLE_WIDGET(pause_button), tangle_texture_new("/opt/jammo/cross_icon.png"), NULL, NULL, NULL);
	g_signal_connect(pause_button, "clicked", G_CALLBACK(on_pause_button_clicked), sequencer);
	clutter_container_add(CLUTTER_CONTAINER(stage), pause_button, NULL);


	//Flamingo is 80px and marimba is 2slots on time
	sample_button = jammo_sample_button_new_from_files("/opt/jammo/themes/animal/140/flamingo.png", "/opt/jammo/themes/animal/140/animalw_140_marimba_2.wav");
	clutter_actor_set_position(sample_button, 100.0, 400.0);
	clutter_container_add_actor(CLUTTER_CONTAINER(stage), sample_button);

	guint64 duration2;
	//This is not yet calculated, need another 'on_duration_notify'
	//duration2 = jammo_sample_get_duration(jammo_sample_button_get_sample(sample_button));

	duration2 = 3428571429LLU;
	g_print("Duration of (two slot) sample is: %" G_GUINT64_FORMAT "\n", duration2);

	int number_of_slots = duration/(duration2/2);
	printf("number_of_slots: %d\n",number_of_slots);


	gfloat slot_width = 42.0;
	gfloat height = 50.0;
	sequencer_track_view_create_area_for_tracks(sequencer,number_of_slots, duration, slot_width,height);



	clutter_container_add(CLUTTER_CONTAINER(stage), jammo_get_actor_by_id("fullsequencer-track-area"), NULL);

	//There are also another loop_track types, but it onl affects for label-image
	sequencer_track_view_add_with_type(DRUM_TRACK_VIEW,FALSE,-1);
	sequencer_track_view_add_with_type(DRUM_TRACK_VIEW,FALSE,-1);
	sequencer_track_view_add_with_type(DRUM_TRACK_VIEW,FALSE,-1);
	sequencer_track_view_add_with_type(DRUM_TRACK_VIEW,FALSE,-1);
	sequencer_track_view_add_with_type(DRUM_TRACK_VIEW,FALSE,-1);
	sequencer_track_view_add_with_type(DRUM_TRACK_VIEW,FALSE,-1);

	clutter_actor_show_all (stage);

	// Do not call this function ever again!
	g_signal_handlers_disconnect_by_func(object, G_CALLBACK(on_duration_notify), user_data);
}

int main (int argc, char **argv)
{
	printf("Not currently working.\n");
	printf("sequencer_track_view_create_area_for_tracks is not returning anything anymore\n\n");
	exit(0);
	JammoSequencer *sequencer;

	jammo_meam_init (&argc, &argv, "jammo_test_duration_cache");
	jammo_chum_init (&argc, &argv);

	script = clutter_script_new();
	add_search_paths(script);


	sequencer = jammo_sequencer_new ();
	g_signal_connect(sequencer, "stopped", G_CALLBACK(on_sequencer_stopped), NULL);

	//There must be backing_track to keep cursor in sync.
	//Duration of the sequencer is ending point of last sample.
	JammoEditingTrack* backing_track;
	JammoSample* sample;
	backing_track = jammo_editing_track_new();
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(backing_track));
	sample = jammo_sample_new_from_file("/opt/jammo/themes/animal/140/backing_track.wav");
	jammo_editing_track_add_sample(backing_track, sample, 0);

	// Create tracks when the duration of the backing track is known.
	g_signal_connect(sample, "notify::duration", G_CALLBACK(on_duration_notify), sequencer);


	clutter_main ();

	return 0;
}
