/*
 * test-instrumet2
 *
 * This file is part of JamMo.
 *
 * This is for testing latency of real time virtual instrument playing.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen 
 */


#include <meam/jammo-meam.h>
#include <meam/jammo-instrument-track.h>
#include <chum/jammo-chum.h>
#include <tangle.h>

#include <tangle.h>
#include <glib-object.h>
#include <ctype.h>
#include <string.h>
#include <clutter/clutter.h>


static JammoInstrumentTrack *instrument_track;

static void
instrument_press (ClutterActor * actor, ClutterEvent * event, gpointer data)
{
  int octave = GPOINTER_TO_INT (data) / 100;
  int note = GPOINTER_TO_INT (data) - 100 * octave;
  char notename = note+octave*12;

	printf("octave: %d, note:%d, notename: %d\n", octave, note, notename);

  jammo_instrument_track_note_on_realtime (instrument_track, notename);
}

static void
instrument_release (ClutterActor * actor, ClutterEvent * event, gpointer data)
{
  int octave = GPOINTER_TO_INT (data) / 100;
  int note = GPOINTER_TO_INT (data) - 100 * octave;
  char notename = note+octave*12;

	printf("octave: %d, note:%d, notename: %d\n", octave, note, notename);

  jammo_instrument_track_note_off_realtime (instrument_track, notename);
}


static ClutterActor *
make_instrument_grid (int type)
{
  //regarding what instrument we are using, make boxes. FIXME this is very ad-hoc.
  int min_j, max_j;
  if (type == 0)
    {
      min_j = 3;
      max_j = 7;
    }
  else if (type == 1)
    {
      min_j = 1;
      max_j = 2;
    }
  else if (type == 2)
    {
      min_j = 2;
      max_j = 6;
    }
  else
    {
      min_j = 0;
      max_j = 8;
    }
  ClutterColor transparency = { 255, 255, 255, 0 };
  ClutterColor dark_color = { 25, 25, 12, 255 };
  ClutterActor *container = clutter_group_new ();
  int i;
  int j;
  // width / notes = 800/12 =66
  // height / octaves = 480/8 = 60
  for (i = 0; i < 12; i++)
    {
      for (j = min_j; j < max_j; j++)
	{
	  if (type == 0 && j == 6 && i > 0)	//Flute doesn't have these notes
	    continue;
	  if ((type == 1 && i == 1) || (type == 1 && i == 3))	//Drumkit doesn't have these notes 
	    continue;
	  if (type == 2 && j == 5 && i > 0)	//Ud doesn't have these notes
	    continue;
	  ClutterActor *box =
	    clutter_rectangle_new_with_color (&transparency);
	  clutter_rectangle_set_border_color (CLUTTER_RECTANGLE (box),
					      &dark_color);
	  clutter_rectangle_set_border_width (CLUTTER_RECTANGLE (box), 1);
	  clutter_actor_set_size (box, 66, 60);
	  clutter_actor_set_position (box, 66 * i, 60 * j);
	  clutter_actor_show (box);
	  clutter_container_add_actor (CLUTTER_CONTAINER (container), box);
	  clutter_actor_set_reactive (box, TRUE);
	  g_signal_connect (box, "button-press-event",
			    G_CALLBACK (instrument_press),
			    GINT_TO_POINTER (j * 100 + i));
	  g_signal_connect (box, "button-release-event",
			    G_CALLBACK (instrument_release),
			    GINT_TO_POINTER (j * 100 + i));
	}
    }
  return container;
}


int
main (int argc, char **argv)
{
  JammoSequencer *sequencer;
  ClutterActor *stage;

  jammo_meam_init (&argc, &argv, "jammo_test_duration_cache");
  jammo_chum_init (&argc, &argv);


  stage = clutter_stage_get_default ();
  //clutter_stage_set_fullscreen(CLUTTER_STAGE(stage), TRUE);
  clutter_actor_set_size (stage, 800.0, 480.0);


  int type = 3;			//0= flute, 1=drumkit ,2=ud, 3=piano
  sequencer = jammo_sequencer_new ();
  instrument_track = jammo_instrument_track_new (type);
  jammo_sequencer_add_track (sequencer, JAMMO_TRACK (instrument_track));
  jammo_instrument_track_set_realtime (instrument_track, TRUE);


  clutter_container_add (CLUTTER_CONTAINER (stage),
			 make_instrument_grid (type), NULL);

  jammo_sequencer_play (JAMMO_SEQUENCER (sequencer));


  clutter_actor_show_all (stage);
  clutter_main ();

  return 0;
}
