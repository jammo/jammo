#include <stdio.h>
#include "community_server_functions.h"


int main(int argc, char** argv)  
{  
  	char* songname=NULL;
	char* userid=NULL;
	char* filename=NULL;
	char* path=NULL;
	char* description=NULL;
	char* date=NULL;
	char* json=NULL;

	getJammoData("testsong", "67", "testsong.ogg", "test description", "17.9.2010", "json file contents should be here");
	getJammoSongData("1", &songname,&userid, &filename, &path, &description, &date, &json);
	printf("Songname %s, userid %s, filename %s, path %s, description %s, date %s, json %s\n", songname,userid, filename, path, description, date, json);
	if(argc==2)
	{
		sendJammoFile(argv[1]);
		
	}
	return 0;
}
