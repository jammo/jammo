#include "../../src/gems/gems_teacher_server_utils.h"
#include "../../src/gems/gems_definitions.h"
#include "../../src/gems/gems_structures.h"
#include "../../src/gems/gems_security.h"
#include "../../src/gems/communication.h"
#include "../../src/cem/cem.h"
#include "../../src/gems/gems_message_functions.h"
#include <peerhood/PeerHood.h>
#include <peerhood/IteratorWrapper.h>
#include <string.h>

typedef struct {
	GList* clients;
	teacher_server_profile_data* database;
	MPeerHood* ph;
	C_Callback* ph_cb;
} serverdata;

serverdata* new_data_container();
void clear_data_container(serverdata* data);
void sighandler(int sig);
void ph_callback_notify(short aEvent, const char* aAddress,void* aData);
gint compare_connections(gems_connection* a, gems_connection* b);
void ph_callback_newconnection(const unsigned short aPort, MAbstractConnection* aConnection, int aConnectionId, void* aData);
gboolean process_data(gems_connection* c, serverdata* data);
void check_connection(gems_connection* c, serverdata* data);
void check_connections(serverdata* data);
void search_clients(serverdata* data);
teacher_server_profile_data* load_database(teacher_server_profile_data* data);
void create_profile_to_database(teacher_server_profile_data* data);
void save_database(teacher_server_profile_data* data);
