
#include "../../src/gems/gems_teacher_server_utils.h"
#include "../../src/cem/cem.h"
#include <stdio.h>

teacher_server_profile_data* load(teacher_server_profile_data* data)
{
	switch(gems_teacher_server_read_profile_database(&data))
	{
		case READ_OK:
			cem_add_to_log("Teacher database read success!",J_LOG_INFO);
			break;
		case READ_FAIL_FILE_NOT_EXIST:
			cem_add_to_log("Teacher database read failure! No database!",J_LOG_INFO);
			break;
		default:
			cem_add_to_log("Teacher database read failure!",J_LOG_INFO);
			break;
	}

	if(data != NULL)
	{
		if(gems_teacher_server_decrypt_profiles(data,"mypassword")) cem_add_to_log("Teacher database decryption success!",J_LOG_INFO);
		else cem_add_to_log("Teacher database decryption failure!",J_LOG_INFO);
	}
	
	return data;
}

void save(teacher_server_profile_data* data)
{
	// Encrypt profiles (all!)
	if(gems_teacher_server_encrypt_profiles(data, "mypassword")) cem_add_to_log("Teacher database encryption success!", J_LOG_INFO);
	else cem_add_to_log("Teacher database encryption failure!", J_LOG_INFO);
	
	if(gems_teacher_server_write_profile_database(data) == WRITE_OK) cem_add_to_log("Teacher database saving success!", J_LOG_INFO);
	else cem_add_to_log("Teacher database saving failure!", J_LOG_INFO);
}

teacher_server_profile_data* add(teacher_server_profile_data* data)
{
	jammo_profile_container* profile = NULL;
	if(!data) return NULL;
	
	// Set 10 profiles
	for(gint i = 0; i < 10; i ++)
	{
		gchar* user = g_strdup_printf("user%d",i+1);
		gchar* pass = g_strdup_printf("%d1234567",i);
		cem_add_to_log("Adding a profile",J_LOG_INFO);
		profile = gems_teacher_server_create_profile(PROF_TYPE_REMOTE,i+1,user,pass,"none","firstuser","lastname",6+(i%3),0);
		if(profile)
		{
			data->profiles = g_list_append(data->profiles, profile);
			cem_add_to_log("Profile added to database", J_LOG_INFO);
		}
		else cem_add_to_log("Profile not added to database", J_LOG_INFO);
		g_free(user);
		g_free(pass);
	}
	
	// create invalid profile
	if(( profile = gems_teacher_server_create_profile(PROF_TYPE_REMOTE,11,"wololoo.user","ad-hoc","none","firstuser","lastname",6+4,0)) != NULL)
		data->profiles = g_list_append(data->profiles, profile);
	else cem_add_to_log("invalid profile",J_LOG_INFO);
	
	return data;
}

int main(int argc, char* argv[])
{
	cem_set_log_level(J_LOG_DEBUG+J_LOG_INFO+J_LOG_ERROR);
	teacher_server_profile_data* data = NULL;
	
	if(g_strcmp0(argv[1],"save") == 0)
	{
		// init container
		data = gems_teacher_server_new_profile_data_container();
		
		// create data to database
		data = add(data);
		
		if(g_list_length(data->profiles) != 0)
		{
			cem_add_to_log("PROFILES IN DATABASE:",J_LOG_INFO);
			gems_teacher_server_print_profile_list(data->profiles);
		}
		else cem_add_to_log("PROFILE DATABASE EMPTY!",J_LOG_INFO);
		
		// encrypt and save content to file
		save(data);
		
		gems_teacher_server_clear_profile_data_container(data);
		data = NULL;
		return 0;
	}
	
	else if(g_strcmp0(argv[1],"load") == 0)
	{
		// init container
		data = gems_teacher_server_new_profile_data_container();
		
		// load
		data = load(data);
		if((data) && (data->profiles) && (g_list_length(data->profiles) != 0))
		{
			cem_add_to_log("PROFILES IN LOADED DATABASE:",J_LOG_INFO);
			gems_teacher_server_print_profile_list(data->profiles);
		}
		else cem_add_to_log("PROFILE DATABASE EMPTY!",J_LOG_INFO);
		
		gems_teacher_server_clear_profile_data_container(data);
		data = NULL;
		return 0;
	}
	
	else if(g_strcmp0(argv[1],"all") == 0)
	{
		// init container
		data = gems_teacher_server_new_profile_data_container();
		
		// create data to database
		data = add(data);
		
		if(g_list_length(data->profiles) != 0)
		{
			cem_add_to_log("PROFILES IN DATABASE:",J_LOG_INFO);
			gems_teacher_server_print_profile_list(data->profiles);
		}
		else cem_add_to_log("PROFILE DATABASE EMPTY!",J_LOG_INFO);
		
		// encrypt and save content to file
		save(data);
		
		gems_teacher_server_clear_profile_data_container(data);
		data = NULL;
		
		// init container
		data = gems_teacher_server_new_profile_data_container();
		
		// load
		data = load(data);
		
		if(g_list_length(data->profiles) != 0)
		{
			cem_add_to_log("PROFILES IN LOADED DATABASE:",J_LOG_INFO);
			gems_teacher_server_print_profile_list(data->profiles);
		}
		else cem_add_to_log("PROFILE DATABASE EMPTY!",J_LOG_INFO);
		
		gems_teacher_server_clear_profile_data_container(data);
		data = NULL;
		return 0;
	}
	
	else
	{
		cem_add_to_log("Invalid parameter, use load/save/all",J_LOG_ERROR);
		return -1;
	}
	
	return 0;
}
