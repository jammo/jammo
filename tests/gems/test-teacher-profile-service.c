
#include <sys/select.h>


#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "test-teacher-profile-service.h"
#include "../../src/configure.h"

volatile gboolean run = TRUE;
volatile gboolean docleanup = FALSE;


serverdata* new_data_container()
{
	serverdata* data = (serverdata*)g_malloc(sizeof(serverdata));
	data->database = gems_teacher_server_new_profile_data_container();
	data->clients = NULL; // client list
	data->ph = NULL; // peerhood
	data->ph_cb = NULL; // peerhood callback
	
	return data;
}

void clear_data_container(serverdata* data)
{
	if(data)
	{
		// Cleanup connected clients
		if(data->clients)
		{
			// Clear each gems_connection
			g_list_foreach(data->clients,(GFunc)gems_clear_gems_connection,NULL);
			g_list_free(data->clients);
		}
		
		if(data->ph) ph_c_delete_peerhood(data->ph);
		if(data->ph_cb) ph_c_delete_callback(data->ph_cb);
		if(data->database) gems_teacher_server_clear_profile_data_container(data->database);
		g_free(data);
		data = NULL;
	}
}

void sighandler(int sig)
{
	cem_add_to_log("Caught kill/term/int signal. Shutting down.",J_LOG_INFO);
	run = FALSE;
}

void ph_callback_notify(short aEvent, const char* aAddress,void* aData)
{
	gchar* logmsg = g_strdup_printf("gems_ph_callback_notify: event notify (%d) from %s.", aEvent,aAddress);
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
}

// Check if connections are the same (the checksums match)
gint compare_connections(gems_connection* a, gems_connection* b)
{
	if(ph_c_connection_get_device_checksum(a->connection) == ph_c_connection_get_device_checksum(b->connection)) return 0;
	else if(ph_c_connection_get_device_checksum(a->connection) < ph_c_connection_get_device_checksum(b->connection)) return -1;
	else return 1;
}

void ph_callback_newconnection(const unsigned short aPort, MAbstractConnection* aConnection, int aConnectionId, void* aData)
{
	serverdata* data = NULL;
	
	if(aData) data = (serverdata*)aData;
	gems_connection *c = gems_new_gems_connection(aConnection,aPort,aConnectionId,1,0);
	
	// If not found from list, add
	if(g_list_find_custom(data->clients,c,(GCompareFunc)compare_connections) == NULL)
	{
		data->clients = g_list_append(data->clients,c);
		cem_add_to_log("New connection arrived",J_LOG_INFO);
	}
	else
	{
		cem_add_to_log("Is already connected",J_LOG_INFO);
		gems_clear_gems_connection(c);
	}
}

gboolean process_data(gems_connection* c,serverdata* data)
{
	if(!c || !data) return FALSE;
	
	// Check envelope type
	if(ntohs(gems_connection_get_16(c,0)) == JAMMO_PACKET_PRIVATE)
	{
		// Check that envelope can be extracted
		if(gems_security_extract_envelope(c) != SECURITY_OK)
		{
			// Omnom, devour the message since it was failurously delicious
			gems_connection_clear_buffer(c);
			return FALSE;
		}
	}
	
	// Localize data (network -> host), add new types to handle into localization func
	gems_connection_localize_data_in_buffer(c);
	gchar* log = NULL;
	guint position = 0;
	// Process packet, check the service id [first 2 bytes]
	switch(gems_connection_get_16(c,0))
	{
		// To teacher profile service
		case TEACHER_PROFILE_SERVICE_ID:
			// Check command
			if(gems_connection_get_16(c,sizeof(gint16)+sizeof(gint32)) == REQ_ENC_PROFILE)
			{
				// Get username length, packet [ 16bit service | 32bit length | 16bit command | username ]
				gint unamelen = gems_connection_get_32(c,sizeof(gint16)) - sizeof(gint16) - sizeof(gint32) - sizeof(gint16);
				// Get username
				gchar* username = gems_connection_get_data(c,sizeof(gint16)+sizeof(gint32)+sizeof(gint16),unamelen);
				
				log = g_strdup_printf("Got profile request, username: \"%s\"",username);
				cem_add_to_log(log,J_LOG_INFO);
				g_free(log);
				
				// Search for profile
				jammo_profile_container* profile = gems_teacher_server_get_profile_for_user(data->database,username);
				if(profile)
				{
					// Found, construct a reply message using encrypted profile (+its length) and username 
					gems_message* msg = gems_create_message_teacher_service_profile_reply(profile->encrypted,profile->encrypted_length,username);
					if(msg)
					{
						// Try to send message as encrypted
						if(!gems_communication_write_encrypted_data(JAMMO_PACKET_ADMIN,c,msg))
						{
							ph_c_connection_delete(c->connection);
							docleanup = TRUE;
						}
						else
						{
							if(c->profile) gems_peer_profile_set_values(c->profile,profile->username,profile->userid,profile->age,profile->avatarid);
							else c->profile = gems_new_peer_profile(profile->username,profile->userid,profile->age,profile->avatarid);
							log = g_strdup_printf("Sent profile for username \"%s\"",username);
							cem_add_to_log(log,J_LOG_INFO);
							g_free(log);
						}
					}
					gems_clear_message(msg);
				}
				else
				{
					log = g_strdup_printf("No profile found for username \"%s\"",username);
					cem_add_to_log(log,J_LOG_INFO);
					g_free(log);
					
					gems_message* errormsg = gems_create_error_message(ERROR_NO_PROFILE_FOR_USERNAME_TYPE,PROFILE_SERVICE_ID);
					
					if(gems_communication_write_data(ph_c_connection_get_fd(c->connection),errormsg) != errormsg->length)
						cem_add_to_log("Cannot write ERROR_NO_PROFILE_FOR_USERNAME",J_LOG_ERROR);
				
					gems_clear_message(errormsg);
					
				}
				g_free(username);
			}
			
			else if(gems_connection_get_16(c,sizeof(gint16)+sizeof(gint32)) == PROFILE_FULL)
			{
				position = sizeof(gint16)+sizeof(gint32)+sizeof(gint16);
			
				gchar* username = gems_connection_get_char(c,position); // do not free, points to username in buffer!!
				position = position + strlen(username) + 1;
			
				guint32 userid = gems_connection_get_32(c,position);
				position = position + sizeof(guint32);
			
				guint16 age = gems_connection_get_16(c,position);
				position = position + sizeof(guint16);
			
				guint32 aid = gems_connection_get_32(c,position);
				position = position + sizeof(guint32);
			
				gchar* firstname = gems_connection_get_char(c,position); // do not free, points to username in buffer!!
				position = position + strlen(firstname) + 1;
			
				gchar* lastname = gems_connection_get_char(c,position); // do not free, points to username in buffer!!
				position = position + strlen(lastname) + 1;
			
				guint32 points = gems_connection_get_32(c,position);
			
				gchar* logmsg = g_strdup_printf("Got full profile: userid:%u username: %s age:%d fullname:%s %s points:%u avatar:%u", 
					userid,username,age,firstname,lastname,points,aid);
				cem_add_to_log(logmsg,J_LOG_INFO);
				g_free(logmsg);
				
				jammo_profile_container* prof = gems_teacher_server_get_profile_for_user_id(data->database,userid);
				
				// found, update
				if(prof)
				{
					if(g_strcmp0(prof->username,username) == 0)
					{
						prof->points = points;
						prof->avatarid = aid;
						cem_add_to_log("Points and avatar updated.",J_LOG_INFO);
					}
					else cem_add_to_log("Username changed, not updated",J_LOG_ERROR);
				}
				else cem_add_to_log("Not a valid profile, ignoring",J_LOG_INFO);
			}
			else if (gems_connection_get_16(c,sizeof(gint16)+sizeof(gint32)) == PROFILE_PASSCHANGED)
			{
				cem_add_to_log("PROFILE_PASSCHANGED",J_LOG_INFO);
				position = sizeof(guint16);
				
				guint32 length = gems_connection_get_32(c,position);
				position = position + sizeof(guint32)+sizeof(guint16);
				
				guint32 uid = gems_connection_get_32(c,position);
				position = position + sizeof(guint32);
				
				guint8 pwlen = *(guint8*)&c->nwbuffer[position];
				position++;
				
				guchar* rechash = (guchar*)gems_connection_get_data(c,position,SHA_HASH_LENGTH);
				position = position + SHA_HASH_LENGTH;
				
				guint passlength = length - position;
				guchar* recnewencpass = (guchar*)gems_connection_get_data(c,position,passlength);
				
				jammo_profile_container* prof = gems_teacher_server_get_profile_for_user_id(data->database,uid);
				
				// found, update
				if(prof)
				{
					guchar* passwd = gems_security_create_password(prof->password,prof->salt);
					if(gems_security_init_security(passwd,prof->salt))
					{
						// decrypt new pass
						guchar* decnewpass = gems_security_decrypt_data(recnewencpass,&passlength);
						
						gchar* newpass = (gchar*)g_malloc0(sizeof(gchar*)*pwlen);
						g_memmove(newpass,decnewpass,pwlen);
						guint newpasslen = pwlen;
						
						guchar* decnewpasshash = gems_security_calculate_hash((guchar*)newpass,&newpasslen);
						
						// compare hashes
						if(gems_security_verify_hash(rechash,decnewpasshash,SHA_HASH_LENGTH))
						{
							log = g_strdup_printf("Password change received, old password = \"%s\", new password = \"%s\"",prof->password,decnewpass);
							cem_add_to_log(log,J_LOG_INFO);
							gems_security_clear_security();
							if(gems_teacher_server_change_password(prof,newpass)) cem_add_to_log("Profile password updated.",J_LOG_INFO);
							else cem_add_to_log("Error, cannot update profile password",J_LOG_ERROR);
						}
						else cem_add_to_log("Invalid password change",J_LOG_INFO);
						
						g_free(newpass);
						g_free(decnewpass);
						g_free(decnewpasshash);
						
						// TODO if hashes ok, clear security and initialize new with new password and encrypt profile
					}
					
					g_free(passwd);
				}
				else cem_add_to_log("Error, no such user exists, discarding",J_LOG_ERROR);
				g_free(rechash);
				g_free(recnewencpass);
			}
			else if (gems_connection_get_16(c,sizeof(gint16)+sizeof(gint32)) == CONTROL_REPLY_LOGIN)
			{
				// contains public profile
				if(gems_connection_get_32(c,sizeof(guint16)) > 8)
				{
					position = 8;
					
					gchar* username = gems_connection_get_char(c,position);
					position = position + strlen(username) + 1;
					
					// User id
					guint32 uid = gems_connection_get_32(c,position);
					position = position + sizeof(guint32);
			
					
					guint16 age = gems_connection_get_16(c,position);
					position = position + sizeof(guint16);
			
					
					guint32 aid = gems_connection_get_32(c,position);
					
					log = g_strdup_printf("Got profile, [Id:%u|Name:%s|Age:%d|Avtr:%u]",
						uid,username,age,aid);
					cem_add_to_log(log,J_LOG_NETWORK_DEBUG);
					g_free(log);

					jammo_profile_container* prof = gems_teacher_server_get_profile_for_user_id(data->database,uid);
					printf("done\n");
					if(prof)
					{
						if(c->profile) gems_peer_profile_set_values(c->profile,prof->username,prof->userid,prof->age,prof->avatarid);
						else c->profile = gems_new_peer_profile(prof->username,prof->userid,prof->age,prof->avatarid);
						
						log = g_strdup_printf("Logged user on device %s/%u is %u:%s, user is in our database.", 
							ph_c_connection_get_remote_address(c->connection),ph_c_connection_get_device_checksum(c->connection),
							prof->userid,prof->username);
						cem_add_to_log(log,J_LOG_INFO);
						g_free(log);
					}
					else
					{
						log = g_strdup_printf("Logged user \"%s\" on device %s/%u not found from database.", 
							username, ph_c_connection_get_remote_address(c->connection),ph_c_connection_get_device_checksum(c->connection));
						cem_add_to_log(log,J_LOG_INFO);
						g_free(log);
					}
				}
				else
				{
					log = g_strdup_printf("User not logged on device %s/%u .", 
						ph_c_connection_get_remote_address(c->connection),ph_c_connection_get_device_checksum(c->connection));
					cem_add_to_log(log,J_LOG_INFO);
					g_free(log);
				}
			}
			break;
		case ERROR_MSG:
			// TODO handle error messages
			break;
		default:
			// TODO handle errors
			break;
	}
	// Clear data in buffer
	gems_connection_clear_buffer(c);
	return TRUE;
}

// Check if connection has data and read the data
void check_connection(gems_connection* c, serverdata* data)
{	
	gint bytes = 0;
	if(!c) return;
	if(!c->connection) return;
	
	// Is connected
	if(ph_c_connection_is_connected(c->connection))
	{
		// incoming data
		if(ph_c_connection_has_data(c->connection))
		{
			//cem_add_to_log("Processing connection: has data", J_LOG_INFO);
			// read service - 16 bits
			if(gems_connection_partial_data_amount(c) == 0)
			{
				gint16 cid = -1;
				bytes = 0;
				
				// Read 16 bits
				if((bytes = gems_communication_read_data(ph_c_connection_get_fd(c->connection), &cid, sizeof(gint16))) != sizeof(gint16))
				{
					cem_add_to_log("Read less than service id",J_LOG_ERROR);
					if(bytes <= 0)
					{
						ph_c_connection_disconnect(c->connection);
						docleanup = TRUE;
					}
				}
				// Add to buffer
				else gems_connection_add_16(c,cid);
			}
			// read length
			else if(gems_connection_partial_data_amount(c) == sizeof(gint16))
			{
				gint32 length = 0;
				bytes = 0;
				
				// read 32 bits
				if((bytes = gems_communication_read_data(ph_c_connection_get_fd(c->connection), &length, sizeof(gint32))) != sizeof(gint32))
				{
					cem_add_to_log("Read less than length id",J_LOG_ERROR);
					if(bytes <= 0)
					{
						ph_c_connection_disconnect(c->connection);
						docleanup = TRUE;
					}
				}
				// add to buffer
				else gems_connection_add_32(c,length);
			}
			// read rest
			else if(gems_connection_partial_data_amount(c) == sizeof(gint16) + sizeof(gint32))
			{
				// Calculate remaining length
				gint remaining = ntohl(gems_connection_get_32(c,sizeof(gint16))) - gems_connection_partial_data_amount(c);
				gchar remainingdata[remaining];
				memset(&remainingdata,0,remaining);
				
				// Read rest of the packet
				if((bytes = gems_communication_read_data(ph_c_connection_get_fd(c->connection), &remainingdata, remaining)) != remaining)
				{
					cem_add_to_log("All data not completely read!",J_LOG_ERROR);
					if(bytes <= 0)
					{
						ph_c_connection_disconnect(c->connection);
						docleanup = TRUE;
					}
					// add what was read
					else gems_connection_add_data(c,remainingdata,bytes);
				}
				// All remaining data was read
				else
				{
					//cem_add_to_log("All data read!",J_LOG_DEBUG);
					gems_connection_add_data(c,remainingdata,remaining);
				}
			}
			// Read 1 byte .. just to be sure
			else
			{
				gchar rbyte = '\0';
				if(gems_communication_read_data(ph_c_connection_get_fd(c->connection), &rbyte, sizeof(gchar)) < sizeof(gchar))
				{
					cem_add_to_log("Cannot read a byte! Closing connection.",J_LOG_ERROR);
					ph_c_connection_disconnect(c->connection);
					docleanup = TRUE;
				}
				else gems_connection_add_data(c,&rbyte,sizeof(gchar));
			}
			
			if(gems_connection_partial_data_amount(c) == ntohl(gems_connection_get_32(c,sizeof(gint16))))
			{
				if(!process_data(c,data))
				{
					ph_c_connection_disconnect(c->connection);
					docleanup = TRUE;
				}
			}
		}
	}
}

// Check every gems_connection* element in list
void check_connections(serverdata* data)
{
	g_list_foreach(data->clients,(GFunc)check_connection,data);
}

void search_clients(serverdata* data)
{
	// Get all devices with control service (this is the service teacher connects to)
	TDeviceList* list = ph_c_get_devicelist_with_services(data->ph,CONTROL_SERVICE_NAME);
	if(list)
	{
		if(ph_c_devicelist_is_empty(list) == FALSE)
		{
			DeviceIterator* iterator = NULL;
			
			for(iterator = ph_c_new_device_iterator(list); !ph_c_device_iterator_is_last(iterator,list); ph_c_device_iterator_next_iterator(iterator))
			{
				MAbstractDevice* dev = ph_c_device_iterator_get_device(iterator);
				
				// Check if connection exists
				if(gems_communication_get_connection(data->clients,ph_c_device_get_checksum(dev)) == NULL)
				{
					// Through WLAN and has Peerhood
					if((g_strcmp0(ph_c_device_get_prototype(dev),"wlan-base") == 0) && (ph_c_device_has_peerhood(dev) == TRUE))
					{
						MAbstractConnection* conn = ph_c_connect_remoteservice(data->ph, dev, CONTROL_SERVICE_NAME);
					
						if(conn)
						{
							// Use gems_connection structs in list
							gems_connection* new_conn = gems_new_gems_connection(conn, 0, 0, 1, 0);
							data->clients = g_list_append(data->clients, new_conn);
							cem_add_to_log("New connection established!", J_LOG_INFO);
						}
					}
				}
			} // for
			ph_c_delete_device_iterator(iterator);
		}
		ph_c_delete_devicelist(list);
	}
}

teacher_server_profile_data* load_database(teacher_server_profile_data* data)
{
	// Read profile db
	switch(gems_teacher_server_read_profile_database(&data))
	{
		case READ_OK:
			cem_add_to_log("Teacher database read success!",J_LOG_INFO);
			break;
		case READ_FAIL_FILE_NOT_EXIST:
			cem_add_to_log("Teacher database read failure! No database!",J_LOG_INFO);
			// Create new if db does not exist
			data = gems_teacher_server_new_profile_data_container();
			return data;
		default:
			cem_add_to_log("Teacher database read failure!",J_LOG_INFO);
			return NULL;
	}
	
	// Decrypt data with password, TODO: clear database if cannot decrypt!
	if(gems_teacher_server_decrypt_profiles(data,"mypassword")) cem_add_to_log("Teacher database decryption success!",J_LOG_INFO);
	else cem_add_to_log("Teacher database decryption failure!",J_LOG_INFO);
	
	return data;
}

// Encrypt profiles and save them, TODO: react to errors
void save_database(teacher_server_profile_data* data)
{
	// Encrypt profiles (all!)
	if(gems_teacher_server_encrypt_profiles(data, "mypassword")) cem_add_to_log("Teacher database encryption success!", J_LOG_INFO);
	else cem_add_to_log("Teacher database encryption failure!", J_LOG_INFO);
	
	if(gems_teacher_server_write_profile_database(data) == WRITE_OK) cem_add_to_log("Teacher database saving success!", J_LOG_INFO);
	else cem_add_to_log("Teacher database saving failure!", J_LOG_INFO);
}

void create_profile_to_database(teacher_server_profile_data* data)
{
	gboolean do_more = TRUE;
	GRand* grand = g_rand_new_with_seed(time(NULL));
	// Username
	gchar* username = (gchar*)g_malloc0(sizeof(gchar*)*50);
	while(do_more)
	{
		printf("Username: ");
		if(fgets(username,50,stdin))
		{
			gems_erase_newline(username);
			// Check for illegal characters
			if(!gems_check_illegal_characters(username))
			{
				// Check if username taken
				if(!gems_teacher_server_get_profile_for_user(data,username)) do_more = FALSE;
				else memset(username,0,50);
			}
			else memset(username,0,50);
		}
	}
	do_more = TRUE;

	gchar* password = (gchar*)g_malloc0(sizeof(gchar*)*PASSLEN);
	while(do_more)
	{
		printf("Password: ");
		if(fgets(password,PASSLEN,stdin))
		{
			gems_erase_newline(password);
			// Check
			if(!gems_check_illegal_characters(password))
			{
				// Check if numbers
				if(gems_all_integers(password)) do_more = FALSE;
				else memset(password,0,50);
			}
			else memset(password,0,PASSLEN);
		}
	}
	do_more = TRUE;
	
	gchar* firstname = (gchar*)g_malloc0(sizeof(gchar*)*50);
	while(do_more)
	{
		printf("Firstname: ");
		if(fgets(firstname,50,stdin))
		{
			gems_erase_newline(firstname);
			// Check for illegal characters
			if(!gems_check_illegal_characters(firstname)) do_more = FALSE;
			else memset(firstname,0,50);
		}
	}
	do_more = TRUE;
	
	gchar* lastname = (gchar*)g_malloc0(sizeof(gchar*)*50);
	while(do_more)
	{
		printf("Lastname: ");
		if(fgets(lastname,50,stdin))
		{
			gems_erase_newline(lastname);
			// Check for illegal characters
			if(!gems_check_illegal_characters(lastname))	do_more = FALSE;
			else memset(lastname,0,50);
		}
	}
	do_more = TRUE;
	
	// Create id which is not taken
	guint32 id = g_rand_int(grand);
	while(gems_teacher_server_get_profile_for_user_id(data,id)) id = g_rand_int(grand);
	
	// here be bug, TODO fix it
	gchar* agestring = (gchar*)g_malloc0(sizeof(gchar*)*4);
	guint16 age = 0;
	while(do_more)
	{
		printf("Age (2 char max!): ");
		if(fgets(agestring,4,stdin))
		{
			gems_erase_newline(agestring);
			// Check for integers
			if(gems_all_integers(agestring))
			{
				age = (guint16)atoi(agestring);
				if(age < 100)	do_more = FALSE;
			}
			else memset(agestring,0,4);
		}
	}
	do_more = TRUE;

	// here be bug, TODO fix it
	gchar* maxlenstr = g_strdup_printf("%u",G_MAXUINT32);
	guint32 maxlen = strlen(maxlenstr);
	gchar* avataridstring = (gchar*)g_malloc0(sizeof(gchar*)*maxlen);

	guint32 avatarid = PROF_DEFAULT_AVATARID;
	/*while(do_more)
	{
		printf("Avatar id (%u char max!): ",maxlen);
		if(fgets(avataridstring,maxlen+1,stdin))
		{
			gems_erase_newline(avataridstring);
			// Check for integers
			if(gems_all_integers(avataridstring))
			{
				avatarid = (guint32)strtoul(avataridstring,NULL,10);
				do_more = FALSE;
			}
			else memset(avataridstring,0,maxlen);
		}
	}
	do_more = TRUE;*/

	// Create profile
	jammo_profile_container* profile = gems_teacher_server_create_profile(PROF_TYPE_REMOTE,id,username,password,"none",firstname,lastname,age,avatarid);
	
	// Profile could be created, add it to list
	if(profile)
	{
		data->profiles = g_list_append(data->profiles,profile);
	
		cem_add_to_log("Profile added",J_LOG_INFO);
	
		gems_teacher_server_print_profile_list(data->profiles);
	}
	else cem_add_to_log("Profile not added, parameters invalid",J_LOG_INFO);
	g_free(username);
	g_free(password);
	g_free(firstname);
	g_free(lastname);
	g_free(agestring);
	g_free(avataridstring);
	g_free(grand);
	cem_add_to_log("done",J_LOG_INFO);
}

int main(int argc,char* argv[])
{
	gint pport = 0,tport = 0;
	serverdata* data = new_data_container();
	gchar stdinput[100];
	memset(&stdinput,0,100);
	
	configure_init_directories(FALSE);
	cem_set_log_level(J_LOG_DEBUG+J_LOG_INFO+J_LOG_ERROR+J_LOG_NETWORK_DEBUG);
	cem_print_also_to_stdout(TRUE);
	
	signal(SIGTERM,sighandler);
	signal(SIGINT,sighandler);
	
	void (*notify)(short, const char*,void*) = NULL;
	notify = &ph_callback_notify;

	void (*newconnection)(const unsigned short, MAbstractConnection*, int,void*) = NULL;
	newconnection = &ph_callback_newconnection;
	
	// Callback and instance of PeerHood
	data->ph_cb = ph_c_create_callback(notify, newconnection,NULL,(void*)data);
	data->ph = ph_c_get_instance(data->ph_cb);
	
	// Load database and print it
	data->database = load_database(data->database);
	if(!data->database) exit(1);
	gems_teacher_server_print_profile_list(data->database->profiles);
	cem_add_to_log("------------------------------------------------------------",J_LOG_INFO);
	cem_add_to_log("Teacher profile service, press Ctrl+C to terminate.",J_LOG_INFO);
	cem_add_to_log("Type \"add\" to enter new profile to database.", J_LOG_INFO);
	cem_add_to_log("Type \"set\" to set a username to login for specific client.", J_LOG_INFO);
	cem_add_to_log("Type \"push\" to send a profile for specific client.", J_LOG_INFO);
	cem_add_to_log("Type \"get\" to get a full profile from all clients.", J_LOG_INFO);
	cem_add_to_log("------------------------------------------------------------",J_LOG_INFO);
	
	if(ph_c_init(data->ph,argc,argv) == TRUE)
	{
		cem_add_to_log("PeerHood initialized",J_LOG_INFO);
		if((tport = ph_c_register_service(data->ph,TEACHER_SERVICE_NAME,"application:jammoteacher,method:teach")) != 0) 
		{
			cem_add_to_log("Registered Teacher service",J_LOG_INFO);
		
			// Add teacher profile service
			if((pport = ph_c_register_service(data->ph,TEACHER_PROFILE_SERVICE_NAME,"application:jammoteacher,method:ALL_YOUR_BASE_ARE_BELONG_TO_US")) != 0)
			{
				cem_add_to_log("Registered Teacher profile service",J_LOG_INFO);
				fd_set s;
				struct timeval tv;
				tv.tv_sec = 0;
				tv.tv_usec = 0;
				while(run)
				{
					check_connections(data); // Check for incoming data
					search_clients(data); // Search for JamMos
				
					if(docleanup)
					{
						cem_add_to_log("Connection cleanup",J_LOG_INFO);
						data->clients = gems_cleanup_connections_from_list(data->clients);
						docleanup = FALSE;
					}
				
					FD_SET(0,&s);
				
					switch(select(0+1,&s,NULL,NULL,&tv))
					{
						case -1:
							exit(1);
							break;
						case 0:
							break;
						default:
							if(FD_ISSET(0,&s)) // stdin
							{
								// Enter pressed, entering a new profile data
								if(fgets(stdinput,100,stdin) == NULL) break;
								
								gems_erase_newline(stdinput);
								
								if(g_strcmp0(stdinput,"add") == 0) create_profile_to_database(data->database);
								
								if((g_strcmp0(stdinput,"set") == 0) || (g_strcmp0(stdinput,"push") == 0))
								{
									gint type = (g_strcmp0(stdinput,"set") == 0) ? 0 : 1 ;
									
									if(g_list_length(data->clients) <= 0)
									{
										cem_add_to_log("no clients",J_LOG_INFO);
										break;
									}
									
									guint32 pnmbr = 0;
									gchar tmpstr[50];
									memset(&tmpstr,0,50);
									
									cem_add_to_log("Select a profile (give user id number or the username)",J_LOG_INFO);
									
									gems_teacher_server_print_profile_list(data->database->profiles);
									
									// Get avatar id
									if(fgets(tmpstr,50,stdin) == NULL) break;
									else
									{
										gems_erase_newline(tmpstr);
										if(gems_all_integers(tmpstr)) pnmbr = (guint32)strtoul(tmpstr,NULL,10);
									}
									
									// Get profile for id
									jammo_profile_container* cont = NULL;
									if(pnmbr != 0) cont = gems_teacher_server_get_profile_for_user_id(data->database,pnmbr);
									else cont = gems_teacher_server_get_profile_for_user(data->database,tmpstr);
									if(!cont)
									{
										cem_add_to_log("Not found, invalid id/username",J_LOG_INFO);
										break;
									}
									
									cem_add_to_log("Selected profile:",J_LOG_INFO);
									gems_teacher_server_print_profile(cont);
									
									gint cnmbr = 0;
									
									// Select connection
									cem_add_to_log("Select a connection (give order number)",J_LOG_INFO);
									for(gint i = 0; i < g_list_length(data->clients); i++)
									{
										gchar* log = g_strdup_printf("%d: %u",i,
											ph_c_connection_get_device_checksum(((gems_connection*)(g_list_nth(data->clients,i)->data))->connection));
										cem_add_to_log(log,J_LOG_INFO);
										g_free(log);
									}
									memset(&tmpstr,0,50);
									// Get connection number
									if(fgets(tmpstr,50,stdin) == NULL) break;
									else
									{
										gems_erase_newline(tmpstr);
										if(gems_all_integers(tmpstr)) cnmbr = (guint32)strtoul(tmpstr,NULL,10);
										else break;
									}
									
									// Create message
									gems_message* msg = NULL;
									if(type == 1) msg = gems_create_message_teacher_service_profile_reply(cont->encrypted,cont->encrypted_length,cont->username);
									else msg = gems_create_message_control_set_login_name(cont->username);
									// Get connection
									gems_connection* cn = (gems_connection*)(g_list_nth(data->clients,cnmbr)->data);
									
									if(gems_communication_write_encrypted_data(JAMMO_PACKET_ADMIN,cn,msg))
										cem_add_to_log("Sent.",J_LOG_INFO);
									else cem_add_to_log("Cannot send.",J_LOG_INFO);
								
									gems_clear_message(msg);
									
								}
								
								if(g_strcmp0(stdinput,"get") == 0)
								{
									gems_message* msg = gems_create_message_profile_request(GET_PROFILE_FULL);
									
									GList* iter = NULL;
									
									for(iter = g_list_first(data->clients); iter; iter = g_list_next(iter))
									{
										gems_communication_write_encrypted_data(JAMMO_PACKET_ADMIN,(gems_connection*)iter->data,msg);
									}
									gems_clear_message(msg);
								}
								
								if(g_strcmp0(stdinput,"logins") == 0)
								{
									gems_message* msg = gems_create_message_control_get_login();
									
									GList* iter = NULL;
									
									for(iter = g_list_first(data->clients); iter; iter = g_list_next(iter))
									{
										gems_communication_write_encrypted_data(JAMMO_PACKET_ADMIN,(gems_connection*)iter->data,msg);
									}
									
									gems_clear_message(msg);
								}
								
							}
							break;
					}
					FD_CLR(0,&s);
					memset(&stdinput,0,100);
				
					g_usleep(500000);
				}
			}
			else cem_add_to_log("Cannot register Teacher profile service",J_LOG_ERROR);
		}
		else cem_add_to_log("Cannot register Teacher service, quitting",J_LOG_ERROR);
	}
	else
	{
		cem_add_to_log("Cannot initialize PeerHood",J_LOG_ERROR);
	}
	if(tport != 0) ph_c_unregister_service(data->ph,TEACHER_SERVICE_NAME);
	if(pport != 0) ph_c_unregister_service(data->ph,TEACHER_PROFILE_SERVICE_NAME);
	if(data)
	{
		save_database(data->database);
		clear_data_container(data);
	}
	jammo_cem_cleanup();
	gems_security_clear_security();
	configure_release_directories();
	gems_free_data();
	
	return 0;
}
