
/*
Testing and demonstrating Logging.

This is list of LOG-levels
  J_LOG_DEBUG    Detailed information on the flow through the system
  J_LOG_INFO      Interesting runtime events (startup/shutdown)
  J_LOG_FATAL    Severe errors that cause premature termination
  J_LOG_ERROR    Other runtime errors or unexpected conditions
  J_LOG_WARN      'Almost' errors, other runtime situations that are undesirable or unexpected
  J_LOG_USER_ACTION   Everything user is doing.
  J_LOG_NETWORK  Networking info, e.g. new connection establishments, game starting etc.
  J_LOG_NETWORK_DEBUG   Networking debug

These levels are not related each others nor they are in any order.

*/

#include <cem/cem.h>
#include <stdio.h>
int main () {

//First set what kind of LOG you are interested in. e.g:
cem_set_log_level(J_LOG_DEBUG+J_LOG_INFO+J_LOG_USER_ACTION);

//Then this is catched:
printf("Catch:\n");
cem_add_to_log("debug_message",J_LOG_DEBUG);
printf("over\n\n");

//But this is not:
printf("Empty:\n");
cem_add_to_log("fatal-message",J_LOG_FATAL);
printf("over\n\n");

//Logger (who sends message to log) can send same message using more than one level.
//If one of these are under watching, message is catched. So this is catched:
printf("Catch:\n");
cem_add_to_log("debug+fatal_message",J_LOG_DEBUG+J_LOG_FATAL);
printf("over\n\n");

//Level can be changed
cem_set_log_level(J_LOG_INFO);
//Then this is not catched:
printf("Empty:\n");
cem_add_to_log("NOT-printed",J_LOG_DEBUG);
printf("over\n\n");

//Log level can appended without knowing current state:
cem_append_to_log_level(J_LOG_FATAL);

//Both are then cathed
printf("Catch one:\n");
cem_add_to_log("error_message",J_LOG_ERROR);
cem_add_to_log("fatal-message",J_LOG_FATAL);
printf("over\n\n");


//Log level can appended without knowing current state:
//appending same another time
cem_append_to_log_level(J_LOG_FATAL);

//Both are then cathed
printf("Catch one:\n");
cem_add_to_log("error_message",J_LOG_ERROR);
cem_add_to_log("fatal-message",J_LOG_FATAL);
printf("over\n\n");


//Only one row even many log-levels are matched:
cem_set_log_level(J_LOG_FATAL+J_LOG_INFO);
printf("Catch and write it only one time:\n");
cem_add_to_log("fatal+info_message",J_LOG_FATAL+J_LOG_INFO);
printf("over\n\n");

//Multirow 
printf("Multirow starts:\n");
cem_add_to_log("row1\nrow2\nrow3\ntab\tulator_message",J_LOG_INFO);
printf("Multirow ends:\n");

}

