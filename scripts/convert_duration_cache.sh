#! /bin/sh

if [ "$1" = "" ]; then
	echo "Usage: $0 <duration_cache>"
	exit 1
fi

temp=$(tempfile)
dir=$(dirname $0)
cache=$($dir/read_duration_cache.py < $1)

IFS=$(echo -n "\n\r")
for f in $cache; do
	name=$(echo $f | cut -d " " -f 1)
	size=$(echo $f | cut -d " " -f 2)
	
	# TODO: Convert
	
	$dir/write_duration_cache.py $name $size >> $temp
done

mv $temp $1
rm -f -- $temp
