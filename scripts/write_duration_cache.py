#! /usr/bin/python

import sys

if len(sys.argv) != 3:
	print "Usage:", sys.argv[0], "<filename> <length>"
else:
	length = len(sys.argv[1])
	b0 = length % 256
	b1 = int(length / 256)
	sys.stdout.write(chr(b1))
	sys.stdout.write(chr(b0))
	sys.stdout.write(sys.argv[1])

	size = int(sys.argv[2])
	b0 = int(size)
	b1 = int(b0 / 256)
	b2 = int(b1 / 256)
	b3 = int(b2 / 256)
	b4 = int(b3 / 256)
	b5 = int(b4 / 256)
	b6 = int(b5 / 256)
	b7 = int(b6 / 256)
	b0 %= 256
	b1 %= 256
	b2 %= 256
	b3 %= 256
	b4 %= 256
	b5 %= 256
	b6 %= 256
	b7 %= 256
	sys.stdout.write(chr(b7))
	sys.stdout.write(chr(b6))
	sys.stdout.write(chr(b5))
	sys.stdout.write(chr(b4))
	sys.stdout.write(chr(b3))
	sys.stdout.write(chr(b2))
	sys.stdout.write(chr(b1))
	sys.stdout.write(chr(b0))
