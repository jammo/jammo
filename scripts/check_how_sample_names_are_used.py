#!/usr/bin/python
#File 'originals_jyu_ross.txt' contains md5sum and filename (with path) for each original sample file
#made with: find . -name "*ogg" -type f -print0 | xargs -0 md5sum  > originals_jyu_ross.txt
original = open('originals_jyu_ross.txt', 'r')

#File 'current.txt' is from jammo_data/wheel_game (or from git data/jammo7-12_all/wheel_game)
#made with: find . -name "*ogg" -type f -print0 | xargs -0 md5sum > current.txt
current = open('current.txt', 'r')


#Put originals to the dictionary. key=md5sum and value=name.
o_hash_dict={}
for line in original.readlines():
	o_hash_dict[line.split(' ',2)[0]]=line.split(' ',2)[2][2:-1] #drop two first character (./) and last linebreak

#this is list of samples in jammo, which are NOT from original sets (we hope this is empty)
extra_list=[] 

#this list will contain matching pairs
result=[]
for c_line in current:
	current_hash = c_line.split(' ',2)[0]
	current_name = c_line.split(' ',2)[2][2:-1] #drop two first character (./) and last linebreak
	if current_hash in o_hash_dict:
		original_name = o_hash_dict[c_line.split(' ',2)[0]]
		result.append( (current_hash, original_name, current_name) ) #tuple
		del o_hash_dict[current_hash] #remove used, so we can check if there are anything left
	else:
		extra_list.append(current_hash, current_name)

#--Printing all results--
for i in sorted(result, key=lambda r: r[1]): #sort by original_name
	print i[0],":", i[1], "->" , i[2]

if extra_list:
	print "\nWhere are these come?"
	for i in extra_list:
		print i

if o_hash_dict:
	print "\nThese originals are not used at all in JamMo:"
	for k,v in o_hash_dict.iteritems():
		print k,v

