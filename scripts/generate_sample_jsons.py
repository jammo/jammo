#!/usr/bin/python
'''
This script will
 a) check correctness of jammo sample files
    file must contain one of those tempos _90_, _110_ or _130_
    file can contain one of those pitches _A_, _D_ or _G_

    Each file must belong group of three or group of nine

 b) Generate torso rows for json-file used on game
'''
import re

#If True, doesn't generate_code
just_check=True

def generate_code(line):
	source = line
	
	s1=re.sub("_90_","_$t_",source)
	s2=re.sub("_110_","_$t_",s1)
	s3=re.sub("_130_","_$t_",s2)
	
	s4=s3
	if re.findall("_A_", s3):
		s4=re.sub("_A_","_$p_",s3)
	elif re.findall("_D_", s3):
		s4=re.sub("_D_","_$p_",s3)
	elif re.findall("_G_", s3):
		s4=re.sub("_G_","_$p_",s3)

	destiny =s4
	return destiny


#'file_list.txt' contains list of samples.
#Made with: ls --color=never | grep --color=never -v file_list.txt | LANG=en_EN sort > file_list.txt
#NOTE: locale affects sorting
#NOTE: beware of ls and grep coloring

files = open('file_list.txt', 'r') 


converted=[]

for line in files.readlines():
	for i in line.split():
		converted.append(generate_code(i))

#this makes handling simpler, because last of the list is not handled
converted.append("last_entry")

error_list=[] #will contain faulty groups

previous=converted[0] #this stores last read entry
counter = 1 #count members of this group
for i in converted[1:]: #start from second, because first is set already
	if i!=previous: #next group starts
		if re.findall("_\$p_", previous): #if it has pitch (_$p_) it must have nine files
			if counter != 9:
				error_list.append(( counter,previous ))
		else: #if there are no pitch, there must be three files
			if counter != 3:
				error_list.append(( counter,previous ))
		if not just_check:
			print "\"{0}\",".format(previous)
		previous=i
		counter=1
	else: #belongs same group
		counter=counter+1


if error_list:
	print "\nErrors:"
	for e in error_list:
		print "#",e[0],":",e[1]
