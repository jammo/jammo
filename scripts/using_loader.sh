#These will be postinst-scripts of relevant packages
#This can be executed.
FOLDER="/home/user/MyDocs/jammo_data"

mkdir -p $FOLDER/backingtracks
mkdir -p $FOLDER/jammersounds/elec
mkdir -p $FOLDER/jammersounds/eth1
mkdir -p $FOLDER/jammersounds/gloc
mkdir -p $FOLDER/jammersounds/mari
mkdir -p $FOLDER/jammersounds/oudi
mkdir -p $FOLDER/jammersounds/xylo
mkdir -p $FOLDER/orientation_games
mkdir -p $FOLDER/virtual-instruments/drumkit
mkdir -p $FOLDER/virtual-instruments/flute
mkdir -p $FOLDER/virtual-instruments/funny_drumkit
mkdir -p $FOLDER/virtual-instruments/piano
mkdir -p $FOLDER/virtual-instruments/slider
mkdir -p $FOLDER/virtual-instruments/ud
mkdir -p $FOLDER/wheel_game
./jammo-loader jammo7-12_all.dif http://www.umsic.org/jammo/data/jammo7-12_all $FOLDER $FOLDER/jammo7_12.duration_cache

#####
FOLDER="/home/user/MyDocs/jammo_data"
mkdir -p $FOLDER/mentor_speech7_12/en_GB/mentor
./jammo-loader jammo7-12_en.dif http://www.umsic.org/jammo/data/jammo7-12_en $FOLDER/mentor_speech7_12/en_GB $FOLDER/jammo7_12.duration_cache

########
FOLDER="/home/user/MyDocs/jammo_data"
mkdir -p $FOLDER/mentor_speech7_12/de_DE/mentor
./jammo-loader jammo7-12_de.dif http://www.umsic.org/jammo/data/jammo7-12_de $FOLDER/mentor_speech7_12/de_GB $FOLDER/jammo7_12.duration_cache

########
FOLDER="/home/user/MyDocs/jammo_data"
mkdir -p $FOLDER/mentor_speech7_12/fi_FI/mentor
./jammo-loader jammo7-12_fi.dif http://www.umsic.org/jammo/data/jammo7-12_fi $FOLDER/mentor_speech7_12/fi_FI $FOLDER/jammo7_12.duration_cache

########
FOLDER="/home/user/MyDocs/jammo_data"
mkdir -p $FOLDER/mentor_speech/en_GB/mentor
./jammo-loader jammo3-6_en.dif http://www.umsic.org/jammo/data/jammo3-6_en $FOLDER/mentor_speech/en_GB $FOLDER/jammo3_6.duration_cache


########
FOLDER="/home/user/MyDocs/jammo_data"
mkdir -p $FOLDER/mentor_speech/de_DE/mentor
./jammo-loader jammo3-6_de.dif http://www.umsic.org/jammo/data/jammo3-6_de $FOLDER/mentor_speech/de_DE $FOLDER/jammo3_6.duration_cache



########
FOLDER="/home/user/MyDocs/jammo_data"
mkdir -p $FOLDER/mentor_speech/fi_FI/mentor
./jammo-loader jammo3-6_fi.dif http://www.umsic.org/jammo/data/jammo3-6_fi $FOLDER/mentor_speech/fi_FI $FOLDER/jammo3_6.duration_cache

