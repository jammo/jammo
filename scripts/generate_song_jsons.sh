#!/bin/bash
#Each song has
# *own view (and they must be listed on main.json)
# *icon on songselection

#we want skip first  position (so mentor fits)
declare -i x_place=0
declare -i y_place=0
echo Put these to songselection.json
for i in $(ls /opt/jammo/songs)
do

x_place=$(( x_place + 1 ))

if [ $x_place -gt 3 ]
then 
 x_place=0
 y_place=$(( y_place + 1 ))
fi

echo 	{
echo		\"id\": \"songselection-song-$i-action\",
echo		\"type\": \"TangleShowActorAction\",
echo		\"actor\": \"song-$i-view\",
echo		\"hide-others\": true,
echo		\"next-action\": \"songselection-select-song-action\"
echo	},
echo
echo {
echo 		\"id\": \"songselection-song-$i-texture\",
echo 		\"type\": \"TangleTexture\",
echo 		\"filename\": \"songs/$i/icon.png\"
echo 	},
echo
echo 	{
echo 		\"id\": \"songselection-song-$i\",
echo 		\"type\": \"TangleButton\",
echo 		\"x\": $((x_place * 160 +10)),
echo 		\"y\": $((y_place * 160 +5)),
echo		\"name\": \"$i\",
echo 		\"clicked-action\": \"songselection-song-$i-action\",
echo 		\"normal-background-actor\": \"songselection-song-$i-texture\"
echo 	},
echo
echo
echo
done
echo
echo
echo
echo
echo
echo


echo Put these to test.json
for i in $(ls /opt/jammo/songs)
do
echo			{
echo				\"id\": \"song-$i-view\",
echo				\"type\": \"TangleView\",
echo				\"background-actor\": \"song-$i-background\",
echo				\"filename\": \"song-$i.json\",
echo				\"show-on-set-parent\": false
echo			}
echo
done
echo
echo
echo
echo
echo
echo

echo These are also in test.json
for i in $(ls /opt/jammo/songs)
do
echo {
echo \"id\": \"song-$i-background\",
echo \"type\": \"TangleTexture\",
echo \"filename\": \"songs/$i/background.png\"
echo },
echo
done

echo
echo
echo
echo
echo
echo
#echo And these will be in own files
for i in $(ls /opt/jammo/songs)
do
echo \[ > song-$i.json
echo	{ >> song-$i.json
echo		\"id\": \"song-$i-tolistening-texture\", >> song-$i.json
echo		\"type\": \"TangleTexture\", >> song-$i.json
echo		\"filename\": \"cross_icon.png\" >> song-$i.json
echo	},>> song-$i.json
echo	{ >> song-$i.json
echo		\"id\": \"song-$i-tolistening\", >> song-$i.json
echo		\"type\": \"TangleButton\", >> song-$i.json
echo		\"x\": 719,>> song-$i.json
echo		\"y\": 16, >> song-$i.json
echo		\"clicked-action\": \"songselection-listen-song-action\", >> song-$i.json
echo		\"normal-background-actor\": \"song-$i-tolistening-texture\" >> song-$i.json
echo	} >> song-$i.json
echo \] >> song-$i.json

echo Made file song-$i.json





done



exit 0
