#ifndef _JAMMO_SLIDER_EVENT_
#define _JAMMO_SLIDER_EVENT_

#include <glib.h>
#include <gst/gst.h>
#include <string.h>

typedef enum {
  JAMMO_SLIDER_EVENT_ON,
  JAMMO_SLIDER_EVENT_OFF,
	JAMMO_SLIDER_EVENT_MOTION
} JammoSliderEventType;

typedef struct _JammoSliderEvent {
	float freq;
	GstClockTime timestamp;  //time in nanoseconds
	JammoSliderEventType type;  //JAMMO_SLIDER_EVENT_ON, JAMMO_SLIDER_EVENT_OFF or JAMMO_SLIDER_EVENT_MOTION
} JammoSliderEvent;

int jammo_slider_event_glist_to_file(GList * list, char * filename);

GList * jammo_slider_event_file_to_glist(char * filename);

void jammo_slider_event_free_glist(GList ** list);

void jammo_slider_event_store_event_to_glist(GList ** list, float freq, JammoSliderEventType type, guint64 timestamp);

int jammo_slider_event_compare_event_time (gconstpointer a,gconstpointer b);
#endif  /*_JAMMO_SLIDER_EVENT_ */
