#ifndef _JAMMO_MIDI_
#define _JAMMO_MIDI_

#include <glib.h>
#include <gst/gst.h>
//#include "../meam/jammo-instrument-track.h"
#include <string.h>

#define JAMMOMIDI_NUMBER_OF_NOTES 96

typedef enum {
  JAMMOMIDI_NOTE_ON,
  JAMMOMIDI_NOTE_OFF
} JammmoMidiEventType;



/*This is used when user asks name of note*/
// sampler uses following note names in its interface

typedef struct _JammoMidiEvent {
	unsigned char note;      //we used range 0...96
	GstClockTime timestamp;  //time in nanoseconds
	JammmoMidiEventType type;  //JAMMOMIDI_NOTE_ON or JAMMOMIDI_NOTE_OFF
} JammoMidiEvent;

const char * jammomidi_note_to_char(unsigned char note);
unsigned char jammomidi_char_to_note(char * note, char octave);

int jammomidi_glist_to_file(GList * list, char * filename);

GList* jammomidi_file_to_glist(const char* filename);

//JammoInstrumentTrack* midi_parser_load_from_file(gchar *filename);

//void midi_parser_load_to_track_from_file (JammoInstrumentTrack* instrument_track, gchar *filename);

void jammomidi_free_glist(GList ** list);

void jammomidi_store_event_to_glist(GList ** list, char note, JammmoMidiEventType type, guint64 timestamp);

int compare_event_time (gconstpointer a,gconstpointer b);
void jammomidi_print_event(JammoMidiEvent* event);
#endif  /*_JAMMO_MIDI_ */
