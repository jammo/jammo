/*
 * jammo-track.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */
 
#ifndef __JAMMO_TRACK_H__
#define __JAMMO_TRACK_H__

#include <glib.h>
#include <glib-object.h>
#include <gst/gst.h>

#define JAMMO_TYPE_TRACK (jammo_track_get_type ())
#define JAMMO_TRACK(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_TRACK, JammoTrack))
#define JAMMO_IS_TRACK(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_TRACK))
#define JAMMO_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_TRACK, JammoTrackClass))
#define JAMMO_IS_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_TRACK))
#define JAMMO_TRACK_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_TRACK, JammoTrackClass))

typedef struct _JammoTrackPrivate JammoTrackPrivate;
typedef struct _JammoTrack JammoTrack;
typedef struct _JammoTrackClass JammoTrackClass;

struct _JammoTrack {
	GInitiallyUnowned parent_instance;
};

/**
 * JammoTrackClass:
 * @get_duration: virtual function for getting the total duration of the track
 * @get_element: virtual function for getting the #GstElement of the track
 * @receive_message: virtual function for receiving a #GstMessage from the backend
 * @set_muted: virtual function for setting muting on and off
 * @latency_query: virtual function for desired latency query
 */
struct _JammoTrackClass {
	GInitiallyUnownedClass parent_class;
	
	guint64 (*get_duration)(JammoTrack* track);
	gboolean (*setup_element)(JammoTrack* track, GstPipeline* pipeline, GstBin* bin, GstElement* sink_element);
	gboolean (*receive_message)(JammoTrack* track, GstMessage* message);
	void (*set_tempo)(JammoTrack* track, guint tempo);
	void (*set_pitch)(JammoTrack* track, const gchar* pitch);
	void (*set_offline)(JammoTrack* track, gboolean offline);
	void (*set_time_signature)(JammoTrack* track, guint time_signature_beats, guint time_signature_note_value);
};

GType jammo_track_get_type(void);

#include "jammo-sequencer.h"

guint64 jammo_track_get_duration(JammoTrack* track);

#endif
