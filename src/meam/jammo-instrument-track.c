/*
 * jammo-instrument-track.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */

#include <stdio.h>
#include <stdlib.h>
#include "jammo-instrument-track.h"
#include "jammo-meam.h"
#include "jammo-meam-private.h"
#include <sys/time.h>
#include "jammo-midi.h"
#include "../configure.h" // for DATA_DIR and AUDIO_DIR


#define INSTRUMENT_FOLDER_DATA DATA_DIR "/virtual-instruments/"
#define INSTRUMENT_FOLDER_AUDIO AUDIO_DIR "/virtual-instruments/"

// default tempo for calculating tempo scale for element
// value does not matter but there has to be a reference tempo
#define DEFAULT_TEMPO 110.0

G_DEFINE_TYPE(JammoInstrumentTrack, jammo_instrument_track, JAMMO_TYPE_PLAYING_TRACK);

enum {
	PROP_0,
	PROP_INSTRUMENT_TYPE,
};

struct _JammoInstrumentTrackPrivate {
	GstElement* element;
	int instrument_type;
	gfloat tempo_scale;
	gint pitch_shift;
};


enum {
	REPORT_NOTE,
	LAST_SIGNAL
};
static guint signals[LAST_SIGNAL];


JammoInstrumentTrack* jammo_instrument_track_new(JammoInstrumentType instrument_type) {
	JammoInstrumentTrack * instrument_track = JAMMO_INSTRUMENT_TRACK(g_object_new(JAMMO_TYPE_INSTRUMENT_TRACK,"instrument-type",instrument_type,NULL));
	instrument_track->priv->element=NULL;
	return instrument_track;
}

static guint64 jammo_instrument_track_get_duration(JammoTrack* track) {
	guint64 duration = 0;

	/* TODO: Get real duration. */

	return duration;
}

static void jammo_instrument_track_set_offline(JammoTrack* track, gboolean offline) {
	JammoInstrumentTrack* instrument_track;
	
	instrument_track = JAMMO_INSTRUMENT_TRACK(track);

	jammo_instrument_track_set_live(instrument_track, !offline);
	jammo_instrument_track_set_realtime(instrument_track, !offline);
}

static void jammo_instrument_track_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoInstrumentTrack* instrument_track;
	
	instrument_track = JAMMO_INSTRUMENT_TRACK(object);

	switch (prop_id) {
		case PROP_INSTRUMENT_TYPE:
			instrument_track->priv->instrument_type = g_value_get_int(value);
			//printf("jammo_instrument_track_set_property: instrument_type=%d \n",instrument_track->priv->instrument_type);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_instrument_track_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
    JammoInstrumentTrack* instrument_track;
		instrument_track = JAMMO_INSTRUMENT_TRACK(object);

        switch (prop_id) {
		case PROP_INSTRUMENT_TYPE:
			g_value_set_int(value, instrument_track->priv->instrument_type);
			break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static void jammo_instrument_track_finalize(GObject* object) {
	G_OBJECT_CLASS(jammo_instrument_track_parent_class)->finalize(object);
}

static void jammo_instrument_track_dispose(GObject* object) {
	JammoInstrumentTrack * instrument_track = JAMMO_INSTRUMENT_TRACK(object);
	if (instrument_track->priv->element) {
		g_object_unref(instrument_track->priv->element);
		instrument_track->priv->element=NULL;
	}
	G_OBJECT_CLASS(jammo_instrument_track_parent_class)->dispose(object);
}

static gboolean jammo_instrument_track_receive_message(JammoTrack* track, GstMessage* message) {
	gboolean retvalue = FALSE;
	const GstStructure* structure;
	gint note;

	structure = gst_message_get_structure(message);
	if (structure && !strcmp(gst_structure_get_name(structure), "sampler")) {
		note = g_value_get_int(gst_structure_get_value(structure, "note"));
		//printf("jammo-instrument-track: receive_note %d\n",note);
		g_signal_emit(track, signals[REPORT_NOTE], 0, note);

		retvalue = TRUE;
	}

	return retvalue;
}

static void jammo_instrument_track_set_tempo(JammoTrack* track, guint tempo) {
	JammoInstrumentTrack* instrument_track;
	
	instrument_track = JAMMO_INSTRUMENT_TRACK(track);
	if (tempo==0) {
		instrument_track->priv->tempo_scale=1.0;
	}
	else {
		instrument_track->priv->tempo_scale=(gfloat)tempo / DEFAULT_TEMPO;
	}
	if (instrument_track->priv->element) {
		g_object_set(instrument_track->priv->element, "tempo-scale", instrument_track->priv->tempo_scale, NULL);
	}
}

static void jammo_instrument_track_set_pitch(JammoTrack* track, const gchar* pitch) {
	JammoInstrumentTrack* instrument_track;
	
	instrument_track = JAMMO_INSTRUMENT_TRACK(track);

	if (instrument_track->priv->instrument_type==JAMMO_INSTRUMENT_TYPE_DRUMKIT) {
		// pitch of drumkit does not need to be changed
		return;
	}

	if (pitch==NULL) instrument_track->priv->pitch_shift=0;
	else if (strcmp(pitch, "C")==0) instrument_track->priv->pitch_shift=0;
	else if (strcmp(pitch, "C#")==0) instrument_track->priv->pitch_shift=1;
	else if (strcmp(pitch, "D")==0) instrument_track->priv->pitch_shift=2;
	else if (strcmp(pitch, "D#")==0) instrument_track->priv->pitch_shift=3;
	else if (strcmp(pitch, "E")==0) instrument_track->priv->pitch_shift=4;
	else if (strcmp(pitch, "F")==0) instrument_track->priv->pitch_shift=5;
	else if (strcmp(pitch, "F#")==0) instrument_track->priv->pitch_shift=6;
	else if (strcmp(pitch, "G")==0) instrument_track->priv->pitch_shift=7;
	else if (strcmp(pitch, "G#")==0) instrument_track->priv->pitch_shift=8;
	else if (strcmp(pitch, "A")==0) instrument_track->priv->pitch_shift=9;
	else if (strcmp(pitch, "A#")==0) instrument_track->priv->pitch_shift=10;
	else if (strcmp(pitch, "B")==0) instrument_track->priv->pitch_shift=11;
	else instrument_track->priv->pitch_shift=0;

	if (instrument_track->priv->element) {
		g_object_set(instrument_track->priv->element, "pitch-shift", instrument_track->priv->pitch_shift, NULL);
	}
}

static GstElement* jammo_instrument_track_setup_playing_element(JammoPlayingTrack* playing_track, GstBin* bin) {
	JammoInstrumentTrack* instrument_track;

	instrument_track = JAMMO_INSTRUMENT_TRACK(playing_track);

	int instrument_nro = instrument_track->priv->instrument_type;
	//printf ("jammo_instrument_track_setup_playing_element: Type of instrument is: %d\n",instrument_nro);
	
	instrument_track->priv->element = gst_element_factory_make("jammosampler", NULL);
	// instrument track has a reference to element so refcount needs to be increased
	g_object_ref(instrument_track->priv->element);

	//We must first define folder of samples
	g_object_set(G_OBJECT(instrument_track->priv->element), "instrument-folder", INSTRUMENT_FOLDER_AUDIO, NULL);

	//Data for instrument can be different folder
	gchar* path=NULL;
	const gchar* filename;
	if (instrument_nro==JAMMO_INSTRUMENT_TYPE_FLUTE){
		filename="flute.txt";
	}
	else if (instrument_nro==JAMMO_INSTRUMENT_TYPE_DRUMKIT){
		filename="drumkit.txt";
	}
	else if (instrument_nro==JAMMO_INSTRUMENT_TYPE_UD){
		filename="ud.txt";
	}
	else if (instrument_nro==JAMMO_INSTRUMENT_TYPE_PIANO){
		filename="piano.txt";
	}
	else
		filename="flute.txt"; //Some sane default value

	path=g_strdup_printf("%s%s",INSTRUMENT_FOLDER_DATA,filename);

	g_object_set(G_OBJECT(instrument_track->priv->element), "init-instrument", path, NULL);
	g_free(path);
	g_object_set(G_OBJECT(instrument_track->priv->element), "realtime", FALSE, NULL);
	g_object_set(G_OBJECT(instrument_track->priv->element), "pitch-shift", instrument_track->priv->pitch_shift, NULL);
	g_object_set(G_OBJECT(instrument_track->priv->element), "tempo-scale", instrument_track->priv->tempo_scale, NULL);
	
	gst_bin_add(bin, instrument_track->priv->element);

	return instrument_track->priv->element;
}


static void jammo_instrument_track_class_init(JammoInstrumentTrackClass* instrument_track_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(instrument_track_class);
	JammoTrackClass* track_class = JAMMO_TRACK_CLASS(instrument_track_class);
	JammoPlayingTrackClass* playing_track_class = JAMMO_PLAYING_TRACK_CLASS(instrument_track_class);

	track_class->get_duration = jammo_instrument_track_get_duration;
	track_class->set_offline = jammo_instrument_track_set_offline;
	track_class->set_pitch = jammo_instrument_track_set_pitch;
	track_class->set_tempo = jammo_instrument_track_set_tempo;

	playing_track_class->setup_playing_element = jammo_instrument_track_setup_playing_element;

	gobject_class->finalize = jammo_instrument_track_finalize;
	gobject_class->dispose = jammo_instrument_track_dispose;
	gobject_class->set_property = jammo_instrument_track_set_property;
	gobject_class->get_property = jammo_instrument_track_get_property;

	track_class->receive_message = jammo_instrument_track_receive_message;

	/**
	 * JammoInstrumentTrack:instrument-type:
	 */
	g_object_class_install_property(gobject_class, PROP_INSTRUMENT_TYPE,
	                                g_param_spec_int("instrument-type",
	                                "Instrument type",
	                                "Specifies type of instrument",
	                                0,JAMMO_INSTRUMENT_TYPE_DUMMY-1,0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));

	signals[REPORT_NOTE] = g_signal_new("report-note", G_TYPE_FROM_CLASS(gobject_class),
	                                    G_SIGNAL_RUN_LAST, 0,
	                                    NULL, NULL,
	                                    g_cclosure_marshal_VOID__INT,
	                                    G_TYPE_NONE, 1,
	                                    G_TYPE_INT);

	g_type_class_add_private(gobject_class, sizeof(JammoInstrumentTrackPrivate));
}


void jammo_instrument_track_add_jammo_midi_event(JammoInstrumentTrack* instrument_track, JammoMidiEvent* event) {
	g_return_if_fail(JAMMO_IS_INSTRUMENT_TRACK(instrument_track));

	g_object_set(G_OBJECT(instrument_track->priv->element), "add-event", event, NULL);
}


/*
If you want put note in specific timestamp, use this function.

timestamp is in ms.
Example usage:
jammo_instrument_track_add_event(instrument_track,'e', 4, TRUE, 4081683703);
*/
void jammo_instrument_track_add_event(JammoInstrumentTrack* instrument_track, char note, JammmoMidiEventType type, GstClockTime timestamp) {
	JammoMidiEvent* new_event;

	g_return_if_fail(JAMMO_IS_INSTRUMENT_TRACK(instrument_track));
	
	new_event = malloc(sizeof(JammoMidiEvent));
	new_event->note = note;
	new_event->timestamp = timestamp;
	new_event->type = type;

	g_object_set(G_OBJECT(instrument_track->priv->element), "add-event", new_event, NULL);
}

/*This deletes always both start and end events!
*/
void jammo_instrument_track_remove_event(JammoInstrumentTrack* instrument_track, char note, GstClockTime timestamp) {
	JammoMidiEvent* new_event;

	g_return_if_fail(JAMMO_IS_INSTRUMENT_TRACK(instrument_track));
	
	new_event = malloc(sizeof(JammoMidiEvent));
	new_event->note = note;
	new_event->timestamp = timestamp;

	g_object_set(G_OBJECT(instrument_track->priv->element), "remove-event", new_event, NULL);
	g_free(new_event);
}

/*
Debug. Prints all events in this track.
*/
void jammo_instrument_track_dump_notes(JammoInstrumentTrack* instrument_track){
	GList* list;
	JammoMidiEvent* n;
	
	g_return_if_fail(JAMMO_IS_INSTRUMENT_TRACK(instrument_track));

	list = jammo_instrument_track_get_event_list(JAMMO_INSTRUMENT_TRACK(instrument_track));

	printf("Track:\n");
	for (; list; list = list->next) {
			n = (list->data);
			printf(" %llu ns: note:%d ,type: %s \n", (unsigned long long)n->timestamp,n->note, n->type==0? "start":"stop");
	}
	printf("/Track\n\n");
}


/*
If you have track in recordmode (=realtime), just call this and it will put timestamp according current 
time and starting time of track.
*/
void jammo_instrument_track_note_on_realtime(JammoInstrumentTrack* instrument_track, char note) {
	g_return_if_fail(JAMMO_IS_INSTRUMENT_TRACK(instrument_track));

	jammo_instrument_track_add_event(instrument_track, note, JAMMOMIDI_NOTE_ON, 18446744073709551615ULL); //MAX
}

//see previous
void jammo_instrument_track_note_off_realtime(JammoInstrumentTrack* instrument_track, char note) {
	g_return_if_fail(JAMMO_IS_INSTRUMENT_TRACK(instrument_track));

	jammo_instrument_track_add_event(instrument_track, note, JAMMOMIDI_NOTE_OFF, 18446744073709551615ULL);
}


static void jammo_instrument_track_init(JammoInstrumentTrack* instrument_track) {
	instrument_track->priv = G_TYPE_INSTANCE_GET_PRIVATE(instrument_track, JAMMO_TYPE_INSTRUMENT_TRACK, JammoInstrumentTrackPrivate);
}


void jammo_instrument_track_set_realtime(JammoInstrumentTrack* instrument_track, gboolean state){
	g_return_if_fail(JAMMO_IS_INSTRUMENT_TRACK(instrument_track));

	g_object_set(G_OBJECT(instrument_track->priv->element), "realtime", state, NULL);
}

void jammo_instrument_track_set_live(JammoInstrumentTrack* instrument_track, gboolean state){
	g_return_if_fail(JAMMO_IS_INSTRUMENT_TRACK(instrument_track));

	g_object_set(G_OBJECT(instrument_track->priv->element), "is-live", state, NULL);
}

GList * jammo_instrument_track_get_event_list(JammoInstrumentTrack* instrument_track) {
	GList * eventlist;

	g_return_val_if_fail(JAMMO_IS_INSTRUMENT_TRACK(instrument_track), NULL);

	g_object_get(G_OBJECT(instrument_track->priv->element),"eventlist", &eventlist, NULL);

	return eventlist;
}

void jammo_instrument_track_set_event_list(JammoInstrumentTrack* instrument_track, GList* eventlist) {
	g_return_if_fail(JAMMO_IS_INSTRUMENT_TRACK(instrument_track));

	if (instrument_track->priv->element==NULL) {
		printf("Can't add eventlist to instrument-track, maybe track is not attached to sequencer (it must be before adding events)\n");
		return;
	}
	g_object_set(G_OBJECT(instrument_track->priv->element),"eventlist", eventlist, NULL);
}


void jammo_instrument_track_set_volume(JammoInstrumentTrack* instrument_track, gfloat volume) {
	g_return_if_fail(JAMMO_IS_INSTRUMENT_TRACK(instrument_track));

	if (volume>1.0) {
		volume=1.0;
	}
	else if (volume <0.0) {
		volume=0.0;
	}
	g_object_set(G_OBJECT(instrument_track->priv->element), "volume", volume, NULL);
}


/**
 Adds all midi events from given filename to given instrument_track.
*/
void jammo_instrument_track_load_to_track_from_file(JammoInstrumentTrack* instrument_track, const gchar* filename){
        GList* list;

	g_return_if_fail(JAMMO_IS_INSTRUMENT_TRACK(instrument_track));
	
	list = jammomidi_file_to_glist(filename);
        jammo_instrument_track_set_event_list(instrument_track, list);
}

/**
	Clears event list of sampler. All recorded and added events are lost.
**/
void jammo_instrument_track_clear_events(JammoInstrumentTrack* instrument_track) {
	g_return_if_fail(JAMMO_IS_INSTRUMENT_TRACK(instrument_track));

	g_object_set(instrument_track->priv->element, "clear-events", TRUE, NULL);
}

