/*
 * jammo-gst-bin.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "jammo-pipeline.h"

G_DEFINE_TYPE(JammoPipeline, jammo_pipeline, GST_TYPE_PIPELINE);

static void jammo_pipeline_handle_message(GstBin* bin, GstMessage* message) {
	if (GST_MESSAGE_TYPE(message) == GST_MESSAGE_EOS) {
		gst_element_post_message(GST_ELEMENT_CAST(bin), gst_message_copy(message));
	}
	GST_BIN_CLASS(jammo_pipeline_parent_class)->handle_message(bin, message);
}

static void jammo_pipeline_class_init(JammoPipelineClass* klass) {
	GstBinClass* bin_class;
	
	bin_class = GST_BIN_CLASS(klass);

	bin_class->handle_message = jammo_pipeline_handle_message;
}

static void jammo_pipeline_init(JammoPipeline* bin) {
}

GstElement* jammo_pipeline_new(const gchar* name) {

	return gst_element_factory_make("jammopipeline", name);
}

