/*
 * jammo-playing-track.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#ifndef __JAMMO_PLAYING_TRACK_H__
#define __JAMMO_PLAYING_TRACK_H__

#include "jammo-track.h"
#include <gst/gst.h>

#define JAMMO_TYPE_PLAYING_TRACK (jammo_playing_track_get_type())
#define JAMMO_PLAYING_TRACK(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), JAMMO_TYPE_PLAYING_TRACK, JammoPlayingTrack))
#define JAMMO_IS_PLAYING_TRACK(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), JAMMO_TYPE_PLAYING_TRACK))
#define JAMMO_PLAYING_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), JAMMO_TYPE_PLAYING_TRACK, JammoPlayingTrackClass))
#define JAMMO_IS_PLAYING_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), JAMMO_TYPE_PLAYING_TRACK))
#define JAMMO_PLAYING_TRACK_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), JAMMO_TYPE_PLAYING_TRACK, JammoPlayingTrackClass))

typedef struct _JammoPlayingTrackPrivate JammoPlayingTrackPrivate;

typedef struct _JammoPlayingTrack {
	JammoTrack parent_instance;
	JammoPlayingTrackPrivate* priv;
} JammoPlayingTrack;

/**
 * JammoTrackClass:
 * @get_playing_element: virtual function for getting the #GstElement of the playing track
 */
typedef struct _JammoPlayingTrackClass {
	JammoTrackClass parent_class;
	
	GstElement* (*setup_playing_element)(JammoPlayingTrack* playing_track, GstBin* bin);
} JammoPlayingTrackClass;

GType jammo_playing_track_get_type(void) G_GNUC_CONST;

gboolean jammo_playing_track_get_muted(JammoPlayingTrack* playing_track);
void jammo_playing_track_set_muted(JammoPlayingTrack* playing_track, gboolean muted);
gfloat jammo_playing_track_get_volume(JammoPlayingTrack* playing_track);
void jammo_playing_track_set_volume(JammoPlayingTrack* playing_track, gfloat volume);
gfloat jammo_playing_track_get_panning(JammoPlayingTrack* playing_track);
void jammo_playing_track_set_panning(JammoPlayingTrack* playing_track, gfloat panning);
guint64 jammo_playing_track_get_echo_delay(JammoPlayingTrack* playing_track);
void jammo_playing_track_set_echo_delay(JammoPlayingTrack* playing_track, guint64 echo_delay);
gfloat jammo_playing_track_get_echo_intensity(JammoPlayingTrack* playing_track);
void jammo_playing_track_set_echo_intensity(JammoPlayingTrack* playing_track, gfloat echo_intensity);
gfloat jammo_playing_track_get_echo_feedback(JammoPlayingTrack* playing_track);
void jammo_playing_track_set_echo_feedback(JammoPlayingTrack* playing_track, gfloat echo_feedback);

gboolean jammo_playing_track_get_volume_enabled(JammoPlayingTrack* playing_track);
void jammo_playing_track_set_volume_enabled(JammoPlayingTrack* playing_track, gboolean is_enabled);
gboolean jammo_playing_track_get_panning_enabled(JammoPlayingTrack* playing_track);
void jammo_playing_track_set_panning_enabled(JammoPlayingTrack* playing_track, gboolean is_enabled);
gboolean jammo_playing_track_get_echo_enabled(JammoPlayingTrack* playing_track);
void jammo_playing_track_set_echo_enabled(JammoPlayingTrack* playing_track, gboolean is_enabled);

#endif
