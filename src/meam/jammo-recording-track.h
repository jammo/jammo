/*
 * jammo-recording-track.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */
 
#ifndef __JAMMO_RECORDING_TRACK_H__
#define __JAMMO_RECORDING_TRACK_H__

#include <glib.h>
#include <glib-object.h>
#include "jammo-track.h"

#define JAMMO_TYPE_RECORDING_TRACK (jammo_recording_track_get_type ())
#define JAMMO_RECORDING_TRACK(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_RECORDING_TRACK, JammoRecordingTrack))
#define JAMMO_IS_RECORDING_TRACK(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_RECORDING_TRACK))
#define JAMMO_RECORDING_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_RECORDING_TRACK, JammoRecordingTrackClass))
#define JAMMO_IS_RECORDING_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_RECORDING_TRACK))
#define JAMMO_RECORDING_TRACK_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_RECORDING_TRACK, JammoRecordingTrackClass))

typedef struct _JammoRecordingTrackPrivate JammoRecordingTrackPrivate;

typedef struct _JammoRecordingTrack {
	JammoTrack parent_instance;
	JammoRecordingTrackPrivate* priv;
} JammoRecordingTrack;

typedef struct _JammoRecordingTrackClass {
	JammoTrackClass parent_class;
} JammoRecordingTrackClass;

GType jammo_recording_track_get_type(void);

JammoRecordingTrack* jammo_recording_track_new(const gchar* filename);
JammoRecordingTrack* jammo_recording_track_new_with_pitch_detect(const gchar* filename);

void jammo_recording_track_set_enabled(JammoRecordingTrack* recording_track, gboolean enable);
#endif
