/*
 * jammo-metronome-track.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther <mikko.gynther@lut.fi>
 */
 
#include "jammo-metronome-track.h"
#include "jammo-meam-private.h"

G_DEFINE_TYPE(JammoMetronomeTrack, jammo_metronome_track, JAMMO_TYPE_PLAYING_TRACK);

enum {
	PROP_0
};

struct _JammoMetronomeTrackPrivate {
	GstElement* element;
	guint tempo;
};

JammoMetronomeTrack* jammo_metronome_track_new() {
	JammoMetronomeTrack * metronome_track = JAMMO_METRONOME_TRACK(g_object_new(JAMMO_TYPE_METRONOME_TRACK, NULL));
	metronome_track->priv->element=NULL;
	return metronome_track;
}

static guint64 jammo_metronome_track_get_duration(JammoTrack* track) {

	return 0;
}

static void jammo_metronome_track_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {

	switch (prop_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_metronome_track_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {

	switch (prop_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_metronome_track_set_tempo_uint(JammoTrack* track, guint tempo) {
	JammoMetronomeTrack * metronome_track = JAMMO_METRONOME_TRACK(track);
	
	metronome_track->priv->tempo = tempo;
	if (metronome_track->priv->element) {
		jammo_metronome_track_set_tempo(metronome_track, (gfloat)tempo);
	}
}

static void jammo_metronome_track_set_pitch(JammoTrack* track, const gchar* pitch) {
	// no need to do anything when pitch changes
	;
}

static void jammo_metronome_track_set_time_signature(JammoTrack* track, guint time_signature_beats, guint time_signature_note_value) {
	JammoMetronomeTrack* metronome_track;
	
	metronome_track = JAMMO_METRONOME_TRACK(track);
	g_object_set(metronome_track->priv->element, "beats", time_signature_beats, NULL);
	g_object_set(metronome_track->priv->element, "note-value", time_signature_note_value, NULL);
}

static GstElement* jammo_metronome_track_setup_playing_element(JammoPlayingTrack* playing_track, GstBin* bin) {
	JammoMetronomeTrack* metronome_track;

	metronome_track = JAMMO_METRONOME_TRACK(playing_track);

	metronome_track->priv->element = gst_element_factory_make("jammometronome", NULL);

	gst_bin_add(bin, metronome_track->priv->element);

	g_object_set(metronome_track->priv->element, "tempo", (gfloat)metronome_track->priv->tempo, NULL);

	return metronome_track->priv->element;
}


static void jammo_metronome_track_finalize(GObject* object) {
	G_OBJECT_CLASS(jammo_metronome_track_parent_class)->finalize(object);
}

static void jammo_metronome_track_dispose(GObject* object) {
	JammoMetronomeTrack * metronome_track = JAMMO_METRONOME_TRACK(object);
	if (metronome_track->priv->element) {
		g_object_unref(metronome_track->priv->element);
		metronome_track->priv->element=NULL;
	}
	G_OBJECT_CLASS(jammo_metronome_track_parent_class)->dispose(object);
}

static void jammo_metronome_track_class_init(JammoMetronomeTrackClass* metronome_track_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(metronome_track_class);
	JammoTrackClass* track_class = JAMMO_TRACK_CLASS(metronome_track_class);
	JammoPlayingTrackClass* playing_track_class = JAMMO_PLAYING_TRACK_CLASS(metronome_track_class);

	gobject_class->finalize = jammo_metronome_track_finalize;
	gobject_class->dispose = jammo_metronome_track_dispose;
	gobject_class->set_property = jammo_metronome_track_set_property;
	gobject_class->get_property = jammo_metronome_track_get_property;

	track_class->get_duration = jammo_metronome_track_get_duration;
	track_class->set_pitch = jammo_metronome_track_set_pitch;
	track_class->set_tempo = jammo_metronome_track_set_tempo_uint;
	track_class->set_time_signature = jammo_metronome_track_set_time_signature;
	playing_track_class->setup_playing_element = jammo_metronome_track_setup_playing_element;

	g_type_class_add_private(gobject_class, sizeof(JammoMetronomeTrackPrivate));
}

static void jammo_metronome_track_init(JammoMetronomeTrack* metronome_track) {
	metronome_track->priv = G_TYPE_INSTANCE_GET_PRIVATE(metronome_track, JAMMO_TYPE_METRONOME_TRACK, JammoMetronomeTrackPrivate);
}

// functions for setting and getting metronome propeties
void jammo_metronome_track_set_tempo(JammoMetronomeTrack * metronome_track, float tempo) {
	g_return_if_fail(JAMMO_IS_METRONOME_TRACK(metronome_track));

	g_object_set(metronome_track->priv->element, "tempo", tempo, NULL);
}
void jammo_metronome_track_set_time_signature_beats(JammoMetronomeTrack * metronome_track, int beats) {
	g_return_if_fail(JAMMO_IS_METRONOME_TRACK(metronome_track));

	g_object_set(metronome_track->priv->element, "beats", beats, NULL);
}

void jammo_metronome_track_set_time_signature_note_value(JammoMetronomeTrack * metronome_track, int note_value) {
	g_return_if_fail(JAMMO_IS_METRONOME_TRACK(metronome_track));

	g_object_set(metronome_track->priv->element, "note-value", note_value, NULL);
}
void jammo_metronome_track_set_accent(JammoMetronomeTrack * metronome_track, gboolean accent) {
	g_return_if_fail(JAMMO_IS_METRONOME_TRACK(metronome_track));

	g_object_set(metronome_track->priv->element, "accent", accent, NULL);
}

float jammo_metronome_track_get_tempo(JammoMetronomeTrack * metronome_track) {
	gfloat tempo;

	g_return_val_if_fail(JAMMO_IS_METRONOME_TRACK(metronome_track), 0.0);

	g_object_get(metronome_track->priv->element, "tempo", &tempo, NULL);

	return tempo;
}

int jammo_metronome_track_get_time_signature_beats(JammoMetronomeTrack * metronome_track) {
	gint beats;

	g_return_val_if_fail(JAMMO_IS_METRONOME_TRACK(metronome_track), 0);

	g_object_get(metronome_track->priv->element, "beats", &beats, NULL);

	return beats;
}

int jammo_metronome_track_get_time_signature_note_value(JammoMetronomeTrack * metronome_track) {
	gint note_value;

	g_return_val_if_fail(JAMMO_IS_METRONOME_TRACK(metronome_track), 0);

	g_object_get(metronome_track->priv->element, "note-value", &note_value, NULL);

	return note_value;
}

gboolean jammo_metronome_track_get_accent(JammoMetronomeTrack * metronome_track) {
	gboolean accent;

	g_return_val_if_fail(JAMMO_IS_METRONOME_TRACK(metronome_track), FALSE);

	g_object_get(metronome_track->priv->element, "accent", &accent, NULL);

	return accent;
}
