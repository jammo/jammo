/*
 * jammo-gst-bin.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#ifndef INCLUDED_JAMMO_PIPELINE_H
#define INCLUDED_JAMMO_PIPELINE_H

#include <gst/gst.h>

#define JAMMO_TYPE_PIPELINE (jammo_pipeline_get_type())
#define JAMMO_PIPELINE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), JAMMO_TYPE_PIPELINE, JammoPipeline))
#define JAMMO_IS_PIPELINE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), JAMMO_TYPE_PIPELINE))
#define JAMMO_PIPELINE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), JAMMO_TYPE_PIPELINE, JammoPipelineClass))
#define JAMMO_IS_PIPELINE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), JAMMO_TYPE_PIPELINE))
#define JAMMO_PIPELINE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), JAMMO_TYPE_PIPELINE, JammoPipelineClass))

typedef struct _JammoPipelinePrivate JammoPipelinePrivate;

typedef struct _JammoPipeline {
	GstPipeline parent_instance;
} JammoPipeline;

typedef struct _JammoPipelineClass {
	GstPipelineClass parent_class;
} JammoPipelineClass;

GType jammo_pipeline_get_type(void);

GstElement* jammo_pipeline_new(const gchar* name);

#endif
