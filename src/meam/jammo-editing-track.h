/*
 * jammo-editing-track.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */
 
#ifndef __JAMMO_EDITING_TRACK_H__
#define __JAMMO_EDITING_TRACK_H__

#include <glib.h>
#include <glib-object.h>
#include "jammo-playing-track.h"

#define JAMMO_TYPE_EDITING_TRACK (jammo_editing_track_get_type ())
#define JAMMO_EDITING_TRACK(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_EDITING_TRACK, JammoEditingTrack))
#define JAMMO_IS_EDITING_TRACK(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_EDITING_TRACK))
#define JAMMO_EDITING_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_EDITING_TRACK, JammoEditingTrackClass))
#define JAMMO_IS_EDITING_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_EDITING_TRACK))
#define JAMMO_EDITING_TRACK_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_EDITING_TRACK, JammoEditingTrackClass))

typedef struct _JammoEditingTrackPrivate JammoEditingTrackPrivate;

typedef struct _JammoEditingTrack {
	JammoPlayingTrack parent_instance;
	JammoEditingTrackPrivate* priv;
} JammoEditingTrack;

typedef struct _JammoEditingTrackClass {
	JammoPlayingTrackClass parent_class;
} JammoEditingTrackClass;

GType jammo_editing_track_get_type(void);

#include "jammo-sample.h"

JammoEditingTrack* jammo_editing_track_new(void);
JammoEditingTrack* jammo_editing_track_new_fixed_duration(guint64 duration);

void jammo_editing_track_add_sample(JammoEditingTrack* track, JammoSample* sample, guint64 position);
void jammo_editing_track_remove_sample(JammoEditingTrack* track, JammoSample* sample);
JammoSample* jammo_editing_track_get_sample(JammoEditingTrack* track, guint64 position);
guint64 jammo_editing_track_get_sample_position(JammoEditingTrack* track, JammoSample* sample);
guint64 jammo_editing_track_get_fixed_duration(JammoEditingTrack* track);

guint jammo_editing_track_get_tempo(JammoEditingTrack* track);
guint jammo_editing_track_get_time_signature_beats(JammoEditingTrack* editing_track);
guint jammo_editing_track_get_time_signature_note_value(JammoEditingTrack* editing_track);

#endif
