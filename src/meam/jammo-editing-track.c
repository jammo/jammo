/*
 * jammo-editing-track.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */
 
#include "jammo-editing-track.h"
#include "jammo-meam.h"
#include "jammo-meam-private.h"
#include "jammo-looper-loop.h"

#include "../configure.h"
#include <string.h>

G_DEFINE_TYPE(JammoEditingTrack, jammo_editing_track, JAMMO_TYPE_PLAYING_TRACK);

enum {
	PROP_0,
	PROP_FIXED_DURATION
};

enum {
	SAMPLE_ADDED,
	SAMPLE_REMOVED,
	LAST_SIGNAL
};

struct _JammoEditingTrackPrivate {
	guint64 fixed_duration;
	GList* samples;
	guint tempo;
	gchar* pitch;
	GstElement * looper;
	guint time_signature_beats;
	guint time_signature_note_value;
};

static void on_duration_notify(GObject* object, GParamSpec* param_spec, gpointer user_data);

static guint signals[LAST_SIGNAL] = { 0 };

JammoEditingTrack* jammo_editing_track_new() {

	return JAMMO_EDITING_TRACK(g_object_new(JAMMO_TYPE_EDITING_TRACK, NULL));
}

JammoEditingTrack* jammo_editing_track_new_fixed_duration(guint64 duration) {

	return JAMMO_EDITING_TRACK(g_object_new(JAMMO_TYPE_EDITING_TRACK, "fixed-duration", duration, NULL));
}

void jammo_editing_track_add_sample(JammoEditingTrack* editing_track, JammoSample* sample, guint64 position) {
	JammoEditingTrack* previous_track;
	GList* list;
	gboolean handled;
	JammoLooperLoop* loop = NULL;
	
	g_return_if_fail(JAMMO_IS_EDITING_TRACK(editing_track));
	
	g_object_ref_sink(sample);
	
	if ((previous_track = jammo_sample_get_editing_track(sample))) {
		jammo_editing_track_remove_sample(previous_track, sample);
	}
	
	jammo_sample_set_tempo(sample, editing_track->priv->tempo);
	jammo_sample_set_pitch(sample, editing_track->priv->pitch);
	_jammo_sample_set_position(sample, position);

	for (list = editing_track->priv->samples; list; list = list->next) {
		if (_jammo_sample_get_position(JAMMO_SAMPLE(list->data)) >= position) {
			break;
		}
	}
	
	editing_track->priv->samples = g_list_insert_before(editing_track->priv->samples, list, sample);

	/*Looper wants filename with
	  *fullpath
	  *$t $p, so it can change tempo and pitch
	*/
	loop = g_malloc0(sizeof(JammoLooperLoop));
	const gchar* file =jammo_sample_get_filename(sample);
	//if it contains already fullpath, do not add anything.
	if (strncmp(file,"/",1)==0)
		loop->filename = g_strdup_printf("%s",file);
	else
		loop->filename = g_strdup_printf("%s/%s",AUDIO_DIR,file);

	loop->start = position;
	g_object_set(editing_track->priv->looper, "add-loop", loop, NULL);
	g_free(loop->filename);
	g_free(loop);
	
	_jammo_sample_set_editing_track(sample, editing_track);
	
	g_signal_emit(editing_track, signals[SAMPLE_ADDED], 0, sample, &handled);
	
	g_signal_connect(sample, "notify::duration", G_CALLBACK(on_duration_notify), editing_track);
	g_object_notify(G_OBJECT(editing_track), "duration");
}

void jammo_editing_track_remove_sample(JammoEditingTrack* editing_track, JammoSample* sample) {
	gboolean handled;
	JammoLooperLoop * loop=NULL;
	
	g_return_if_fail(JAMMO_IS_EDITING_TRACK(editing_track));

	editing_track->priv->samples = g_list_remove(editing_track->priv->samples, sample);

	loop = g_malloc(sizeof(JammoLooperLoop));
	memset(loop, 0, sizeof(JammoLooperLoop));
	loop->filename = g_strdup(jammo_sample_get_filename(sample));
	loop->start = _jammo_sample_get_position(sample);
	g_object_set(editing_track->priv->looper, "remove-loop", loop, NULL);
	g_free(loop->filename);
	g_free(loop);

	_jammo_sample_set_editing_track(sample, NULL);

	g_signal_emit(editing_track, signals[SAMPLE_REMOVED], 0, sample, &handled);

	g_object_unref(sample);
}

JammoSample* jammo_editing_track_get_sample(JammoEditingTrack* editing_track, guint64 position) {
	JammoSample* sample = NULL;
	guint64 p;
	GList* list;
	JammoSample* s;
	guint64 duration;
	
	g_return_val_if_fail(JAMMO_IS_EDITING_TRACK(editing_track), NULL);
	
	p = 0;
	for (list = editing_track->priv->samples; list; list = list->next) {
		s = JAMMO_SAMPLE(list->data);
		duration = jammo_sample_get_duration(s);
		if (p + duration >= position) {
			sample = s;
			break;
		}
		p += duration;
	}
	
	return sample;
}

guint64 jammo_track_get_sample_position(JammoEditingTrack* editing_track, JammoSample* sample) {
	g_return_val_if_fail(JAMMO_IS_EDITING_TRACK(editing_track), 0);
	g_return_val_if_fail(jammo_sample_get_editing_track(sample) == editing_track, 0);
	
	return _jammo_sample_get_position(sample);
}

guint64 jammo_editing_track_get_fixed_duration(JammoEditingTrack* track) {
	g_return_val_if_fail(JAMMO_IS_EDITING_TRACK(track), 0);

	return track->priv->fixed_duration;
}

static guint64 jammo_editing_track_get_duration(JammoTrack* track) {
	guint64 duration = 0;
	JammoEditingTrack* editing_track;
	GList* list;
	guint64 d;
	
	editing_track = JAMMO_EDITING_TRACK(track);
	
	if (editing_track->priv->fixed_duration != JAMMO_DURATION_INVALID) {
		duration = editing_track->priv->fixed_duration;
	} else {
		for (list = editing_track->priv->samples; list; list = list->next) {
			if ((d = jammo_sample_get_duration(JAMMO_SAMPLE(list->data))) == JAMMO_DURATION_INVALID) {
				duration = JAMMO_DURATION_INVALID;
				break;
			}
			duration += d;
		}
	}

	return duration;
}

guint jammo_editing_track_get_tempo(JammoEditingTrack* editing_track) {
	g_return_val_if_fail(JAMMO_IS_EDITING_TRACK(editing_track), 0);

	return editing_track->priv->tempo;
}

static void jammo_editing_track_set_tempo(JammoTrack* track, guint tempo) {
	JammoEditingTrack* editing_track;
	GList* list;
	JammoSample* sample;
	GstElement* looper;
	
	editing_track = JAMMO_EDITING_TRACK(track);
	if (editing_track->priv->tempo != tempo) {
		looper=editing_track->priv->looper;
		if (looper) //it is NULL when we are initializing track
			g_object_set(looper, "tempo", tempo, NULL);
		for (list = editing_track->priv->samples; list; list = list->next) {
			sample = JAMMO_SAMPLE(list->data);
			jammo_sample_set_tempo(sample, tempo);
		}
	
		editing_track->priv->tempo = tempo;
	}
}

static void jammo_editing_track_set_pitch(JammoTrack* track, const gchar* pitch) {
	JammoEditingTrack* editing_track;
	GstElement* looper;
	GList* list;
	
	editing_track = JAMMO_EDITING_TRACK(track);
	
	if (g_strcmp0(editing_track->priv->pitch, pitch)) {
		g_free(editing_track->priv->pitch);
		editing_track->priv->pitch = g_strdup(pitch);
		looper=editing_track->priv->looper;
		if (looper) //it is NULL when we are initializing track
			g_object_set(looper, "pitch", pitch, NULL);
		for (list = editing_track->priv->samples; list; list = list->next) {
			jammo_sample_set_pitch(JAMMO_SAMPLE(list->data), pitch);
		}
	}
}

guint jammo_editing_track_get_time_signature_beats(JammoEditingTrack* editing_track) {
	g_return_val_if_fail(JAMMO_IS_EDITING_TRACK(editing_track), 0);

	return editing_track->priv->time_signature_beats;
}

guint jammo_editing_track_get_time_signature_note_value(JammoEditingTrack* editing_track) {
	g_return_val_if_fail(JAMMO_IS_EDITING_TRACK(editing_track), 0);

	return editing_track->priv->time_signature_note_value;
}

// setting time signature is not possible if there are samples on the track
static void jammo_editing_track_set_time_signature(JammoTrack* track, guint time_signature_beats, guint time_signature_note_value) {
	JammoEditingTrack* editing_track;
	
	editing_track = JAMMO_EDITING_TRACK(track);
	if (g_list_length(editing_track->priv->samples)!=0) {
		g_warning("Can not change time signature. There are samples on editing track");
	}
	else {
		editing_track->priv->time_signature_beats=time_signature_beats;
		editing_track->priv->time_signature_note_value=time_signature_note_value;
	}
}

static GstElement* jammo_editing_track_setup_playing_element(JammoPlayingTrack* playing_track, GstBin* bin) {
	//printf("\n*** jammo_editing_track_setup_playing_element **\n");
	JammoEditingTrack* editing_track;
	gint64 duration;
	
	editing_track = JAMMO_EDITING_TRACK(playing_track);

	editing_track->priv->looper = gst_element_factory_make("jammolooper", NULL);
	gst_bin_add_many(bin, editing_track->priv->looper, NULL);

	g_object_set(editing_track->priv->looper, "pitch", editing_track->priv->pitch, NULL);
	g_object_set(editing_track->priv->looper, "tempo", editing_track->priv->tempo, NULL);
	
	if (editing_track->priv->fixed_duration != JAMMO_DURATION_INVALID) {
		duration = editing_track->priv->fixed_duration;
	} else {
		duration = 0.1 * GST_SECOND;
	}

	return editing_track->priv->looper;
}

static void jammo_editing_track_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoEditingTrack* track;
	
	track = JAMMO_EDITING_TRACK(object);

	switch (prop_id) {
		case PROP_FIXED_DURATION:
			track->priv->fixed_duration = g_value_get_uint64(value);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_editing_track_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoEditingTrack* track;

	track = JAMMO_EDITING_TRACK(object);

        switch (prop_id) {
		case PROP_FIXED_DURATION:
			g_value_set_uint64(value, track->priv->fixed_duration);
			break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}
static void jammo_editing_track_finalize(GObject* object) {
	JammoEditingTrack* editing_track;

	editing_track = JAMMO_EDITING_TRACK(object);

	g_free(editing_track->priv->pitch);

	G_OBJECT_CLASS(jammo_editing_track_parent_class)->finalize(object);
}

static void jammo_editing_track_dispose(GObject* object) {
	JammoEditingTrack* editing_track;

	editing_track = JAMMO_EDITING_TRACK(object);

	g_list_foreach(editing_track->priv->samples, (GFunc)_jammo_sample_set_editing_track, NULL);
	g_list_foreach(editing_track->priv->samples, (GFunc)g_object_unref, NULL);
	g_list_free(editing_track->priv->samples);
	editing_track->priv->samples = NULL;

	G_OBJECT_CLASS(jammo_editing_track_parent_class)->dispose(object);
}

static void jammo_editing_track_class_init(JammoEditingTrackClass* editing_track_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(editing_track_class);
	JammoTrackClass* track_class = JAMMO_TRACK_CLASS(editing_track_class);
	JammoPlayingTrackClass* playing_track_class = JAMMO_PLAYING_TRACK_CLASS(editing_track_class);

	track_class->get_duration = jammo_editing_track_get_duration;
	track_class->set_tempo = jammo_editing_track_set_tempo;
	track_class->set_pitch = jammo_editing_track_set_pitch;
	track_class->set_time_signature = jammo_editing_track_set_time_signature;
	playing_track_class->setup_playing_element = jammo_editing_track_setup_playing_element;
	gobject_class->finalize = jammo_editing_track_finalize;
	gobject_class->dispose = jammo_editing_track_dispose;
	gobject_class->set_property = jammo_editing_track_set_property;
	gobject_class->get_property = jammo_editing_track_get_property;

	/**
	 * JammoSequencer:fixed-duration:
	 *
	 * The fixed duration of the track or JAMMO_DURATION_INVALID if the track duration is dynamic.
	 */
	g_object_class_install_property(gobject_class, PROP_FIXED_DURATION,
	                                g_param_spec_uint64("fixed-duration",
	                                "Fixed duration",
	                                "The fixed duration of the track or JAMMO_DURATION_INVALID if the track duration is dynamic",
	                                0, G_MAXUINT64, JAMMO_DURATION_INVALID,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	/**
	 * JammoEditingTrack:sample-added:
	 * @editing_track: the object which received the signal
	 */
	signals[SAMPLE_ADDED] = g_signal_new("sample-added", G_TYPE_FROM_CLASS(gobject_class),
	                                     G_SIGNAL_RUN_LAST, 0,
					     NULL, NULL,
					     g_cclosure_marshal_VOID__OBJECT,
					     G_TYPE_NONE, 1,
					     JAMMO_TYPE_SAMPLE);
	/**
	 * JammoEditingTrack:sample-added:
	 * @editing_track: the object which received the signal
	 */
	signals[SAMPLE_REMOVED] = g_signal_new("sample-removed", G_TYPE_FROM_CLASS(gobject_class),
	                                       G_SIGNAL_RUN_LAST, 0,
					       NULL, NULL,
					       g_cclosure_marshal_VOID__OBJECT,
					       G_TYPE_NONE, 1,
					       JAMMO_TYPE_SAMPLE);

	g_type_class_add_private(gobject_class, sizeof(JammoEditingTrackPrivate));
}

static void jammo_editing_track_init(JammoEditingTrack* editing_track) {
	editing_track->priv = G_TYPE_INSTANCE_GET_PRIVATE(editing_track, JAMMO_TYPE_EDITING_TRACK, JammoEditingTrackPrivate);
	editing_track->priv->tempo = 110;
	editing_track->priv->time_signature_beats=4;
	editing_track->priv->time_signature_note_value=4;
}

static void on_duration_notify(GObject* object, GParamSpec* param_spec, gpointer user_data) {
	g_object_notify(G_OBJECT(user_data), "duration");
}
