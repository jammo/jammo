/*
 * jammo-slider-track.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */

#include <stdio.h>
#include <stdlib.h>
#include "jammo-slider-track.h"
#include "jammo-meam.h"
#include "jammo-meam-private.h"
#include <sys/time.h>

// default tempo for calculating tempo scale for element
// value does not matter but there has to be a reference tempo
#define DEFAULT_TEMPO 110.0

G_DEFINE_TYPE(JammoSliderTrack, jammo_slider_track, JAMMO_TYPE_PLAYING_TRACK);

enum {
	PROP_0,
	PROP_SLIDER_TYPE,
};

struct _JammoSliderTrackPrivate {
	GstElement* slider;
	GstElement* filter;
	int slider_type;
	gfloat tempo_scale;
	gint pitch_shift;
	struct timeval creation_time; //Not used yet
};

enum {
	REPORT_CURRENT_FREQUENCY,
	LAST_SIGNAL
};
static guint signals[LAST_SIGNAL];


JammoSliderTrack* jammo_slider_track_new(JammoSliderType slider_type) {
	JammoSliderTrack * slider_track = JAMMO_SLIDER_TRACK(g_object_new(JAMMO_TYPE_SLIDER_TRACK,"slider-type",slider_type,NULL));
	slider_track->priv->slider=NULL;
	return slider_track;
}

static guint64 jammo_slider_track_get_duration(JammoTrack* track) {
	guint64 duration = 0;

	/* TODO: Get real duration. */

	return duration;
}

static void jammo_slider_track_set_offline(JammoTrack* track, gboolean offline) {
	JammoSliderTrack* slider_track;

	slider_track = JAMMO_SLIDER_TRACK(track);

	jammo_slider_track_set_live(slider_track, !offline);
	jammo_slider_track_set_recording(slider_track, !offline);
}

static void jammo_slider_track_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoSliderTrack* slider_track;
	
	slider_track = JAMMO_SLIDER_TRACK(object);

	switch (prop_id) {
		case PROP_SLIDER_TYPE:
			slider_track->priv->slider_type = g_value_get_int(value);
			//printf("jammo_slider_track_set_property: slider_type=%d \n",slider_track->priv->slider_type);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_slider_track_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
    JammoSliderTrack* slider_track;
		slider_track = JAMMO_SLIDER_TRACK(object);

        switch (prop_id) {
					case PROP_SLIDER_TYPE:
						g_value_set_int(value, slider_track->priv->slider_type);
						break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static void jammo_slider_track_finalize(GObject* object) {
	G_OBJECT_CLASS(jammo_slider_track_parent_class)->finalize(object);
}

static void jammo_slider_track_dispose(GObject* object) {
	JammoSliderTrack* slider_track;

	slider_track = JAMMO_SLIDER_TRACK(object);

	if (slider_track->priv->slider) {
		g_object_unref(slider_track->priv->slider);
		slider_track->priv->slider=NULL;
	}
	if (slider_track->priv->filter) {
		g_object_unref(slider_track->priv->filter);
		slider_track->priv->filter=NULL;
	}

	G_OBJECT_CLASS(jammo_slider_track_parent_class)->dispose(object);
}

static gboolean jammo_slider_track_receive_message(JammoTrack* track, GstMessage* message) {
	gboolean retvalue = FALSE;
	const GstStructure* structure;
	gfloat frequency;

	structure = gst_message_get_structure(message);
	if (structure && !strcmp(gst_structure_get_name(structure), "slider")) {
		frequency = g_value_get_float(gst_structure_get_value(structure, "frequency"));
		g_signal_emit(track, signals[REPORT_CURRENT_FREQUENCY], 0, frequency);

		retvalue = TRUE;
	}
	
	return retvalue;
}

static void jammo_slider_track_set_tempo(JammoTrack* track, guint tempo) {
	JammoSliderTrack* slider_track;
	
	slider_track = JAMMO_SLIDER_TRACK(track);

	if (tempo==0) {
		slider_track->priv->tempo_scale=1.0;
	}
	else {
		slider_track->priv->tempo_scale=(gfloat)tempo / DEFAULT_TEMPO;
	}
	if (slider_track->priv->slider) {
		g_object_set(slider_track->priv->slider, "tempo-scale", slider_track->priv->tempo_scale, NULL);
	}
	
}

static void jammo_slider_track_set_pitch(JammoTrack* track, const gchar* pitch) {
	JammoSliderTrack* slider_track;
	
	slider_track = JAMMO_SLIDER_TRACK(track);

	if (pitch==NULL) slider_track->priv->pitch_shift=0;
	else if (strcmp(pitch, "C")==0) slider_track->priv->pitch_shift=0;
	else if (strcmp(pitch, "C#")==0) slider_track->priv->pitch_shift=1;
	else if (strcmp(pitch, "D")==0) slider_track->priv->pitch_shift=2;
	else if (strcmp(pitch, "D#")==0) slider_track->priv->pitch_shift=3;
	else if (strcmp(pitch, "E")==0) slider_track->priv->pitch_shift=4;
	else if (strcmp(pitch, "F")==0) slider_track->priv->pitch_shift=5;
	else if (strcmp(pitch, "F#")==0) slider_track->priv->pitch_shift=6;
	else if (strcmp(pitch, "G")==0) slider_track->priv->pitch_shift=7;
	else if (strcmp(pitch, "G#")==0) slider_track->priv->pitch_shift=8;
	else if (strcmp(pitch, "A")==0) slider_track->priv->pitch_shift=9;
	else if (strcmp(pitch, "A#")==0) slider_track->priv->pitch_shift=10;
	else if (strcmp(pitch, "B")==0) slider_track->priv->pitch_shift=11;
	else slider_track->priv->pitch_shift=0;

	if (slider_track->priv->slider) {
		g_object_set(slider_track->priv->slider, "pitch-shift", slider_track->priv->pitch_shift, NULL);
	}
}

static GstElement* jammo_slider_track_setup_playing_element(JammoPlayingTrack* playing_track, GstBin* bin) {
	JammoSliderTrack* slider_track;

	slider_track = JAMMO_SLIDER_TRACK(playing_track);

	int slider_nro = slider_track->priv->slider_type;
	//printf ("jammo_slider_track_setup_playing_element: slider_type is: %d\n",slider_nro);
	
	slider_track->priv->slider = gst_element_factory_make("jammoslider", NULL);
	// slider track has a reference to slider
	g_object_ref(slider_track->priv->slider);
	slider_track->priv->filter = gst_element_factory_make("audiocheblimit", NULL);
	// slider track has a reference to filter
	g_object_ref(slider_track->priv->filter);
	g_object_set(G_OBJECT(slider_track->priv->slider), "instrument", slider_nro, NULL);

	if (slider_nro == JAMMO_SLIDER_TYPE_ORGAN) {
			g_object_set(G_OBJECT(slider_track->priv->slider), "attack", 0.05, NULL);
			g_object_set(G_OBJECT(slider_track->priv->slider), "decay", 0.05, NULL);
			g_object_set(G_OBJECT(slider_track->priv->slider), "sustain", 1.0, NULL);
			g_object_set(G_OBJECT(slider_track->priv->slider), "release", 0.15, NULL);
			// filter is low-pass by default
			g_object_set(slider_track->priv->filter, "cutoff", 10000.0, NULL);
	}

	else if (slider_nro == JAMMO_SLIDER_TYPE_KARPLUS) {
			g_object_set(G_OBJECT(slider_track->priv->slider), "attack", 0.002, NULL);
			g_object_set(G_OBJECT(slider_track->priv->slider), "decay", 0.002, NULL);
			g_object_set(G_OBJECT(slider_track->priv->slider), "sustain", 1.0, NULL);
			g_object_set(G_OBJECT(slider_track->priv->slider), "release", 0.002, NULL);
			// filter is low-pass by default
			g_object_set(slider_track->priv->filter, "cutoff", 10000.0, NULL);
	}
	
	else if (slider_nro == JAMMO_SLIDER_TYPE_FM_MODULATION) {
			g_object_set(G_OBJECT(slider_track->priv->slider), "attack", 0.34, NULL);
			g_object_set(G_OBJECT(slider_track->priv->slider), "decay", 0.045, NULL);
			g_object_set(G_OBJECT(slider_track->priv->slider), "sustain", 0.8, NULL);
			g_object_set(G_OBJECT(slider_track->priv->slider), "release", 0.11, NULL);
			// filter low-pass by default
			g_object_set(slider_track->priv->filter, "cutoff", 1000.0, NULL);
	}
	
	g_object_set(G_OBJECT(slider_track->priv->slider), "recording", FALSE, NULL);
	g_object_set(G_OBJECT(slider_track->priv->slider), "pitch-shift", slider_track->priv->pitch_shift, NULL);
	g_object_set(G_OBJECT(slider_track->priv->slider), "tempo-scale", slider_track->priv->tempo_scale, NULL);
	
	gst_bin_add_many(bin, slider_track->priv->slider, slider_track->priv->filter, NULL);
	gst_element_link(slider_track->priv->slider, slider_track->priv->filter);
	
	gettimeofday(&slider_track->priv->creation_time, NULL);

	return slider_track->priv->filter;
}

static void jammo_slider_track_class_init(JammoSliderTrackClass* slider_track_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(slider_track_class);
	JammoTrackClass* track_class = JAMMO_TRACK_CLASS(slider_track_class);
	JammoPlayingTrackClass* playing_track_class = JAMMO_PLAYING_TRACK_CLASS(slider_track_class);

	track_class->get_duration = jammo_slider_track_get_duration;
	track_class->set_offline = jammo_slider_track_set_offline;
	track_class->set_pitch = jammo_slider_track_set_pitch;
	track_class->set_tempo = jammo_slider_track_set_tempo;

	playing_track_class->setup_playing_element = jammo_slider_track_setup_playing_element;

	gobject_class->finalize = jammo_slider_track_finalize;
	gobject_class->dispose = jammo_slider_track_dispose;
	gobject_class->set_property = jammo_slider_track_set_property;
	gobject_class->get_property = jammo_slider_track_get_property;

	track_class->receive_message = jammo_slider_track_receive_message;

	/**
	 * JammoSliderTrack:slider-type:
	 */
	g_object_class_install_property(gobject_class, PROP_SLIDER_TYPE,
	                                g_param_spec_int("slider-type",
	                                "Slider type",
	                                "Specifies type of slider",
	                                0,JAMMO_SLIDER_TYPE_DUMMY-1,0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));

	signals[REPORT_CURRENT_FREQUENCY] = g_signal_new("report-current-frequency", G_TYPE_FROM_CLASS(gobject_class),
	                                    G_SIGNAL_RUN_LAST, 0,
	                                    NULL, NULL,
	                                    g_cclosure_marshal_VOID__FLOAT,
	                                    G_TYPE_NONE, 1,
	                                    G_TYPE_FLOAT);

	g_type_class_add_private(gobject_class, sizeof(JammoSliderTrackPrivate));
}

void jammo_slider_track_add_event(JammoSliderTrack* slider_track,float freq, JammoSliderEventType type, GstClockTime timestamp) {
	JammoSliderEvent* new_event = malloc(sizeof(JammoSliderEvent));
	new_event->freq = freq;
	new_event->timestamp = timestamp;
	new_event->type = type;

	g_return_if_fail(JAMMO_IS_SLIDER_TRACK(slider_track));

	g_object_set(G_OBJECT(slider_track->priv->slider), "add-event", new_event, NULL);
	g_free(new_event);
}

void jammo_slider_track_remove_event(JammoSliderTrack* slider_track,float freq, JammoSliderEventType type, GstClockTime timestamp) {
	JammoSliderEvent* new_event = malloc(sizeof(JammoSliderEvent));
	new_event->freq = freq;
	new_event->type = type;
	new_event->timestamp = timestamp;

	g_return_if_fail(JAMMO_IS_SLIDER_TRACK(slider_track));

	g_object_set(G_OBJECT(slider_track->priv->slider), "remove-event", new_event, NULL);
	g_free(new_event);
}

void jammo_slider_track_set_frequency_realtime(JammoSliderTrack* slider_track,float frequency){
	g_return_if_fail(JAMMO_IS_SLIDER_TRACK(slider_track));

	jammo_slider_track_add_event(slider_track, frequency, JAMMO_SLIDER_EVENT_MOTION, 18446744073709551615ULL); //MAX
}

void jammo_slider_track_set_on_realtime(JammoSliderTrack* slider_track, float frequency){
	g_return_if_fail(JAMMO_IS_SLIDER_TRACK(slider_track));

	jammo_slider_track_add_event(slider_track, frequency, JAMMO_SLIDER_EVENT_ON, 18446744073709551615ULL); //MAX
}

void jammo_slider_track_set_off_realtime(JammoSliderTrack* slider_track, float frequency){
	g_return_if_fail(JAMMO_IS_SLIDER_TRACK(slider_track));

	jammo_slider_track_add_event(slider_track, frequency, JAMMO_SLIDER_EVENT_OFF, 18446744073709551615ULL); //MAX
}

static void jammo_slider_track_init(JammoSliderTrack* slider_track) {
	slider_track->priv = G_TYPE_INSTANCE_GET_PRIVATE(slider_track, JAMMO_TYPE_SLIDER_TRACK, JammoSliderTrackPrivate);
}

GList * jammo_slider_track_get_event_list(JammoSliderTrack* slider_track) {
	GList * eventlist;

	g_return_val_if_fail(JAMMO_IS_SLIDER_TRACK(slider_track), NULL);

	g_object_get(G_OBJECT(slider_track->priv->slider),"eventlist", &eventlist, NULL);

	return eventlist;
}

void jammo_slider_track_set_event_list(JammoSliderTrack* slider_track, GList* eventlist) {
	g_return_if_fail(JAMMO_IS_SLIDER_TRACK(slider_track));

	g_object_set(G_OBJECT(slider_track->priv->slider),"eventlist", eventlist, NULL);
}

void jammo_slider_track_set_recording(JammoSliderTrack* slider_track, gboolean recording) {
	g_return_if_fail(JAMMO_IS_SLIDER_TRACK(slider_track));

	g_object_set(G_OBJECT(slider_track->priv->slider),"recording", recording, NULL);
}

void jammo_slider_track_set_live(JammoSliderTrack* slider_track, gboolean state){
	g_return_if_fail(JAMMO_IS_SLIDER_TRACK(slider_track));

	g_object_set(G_OBJECT(slider_track->priv->slider), "is-live", state, NULL);
}

/*
Debug. Prints all events in this track.
*/
void jammo_slider_track_dump_events(JammoSliderTrack* slider_track){
	GList* list= jammo_slider_track_get_event_list(JAMMO_SLIDER_TRACK(slider_track));
	JammoSliderEvent* n;

	g_return_if_fail(JAMMO_IS_SLIDER_TRACK(slider_track));

	printf("Track:\n");
	for (; list; list = list->next) {
			n = (list->data);
			printf(" %llu ns: freq:%f ,type: %d \n", (unsigned long long)n->timestamp,n->freq, n->type);
	}
	printf("/Track\n\n");
}

/**
	Clears event list of sampler. All recorded and added events are lost.
**/
void jammo_slider_track_clear_events(JammoSliderTrack* slider_track) {
	g_return_if_fail(JAMMO_IS_SLIDER_TRACK(slider_track));

	g_object_set(slider_track->priv->slider, "clear-events", TRUE, NULL);
}
