/*
 * jammo-metronome-track.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther <mikko.gynther@lut.fi>
 */
 
#ifndef __JAMMO_METRONOME_TRACK_H__
#define __JAMMO_METRONOME_TRACK_H__

#include <glib.h>
#include <glib-object.h>
#include "jammo-playing-track.h"

#define JAMMO_TYPE_METRONOME_TRACK (jammo_metronome_track_get_type ())
#define JAMMO_METRONOME_TRACK(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_METRONOME_TRACK, JammoMetronomeTrack))
#define JAMMO_IS_METRONOME_TRACK(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_METRONOME_TRACK))
#define JAMMO_METRONOME_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_METRONOME_TRACK, JammoMetronomeTrackClass))
#define JAMMO_IS_METRONOME_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_METRONOME_TRACK))
#define JAMMO_METRONOME_TRACK_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_METRONOME_TRACK, JammoMetronomeTrackClass))

typedef struct _JammoMetronomeTrackPrivate JammoMetronomeTrackPrivate;

typedef struct _JammoMetronomeTrack {
	JammoPlayingTrack parent_instance;
	JammoMetronomeTrackPrivate* priv;
} JammoMetronomeTrack;

typedef struct _JammoMetronomeTrackClass {
	JammoPlayingTrackClass parent_class;
} JammoMetronomeTrackClass;

GType jammo_metronome_track_get_type(void);

JammoMetronomeTrack* jammo_metronome_track_new();

void jammo_metronome_track_set_tempo(JammoMetronomeTrack * metronome_track, float tempo);
void jammo_metronome_track_set_time_signature_beats(JammoMetronomeTrack * metronome_track, int beats);
void jammo_metronome_track_set_time_signature_note_value(JammoMetronomeTrack * metronome_track, int note_value);
void jammo_metronome_track_set_accent(JammoMetronomeTrack * metronome_track, gboolean accent);
float jammo_metronome_track_get_tempo(JammoMetronomeTrack * metronome_track);
int jammo_metronome_track_get_time_signature_beats(JammoMetronomeTrack * metronome_track);
int jammo_metronome_track_get_time_signature_note_value(JammoMetronomeTrack * metronome_track);
gboolean jammo_metronome_track_get_accent(JammoMetronomeTrack * metronome_track);

#endif
