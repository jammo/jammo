#include <stdio.h>
#include <stdlib.h>
#include "jammo-loop.h"

/**
Writes given GList (containing loop id & slot pairs) to given filename.
*/
gint jammo_loop_glist_to_file(GList * list, char * filename) {
	FILE * fp;

	fp = fopen(filename, "w");
	if (fp==NULL) {
		return -1;
	}

	GList * temp;
	guint32 loop_id, slot;

	for (temp=list;temp;temp=temp->next) {
		loop_id=((JammoLoop*)(temp->data))->loop_id;
		slot=((JammoLoop*)(temp->data))->slot;
		fprintf(fp, "%u%u\n", loop_id, slot);
	}
	fclose(fp);

	return 0;
}

GList * jammo_loop_file_to_glist(char * filename) {
	FILE * fp;
	GList * list;

	list=NULL;
	if (filename==NULL){
		return NULL;
	}

	fp = fopen(filename, "r");
	if (fp==NULL) {
		return NULL;
	}

	guint32 loop_id, slot;

	while(fscanf(fp, "%u%u\n", &loop_id, &slot) != EOF) {
		JammoLoop * loop;
		loop=malloc(sizeof(JammoLoop));
		loop->loop_id=loop_id;
		loop->slot=slot;

		printf("Loop from file:'%u' loop_id, '%u' slot\n", loop->loop_id, loop->slot);
		list = g_list_prepend(list, loop);
	}
	list = g_list_reverse(list);
	fclose(fp);

	return list;
}


// a function for freeing jammo_loop GList
// memory allocated in creating the list is not freed automatically
void jammo_loop_free_glist(GList ** list) {
	int i;
  for (i=0;i<g_list_length(*list);i++) {
		//JammoLoop * stored_loop;
		gpointer data = g_list_nth_data(*list, i);
		if (data!=NULL) {
			//stored_loop=(JammoLoop *)data;
			free(data);
		}
		data=NULL;
	}
	g_list_free(*list);
	*list=NULL;
}

// a function for storing midi events in a GList
void jammo_loop_store_loop_to_glist(GList ** list, guint32 loop_id, guint32 slot) {
	JammoLoop * loop;
	loop=malloc(sizeof(JammoLoop));
	loop->loop_id=loop_id;
	loop->slot=slot;
	*list = g_list_append(*list ,loop);
}

//For debug purpose
void jammo_loop_print_event(JammoLoop* loop) {
	printf("Jammo-Loop:loop_id:%u slot: %u\n", loop->loop_id, loop->slot);
}

