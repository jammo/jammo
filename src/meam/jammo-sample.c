/*
 * jammo-sample.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "jammo-sample.h"
#include "jammo-track.h"
#include "jammo-meam.h"
#include "jammo-meam-private.h"
#include <string.h>
#include <tangle.h>

#include "../configure.h"
#include "../cem/cem.h"

enum {
	PROP_0,
	PROP_FILENAME,
	PROP_REAL_FILENAME,
	PROP_DURATION,
	PROP_TEMPO,
	PROP_PITCH,
	PROP_SEQUENCER
};

enum {
	STOPPED,
	LAST_SIGNAL
};

struct _JammoSamplePrivate {
	gchar* filename;
	gchar* real_filename;
	guint64 duration;
	guint tempo;
	gchar* pitch;
	JammoSequencer* sequencer;
	JammoEditingTrack* editing_track;
	guint64 start;
};

typedef struct {
	gboolean preroll_only;
	JammoSample* sample;
	GstElement* pipeline;
	GstElement* sink;
} PlayData;

/**
 * SECTION:jammo-sample
 * @Short_description: A sampled audio loop
 * @Title: JammoSample
 *
 * A #JammoSample is a piece of audio material
 * that is read from a file. It can be
 * put on a #JammoEditingTrack.
 *
 * A new #JammoSample has floating reference after instantiation.
 * The reference will be sunk by a #JammoTrack. Thus,
 * an application does not need to handle reference counting for
 * the #JammoSample object, if (and only if) it used as part
 * of a track and a sequencer.
 *
 * However, if a #JammoSample is used alone to play only
 * one audio file, an application must free resources
 * by unreferencing it with g_object_unref() after usage.
 */

G_DEFINE_TYPE(JammoSample, jammo_sample, G_TYPE_INITIALLY_UNOWNED);

static void play_sample(JammoSample* sample, gboolean preroll_only);
static void stop_and_free_play_data(PlayData* play_data);
static void initialize_state(JammoSample* sample);
static void on_sequencer_notify(JammoSample* sample);

static guint signals[LAST_SIGNAL] = { 0 };
static GList* play_datas = NULL;

/**
 * jammo_sample_new_from_file:
 * @filename: the name of a file containing sample data
 *
 * Instantiates a new #JammoSample with given audio data.
 *
 * Return value: a new #JammoSample.
 */
JammoSample* jammo_sample_new_from_file(const gchar* filename) {

	return JAMMO_SAMPLE(g_object_new(JAMMO_TYPE_SAMPLE, "filename", filename, NULL));
}

/**
 * jammo_sample_new_from_file:
 * @filename: the name of a file containing sample data
 * @sequencer: a sequencer that is used to track tempo and pitch
 *
 * Instantiates a new #JammoSample with given audio data.
 *
 * The #JammoSequencer is used to track tempo and pitch until
 * the sample is added into a #JammoEditingTrack. When that
 * happens, the sequencer is not relevant in any way anymore.
 *
 * Return value: a new #JammoSample.
 */
JammoSample* jammo_sample_new_from_file_with_sequencer(const gchar* filename, JammoSequencer* sequencer) {

	return JAMMO_SAMPLE(g_object_new(JAMMO_TYPE_SAMPLE, "filename", filename, "sequencer", sequencer, NULL));
}

/**
 * jammo_sample_new_from_existing:
 * @sample: an existing #JammoSample
 *
 * An utility function to instantiate a new #JammoSample
 * that has the same audio data file than an existing
 * #JammoSample. The benefit is that if the duration
 * of the existing sample is already calculated, it
 * does not need to recalculate.
 *
 * Return value: a #JammoSample
 */
JammoSample* jammo_sample_new_from_existing(JammoSample* sample) {
	g_return_val_if_fail(JAMMO_IS_SAMPLE(sample), NULL);

	return JAMMO_SAMPLE(g_object_new(JAMMO_TYPE_SAMPLE,
	                                 "filename", sample->priv->filename,
					 "duration", sample->priv->duration,
					 "sequencer", sample->priv->sequencer,
					 NULL));
}

/**
 * jammo_sample_get_filename:
 * @sample: a #JammoSample
 *
 * Returns the name of the audio data file.
 *
 * Return value: the file name string
 */
const gchar* jammo_sample_get_filename(JammoSample* sample) {
	g_return_val_if_fail(JAMMO_IS_SAMPLE(sample), NULL);

	return sample->priv->filename;
}

/**
 * jammo_sample_get_real_filename:
 * @sample: a #JammoSample
 *
 * Returns the real file name of the audio data file.
 *
 * Return value: the real file name string
 */
const gchar* jammo_sample_get_real_filename(JammoSample* sample) {
	g_return_val_if_fail(JAMMO_IS_SAMPLE(sample), NULL);

	return sample->priv->real_filename;
}

/**
 * jammo_sample_get_duration:
 * @sample: a #JammoSample
 *
 * Returns the duration of the sample, if it is available.
 * May also return JAMMO_DURATION_INVALID, if the duration
 * is not known (yet) or sampe data file is not available.
 *
 * Return value: duration in nanoseconds, or JAMMO_DURATION_INVALID
 * if that information is not available.
 */
guint64 jammo_sample_get_duration(JammoSample* sample) {
	g_return_val_if_fail(JAMMO_IS_SAMPLE(sample), 0);

	if (sample->priv->duration == JAMMO_DURATION_INVALID &&
	   ((sample->priv->duration = jammo_meam_get_cached_duration(sample->priv->real_filename)) == JAMMO_DURATION_INVALID)) {
		gchar* message = g_strdup_printf("No cached duration for sample '%s', prerolling.", sample->priv->real_filename);
		cem_add_to_log(message,J_LOG_WARN);
		g_free(message);
		play_sample(sample, TRUE);
	}
	
	return sample->priv->duration;
}

/**
 * jammo_sample_get_editing_track:
 * @sample: a #JammoSample
 *
 * Returns the editing track this sample is part of. A sample
 * can be also stand-alone meaning that it is not (yet)
 * added into a #JammoEditingTrack.
 *
 * Return value: a #JammoEditingTrack, or NULL if the sample is
 * stand-alone.
 */
JammoEditingTrack* jammo_sample_get_editing_track(JammoSample* sample) {
	g_return_val_if_fail(JAMMO_IS_SAMPLE(sample), NULL);

	return sample->priv->editing_track;
}

/**
 * jammo_sample_get_tempo:
 * @sample: a #JammoSample
 *
 * Returns the tempo of the sample.
 *
 * Return value: tempo in bpm (beats per minute).
 */
guint jammo_sample_get_tempo(JammoSample* sample) {
	g_return_val_if_fail(JAMMO_IS_SAMPLE(sample), 0);
	
	return sample->priv->tempo;
}

/**
 * jammo_sample_set_tempo:
 * @sample: a #JammoSample
 * @tempo: tempo in bpm (beats per minute)
 *
 * Sets the tempo of the sample.
 */
void jammo_sample_set_tempo(JammoSample* sample, guint tempo) {
	g_return_if_fail(JAMMO_IS_SAMPLE(sample));

	if (sample->priv->tempo != tempo) {
		sample->priv->tempo = tempo;
		initialize_state(sample);
		g_object_notify(G_OBJECT(sample), "tempo");
	}
}

/**
 * jammo_sample_get_pitch:
 * @sample: a #JammoSample
 *
 * Returns the pitch of the sample.
 *
 * Return value: pitch as a note (string).
 */
const gchar* jammo_sample_get_pitch(JammoSample* sample) {
	g_return_val_if_fail(JAMMO_IS_SAMPLE(sample), NULL);

	return sample->priv->pitch;
}

/**
 * jammo_sample_set_pitch:
 * @sample: a #JammoSample
 * @pitch: pitch as a note (string)
 *
 * Sets the pitch of the sample.
 */
void jammo_sample_set_pitch(JammoSample* sample, const gchar* pitch) {
	g_return_if_fail(JAMMO_IS_SAMPLE(sample));

	if (g_strcmp0(sample->priv->pitch, pitch)) {
		g_free(sample->priv->pitch);
		sample->priv->pitch = g_strdup(pitch);
		initialize_state(sample);
		g_object_notify(G_OBJECT(sample), "pitch");
	}
}

/**
 * jammo_sample_get_position:
 * @sample: a #JammoSample
 *
 * Returns the start time of the sample on an editing track.
 *
 * Return value: start time (guint64).
 */
guint64 _jammo_sample_get_position(JammoSample* sample) {
	g_return_val_if_fail(JAMMO_IS_SAMPLE(sample), 0);

	return sample->priv->start;
}

/**
 * jammo_sample_set_position:
 * @sample: a #JammoSample
 *
 * Sets the start time of the sample on an editing track.
 *
 */
void _jammo_sample_set_position(JammoSample* sample, guint64 position) {
	g_return_if_fail(JAMMO_IS_SAMPLE(sample));

	sample->priv->start = position;
}

/**
 * jammo_sample_play:
 * @sample: a #JammoSample
 *
 * Plays the sample alone. This function does not
 * use sequencer functionality, so the sample does
 * need to be part of any #JammoTrack (but it can
 * be).
 */
void jammo_sample_play(JammoSample* sample) {
	g_return_if_fail(JAMMO_IS_SAMPLE(sample));

	jammo_sample_stop(sample);
	play_sample(sample, FALSE);	
}

/**
 * jammo_sample_stop:
 * @sample: a #JammoSample
 *
 * Stops playing of the sample that have been started
 * with #jammo_sample_play.
 */
void jammo_sample_stop(JammoSample* sample) {
	GList* list;
	
	g_return_if_fail(JAMMO_IS_SAMPLE(sample));

	for (list = play_datas; list; list = list->next) {
		if (((PlayData*)list->data)->sample == sample) {
			break;
		}
	}
	if (list) {
		stop_and_free_play_data((PlayData*)list->data);
		play_datas = g_list_delete_link(play_datas, list);
	}
}

/**
 * jammo_sample_stop_all:
 *
 * Stops all playing samples that have been started
 * with #jammo_sample_play.
 */
void jammo_sample_stop_all(void) {
	if (play_datas) {
		g_list_foreach(play_datas, (GFunc)stop_and_free_play_data, NULL);
		g_list_free(play_datas);
		play_datas = NULL;
	}
}

void _jammo_sample_set_editing_track(JammoSample* sample, JammoEditingTrack* editing_track) {
	g_return_if_fail(JAMMO_IS_SAMPLE(sample));

	sample->priv->editing_track = editing_track;
	
	if (sample->priv->sequencer) {
		g_signal_handlers_disconnect_by_func(sample->priv->sequencer, G_CALLBACK(on_sequencer_notify), sample);
		g_object_remove_weak_pointer(G_OBJECT(sample->priv->sequencer), (gpointer*)&sample->priv->sequencer);
		sample->priv->sequencer = NULL;
	}
}

static void jammo_sample_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoSample* sample;
	
	sample = JAMMO_SAMPLE(object);

	switch (prop_id) {
		case PROP_FILENAME:
			sample->priv->filename = g_strdup(g_value_get_string(value));
			break;
		case PROP_DURATION:
			sample->priv->duration = g_value_get_uint64(value);
			break;
		case PROP_TEMPO:
			jammo_sample_set_tempo(sample, g_value_get_uint(value));
			break;
		case PROP_PITCH:
			jammo_sample_set_pitch(sample, g_value_get_string(value));
			break;
		case PROP_SEQUENCER:
			sample->priv->sequencer = JAMMO_SEQUENCER(g_value_get_object(value));
			if (sample->priv->sequencer) {
				g_object_add_weak_pointer(G_OBJECT(sample->priv->sequencer), (gpointer*)&sample->priv->sequencer);
				g_object_get(sample->priv->sequencer, "tempo", &sample->priv->tempo, "pitch", &sample->priv->pitch, NULL);
				g_signal_connect_swapped(sample->priv->sequencer, "notify::tempo", G_CALLBACK(on_sequencer_notify), sample);
				g_signal_connect_swapped(sample->priv->sequencer, "notify::pitch", G_CALLBACK(on_sequencer_notify), sample);
			}
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_sample_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoSample* sample;

	sample = JAMMO_SAMPLE(object);

        switch (prop_id) {
		case PROP_FILENAME:
			g_value_set_string(value, sample->priv->filename);
			break;
		case PROP_REAL_FILENAME:
			g_value_set_string(value, sample->priv->real_filename);
			break;
		case PROP_DURATION:
			g_value_set_uint64(value, sample->priv->duration);
			break;
		case PROP_TEMPO:
			g_value_set_uint(value, sample->priv->tempo);
			break;
		case PROP_PITCH:
			g_value_set_string(value, sample->priv->pitch);
			break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static GObject* jammo_sample_constructor(GType type, guint n_properties, GObjectConstructParam* properties) {
	GObject* object;
	JammoSample* sample;

	object = G_OBJECT_CLASS(jammo_sample_parent_class)->constructor(type, n_properties, properties);

	sample = JAMMO_SAMPLE(object);
	initialize_state(sample);

	return object;
}

static void jammo_sample_finalize(GObject* object) {
	JammoSample* sample;
	
	sample = JAMMO_SAMPLE(object);
	g_free(sample->priv->filename);
	g_free(sample->priv->real_filename);
	g_free(sample->priv->pitch);

	G_OBJECT_CLASS(jammo_sample_parent_class)->finalize(object);
}

static void jammo_sample_dispose(GObject* object) {
	JammoSample* sample;
	
	sample = JAMMO_SAMPLE(object);
	if (sample->priv->sequencer) {
		g_signal_handlers_disconnect_by_func(sample->priv->sequencer, G_CALLBACK(on_sequencer_notify), sample);
		g_object_remove_weak_pointer(G_OBJECT(sample->priv->sequencer), (gpointer*)&sample->priv->sequencer);
		sample->priv->sequencer = NULL;
	}	

	G_OBJECT_CLASS(jammo_sample_parent_class)->dispose(object);
}

static void jammo_sample_class_init(JammoSampleClass* klass) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(klass);

	gobject_class->constructor = jammo_sample_constructor;
	gobject_class->finalize = jammo_sample_finalize;
	gobject_class->dispose = jammo_sample_dispose;
	gobject_class->set_property = jammo_sample_set_property;
	gobject_class->get_property = jammo_sample_get_property;

	/**
	 * JammoSample:filename:
	 *
	 * The name of the sample data file.
	 *
	 * The filename can contain the following parameters that are substited with the corresponding property values:
	 *
	 * $t for tempo (unsigned integer), see :tempo
	 *
	 * $p for pitch (free form string), see :pitch
	 *
	 * Thus, the actual file name may change automatically when the values of the parameters above will change.
	 */
	g_object_class_install_property(gobject_class, PROP_FILENAME,
	                                g_param_spec_string("filename",
	                                "File name",
	                                "The name of the sample data file",
	                                NULL,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSample:real-filename:
	 *
	 * The real name of the sample data file after the parameters have been parsed (see :filename).
	 */
	g_object_class_install_property(gobject_class, PROP_REAL_FILENAME,
	                                g_param_spec_string("real-filename",
	                                "Real file name",
	                                "The real name of the sample data file after the parameters have been parsed",
	                                NULL,
	                                G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSample:duration:
	 *
	 * The duration of the sample in nanoseconds.
	 *
	 * This property is not normally set by an application.
	 */
	g_object_class_install_property(gobject_class, PROP_DURATION,
	                                g_param_spec_uint64("duration",
	                                "Duration",
	                                "The duration of the sample in nanoseconds",
	                                0, G_MAXUINT64, JAMMO_DURATION_INVALID,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSample:tempo:
	 *
	 * The tempo in beats per minute for this sample. The value is used when constructing the real filename of this sample
	 * (see :filename).
	 */
	g_object_class_install_property(gobject_class, PROP_TEMPO,
	                                g_param_spec_uint("tempo",
	                                "Tempo",
	                                "The tempo in beats per minute for this sample",
	                                0, G_MAXUINT, 110,
	                                G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSample:pitch:
	 *
	 * The pitch as a note for this sample. The value is used when constructing the real filename of this sample
	 * (see :filename).
	 */
	g_object_class_install_property(gobject_class, PROP_PITCH,
	                                g_param_spec_string("pitch",
	                                "Pitch",
	                                "The pitch as a note for this sample",
	                                NULL,
	                                G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSample:sequencer:
	 *
	 * A sequencer that is used to track tempo and pitch until this sample is added to an editing track.
	 */
	g_object_class_install_property(gobject_class, PROP_SEQUENCER,
	                                g_param_spec_object("sequencer",
	                                "Sequencer",
	                                "A sequencer that is used to track tempo and pitch until this sample is added to an editing track",
	                                JAMMO_TYPE_SEQUENCER,
	                                G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	/**
	 * JammoSample::stopped:
	 * @sample: the object which received the signal
	 *
	 * The ::stopped signal is emitted when the playing of the (single) sample has stopped.
	 *
	 * This signal is not emitted when the sample is played as part of a track.
	 */
	signals[STOPPED] = g_signal_new("stopped", G_TYPE_FROM_CLASS(gobject_class),
					       G_SIGNAL_RUN_LAST, 0,
					       NULL, NULL,
					       g_cclosure_marshal_VOID__VOID,
					       G_TYPE_NONE, 0);

	g_type_class_add_private (gobject_class, sizeof(JammoSamplePrivate));
}

static void jammo_sample_init(JammoSample* sample) {
	sample->priv = G_TYPE_INSTANCE_GET_PRIVATE(sample, JAMMO_TYPE_SAMPLE, JammoSamplePrivate);
	sample->priv->tempo = 110;
	sample->priv->start=0;
}

static gboolean preroll_bus_callback(GstBus* bus, GstMessage* message, gpointer data) {
	PlayData* play_data;
	gboolean cleanup;
	GstFormat format;
	gint64 duration;
	
	play_data = (PlayData*)data;
	cleanup = FALSE;
	if (GST_MESSAGE_TYPE(message) == GST_MESSAGE_ASYNC_DONE || GST_MESSAGE_TYPE(message) == GST_MESSAGE_EOS) {
		format = GST_FORMAT_TIME;
		if (gst_element_query_duration(play_data->pipeline, &format, &duration)) {
			play_data->sample->priv->duration = (guint64)duration;
			g_object_notify(G_OBJECT(play_data->sample), "duration");
			
			jammo_meam_set_cached_duration(play_data->sample->priv->real_filename, play_data->sample->priv->duration);
		}
		cleanup = TRUE;
	} else if (GST_MESSAGE_TYPE(message) == GST_MESSAGE_ERROR) {
		play_data->sample->priv->duration = 0;
		cleanup = TRUE;
	}
	if (cleanup) {
		stop_and_free_play_data(play_data);
	}
	
	return !cleanup;
}

static gboolean play_bus_callback(GstBus* bus, GstMessage* message, gpointer data) {
	PlayData* play_data;
	gboolean cleanup;
	
	play_data = (PlayData*)data;
	cleanup = FALSE;
	if (GST_MESSAGE_TYPE(message) == GST_MESSAGE_EOS ||
	    GST_MESSAGE_TYPE(message) == GST_MESSAGE_ERROR) {
		stop_and_free_play_data(play_data);
		play_datas = g_list_remove(play_datas, play_data);
		cleanup = TRUE;
	}
	
	return !cleanup;
}

static void on_new_decoded_pad(GstElement* element, GstPad* pad, gboolean last, gpointer data) {
	PlayData* play_data;
	GstPad* sinkpad;
	GstCaps* caps;

	play_data = (PlayData*)data;
	sinkpad = gst_element_get_pad(play_data->sink, "sink");
	if (!GST_PAD_IS_LINKED(sinkpad)) {
		caps = gst_pad_get_caps(pad);
		if (g_strrstr(gst_structure_get_name(gst_caps_get_structure(caps, 0)), "audio")) {
			gst_pad_link(pad, sinkpad);
		}
		gst_caps_unref(caps);
	}
	g_object_unref(sinkpad);
}

static void play_sample(JammoSample* sample, gboolean preroll_only) {
	PlayData* play_data;
	GstBus* bus;
	GstElement* filesrc;
	GstElement* decodebin;
	GstElement* sink;

	play_data = g_malloc0(sizeof(PlayData));
	play_data->preroll_only = preroll_only;
	play_data->sample = JAMMO_SAMPLE(g_object_ref(sample));
	play_data->pipeline = gst_element_factory_make("pipeline", NULL);
	bus = gst_pipeline_get_bus(GST_PIPELINE(play_data->pipeline));
	gst_bus_add_watch(bus, (preroll_only ? preroll_bus_callback : play_bus_callback), play_data);
	gst_object_unref(bus);

	filesrc = gst_element_factory_make("filesrc", NULL);
	g_object_set(filesrc, "location", sample->priv->real_filename, NULL);
	decodebin = gst_element_factory_make("decodebin", NULL);
	g_signal_connect(decodebin, "new-decoded-pad", G_CALLBACK(on_new_decoded_pad), play_data);

	if (preroll_only) {
		play_data->sink = gst_element_factory_make("fakesink", NULL);
		gst_bin_add_many(GST_BIN(play_data->pipeline), filesrc, decodebin, play_data->sink, NULL);
	} else {
		play_data->sink = gst_element_factory_make("audioconvert", NULL);
		sink = gst_element_factory_make("autoaudiosink", NULL);
		gst_bin_add_many(GST_BIN(play_data->pipeline), filesrc, decodebin, play_data->sink, sink, NULL);
		gst_element_link(play_data->sink, sink);

		play_datas = g_list_prepend(play_datas, play_data);
	}
	gst_element_link(filesrc, decodebin);

	if (preroll_only) {
		gst_element_set_state(play_data->pipeline, GST_STATE_PAUSED); 
	} else {
		gst_element_set_state(play_data->pipeline, GST_STATE_PLAYING); 
	}
}

static void stop_and_free_play_data(PlayData* play_data) {
	if (!play_data->preroll_only) {
		g_signal_emit(play_data->sample, signals[STOPPED], 0);
	}
	g_object_unref(play_data->sample);
	gst_element_set_state(play_data->pipeline, GST_STATE_NULL);
	gst_object_unref(play_data->pipeline);
	g_free(play_data);
}

static gchar* str_replace(gchar* string, gsize position, gsize length, const gchar* replacement, gchar** replacement_end_return) {
	gsize string_length;
	gsize replacement_length;
	gchar* s;
	
	string_length = strlen(string);
	replacement_length = strlen(replacement);
	s = (gchar*)g_malloc(string_length - length + replacement_length + 1);
	g_memmove(s, string, position);
	g_memmove(s + position, replacement, replacement_length);
	g_memmove(s + position + replacement_length, string + position + length, string_length - position - length + 1);

	if (replacement_end_return) {
		*replacement_end_return = s + position + replacement_length;
	}

	g_free(string);

	return s;
}

static void initialize_state(JammoSample* sample) {
	gchar* filename;
	gchar* s;
	gchar* tempo_string;

	filename = s = g_strdup(sample->priv->filename);
	while ((s = strchr(s, '$'))) {
		switch (s[1]) {
			case '$':
				filename = str_replace(filename, s - filename, 2, "$", &s);
				break;
			case 't':
				tempo_string = g_strdup_printf("%u", sample->priv->tempo);
				filename = str_replace(filename, s - filename, 2, tempo_string, &s);
				g_free(tempo_string);
				break;
			case 'p':
				filename = str_replace(filename, s - filename, 2, (sample->priv->pitch ? sample->priv->pitch : "?"), &s);
				break;
			default:
				g_warning("Unexpected parameter in the sample filename: '$%c', skipped.", s[1]);
				s += 2;
				break;
		}
	}
	
	if (g_strcmp0(sample->priv->real_filename, filename)) {
		g_free(sample->priv->real_filename);

		if (filename[0] == '/' ) {
			sample->priv->real_filename = g_strdup_printf("%s",filename);
		} else {
			sample->priv->real_filename = g_strdup_printf("%s/%s",AUDIO_DIR,filename);
		}
	}

	g_free(filename);
}

static void on_sequencer_notify(JammoSample* sample) {
	if (sample->priv->sequencer) {
		jammo_sample_set_tempo(sample, jammo_sequencer_get_tempo(sample->priv->sequencer));
		jammo_sample_set_pitch(sample, jammo_sequencer_get_pitch(sample->priv->sequencer));
	}
}
