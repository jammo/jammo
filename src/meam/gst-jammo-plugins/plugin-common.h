/*This file is part of JamMo.
*/
#ifndef __PLUGIN_COMMON_H__
#define __PLUGIN_COMMON_H__

#include <gst/gst.h>

gfloat jammo_gst_do_interpolation(gfloat interpolation_point, gfloat value1, gfloat value2);
gfloat jammo_gst_do_pitch(gint needed_pitch);
gfloat jammo_gst_do_xfader(gint16 *sampletable, gfloat *xfadecounter, gfloat *xfadeindex, gint xfade, gfloat value, gfloat scale, gint needed_pitch, gint samplerate);

#endif /* __PLUGIN_COMMON_H__ */
