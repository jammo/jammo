#include "plugin-common.h"
gfloat jammo_gst_do_interpolation(gfloat interpolation_point, gfloat value1, gfloat value2) {
	/* linear interpolation */
	return ((1-interpolation_point)*value1 + interpolation_point*value2);
}

gfloat jammo_gst_do_pitch(gint needed_pitch){
	gfloat pitch=1.0;
		if (needed_pitch<=0)
			while (needed_pitch<0) {
				pitch*=0.9439;
				needed_pitch++;
			}

		else
			while (needed_pitch>0) {
				pitch*=1.0595;
				needed_pitch--;
			}
		
	return pitch;
}

gfloat jammo_gst_do_xfader(gint16 *sampletable, gfloat *xfadecounter, gfloat *xfadeindex, gint xfade, gfloat value, gfloat scale, gint needed_pitch, gint samplerate) {

	gfloat xfademix;

	/* linear crossfade */
	/* fade in signal */
	xfademix= (0.0+1.0* (*xfadecounter)/(xfade*1.0));
	value*= xfademix;
	/* fade out signal */
	xfademix= (1.0-1.0* (*xfadecounter)/(xfade*1.0));
	value+= (xfademix)*( jammo_gst_do_interpolation( *xfadeindex-(gint)*xfadeindex, sampletable[(gint)*xfadeindex], sampletable[((gint)*xfadeindex+1)] ) /(G_MAXINT16*1.0) * scale);
	/* Compensation of volume loss during crossfade */
	/*values[counter]*=1.05-fabs(0.05-0.1* (*xfadecounter)/(*xfade*1.0));*/
	*xfadecounter+=1.0;
	/* 44100 is samplerate of sample files. value could be read from wav header and stored to i.e slicedWav->samplerate. then we would be able to used samples with different sample rates */
	/* samplerate variable has the output samplerate of jammosampler */
	*xfadeindex += jammo_gst_do_pitch(needed_pitch)*44100/(samplerate+0.0);

	return value;
}
