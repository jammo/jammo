#ifndef __GST_PITCHDETECT_H__
#define __GST_PITCHDETECT_H__

#include <gst/gst.h>
#include <gst/base/gstadapter.h>
#include <gst/base/gstbasetransform.h>



G_BEGIN_DECLS

#define GST_TYPE_PITCHDETECT \
  (gst_pitchdetect_get_type())
#define GST_PITCHDETECT(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_PITCHDETECT,GstPitchDetect))
#define GST_PITCHDETECT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_PITCHDETECT,GstPitchDetectClass))
#define GST_IS_PLUGIN_TEMPLATE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_PITCHDETECT))
#define GST_IS_PLUGIN_TEMPLATE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_PITCHDETECT))

typedef struct _GstPitchDetect      GstPitchDetect;
typedef struct _GstPitchDetectClass GstPitchDetectClass;

struct _GstPitchDetect {
  GstBaseTransform element;

  GstPad *sinkpad,*srcpad;
  GstAdapter *adapter;
};

struct _GstPitchDetectClass {
  GstBaseTransformClass parent_class;
};

GType gst_pitchdetect_get_type (void);

G_END_DECLS

#endif /* __GST_PITCHDETECT_H__ */
