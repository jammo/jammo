
/**
 * SECTION:element-pitchdetect
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <math.h>

#include "gstjammopitchdetect.h"

/* Filter signals and args */
enum {
  PROP_0,
  PROP_DEBUG,
  PROP_SILENCE_THRESHOLD,
  PROP_LAST
};

#define DEFAULT_SILENCE 0.01
#define MAX_WAVELENGTH 294
#define MIN_WAVELENGTH 44
gboolean debug_enabled=FALSE;
gfloat silence_threshold= DEFAULT_SILENCE;

static GstStaticPadTemplate sink_template =
GST_STATIC_PAD_TEMPLATE (
  "sink",
  GST_PAD_SINK,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS ("audio/x-raw-int, "
      "rate = (int) [ 1, MAX ], "
      "channels = (int) [1, MAX], "
      "endianness = (int) BYTE_ORDER, "
      "width = (int) 16, " "depth = (int) 16, " "signed = (boolean) true")
);

static GstStaticPadTemplate src_template =
GST_STATIC_PAD_TEMPLATE (
  "src",
  GST_PAD_SRC,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS ("audio/x-raw-int, "
      "rate = (int) [ 1, MAX ], "
      "channels = (int) [1, MAX], "
      "endianness = (int) BYTE_ORDER, "
      "width = (int) 16, " "depth = (int) 16, " "signed = (boolean) true")
);


GST_BOILERPLATE (GstPitchDetect, gst_pitchdetect, GstBaseTransform,
    GST_TYPE_BASE_TRANSFORM);

static void gst_pitchdetect_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_pitchdetect_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static void gst_pitchdetect_dispose (GObject * object);

static gboolean gst_pitchdetect_set_caps (GstBaseTransform * trans, GstCaps * in,
    GstCaps * out);
static gboolean gst_pitchdetect_start (GstBaseTransform * trans);

static GstFlowReturn gst_pitchdetect_transform_ip (GstBaseTransform * trans,
    GstBuffer * in);

/* GObject vmethod implementations */

static void
gst_pitchdetect_base_init (gpointer klass)
{
  static GstElementDetails element_details = {
   (gchar*)"Pitch detector",
   (gchar*)"Analyzer/Audio",
   (gchar*) "Give audio signal, output frequency",
   (gchar*)"Aapo Rantalainen <aapo.rantalainen@gmail.com>, Mikko Gynther <mikko.gynther@lut.fi>"
  };
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&src_template));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_template));
  gst_element_class_set_details (element_class, &element_details);
}

static void
gst_pitchdetect_class_init (GstPitchDetectClass * klass)
{
  GObjectClass *gobject_class;
  GstBaseTransformClass *trans_class = GST_BASE_TRANSFORM_CLASS (klass);

  gobject_class = (GObjectClass *) klass;

  gobject_class->set_property = gst_pitchdetect_set_property;
  gobject_class->get_property = gst_pitchdetect_get_property;


 g_object_class_install_property (gobject_class, PROP_DEBUG,
      g_param_spec_boolean ("debug", "debug printing",
          "Whether to print frequency", FALSE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
 g_object_class_install_property (gobject_class, PROP_SILENCE_THRESHOLD,
      g_param_spec_float ("silence", "silence", "What is consider silence (if you want hear low sounds, use small value)", 0.0,
          1.0, DEFAULT_SILENCE,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));

 
  gobject_class->dispose = gst_pitchdetect_dispose;

  trans_class->set_caps = GST_DEBUG_FUNCPTR (gst_pitchdetect_set_caps);
  trans_class->start = GST_DEBUG_FUNCPTR (gst_pitchdetect_start);
  trans_class->transform_ip = GST_DEBUG_FUNCPTR (gst_pitchdetect_transform_ip);
  trans_class->passthrough_on_same_caps = TRUE;

  GST_BASE_TRANSFORM_CLASS (klass)->transform_ip =
      GST_DEBUG_FUNCPTR (gst_pitchdetect_transform_ip);
}

static void
gst_pitchdetect_init (GstPitchDetect *filter, GstPitchDetectClass * klass)
{
  filter->adapter = gst_adapter_new ();
}

static void
gst_pitchdetect_dispose (GObject * object)
{
  GstPitchDetect *filter = GST_PITCHDETECT (object);

  if (filter->adapter) {
    g_object_unref (filter->adapter);
    filter->adapter = NULL;
  }


  G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
gst_pitchdetect_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{

  switch (prop_id) {
    case PROP_DEBUG:
      debug_enabled= g_value_get_boolean (value);
      break;
    case PROP_SILENCE_THRESHOLD:
      silence_threshold = g_value_get_float (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_pitchdetect_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{

  switch (prop_id) {
    case PROP_DEBUG:
      g_value_set_boolean (value, debug_enabled);
      break;
    case PROP_SILENCE_THRESHOLD:
      g_value_set_float (value,silence_threshold);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static gboolean
gst_pitchdetect_set_caps (GstBaseTransform * trans, GstCaps * in, GstCaps * out)
{
  //GstStructure *structure;
  //structure = gst_caps_get_structure (in, 0);

  return TRUE;
}

static gboolean
gst_pitchdetect_start (GstBaseTransform * trans)
{
  GstPitchDetect *filter = GST_PITCHDETECT (trans);

  gst_adapter_clear (filter->adapter);
  

  return TRUE;
}



/* GstBaseTransform vmethod implementations */

/* this function does the actual processing
 */
static GstFlowReturn
gst_pitchdetect_transform_ip (GstBaseTransform * trans,
    GstBuffer * in)
{
  GstPitchDetect *filter = GST_PITCHDETECT (trans);
  gint16 *samples;
  gint wanted;
  gint i, j;
	gfloat rms=0.0;

  gst_adapter_push (filter->adapter, gst_buffer_ref (in));
  wanted = 44100; // request 44100 samples

  while (gst_adapter_available (filter->adapter) > wanted) {
        samples = (gint16 *) gst_adapter_take (filter->adapter, wanted);

	gfloat autocorrelation;
	gint frames=2*MAX_WAVELENGTH;
	gfloat best=0.0;
	gfloat bestfreq=-1.0;

	/* calculate rms */
	for(i=0;i<frames;i++)
			rms+= pow(samples[i], 2) / (frames);

	rms=sqrt(rms);

	/* do autocorrelation if signal is loud enough
		note that this is not calibrated to a reasonable value
		it just shows how some input can be considered as too quiet */
	if (rms>silence_threshold*G_MAXINT16) {
		/* 44100 sample/s / 294 sample = 150 Hz */
		/* 44100 sample/s / 41 sample = 1002 Hz */
		for (j=MAX_WAVELENGTH;j>MIN_WAVELENGTH;j-=j/MIN_WAVELENGTH) {
			autocorrelation=0.0;
			for(i=0;i<frames;i++)
				if (i+j<frames)
					autocorrelation+=samples[i]*samples[i+j];

			if (autocorrelation>best) {
				
				best=autocorrelation;
				/* Sample rate should be asked from gstreamer instead of hard coded 44100 */
				bestfreq=44100/(j*1.0);
			}

			// to prevent a loop that runs forever
			if (j/MIN_WAVELENGTH<=0)
				j--;

			}
	}
	
	GstStructure *s;
	s = gst_structure_new ("pitchdetect", "frequency", G_TYPE_FLOAT, bestfreq, NULL);

	GstMessage *m = gst_message_new_element (GST_OBJECT (filter), s);

	gst_element_post_message (GST_ELEMENT (filter), m);
	if(debug_enabled)
		printf("Played frequency is %f\n", bestfreq);

	g_free(samples);
  }

  return GST_FLOW_OK;
}

gboolean plugin_pitchdetect_init (GstPlugin * plugin)
{
  return gst_element_register (plugin, "pitchdetect", GST_RANK_NONE,
      GST_TYPE_PITCHDETECT);
}
