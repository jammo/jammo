/* This file is part of JamMo.
License:LGPL 2.1

 */
/**
 * SECTION:element-jammometronome
 *
 * JammoMetronome is a metronome element. 
 *
 */

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <gst/controller/gstcontroller.h>
#include <sys/time.h>

#include "../plugin-common.h"
#include "gstjammometronome.h"

/*e.g. Nokia Internet Tablet n810 doesn't have this (maemo diablo)*/
#ifndef G_PARAM_STATIC_STRINGS
#define	G_PARAM_STATIC_STRINGS (G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB)
#endif

#ifndef M_PI
#define M_PI  3.14159265358979323846
#endif

#ifndef M_PI_M2
#define M_PI_M2  6.28318530717958647692
#endif

#define JAMMO_METRONOME_MAXTEMPO 300.0
#define JAMMO_METRONOME_MINTEMPO 30.0
#define JAMMO_METRONOME_DEFAULT_TEMPO 120.0
#define JAMMO_METRONOME_MAXBEATS 32
#define JAMMO_METRONOME_MINBEATS 1
#define JAMMO_METRONOME_DEFAULT_BEATS 4
#define JAMMO_METRONOME_DEFAULT_NOTE_VALUE 4

static const GstElementDetails gst_jammo_metronome_details =
GST_ELEMENT_DETAILS ((gchar*)"JamMo Metronome",
   (gchar*)"Source/Audio",
   (gchar*)"Metronome",
   (gchar*)"Mikko Gynther <mikko.gynther@lut.fi>");

#define DEFAULT_SAMPLES_PER_BUFFER   256
#define DEFAULT_VOLUME               0.6
#define DEFAULT_NORMAL_WAVELENGTH    128.0
#define DEFAULT_ACCENT_WAVELENGTH    64.0
#define DEFAULT_IS_LIVE              FALSE
#define DEFAULT_TIMESTAMP_OFFSET     G_GINT64_CONSTANT (0)
#define DEFAULT_CAN_ACTIVATE_PUSH    TRUE
#define DEFAULT_CAN_ACTIVATE_PULL    FALSE

enum
{
  PROP_0,
  PROP_SAMPLES_PER_BUFFER,
  PROP_TEMPO,
	PROP_TIME_SIGNATURE_BEATS,
	PROP_TIME_SIGNATURE_NOTE_VALUE,
	PROP_ACCENT,
  PROP_VOLUME,
  PROP_IS_LIVE,
  PROP_TIMESTAMP_OFFSET,
  PROP_CAN_ACTIVATE_PUSH,
  PROP_CAN_ACTIVATE_PULL,
  PROP_LAST
};

static GstStaticPadTemplate gst_jammo_metronome_src_template =
    GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-raw-int, "
        "endianness = (int) BYTE_ORDER, "
        "signed = (boolean) true, "
        "width = (int) 16, "
        "depth = (int) 16, "
        "rate = (int) [ 1, MAX ], "
        "channels = (int) 1; "
        "audio/x-raw-int, "
        "endianness = (int) BYTE_ORDER, "
        "signed = (boolean) true, "
        "width = (int) 32, "
        "depth = (int) 32,"
        "rate = (int) [ 1, MAX ], "
        "channels = (int) 1; "
        "audio/x-raw-float, "
        "endianness = (int) BYTE_ORDER, "
        "width = (int) { 32, 64 }, "
        "rate = (int) [ 1, MAX ], " "channels = (int) 1")
    );

GST_BOILERPLATE (GstJammoMetronome, gst_jammo_metronome, GstBaseSrc, GST_TYPE_BASE_SRC)

static void gst_jammo_metronome_dispose (GObject * object);
static void gst_jammo_metronome_finalize (GObject * object);

static void gst_jammo_metronome_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec);
static void gst_jammo_metronome_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);

static gboolean gst_jammo_metronome_setcaps (GstBaseSrc * basesrc,
    GstCaps * caps);
static void gst_jammo_metronome_src_fixate (GstPad * pad, GstCaps * caps);

static gboolean gst_jammo_metronome_is_seekable (GstBaseSrc * basesrc);
static gboolean gst_jammo_metronome_check_get_range (GstBaseSrc * basesrc);
static gboolean gst_jammo_metronome_do_seek (GstBaseSrc * basesrc,
    GstSegment * segment);
static gboolean gst_jammo_metronome_query (GstBaseSrc * basesrc,
    GstQuery * query);

static void gst_jammo_metronome_get_times (GstBaseSrc * basesrc,
    GstBuffer * buffer, GstClockTime * start, GstClockTime * end);
static gboolean gst_jammo_metronome_start (GstBaseSrc * basesrc);
static gboolean gst_jammo_metronome_stop (GstBaseSrc * basesrc);
static GstFlowReturn gst_jammo_metronome_create (GstBaseSrc * basesrc,
    guint64 offset, guint length, GstBuffer ** buffer);

static void init_sample_table (gint * sampletable, gint size, gfloat volume, gfloat wavelength);

static void
gst_jammo_metronome_base_init (gpointer g_class)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&gst_jammo_metronome_src_template));
  gst_element_class_set_details (element_class, &gst_jammo_metronome_details);
}

static void gst_jammo_metronome_class_init (GstJammoMetronomeClass * klass)
{
  GObjectClass *gobject_class;
  GstBaseSrcClass *gstbasesrc_class;

  gobject_class = (GObjectClass *) klass;
  gstbasesrc_class = (GstBaseSrcClass *) klass;

	gobject_class->dispose = gst_jammo_metronome_dispose;
	gobject_class->finalize = gst_jammo_metronome_finalize;

  gobject_class->set_property = gst_jammo_metronome_set_property;
  gobject_class->get_property = gst_jammo_metronome_get_property;

  g_object_class_install_property (gobject_class, PROP_SAMPLES_PER_BUFFER,
      g_param_spec_int ("samplesperbuffer", "Samples per buffer",
          "Number of samples in each outgoing buffer",
          1, G_MAXINT, DEFAULT_SAMPLES_PER_BUFFER,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_TEMPO,
      g_param_spec_float ("tempo", "Tempo", "Tempo",
          JAMMO_METRONOME_MINTEMPO, JAMMO_METRONOME_MAXTEMPO, JAMMO_METRONOME_DEFAULT_TEMPO,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_TIME_SIGNATURE_BEATS,
      g_param_spec_int ("beats", "Beats", "Time signature: beats",
          1, 32, 4,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_TIME_SIGNATURE_NOTE_VALUE,
      g_param_spec_int ("note-value", "Note value", "Time signature: note value",
          1, 128, 4,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_ACCENT,
      g_param_spec_boolean ("accent", "Accent", "Whether to play accent on first beat of a bar or not",
          TRUE, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_VOLUME,
      g_param_spec_double ("volume", "Volume", "Volume of test signal", 0.0,
          1.0, DEFAULT_VOLUME,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_IS_LIVE,
      g_param_spec_boolean ("is-live", "Is Live",
          "Whether to act as a live source", DEFAULT_IS_LIVE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_TIMESTAMP_OFFSET, g_param_spec_int64 ("timestamp-offset",
          "Timestamp offset",
          "An offset added to timestamps set on buffers (in ns)", G_MININT64,
          G_MAXINT64, DEFAULT_TIMESTAMP_OFFSET,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_CAN_ACTIVATE_PUSH,
      g_param_spec_boolean ("can-activate-push", "Can activate push",
          "Can activate in push mode", DEFAULT_CAN_ACTIVATE_PUSH,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_CAN_ACTIVATE_PULL,
      g_param_spec_boolean ("can-activate-pull", "Can activate pull",
          "Can activate in pull mode", DEFAULT_CAN_ACTIVATE_PULL,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  gstbasesrc_class->set_caps = GST_DEBUG_FUNCPTR (gst_jammo_metronome_setcaps);
  gstbasesrc_class->is_seekable =
      GST_DEBUG_FUNCPTR (gst_jammo_metronome_is_seekable);
  gstbasesrc_class->check_get_range =
      GST_DEBUG_FUNCPTR (gst_jammo_metronome_check_get_range);
  gstbasesrc_class->do_seek = GST_DEBUG_FUNCPTR (gst_jammo_metronome_do_seek);
  gstbasesrc_class->query = GST_DEBUG_FUNCPTR (gst_jammo_metronome_query);
  gstbasesrc_class->get_times =
      GST_DEBUG_FUNCPTR (gst_jammo_metronome_get_times);
  gstbasesrc_class->start = GST_DEBUG_FUNCPTR (gst_jammo_metronome_start);
  gstbasesrc_class->stop = GST_DEBUG_FUNCPTR (gst_jammo_metronome_stop);
  gstbasesrc_class->create = GST_DEBUG_FUNCPTR (gst_jammo_metronome_create);
}

static void gst_jammo_metronome_init (GstJammoMetronome * src, GstJammoMetronomeClass * g_class)
{
  GstPad *pad = GST_BASE_SRC_PAD (src);

  gst_pad_set_fixatecaps_function (pad, gst_jammo_metronome_src_fixate);

  src->samplerate = 44100;
  src->format = GST_JAMMO_METRONOME_FORMAT_NONE;

  src->volume = DEFAULT_VOLUME;

	src->time=0;
	src->tempo=JAMMO_METRONOME_DEFAULT_TEMPO;
	src->beats=JAMMO_METRONOME_DEFAULT_BEATS;
	src->note_value=JAMMO_METRONOME_DEFAULT_NOTE_VALUE;
	src->volume=DEFAULT_VOLUME;
	src->accent=TRUE;
	src->sampletable_size=1024;

  /* we operate in time */
  gst_base_src_set_format (GST_BASE_SRC (src), GST_FORMAT_TIME);
  gst_base_src_set_live (GST_BASE_SRC (src), DEFAULT_IS_LIVE);

  src->samples_per_buffer = DEFAULT_SAMPLES_PER_BUFFER;
  src->generate_samples_per_buffer = src->samples_per_buffer;
  src->timestamp_offset = DEFAULT_TIMESTAMP_OFFSET;
  src->can_activate_pull = DEFAULT_CAN_ACTIVATE_PULL;

// gst_base_src_set_blocksize is since 0.10.22, maemo has only 0.10.13
// This works without this.
//  gst_base_src_set_blocksize (GST_BASE_SRC (src), -1);

	init_sample_table (src->sampletable_normal, src->sampletable_size, src->volume, DEFAULT_NORMAL_WAVELENGTH);
	init_sample_table (src->sampletable_accent, src->sampletable_size, src->volume, DEFAULT_ACCENT_WAVELENGTH);

}

static void gst_jammo_metronome_src_fixate (GstPad * pad, GstCaps * caps)
{
  GstJammoMetronome *src = GST_JAMMO_METRONOME (GST_PAD_PARENT (pad));
  const gchar *name;
  GstStructure *structure;

  structure = gst_caps_get_structure (caps, 0);

  GST_DEBUG_OBJECT (src, "fixating samplerate to %d", src->samplerate);

  gst_structure_fixate_field_nearest_int (structure, "rate", src->samplerate);

  name = gst_structure_get_name (structure);
  if (strcmp (name, "audio/x-raw-int") == 0)
    gst_structure_fixate_field_nearest_int (structure, "width", 32);
  else if (strcmp (name, "audio/x-raw-float") == 0)
    gst_structure_fixate_field_nearest_int (structure, "width", 64);
}



static gboolean gst_jammo_metronome_query (GstBaseSrc * basesrc, GstQuery * query)
{
  GstJammoMetronome *src = GST_JAMMO_METRONOME (basesrc);
  gboolean res = FALSE;

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_CONVERT:
    {
      GstFormat src_fmt, dest_fmt;
      gint64 src_val, dest_val;

      gst_query_parse_convert (query, &src_fmt, &src_val, &dest_fmt, &dest_val);
      if (src_fmt == dest_fmt) {
        dest_val = src_val;
        goto done;
      }

      switch (src_fmt) {
        case GST_FORMAT_DEFAULT:
          switch (dest_fmt) {
            case GST_FORMAT_TIME:
              /* samples to time */
              dest_val =
                  gst_util_uint64_scale_int (src_val, GST_SECOND,
                  src->samplerate);
              break;
            default:
              goto error;
          }
          break;
        case GST_FORMAT_TIME:
          switch (dest_fmt) {
            case GST_FORMAT_DEFAULT:
              /* time to samples */
              dest_val =
                  gst_util_uint64_scale_int (src_val, src->samplerate,
                  GST_SECOND);
              break;
            default:
              goto error;
          }
          break;
        default:
          goto error;
      }
    done:
      gst_query_set_convert (query, src_fmt, src_val, dest_fmt, dest_val);
      res = TRUE;
      break;
    }
    default:
      res = GST_BASE_SRC_CLASS (parent_class)->query (basesrc, query);
      break;
  }

  return res;
  /* ERROR */
error:
  {
    GST_DEBUG_OBJECT (src, "query failed");
    return FALSE;
  }
}

static void
gst_jammo_metronome_dispose (GObject * object)
{
  G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
gst_jammo_metronome_finalize (GObject * object)
{
  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void init_sample_table (gint * sampletable, gint size, gfloat volume, gfloat wavelength) {
  gint i;
  gdouble ang = 0.0;
  gdouble step = M_PI_M2 / wavelength;
  gdouble amp = volume;

  for (i = 0; i < size; i++) {
    sampletable[i] = G_MAXINT16 * sin (ang) * amp;
    ang += step;
  }
}


static gfloat produce_sound(GstJammoMetronome * src) {

	//printf("producing sound\n");
	gfloat value=0.0;
	gfloat scale=1.0; /* This function uses only float type. Typecast in DEFINE_PROCESS */
//printf("oneshot\n");
		/* these temporary variables are used to keep the algorithms readable */
	gint * sampletable;
	if (src->next_accented) {
		sampletable=src->sampletable_accent;
	}
	else {
		sampletable=src->sampletable_normal;
	}

		/* if not finished playing */
		if (src->accumulator < src->sampletable_size) {
			/* Playing */
			src->playing=TRUE;
			value=( jammo_gst_do_interpolation(src->accumulator-(gint)src->accumulator, sampletable[(gint)src->accumulator], sampletable[((gint)src->accumulator+1)]) /(G_MAXINT16*1.0) * scale );
			/* move accumulator */
			/* 44100 is samplerate of sampletable.*/
			src->accumulator += 44100/(src->samplerate+0.0);

		/* End if not finished playing */
		}
		else {
			/* Finished playing */
			value=0.0;
			src->accumulator=0.0;
			src->playing=FALSE;
		}
	/*End sampler code */
	return value;
}


static void do_playback_process(GstJammoMetronome * src){

	if (src->time > src->next){
		(src->beat)++;

		//printf("DEBUG: gstjammometronome: delayed: %llu ms\n",(unsigned long long) (src->time - src->next)/1000/1000);
		src->accumulator=0.0;

		if (src->beat > src->beats) {
			src->beat=1;
		}

		if (src->accent && src->beat==1) {
			src->next_accented=TRUE;
		}
		else {
			src->next_accented=FALSE;
		}

		(src->next) += 60000000000LLU / ( (src->tempo/4.0) * src->note_value);

		// uncomment to see text output
		/*printf("beat %d / beats %d\n", src->beat, src->beats);
		printf("tempo %f, note_value %d\n", src->tempo, src->note_value);
		printf("now %llu, next click %llu\n", src->time, src->next);*/

		src->trigged=TRUE;
	}

}

#define DEFINE_PROCESS(type,scale) \
static void gst_jammo_metronome_process_##type (GstJammoMetronome * src, g##type * samples) { \
\
	do_playback_process(src); \
\
	gint i; \
	g##type value=0; \
\
	gdouble amp; \
	amp = src->volume * scale; \
\
	for(i=0; i<src->generate_samples_per_buffer; i++) { \
		if (src->trigged || src->playing) { \
			src->trigged=FALSE; \
			value=(g##type)(amp*produce_sound(src)); \
		} \
		else { \
			value=0; \
		} \
		samples[i]=value; \
	} \
}

DEFINE_PROCESS (int16, 32767.0)
DEFINE_PROCESS (int32, 2147483647.0)
DEFINE_PROCESS (float, 1.0)
DEFINE_PROCESS (double, 1.0)


static JammoMetronomeProcessFunc process_funcs[] = {
  (JammoMetronomeProcessFunc) gst_jammo_metronome_process_int16,
  (JammoMetronomeProcessFunc) gst_jammo_metronome_process_int32,
  (JammoMetronomeProcessFunc) gst_jammo_metronome_process_float,
  (JammoMetronomeProcessFunc) gst_jammo_metronome_process_double
};

static gboolean gst_jammo_metronome_setcaps (GstBaseSrc * basesrc, GstCaps * caps)
{
  GstJammoMetronome *src = GST_JAMMO_METRONOME (basesrc);
  const GstStructure *structure;
  const gchar *name;
  gint width;
  gboolean ret;

  structure = gst_caps_get_structure (caps, 0);
  ret = gst_structure_get_int (structure, "rate", &src->samplerate);

  GST_DEBUG_OBJECT (src, "negotiated to samplerate %d", src->samplerate);

  name = gst_structure_get_name (structure);
  if (strcmp (name, "audio/x-raw-int") == 0) {
    ret &= gst_structure_get_int (structure, "width", &width);
    src->format = (width == 32) ? GST_JAMMO_METRONOME_FORMAT_S32 :
        GST_JAMMO_METRONOME_FORMAT_S16;
  } else {
    ret &= gst_structure_get_int (structure, "width", &width);
    src->format = (width == 32) ? GST_JAMMO_METRONOME_FORMAT_F32 :
        GST_JAMMO_METRONOME_FORMAT_F64;
  }

  /* allocate a new buffer suitable for this pad */
  switch (src->format) {
    case GST_JAMMO_METRONOME_FORMAT_S16:
      printf("format is now gint16\n");
      src->sample_size = sizeof (gint16);
      break;
    case GST_JAMMO_METRONOME_FORMAT_S32:
      printf("format is now gint32\n");
      src->sample_size = sizeof (gint32);
      break;
    case GST_JAMMO_METRONOME_FORMAT_F32:
      printf("format is now gfloat32\n");
      src->sample_size = sizeof (gfloat);
      break;
    case GST_JAMMO_METRONOME_FORMAT_F64:
      printf("format is now gfloat32\n");
      src->sample_size = sizeof (gdouble);
      break;
    default:
      /* can't really happen */
      ret = FALSE;
      break;
  }

  if (src->format == -1) {
    src->process = NULL;
    return FALSE;
  }
  src->process = process_funcs[src->format];

  return ret;
}

static void gst_jammo_metronome_get_times (GstBaseSrc * basesrc, GstBuffer * buffer,
    GstClockTime * start, GstClockTime * end)
{
	GstJammoMetronome *src = GST_JAMMO_METRONOME (basesrc);

	GstClockTime timestamp = GST_BUFFER_TIMESTAMP (buffer);
	// store time of buffer, it is used in playback mode for processing events
	// and in recording mode for timestamping
	src->time = timestamp;

  /* for live sources, sync on the timestamp of the buffer */
  if (gst_base_src_is_live (basesrc)) {

    if (GST_CLOCK_TIME_IS_VALID (timestamp)) {
      /* get duration to calculate end time */
      GstClockTime duration = GST_BUFFER_DURATION (buffer);

      if (GST_CLOCK_TIME_IS_VALID (duration)) {
        *end = timestamp + duration;
      }
      *start = timestamp;
    }
  } else {
    *start = -1;
    *end = -1;
  }
}

static gboolean gst_jammo_metronome_start (GstBaseSrc * basesrc)
{
  GstJammoMetronome *src = GST_JAMMO_METRONOME (basesrc);

  printf("DEBUG:gstjammometronome: jammo_metronome_starts\n");

  src->next_sample = 0;
  src->next_byte = 0;
  src->next_time = 0;
  src->check_seek_stop = FALSE;
  src->eos_reached = FALSE;
  src->tags_pushed = FALSE;

  src->accumulator = 0;
	src->next=0;
	src->beat=0;

  return TRUE;
}

static gboolean gst_jammo_metronome_stop (GstBaseSrc * basesrc)
{
  return TRUE;
}

/* seek to time, will be called when we operate in push mode. In pull mode we
 * get the requiested byte offset. */
static gboolean gst_jammo_metronome_do_seek (GstBaseSrc * basesrc, GstSegment * segment)
{
  GstJammoMetronome *src = GST_JAMMO_METRONOME (basesrc);
  GstClockTime time_to_seek;

  segment->time = segment->start;
  time_to_seek = segment->last_stop;

  /* now move to the time indicated */
  src->next_sample =
      gst_util_uint64_scale_int (time_to_seek, src->samplerate, GST_SECOND);
  src->next_byte = src->next_sample * src->sample_size;
  src->next_time =
      gst_util_uint64_scale_int (src->next_sample, GST_SECOND, src->samplerate);

  g_assert (src->next_time <= time_to_seek);

  if (GST_CLOCK_TIME_IS_VALID (segment->stop)) {
    time_to_seek = segment->stop;
    src->sample_stop = gst_util_uint64_scale_int (time_to_seek, src->samplerate,
        GST_SECOND);
    src->check_seek_stop = TRUE;
  } else {
    src->check_seek_stop = FALSE;
  }
  src->eos_reached = FALSE;

  return TRUE;
}

static gboolean gst_jammo_metronome_is_seekable (GstBaseSrc * basesrc)
{
  /* we're seekable... */
  return TRUE;
}

static gboolean gst_jammo_metronome_check_get_range (GstBaseSrc * basesrc)
{
  GstJammoMetronome *src;

  src = GST_JAMMO_METRONOME (basesrc);

  /* if we can operate in pull mode */
  return src->can_activate_pull;
}

static GstFlowReturn gst_jammo_metronome_create (GstBaseSrc * basesrc, guint64 offset,
    guint length, GstBuffer ** buffer)
{
  GstFlowReturn res;
  GstJammoMetronome *src;
  GstBuffer *buf;
  GstClockTime next_time;
  gint64 next_sample, next_byte;
  guint bytes, samples;

  src = GST_JAMMO_METRONOME (basesrc);

  /* example for tagging generated data */
  if (!src->tags_pushed) {
    GstTagList *taglist;
    GstEvent *event;

    taglist = gst_tag_list_new ();

    gst_tag_list_add (taglist, GST_TAG_MERGE_APPEND,
        GST_TAG_DESCRIPTION, "jammo metronome", NULL);

    event = gst_event_new_tag (taglist);
    gst_pad_push_event (basesrc->srcpad, event);
    src->tags_pushed = TRUE;
  }

  if (src->eos_reached)
    return GST_FLOW_UNEXPECTED;

  /* if no length was given, use our default length in samples otherwise convert
   * the length in bytes to samples. */
  /*if (length == -1)
    samples = src->samples_per_buffer;
  else
    samples = length / src->sample_size;*/

	// we need to use smaller buffer than requested
	// otherwise there will be too much latency
	samples = src->samples_per_buffer;

  /* if no offset was given, use our next logical byte */
  if (offset == -1)
    offset = src->next_byte;

  /* now see if we are at the byteoffset we think we are */
  if (offset != src->next_byte) {
    GST_DEBUG_OBJECT (src, "seek to new offset %" G_GUINT64_FORMAT, offset);
    /* we have a discont in the expected sample offset, do a 'seek' */
    src->next_sample = src->next_byte / src->sample_size;
    src->next_time =
        gst_util_uint64_scale_int (src->next_sample, GST_SECOND,
        src->samplerate);
    src->next_byte = offset;
  }

  /* check for eos */
  if (src->check_seek_stop &&
      (src->sample_stop > src->next_sample) &&
      (src->sample_stop < src->next_sample + samples)
      ) {
    /* calculate only partial buffer */
    src->generate_samples_per_buffer = src->sample_stop - src->next_sample;
    next_sample = src->sample_stop;
    src->eos_reached = TRUE;
  } else {
    /* calculate full buffer */
    src->generate_samples_per_buffer = samples;
    next_sample = src->next_sample + samples;
  }

  bytes = src->generate_samples_per_buffer * src->sample_size;

  if ((res = gst_pad_alloc_buffer (basesrc->srcpad, src->next_sample,
              bytes, GST_PAD_CAPS (basesrc->srcpad), &buf)) != GST_FLOW_OK) {
    return res;
  }

  next_byte = src->next_byte + bytes;
  next_time = gst_util_uint64_scale_int (next_sample, GST_SECOND,
      src->samplerate);

  GST_LOG_OBJECT (src, "samplerate %d", src->samplerate);
  GST_LOG_OBJECT (src, "next_sample %" G_GINT64_FORMAT ", ts %" GST_TIME_FORMAT,
      next_sample, GST_TIME_ARGS (next_time));

  GST_BUFFER_TIMESTAMP (buf) = src->timestamp_offset + src->next_time;
  GST_BUFFER_OFFSET (buf) = src->next_sample;
  GST_BUFFER_OFFSET_END (buf) = next_sample;
  GST_BUFFER_DURATION (buf) = next_time - src->next_time;

  gst_object_sync_values (G_OBJECT (src), src->next_time);

  src->next_time = next_time;
  src->next_sample = next_sample;
  src->next_byte = next_byte;

  GST_LOG_OBJECT (src, "generating %u samples at ts %" GST_TIME_FORMAT,
      src->generate_samples_per_buffer,
      GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (buf)));

  src->process (src, GST_BUFFER_DATA (buf));

  if (G_UNLIKELY (src->volume == 0.0)) {
    GST_BUFFER_FLAG_SET (buf, GST_BUFFER_FLAG_GAP);
  }

  *buffer = buf;

  return GST_FLOW_OK;
}


static void
gst_jammo_metronome_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstJammoMetronome *src = GST_JAMMO_METRONOME (object);

  switch (prop_id) {
    case PROP_SAMPLES_PER_BUFFER:
      src->samples_per_buffer = g_value_get_int (value);
      break;
    case PROP_VOLUME:
      src->volume = g_value_get_double (value);
      break;
    case PROP_IS_LIVE:
      gst_base_src_set_live (GST_BASE_SRC (src), g_value_get_boolean (value));
      break;
    case PROP_TIMESTAMP_OFFSET:
      src->timestamp_offset = g_value_get_int64 (value);
      break;
    case PROP_CAN_ACTIVATE_PUSH:
      GST_BASE_SRC (src)->can_activate_push = g_value_get_boolean (value);
      break;
    case PROP_CAN_ACTIVATE_PULL:
      src->can_activate_pull = g_value_get_boolean (value);
      break;
		case PROP_TEMPO:
			// there is no need to check the value because limits are defined in properties	
			src->tempo = g_value_get_float (value);
			break;
		case PROP_TIME_SIGNATURE_BEATS:
			// there is no need to check the value because limits are defined in properties	
			src->beats = g_value_get_int (value);
			break;
		case PROP_TIME_SIGNATURE_NOTE_VALUE:
			// check that value is 1, 2, 4, 8, 16, 32 or 64
			if (g_value_get_int (value)!=1 && g_value_get_int (value)!=2 && g_value_get_int (value)!=4 && g_value_get_int (value)!=8 && g_value_get_int (value)!=16 && g_value_get_int (value)!=32 && g_value_get_int (value)!=64) {
				g_warning("Invalid note value %d for time signature\n", g_value_get_int (value));
			}
			else {
				src->note_value = g_value_get_int (value);
			}
			break;
		case PROP_ACCENT:
			src->accent = g_value_get_boolean (value);
			break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void gst_jammo_metronome_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstJammoMetronome *src = GST_JAMMO_METRONOME (object);

  switch (prop_id) {
    case PROP_SAMPLES_PER_BUFFER:
      g_value_set_int (value, src->samples_per_buffer);
      break;
    case PROP_VOLUME:
      g_value_set_double (value, src->volume);
      break;
    case PROP_IS_LIVE:
      g_value_set_boolean (value, gst_base_src_is_live (GST_BASE_SRC (src)));
      break;
    case PROP_TIMESTAMP_OFFSET:
      g_value_set_int64 (value, src->timestamp_offset);
      break;
    case PROP_CAN_ACTIVATE_PUSH:
      g_value_set_boolean (value, GST_BASE_SRC (src)->can_activate_push);
      break;
    case PROP_CAN_ACTIVATE_PULL:
      g_value_set_boolean (value, src->can_activate_pull);
      break;
    case PROP_TEMPO:
      g_value_set_float (value, src->tempo);
      break;
    case PROP_TIME_SIGNATURE_BEATS:
      g_value_set_int (value, src->beats);
      break;
    case PROP_TIME_SIGNATURE_NOTE_VALUE:
      g_value_set_int (value, src->note_value);
      break;
    case PROP_ACCENT:
      g_value_set_boolean (value, src->accent);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}
