/*This file is part of JamMo.
License:LGPL 2.1
 */

#ifndef __GST_JAMMO_METRONOME_H__
#define __GST_JAMMO_METRONOME_H__

#include <gst/gst.h>
#include <gst/base/gstbasesrc.h>

G_BEGIN_DECLS

#define GST_TYPE_JAMMO_METRONOME \
  (gst_jammo_metronome_get_type())
#define GST_JAMMO_METRONOME(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_JAMMO_METRONOME,GstJammoMetronome))
#define GST_JAMMO_METRONOME_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_JAMMO_METRONOME,GstJammoMetronomeClass))
#define GST_IS_JAMMO_METRONOME(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_JAMMO_METRONOME))
#define GST_IS_JAMMO_METRONOME_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_JAMMO_METRONOME))

typedef enum {
  GST_JAMMO_METRONOME_FORMAT_NONE = -1,
  GST_JAMMO_METRONOME_FORMAT_S16 = 0,
  GST_JAMMO_METRONOME_FORMAT_S32,
  GST_JAMMO_METRONOME_FORMAT_F32,
  GST_JAMMO_METRONOME_FORMAT_F64
} GstJammoMetronomeFormat;

typedef struct _GstJammoMetronome GstJammoMetronome;
typedef struct _GstJammoMetronomeClass GstJammoMetronomeClass;

typedef void (*JammoMetronomeProcessFunc) (GstJammoMetronome*, guint8 *);
//typedef float (*JammoSampleInstrumentFunc) (gdouble);


/**
 * GstJammoMetronome:
 * object structure.
 */
struct _GstJammoMetronome {
  GstBaseSrc parent;

  gboolean playback_not_started; 
  GstClockTime creation_time;

	GstClockTime time;

  JammoMetronomeProcessFunc process;

  /* parameters */
  gdouble volume;

  /* audio parameters */
  gint samplerate;
  gint samples_per_buffer;
  gint sample_size;
  GstJammoMetronomeFormat format;
  
  /*< private >*/
  gboolean tags_pushed;			/* send tags just once ? */
  GstClockTimeDiff timestamp_offset;    /* base offset */
  GstClockTime next_time;               /* next timestamp */
  gint64 next_sample;                   /* next sample to send */
  gint64 next_byte;                     /* next byte to send */
  gint64 sample_stop;
  gboolean check_seek_stop;
  gboolean eos_reached;
	gboolean last_event_processed;
  gint generate_samples_per_buffer;	/* used to generate a partial buffer */
  gboolean can_activate_pull;
  
  /* waveform specific context data */
  gdouble accumulator;			/* phase angle */
	gfloat tempo;
	gint beats, note_value;
	gboolean accent;
	gboolean next_accented;
	gint sampletable_size;
	gboolean trigged;
	gboolean playing;
	GstClockTime next;
	gint beat;
	gint sampletable_normal[1024];
	gint sampletable_accent[1024];

};

struct _GstJammoMetronomeClass {
  GstBaseSrcClass parent_class;
};

GType gst_jammo_metronome_get_type (void);

G_END_DECLS

#endif /* __GST_JAMMO_METRONOME_H__ */
