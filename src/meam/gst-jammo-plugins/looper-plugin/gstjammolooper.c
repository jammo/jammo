/* This file is part of JamMo.
License:LGPL 2.1

 */
/**
 * SECTION:element-jammolooper
 *
 * JammoLooper
 */

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <gst/controller/gstcontroller.h>

#include "../plugin-common.h"
#include "gstjammolooper.h"

#include "../../../configure.h" // for DATA_DIR

/*e.g. Nokia Internet Tablet n810 doesn't have this (maemo diablo)*/
#ifndef G_PARAM_STATIC_STRINGS
#define	G_PARAM_STATIC_STRINGS (G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB)
#endif

//#define SLIDER_FOLDER DATA_DIR "/virtual-instruments/slider/"

static const GstElementDetails gst_jammo_looper_details =
GST_ELEMENT_DETAILS ((gchar*)"JamMo Looper",
    (gchar*)"Source/Audio",
    (gchar*)"JamMo Looper",
    (gchar*)"Mikko Gynther <mikko.gynther@lut.fi>");

#define DEFAULT_SAMPLES_PER_BUFFER   256
#define DEFAULT_VOLUME               0.6
#define DEFAULT_IS_LIVE              FALSE
#define DEFAULT_TIMESTAMP_OFFSET     G_GINT64_CONSTANT (0)
#define DEFAULT_CAN_ACTIVATE_PUSH    TRUE
#define DEFAULT_CAN_ACTIVATE_PULL    FALSE

enum
{
  PROP_0,
  PROP_SAMPLES_PER_BUFFER,
  PROP_ADD_LOOP,
  PROP_REMOVE_LOOP,
  PROP_CLEAR_LOOPS,
  PROP_VOLUME,
  PROP_IS_LIVE,
  PROP_TIMESTAMP_OFFSET,
  PROP_CAN_ACTIVATE_PUSH,
  PROP_CAN_ACTIVATE_PULL,
	PROP_LOOPLIST,
	PROP_TEMPO,
	PROP_PITCH,
  PROP_LAST
};

static GstStaticPadTemplate gst_jammo_looper_src_template =
    GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-raw-int, "
        "endianness = (int) BYTE_ORDER, "
        "signed = (boolean) true, "
        "width = (int) 16, "
        "depth = (int) 16, "
        "rate = (int) [ 1, MAX ], "
        "channels = (int) 2; "
        "audio/x-raw-int, "
        "endianness = (int) BYTE_ORDER, "
        "signed = (boolean) true, "
        "width = (int) 32, "
        "depth = (int) 32,"
        "rate = (int) [ 1, MAX ], "
        "channels = (int) 2; "
        "audio/x-raw-float, "
        "endianness = (int) BYTE_ORDER, "
        "width = (int) { 32, 64 }, "
        "rate = (int) [ 1, MAX ], " "channels = (int) 2")
    );

GST_BOILERPLATE (GstJammoLooper, gst_jammo_looper, GstBaseSrc, GST_TYPE_BASE_SRC)

static void gst_jammo_looper_dispose (GObject * object);
static void gst_jammo_looper_finalize (GObject * object);

static void gst_jammo_looper_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec);
static void gst_jammo_looper_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);

static gboolean gst_jammo_looper_setcaps (GstBaseSrc * basesrc,
    GstCaps * caps);
static void gst_jammo_looper_src_fixate (GstPad * pad, GstCaps * caps);

static gboolean gst_jammo_looper_is_seekable (GstBaseSrc * basesrc);
static gboolean gst_jammo_looper_check_get_range (GstBaseSrc * basesrc);
static gboolean gst_jammo_looper_do_seek (GstBaseSrc * basesrc,
    GstSegment * segment);
static gboolean gst_jammo_looper_query (GstBaseSrc * basesrc,
    GstQuery * query);

static void gst_jammo_looper_get_times (GstBaseSrc * basesrc,
    GstBuffer * buffer, GstClockTime * start, GstClockTime * end);
static gboolean gst_jammo_looper_start (GstBaseSrc * basesrc);
static gboolean gst_jammo_looper_stop (GstBaseSrc * basesrc);
static GstFlowReturn gst_jammo_looper_create (GstBaseSrc * basesrc,
    guint64 offset, guint length, GstBuffer ** buffer);

static void free_loop_list(GList * list);

static void gst_jammo_looper_base_init (gpointer g_class)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&gst_jammo_looper_src_template));
  gst_element_class_set_details (element_class, &gst_jammo_looper_details);
}

static void gst_jammo_looper_class_init (GstJammoLooperClass * klass)
{
  GObjectClass *gobject_class;
  GstBaseSrcClass *gstbasesrc_class;

  gobject_class = (GObjectClass *) klass;
  gstbasesrc_class = (GstBaseSrcClass *) klass;

	gobject_class->dispose = gst_jammo_looper_dispose;  
	gobject_class->finalize = gst_jammo_looper_finalize;

  gobject_class->set_property = gst_jammo_looper_set_property;
  gobject_class->get_property = gst_jammo_looper_get_property;

  g_object_class_install_property (gobject_class, PROP_SAMPLES_PER_BUFFER,
      g_param_spec_int ("samplesperbuffer", "Samples per buffer",
          "Number of samples in each outgoing buffer",
          1, G_MAXINT, DEFAULT_SAMPLES_PER_BUFFER,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property(gobject_class, PROP_REMOVE_LOOP,
                                 g_param_spec_pointer ("remove-loop",
                                 "Removes a loop",
                                 "",
                                 G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
  g_object_class_install_property(gobject_class, PROP_ADD_LOOP,
                                 g_param_spec_pointer ("add-loop",
                                 "Add loop",
                                 "",
                                 G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
  g_object_class_install_property (gobject_class, PROP_CLEAR_LOOPS,
      g_param_spec_boolean ("clear-loops", "Clear loops",
          "Clear loop list", FALSE,
          G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_VOLUME,
      g_param_spec_double ("volume", "Volume", "Volume of test signal", 0.0,
          1.0, DEFAULT_VOLUME,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_IS_LIVE,
      g_param_spec_boolean ("is-live", "Is Live",
          "Whether to act as a live source", DEFAULT_IS_LIVE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_TIMESTAMP_OFFSET, g_param_spec_int64 ("timestamp-offset",
          "Timestamp offset",
          "An offset added to timestamps set on buffers (in ns)", G_MININT64,
          G_MAXINT64, DEFAULT_TIMESTAMP_OFFSET,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_CAN_ACTIVATE_PUSH,
      g_param_spec_boolean ("can-activate-push", "Can activate push",
          "Can activate in push mode", DEFAULT_CAN_ACTIVATE_PUSH,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_CAN_ACTIVATE_PULL,
      g_param_spec_boolean ("can-activate-pull", "Can activate pull",
          "Can activate in pull mode", DEFAULT_CAN_ACTIVATE_PULL,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_LOOPLIST,
      g_param_spec_pointer ("looplist", "A list of loops",
          "Get or set a list of loops", G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_TEMPO,
      g_param_spec_int ("tempo", "Tempo", "Tempo",
      		1, 250, 110,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_PITCH,
      g_param_spec_string ("pitch", "Pitch shift", "Pitch shift of produced sound",
          NULL,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));
  gstbasesrc_class->set_caps = GST_DEBUG_FUNCPTR (gst_jammo_looper_setcaps);
  gstbasesrc_class->is_seekable =
      GST_DEBUG_FUNCPTR (gst_jammo_looper_is_seekable);
  gstbasesrc_class->check_get_range =
      GST_DEBUG_FUNCPTR (gst_jammo_looper_check_get_range);
  gstbasesrc_class->do_seek = GST_DEBUG_FUNCPTR (gst_jammo_looper_do_seek);
  gstbasesrc_class->query = GST_DEBUG_FUNCPTR (gst_jammo_looper_query);
  gstbasesrc_class->get_times =
      GST_DEBUG_FUNCPTR (gst_jammo_looper_get_times);
  gstbasesrc_class->start = GST_DEBUG_FUNCPTR (gst_jammo_looper_start);
  gstbasesrc_class->stop = GST_DEBUG_FUNCPTR (gst_jammo_looper_stop);
  gstbasesrc_class->create = GST_DEBUG_FUNCPTR (gst_jammo_looper_create);
}

static void gst_jammo_looper_init (GstJammoLooper * src, GstJammoLooperClass * g_class)
{
	GstPad *pad = GST_BASE_SRC_PAD (src);

	gst_pad_set_fixatecaps_function (pad, gst_jammo_looper_src_fixate);

	src->samplerate = 44100;
	src->format = GST_JAMMO_LOOPER_FORMAT_NONE;

	src->volume = DEFAULT_VOLUME;

	src->time=0;

	src->pitch=g_strdup("D");
	src->tempo=110;
	src->loops=NULL;
	src->sample_index=0;
	src->loops_started=FALSE;

	src->previous_samples=DEFAULT_SAMPLES_PER_BUFFER;
	src->floatbuffer=g_malloc(sizeof(gfloat)*DEFAULT_SAMPLES_PER_BUFFER);

  /* we operate in time */
  gst_base_src_set_format (GST_BASE_SRC (src), GST_FORMAT_TIME);
  gst_base_src_set_live (GST_BASE_SRC (src), DEFAULT_IS_LIVE);

  src->samples_per_buffer = DEFAULT_SAMPLES_PER_BUFFER;
  src->generate_samples_per_buffer = src->samples_per_buffer;
  src->timestamp_offset = DEFAULT_TIMESTAMP_OFFSET;
  src->can_activate_pull = DEFAULT_CAN_ACTIVATE_PULL;

// gst_base_src_set_blocksize is since 0.10.22, maemo has only 0.10.13
// This works without this.
//  gst_base_src_set_blocksize (GST_BASE_SRC (src), -1)

}

static void gst_jammo_looper_src_fixate (GstPad * pad, GstCaps * caps)
{
  GstJammoLooper *src = GST_JAMMO_LOOPER (GST_PAD_PARENT (pad));
  const gchar *name;
  GstStructure *structure;

  structure = gst_caps_get_structure (caps, 0);

  GST_DEBUG_OBJECT (src, "fixating samplerate to %d", src->samplerate);

  gst_structure_fixate_field_nearest_int (structure, "rate", src->samplerate);

  name = gst_structure_get_name (structure);
  if (strcmp (name, "audio/x-raw-int") == 0)
    gst_structure_fixate_field_nearest_int (structure, "width", 32);
  else if (strcmp (name, "audio/x-raw-float") == 0)
    gst_structure_fixate_field_nearest_int (structure, "width", 64);
}



static gboolean gst_jammo_looper_query (GstBaseSrc * basesrc, GstQuery * query)
{
  GstJammoLooper *src = GST_JAMMO_LOOPER (basesrc);
  gboolean res = FALSE;

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_CONVERT:
    {
      GstFormat src_fmt, dest_fmt;
      gint64 src_val, dest_val;

      gst_query_parse_convert (query, &src_fmt, &src_val, &dest_fmt, &dest_val);
      if (src_fmt == dest_fmt) {
        dest_val = src_val;
        goto done;
      }

      switch (src_fmt) {
        case GST_FORMAT_DEFAULT:
          switch (dest_fmt) {
            case GST_FORMAT_TIME:
              /* samples to time */
              dest_val =
                  gst_util_uint64_scale_int (src_val, GST_SECOND,
                  src->samplerate);
              break;
            default:
              goto error;
          }
          break;
        case GST_FORMAT_TIME:
          switch (dest_fmt) {
            case GST_FORMAT_DEFAULT:
              /* time to samples */
              dest_val =
                  gst_util_uint64_scale_int (src_val, src->samplerate,
                  GST_SECOND);
              break;
            default:
              goto error;
          }
          break;
        default:
          goto error;
      }
    done:
      gst_query_set_convert (query, src_fmt, src_val, dest_fmt, dest_val);
      res = TRUE;
      break;
    }
    default:
      res = GST_BASE_SRC_CLASS (parent_class)->query (basesrc, query);
      break;
  }

  return res;
  /* ERROR */
error:
  {
    GST_DEBUG_OBJECT (src, "query failed");
    return FALSE;
  }
}

static void
gst_jammo_looper_dispose (GObject * object)
{
	GstJammoLooper *src = GST_JAMMO_LOOPER (object);

	if (src->loops) {
		free_loop_list(src->loops);
 		src->loops=NULL;
	}
	if (src->pitch) {
		g_free(src->pitch);
		src->pitch=NULL;
	}
	if (src->floatbuffer) {
		g_free(src->floatbuffer);
		src->floatbuffer=NULL;
	}

  G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
gst_jammo_looper_finalize (GObject * object)
{
  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static gchar* str_replace(gchar* string, gsize position, gsize length, const gchar* replacement, gchar** replacement_end_return) {
	gsize string_length;
	gsize replacement_length;
	gchar* s;
	
	string_length = strlen(string);
	replacement_length = strlen(replacement);
	s = (gchar*)g_malloc(string_length - length + replacement_length + 1);
	g_memmove(s, string, position);
	g_memmove(s + position, replacement, replacement_length);
	g_memmove(s + position + replacement_length, string + position + length, string_length - position - length + 1);

	if (replacement_end_return) {
		*replacement_end_return = s + position + replacement_length;
	}

	g_free(string);

	return s;
}

// LooperLoop includes internal stuff needed in playback
// in addition to JammoLooperLoop data
typedef struct _LooperLoop {
	guint64 start;
	guint64 original_start;
	gchar * filename;
	gchar * real_filename;
	SNDFILE * sndfileptr;
	SF_INFO sfinfo;
	gint file_position;
} LooperLoop;

static void open_loop_file(LooperLoop * loop, guint tempo, gchar * pitch) {
	gchar* filename;
	gchar* s;
	gchar* tempo_string;
	filename = s = g_strdup(loop->filename);
	while ((s = strchr(s, '$'))) {
		switch (s[1]) {
			case '$':
				filename = str_replace(filename, s - filename, 2, "$", &s);
				break;
			case 't':
				tempo_string = g_strdup_printf("%u", tempo);
				filename = str_replace(filename, s - filename, 2, tempo_string, &s);
				g_free(tempo_string);
				break;
			case 'p':
				filename = str_replace(filename, s - filename, 2, (pitch ? pitch : "?"), &s);
				break;
			default:
				g_warning("Unexpected parameter in the sample filename: '$%c', skipped.", s[1]);
				s += 2;
				break;
		}
	}
	if (g_strcmp0(loop->real_filename, filename)) {
		g_free(loop->real_filename);
		loop->real_filename = filename;
	}
	loop->sfinfo.format=0;
	loop->sndfileptr = sf_open(loop->real_filename, SFM_READ, &(loop->sfinfo));
}

static gint loop_compare(gconstpointer a, gconstpointer b) {
	LooperLoop *loop1, *loop2;

	if (!a) {
		return -1;
	}
	if (!b) {
		return 1;
	}

	loop1 = (LooperLoop*)a;
	loop2 = (LooperLoop*)b;
	if (loop1->start < loop2->start) {
		return -1;
	}
	else if (loop1->start > loop2->start) {
		return 1;
	}
	else {
		return 0;
	}
	return 0;
}

static void free_loop_list(GList * list) {
	gint i;
	LooperLoop * loop=NULL;
	for (i=0;i<g_list_length(list);i++) {
		loop = g_list_nth_data(list, i);
		if (loop->filename) {
			g_free(loop->filename);
		}
		if (loop->real_filename) {
			g_free(loop->real_filename);
		}
		if (loop->sndfileptr) {
			sf_close(loop->sndfileptr);
		}
		g_free(loop);
		loop=NULL;
	}
	g_list_free(list);
	list=NULL;
}

static void process_float(GstJammoLooper * src) {
	gint i, read_frames=0, new_frames=0;
	LooperLoop * loop=NULL;
	loop = (LooperLoop *)g_list_nth_data(src->loops, src->sample_index);

	for (i=0; i<src->generate_samples_per_buffer;i++) {
		src->floatbuffer[i]=0;
	}

	if (loop==NULL) {
		src->eos_reached=TRUE;
	}

	while (loop) {
		if (src->time <= loop->start) {
			break;
		}
		/* 1 channel. MONO */
		if (loop->sfinfo.channels==1) {
			new_frames=sf_read_float(loop->sndfileptr, &(src->floatbuffer[read_frames]), src->generate_samples_per_buffer/2-read_frames);
			// copy to two channels
			for (i=read_frames+new_frames-1; i>=read_frames; i--) {
				//Boundary check
				int index1=read_frames+2*i+1;
				if (index1>=src->generate_samples_per_buffer)
						index1=src->generate_samples_per_buffer-1;
				int index2=read_frames+2*i;
				if (index2>=src->generate_samples_per_buffer)
						index2=src->generate_samples_per_buffer-1;

				src->floatbuffer[index1]=src->floatbuffer[read_frames+i];
				src->floatbuffer[index2]=src->floatbuffer[read_frames+i];
			}
			//printf("last new %d, first new %d\n", read_frames+new_frames-1, read_frames);
			//printf("last stereo %d, first stereo %d\n", read_frames+new_frames-1+new_frames);
			read_frames+=new_frames*2;
		}
		else {
			/* 2 channels. STEREO */
			read_frames+=sf_read_float(loop->sndfileptr, &(src->floatbuffer[read_frames]), src->generate_samples_per_buffer-read_frames);
		}

		if (read_frames*2 >=  src->generate_samples_per_buffer) {
			break;
		}

		src->sample_index+=1;
		loop = (LooperLoop *)g_list_nth_data(src->loops, src->sample_index);
	}
}

#define DEFINE_PROCESS(type,scale) \
static void gst_jammo_looper_process_##type (GstJammoLooper * src, g##type * samples) {\
	gint i; \
/*	LooperLoop * loop=NULL; \
	loop = (LooperLoop *)g_list_nth_data(src->loops, src->sample_index); */\
\
	process_float(src); \
\
	/* 2 channels */ \
	for (i=0;i<src->generate_samples_per_buffer;i++) { \
		samples[i]=(g##type)src->floatbuffer[i]*scale*src->volume; \
	} \
}

DEFINE_PROCESS (int16, 32767.0)
DEFINE_PROCESS (int32, 2147483647.0)
DEFINE_PROCESS (float, 1.0)
DEFINE_PROCESS (double, 1.0)


static JammoLooperProcessFunc process_funcs[] = {
  (JammoLooperProcessFunc) gst_jammo_looper_process_int16,
  (JammoLooperProcessFunc) gst_jammo_looper_process_int32,
  (JammoLooperProcessFunc) gst_jammo_looper_process_float,
  (JammoLooperProcessFunc) gst_jammo_looper_process_double
};

static gboolean gst_jammo_looper_setcaps (GstBaseSrc * basesrc, GstCaps * caps)
{
  GstJammoLooper *src = GST_JAMMO_LOOPER (basesrc);
  const GstStructure *structure;
  const gchar *name;
  gint width;
  gboolean ret;

  structure = gst_caps_get_structure (caps, 0);
  ret = gst_structure_get_int (structure, "rate", &src->samplerate);

  GST_DEBUG_OBJECT (src, "negotiated to samplerate %d", src->samplerate);

  name = gst_structure_get_name (structure);
  if (strcmp (name, "audio/x-raw-int") == 0) {
    ret &= gst_structure_get_int (structure, "width", &width);
    src->format = (width == 32) ? GST_JAMMO_LOOPER_FORMAT_S32 :
        GST_JAMMO_LOOPER_FORMAT_S16;
  } else {
    ret &= gst_structure_get_int (structure, "width", &width);
    src->format = (width == 32) ? GST_JAMMO_LOOPER_FORMAT_F32 :
        GST_JAMMO_LOOPER_FORMAT_F64;
  }

  /* allocate a new buffer suitable for this pad */
  switch (src->format) {
    case GST_JAMMO_LOOPER_FORMAT_S16:
      //printf("format is now gint16\n");
      src->sample_size = sizeof (gint16);
      break;
    case GST_JAMMO_LOOPER_FORMAT_S32:
      //printf("format is now gint32\n");
      src->sample_size = sizeof (gint32);
      break;
    case GST_JAMMO_LOOPER_FORMAT_F32:
      //printf("format is now gfloat32\n");
      src->sample_size = sizeof (gfloat);
      break;
    case GST_JAMMO_LOOPER_FORMAT_F64:
      //printf("format is now gfloat32\n");
      src->sample_size = sizeof (gdouble);
      break;
    default:
      /* can't really happen */
      ret = FALSE;
      break;
  }

  if (src->format == -1) {
    src->process = NULL;
    return FALSE;
  }
  src->process = process_funcs[src->format];

  return ret;
}

static void gst_jammo_looper_get_times (GstBaseSrc * basesrc, GstBuffer * buffer,
    GstClockTime * start, GstClockTime * end)
{

	GstJammoLooper * src = GST_JAMMO_LOOPER(basesrc);

	GstClockTime timestamp = GST_BUFFER_TIMESTAMP (buffer);
	src->time=timestamp;
  /* for live sources, sync on the timestamp of the buffer */
  if (gst_base_src_is_live (basesrc)) {
    

    if (GST_CLOCK_TIME_IS_VALID (timestamp)) {
      /* get duration to calculate end time */
      GstClockTime duration = GST_BUFFER_DURATION (buffer);

      if (GST_CLOCK_TIME_IS_VALID (duration)) {
        *end = timestamp + duration;
      }
      *start = timestamp;
    }
  } else {
    *start = -1;
    *end = -1;
  }
}

static void rewind_loops(GList * loops) {
	GList * l=NULL;
	LooperLoop * loop=NULL;
	for (l=loops;l;l=l->next) {
		loop = (LooperLoop*)l->data;
		sf_seek(loop->sndfileptr, 0, SEEK_SET);
	}
}

static gboolean gst_jammo_looper_start (GstBaseSrc * basesrc)
{
  GstJammoLooper *src = GST_JAMMO_LOOPER (basesrc);

  src->next_sample = 0;
  src->next_byte = 0;
  src->next_time = 0;
  src->check_seek_stop = FALSE;
  src->eos_reached = FALSE;
  src->tags_pushed = FALSE;
  src->time = 0;
  src->sample_index=0;
  if (src->loops_started) {
	 	rewind_loops(src->loops);
	}
 	src->loops_started=TRUE;

  return TRUE;
}

static gboolean gst_jammo_looper_stop (GstBaseSrc * basesrc)
{
	GstJammoLooper *src = GST_JAMMO_LOOPER (basesrc);
	rewind_loops(src->loops);
	src->loops_started=FALSE;
  return TRUE;
}

/* seek to time, will be called when we operate in push mode. In pull mode we
 * get the requiested byte offset. */
static gboolean gst_jammo_looper_do_seek (GstBaseSrc * basesrc, GstSegment * segment)
{
  GstJammoLooper *src = GST_JAMMO_LOOPER (basesrc);
  GstClockTime time_to_seek;

  segment->time = segment->start;
  time_to_seek = segment->last_stop;

	src->time = segment->time;
	src->sample_index = 0;
	LooperLoop * loop=NULL;
	GList * l=NULL;

	rewind_loops(src->loops);

	for (l=src->loops;l;l=l->next) {
		loop = (LooperLoop*)l->data;
		if (loop) {
			if (loop->start + (loop->sfinfo.frames)*1000000000LLU/src->samplerate <= src->time ) {
				;
			}
			else if (loop->start <= src->time && loop->start+(loop->sfinfo.frames)*1000000000LLU/src->samplerate >= src->time) {
				loop->file_position = (src->time - loop->start) * src->samplerate / 1000000000LLU ;
				sf_seek(loop->sndfileptr, loop->file_position, SEEK_SET);
				break;
			}
			else if (loop->start > src->time) {
				break;
			}
		}
		else {
			break;
		}
		src->sample_index += 1;
	}

  /* now move to the time indicated */
  src->next_sample =
      gst_util_uint64_scale_int (time_to_seek, src->samplerate, GST_SECOND);
  src->next_byte = src->next_sample * src->sample_size;
  src->next_time =
      gst_util_uint64_scale_int (src->next_sample, GST_SECOND, src->samplerate);

  g_assert (src->next_time <= time_to_seek);

  if (GST_CLOCK_TIME_IS_VALID (segment->stop)) {
    time_to_seek = segment->stop;
    src->sample_stop = gst_util_uint64_scale_int (time_to_seek, src->samplerate,
        GST_SECOND);
    src->check_seek_stop = TRUE;
  } else {
    src->check_seek_stop = FALSE;
  }
  src->eos_reached = FALSE;

  return TRUE;
}

static gboolean gst_jammo_looper_is_seekable (GstBaseSrc * basesrc)
{
  /* we're seekable... */
  return TRUE;
}

static gboolean gst_jammo_looper_check_get_range (GstBaseSrc * basesrc)
{
  GstJammoLooper *src;

  src = GST_JAMMO_LOOPER (basesrc);

  /* if we can operate in pull mode */
  return src->can_activate_pull;
}

static GstFlowReturn gst_jammo_looper_create (GstBaseSrc * basesrc, 
    guint64 offset, guint length, GstBuffer ** buffer)
{
  GstFlowReturn res;
  GstJammoLooper *src;
  GstBuffer *buf;
  GstClockTime next_time;
  gint64 next_sample, next_byte;
  guint bytes, samples;

  src = GST_JAMMO_LOOPER (basesrc);

  /* example for tagging generated data */
  if (!src->tags_pushed) {
    GstTagList *taglist;
    GstEvent *event;

    taglist = gst_tag_list_new ();

    gst_tag_list_add (taglist, GST_TAG_MERGE_APPEND,
        GST_TAG_DESCRIPTION, "jammo looper", NULL);

    event = gst_event_new_tag (taglist);
    gst_pad_push_event (basesrc->srcpad, event);
    src->tags_pushed = TRUE;
  }

  if (src->eos_reached)
    return GST_FLOW_UNEXPECTED;

  /* if no length was given, use our default length in samples otherwise convert
   * the length in bytes to samples. */
  if (length == -1)
    samples = src->samples_per_buffer;
  else
    samples = length / src->sample_size;

	if (samples != src->previous_samples) {
		if (src->floatbuffer) {
			g_free(src->floatbuffer);
		}
		src->floatbuffer=g_malloc(sizeof(gfloat)*samples);
		src->previous_samples=samples;
	}

  /* if no offset was given, use our next logical byte */
  if (offset == -1)
    offset = src->next_byte;

  /* now see if we are at the byteoffset we think we are */
  if (offset != src->next_byte) {
    GST_DEBUG_OBJECT (src, "seek to new offset %" G_GUINT64_FORMAT, offset);
    /* we have a discont in the expected sample offset, do a 'seek' */
    src->next_sample = src->next_byte / src->sample_size;
    src->next_time =
        gst_util_uint64_scale_int (src->next_sample, GST_SECOND,
        src->samplerate);
    src->next_byte = offset;
  }

  /* check for eos */
  if (src->check_seek_stop &&
      (src->sample_stop > src->next_sample) &&
      (src->sample_stop < src->next_sample + samples)
      ) {
    /* calculate only partial buffer */
    src->generate_samples_per_buffer = src->sample_stop - src->next_sample;
    next_sample = src->sample_stop;
    src->eos_reached = TRUE;
  } else {
    /* calculate full buffer */
    src->generate_samples_per_buffer = samples;
    next_sample = src->next_sample + samples;
  }

  bytes = src->generate_samples_per_buffer * src->sample_size;

  if ((res = gst_pad_alloc_buffer (basesrc->srcpad, src->next_sample,
              bytes, GST_PAD_CAPS (basesrc->srcpad), &buf)) != GST_FLOW_OK) {
    return res;
  }

  next_byte = src->next_byte + bytes;
  next_time = gst_util_uint64_scale_int (next_sample, GST_SECOND,
      src->samplerate);

  GST_LOG_OBJECT (src, "samplerate %d", src->samplerate);
  GST_LOG_OBJECT (src, "next_sample %" G_GINT64_FORMAT ", ts %" GST_TIME_FORMAT,
      next_sample, GST_TIME_ARGS (next_time));

  GST_BUFFER_TIMESTAMP (buf) = src->timestamp_offset + src->next_time;
  GST_BUFFER_OFFSET (buf) = src->next_sample;
  GST_BUFFER_OFFSET_END (buf) = next_sample;
  GST_BUFFER_DURATION (buf) = next_time - src->next_time;

  gst_object_sync_values (G_OBJECT (src), src->next_time);

  src->next_time = next_time;
  src->next_sample = next_sample;
  src->next_byte = next_byte;

  GST_LOG_OBJECT (src, "generating %u samples at ts %" GST_TIME_FORMAT,
      src->generate_samples_per_buffer,
      GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (buf)));

  src->process (src, GST_BUFFER_DATA (buf));

  if (G_UNLIKELY (src->volume == 0.0)) {
    GST_BUFFER_FLAG_SET (buf, GST_BUFFER_FLAG_GAP);
  }

  *buffer = buf;

  return GST_FLOW_OK;
}

static void gst_jammo_looper_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstJammoLooper *src = GST_JAMMO_LOOPER (object);

  switch (prop_id) {
    case PROP_SAMPLES_PER_BUFFER:
      src->samples_per_buffer = g_value_get_int (value);
      break;
    case PROP_VOLUME:
      src->volume = g_value_get_double (value);
      break;
    case PROP_IS_LIVE:
      gst_base_src_set_live (GST_BASE_SRC (src), g_value_get_boolean (value));
      break;
    case PROP_TIMESTAMP_OFFSET:
      src->timestamp_offset = g_value_get_int64 (value);
      break;
    case PROP_CAN_ACTIVATE_PUSH:
      GST_BASE_SRC (src)->can_activate_push = g_value_get_boolean (value);
      break;
    case PROP_CAN_ACTIVATE_PULL:
      src->can_activate_pull = g_value_get_boolean (value);
      break;

		case PROP_REMOVE_LOOP: {
				// removes based on start time only
				gint i;
				LooperLoop * loop=NULL;
				JammoLooperLoop* removed_loop=g_value_get_pointer(value);
				for (i=0;i<g_list_length(src->loops);i++) {
					loop = g_list_nth_data(src->loops, i);
					if (loop->original_start==removed_loop->start) {
						src->loops=g_list_remove(src->loops, loop);
						if (loop->filename) {
							g_free(loop->filename);
							loop->filename=NULL;
						}
						if (loop->real_filename) {
							g_free(loop->real_filename);
							loop->real_filename=NULL;
						}
						if (loop->sndfileptr) {
							sf_close(loop->sndfileptr);
							loop->sndfileptr=NULL;
						}
						g_free(loop);
						loop=NULL;
						break;
					}
				}
			}
			break;
    case PROP_ADD_LOOP: {
  	  	JammoLooperLoop * loop = g_value_get_pointer(value);
  	  	LooperLoop * add_loop = g_malloc(sizeof(LooperLoop));
  	  	memset(add_loop, 0, sizeof(LooperLoop));
  	  	add_loop->start=loop->start*2; //This is needed. I don't know why.
  	  	add_loop->original_start=loop->start; //This is not multiplied, it is used for removing samples
  	  	add_loop->filename=g_strdup(loop->filename);
  	  	open_loop_file(add_loop, src->tempo, src->pitch);
	    	src->loops=g_list_insert_sorted(src->loops, add_loop, loop_compare);
    	}
			break;
    case PROP_CLEAR_LOOPS:
	    if (src->loops) {
  	 		free_loop_list(src->loops);
  	 		src->loops=NULL;
  	 	}
      break;
   case PROP_LOOPLIST:
   		/*if (src->loops) {
   			free_loop_list(src->loops);
   			src->loops=NULL;
   		}*/
   		g_warning("loop list property not implemented");
			break;
    case PROP_TEMPO: {
		    gint i;
		    guint tempo;
		    LooperLoop * loop=NULL;
		    tempo=g_value_get_int (value);
				for (i=0;i<g_list_length(src->loops);i++) {
					loop = g_list_nth_data(src->loops, i);
					sf_close(loop->sndfileptr);
					open_loop_file(loop, tempo, src->pitch);
					loop->start *= (gdouble)src->tempo / (gdouble)tempo;
				}
      	src->tempo = tempo;
      	//printf("tempo changed to %u\n", src->tempo);
			}
      break;
    case PROP_PITCH: {
				gint i;
				LooperLoop * loop=NULL;
		  	if (src->pitch) {
		  		g_free(src->pitch);
		  		src->pitch=NULL;
		  	}
				if (src->pitch)
					g_free(src->pitch);
		    src->pitch = g_strdup_printf("%s",g_value_get_string(value));
		    //printf("pitch changed to %s\n", src->pitch);
				for (i=0;i<g_list_length(src->loops);i++) {
					loop = g_list_nth_data(src->loops, i);
					sf_close(loop->sndfileptr);
					open_loop_file(loop, src->tempo, src->pitch);
				}
			}
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void gst_jammo_looper_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstJammoLooper *src = GST_JAMMO_LOOPER (object);

  switch (prop_id) {
    case PROP_SAMPLES_PER_BUFFER:
      g_value_set_int (value, src->samples_per_buffer);
      break;
    case PROP_VOLUME:
      g_value_set_double (value, src->volume);
      break;
    case PROP_IS_LIVE:
      g_value_set_boolean (value, gst_base_src_is_live (GST_BASE_SRC (src)));
      break;
    case PROP_TIMESTAMP_OFFSET:
      g_value_set_int64 (value, src->timestamp_offset);
      break;
    case PROP_CAN_ACTIVATE_PUSH:
      g_value_set_boolean (value, GST_BASE_SRC (src)->can_activate_push);
      break;
    case PROP_CAN_ACTIVATE_PULL:
      g_value_set_boolean (value, src->can_activate_pull);
      break;
		case PROP_LOOPLIST:
			g_value_set_pointer (value, src->eventlist);
			break;
		case PROP_TEMPO:
			g_value_set_int (value, src->tempo);
			break;
		case PROP_PITCH:
			g_value_set_string (value, src->pitch);
			break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;

  }
}
