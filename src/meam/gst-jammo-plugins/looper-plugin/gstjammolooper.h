/*This file is part of JamMo.
License:LGPL 2.1
 */

#ifndef __GST_JAMMO_LOOPER_H__
#define __GST_JAMMO_LOOPER_H__

#include <gst/gst.h>
#include <gst/base/gstbasesrc.h>
#include <sndfile.h>
#include "../../jammo-looper-loop.h"


G_BEGIN_DECLS

#define GST_TYPE_JAMMO_LOOPER \
  (gst_jammo_looper_get_type())
#define GST_JAMMO_LOOPER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_JAMMO_LOOPER,GstJammoLooper))
#define GST_JAMMO_LOOPER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_JAMMO_LOOPER,GstJammoLooperClass))
#define GST_IS_JAMMO_LOOPER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_JAMMO_LOOPER))
#define GST_IS_JAMMO_LOOPER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_JAMMO_LOOPER))

typedef enum {
  GST_JAMMO_LOOPER_FORMAT_NONE = -1,
  GST_JAMMO_LOOPER_FORMAT_S16 = 0,
  GST_JAMMO_LOOPER_FORMAT_S32,
  GST_JAMMO_LOOPER_FORMAT_F32,
  GST_JAMMO_LOOPER_FORMAT_F64
} GstJammoLooperFormat;


typedef struct _GstJammoLooper GstJammoLooper;
typedef struct _GstJammoLooperClass GstJammoLooperClass;

typedef void (*JammoLooperProcessFunc) (GstJammoLooper*, guint8 *);
//typedef gfloat (*JammoSliderInstrumentFunc) (GstJammoSlider*, gdouble);

/**
 * GstJammoLooper:
 * object structure.
 */
struct _GstJammoLooper {
  GstBaseSrc parent;

  JammoLooperProcessFunc process;

  /* audio parameters */
  gint samplerate;
  gint samples_per_buffer;
  gint sample_size;
  GstJammoLooperFormat format;
  
  /*< private >*/
  gboolean tags_pushed;			/* send tags just once ? */
  GstClockTimeDiff timestamp_offset;    /* base offset */
  GstClockTime next_time;               /* next timestamp */
  gint64 next_sample;                   /* next sample to send */
  gint64 next_byte;                     /* next byte to send */
  gint64 sample_stop;
  gboolean check_seek_stop;
  gboolean eos_reached;
  gint generate_samples_per_buffer;	/* used to generate a partial buffer */
  gboolean can_activate_pull;
  gint previous_samples;

	GstClockTime time; /* used for playback and timestamps */
	gboolean recording;
	GList * eventlist;
	gint playback_index_for_event;
	gboolean last_event_processed;
  
  /* Looper data */
  gfloat volume;
	guint tempo;
	gchar * pitch;	
	GList * loops;
	gint sample_index;
	gfloat * floatbuffer;
	gboolean loops_started;
};

struct _GstJammoLooperClass {
  GstBaseSrcClass parent_class;
};

GType gst_jammo_looper_get_type (void);

G_END_DECLS

#endif /* __GST_JAMMO_LOOPER_H__ */
