/* This file is part of JamMo.
License:LGPL 2.1

 */
/**
 * SECTION:element-jammoslider
 *
 * JammoSlider
 */

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <gst/controller/gstcontroller.h>

#include "../plugin-common.h"
#include "gstjammoslider.h"

#include "../../../configure.h" // for DATA_DIR

/*e.g. Nokia Internet Tablet n810 doesn't have this (maemo diablo)*/
#ifndef G_PARAM_STATIC_STRINGS
#define	G_PARAM_STATIC_STRINGS (G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB)
#endif

#ifndef M_PI
#define M_PI  3.14159265358979323846
#endif

#ifndef M_PI_2
#define M_PI_2  1.57079632679489661923
#endif

#define M_PI_M2 ( M_PI + M_PI )

// time that is the threshold for discarding motion events
// there is no need to store all motion events that are received really closely
#define DISCARDTIME 1000000

// minimum frequency to slide between samples if sliding is needed
#define MINSLIDE 0.002

// multiplier for difference of target and current frequency between samples
#define DISTANCEFACTOR 0.00076

// sample count for rounding
#define ROUNDINGCOUNT 10000

// volume of blow sound with FM_MODULATION algorithm
#define BLOW_VOLUME 0.25

#define SLIDER_FOLDER AUDIO_DIR "/virtual-instruments/slider/"

static const GstElementDetails gst_jammo_slider_details =
GST_ELEMENT_DETAILS ((gchar*)"JamMo Slider",
    (gchar*)"Source/Audio",
    (gchar*)"JamMo Slider",
    (gchar*)"Aapo Rantalainen <aapo.rantalainen@gmail.com>, Mikko Gynther <mikko.gynther@lut.fi>");

#define DEFAULT_SAMPLES_PER_BUFFER   256
#define DEFAULT_INSTRUMENT           0
#define DEFAULT_VOLUME               0.6
#define DEFAULT_IS_LIVE              FALSE
#define DEFAULT_TIMESTAMP_OFFSET     G_GINT64_CONSTANT (0)
#define DEFAULT_CAN_ACTIVATE_PUSH    TRUE
#define DEFAULT_CAN_ACTIVATE_PULL    FALSE
#define NUMBER_OF_INSTRUMENTS	     sizeof(instruments)/sizeof(*instruments)

//This are for tuning slider
#define DEFAULT_ATTACK 0.2
#define DEFAULT_DECAY 0.2
#define DEFAULT_SUSTAIN 0.5
#define DEFAULT_RELEASE 0.2

enum
{
  PROP_0,
  PROP_SAMPLES_PER_BUFFER,
  PROP_INSTRUMENT,
  PROP_ADD_EVENT,
  PROP_REMOVE_EVENT,
  PROP_CLEAR_EVENTS,
  PROP_ATTACK,
  PROP_DECAY,
  PROP_SUSTAIN,
  PROP_RELEASE,
  PROP_VOLUME,
  PROP_IS_LIVE,
  PROP_TIMESTAMP_OFFSET,
  PROP_CAN_ACTIVATE_PUSH,
  PROP_CAN_ACTIVATE_PULL,
	PROP_RECORDING,
	PROP_EVENTLIST,
	PROP_TEMPO_SCALE,
	PROP_PITCH_SHIFT,
  PROP_LAST
};

static GstStaticPadTemplate gst_jammo_slider_src_template =
    GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-raw-int, "
        "endianness = (int) BYTE_ORDER, "
        "signed = (boolean) true, "
        "width = (int) 16, "
        "depth = (int) 16, "
        "rate = (int) [ 1, MAX ], "
        "channels = (int) 1; "
        "audio/x-raw-int, "
        "endianness = (int) BYTE_ORDER, "
        "signed = (boolean) true, "
        "width = (int) 32, "
        "depth = (int) 32,"
        "rate = (int) [ 1, MAX ], "
        "channels = (int) 1; "
        "audio/x-raw-float, "
        "endianness = (int) BYTE_ORDER, "
        "width = (int) { 32, 64 }, "
        "rate = (int) [ 1, MAX ], " "channels = (int) 1")
    );

GST_BOILERPLATE (GstJammoSlider, gst_jammo_slider, GstBaseSrc, GST_TYPE_BASE_SRC)

static void gst_jammo_slider_dispose (GObject * object);
static void gst_jammo_slider_finalize (GObject * object);

static void gst_jammo_slider_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec);
static void gst_jammo_slider_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);

static gboolean gst_jammo_slider_setcaps (GstBaseSrc * basesrc,
    GstCaps * caps);
static void gst_jammo_slider_src_fixate (GstPad * pad, GstCaps * caps);

static gboolean gst_jammo_slider_is_seekable (GstBaseSrc * basesrc);
static gboolean gst_jammo_slider_check_get_range (GstBaseSrc * basesrc);
static gboolean gst_jammo_slider_do_seek (GstBaseSrc * basesrc,
    GstSegment * segment);
static gboolean gst_jammo_slider_query (GstBaseSrc * basesrc,
    GstQuery * query);

static void gst_jammo_slider_get_times (GstBaseSrc * basesrc,
    GstBuffer * buffer, GstClockTime * start, GstClockTime * end);
static gboolean gst_jammo_slider_start (GstBaseSrc * basesrc);
static gboolean gst_jammo_slider_stop (GstBaseSrc * basesrc);
static GstFlowReturn gst_jammo_slider_create (GstBaseSrc * basesrc,
    guint64 offset, guint length, GstBuffer ** buffer);


static void gst_jammo_slider_base_init (gpointer g_class)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&gst_jammo_slider_src_template));
  gst_element_class_set_details (element_class, &gst_jammo_slider_details);
}

static void init_sine_table(gfloat * sine_table, gint size) {
	gint i;
	for (i=0;i<size;i++) {
		sine_table[i]=sinf(i/(gfloat)size * M_PI_M2);
	}
}

/* Very simple organ type sound additive synthesis algorithm.*/
static void init_organ_table(gfloat * organ_table, gint size) {
	gint i;
	for (i=0;i<size;i++) {
		gfloat scaled = (i/(gfloat)size * M_PI_M2);  //M_PI_M2= 2*pi
		organ_table[i]= 0.6 * (
            sinf(scaled  ) +
     1.0/6 *sinf(scaled*2) +
     1.0/3 *sinf(scaled*3) +
     1.0/4 *sinf(scaled*4) +
     1.0/3 *sinf(scaled*5) +
     1.0/6 *sinf(scaled*6) +
     1.0/6 *sinf(scaled*7) +
     1.0/4 *sinf(scaled*8)
     );
	}
}

/* fm synthesis algorithm */
static void init_fm_table(gfloat * fm_table, gint size) {
	gint i;
	for (i=0;i<size;i++) {
		gfloat scaled = (i/(gfloat)size * M_PI_M2);  //M_PI_M2= 2*pi
		fm_table[i]=(
            sinf(scaled) +
       0.6 *sinf(scaled*8)
    );
	}
}

static gfloat sine_value(GstJammoSlider * src, gfloat phase) {
	while (phase>M_PI_M2) phase-=M_PI_M2;
	return src->sine_table[(gint)(phase/M_PI_M2*src->table_size)];
}

/*
Some day we will have three different instrument using slider.
Organ and fm_modulation are only for testing.
*/

static gfloat organ(GstJammoSlider * src, gdouble phase){
	while (phase>M_PI_M2) phase-=M_PI_M2;
	return src->organ_table[(gint)(phase/M_PI_M2*src->table_size)];
}


static gfloat fm_modulation(GstJammoSlider * src, gdouble phase){
	while (phase>M_PI_M2) phase-=M_PI_M2;
	return src->fm_table[(gint)(phase/M_PI_M2*src->table_size)];
}




#define MAXDEL  (2048)       /* MUST be a power of 2. */
#define BITMASK (MAXDEL-1)  /* Circular buffer. */
static short    y[MAXDEL];
static long     ap=0;
static short    n=0;

static short karplus_strong (short x, gfloat length) {
  long            a, b;       /* No need to remember a and b. */

  a = (long)x + jammo_gst_do_interpolation(length-(int)length, y[((gint)(n - length)) & BITMASK],y[((gint)(n - (length+1))) & BITMASK]); /* interpolation */

  //b = ((a + ap) * 127) >> 8;            /* Coeff = +0.49609375. */
    b = ((a + ap) * 127) / (255);
    ap = a;                               /* Remember. */
    y[n++ & BITMASK] = (short)b;          /* Write in buffer. */
    return (short)b;
}


static gfloat karplus(GstJammoSlider * src){
  gfloat freq = src->freq_slow;
  short inp;
  if (src->adsr_counter < src->samplerate/freq)  /* white noise BURST. */
      inp = (short)((rand() & 32767) - 16384);
  else                                           /* No more input (silence). */
      inp = 0;

return karplus_strong(inp, src->samplerate/freq)/32767.0; //short divided by float
}

static JammoSliderInstrumentFunc instruments[] = {
  (JammoSliderInstrumentFunc) organ,
  (JammoSliderInstrumentFunc) fm_modulation,
  (JammoSliderInstrumentFunc) karplus
};


static void gst_jammo_slider_class_init (GstJammoSliderClass * klass)
{
  GObjectClass *gobject_class;
  GstBaseSrcClass *gstbasesrc_class;

  gobject_class = (GObjectClass *) klass;
  gstbasesrc_class = (GstBaseSrcClass *) klass;

	gobject_class->dispose = gst_jammo_slider_dispose;  
	gobject_class->finalize = gst_jammo_slider_finalize;

  gobject_class->set_property = gst_jammo_slider_set_property;
  gobject_class->get_property = gst_jammo_slider_get_property;

  g_object_class_install_property (gobject_class, PROP_SAMPLES_PER_BUFFER,
      g_param_spec_int ("samplesperbuffer", "Samples per buffer",
          "Number of samples in each outgoing buffer",
          1, G_MAXINT, DEFAULT_SAMPLES_PER_BUFFER,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_INSTRUMENT,
      g_param_spec_int ("instrument", "Virtual Instrument",
          "What instrument is used",
          0, NUMBER_OF_INSTRUMENTS-1, DEFAULT_INSTRUMENT,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property(gobject_class, PROP_REMOVE_EVENT,
                                 g_param_spec_pointer ("remove-event",
                                 "Removes both start and stop events",
                                 "",
                                 G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
  g_object_class_install_property(gobject_class, PROP_ADD_EVENT,
                                 g_param_spec_pointer ("add-event",
                                 "Add event",
                                 "",
                                 G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
  g_object_class_install_property (gobject_class, PROP_CLEAR_EVENTS,
      g_param_spec_boolean ("clear-events", "Clear events",
          "Clear event list", FALSE,
          G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS));
 g_object_class_install_property (gobject_class, PROP_ATTACK,
      g_param_spec_float ("attack", "attack",
          "Attack in seconds",
          0.0, 20.0, DEFAULT_ATTACK,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
 g_object_class_install_property (gobject_class, PROP_DECAY,
      g_param_spec_float ("decay", "decay",
          "Decay in seconds",
          0.0, 20.0, DEFAULT_DECAY,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
 g_object_class_install_property (gobject_class, PROP_RELEASE,
      g_param_spec_float ("release", "release",
          "Release in seconds",
          0.0, 20.0, DEFAULT_RELEASE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
 g_object_class_install_property (gobject_class, PROP_SUSTAIN,
      g_param_spec_float ("sustain", "sustain", "sustain",
          0.0,1.0,DEFAULT_SUSTAIN,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS ));
  g_object_class_install_property (gobject_class, PROP_VOLUME,
      g_param_spec_double ("volume", "Volume", "Volume of test signal", 0.0,
          1.0, DEFAULT_VOLUME,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_IS_LIVE,
      g_param_spec_boolean ("is-live", "Is Live",
          "Whether to act as a live source", DEFAULT_IS_LIVE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_TIMESTAMP_OFFSET, g_param_spec_int64 ("timestamp-offset",
          "Timestamp offset",
          "An offset added to timestamps set on buffers (in ns)", G_MININT64,
          G_MAXINT64, DEFAULT_TIMESTAMP_OFFSET,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_CAN_ACTIVATE_PUSH,
      g_param_spec_boolean ("can-activate-push", "Can activate push",
          "Can activate in push mode", DEFAULT_CAN_ACTIVATE_PUSH,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_CAN_ACTIVATE_PULL,
      g_param_spec_boolean ("can-activate-pull", "Can activate pull",
          "Can activate in pull mode", DEFAULT_CAN_ACTIVATE_PULL,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_RECORDING,
      g_param_spec_boolean ("recording", "Recording",
          "Recording mode", TRUE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_EVENTLIST,
      g_param_spec_pointer ("eventlist", "A list of events",
          "Get or set a list of events", G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_TEMPO_SCALE,
      g_param_spec_float ("tempo-scale", "Tempo scale", "Tempo scale factor of playback mode",
      		0.0, 5.0, 1.0,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_PITCH_SHIFT,
      g_param_spec_int ("pitch-shift", "Pitch shift", "Pitch shift of produced sound",
      		-12, 12, 0,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));
  gstbasesrc_class->set_caps = GST_DEBUG_FUNCPTR (gst_jammo_slider_setcaps);
  gstbasesrc_class->is_seekable =
      GST_DEBUG_FUNCPTR (gst_jammo_slider_is_seekable);
  gstbasesrc_class->check_get_range =
      GST_DEBUG_FUNCPTR (gst_jammo_slider_check_get_range);
  gstbasesrc_class->do_seek = GST_DEBUG_FUNCPTR (gst_jammo_slider_do_seek);
  gstbasesrc_class->query = GST_DEBUG_FUNCPTR (gst_jammo_slider_query);
  gstbasesrc_class->get_times =
      GST_DEBUG_FUNCPTR (gst_jammo_slider_get_times);
  gstbasesrc_class->start = GST_DEBUG_FUNCPTR (gst_jammo_slider_start);
  gstbasesrc_class->stop = GST_DEBUG_FUNCPTR (gst_jammo_slider_stop);
  gstbasesrc_class->create = GST_DEBUG_FUNCPTR (gst_jammo_slider_create);
}

static void gst_jammo_slider_init (GstJammoSlider * src, GstJammoSliderClass * g_class)
{
  GstPad *pad = GST_BASE_SRC_PAD (src);

  gst_pad_set_fixatecaps_function (pad, gst_jammo_slider_src_fixate);

  src->samplerate = 44100;
  src->format = GST_JAMMO_SLIDER_FORMAT_NONE;

  src->volume = DEFAULT_VOLUME;
  src->rounding_counter=0;

	src->last_event_processed=FALSE;
	src->time=0;
	src->last_reported_frequency = -1;

  src->attack  = DEFAULT_ATTACK;
  src->decay   = DEFAULT_DECAY;
  src->sustain = DEFAULT_SUSTAIN;
  src->release = DEFAULT_RELEASE;

	src->pitch_shift=0;
	src->tempo_scale=1.0;

	// by default slider records
	src->recording=TRUE;

  /* we operate in time */
  gst_base_src_set_format (GST_BASE_SRC (src), GST_FORMAT_TIME);
  gst_base_src_set_live (GST_BASE_SRC (src), DEFAULT_IS_LIVE);

  src->samples_per_buffer = DEFAULT_SAMPLES_PER_BUFFER;
  src->generate_samples_per_buffer = src->samples_per_buffer;
  src->timestamp_offset = DEFAULT_TIMESTAMP_OFFSET;
  src->can_activate_pull = DEFAULT_CAN_ACTIVATE_PULL;

// gst_base_src_set_blocksize is since 0.10.22, maemo has only 0.10.13
// This works without this.
//  gst_base_src_set_blocksize (GST_BASE_SRC (src), -1)

	// open blow sound file for flute like slider
	char * path=NULL;
	path=g_strdup_printf("%s%s",SLIDER_FOLDER,"slider_blow.wav");
	//printf("opening file %s\n", path);
	memset(&(src->blow_file_info), 0, sizeof(SF_INFO));
	src->blow_file = sf_open(path, SFM_READ, &(src->blow_file_info));
	if (src->blow_file==NULL) {
		printf("slider-error: opening file '%s' failed\n",path);
	}
	else {
		// allocate buffer
		src->blow_table = malloc(sizeof(gint16)*src->blow_file_info.frames);
	
		// read to buffer
		if (sf_read_short(src->blow_file, src->blow_table, (src->blow_file_info).frames)
			!= src->blow_file_info.frames) {
				printf("error reading sample file\n");
		}
		sf_close(src->blow_file);
	}
	g_free(path);

	src->blow_accumulator=0.0;
	src->blow_loop_start=5000;
	src->blow_loop_end=40000;

	src->table_size=1024;
	src->sine_table = g_malloc(sizeof(gfloat)*src->table_size);
	src->fm_table = g_malloc(sizeof(gfloat)*src->table_size);
	src->organ_table = g_malloc(sizeof(gfloat)*src->table_size);
	init_sine_table(src->sine_table, src->table_size);
	init_organ_table(src->organ_table, src->table_size);
	init_fm_table(src->fm_table, src->table_size);

}

static void gst_jammo_slider_src_fixate (GstPad * pad, GstCaps * caps)
{
  GstJammoSlider *src = GST_JAMMO_SLIDER (GST_PAD_PARENT (pad));
  const gchar *name;
  GstStructure *structure;

  structure = gst_caps_get_structure (caps, 0);

  GST_DEBUG_OBJECT (src, "fixating samplerate to %d", src->samplerate);

  gst_structure_fixate_field_nearest_int (structure, "rate", src->samplerate);

  name = gst_structure_get_name (structure);
  if (strcmp (name, "audio/x-raw-int") == 0)
    gst_structure_fixate_field_nearest_int (structure, "width", 32);
  else if (strcmp (name, "audio/x-raw-float") == 0)
    gst_structure_fixate_field_nearest_int (structure, "width", 64);
}



static gboolean gst_jammo_slider_query (GstBaseSrc * basesrc, GstQuery * query)
{
  GstJammoSlider *src = GST_JAMMO_SLIDER (basesrc);
  gboolean res = FALSE;

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_CONVERT:
    {
      GstFormat src_fmt, dest_fmt;
      gint64 src_val, dest_val;

      gst_query_parse_convert (query, &src_fmt, &src_val, &dest_fmt, &dest_val);
      if (src_fmt == dest_fmt) {
        dest_val = src_val;
        goto done;
      }

      switch (src_fmt) {
        case GST_FORMAT_DEFAULT:
          switch (dest_fmt) {
            case GST_FORMAT_TIME:
              /* samples to time */
              dest_val =
                  gst_util_uint64_scale_int (src_val, GST_SECOND,
                  src->samplerate);
              break;
            default:
              goto error;
          }
          break;
        case GST_FORMAT_TIME:
          switch (dest_fmt) {
            case GST_FORMAT_DEFAULT:
              /* time to samples */
              dest_val =
                  gst_util_uint64_scale_int (src_val, src->samplerate,
                  GST_SECOND);
              break;
            default:
              goto error;
          }
          break;
        default:
          goto error;
      }
    done:
      gst_query_set_convert (query, src_fmt, src_val, dest_fmt, dest_val);
      res = TRUE;
      break;
    }
    default:
      res = GST_BASE_SRC_CLASS (parent_class)->query (basesrc, query);
      break;
  }

  return res;
  /* ERROR */
error:
  {
    GST_DEBUG_OBJECT (src, "query failed");
    return FALSE;
  }
}

static void
gst_jammo_slider_dispose (GObject * object)
{
	GstJammoSlider *src = GST_JAMMO_SLIDER (object);
	if (src->blow_table) {
		g_free(src->blow_table);
		src->blow_table=NULL;
	}
	if (src->sine_table) {
		g_free(src->sine_table);
		src->sine_table=NULL;
	}
	if (src->organ_table) {
		g_free(src->organ_table);
		src->organ_table=NULL;
	}
	if (src->fm_table) {
		g_free(src->fm_table);
		src->fm_table=NULL;
	}
	if (src->eventlist) {
		jammo_slider_event_free_glist(&(src->eventlist));
	}
  G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
gst_jammo_slider_finalize (GObject * object)
{
  G_OBJECT_CLASS (parent_class)->finalize (object);
}

/*This array is automatic generated with freq_util.c*/
/*It is based on facts (=supposition) 
a) there are 12 notes on one octave
b) there are octaves from 0...7
*/
static gfloat freq_of_note[] = { 32.703201293945312, 34.647899627685547, 36.708099365234375, 38.890899658203125, 41.203498840332031, 43.653598785400391, 46.249298095703125, 48.999500274658203, 51.912998199462891, 55.000000000000000, 58.270500183105469, 61.735401153564453,
 65.406402587890625, 69.295799255371094, 73.416198730468750, 77.781799316406250, 82.406997680664062, 87.307197570800781, 92.498596191406250, 97.999000549316406, 103.825996398925781, 110.000000000000000, 116.541000366210938, 123.470802307128906,
 130.812805175781250, 138.591598510742188, 146.832397460937500, 155.563598632812500, 164.813995361328125, 174.614395141601562, 184.997192382812500, 195.998001098632812, 207.651992797851562, 220.000000000000000, 233.082000732421875, 246.941604614257812,
 261.625610351562500, 277.183197021484375, 293.664794921875000, 311.127197265625000, 329.627990722656250, 349.228790283203125, 369.994384765625000, 391.996002197265625, 415.303985595703125, 440.000000000000000, 466.164001464843750, 493.883209228515625,
 523.251220703125000, 554.366394042968750, 587.329589843750000, 622.254394531250000, 659.255981445312500, 698.457580566406250, 739.988769531250000, 783.992004394531250, 830.607971191406250, 880.000000000000000, 932.328002929687500, 987.766418457031250,
 1046.502441406250000, 1108.732788085937500, 1174.659179687500000, 1244.508789062500000, 1318.511962890625000, 1396.915161132812500, 1479.977539062500000, 1567.984008789062500, 1661.215942382812500, 1760.000000000000000, 1864.656005859375000, 1975.532836914062500,
 2093.004882812500000, 2217.465576171875000, 2349.318359375000000, 2489.017578125000000, 2637.023925781250000, 2793.830322265625000, 2959.955078125000000, 3135.968017578125000, 3322.431884765625000, 3520.000000000000000, 3729.312011718750000, 3951.065673828125000,
 4186.009765625000000, 4434.931152343750000, 4698.636718750000000, 4978.035156250000000, 5274.047851562500000, 5587.660644531250000, 5919.910156250000000, 6271.936035156250000, 6644.863769531250000, 7040.000000000000000, 7458.624023437500000, 7902.131347656250000
};

static gfloat freq_of_quarter_tone[] = { 32.703201, 33.661480, 34.647835, 35.663094, 36.708103, 37.783733,
38.890881, 40.030468, 41.203453, 42.410805, 43.653538, 44.932682, 46.249310, 47.604519, 48.999439, 50.435230,
51.913097, 53.434265, 55.000008, 56.611633, 58.270481, 59.977936, 61.735424, 63.544411, 65.406403, 67.322960,
69.295670, 71.326187, 73.416206, 75.567467, 77.781761, 80.060936, 82.406906, 84.821609, 87.307076, 89.865364,
92.498619, 95.209038, 97.998878, 100.870461, 103.826195, 106.868530, 110.000015, 113.223267, 116.540962,
119.955872, 123.470848, 127.088821, 130.812805, 134.645920, 138.591339, 142.652374, 146.832413, 151.134933,
155.563522, 160.121872, 164.813812, 169.643219, 174.614151, 179.730728, 184.997238, 190.418076, 195.997757,
201.740921, 207.652390, 213.737061, 220.000031, 226.446533, 233.081924, 239.911743, 246.941696, 254.177643,
261.625610, 269.291840, 277.182678, 285.304749, 293.664825, 302.269867, 311.127045, 320.243744, 329.627625,
339.286438, 349.228302, 359.461456, 369.994476, 380.836151, 391.995514, 403.481842, 415.304779, 427.474121,
440.000061, 452.893066, 466.163849, 479.823486, 493.883392, 508.355286, 523.251221, 538.583679, 554.365356,
570.609497, 587.329651, 604.539734, 622.254089, 640.487488, 659.255249, 678.572876, 698.456604, 718.922913,
739.988953, 761.672302, 783.991028, 806.963684, 830.609558, 854.948242, 880.000122, 905.786133, 932.327698,
959.646973, 987.766785, 1016.710571, 1046.502441, 1077.167358, 1108.730713, 1141.218994, 1174.659302,
1209.079468, 1244.508179, 1280.974976, 1318.510498, 1357.145752, 1396.913208, 1437.845825, 1479.977905,
1523.344604, 1567.982056, 1613.927368, 1661.219116, 1709.896484, 1760.000244, 1811.572266, 1864.655396,
1919.293945, 1975.533569, 2033.421143, 2093.004883, 2154.334717, 2217.461426, 2282.437988, 2349.318604,
2418.158936, 2489.016357, 2561.949951, 2637.020996, 2714.291504, 2793.826416, 2875.691650, 2959.955811,
3046.689209, 3135.964111, 3227.854736, 3322.438232, 3419.792969, 3520.000488, 3623.144531, 3729.310791,
3838.587891, 3951.067139, 4066.842285, 4186.009766, 4308.669434, 4434.922852, 4564.875977, 4698.637207,
4836.317871, 4978.032715, 5123.899902, 5274.041992, 5428.583008, 5587.652832, 5751.383301, 5919.911621,
6093.378418, 6271.928223, 6455.709473, 6644.876465, 6839.585938, 7040.000977, 7246.289062, 7458.621582,
7677.175781, 7902.134277
};

/*Simple binary search from frequence table (sorted).
Most of the time asked value is not in table -> we check which
one is nearer (which direction to round)*/
static gfloat freq_rounding(gfloat freq){
	int min = 1; //prevents overflow
	int max = NUMBER_OF_OCTAVES*NUMBER_OF_NOTES;
	int mid = min + ((max-min)/2);

	while  ((freq != freq_of_note[mid]) && min < max) {
		if (freq > freq_of_note[mid]) {
			min = mid +1;
			}
		else {
		max = mid -1;
		}
	mid = min + ((max-min)/2);
	}

	gfloat delta  =freq_of_note[mid]-freq;    //this is delta of parameter and bigger value from table
	gfloat delta2 =freq-freq_of_note[mid-1];  //this is delta of parameter and smaller value from table
	return delta<delta2?freq_of_note[mid]:freq_of_note[mid-1]; //return value, which is nearer to parameter
}

/* similar rounding for new notes */
/* this is done instantly. rounding to nearest quarter tone */
static gfloat freq_rounding_new_note(gfloat freq){
	int min = 1; //prevents overflow
	int max = NUMBER_OF_OCTAVES*NUMBER_OF_NOTES*2-1;
	int mid = min + ((max-min)/2);

	while  ((freq != freq_of_quarter_tone[mid]) && min < max) {
		if (freq > freq_of_quarter_tone[mid]) {
			min = mid +1;
		}
		else {
			max = mid -1;
		}
	mid = min + ((max-min)/2);
	}

	gfloat delta  =freq_of_quarter_tone[mid]-freq;    //this is delta of parameter and bigger value from table
	gfloat delta2 =freq-freq_of_quarter_tone[mid-1];  //this is delta of parameter and smaller value from table
	return delta<delta2?freq_of_quarter_tone[mid]:freq_of_quarter_tone[mid-1]; //return value, which is nearer to parameter
}

// a function for initializing parameters when starting to play a new note
static void process_slider_event_on(GstJammoSlider * src, float freq) {
	src->state=TRUE;
	src->freq=freq;
	src->released=FALSE;
	src->release_counter=0;
	src->adsr_counter=0;
	src->accumulator=0.0;
	src->tremolo_phase=0.0;
	src->rounded=FALSE;
	src->rounding_counter=0;
	// sliding to a new note is not wanted
	// but new notes have to be rounded to nearest quarter tone
	// note that this rounding is done to audio only. events in stored list are not rounded in list
	// all new on events (including playback from stored list) are rounded during playback
	src->freq_slow=freq_rounding_new_note(freq);
}

static void process_slider_event_off(GstJammoSlider * src, float freq) {
	src->released=TRUE;
	src->freq=freq;
	src->rounding_counter=0;
	src->rounded=FALSE;
}

static void process_slider_event_motion(GstJammoSlider * src, float freq) {
	src->freq=freq;
	src->rounding_counter=0;
	src->rounded=FALSE;
}

static void do_playback_process(GstJammoSlider * src){
	/*GstClockTime now = gst_clock_get_time(gst_system_clock_obtain()); //time just now		
	GstClockTime current = GST_CLOCK_DIFF(src->playback_starting_time,now);*/

	JammoSliderEvent* event;
	GstClockTime timestamp;
	/*
	timestamp is in nanoseconds = ns
	e.g.:
	6.081 683 703 s
	6 081.683 703 ms
	6 081 683.703 µs
	6 081 683 703 ns
	*/

	event = g_list_nth_data(src->eventlist,src->playback_index_for_event);
	if (event==NULL)
		{
			src->state=FALSE;
			src->accumulator=0.0;
			src->tremolo_phase=0.0;
			src->eos_reached=TRUE;
			return;
		}
	timestamp = event->timestamp;

	while (src->time * src->tempo_scale > timestamp){
		//printf("DEBUG: gstjammoslider: delayed: %llu ms, freq: %f, type: %d\n",(unsigned long long) ((src->time * src->tempo_scale - timestamp)/src->tempo_scale)/1000/1000, event->freq, event->type);
		gfloat freq=event->freq * jammo_gst_do_pitch(src->pitch_shift);

		if (freq >= MINFREQ && freq <= MAXFREQ) {
			if (event->type==JAMMO_SLIDER_EVENT_ON ) {
				process_slider_event_on(src, freq);
			}

			else if (event->type==JAMMO_SLIDER_EVENT_OFF) {
				process_slider_event_off(src, freq);
			}

			else if (event->type==JAMMO_SLIDER_EVENT_MOTION) {
				if (src->state==TRUE) {
					process_slider_event_motion(src, freq);
				}
			}
			else {
				printf("a faulty event in list\n");
				printf("freq: %f, type %d\n", freq, event->type);
			}
		}
		else {
				printf("a faulty event in list\n");
				printf("freq: %f\n", freq);
		}

		// move to the next event
		src->playback_index_for_event++;

		// check if there are no more events
		if (src->playback_index_for_event==g_list_length(src->eventlist)) {
			src->last_event_processed=TRUE;
			// release just in case some event-off is missing
				src->released=TRUE;
		}

		// get next event from the list
		event = g_list_nth_data(src->eventlist,src->playback_index_for_event);
		if (event==NULL)
		{
			src->released=TRUE;
			return;
		}
		// get the time of the event
		timestamp = event->timestamp;
	}

}

#define DEFINE_PROCESS(type,scale) \
static void gst_jammo_slider_process_##type (GstJammoSlider * src, g##type * samples) {\
  gint i;\
  g##type value=0;\
  gdouble step;\
\
	if (!(src->recording)) { \
		do_playback_process(src); \
	} \
\
  /*Process whole buffer*/ \
  for (i = 0; i < src->generate_samples_per_buffer; i++) {\
\
		/* slide from current to target frequency */ \
	  if ( src->freq_slow < src->freq &&  src->freq - src->freq_slow > 1.0){\
			/* slide up */ \
			if ((src->freq - src->freq_slow)*DISTANCEFACTOR > MINSLIDE) { \
		    src->freq_slow+=(src->freq - src->freq_slow)*DISTANCEFACTOR; \
			} \
			else { \
				src->freq_slow+=MINSLIDE; \
			} \
	    src->rounding_counter=0;\
	  }\
	  else if ( src->freq_slow > src->freq && src->freq_slow - src->freq > 1.0 ){\
			/* slide down */ \
			if ((src->freq_slow - src->freq)*DISTANCEFACTOR > MINSLIDE) { \
	   	 src->freq_slow-=(src->freq_slow - src->freq)*DISTANCEFACTOR;\
			} \
			else { \
				src->freq_slow-=MINSLIDE; \
			} \
	    src->rounding_counter=0;\
		}\
	  else {\
			/* no need to slide */ \
	    if (src->state==TRUE && src->rounded==FALSE) {\
	      src->rounding_counter++;\
			} \
			/* round to nearest semitone if stayed in one frequency long enough */ \
			if (src->rounding_counter==ROUNDINGCOUNT) { \
		  	src->freq=freq_rounding(src->freq);\
				src->rounded=TRUE; \
	      src->rounding_counter++;\
			} \
		}\
\
\
		/* calculate step size */\
	  step = M_PI_M2 * src->freq_slow / src->samplerate;\
	  src->accumulator+= step;\
	  if (src->accumulator >= M_PI_M2)\
			  src->accumulator -= M_PI_M2;\
\
	  /* generate sound */ \
		if(src->state){\
			if (src->instrument==2)\
				value = (g##type)( (0.9+fabs(0.1*sine_value(src, src->tremolo_phase))) *0.8* scale * (karplus(src) ));\
			else if (src->instrument==1) {\
				/* add also blowing sound */ \
				value = (g##type)( (0.9+fabs(0.1*sine_value(src, src->tremolo_phase))) *0.8* scale * (instruments[src->instrument](src, src->accumulator) )+ BLOW_VOLUME * src->blow_table[(gint)src->blow_accumulator] / (gfloat)G_MAXINT16 );\
				src->blow_accumulator+=src->samplerate/(gfloat)src->blow_file_info.samplerate; \
			} \
			else \
				value = (g##type)( (0.9+fabs(0.1*sine_value(src, src->tremolo_phase))) *0.8* scale * (instruments[src->instrument](src, src->accumulator) ));\
		}\
	  else {\
	    value=0;\
		} \
	  src->tremolo_phase += (M_PI*10/src->samplerate); \
	  if (src->tremolo_phase>2*M_PI)\
		    src->tremolo_phase-=2*M_PI;\
\
		if (src->blow_accumulator > src->blow_loop_end) { \
			src->blow_accumulator=src->blow_loop_start; \
		} \
\
		/* if playing, process ADSR */ \
		if (src->state==TRUE) {\
	    if (!src->released) {\
	      /* Attack */\
				if (src->adsr_counter< src->attack * src->samplerate) { \
					value*=src->adsr_counter/(src->attack * src->samplerate);\
					src->release_multiplier=src->adsr_counter/(src->attack * src->samplerate);\
					src->adsr_counter++;\
					/*printf("attack\n");*/\
				}\
  \
	      /* Decay */ \
				if (src->adsr_counter >= src->attack * src->samplerate && src->adsr_counter < (src->attack*src->samplerate)+(src->decay*src->samplerate)) {\
					value*=(1-src->sustain)*(1-(src->adsr_counter/((src->attack*src->samplerate)+(src->decay*src->samplerate))))+src->sustain;\
					src->release_multiplier=(1-src->sustain)*(1-(src->adsr_counter/((src->attack*src->samplerate)+(src->decay*src->samplerate))))+src->sustain;\
					src->adsr_counter++; \
					/*printf("decay\n");*/ \
	      } \
  \
				/* Sustain */ \
				if (src->adsr_counter >= (src->attack*src->samplerate)+(src->decay*src->samplerate)) {\
					value*=src->sustain; \
					src->release_multiplier=src->sustain;\
					src->adsr_counter+=1; \
					/*printf("sustain %f\n", src->sustain);*/ \
	      }\
	    }\
\
			/* Release */ \
			if (src->released) {\
				value*=src->release_multiplier*(1-(src->release_counter/(src->release*src->samplerate)));\
				src->release_counter++;\
				if (src->release_counter >= (src->release*src->samplerate)) \
					src->state=FALSE;\
				/*printf("release\n");*/ \
			} \
		}\
	  samples[i]=value;\
	} /* end process whole buffer */ \
	\
	if (src->freq!=src->last_reported_frequency) {\
		src->last_reported_frequency=src->freq; \
		GstStructure *s;\
		s = gst_structure_new ("slider", "frequency", G_TYPE_FLOAT, src->freq * jammo_gst_do_pitch(-src->pitch_shift), NULL);\
\
		GstMessage *m = gst_message_new_element (GST_OBJECT (src), s);\
		gst_element_post_message (GST_ELEMENT (src), m);\
	}\
}

DEFINE_PROCESS (int16, 32767.0)
DEFINE_PROCESS (int32, 2147483647.0)
DEFINE_PROCESS (float, 1.0)
DEFINE_PROCESS (double, 1.0)


static JammoSliderProcessFunc process_funcs[] = {
  (JammoSliderProcessFunc) gst_jammo_slider_process_int16,
  (JammoSliderProcessFunc) gst_jammo_slider_process_int32,
  (JammoSliderProcessFunc) gst_jammo_slider_process_float,
  (JammoSliderProcessFunc) gst_jammo_slider_process_double
};

static gboolean gst_jammo_slider_setcaps (GstBaseSrc * basesrc, GstCaps * caps)
{
  GstJammoSlider *src = GST_JAMMO_SLIDER (basesrc);
  const GstStructure *structure;
  const gchar *name;
  gint width;
  gboolean ret;

  structure = gst_caps_get_structure (caps, 0);
  ret = gst_structure_get_int (structure, "rate", &src->samplerate);

  GST_DEBUG_OBJECT (src, "negotiated to samplerate %d", src->samplerate);

  name = gst_structure_get_name (structure);
  if (strcmp (name, "audio/x-raw-int") == 0) {
    ret &= gst_structure_get_int (structure, "width", &width);
    src->format = (width == 32) ? GST_JAMMO_SLIDER_FORMAT_S32 :
        GST_JAMMO_SLIDER_FORMAT_S16;
  } else {
    ret &= gst_structure_get_int (structure, "width", &width);
    src->format = (width == 32) ? GST_JAMMO_SLIDER_FORMAT_F32 :
        GST_JAMMO_SLIDER_FORMAT_F64;
  }

  /* allocate a new buffer suitable for this pad */
  switch (src->format) {
    case GST_JAMMO_SLIDER_FORMAT_S16:
      //printf("format is now gint16\n");
      src->sample_size = sizeof (gint16);
      break;
    case GST_JAMMO_SLIDER_FORMAT_S32:
      //printf("format is now gint32\n");
      src->sample_size = sizeof (gint32);
      break;
    case GST_JAMMO_SLIDER_FORMAT_F32:
      //printf("format is now gfloat32\n");
      src->sample_size = sizeof (gfloat);
      break;
    case GST_JAMMO_SLIDER_FORMAT_F64:
      //printf("format is now gfloat32\n");
      src->sample_size = sizeof (gdouble);
      break;
    default:
      /* can't really happen */
      ret = FALSE;
      break;
  }

  if (src->format == -1) {
    src->process = NULL;
    return FALSE;
  }
  src->process = process_funcs[src->format];

  return ret;
}

static void gst_jammo_slider_get_times (GstBaseSrc * basesrc, GstBuffer * buffer,
    GstClockTime * start, GstClockTime * end)
{

	GstJammoSlider * src = GST_JAMMO_SLIDER(basesrc);

	GstClockTime timestamp = GST_BUFFER_TIMESTAMP (buffer);
	src->time=timestamp;
  /* for live sources, sync on the timestamp of the buffer */
  if (gst_base_src_is_live (basesrc)) {
    

    if (GST_CLOCK_TIME_IS_VALID (timestamp)) {
      /* get duration to calculate end time */
      GstClockTime duration = GST_BUFFER_DURATION (buffer);

      if (GST_CLOCK_TIME_IS_VALID (duration)) {
        *end = timestamp + duration;
      }
      *start = timestamp;
    }
  } else {
    *start = -1;
    *end = -1;
  }
}

static gboolean gst_jammo_slider_start (GstBaseSrc * basesrc)
{
  GstJammoSlider *src = GST_JAMMO_SLIDER (basesrc);

  src->next_sample = 0;
  src->next_byte = 0;
  src->next_time = 0;
  src->check_seek_stop = FALSE;
  src->eos_reached = FALSE;
  src->tags_pushed = FALSE;
  src->tremolo_phase = 0;
	src->adsr_counter=0;
	src->accumulator=0;
  src->time = 0;

  //realtime recording
  if (src->recording) {
    //printf("DEBUG:gstjammoslider: recording mode\n");
		jammo_slider_event_free_glist(&(src->eventlist));
    src->eventlist=NULL;
		src->time_of_last_stored_event=0;
  }
  //playback
  else  {
    //printf("DEBUG:gstjammoslider: not recording, playback\n");

    src->playback_index_for_event=0;

    #ifdef only_testing
    //Test:
   GList* list;
   JammoSliderEvent* n;

   for (list = src->eventlist; list; list = list->next) {
      n = (list->data);
      printf("TEST:gstjammoslider:In stack: %llu ms: freq:%f ,type: %d \n", (unsigned long long)n->timestamp/1000/1000,n->freq,n->type);
   }
   #endif
  }

  return TRUE;
}

static gboolean gst_jammo_slider_stop (GstBaseSrc * basesrc)
{
  return TRUE;
}

/* seek to time, will be called when we operate in push mode. In pull mode we
 * get the requiested byte offset. */
static gboolean gst_jammo_slider_do_seek (GstBaseSrc * basesrc, GstSegment * segment)
{
  GstJammoSlider *src = GST_JAMMO_SLIDER (basesrc);
  GstClockTime time_to_seek;

  segment->time = segment->start;
  time_to_seek = segment->last_stop;

	// seek eventlist to correct position
	src->time = segment->time;
	src->playback_index_for_event=0;
	JammoSliderEvent * event =NULL;
	while (1) {
		event=g_list_nth_data(src->eventlist,src->playback_index_for_event);
		// stop seeking if all events seeked
		if (event==NULL) {
			break;
		}
		// stop seeking if next event is before current time
		if (src->time <= event->timestamp) {
			break;
		}
		// no need to stop seeking yet
		src->playback_index_for_event+=1;
	}
	// turn sound off
	src->released=TRUE;
	src->rounding_counter=0;
	src->rounded=FALSE;

  /* now move to the time indicated */
  src->next_sample =
      gst_util_uint64_scale_int (time_to_seek, src->samplerate, GST_SECOND);
  src->next_byte = src->next_sample * src->sample_size;
  src->next_time =
      gst_util_uint64_scale_int (src->next_sample, GST_SECOND, src->samplerate);

  g_assert (src->next_time <= time_to_seek);

  if (GST_CLOCK_TIME_IS_VALID (segment->stop)) {
    time_to_seek = segment->stop;
    src->sample_stop = gst_util_uint64_scale_int (time_to_seek, src->samplerate,
        GST_SECOND);
    src->check_seek_stop = TRUE;
  } else {
    src->check_seek_stop = FALSE;
  }
  src->eos_reached = FALSE;

  return TRUE;
}

static gboolean gst_jammo_slider_is_seekable (GstBaseSrc * basesrc)
{
  /* we're seekable... */
  return TRUE;
}

static gboolean gst_jammo_slider_check_get_range (GstBaseSrc * basesrc)
{
  GstJammoSlider *src;

  src = GST_JAMMO_SLIDER (basesrc);

  /* if we can operate in pull mode */
  return src->can_activate_pull;
}

static GstFlowReturn gst_jammo_slider_create (GstBaseSrc * basesrc, 
    guint64 offset, guint length, GstBuffer ** buffer)
{
  GstFlowReturn res;
  GstJammoSlider *src;
  GstBuffer *buf;
  GstClockTime next_time;
  gint64 next_sample, next_byte;
  guint bytes, samples;

  src = GST_JAMMO_SLIDER (basesrc);

  /* example for tagging generated data */
  if (!src->tags_pushed) {
    GstTagList *taglist;
    GstEvent *event;

    taglist = gst_tag_list_new ();

    gst_tag_list_add (taglist, GST_TAG_MERGE_APPEND,
        GST_TAG_DESCRIPTION, "jammo instrument", NULL);

    event = gst_event_new_tag (taglist);
    gst_pad_push_event (basesrc->srcpad, event);
    src->tags_pushed = TRUE;
  }

  if (src->eos_reached)
    return GST_FLOW_UNEXPECTED;

  /* if no length was given, use our default length in samples otherwise convert
   * the length in bytes to samples. */
  /*if (length == -1)
    samples = src->samples_per_buffer;
  else
    samples = length / src->sample_size;*/

	// we need to use smaller buffer than requested
	// otherwise there will be too much latency
	samples = src->samples_per_buffer;

  /* if no offset was given, use our next logical byte */
  if (offset == -1)
    offset = src->next_byte;

  /* now see if we are at the byteoffset we think we are */
  if (offset != src->next_byte) {
    GST_DEBUG_OBJECT (src, "seek to new offset %" G_GUINT64_FORMAT, offset);
    /* we have a discont in the expected sample offset, do a 'seek' */
    src->next_sample = src->next_byte / src->sample_size;
    src->next_time =
        gst_util_uint64_scale_int (src->next_sample, GST_SECOND,
        src->samplerate);
    src->next_byte = offset;
  }

  /* check for eos */
  if (src->check_seek_stop &&
      (src->sample_stop > src->next_sample) &&
      (src->sample_stop < src->next_sample + samples)
      ) {
    /* calculate only partial buffer */
    src->generate_samples_per_buffer = src->sample_stop - src->next_sample;
    next_sample = src->sample_stop;
    src->eos_reached = TRUE;
  } else {
    /* calculate full buffer */
    src->generate_samples_per_buffer = samples;
    next_sample = src->next_sample + samples;
  }

  bytes = src->generate_samples_per_buffer * src->sample_size;

  if ((res = gst_pad_alloc_buffer (basesrc->srcpad, src->next_sample,
              bytes, GST_PAD_CAPS (basesrc->srcpad), &buf)) != GST_FLOW_OK) {
    return res;
  }

  next_byte = src->next_byte + bytes;
  next_time = gst_util_uint64_scale_int (next_sample, GST_SECOND,
      src->samplerate);

  GST_LOG_OBJECT (src, "samplerate %d", src->samplerate);
  GST_LOG_OBJECT (src, "next_sample %" G_GINT64_FORMAT ", ts %" GST_TIME_FORMAT,
      next_sample, GST_TIME_ARGS (next_time));

  GST_BUFFER_TIMESTAMP (buf) = src->timestamp_offset + src->next_time;
  GST_BUFFER_OFFSET (buf) = src->next_sample;
  GST_BUFFER_OFFSET_END (buf) = next_sample;
  GST_BUFFER_DURATION (buf) = next_time - src->next_time;

  gst_object_sync_values (G_OBJECT (src), src->next_time);

  src->next_time = next_time;
  src->next_sample = next_sample;
  src->next_byte = next_byte;

  GST_LOG_OBJECT (src, "generating %u samples at ts %" GST_TIME_FORMAT,
      src->generate_samples_per_buffer,
      GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (buf)));

  src->process (src, GST_BUFFER_DATA (buf));

  if (G_UNLIKELY (src->volume == 0.0)) {
    GST_BUFFER_FLAG_SET (buf, GST_BUFFER_FLAG_GAP);
  }

  *buffer = buf;

  return GST_FLOW_OK;
}

static void gst_jammo_slider_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstJammoSlider *src = GST_JAMMO_SLIDER (object);
  GList* list; //used when looping over eventlist
  JammoSliderEvent* event_iterator;  //used when looping over eventlist

  switch (prop_id) {
    case PROP_SAMPLES_PER_BUFFER:
      src->samples_per_buffer = g_value_get_int (value);
      break;
    case PROP_INSTRUMENT:
      src->instrument = g_value_get_int (value);
      break;
    case PROP_ATTACK:
      src->attack = g_value_get_float (value);
      break;
    case PROP_DECAY:
      src->decay = g_value_get_float (value);
      break;
    case PROP_RELEASE:
      src->release = g_value_get_float (value);
      break;
    case PROP_SUSTAIN:
      src->sustain = g_value_get_float (value);
      break;
    case PROP_VOLUME:
      src->volume = g_value_get_double (value);
      break;
    case PROP_IS_LIVE:
      gst_base_src_set_live (GST_BASE_SRC (src), g_value_get_boolean (value));
      break;
    case PROP_TIMESTAMP_OFFSET:
      src->timestamp_offset = g_value_get_int64 (value);
      break;
    case PROP_CAN_ACTIVATE_PUSH:
      GST_BASE_SRC (src)->can_activate_push = g_value_get_boolean (value);
      break;
    case PROP_CAN_ACTIVATE_PULL:
      src->can_activate_pull = g_value_get_boolean (value);
      break;

		case PROP_REMOVE_EVENT:
			;
			// note that remove removes only one event
			// to remove a note one has to remove on, off, and possible motion events
			JammoSliderEvent* removed_event = (JammoSliderEvent*)(g_value_get_pointer(value));

			//printf("Removing this: %llu ms: note:%d \n",(unsigned long long)time,note_);
			for (list = src->eventlist; list; list = list->next) {
				//printf(".\n");
				event_iterator = (list->data);
				//printf("Scanning stack: %llu ms: freq:%f ,type: %d \n", (unsigned long long)n->timestamp,n->freq, n->type);

				// find and remove
				if (event_iterator->type==removed_event->type && event_iterator->freq==removed_event->freq && event_iterator->timestamp==removed_event->timestamp) {
					printf("event removed\n");
					src->eventlist = g_list_remove(src->eventlist,event_iterator);
					g_free(event_iterator);
				}

			}

		//printf("removing event done\n");
			break;

    case PROP_ADD_EVENT:
			;
			JammoSliderEvent* added_event = (JammoSliderEvent*)(g_value_get_pointer(value));
			guint64 timestamp = added_event->timestamp;
			gboolean store=TRUE;

			//realtime. There are no timestamp in event, use current time
			if (src->recording){
				//printf("realtime\n");
				timestamp = src->time * src->tempo_scale;

				if (added_event->type==JAMMO_SLIDER_EVENT_ON) {
					process_slider_event_on(src, added_event->freq * jammo_gst_do_pitch(src->pitch_shift));
				}
				else if (added_event->type==JAMMO_SLIDER_EVENT_OFF) {
					process_slider_event_off(src, added_event->freq * jammo_gst_do_pitch(src->pitch_shift));
				}
				else if (added_event->type==JAMMO_SLIDER_EVENT_MOTION) {
					if (src->state==TRUE) {
						process_slider_event_motion(src, added_event->freq * jammo_gst_do_pitch(src->pitch_shift));
						// check if event can be discarded
						if (src->previous_event.type==JAMMO_SLIDER_EVENT_MOTION &&
							timestamp - src->time_of_last_stored_event < DISCARDTIME) {
							// find previous event from the list
							JammoSliderEvent * temp_event;
							for (list=src->eventlist;list;list=list->next) {
								temp_event=list->data;
								if (temp_event->type == src->previous_event.type &&
									temp_event->freq == src->previous_event.freq &&
									temp_event->timestamp == src->previous_event.timestamp) {
										break;
								}
							}
							if (list!=NULL) {
								store=FALSE;
							}
						}
					}
					else {
						store=FALSE;
					}
				}
			}

			else { //adding non-realtime (nothing special)
				//printf("non-realtime\n");
				timestamp=added_event->timestamp;
			}

			if (store) {
				jammo_slider_event_store_event_to_glist(&(src->eventlist) ,added_event->freq, added_event->type, timestamp);
				//printf("DEBUG: gstjammoslider: added event %llu ms: freq:'%f', type=%d\n",(unsigned long long)(timestamp/1000/1000),event->freq,event->type);
				src->previous_event.type=added_event->type;
				src->previous_event.freq=added_event->freq;
				src->previous_event.timestamp=timestamp;
				src->time_of_last_stored_event=timestamp;
			}

			break;
    case PROP_CLEAR_EVENTS:
      if (g_value_get_boolean (value)) {
	      jammo_slider_event_free_glist(&(src->eventlist));
	      src->eventlist=NULL;
      }
      break;
    case PROP_RECORDING:
      src->recording = g_value_get_boolean (value);
      // when playbacking slider needs to be not is-live so that seeking works better
      // when recording slider needs to be is-live for smoother response
      gst_base_src_set_live (GST_BASE_SRC (src), g_value_get_boolean (value));
      break;
   case PROP_EVENTLIST:
      //We got GList containing JammoSliderEvents, take all one-by-one and add to own internal eventlist
			jammo_slider_event_free_glist(&(src->eventlist));
			src->eventlist=NULL;
      for (list = g_value_get_pointer (value); list; list = list->next) {
        event_iterator = (list->data);
        //printf("CASE PROP_EVENTLIST:gstjammosampler:Add: %llu ns: note:%d ,type: %s \n", (unsigned long long)n->timestamp,n->note, n->type==0? "start":"stop");
				// we want to allocate memory for the event so that nothing else will delete events
				// from samplers list
				jammo_slider_event_store_event_to_glist(&(src->eventlist), event_iterator->freq, event_iterator->type, event_iterator->timestamp);
        }
			break;
    case PROP_TEMPO_SCALE:
      src->tempo_scale = g_value_get_float (value);
      break;
    case PROP_PITCH_SHIFT:
      src->pitch_shift = g_value_get_int (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void gst_jammo_slider_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstJammoSlider *src = GST_JAMMO_SLIDER (object);

  switch (prop_id) {
    case PROP_SAMPLES_PER_BUFFER:
      g_value_set_int (value, src->samples_per_buffer);
      break;
    case PROP_INSTRUMENT:
      g_value_set_enum (value, src->instrument);
      break;
    case PROP_ATTACK:
      g_value_set_float (value, src->attack);
      break;
    case PROP_DECAY:
      g_value_set_float (value, src->decay);
      break;
    case PROP_RELEASE:
      g_value_set_float (value, src->release);
      break;
    case PROP_SUSTAIN:
       g_value_set_float (value, src->sustain);
      break;
    case PROP_VOLUME:
      g_value_set_double (value, src->volume);
      break;
    case PROP_IS_LIVE:
      g_value_set_boolean (value, gst_base_src_is_live (GST_BASE_SRC (src)));
      break;
    case PROP_TIMESTAMP_OFFSET:
      g_value_set_int64 (value, src->timestamp_offset);
      break;
    case PROP_CAN_ACTIVATE_PUSH:
      g_value_set_boolean (value, GST_BASE_SRC (src)->can_activate_push);
      break;
    case PROP_CAN_ACTIVATE_PULL:
      g_value_set_boolean (value, src->can_activate_pull);
      break;
    case PROP_RECORDING:
      g_value_set_boolean (value, src->recording);
      break;
		case PROP_EVENTLIST:
			g_value_set_pointer (value, src->eventlist);
			break;
		case PROP_TEMPO_SCALE:
			g_value_set_float (value, src->tempo_scale);
			break;
		case PROP_PITCH_SHIFT:
			g_value_set_int (value, src->pitch_shift);
			break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;

  }
}
