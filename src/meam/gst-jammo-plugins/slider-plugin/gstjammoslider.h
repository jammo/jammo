/*This file is part of JamMo.
License:LGPL 2.1
 */

#ifndef __GST_JAMMO_SLIDER_H__
#define __GST_JAMMO_SLIDER_H__

#include <gst/gst.h>
#include <gst/base/gstbasesrc.h>
#include <sndfile.h>
#include "../../jammo-slider-event.h"


G_BEGIN_DECLS

#define GST_TYPE_JAMMO_SLIDER \
  (gst_jammo_slider_get_type())
#define GST_JAMMO_SLIDER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_JAMMO_SLIDER,GstJammoSlider))
#define GST_JAMMO_SLIDER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_JAMMO_SLIDER,GstJammoSliderClass))
#define GST_IS_JAMMO_SLIDER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_JAMMO_SLIDER))
#define GST_IS_JAMMO_SLIDER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_JAMMO_SLIDER))

typedef enum {
  GST_JAMMO_SLIDER_FORMAT_NONE = -1,
  GST_JAMMO_SLIDER_FORMAT_S16 = 0,
  GST_JAMMO_SLIDER_FORMAT_S32,
  GST_JAMMO_SLIDER_FORMAT_F32,
  GST_JAMMO_SLIDER_FORMAT_F64
} GstJammoSliderFormat;


typedef struct _GstJammoSlider GstJammoSlider;
typedef struct _GstJammoSliderClass GstJammoSliderClass;

typedef void (*JammoSliderProcessFunc) (GstJammoSlider*, guint8 *);
typedef gfloat (*JammoSliderInstrumentFunc) (GstJammoSlider*, gdouble);

/*If you change these:
-generate freq-table again
-check names of notes (it is array with size 12) */
#define NUMBER_OF_OCTAVES 8
#define NUMBER_OF_NOTES 12

// maximum and minimum frequencies slider tries to produce
#define MINFREQ 10.0
#define MAXFREQ 2000.0

/**
 * GstJammoSlider:
 * object structure.
 */
struct _GstJammoSlider {
  GstBaseSrc parent;

  JammoSliderProcessFunc process;

  /* parameters */
  gint instrument; 	/*Type of instrument*/
  gdouble volume;
  gboolean note_table[NUMBER_OF_OCTAVES*NUMBER_OF_NOTES];


  /* audio parameters */
  gint samplerate;
  gint samples_per_buffer;
  gint sample_size;
  GstJammoSliderFormat format;
  
  /*< private >*/
  gboolean tags_pushed;			/* send tags just once ? */
  GstClockTimeDiff timestamp_offset;    /* base offset */
  GstClockTime next_time;               /* next timestamp */
  gint64 next_sample;                   /* next sample to send */
  gint64 next_byte;                     /* next byte to send */
  gint64 sample_stop;
  gboolean check_seek_stop;
  gboolean eos_reached;
  gint generate_samples_per_buffer;	/* used to generate a partial buffer */
  gboolean can_activate_pull;
  gfloat last_reported_frequency;

	GstClockTime time; /* used for playback and timestamps */
	gboolean recording;
	GList * eventlist;
	gint playback_index_for_event;
	gboolean last_event_processed;
	JammoSliderEvent previous_event;
	GstClockTime time_of_last_stored_event;
  
  /* waveform specific context data */
  gfloat tremolo_phase;		/*Vibration of tones*/
  gfloat freq;
  gfloat freq_slow;
  gfloat accumulator;
  gboolean state;
  gint rounding_counter;
  gboolean released;
	gboolean rounded;
  gint release_counter;
  gfloat release_multiplier;
	gfloat * sine_table;
	gfloat * organ_table;
	gfloat * fm_table;
	gint table_size;

  /* Slider ADSR */
  gfloat attack;
  gfloat decay;
  gfloat sustain;
  gfloat release;
  gint adsr_counter;
  
  /* blow sound for flute like instrument */
	SNDFILE * blow_file;
	SF_INFO blow_file_info;
  gfloat blow_accumulator;
  gint blow_loop_start;
  gint blow_loop_end;
  gint16 * blow_table;
  
	// tempo & pitch
	gfloat tempo_scale;
	gint pitch_shift;
			
};

struct _GstJammoSliderClass {
  GstBaseSrcClass parent_class;
};

GType gst_jammo_slider_get_type (void);

G_END_DECLS

#endif /* __GST_JAMMO_SLIDER_H__ */
