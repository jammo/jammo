/*This file is part of JamMo.
License:LGPL 2.1
 */

#ifndef __GST_JAMMO_SAMPLER_H__
#define __GST_JAMMO_SAMPLER_H__

#include <stdio.h>
#include <gst/gst.h>
#include <gst/base/gstbasesrc.h>
#include "../../jammo-midi.h"

G_BEGIN_DECLS

#define GST_TYPE_JAMMO_SAMPLER \
  (gst_jammo_sampler_get_type())
#define GST_JAMMO_SAMPLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_JAMMO_SAMPLER,GstJammoSampler))
#define GST_JAMMO_SAMPLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_JAMMO_SAMPLER,GstJammoSamplerClass))
#define GST_IS_JAMMO_SAMPLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_JAMMO_SAMPLER))
#define GST_IS_JAMMO_SAMPLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_JAMMO_SAMPLER))

typedef enum {
  GST_JAMMO_SAMPLER_FORMAT_NONE = -1,
  GST_JAMMO_SAMPLER_FORMAT_S16 = 0,
  GST_JAMMO_SAMPLER_FORMAT_S32,
  GST_JAMMO_SAMPLER_FORMAT_F32,
  GST_JAMMO_SAMPLER_FORMAT_F64
} GstJammoSamplerFormat;

typedef enum {
  GST_JAMMO_SAMPLER_SAMPLER_LOOPING,
	GST_JAMMO_SAMPLER_SAMPLER_NONLOOPING,
	GST_JAMMO_SAMPLER_SAMPLER_ONESHOT
} GstJammoSamplerSampleCategory;

typedef struct _GstJammoSampler GstJammoSampler;
typedef struct _GstJammoSamplerClass GstJammoSamplerClass;

typedef void (*JammoSamplerProcessFunc) (GstJammoSampler*, guint8 *);
typedef float (*JammoSampleInstrumentFunc) (gdouble);

/*If you change these:
-generate freq-table again
-check names of notes (it is array with size 12) */
#define MAXNOTES 20

typedef struct _SlicedWav SlicedWav;

struct _SlicedWav{
FILE * filepointer;
gint original_samplerate;
gint16 *sampletable;
gfloat sampleindex;
/* Memory for filename is allocated dynamically */
gchar *filename;

// sample_end can be used in releasing if we want to jump to later position 
// in sample table.
// gint sample_end;

//xfade is used in looping
//length
gint xfade;
//point in sample table
gfloat xfadeindex;
//position of xfade
gfloat xfadecounter;

//releasing is currently done with simple fade out
gboolean released;
//position of release
gfloat releasecounter;
//length
gint fade_out;

gint loop_end;  
gint loop_start; 
gint file_size;  	//this is same than size of sampletable
gint needed_pitch;	//if base was is D#4, put -1 if you want D4. put +1 if you want E4.
};

//Notes are ints too.
/*typedef struct _JammoMidiEvent {
	int note;
	int octave;
	GstClockTime timestamp;
	int type; //0=start, 1=stop
} JammoMidiEvent;
*/

/**
 * GstJammoSampler:
 * object structure.
 */
struct _GstJammoSampler {
  GstBaseSrc parent;

  //GstClockTime playback_starting_time; //Starting point for playback
  int playback_index_for_event;  //This is index for next event in eventlist
  gboolean playback_not_started; 
  GstClockTime creation_time;

	GstClockTime time;

  JammoSamplerProcessFunc process;

  GList* eventlist;
  gboolean recording;
  /* parameters */
  gdouble volume;
  gboolean note_table[JAMMOMIDI_NUMBER_OF_NOTES];

  /* Looping, non-looping or one-shot */
  GstJammoSamplerSampleCategory category;

  /* audio parameters */
  gint samplerate;
  gint samples_per_buffer;
  gint sample_size;
  GstJammoSamplerFormat format;
  
  /*< private >*/
  gboolean tags_pushed;			/* send tags just once ? */
  GstClockTimeDiff timestamp_offset;    /* base offset */
  GstClockTime next_time;               /* next timestamp */
  gint64 next_sample;                   /* next sample to send */
  gint64 next_byte;                     /* next byte to send */
  gint64 sample_stop;
  gboolean check_seek_stop;
  gboolean eos_reached;
	gboolean last_event_processed;
  gint generate_samples_per_buffer;	/* used to generate a partial buffer */
  gboolean can_activate_pull;

	// tempo & pitch
	gfloat tempo_scale;
	gint pitch_shift;
  
  /* waveform specific context data */
  gdouble accumulators[JAMMOMIDI_NUMBER_OF_NOTES];			/* phase angle */
  gint notes_playing;  		/*How many notes are playing in this moment*/

  gchar *base_folder; /*This folder of virtual instrument samples*/

  SlicedWav *wav_based_instrument[JAMMOMIDI_NUMBER_OF_NOTES];	
};

struct _GstJammoSamplerClass {
  GstBaseSrcClass parent_class;
};

GType gst_jammo_sampler_get_type (void);

G_END_DECLS

#endif /* __GST_JAMMO_SAMPLER_H__ */
