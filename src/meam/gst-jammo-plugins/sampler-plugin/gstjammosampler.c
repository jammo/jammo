/* This file is part of JamMo.
License:LGPL 2.1

 */
/**
 * SECTION:element-jammosampler
 *
 * JammoSampler can be used to generate virtual instruments. 
 *
 */

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <gst/controller/gstcontroller.h>
#include <sys/time.h>
#include <sndfile.h>

#include "../plugin-common.h"
#include "gstjammosampler.h"

/*e.g. Nokia Internet Tablet n810 doesn't have this (maemo diablo)*/
#ifndef G_PARAM_STATIC_STRINGS
#define	G_PARAM_STATIC_STRINGS (G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB)
#endif

#ifndef M_PI
#define M_PI  3.14159265358979323846
#endif

#ifndef M_PI_2
#define M_PI_2  1.57079632679489661923
#endif

#define M_PI_M2 ( M_PI + M_PI )

static const GstElementDetails gst_jammo_sampler_details =
GST_ELEMENT_DETAILS ( (gchar*)"JamMo Virtual Instrument",
     (gchar*)"Source/Audio",
     (gchar*)"Creates virtual instrument",
     (gchar*)"Aapo Rantalainen <aapo.rantalainen@gmail.com>, Mikko Gynther <mikko.gynther@lut.fi>");

#define DEFAULT_SAMPLES_PER_BUFFER   512
#define DEFAULT_VOLUME               0.6
#define DEFAULT_IS_LIVE              FALSE
#define DEFAULT_TIMESTAMP_OFFSET     G_GINT64_CONSTANT (0)
#define DEFAULT_CAN_ACTIVATE_PUSH    TRUE
#define DEFAULT_CAN_ACTIVATE_PULL    FALSE

enum
{
  PROP_0,
  PROP_SAMPLES_PER_BUFFER,
  PROP_CATEGORY,
  PROP_INITIALIZE_INSTRUMENT,
  PROP_INSTRUMENT_FOLDER,
  PROP_ADD_EVENT,
  PROP_REMOVE_EVENT,
  PROP_CLEAR_EVENTS,
  PROP_VOLUME,
  PROP_IS_LIVE,
  PROP_TIMESTAMP_OFFSET,
  PROP_CAN_ACTIVATE_PUSH,
  PROP_CAN_ACTIVATE_PULL,
  PROP_RECORDING,
  PROP_EVENTLIST,
  PROP_TEMPO_SCALE,
  PROP_PITCH_SHIFT,
  PROP_LAST
};

static GstStaticPadTemplate gst_jammo_sampler_src_template =
    GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-raw-int, "
        "endianness = (int) BYTE_ORDER, "
        "signed = (boolean) true, "
        "width = (int) 16, "
        "depth = (int) 16, "
        "rate = (int) [ 1, MAX ], "
        "channels = (int) 1; "
        "audio/x-raw-int, "
        "endianness = (int) BYTE_ORDER, "
        "signed = (boolean) true, "
        "width = (int) 32, "
        "depth = (int) 32,"
        "rate = (int) [ 1, MAX ], "
        "channels = (int) 1; "
        "audio/x-raw-float, "
        "endianness = (int) BYTE_ORDER, "
        "width = (int) { 32, 64 }, "
        "rate = (int) [ 1, MAX ], " "channels = (int) 1")
    );

GST_BOILERPLATE (GstJammoSampler, gst_jammo_sampler, GstBaseSrc,GST_TYPE_BASE_SRC)

static void gst_jammo_sampler_dispose (GObject * object);
static void gst_jammo_sampler_finalize (GObject * object);

static void gst_jammo_sampler_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec);
static void gst_jammo_sampler_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);

static gboolean gst_jammo_sampler_setcaps (GstBaseSrc * basesrc,
    GstCaps * caps);
static void gst_jammo_sampler_src_fixate (GstPad * pad, GstCaps * caps);

static gboolean gst_jammo_sampler_is_seekable (GstBaseSrc * basesrc);
static gboolean gst_jammo_sampler_check_get_range (GstBaseSrc * basesrc);
static gboolean gst_jammo_sampler_do_seek (GstBaseSrc * basesrc,
    GstSegment * segment);
static gboolean gst_jammo_sampler_query (GstBaseSrc * basesrc,
    GstQuery * query);

static void gst_jammo_sampler_get_times (GstBaseSrc * basesrc,
    GstBuffer * buffer, GstClockTime * start, GstClockTime * end);
static gboolean gst_jammo_sampler_start (GstBaseSrc * basesrc);
static gboolean gst_jammo_sampler_stop (GstBaseSrc * basesrc);
static GstFlowReturn gst_jammo_sampler_create (GstBaseSrc * basesrc,
    guint64 offset, guint length, GstBuffer ** buffer);


static void
gst_jammo_sampler_base_init (gpointer g_class)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&gst_jammo_sampler_src_template));
  gst_element_class_set_details (element_class, &gst_jammo_sampler_details);
}

static gchar notenames[] = { 'c', '1', 'd', '2', 'e', 'f',
  '3', 'g', '4', 'a', '5', 'b'
};

/*This is used when user pass char to instrument.
It is best that user can use chars when calling instrument*/
static gint get_note(gchar ch){
  int i;
  for (i=0;i<12;i++) {
    if (ch==notenames[i]) {
      return i;
		}
  }
return -1; //error case
}


static void gst_jammo_sampler_class_init (GstJammoSamplerClass * klass)
{
  GObjectClass *gobject_class;
  GstBaseSrcClass *gstbasesrc_class;

  gobject_class = (GObjectClass *) klass;
  gstbasesrc_class = (GstBaseSrcClass *) klass;

	gobject_class->dispose = gst_jammo_sampler_dispose;
	gobject_class->finalize = gst_jammo_sampler_finalize;

  gobject_class->set_property = gst_jammo_sampler_set_property;
  gobject_class->get_property = gst_jammo_sampler_get_property;

  g_object_class_install_property (gobject_class, PROP_SAMPLES_PER_BUFFER,
      g_param_spec_int ("samplesperbuffer", "Samples per buffer",
          "Number of samples in each outgoing buffer",
          1, G_MAXINT, DEFAULT_SAMPLES_PER_BUFFER,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_CATEGORY,
      g_param_spec_int ("category", "Category of current instrument",
          "Looping/non-lopping/one-shot",
          0, 3, GST_JAMMO_SAMPLER_SAMPLER_LOOPING,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_INITIALIZE_INSTRUMENT,
      g_param_spec_string ("init-instrument", "init Virtual Instrument",
          "Data of instrument", NULL,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_INSTRUMENT_FOLDER,
      g_param_spec_string ("instrument-folder", "Virtual Instruments basefolder",
          "instruments basefolder", NULL,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property(gobject_class, PROP_REMOVE_EVENT,
                                 g_param_spec_pointer ("remove-event",
                                 "Removes both start and stop events",
                                 "",
                                 G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
  g_object_class_install_property(gobject_class, PROP_ADD_EVENT,
                                 g_param_spec_pointer ("add-event",
                                 "Add event",
                                 "",
                                 G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
  g_object_class_install_property (gobject_class, PROP_CLEAR_EVENTS,
      g_param_spec_boolean ("clear-events", "Clear events",
          "Clear event list", FALSE,
          G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_VOLUME,
      g_param_spec_double ("volume", "Volume", "Volume of test signal", 0.0,
          1.0, DEFAULT_VOLUME,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_IS_LIVE,
      g_param_spec_boolean ("is-live", "Is Live",
          "Whether to act as a live source", DEFAULT_IS_LIVE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_TIMESTAMP_OFFSET, g_param_spec_int64 ("timestamp-offset",
          "Timestamp offset",
          "An offset added to timestamps set on buffers (in ns)", G_MININT64,
          G_MAXINT64, DEFAULT_TIMESTAMP_OFFSET,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_CAN_ACTIVATE_PUSH,
      g_param_spec_boolean ("can-activate-push", "Can activate push",
          "Can activate in push mode", DEFAULT_CAN_ACTIVATE_PUSH,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_CAN_ACTIVATE_PULL,
      g_param_spec_boolean ("can-activate-pull", "Can activate pull",
          "Can activate in pull mode", DEFAULT_CAN_ACTIVATE_PULL,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_RECORDING,
      g_param_spec_boolean ("realtime", "Realtime",
          "Realtime", TRUE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_EVENTLIST,
      g_param_spec_pointer ("eventlist", "Get a list of events",
          "Get a list of events", G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_TEMPO_SCALE,
      g_param_spec_float ("tempo-scale", "Tempo scale", "Tempo scale factor of playback mode",
      		0.0, 5.0, 1.0,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_PITCH_SHIFT,
      g_param_spec_int ("pitch-shift", "Pitch shift", "Pitch shift of produced sound",
      		-12, 12, 0,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));
  gstbasesrc_class->set_caps = GST_DEBUG_FUNCPTR (gst_jammo_sampler_setcaps);
  gstbasesrc_class->is_seekable =
      GST_DEBUG_FUNCPTR (gst_jammo_sampler_is_seekable);
  gstbasesrc_class->check_get_range =
      GST_DEBUG_FUNCPTR (gst_jammo_sampler_check_get_range);
  gstbasesrc_class->do_seek = GST_DEBUG_FUNCPTR (gst_jammo_sampler_do_seek);
  gstbasesrc_class->query = GST_DEBUG_FUNCPTR (gst_jammo_sampler_query);
  gstbasesrc_class->get_times =
      GST_DEBUG_FUNCPTR (gst_jammo_sampler_get_times);
  gstbasesrc_class->start = GST_DEBUG_FUNCPTR (gst_jammo_sampler_start);
  gstbasesrc_class->stop = GST_DEBUG_FUNCPTR (gst_jammo_sampler_stop);
  gstbasesrc_class->create = GST_DEBUG_FUNCPTR (gst_jammo_sampler_create);
}



static void init_sampler (GstJammoSampler * src, gint index_parameter, const gchar *filename, gint loop_start, gint loop_end, gint file_size, gint needed_pitch, gint xfade, gint fade_out){
gint i;

src->wav_based_instrument[index_parameter]->sampleindex=0;
src->wav_based_instrument[index_parameter]->loop_start=loop_start;
src->wav_based_instrument[index_parameter]->loop_end=loop_end;
src->wav_based_instrument[index_parameter]->file_size=file_size;
src->wav_based_instrument[index_parameter]->needed_pitch=needed_pitch;
if (xfade>=0)
	src->wav_based_instrument[index_parameter]->xfade=xfade;
else
	src->wav_based_instrument[index_parameter]->xfade=0;
src->wav_based_instrument[index_parameter]->xfadecounter=-1.0;
src->wav_based_instrument[index_parameter]->released=FALSE;
src->wav_based_instrument[index_parameter]->releasecounter=-1.0;
src->wav_based_instrument[index_parameter]->fade_out=fade_out;

SF_INFO sfinfo;
sfinfo.format = 0;
SNDFILE * sndfileptr = sf_open(filename, SFM_READ, &sfinfo);
if (sndfileptr==NULL) {
	printf("\nFATAL-ERROR: gstjammosampler can't open file '%s'. Better to quit (...but keep running)\n", filename);
	//exit(1);
}

/*	Same samples are not read to memory multiple times
		filenames of used waves are stored and names are compared
		if names match change pointer value to that
		else allocate memory and read data */

src->wav_based_instrument[index_parameter]->sampletable =NULL;

// samplerate of sample file can be played with correct pitch even if samplerate of a file is
// different from the samplerate of sampler
src->wav_based_instrument[index_parameter]->original_samplerate=sfinfo.samplerate;

// sampler is mono sampler
if (sfinfo.channels>1) {
	g_warning("Loading a multi-channel file to mono sampler.");
}

//printf("\nbefore looking for same sample\n");

for (i=0;i<JAMMOMIDI_NUMBER_OF_NOTES;i++) {
	if (src->wav_based_instrument[i]->filename!=NULL && strcmp(filename, src->wav_based_instrument[i]->filename)==0 && i!=index_parameter) {
		src->wav_based_instrument[index_parameter]->sampletable=src->wav_based_instrument[i]->sampletable;
		//printf("sampletable pointer ok\n");
		if (src->wav_based_instrument[index_parameter]->filename==NULL)
			src->wav_based_instrument[index_parameter]->filename =malloc((strlen(filename)+1)*sizeof(gchar));
		else
			src->wav_based_instrument[index_parameter]->filename =realloc(src->wav_based_instrument[index_parameter]->filename, (strlen(filename)+1)*sizeof(gchar) );
		strcpy(src->wav_based_instrument[index_parameter]->filename, src->wav_based_instrument[i]->filename);
	//printf("same sample table used\n");
	break;
	}
}

/* same sample was not already used */
if (src->wav_based_instrument[index_parameter]->sampletable ==NULL) {
	//printf("allocating memory\n");
	src->wav_based_instrument[index_parameter]->sampletable =malloc(file_size*sizeof(gint16));
	if (src->wav_based_instrument[index_parameter]->filename==NULL)
		src->wav_based_instrument[index_parameter]->filename =malloc((strlen(filename)+1)*sizeof(gchar));
	else
		src->wav_based_instrument[index_parameter]->filename =realloc(src->wav_based_instrument[index_parameter]->filename, (strlen(filename)+1)*sizeof(gchar) );
	strcpy(src->wav_based_instrument[index_parameter]->filename, filename);
	/* read audio data to sample table */
	if (sndfileptr){
		if (sf_read_short(sndfileptr, src->wav_based_instrument[index_parameter]->sampletable, file_size)!=file_size){
			printf("error reading sample file\n");
			}
		}
	//TODO: load something silence to sampletable
	//else{ }
}

if (sndfileptr)
	sf_close(sndfileptr);
}



static void gst_jammo_sampler_init (GstJammoSampler * src, GstJammoSamplerClass * g_class)
{
  gint i;
  GstPad *pad = GST_BASE_SRC_PAD (src);

  gst_pad_set_fixatecaps_function (pad, gst_jammo_sampler_src_fixate);

  src->samplerate = 44100;
  src->format = GST_JAMMO_SAMPLER_FORMAT_NONE;

  src->volume = DEFAULT_VOLUME;
  src->base_folder = g_strdup_printf("default");

  //All notes start silently
  for (i=0;i<JAMMOMIDI_NUMBER_OF_NOTES;i++)
    src->note_table[i]=FALSE;

  src->notes_playing=0;
	src->last_event_processed=FALSE;

	src->time=0;
	src->pitch_shift=0;
	src->tempo_scale=1.0;

  /* we operate in time */
  gst_base_src_set_format (GST_BASE_SRC (src), GST_FORMAT_TIME);
  gst_base_src_set_live (GST_BASE_SRC (src), DEFAULT_IS_LIVE);

  src->samples_per_buffer = DEFAULT_SAMPLES_PER_BUFFER;
  src->generate_samples_per_buffer = src->samples_per_buffer;
  src->timestamp_offset = DEFAULT_TIMESTAMP_OFFSET;
  src->can_activate_pull = DEFAULT_CAN_ACTIVATE_PULL;

// gst_base_src_set_blocksize is since 0.10.22, maemo has only 0.10.13
// This works without this.
//  gst_base_src_set_blocksize (GST_BASE_SRC (src), -1);


  for (i=0;i<JAMMOMIDI_NUMBER_OF_NOTES;i++){
    src->wav_based_instrument[i] =malloc(sizeof(SlicedWav));
		if (src->wav_based_instrument[i]==NULL) {
		printf("error allocating memory. exiting\n");
		exit(1);
		}
	 src->wav_based_instrument[i]->filename=NULL;
  }

}

static void gst_jammo_sampler_src_fixate (GstPad * pad, GstCaps * caps)
{
  GstJammoSampler *src = GST_JAMMO_SAMPLER (GST_PAD_PARENT (pad));
  const gchar *name;
  GstStructure *structure;

  structure = gst_caps_get_structure (caps, 0);

  GST_DEBUG_OBJECT (src, "fixating samplerate to %d", src->samplerate);

  gst_structure_fixate_field_nearest_int (structure, "rate", src->samplerate);

  name = gst_structure_get_name (structure);
  if (strcmp (name, "audio/x-raw-int") == 0)
    gst_structure_fixate_field_nearest_int (structure, "width", 32);
  else if (strcmp (name, "audio/x-raw-float") == 0)
    gst_structure_fixate_field_nearest_int (structure, "width", 64);
}





static gboolean gst_jammo_sampler_query (GstBaseSrc * basesrc, GstQuery * query)
{
  GstJammoSampler *src = GST_JAMMO_SAMPLER (basesrc);
  gboolean res = FALSE;

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_CONVERT:
    {
      GstFormat src_fmt, dest_fmt;
      gint64 src_val, dest_val;

      gst_query_parse_convert (query, &src_fmt, &src_val, &dest_fmt, &dest_val);
      if (src_fmt == dest_fmt) {
        dest_val = src_val;
        goto done;
      }

      switch (src_fmt) {
        case GST_FORMAT_DEFAULT:
          switch (dest_fmt) {
            case GST_FORMAT_TIME:
              /* samples to time */
              dest_val =
                  gst_util_uint64_scale_int (src_val, GST_SECOND,
                  src->samplerate);
              break;
            default:
              goto error;
          }
          break;
        case GST_FORMAT_TIME:
          switch (dest_fmt) {
            case GST_FORMAT_DEFAULT:
              /* time to samples */
              dest_val =
                  gst_util_uint64_scale_int (src_val, src->samplerate,
                  GST_SECOND);
              break;
            default:
              goto error;
          }
          break;
        default:
          goto error;
      }
    done:
      gst_query_set_convert (query, src_fmt, src_val, dest_fmt, dest_val);
      res = TRUE;
      break;
    }
    default:
      res = GST_BASE_SRC_CLASS (parent_class)->query (basesrc, query);
      break;
  }

  return res;
  /* ERROR */
error:
  {
    GST_DEBUG_OBJECT (src, "query failed");
    return FALSE;
  }
}

static void
gst_jammo_sampler_dispose (GObject * object)
{
  GstJammoSampler *src = GST_JAMMO_SAMPLER (object);

	// free audio data and filenames
	int i, j;
	for (i=0;i<JAMMOMIDI_NUMBER_OF_NOTES;i++) {
		if (src->wav_based_instrument[i]->filename!=NULL) {
			g_free(src->wav_based_instrument[i]->filename);
			src->wav_based_instrument[i]->filename=NULL;
			// if filename was not NULL there may be audio data as well
			if (src->wav_based_instrument[i]->sampletable!=NULL) {
				// the same audio data is used for multiple notes
				// set other pointers as NULL and free one
				for (j=0;j<JAMMOMIDI_NUMBER_OF_NOTES;j++) {
					if (i!=j && src->wav_based_instrument[j]!=NULL &&
						src->wav_based_instrument[j]->filename!=NULL &&
						src->wav_based_instrument[j]->sampletable!=NULL &&
						src->wav_based_instrument[i]->sampletable==src->wav_based_instrument[j]->sampletable) {
							src->wav_based_instrument[j]->sampletable=NULL;
						}
				}
				g_free(src->wav_based_instrument[i]->sampletable);
				src->wav_based_instrument[i]->sampletable=NULL;
			}
		}
		if (src->wav_based_instrument[i]!=NULL) {
			g_free(src->wav_based_instrument[i]);
			src->wav_based_instrument[i]=NULL;
		}
	}
	
	// free event list
	jammomidi_free_glist(&(src->eventlist));

  G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
gst_jammo_sampler_finalize (GObject * object)
{
  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static gfloat looping_sampler(GstJammoSampler * src, gint j) {

	gfloat *sampleindex, *xfadeindex, *xfadecounter;
	gint *xfade;
	gint16 *sampletable;
	gfloat value=0.0;
	gfloat scale=1.0; /* This function uses only float type. Typecast in DEFINE_PROCESS */

		/* these temporary variables are used to keep the algorithms readable */
	 	sampleindex=&src->wav_based_instrument[j]->sampleindex;
		sampletable=src->wav_based_instrument[j]->sampletable;
		xfade= &src->wav_based_instrument[j]->xfade;
		xfadecounter= &src->wav_based_instrument[j]->xfadecounter;
		xfadeindex= &src->wav_based_instrument[j]->xfadeindex;

		/* if not finished playing */
		if (*sampleindex < src->wav_based_instrument[j]->file_size) {
			/* Playing */
			// do not interpolate if samplerates match
			if (src->wav_based_instrument[j]->original_samplerate == src->samplerate) {
				value= sampletable[(gint)*sampleindex] / (G_MAXINT16*1.0) * scale;
			}
			else {
				// samplerates do not match, interpolation needed
				if ((gint)(*sampleindex+1) >= src->wav_based_instrument[j]->file_size) {
					// last sample from table is being used. can not interpolate out of sample table
					value= sampletable[(gint)*sampleindex] / (G_MAXINT16*1.0) * scale;
				}
				else {
					value=( jammo_gst_do_interpolation(*sampleindex-(gint)*sampleindex, sampletable[(gint)*sampleindex], sampletable[((gint)*sampleindex+1)]) /(G_MAXINT16*1.0) * scale );
				}
			}
			/* move sampleindex according to needed pitch */
			/* samplerate variable has the output samplerate of jammosampler */
			/* data from sound file is played with correct pitch by interpolating according to file's samplerate */
			src->wav_based_instrument[j]->sampleindex += jammo_gst_do_pitch(src->wav_based_instrument[j]->needed_pitch)*src->wav_based_instrument[j]->original_samplerate/(src->samplerate+0.0);

			/* if note has been released after previous round */
			if (src->wav_based_instrument[j]->released) {
			/* do simple fade out. no jumps in this solution */
			src->wav_based_instrument[j]->releasecounter=0.0;
			/* to avoid changing releasecounter back to zero */
			src->wav_based_instrument[j]->released=FALSE;
			}

			/* fade in to prevent clicks and pops*/
			if (*sampleindex<50) {
				value= (*sampleindex*value/50.0);
			}

			/* fade out to prevent clicks and pops. file is ending */
			if (*sampleindex > src->wav_based_instrument[j]->file_size-src->wav_based_instrument[j]->fade_out) {
				value= ((src->wav_based_instrument[j]->file_size-*sampleindex)*value/(src->wav_based_instrument[j]->fade_out*1.0));
			}

			/* fade out. note has been released */
			if (src->wav_based_instrument[j]->releasecounter >= 0.0) {
				value*= 1.0-(src->wav_based_instrument[j]->releasecounter/(src->wav_based_instrument[j]->fade_out*1.0));
				src->wav_based_instrument[j]->releasecounter++;
				if (src->wav_based_instrument[j]->releasecounter >= src->wav_based_instrument[j]->fade_out) {
					/* Finished playing */
					value=0.0;
					*sampleindex=0;
					*xfadecounter=-1.0;
					src->wav_based_instrument[j]->releasecounter=-1.0;
					src->wav_based_instrument[j]->released=FALSE;
					src->note_table[j]=FALSE;
					src->notes_playing--;
					}
			}

			/* looping  if not released and current position is more than loop_end*/
			if (src->wav_based_instrument[j]->releasecounter < 0.0 && src->wav_based_instrument[j]->sampleindex >= (src->wav_based_instrument[j]->loop_end)-*xfade/2.0) {
				if (*xfade > 0)
					src->wav_based_instrument[j]->xfadecounter=0.0;
				*xfadeindex=*sampleindex+(*sampleindex-(int)*sampleindex);
				*sampleindex=src->wav_based_instrument[j]->loop_start-*xfade/2.0;
			}

			/* xfade when looping */
			if (*xfadecounter >= 0.0 && *sampleindex < src->wav_based_instrument[j]->loop_end) {
					value=jammo_gst_do_xfader(sampletable, xfadecounter, xfadeindex, *xfade, value, scale, src->wav_based_instrument[j]->needed_pitch, src->samplerate);
					/* End x fade if enough rounds */\
					if (*xfadecounter > *xfade) {
						*xfadecounter=-1.0;
					}
			}

		/* End if not finished playing */
		}
		else {
			/* Finished playing */
			value=0.0;
			*sampleindex=0;
			*xfadecounter=-1.0;
			src->wav_based_instrument[j]->releasecounter=-1.0;
			src->wav_based_instrument[j]->released=FALSE;
			src->note_table[j]=FALSE;
			src->notes_playing--;
		}
	/*End sampler code */
	return value;
}

static gfloat nonlooping_sampler(GstJammoSampler * src, gint j) {

	gfloat *sampleindex;
	gint16 *sampletable;
	gfloat value=0.0;
	gfloat scale=1.0; /* This function uses only float type. Typecast in DEFINE_PROCESS */
		/* these temporary variables are used to keep the algorithms readable */
	 	sampleindex=&src->wav_based_instrument[j]->sampleindex;
		sampletable=src->wav_based_instrument[j]->sampletable;

		/* if not finished playing */
		if (*sampleindex < src->wav_based_instrument[j]->file_size) {
			/* Playing */
			// do not interpolate if samplerates match
			if (src->wav_based_instrument[j]->original_samplerate == src->samplerate) {
				value= sampletable[(gint)*sampleindex] / (G_MAXINT16*1.0) * scale;
			}
			else {
				// samplerates do not match, interpolation needed
				if ((gint)(*sampleindex+1) >= src->wav_based_instrument[j]->file_size) {
					// last sample from table is being used. can not interpolate out of sample table
					value= sampletable[(gint)*sampleindex] / (G_MAXINT16*1.0) * scale;
				}
				else {
					value=( jammo_gst_do_interpolation(*sampleindex-(gint)*sampleindex, sampletable[(gint)*sampleindex], sampletable[((gint)*sampleindex+1)]) /(G_MAXINT16*1.0) * scale );
				}
			}
			/* move sampleindex according to needed pitch */
			/* samplerate variable has the output samplerate of jammosampler */
			/* data from sound file is played with correct pitch by interpolating according to file's samplerate */
			src->wav_based_instrument[j]->sampleindex += jammo_gst_do_pitch(src->wav_based_instrument[j]->needed_pitch)*src->wav_based_instrument[j]->original_samplerate/(src->samplerate+0.0);

			/* if note has been released after previous round */
			if (src->wav_based_instrument[j]->released) {
			/* do simple fade out. no jumps in this solution */
			src->wav_based_instrument[j]->releasecounter=0.0;
			/* to avoid changing releasecounter back to zero */
			src->wav_based_instrument[j]->released=FALSE;
			}

			/* fade in to prevent clicks and pops*/
			if (*sampleindex<50) {
				value= (*sampleindex*value/50.0);
			}

			/* fade out to prevent clicks and pops. file is ending */
			if (*sampleindex > src->wav_based_instrument[j]->file_size-src->wav_based_instrument[j]->fade_out) {
				value= ((src->wav_based_instrument[j]->file_size-*sampleindex)*value/(src->wav_based_instrument[j]->fade_out*1.0));
			}

			/* fade out. note has been released */
			if (src->wav_based_instrument[j]->releasecounter >= 0.0) {
				value*= 1.0-(src->wav_based_instrument[j]->releasecounter/(src->wav_based_instrument[j]->fade_out*1.0));
				src->wav_based_instrument[j]->releasecounter++;
				if (src->wav_based_instrument[j]->releasecounter >= src->wav_based_instrument[j]->fade_out) {
					/* Finished playing */
					value=0.0;
					*sampleindex=0;
					src->wav_based_instrument[j]->releasecounter=-1.0;
					src->wav_based_instrument[j]->released=FALSE;
					src->note_table[j]=FALSE;
					src->notes_playing--;
					}
			}

		/* End if not finished playing */
		}
		else {
			/* Finished playing */
			value=0.0;
			*sampleindex=0;
			src->wav_based_instrument[j]->releasecounter=-1.0;
			src->wav_based_instrument[j]->released=FALSE;
			src->note_table[j]=FALSE;
			src->notes_playing--;
		}
	/*End sampler code */
	return value;
}

static gfloat oneshot_sampler(GstJammoSampler * src, gint j) {

	gfloat *sampleindex;
	gint16 *sampletable;
	gfloat value=0.0;
	gfloat scale=1.0; /* This function uses only float type. Typecast in DEFINE_PROCESS */
//printf("oneshot\n");
		/* these temporary variables are used to keep the algorithms readable */
	 	sampleindex=&src->wav_based_instrument[j]->sampleindex;
		sampletable=src->wav_based_instrument[j]->sampletable;

		/* if not finished playing */
		if (*sampleindex < src->wav_based_instrument[j]->file_size) {
			/* Playing */
			// do not interpolate if samplerates match
			if (src->wav_based_instrument[j]->original_samplerate == src->samplerate) {
				value= sampletable[(gint)*sampleindex] / (G_MAXINT16*1.0) * scale;
			}
			else {
				// samplerates do not match, interpolation needed
				if ((gint)(*sampleindex+1) >= src->wav_based_instrument[j]->file_size) {
					// last sample from table is being used. can not interpolate out of sample table
					value= sampletable[(gint)*sampleindex] / (G_MAXINT16*1.0) * scale;
				}
				else {
					value=( jammo_gst_do_interpolation(*sampleindex-(gint)*sampleindex, sampletable[(gint)*sampleindex], sampletable[((gint)*sampleindex+1)]) /(G_MAXINT16*1.0) * scale );
				}
			}
			/* move sampleindex according to needed pitch */
			/* samplerate variable has the output samplerate of jammosampler */
			/* data from sound file is played with correct pitch by interpolating according to file's samplerate */
			src->wav_based_instrument[j]->sampleindex += jammo_gst_do_pitch(src->wav_based_instrument[j]->needed_pitch)*src->wav_based_instrument[j]->original_samplerate/(src->samplerate+0.0);

			/* fade in to prevent clicks and pops*/
			if (*sampleindex<50) {
				value= (*sampleindex*value/50.0);
			}

			/* fade out to prevent clicks and pops. file is ending */
			if (*sampleindex > src->wav_based_instrument[j]->file_size-src->wav_based_instrument[j]->fade_out) {
				value= ((src->wav_based_instrument[j]->file_size-*sampleindex)*value/(src->wav_based_instrument[j]->fade_out*1.0));
			}
		/* End if not finished playing */
		}
		else {
			/* Finished playing */
			value=0.0;
			*sampleindex=0;
			src->wav_based_instrument[j]->releasecounter=-1.0;
			src->wav_based_instrument[j]->released=FALSE;
			src->note_table[j]=FALSE;
			src->notes_playing--;
		}
	/*End sampler code */
	return value;
}


static void do_playback_process(GstJammoSampler * src){
	/*GstClockTime now = gst_clock_get_time(gst_system_clock_obtain()); //time just now		
	GstClockTime current = GST_CLOCK_DIFF(src->playback_starting_time,now);*/

	JammoMidiEvent* event;
	GstClockTime timestamp;
	/*
	timestamp is in nanoseconds = ns
	e.g.:
	6.081 683 703 s
	6 081.683 703 ms
	6 081 683.703 µs
	6 081 683 703 ns
	*/

	event = g_list_nth_data(src->eventlist,src->playback_index_for_event);
	if (event==NULL)
		{
			int i=0;
			gboolean finished=TRUE;
			// check if all notes have finished playing
			for(i=0;i<JAMMOMIDI_NUMBER_OF_NOTES;i++)
				if (src->note_table[i]==TRUE)
					finished=FALSE;

			if (finished) {
				src->eos_reached=TRUE;
				src->playback_not_started=TRUE;
				src->last_event_processed=FALSE;
			}
			return;
		}
	timestamp = event->timestamp;

	while (src->time * src->tempo_scale > timestamp){
		//printf("DEBUG: gstjammosampler: delayed: %llu ms\n",(unsigned long long) ((src->time * src->tempo_scale - timestamp)/src->tempo_scale)/1000/1000);
		int note=event->note + src->pitch_shift;

		//note is unsigned.
		//We can use sign to tell type of note. ON= +, OFF= -
		int sign = event->type==JAMMOMIDI_NOTE_ON? 1:-1;
		//printf("note %d, sign %d\n",note,sign);

		//Report for visualization
		GstStructure *s;\
		s = gst_structure_new ("sampler", "note", G_TYPE_INT, sign * note, NULL);
		GstMessage *m = gst_message_new_element (GST_OBJECT (src), s);
		gst_element_post_message (GST_ELEMENT (src), m);


		if (event->type==JAMMOMIDI_NOTE_ON && note>-1 && note < JAMMOMIDI_NUMBER_OF_NOTES) {
			// if the note was not playing increment the number of playing notes
			if (src->note_table[note]==FALSE)
				src->notes_playing++;
			// start playing the note from the beginning
			if (src->wav_based_instrument[note]->filename!=NULL) {
				src->note_table[note]=TRUE;
				src->wav_based_instrument[note]->sampleindex=0;
				src->wav_based_instrument[note]->xfadecounter=-1.0;
				src->wav_based_instrument[note]->releasecounter=-1.0;
				src->wav_based_instrument[note]->released=FALSE;
			}
			else {
				// error with the sample file
				src->note_table[note]=FALSE;
				printf("gstjammosampler: error. Trying play note/octave not in sample file\n");
			}
		}

		else if (event->type==JAMMOMIDI_NOTE_OFF && note>-1 && note < JAMMOMIDI_NUMBER_OF_NOTES) {
			// release note only, the number of notes will be decreased when the note has faded
			src->wav_based_instrument[note]->released=TRUE;
		}
		else {
			printf("a faulty event in list\n");
			printf("note: %d\n", note);
		}

		// move to the next event
		src->playback_index_for_event++;

		// check if there are no more events
		if (src->playback_index_for_event==g_list_length(src->eventlist)) {
			int i=0;
			src->last_event_processed=TRUE;
			// release all notes just in case some note-off is missing
			for(i=0;i<JAMMOMIDI_NUMBER_OF_NOTES;i++)
				src->wav_based_instrument[i]->released=TRUE;
		}

		// get next event from the list
		event = g_list_nth_data(src->eventlist,src->playback_index_for_event);
		if (event==NULL)
		{
			return;
		}
		// get the time of the event
		timestamp = event->timestamp;
	}

}

#define DEFINE_PROCESS(type,scale) \
static void gst_jammo_sampler_process_##type (GstJammoSampler * src, g##type * samples) {\
	/* using a static number of maximum simultaneous notes to make sampler slightly lighter */\
	/* malloc consumes some cycles and usually there is no need to play many notes simultaneously */ \
	/* edit MAXNOTES in header file to change the number of maximum simultaneous notes */ \
  static g##type values[MAXNOTES];\
	if (!(src->recording)) /*we are in playback mode*/ \
		do_playback_process(src); \
\
	/* if volume is divided with number of playing tones instrument */ \
	/* gets quieter when more simultaneous notes are played */ \
\
	/* this way overall volume can be adjusted according to number */ \
	/* of playing notes but without lowering the overall volume too much */ \
\
	/* however it is possible to get audible clipping this way. */ \
	/*one way to avoid this is not to scale samples to full volume, i.e. */ \
	/*use smaller scale value than currently. in another words leaving some*/ \
	/*headroom to simultaneously playing samples */ \
\
	/* at the moment sampler works fine with flute, drum kit and ud.*/ \
	/*clipping is not too bad */\
  gdouble used_volume=src->volume/pow(src->notes_playing*1.0, 0.3);\
  gint i,j;\
  g##type value=0;\
\
  gdouble amp; \
  gint counter=0;\
  for (i=0;i<MAXNOTES;i++)\
     values[i]=0.0;\
\
  amp = used_volume * scale;\
\
  /*Process whole buffer*/ \
  for (i = 0; i < src->generate_samples_per_buffer; i++) \
      {\
\
      /* Find what tone must be processed */ \
      counter=0;\
      for (j=0;j<JAMMOMIDI_NUMBER_OF_NOTES;j++) \
	  {\
	  if (src->note_table[j]) \
	    {\
	    if (src->category==GST_JAMMO_SAMPLER_SAMPLER_LOOPING)\
		values[counter]=(g##type)(amp*looping_sampler(src, j));\
	    if (src->category==GST_JAMMO_SAMPLER_SAMPLER_NONLOOPING)\
		values[counter]=(g##type)(amp*nonlooping_sampler(src, j));\
	    if (src->category==GST_JAMMO_SAMPLER_SAMPLER_ONESHOT)\
		values[counter]=(g##type)(amp*oneshot_sampler(src, j));\
	    counter++;\
			if (counter>=MAXNOTES-1) \
				break; \
	    /* end if note is playing */ \
	    } \
	  /* end processing tones */ \
	  }\
\
      /*Add all playing notes together */ \
      if (counter==0) \
	samples[i]=0;\
      else \
	  { \
	  value=values[0];	\
	  for (j=1;j<counter;j++)\
	  /* Hard clipping. When adding values we may end up */ \
	  /*with too big values. Although very nasty this sounds much */ \
	  /*better than data type overflows */ \
		  if (value + values[j]>scale || value + values[j]< -scale) \
		      value=(1.0*value/fabs(value*1.0))*scale; \
		  else \
		  /* Add values of all playing tones together */ \
		      value=value + values[j];\
		  samples[i]=value;\
	  } \
\
      } \
}

DEFINE_PROCESS (int16, 32767.0)
DEFINE_PROCESS (int32, 2147483647.0)
DEFINE_PROCESS (float, 1.0)
DEFINE_PROCESS (double, 1.0)


static JammoSamplerProcessFunc process_funcs[] = {
  (JammoSamplerProcessFunc) gst_jammo_sampler_process_int16,
  (JammoSamplerProcessFunc) gst_jammo_sampler_process_int32,
  (JammoSamplerProcessFunc) gst_jammo_sampler_process_float,
  (JammoSamplerProcessFunc) gst_jammo_sampler_process_double
};

static gboolean gst_jammo_sampler_setcaps (GstBaseSrc * basesrc, GstCaps * caps)
{
  GstJammoSampler *src = GST_JAMMO_SAMPLER (basesrc);
  const GstStructure *structure;
  const gchar *name;
  gint width;
  gboolean ret;

  structure = gst_caps_get_structure (caps, 0);
  ret = gst_structure_get_int (structure, "rate", &src->samplerate);

  GST_DEBUG_OBJECT (src, "negotiated to samplerate %d", src->samplerate);

  name = gst_structure_get_name (structure);
  if (strcmp (name, "audio/x-raw-int") == 0) {
    ret &= gst_structure_get_int (structure, "width", &width);
    src->format = (width == 32) ? GST_JAMMO_SAMPLER_FORMAT_S32 :
        GST_JAMMO_SAMPLER_FORMAT_S16;
  } else {
    ret &= gst_structure_get_int (structure, "width", &width);
    src->format = (width == 32) ? GST_JAMMO_SAMPLER_FORMAT_F32 :
        GST_JAMMO_SAMPLER_FORMAT_F64;
  }

  /* allocate a new buffer suitable for this pad */
  switch (src->format) {
    case GST_JAMMO_SAMPLER_FORMAT_S16:
      //printf("format is now gint16\n");
      src->sample_size = sizeof (gint16);
      break;
    case GST_JAMMO_SAMPLER_FORMAT_S32:
      //printf("format is now gint32\n");
      src->sample_size = sizeof (gint32);
      break;
    case GST_JAMMO_SAMPLER_FORMAT_F32:
      //printf("format is now gfloat32\n");
      src->sample_size = sizeof (gfloat);
      break;
    case GST_JAMMO_SAMPLER_FORMAT_F64:
      //printf("format is now gfloat32\n");
      src->sample_size = sizeof (gdouble);
      break;
    default:
      /* can't really happen */
      ret = FALSE;
      break;
  }

  if (src->format == -1) {
    src->process = NULL;
    return FALSE;
  }
  src->process = process_funcs[src->format];

  return ret;
}

static void gst_jammo_sampler_get_times (GstBaseSrc * basesrc, GstBuffer * buffer,
    GstClockTime * start, GstClockTime * end)
{
	GstJammoSampler *src = GST_JAMMO_SAMPLER (basesrc);

	GstClockTime timestamp = GST_BUFFER_TIMESTAMP (buffer);
	// store time of buffer, it is used in playback mode for processing events
	// and in recording mode for timestamping
	src->time = timestamp;

  /* for live sources, sync on the timestamp of the buffer */
  if (gst_base_src_is_live (basesrc)) {

    if (GST_CLOCK_TIME_IS_VALID (timestamp)) {
      /* get duration to calculate end time */
      GstClockTime duration = GST_BUFFER_DURATION (buffer);

      if (GST_CLOCK_TIME_IS_VALID (duration)) {
        *end = timestamp + duration;
      }
      *start = timestamp;
    }
  } else {
    *start = -1;
    *end = -1;
  }
}

static gboolean gst_jammo_sampler_start (GstBaseSrc * basesrc)
{
  GstJammoSampler *src = GST_JAMMO_SAMPLER (basesrc);
  int i;

  //printf("DEBUG:gstjammosampler: jammo_sampler_starts\n");

  src->next_sample = 0;
  src->next_byte = 0;
  src->next_time = 0;
  src->check_seek_stop = FALSE;
  src->eos_reached = FALSE;
  src->tags_pushed = FALSE;
  src->time = 0;
  for (i=0; i<JAMMOMIDI_NUMBER_OF_NOTES;i++)
       src->accumulators[i] = 0;


  //realtime recording
  if (src->recording) {
    //printf("DEBUG:gstjammosampler: recording mode\n");
    src->eventlist=NULL;
    src->creation_time = gst_clock_get_time(gst_system_clock_obtain()); //time just now
    src->playback_not_started=TRUE;
  }
  //playback
  else  {
    //printf("DEBUG:gstjammosampler: not recording, playback\n");

    //src->playback_starting_time = gst_clock_get_time(gst_system_clock_obtain());
    src->playback_not_started=FALSE;
    src->playback_index_for_event=0;

    /*
    //Test:
   GList* list;
   JammoMidiEvent* n;

   for (list = src->eventlist; list; list = list->next) {
      n = (list->data);
      printf("TEST:gstjammosampler:In stack: %llu ms: note:%d ,type: %s \n", (unsigned long long)n->timestamp/1000/1000
            ,n->note,n->type==JAMMOMIDI_NOTE_ON? "start":"stop");
   }
   */
  }


  return TRUE;
}

static gboolean gst_jammo_sampler_stop (GstBaseSrc * basesrc)
{
  return TRUE;
}

/* seek to time, will be called when we operate in push mode. In pull mode we
 * get the requiested byte offset. */
static gboolean gst_jammo_sampler_do_seek (GstBaseSrc * basesrc, GstSegment * segment)
{
  GstJammoSampler *src = GST_JAMMO_SAMPLER (basesrc);
  GstClockTime time_to_seek;

  segment->time = segment->start;
  time_to_seek = segment->last_stop;
  
	// seek eventlist to correct position
	src->time = segment->time;
	src->playback_index_for_event=0;
	JammoMidiEvent * event =NULL;
	while (1) {
		event=g_list_nth_data(src->eventlist,src->playback_index_for_event);
		// stop seeking if all events seeked
		if (event==NULL) {
			break;
		}
		// stop seeking if next event is before current time
		if (src->time <= event->timestamp) {
			break;
		}
		// no need to stop seeking yet
		src->playback_index_for_event+=1;
	}
	// turn all notes off
	gint i;
	for (i=0;i<JAMMOMIDI_NUMBER_OF_NOTES;i++){
		if (src->wav_based_instrument[i]->filename!=NULL) {
			src->note_table[i]=FALSE;
			src->wav_based_instrument[i]->sampleindex=0;
			src->wav_based_instrument[i]->xfadecounter=-1.0;
			src->wav_based_instrument[i]->releasecounter=-1.0;
			src->wav_based_instrument[i]->released=FALSE;
		}
	}

  /* now move to the time indicated */
  src->next_sample =
      gst_util_uint64_scale_int (time_to_seek, src->samplerate, GST_SECOND);
  src->next_byte = src->next_sample * src->sample_size;
  src->next_time =
      gst_util_uint64_scale_int (src->next_sample, GST_SECOND, src->samplerate);

  g_assert (src->next_time <= time_to_seek);

  if (GST_CLOCK_TIME_IS_VALID (segment->stop)) {
    time_to_seek = segment->stop;
    src->sample_stop = gst_util_uint64_scale_int (time_to_seek, src->samplerate,
        GST_SECOND);
    src->check_seek_stop = TRUE;
  } else {
    src->check_seek_stop = FALSE;
  }
  src->eos_reached = FALSE;

  return TRUE;
}

static gboolean gst_jammo_sampler_is_seekable (GstBaseSrc * basesrc)
{
  /* we're seekable... */
  return TRUE;
}

static gboolean gst_jammo_sampler_check_get_range (GstBaseSrc * basesrc)
{
  GstJammoSampler *src;

  src = GST_JAMMO_SAMPLER (basesrc);

  /* if we can operate in pull mode */
  return src->can_activate_pull;
}

static GstFlowReturn gst_jammo_sampler_create (GstBaseSrc * basesrc, guint64 offset,
    guint length, GstBuffer ** buffer)
{
  GstFlowReturn res;
  GstJammoSampler *src;
  GstBuffer *buf;
  GstClockTime next_time;
  gint64 next_sample, next_byte;
  guint bytes, samples;

  src = GST_JAMMO_SAMPLER (basesrc);

  /* example for tagging generated data */
  if (!src->tags_pushed) {
    GstTagList *taglist;
    GstEvent *event;

    taglist = gst_tag_list_new ();

    gst_tag_list_add (taglist, GST_TAG_MERGE_APPEND,
        GST_TAG_DESCRIPTION, "jammo instrument", NULL);

    event = gst_event_new_tag (taglist);
    gst_pad_push_event (basesrc->srcpad, event);
    src->tags_pushed = TRUE;
  }

  if (src->eos_reached)
    return GST_FLOW_UNEXPECTED;

  /* if no length was given, use our default length in samples otherwise convert
   * the length in bytes to samples. */
  /*if (length == -1)
    samples = src->samples_per_buffer;
  else
    samples = length / src->sample_size;*/

	// we need to use smaller buffer than requested
	// otherwise there will be too much latency
	samples = src->samples_per_buffer;

  /* if no offset was given, use our next logical byte */
  if (offset == -1)
    offset = src->next_byte;

  /* now see if we are at the byteoffset we think we are */
  if (offset != src->next_byte) {
    GST_DEBUG_OBJECT (src, "seek to new offset %" G_GUINT64_FORMAT, offset);
    /* we have a discont in the expected sample offset, do a 'seek' */
    src->next_sample = src->next_byte / src->sample_size;
    src->next_time =
        gst_util_uint64_scale_int (src->next_sample, GST_SECOND,
        src->samplerate);
    src->next_byte = offset;
  }

  /* check for eos */
  if (src->check_seek_stop &&
      (src->sample_stop > src->next_sample) &&
      (src->sample_stop < src->next_sample + samples)
      ) {
    /* calculate only partial buffer */
    src->generate_samples_per_buffer = src->sample_stop - src->next_sample;
    next_sample = src->sample_stop;
    src->eos_reached = TRUE;
  } else {
    /* calculate full buffer */
    src->generate_samples_per_buffer = samples;
    next_sample = src->next_sample + samples;
  }

  bytes = src->generate_samples_per_buffer * src->sample_size;

  if ((res = gst_pad_alloc_buffer (basesrc->srcpad, src->next_sample,
              bytes, GST_PAD_CAPS (basesrc->srcpad), &buf)) != GST_FLOW_OK) {
    return res;
  }

  next_byte = src->next_byte + bytes;
  next_time = gst_util_uint64_scale_int (next_sample, GST_SECOND,
      src->samplerate);

  GST_LOG_OBJECT (src, "samplerate %d", src->samplerate);
  GST_LOG_OBJECT (src, "next_sample %" G_GINT64_FORMAT ", ts %" GST_TIME_FORMAT,
      next_sample, GST_TIME_ARGS (next_time));

  GST_BUFFER_TIMESTAMP (buf) = src->timestamp_offset + src->next_time;
  GST_BUFFER_OFFSET (buf) = src->next_sample;
  GST_BUFFER_OFFSET_END (buf) = next_sample;
  GST_BUFFER_DURATION (buf) = next_time - src->next_time;

  gst_object_sync_values (G_OBJECT (src), src->next_time);

  src->next_time = next_time;
  src->next_sample = next_sample;
  src->next_byte = next_byte;

  GST_LOG_OBJECT (src, "generating %u samples at ts %" GST_TIME_FORMAT,
      src->generate_samples_per_buffer,
      GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (buf)));

  src->process (src, GST_BUFFER_DATA (buf));

  if (G_UNLIKELY (src->volume == 0.0)) {
    GST_BUFFER_FLAG_SET (buf, GST_BUFFER_FLAG_GAP);
  }

  *buffer = buf;

  return GST_FLOW_OK;
}


//Simple Tokenizer
//////////////////
/*
Call first tokenizer, with string and char.
Then call get_token(&token) until NULL is returned.
Handle only one tokenizer at time.
*/
const gchar *base_row;    //Tokenized string
char token_mark;    //Char between each token
int previous_index; //last tokens end index
gboolean last_token_used; //is end reached
char *token_pch;	   //


static void tokenizer(const gchar *str, char mark){
  base_row=str;
  token_mark=mark;
  previous_index=0;
  last_token_used=FALSE;
  token_pch= strchr (str,token_mark);
}

/*Put new token in given parameter
Return NULL, if all is used.*/
static void get_token(gchar **value){
  if (last_token_used){
    *value=NULL;
    return;
  }

  gchar *token;
  if (token_pch!=NULL) {
      int end_point=(token_pch-base_row+1);
      //printf ("debug: found at %d\n",end_point);
      int length=end_point-previous_index-1;
      token = g_strndup (base_row+previous_index,length);

      previous_index=end_point;
      token_pch=strchr(token_pch+1,token_mark);
      //printf("debug: token: %s\n",token);
      *value=token;
      return;
    }

  //last token must be handled separatedly
  token = g_strndup (base_row+previous_index,strlen(base_row)-previous_index-1);
  //printf("debug: token: %s\n",token);
  *value=token;
  last_token_used=TRUE;
}
////////////////////////////////////////////

static void
gst_jammo_sampler_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstJammoSampler *src = GST_JAMMO_SAMPLER (object);
  GstClockTime timestamp;

  switch (prop_id) {
    case PROP_SAMPLES_PER_BUFFER:
      src->samples_per_buffer = g_value_get_int (value);
      break;
    case PROP_CATEGORY:
      src->category = g_value_get_int (value);
    break;

    //First call this and then INITIALIZE_INSTRUMENT
    case PROP_INSTRUMENT_FOLDER:
      src->base_folder = g_strdup_printf("%s",g_value_get_string(value));
    break;

    //this is meant to call with filename containing all information of instrument
    case PROP_INITIALIZE_INSTRUMENT:
      ;
      //this is file we read
      const gchar *instrument_filename = g_value_get_string(value);

      //these are all values we need for each note
      int min;
      int max;
      const gchar *filename;
      int loop_start;
      int loop_end;
      int file_size;
      int base;
      int xfade;
      int fade_out;

      //These are needed for file handling
      int bytes_read;
      size_t nbytes = 100;
      char *current;
      current = (char *) malloc (nbytes + 1);
      int cont=1; /*this is used for while-loops*/

      FILE *ifp;

      ifp = fopen(instrument_filename, "r");
      if (ifp == NULL) {
	printf("Can't open input file '%s'\n",instrument_filename);
	break;
      }

      //read general values
      bytes_read = getline (&current, &nbytes, ifp);
      if (bytes_read <= 0) {
	printf("instrument file '%s' is empty\n",instrument_filename);
	break;
      }

      //First handle general values
      if (strncmp(current,"LOOPING",7)==0)
	  src->category=GST_JAMMO_SAMPLER_SAMPLER_LOOPING;
      if (strncmp(current,"NONLOOPING",10)==0)
	  src->category=GST_JAMMO_SAMPLER_SAMPLER_NONLOOPING;
      if (strncmp(current,"ONESHOT",7)==0)
	  src->category=GST_JAMMO_SAMPLER_SAMPLER_ONESHOT;

      //This is the loop which read whole file until end is reached
      while (cont){
	  bytes_read = getline (&current, &nbytes, ifp);
	  if (bytes_read <= 0) {
	    cont=0;
	  } else {
		//printf("debug: row='%s'",current);
		tokenizer(current,',');
		char *token;
		get_token(&token);
		filename=g_strdup_printf("%s%s",src->base_folder,token); //Add base-folder before filename
		//printf("base_folder='%s', token='%s', filename='%s'\n",src->base_folder,token,filename);

		get_token(&token);
		base= get_note(token[0])+12*atoi(&token[1]);
		get_token(&token);
		min = get_note(token[0])+12*atoi(&token[1]);
		get_token(&token);
		max = get_note(token[0])+12*atoi(&token[1]);
		//printf("base: %d,min:%d, max:%d\n", base,min,max);
		
		get_token(&token);
		loop_start=atoi(token);
		get_token(&token);
		loop_end=atoi(token);
		get_token(&token);
		file_size=atoi(token);
		//printf("loop_start:%d,loop_end:%d,file_size:%d\n",loop_start,loop_end,file_size);

		get_token(&token);
		xfade=atoi(token);
		get_token(&token);
		fade_out=atoi(token);
		//printf("xfade:%d,fade_out:%d\n\n",xfade,fade_out);
		
		//All values are now read
		int j;
		for (j=min;j<=max;j++)
		    {
		    init_sampler(src, j,
		    filename,
		    loop_start,
		    loop_end,
		    file_size,
		    j-base,
		    xfade,
		    fade_out);
		    }
		
	}
      }

      break;


		case PROP_REMOVE_EVENT:
			;
			JammoMidiEvent* event = (JammoMidiEvent*)(g_value_get_pointer(value));
			unsigned char note_ = event->note;

			printf("remove event\n");
			timestamp= event->timestamp;


			JammoMidiEvent* candidate_for_removal=NULL;
			GList* list;
			JammoMidiEvent* n;

			//printf("Removing this: %llu ms: note:%d \n",(unsigned long long)timestamp,note_);
			for (list = src->eventlist; list; list = list->next) {
				//printf(".\n");
				n = (list->data);
				//printf("Scanning stack: %llu ms: note:%d ,type: %s \n", (unsigned long long)n->timestamp,get_note(n->note), n->type==JAMMOMIDI_NOTE_ON? "start":"stop");

				//First find last START
				if (note_!= n->note)
					{
					//printf(" This is not asked note-octave \n");
					continue;
					}
				//printf(" This is asked note-octave\n");

				if (n->type==JAMMOMIDI_NOTE_OFF && candidate_for_removal==NULL) {
					//printf("  We are not yet found any start, and this is stop, so keep looking.\n");
					continue;
				}

				if (timestamp >= n->timestamp )
					{
						//printf("time is correct\n");
						if (n->type==JAMMOMIDI_NOTE_ON)
							{
							//printf("   START found\n");
							candidate_for_removal=n;
							//printf("   set this for candidate_for_removal\n");
							}
					}

					else{
							if (n->type==JAMMOMIDI_NOTE_OFF) {
								//printf("STOP found\n");
								//printf("Now DELETE candidate (i.e start) and this stop\n");
								src->eventlist = g_list_remove(src->eventlist,n);
								g_free(n);
							}

							else if (n->type==JAMMOMIDI_NOTE_ON) {
								//printf("Second start without stop, but this is not asked (because of time)\n");
								//printf("Delete only candidate (i.e previous start)\n");
							}

							src->eventlist = g_list_remove(src->eventlist,candidate_for_removal);
							g_free(candidate_for_removal);
							candidate_for_removal=NULL;
							continue;
						}

			}

			//If candidate_for_removal is set but not removed, remove it now
			if (candidate_for_removal !=NULL) {
				//printf("We found only start event, but not stop. Remove that start now.\n");
				src->eventlist = g_list_remove(src->eventlist,candidate_for_removal);
				g_free(candidate_for_removal);
			}

		//printf("removing event done\n");
			break;

    case PROP_ADD_EVENT:
			;
			//JammoMidiEvent* event;
			event = (JammoMidiEvent*)(g_value_get_pointer(value));
			unsigned char note = event->note;

			//realtime. There are no timestamp in event, use current time
			if (src->recording){
			//printf("realtime\n");
			timestamp = src->time * src->tempo_scale;

			event->timestamp=timestamp;
			// when pitch shifting must make sure that note will not be less than zero
			// note that pitch shifting only affects playback, not stored event
			// stored events will be pitch shifted when played back
			if (((gint)note)+ src->pitch_shift>=0) {
				note+=src->pitch_shift;
			}

		if (event->type==JAMMOMIDI_NOTE_ON) {
		/* It should be checked that note can be played, i.e. not all sampler
		instruments can play c0. Playing notes that are not possible to play was a
		likely source of segmentation faults with jammo's VI mode. no segfaults after
		this code was added */
		if (note < JAMMOMIDI_NUMBER_OF_NOTES) {
			if (src->wav_based_instrument[note]->filename!=NULL) {
				if (src->note_table[note]==FALSE)
					src->notes_playing++;
				src->note_table[note]=TRUE;
				src->wav_based_instrument[note]->sampleindex=0;
				src->wav_based_instrument[note]->xfadecounter=-1.0;
				src->wav_based_instrument[note]->releasecounter=-1.0;
				src->wav_based_instrument[note]->released=FALSE;
			}
			else {
				// error with the sample file
				src->note_table[note]=FALSE;
				printf("gstjammosampler: error. Trying add note/octave not in sample file\n");
			}
		}
	}
	else if (note < JAMMOMIDI_NUMBER_OF_NOTES) { //JAMMOMIDI_NOTE_OFF
		// release note only, the number of notes will be decreased when the note has faded
		src->wav_based_instrument[note]->released=TRUE;
	}


	}

	else { //adding non-realtime (nothing special)
			//printf("non-realtime\n");
			}

	src->eventlist = g_list_insert_sorted(src->eventlist ,event,compare_event_time);


	//printf("DEBUG: gstjammosampler: added event %llu ms: note:'%u'(%s %d) ,type=%s\n",(unsigned long long)(event->timestamp/1000/1000),note,jammomidi_note_to_char(note),note/12,event->type==JAMMOMIDI_NOTE_ON? "start":"stop");

	break;
    case PROP_CLEAR_EVENTS:
      if (g_value_get_boolean (value)) {
	      jammomidi_free_glist(&(src->eventlist));
	      src->eventlist=NULL;
      }
      break;
    case PROP_VOLUME:
      src->volume = g_value_get_double (value);
      break;
    case PROP_IS_LIVE:
      gst_base_src_set_live (GST_BASE_SRC (src), g_value_get_boolean (value));
      break;
    case PROP_TIMESTAMP_OFFSET:
      src->timestamp_offset = g_value_get_int64 (value);
      break;
    case PROP_CAN_ACTIVATE_PUSH:
      GST_BASE_SRC (src)->can_activate_push = g_value_get_boolean (value);
      break;
    case PROP_CAN_ACTIVATE_PULL:
      src->can_activate_pull = g_value_get_boolean (value);
      break;
    case PROP_RECORDING:
      src->recording = g_value_get_boolean (value);
			// when playbacking sampler needs to be not is-live so that seeking works better
			// when recording sampler needs to be is-live for smoother response
      gst_base_src_set_live (GST_BASE_SRC (src), g_value_get_boolean (value));
      break;
   case PROP_EVENTLIST:
      //We got GList containing JammoMidiEvents, take all one-by-one and add to own internal eventlist
      for (list = g_value_get_pointer (value); list; list = list->next) {
        n = (list->data);
        //printf("CASE PROP_EVENTLIST:gstjammosampler:Add: %llu ns: note:%d ,type: %s \n", (unsigned long long)n->timestamp,n->note, n->type==0? "start":"stop");
				// we want to allocate memory for the event so that nothing else will delete events
				// from samplers list
				jammomidi_store_event_to_glist(&(src->eventlist), n->note, n->type, n->timestamp);
        }
      break;
    case PROP_TEMPO_SCALE:
      src->tempo_scale = g_value_get_float (value);
      break;
    case PROP_PITCH_SHIFT:
      src->pitch_shift = g_value_get_int (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void gst_jammo_sampler_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstJammoSampler *src = GST_JAMMO_SAMPLER (object);

  switch (prop_id) {
    case PROP_SAMPLES_PER_BUFFER:
      g_value_set_int (value, src->samples_per_buffer);
      break;

    case PROP_CATEGORY:
      g_value_set_enum (value, src->category);
      break;

    /*Not meant to ask*/
    case PROP_INITIALIZE_INSTRUMENT:
      g_value_set_enum (value, -1);
      break;
    case PROP_INSTRUMENT_FOLDER:
      g_value_set_string (value, src->base_folder);
      break;
    case PROP_ADD_EVENT:
      break;
    case PROP_VOLUME:
      g_value_set_double (value, src->volume);
      break;
    case PROP_IS_LIVE:
      g_value_set_boolean (value, gst_base_src_is_live (GST_BASE_SRC (src)));
      break;
    case PROP_TIMESTAMP_OFFSET:
      g_value_set_int64 (value, src->timestamp_offset);
      break;
    case PROP_CAN_ACTIVATE_PUSH:
      g_value_set_boolean (value, GST_BASE_SRC (src)->can_activate_push);
      break;
    case PROP_CAN_ACTIVATE_PULL:
      g_value_set_boolean (value, src->can_activate_pull);
      break;
    case PROP_RECORDING:
      g_value_set_boolean (value, src->recording);
      break;
		case PROP_EVENTLIST:
			g_value_set_pointer (value, src->eventlist);
			break;
		case PROP_TEMPO_SCALE:
			g_value_set_float (value, src->tempo_scale);
			break;
		case PROP_PITCH_SHIFT:
			g_value_set_int (value, src->pitch_shift);
			break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}
