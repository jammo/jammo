#ifndef _JAMMO_LOOP_
#define _JAMMO_LOOP_

#include <glib.h>
#include <gst/gst.h>
#include <string.h>

typedef struct _JammoLoop {
	guint32 loop_id, slot;
} JammoLoop;

int jammo_loop_glist_to_file(GList * list, char * filename);

GList * jammo_loop_file_to_glist(char * filename);

void jammo_loop_free_glist(GList ** list);

void jammo_loop_store_loop_to_glist(GList ** list, guint32 loop_id, guint32 slot);

void jammo_loop_print_loop(JammoLoop * loop);
#endif  /*_JAMMO_LOOP_ */
