/*
 * jammo-backing-track.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */
 
#include "jammo-backing-track.h"
#include "jammo-meam-private.h"
#include "jammo-meam.h"
#include <string.h>

#include <tangle.h> //For tangle_lookup_filename
#include "../cem/cem.h"

G_DEFINE_TYPE(JammoBackingTrack, jammo_backing_track, JAMMO_TYPE_PLAYING_TRACK);

enum {
	PROP_0,
	PROP_FILENAME,
	PROP_REAL_FILENAME
};

struct _JammoBackingTrackPrivate {
	gchar* filename;
	gchar* real_filename;
	guint tempo;
	gchar* pitch;
	GstElement* source;
	guint64 duration;
};

typedef struct {
	JammoBackingTrack* backing_track;
	GstElement* pipeline;
} PrerollData;

static void on_new_decoded_pad(GstElement* element, GstPad* pad, gboolean last, gpointer data);
static void preroll(JammoBackingTrack* backing_track);
static void setup_source(JammoBackingTrack* backing_track);

JammoBackingTrack* jammo_backing_track_new(const gchar* filename) {

	return JAMMO_BACKING_TRACK(g_object_new(JAMMO_TYPE_BACKING_TRACK, "filename", filename, NULL));
}

const gchar* jammo_backing_track_get_filename(JammoBackingTrack* backing_track) {
	g_return_val_if_fail(JAMMO_IS_BACKING_TRACK(backing_track), NULL);

	return backing_track->priv->filename;
}

void jammo_backing_track_set_filename(JammoBackingTrack* backing_track, const gchar* filename) {
	g_return_if_fail(JAMMO_IS_BACKING_TRACK(backing_track));

	if (g_strcmp0(backing_track->priv->filename, filename)) {
		g_free(backing_track->priv->filename);
		backing_track->priv->filename = g_strdup(filename);
		setup_source(backing_track);
		g_object_notify(G_OBJECT(backing_track), "filename");
	}
}

static void jammo_backing_track_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoBackingTrack* backing_track;
	
	backing_track = JAMMO_BACKING_TRACK(object);

	switch (prop_id) {
		case PROP_FILENAME:
			jammo_backing_track_set_filename(backing_track, g_value_get_string(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_backing_track_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoBackingTrack* backing_track;

	backing_track = JAMMO_BACKING_TRACK(object);

        switch (prop_id) {
		case PROP_FILENAME:
			g_value_set_string(value, backing_track->priv->filename);
			break;
		case PROP_REAL_FILENAME:
			g_value_set_string(value, backing_track->priv->filename);
			break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static guint64 jammo_backing_track_get_duration(JammoTrack* track) {
	JammoBackingTrack* backing_track;
	
	backing_track = JAMMO_BACKING_TRACK(track);
	
	if (backing_track->priv->real_filename &&
	    backing_track->priv->duration == JAMMO_DURATION_INVALID &&
	    ((backing_track->priv->duration = jammo_meam_get_cached_duration(backing_track->priv->real_filename)) == JAMMO_DURATION_INVALID)) {
		gchar* message = g_strdup_printf("No cached duration for backing-track-sample '%s', prerolling.", backing_track->priv->real_filename);
		cem_add_to_log(message,J_LOG_WARN);
		g_free(message);
		preroll(backing_track);
	}

	return JAMMO_BACKING_TRACK(track)->priv->duration;
}

static void jammo_backing_track_set_tempo(JammoTrack* track, guint tempo) {
	JammoBackingTrack* backing_track;
	
	backing_track = JAMMO_BACKING_TRACK(track);

	if (backing_track->priv->tempo != tempo) {
		backing_track->priv->tempo = tempo;
		setup_source(backing_track);	
	}
}

static void jammo_backing_track_set_pitch(JammoTrack* track, const gchar* pitch) {
	JammoBackingTrack* backing_track;
	
	backing_track = JAMMO_BACKING_TRACK(track);

	if (g_strcmp0(backing_track->priv->pitch, pitch)) {
		g_free(backing_track->priv->pitch);
		backing_track->priv->pitch = g_strdup(pitch);
		setup_source(backing_track);
	}
}

static GstElement* jammo_backing_track_setup_playing_element(JammoPlayingTrack* playing_track, GstBin* bin) {
	JammoBackingTrack* backing_track;
	GstElement* decodebin;
	GstElement* convert;
	
	backing_track = JAMMO_BACKING_TRACK(playing_track);

	g_return_val_if_fail(backing_track->priv->filename, NULL);

	backing_track->priv->source = gst_element_factory_make("filesrc", NULL);
	g_object_set(backing_track->priv->source, "use-mmap", TRUE, NULL);
	setup_source(backing_track);

	jammo_backing_track_get_duration(JAMMO_TRACK(backing_track));

	decodebin = gst_element_factory_make("decodebin", NULL);
	convert = gst_element_factory_make("audioconvert", NULL);
	g_signal_connect(decodebin, "new-decoded-pad", G_CALLBACK(on_new_decoded_pad), convert);

	gst_bin_add_many(bin, backing_track->priv->source, decodebin, convert, NULL);
	gst_element_link(backing_track->priv->source, decodebin);

	return convert;
}

static void jammo_backing_track_finalize(GObject* object) {
	JammoBackingTrack* backing_track;
	
	backing_track = JAMMO_BACKING_TRACK(object);
	g_free(backing_track->priv->filename);

	G_OBJECT_CLASS(jammo_backing_track_parent_class)->finalize(object);
}

static void jammo_backing_track_dispose(GObject* object) {
	G_OBJECT_CLASS(jammo_backing_track_parent_class)->dispose(object);
}

static void jammo_backing_track_class_init(JammoBackingTrackClass* backing_track_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(backing_track_class);
	JammoTrackClass* track_class = JAMMO_TRACK_CLASS(backing_track_class);
	JammoPlayingTrackClass* playing_track_class = JAMMO_PLAYING_TRACK_CLASS(backing_track_class);

	gobject_class->finalize = jammo_backing_track_finalize;
	gobject_class->dispose = jammo_backing_track_dispose;
	gobject_class->set_property = jammo_backing_track_set_property;
	gobject_class->get_property = jammo_backing_track_get_property;

	track_class->get_duration = jammo_backing_track_get_duration;
	track_class->set_tempo = jammo_backing_track_set_tempo;
	track_class->set_pitch = jammo_backing_track_set_pitch;
	playing_track_class->setup_playing_element = jammo_backing_track_setup_playing_element;

	/**
	 * JammoBackingTrack:filename:
	 */
	g_object_class_install_property(gobject_class, PROP_FILENAME,
	                                g_param_spec_string("filename",
	                                "File name",
	                                "The file name of the played audio",
	                                NULL,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoBackingTrack:real-filename:
	 */
	g_object_class_install_property(gobject_class, PROP_REAL_FILENAME,
	                                g_param_spec_string("real-filename",
	                                "Real file name",
	                                "The real file name of the played audio after the parameters have been parsed",
	                                NULL,
	                                G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	g_type_class_add_private(gobject_class, sizeof(JammoBackingTrackPrivate));
}

static void jammo_backing_track_init(JammoBackingTrack* backing_track) {
	backing_track->priv = G_TYPE_INSTANCE_GET_PRIVATE(backing_track, JAMMO_TYPE_BACKING_TRACK, JammoBackingTrackPrivate);
	backing_track->priv->duration = JAMMO_DURATION_INVALID;
}

static void on_new_decoded_pad(GstElement* element, GstPad* pad, gboolean last, gpointer data) {
	GstElement* sink;
	GstPad* sinkpad;
	GstCaps* caps;

	sink = GST_ELEMENT(data);
	sinkpad = gst_element_get_pad(sink, "sink");
	if (!GST_PAD_IS_LINKED(sinkpad)) {
		caps = gst_pad_get_caps(pad);
		if (g_strrstr(gst_structure_get_name(gst_caps_get_structure(caps, 0)), "audio")) {
			gst_pad_link(pad, sinkpad);
		}
		gst_caps_unref(caps);
	}
	g_object_unref(sinkpad);
}

static gboolean preroll_bus_callback(GstBus* bus, GstMessage* message, gpointer data) {
	PrerollData* preroll_data;
	gboolean cleanup;
	GstFormat format;
	gint64 duration;
	
	preroll_data = (PrerollData*)data;
	cleanup = FALSE;
	if (GST_MESSAGE_TYPE(message) == GST_MESSAGE_ASYNC_DONE || GST_MESSAGE_TYPE(message) == GST_MESSAGE_EOS) {
		format = GST_FORMAT_TIME;
		if (gst_element_query_duration(preroll_data->pipeline, &format, &duration)) {
			preroll_data->backing_track->priv->duration = (guint64)duration;
			g_object_notify(G_OBJECT(preroll_data->backing_track), "duration");

			jammo_meam_set_cached_duration(preroll_data->backing_track->priv->real_filename, preroll_data->backing_track->priv->duration);
		}
		cleanup = TRUE;
	} else if (GST_MESSAGE_TYPE(message) == GST_MESSAGE_ERROR) {
		preroll_data->backing_track->priv->duration = 0;
		cleanup = TRUE;
	}

	if (cleanup) {
		g_object_unref(preroll_data->backing_track);
		gst_element_set_state(preroll_data->pipeline, GST_STATE_NULL);
		gst_object_unref(preroll_data->pipeline);
		g_free(preroll_data);
	}

	return !cleanup;
}

static void preroll(JammoBackingTrack* backing_track) {
	PrerollData* preroll_data;
	GstBus* bus;
	GstElement* filesrc;
	GstElement* decodebin;
	GstElement* sink;

	preroll_data = g_malloc0(sizeof(PrerollData));
	preroll_data->backing_track = backing_track;
	g_object_ref(preroll_data->backing_track);
	preroll_data->pipeline = gst_element_factory_make("pipeline", NULL);
	bus = gst_pipeline_get_bus(GST_PIPELINE(preroll_data->pipeline));
	gst_bus_add_watch(bus, preroll_bus_callback, preroll_data);
	gst_object_unref(bus);

	filesrc = gst_element_factory_make("filesrc", NULL);
	g_object_set(filesrc, "location", backing_track->priv->real_filename, NULL);
	decodebin = gst_element_factory_make("decodebin", NULL);
	sink = gst_element_factory_make("fakesink", NULL);
	gst_bin_add_many(GST_BIN(preroll_data->pipeline), filesrc, decodebin, sink, NULL);
	gst_element_link(filesrc, decodebin);
	g_signal_connect(decodebin, "new-decoded-pad", G_CALLBACK(on_new_decoded_pad), sink);

	gst_element_set_state(preroll_data->pipeline, GST_STATE_PAUSED); 
}

static gchar* str_replace(gchar* string, gsize position, gsize length, const gchar* replacement, gchar** replacement_end_return) {
	gsize string_length;
	gsize replacement_length;
	gchar* s;
	
	string_length = strlen(string);
	replacement_length = strlen(replacement);
	s = (gchar*)g_malloc(string_length - length + replacement_length + 1);
	g_memmove(s, string, position);
	g_memmove(s + position, replacement, replacement_length);
	g_memmove(s + position + replacement_length, string + position + length, string_length - position - length + 1);

	if (replacement_end_return) {
		*replacement_end_return = s + position + replacement_length;
	}

	g_free(string);

	return s;
}

static void setup_source(JammoBackingTrack* backing_track) {
	gboolean parameters_ok = TRUE;
	gchar* filename;
	gchar* s;
	gchar* tempo_string;
	guint64 old_duration;
	
	if (backing_track->priv->source) {
		filename = s = g_strdup(backing_track->priv->filename);
		while (parameters_ok &&(s = strchr(s, '$'))) {
			switch (s[1]) {
				case '$':
					filename = str_replace(filename, s - filename, 2, "$", &s);
					break;
				case 't':
					tempo_string = g_strdup_printf("%u", backing_track->priv->tempo);
					filename = str_replace(filename, s - filename, 2, tempo_string, &s);
					g_free(tempo_string);
					break;
				case 'p':
					if (backing_track->priv->pitch) {
						filename = str_replace(filename, s - filename, 2, backing_track->priv->pitch, &s);
					} else {
						parameters_ok = FALSE;
					}
					break;
				default:
					g_warning("Unexpected parameter in the backing track filename: '$%c', skipped.", s[1]);
					s += 2;
					break;
			}
		}

		if (parameters_ok && g_strcmp0(backing_track->priv->real_filename, filename)) {
			g_free(backing_track->priv->real_filename);
			//backing_track->priv->real_filename = filename;

			//printf("file: %s\n",filename);
			gchar* filename_with_path;
			filename_with_path=tangle_lookup_filename(filename);
			backing_track->priv->real_filename = g_strdup(filename_with_path);
			//printf("file_with_path: %s\n",filename_with_path);
			g_free(filename_with_path);


			g_object_set(backing_track->priv->source, "location", backing_track->priv->real_filename, NULL);
			
			old_duration = backing_track->priv->duration;
			backing_track->priv->duration = jammo_meam_get_cached_duration(backing_track->priv->real_filename);
			if (old_duration != backing_track->priv->duration) {
				g_object_notify(G_OBJECT(backing_track), "duration");
			}
		} else {
			g_free(filename);
		}
	}
}


