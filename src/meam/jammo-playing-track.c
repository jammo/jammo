/*
 * jammo-playing-track.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "jammo-playing-track.h"

G_DEFINE_ABSTRACT_TYPE(JammoPlayingTrack, jammo_playing_track, JAMMO_TYPE_TRACK);

enum {
	PROP_0,
	PROP_MUTED,
	PROP_VOLUME,
	PROP_PANNING,
	PROP_ECHO_DELAY,
	PROP_ECHO_INTENSITY,
	PROP_ECHO_FEEDBACK,
	PROP_VOLUME_ENABLED,
	PROP_PANNING_ENABLED,
	PROP_ECHO_ENABLED
};

struct _JammoPlayingTrackPrivate {
	gboolean muted;
	gfloat volume;
	gfloat panning;
	guint64 echo_delay;
	gfloat echo_intensity;
	gfloat echo_feedback;

	GstBin* bin;
	GstElement* source;
	GstElement* audioecho;
	GstElement* volume_element;
	GstElement* audiopanorama;
	GstElement* sink;

	GstPad* audioecho_pad;
	GstPad* volume_element_pad;
	GstPad* audiopanorama_pad;
	GstPad* sink_pad;
	
	guint volume_enabled : 1;
	guint panning_enabled : 1;
	guint echo_enabled : 1;
};

static void setup_pipeline(JammoPlayingTrack* playing_track);
static void create_element(JammoPlayingTrack* playing_track, const gchar* element_name, GstElement** element_pointer);
static void destroy_element(JammoPlayingTrack* playing_track, GstElement** element_pointer);

/**
 * jammo_playing_track_get_muted:
 * @playing_track: a #JammoPlayingTrack
 *
 * Returns whether the track is muted or audible.
 *
 * Return value: whether the track is muted or not
 */
gboolean jammo_playing_track_get_muted(JammoPlayingTrack* playing_track) {
	g_return_val_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track), FALSE);

	return playing_track->priv->muted;
}

/**
 * jammo_playing_track_set_muted:
 * @playing_track: a #JammoPlayingTrack
 * @muted: whether the track is muted or not
 *
 * Sets whether the track is muted or audible.
 */
void jammo_playing_track_set_muted(JammoPlayingTrack* playing_track, gboolean muted) {
	g_return_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track));

	if (playing_track->priv->muted != muted) {
		playing_track->priv->muted = muted;
		jammo_playing_track_set_volume_enabled(playing_track, playing_track->priv->muted || playing_track->priv->volume != 1.0);
		if (playing_track->priv->volume_element) {
			g_object_set(playing_track->priv->volume_element, "mute", playing_track->priv->muted, NULL);
		}
		g_object_notify(G_OBJECT(playing_track), "muted");
	}
}

/**
 * jammo_playing_track_get_volume:
 * @playing_track: a #JammoPlayingTrack
 *
 * Returns the individual volume of the track.
 *
 * Return value: a volume between 0.0 - 1.0
 */
gfloat jammo_playing_track_get_volume(JammoPlayingTrack* playing_track) {
	g_return_val_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track), 0.0);
	
	return playing_track->priv->volume;
}

/**
 * jammo_playing_track_set_volume:
 * @playing_track: a #JammoPlayingTrack
 * @volume: volume for the track: 0.0(min) - 1.0(max)
 *
 * Sets the individual volume of the track.
 */
void jammo_playing_track_set_volume(JammoPlayingTrack* playing_track, gfloat volume) {
	g_return_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track));

	if (playing_track->priv->volume != volume) {
		playing_track->priv->volume = volume;
		jammo_playing_track_set_volume_enabled(playing_track, playing_track->priv->muted || playing_track->priv->volume != 1.0);
		if (playing_track->priv->volume_element) {
			g_object_set(playing_track->priv->volume_element, "volume", playing_track->priv->volume, NULL);
		}
		g_object_notify(G_OBJECT(playing_track), "volume");
	}
}

/**
 * jammo_playing_track_get_panning:
 * @playing_track: a #JammoPlayingTrack
 *
 * Returns the audio position in stereo panorama (left - right).
 *
 * Return value: a volume between -1.0 - 1.0
 */
gfloat jammo_playing_track_get_panning(JammoPlayingTrack* playing_track) {
	g_return_val_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track), 0.0);

	return playing_track->priv->panning;
}

/**
 * jammo_playing_track_set_panning:
 * @playing_track: a #JammoPlayingTrack
 * @panning: the position in stereo panorama: -1.0 (left) - 1.0 (right)
 *
 * Sets the audio position in stereo panorama (left, center, right).
 */
void jammo_playing_track_set_panning(JammoPlayingTrack* playing_track, gfloat panning) {
	g_return_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track));

	if (playing_track->priv->panning != panning) {
		playing_track->priv->panning = panning;
		jammo_playing_track_set_panning_enabled(playing_track, playing_track->priv->panning != 0.0);
		if (playing_track->priv->audiopanorama) {
			g_object_set(playing_track->priv->audiopanorama, "panorama", playing_track->priv->panning, NULL);
		}
		g_object_notify(G_OBJECT(playing_track), "panning");
	}
}

guint64 jammo_playing_track_get_echo_delay(JammoPlayingTrack* playing_track) {
	g_return_val_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track), 0);
	
	return playing_track->priv->echo_delay;
}

void jammo_playing_track_set_echo_delay(JammoPlayingTrack* playing_track, guint64 echo_delay) {
	g_return_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track));

	if (playing_track->priv->echo_delay != echo_delay) {
		playing_track->priv->echo_delay = echo_delay;
		if (playing_track->priv->audioecho) {
			g_object_set(playing_track->priv->audioecho, "delay", playing_track->priv->echo_delay, NULL);
		}
		g_object_notify(G_OBJECT(playing_track), "echo-delay");
	}
}

gfloat jammo_playing_track_get_echo_intensity(JammoPlayingTrack* playing_track) {
	g_return_val_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track), 0.0);

	return playing_track->priv->echo_intensity;
}

void jammo_playing_track_set_echo_intensity(JammoPlayingTrack* playing_track, gfloat echo_intensity) {
	g_return_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track));

	if (playing_track->priv->echo_intensity != echo_intensity) {
		playing_track->priv->echo_intensity = echo_intensity;
		jammo_playing_track_set_echo_enabled(playing_track, playing_track->priv->echo_intensity != 0.0 && playing_track->priv->echo_feedback != 0.0);
		if (playing_track->priv->audioecho) {
			g_object_set(playing_track->priv->audioecho,"intensity", playing_track->priv->echo_intensity, NULL);
		}
		g_object_notify(G_OBJECT(playing_track), "echo-intensity");
	}
}

gfloat jammo_playing_track_get_echo_feedback(JammoPlayingTrack* playing_track) {
	g_return_val_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track), 0.0);
	
	return playing_track->priv->echo_feedback;
}

void jammo_playing_track_set_echo_feedback(JammoPlayingTrack* playing_track, gfloat echo_feedback) {
	g_return_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track));

	if (playing_track->priv->echo_feedback != echo_feedback) {
		playing_track->priv->echo_feedback = echo_feedback;
		jammo_playing_track_set_echo_enabled(playing_track, playing_track->priv->echo_intensity != 0.0 && playing_track->priv->echo_feedback != 0.0);
		if (playing_track->priv->audioecho) {
			g_object_set(playing_track->priv->audioecho, "feedback", playing_track->priv->echo_feedback, NULL);
		}
		g_object_notify(G_OBJECT(playing_track), "echo-feedback");
	}
}

gboolean jammo_playing_track_get_volume_enabled(JammoPlayingTrack* playing_track) {
	g_return_val_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track), FALSE);

	return playing_track->priv->volume_enabled;
}

void jammo_playing_track_set_volume_enabled(JammoPlayingTrack* playing_track, gboolean is_enabled) {
	g_return_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track));

	if (playing_track->priv->volume_enabled != is_enabled) {
		playing_track->priv->volume_enabled = is_enabled;
		if (!playing_track->priv->source || GST_STATE(playing_track->priv->source) != GST_STATE_PLAYING) {
			if (playing_track->priv->volume_enabled) {
				create_element(playing_track, "volume", &playing_track->priv->volume_element);
				g_object_set(playing_track->priv->volume_element, "mute", playing_track->priv->muted, "volume", playing_track->priv->volume, NULL);
			} else {
				destroy_element(playing_track, &playing_track->priv->volume_element);
			}
		}
		g_object_notify(G_OBJECT(playing_track), "volume-enabled");	
	}
}

gboolean jammo_playing_track_get_panning_enabled(JammoPlayingTrack* playing_track) {
	g_return_val_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track), FALSE);

	return playing_track->priv->panning_enabled;
}

void jammo_playing_track_set_panning_enabled(JammoPlayingTrack* playing_track, gboolean is_enabled) {
	g_return_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track));

	if (playing_track->priv->panning_enabled != is_enabled) {
		playing_track->priv->panning_enabled = is_enabled;
		if (!playing_track->priv->source || GST_STATE(playing_track->priv->source) != GST_STATE_PLAYING) {
			if (playing_track->priv->panning_enabled) {
				create_element(playing_track, "audiopanorama", &playing_track->priv->audiopanorama);
				g_object_set(playing_track->priv->audiopanorama, "panorama", playing_track->priv->panning, NULL);
			} else {
				destroy_element(playing_track, &playing_track->priv->audiopanorama);
			}
		}
		g_object_notify(G_OBJECT(playing_track), "panning-enabled");	
	}
}

gboolean jammo_playing_track_get_echo_enabled(JammoPlayingTrack* playing_track) {
	g_return_val_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track), FALSE);

	return playing_track->priv->echo_enabled;
}

void jammo_playing_track_set_echo_enabled(JammoPlayingTrack* playing_track, gboolean is_enabled) {
	g_return_if_fail(JAMMO_IS_PLAYING_TRACK(playing_track));

	if (playing_track->priv->echo_enabled != is_enabled) {
		playing_track->priv->echo_enabled = is_enabled;
		if (!playing_track->priv->source || GST_STATE(playing_track->priv->source) != GST_STATE_PLAYING) {
			if (playing_track->priv->echo_enabled) {
				create_element(playing_track, "audioecho", &playing_track->priv->audioecho);
				g_object_set(playing_track->priv->audioecho, "delay", playing_track->priv->echo_delay, "intensity", playing_track->priv->echo_intensity, "feedback", playing_track->priv->echo_feedback, NULL);
			} else {
				destroy_element(playing_track, &playing_track->priv->audioecho);
			}
		}
		g_object_notify(G_OBJECT(playing_track), "echo-enabled");	
	}
}


static gboolean jammo_playing_track_setup_element(JammoTrack* track, GstPipeline* pipeline, GstBin* bin, GstElement* sink_element) {
	JammoPlayingTrack* playing_track;
	
	playing_track = JAMMO_PLAYING_TRACK(track);

	g_return_val_if_fail(playing_track->priv->bin == NULL, FALSE);
	
	playing_track->priv->bin = bin;
	playing_track->priv->sink = sink_element;
	g_object_ref(playing_track->priv->sink);

	playing_track->priv->source = JAMMO_PLAYING_TRACK_GET_CLASS(playing_track)->setup_playing_element(playing_track, bin);
	g_object_ref(playing_track->priv->source);
	
	if (playing_track->priv->echo_enabled) {
		create_element(playing_track, "audioecho", &playing_track->priv->audioecho);
		g_object_set(playing_track->priv->audioecho, "delay", playing_track->priv->echo_delay, "intensity", playing_track->priv->echo_intensity, "feedback", playing_track->priv->echo_feedback, NULL);
	}
	if (playing_track->priv->volume_enabled) {
		create_element(playing_track, "volume", &playing_track->priv->volume_element);
		g_object_set(playing_track->priv->volume_element, "mute", playing_track->priv->muted, "volume", playing_track->priv->volume, NULL);
	}
	if (playing_track->priv->panning_enabled) {
		create_element(playing_track, "audiopanorama", &playing_track->priv->audiopanorama);
		g_object_set(playing_track->priv->audiopanorama, "panorama", playing_track->priv->panning, NULL);
	}
	
	setup_pipeline(playing_track);
	
	return TRUE;
}

static void jammo_playing_track_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoPlayingTrack* playing_track;
	
	playing_track = JAMMO_PLAYING_TRACK(object);

	switch (prop_id) {
		case PROP_MUTED:
			jammo_playing_track_set_muted(playing_track, g_value_get_boolean(value));
			break;
		case PROP_VOLUME:
			jammo_playing_track_set_volume(playing_track, g_value_get_float(value));
			break;
		case PROP_PANNING:
			jammo_playing_track_set_panning(playing_track, g_value_get_float(value));
			break;
		case PROP_ECHO_DELAY:
			jammo_playing_track_set_echo_delay(playing_track, g_value_get_uint64(value));
			break;
		case PROP_ECHO_INTENSITY:
			jammo_playing_track_set_echo_intensity(playing_track, g_value_get_float(value));
			break;
		case PROP_ECHO_FEEDBACK:
			jammo_playing_track_set_echo_feedback(playing_track, g_value_get_float(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_playing_track_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoPlayingTrack* playing_track;

	playing_track = JAMMO_PLAYING_TRACK(object);

        switch (prop_id) {
		case PROP_MUTED:
			g_value_set_boolean(value, playing_track->priv->muted);
			break;
		case PROP_VOLUME:
			g_value_set_float(value, playing_track->priv->volume);
			break;
		case PROP_PANNING:
			g_value_set_float(value, playing_track->priv->panning);
			break;
		case PROP_ECHO_DELAY:
			g_value_set_uint64(value, playing_track->priv->echo_delay);
			break;
		case PROP_ECHO_INTENSITY:
			g_value_set_float(value, playing_track->priv->echo_intensity);
			break;
		case PROP_ECHO_FEEDBACK:
			g_value_set_float(value, playing_track->priv->echo_feedback);
			break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static void jammo_playing_track_finalize(GObject* object) {
	G_OBJECT_CLASS(jammo_playing_track_parent_class)->finalize(object);
}

static void jammo_playing_track_dispose(GObject* object) {
        JammoPlayingTrack* playing_track;

	playing_track = JAMMO_PLAYING_TRACK(object);

	if (playing_track->priv->sink) {
		g_object_unref(playing_track->priv->sink);
		playing_track->priv->sink = NULL;
	}
	if (playing_track->priv->source) {
		g_object_unref(playing_track->priv->source);
		playing_track->priv->source = NULL;
	}
	if (playing_track->priv->audioecho_pad) {
		g_object_unref(playing_track->priv->audioecho_pad);
		playing_track->priv->audioecho_pad = NULL;
	}
	if (playing_track->priv->volume_element_pad) {
		g_object_unref(playing_track->priv->volume_element_pad);
		playing_track->priv->volume_element_pad = NULL;
	}
	if (playing_track->priv->audiopanorama_pad) {
		g_object_unref(playing_track->priv->audiopanorama_pad);
		playing_track->priv->audiopanorama_pad = NULL;
	}
	if (playing_track->priv->sink_pad) {
		g_object_unref(playing_track->priv->sink_pad);
		playing_track->priv->sink_pad = NULL;
	}

	G_OBJECT_CLASS(jammo_playing_track_parent_class)->dispose(object);
}
	
static void jammo_playing_track_class_init(JammoPlayingTrackClass* playing_track_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(playing_track_class);
	JammoTrackClass* track_class = JAMMO_TRACK_CLASS(playing_track_class);

	gobject_class->finalize = jammo_playing_track_finalize;
	gobject_class->dispose = jammo_playing_track_dispose;
	gobject_class->set_property = jammo_playing_track_set_property;
	gobject_class->get_property = jammo_playing_track_get_property;

	track_class->setup_element = jammo_playing_track_setup_element;

	/**
	 * JammoPlayingTrack:muted:
	 *
	 * Whether the track is muted or audible.
	 */
	g_object_class_install_property(gobject_class, PROP_MUTED,
	                                g_param_spec_boolean("muted",
	                                "Muted",
	                                "Whether the track is muted or audible",
	                                FALSE,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoPlayingTrack:volume:
	 *
	 * The volume of the track.
	 */
	g_object_class_install_property(gobject_class, PROP_VOLUME,
	                                g_param_spec_float("volume",
	                                "Volume",
	                                "The volume of the track",
	                                0.0, 1.0, 1.0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoPlayingTrack:panning:
	 *
	 * The audio position in stereo panorama.
	 */
	g_object_class_install_property(gobject_class, PROP_PANNING,
	                                g_param_spec_float("panning",
	                                "Panning",
	                                "The audio position in stereo panorama",
	                                -1.0, 1.0, 0.0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoPlayingTrack:echo-delay:
	 *
	 * Delay of the echo in nanoseconds.
	 */
	g_object_class_install_property(gobject_class, PROP_ECHO_DELAY,
	                                g_param_spec_uint64("echo-delay",
	                                "Echo delay",
	                                "The delay of the echo in nanoseconds",
	                                1, G_MAXUINT64, 1,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoPlayingTrack:echo-intensity:
	 *
	 * The intensity of the echo.
	 */
	g_object_class_install_property(gobject_class, PROP_ECHO_INTENSITY,
	                                g_param_spec_float("echo-intensity",
	                                "Echo intensity",
	                                "The intensity of the echo",
	                                0.0, 1.0, 0.0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoPlayingTrack:echo-feedback:
	 *
	 * Amount of feedback in percentage.
	 */
	g_object_class_install_property(gobject_class, PROP_ECHO_FEEDBACK,
	                                g_param_spec_float("echo-feedback",
	                                "Echo feedback",
	                                "Amount of feedback in percentage",
	                                0.0, 1.0, 0.0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoPlayingTrack:volume-enabled:
	 *
	 * Whether volume is enabled or not. Chaning this value does not really have any effect when the sequencer is playing.
	 */
	g_object_class_install_property(gobject_class, PROP_VOLUME_ENABLED,
	                                g_param_spec_boolean("volume-enabled",
	                                "Volume enabled",
	                                "Whether volume is enabled or not",
	                                FALSE,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoPlayingTrack:panning-enabled:
	 *
	 * Whether panning is enabled or not. Chaning this value does not really have any effect when the sequencer is playing.
	 */
	g_object_class_install_property(gobject_class, PROP_PANNING_ENABLED,
	                                g_param_spec_boolean("panning-enabled",
	                                "Panning enabled",
	                                "Whether panning is enabled or not",
	                                FALSE,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoPlayingTrack:echo-enabled:
	 *
	 * Whether echo is enabled or not. Chaning this value does not really have any effect when the sequencer is playing.
	 */
	g_object_class_install_property(gobject_class, PROP_ECHO_ENABLED,
	                                g_param_spec_boolean("echo-enabled",
	                                "Echo enabled",
	                                "Whether echo is enabled or not",
	                                FALSE,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	g_type_class_add_private (gobject_class, sizeof (JammoPlayingTrackPrivate));
}

static void jammo_playing_track_init(JammoPlayingTrack* playing_track) {
	playing_track->priv = G_TYPE_INSTANCE_GET_PRIVATE(playing_track, JAMMO_TYPE_PLAYING_TRACK, JammoPlayingTrackPrivate);
	playing_track->priv->volume = 1.0;
	playing_track->priv->echo_delay = 1;
}

static GstElement* link_if_sink_exists(GstElement* source_element, GstElement* sink_element, GstPad** sink_pad_pointer) {
	GstElement* return_element = source_element;
	GstPad* source_pad;
	GstPad* peer_pad;
	gboolean link = TRUE;
	
	if (sink_element) {
		source_pad = gst_element_get_static_pad(source_element, "src");
		
		if (source_pad && gst_pad_is_linked(source_pad)) {
			if ((peer_pad = gst_pad_get_peer(source_pad)) == *sink_pad_pointer) {
				gst_pad_set_caps(source_pad, NULL);
				gst_pad_set_caps(peer_pad, NULL);
				link = FALSE;
			} else {
				gst_pad_unlink(source_pad, peer_pad);
			}
			g_object_unref(peer_pad);
		}
		if (link && *sink_pad_pointer && gst_pad_is_linked(*sink_pad_pointer)) {
			peer_pad = gst_pad_get_peer(*sink_pad_pointer);
			gst_pad_unlink(peer_pad, *sink_pad_pointer);
			g_object_unref(peer_pad);
		}
		if (link && !gst_element_link(source_element, sink_element)) {
			g_critical("Error when linking elements.");
		} else {
			if (*sink_pad_pointer) {
				g_object_unref(*sink_pad_pointer);
			}
			*sink_pad_pointer = gst_pad_get_peer(source_pad);
		}

		if (source_pad) {
			g_object_unref(G_OBJECT(source_pad));
		}
		
		return_element = sink_element;
	} else if (*sink_pad_pointer) {
		g_object_unref(*sink_pad_pointer);
		*sink_pad_pointer = NULL;
	}

	return return_element;
}

static void setup_pipeline(JammoPlayingTrack* playing_track) {
	GstElement* element;
	
	if (playing_track->priv->bin) {
		element = link_if_sink_exists(playing_track->priv->source, playing_track->priv->audioecho, &playing_track->priv->audioecho_pad);
		element = link_if_sink_exists(element, playing_track->priv->volume_element, &playing_track->priv->volume_element_pad);
		element = link_if_sink_exists(element, playing_track->priv->audiopanorama, &playing_track->priv->audiopanorama_pad);
		link_if_sink_exists(element, playing_track->priv->sink, &playing_track->priv->sink_pad);
	}
}

static void create_element(JammoPlayingTrack* playing_track, const gchar* element_name, GstElement** element_pointer) {
	if (playing_track->priv->bin) {
		if (!(*element_pointer = gst_element_factory_make(element_name, NULL))) {
			g_critical("Could not create element '%s'.", element_name);
		} else {
			gst_bin_add(playing_track->priv->bin, *element_pointer);
			setup_pipeline(playing_track);
		}
	}	
}

static void destroy_element(JammoPlayingTrack* playing_track, GstElement** element_pointer) {
	gst_bin_remove(playing_track->priv->bin, *element_pointer);
	*element_pointer = NULL;
	setup_pipeline(playing_track);
}
