/*
 * jammo-sequencer.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */
 
#include "jammo-sequencer.h"
#include "jammo-meam.h"
#include "jammo-meam-private.h"
#include "jammo-pipeline.h"
#include "../cem/cem.h"

#include <clutter/clutter.h>

#define BUFFER_TIME 10000


/**
 * SECTION:jammo-sequencer
 * @Short_description: A multitrack sequencer
 * @Title: JammoSequencer
 *
 * A #JammoSequencer consists of multiple tracks that
 * each can play own sound simultaneously.
 *
 * A #JammoSequencer must be unreferenced with g_object_unref()
 * after usage. It automatically frees
 * associated #JammoTrack objects.
 */

static void clutter_scriptable_iface_init(ClutterScriptableIface* iface);

G_DEFINE_TYPE_WITH_CODE(JammoSequencer, jammo_sequencer, G_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(CLUTTER_TYPE_SCRIPTABLE, clutter_scriptable_iface_init););

enum {
	PROP_0,
	PROP_READY,
	PROP_TEMPO,
	PROP_PITCH,
	PROP_OUTPUT_FILENAME,
	PROP_PLAY,
	PROP_TIME_SIGNATURE_BEATS,
	PROP_TIME_SIGNATURE_NOTE_VALUE
};

enum {
	STARTED,
	STOPPED,
	PAUSED,
	LAST_SIGNAL
};

struct _JammoSequencerPrivate {
	guint tempo;
	gchar* pitch;
	guint time_signature_beats;
	guint time_signature_note_value;

	GstElement* pipeline;
	GstElement* bin;
	GstElement* adder;
	GstElement* queue;
	GstElement* audiosink;
	GstElement* audioconvert;
	GstElement* vorbisenc;
	GstElement* oggmux;
	GstElement* filesink;
	GList* tracks;
	guint64 duration;
	gchar* output_filename;
	
	guint ready : 1;
	guint playing : 1;
};

static gboolean on_bus_message(GstBus* bus, GstMessage* message, gpointer data);
static void on_duration_notify(GObject* object, GParamSpec* param_spec, gpointer user_data);
static GstElement* create_element(const gchar* name);
static gboolean setup_audiosink(JammoSequencer* sequencer);
static gboolean setup_filesink(JammoSequencer* sequencer, const gchar* filename);
static void start_playing(JammoSequencer* sequencer);

static ClutterScriptableIface* parent_scriptable_iface = NULL;
static guint signals[LAST_SIGNAL] = { 0 };

// quality for ogg file created in file mode
#define OGGQUALITY 0.5

/**
 * jammo_sequencer_new:
 *
 * Creates a new sequencer.
 *
 * Return value: a #JammoSequencer
 */
JammoSequencer* jammo_sequencer_new() {

	return JAMMO_SEQUENCER(g_object_new(JAMMO_TYPE_SEQUENCER, NULL));
}

/**
 * jammo_sequencer_is_ready:
 * @sequencer: a #JammoSequencer
 *
 * Tells whether the sequencer is ready to play immediately
 * or not. A sequencer may not be ready if, for example,
 * some track or sample is still loading content or
 * calculating the duration.
 *
 * Return value: TRUE if the sequencer is ready, FALSE otherwise.
 */
gboolean jammo_sequencer_is_ready(JammoSequencer* sequencer) {
	g_return_val_if_fail(JAMMO_IS_SEQUENCER(sequencer), FALSE);
	
	return sequencer->priv->ready;
}

/**
 * jammo_sequencer_get_tempo:
 * @sequencer: a #JammoSequencer
 *
 * Returns the tempo of the sequencer.
 *
 * Return value: tempo in bpm (beats per minute).
 */
guint jammo_sequencer_get_tempo(JammoSequencer* sequencer) {
	g_return_val_if_fail(JAMMO_IS_SEQUENCER(sequencer), 0);

	return sequencer->priv->tempo;
}

/**
 * jammo_sequencer_set_tempo:
 * @sequencer: a #JammoSequencer
 * @tempo: tempo in bpm (beats per minute)
 *
 * Sets the tempo of the sequencer.
 */
void jammo_sequencer_set_tempo(JammoSequencer* sequencer, guint tempo) {
	GList* list;
	JammoTrack* track;

	g_return_if_fail(JAMMO_IS_SEQUENCER(sequencer));

	if (sequencer->priv->tempo != tempo) {
		sequencer->priv->tempo = tempo;
		
		for (list = sequencer->priv->tracks; list; list = list->next) {
			track = JAMMO_TRACK(list->data);
			JAMMO_TRACK_GET_CLASS(track)->set_tempo(track, tempo);
		}
		
		g_object_notify(G_OBJECT(sequencer), "tempo");
		//Force calculating of duration
		sequencer->priv->duration = 0;
	}
}

/**
 * jammo_sequencer_get_pitch:
 * @sequencer: a #JammoSequencer
 *
 * Returns the pitch of the sequencer.
 *
 * Return value: pitch as a note (string).
 */
const gchar* jammo_sequencer_get_pitch(JammoSequencer* sequencer) {
	g_return_val_if_fail(JAMMO_IS_SEQUENCER(sequencer), NULL);

	return sequencer->priv->pitch;
}

/**
 * jammo_sequencer_set_pitch:
 * @sequencer: a #JammoSequencer
 * @pitch: pitch as a note (string)
 *
 * Sets the pitch of the sequencer.
 */
void jammo_sequencer_set_pitch(JammoSequencer* sequencer, const gchar* pitch) {
	GList* list;
	JammoTrack* track;
	
	g_return_if_fail(JAMMO_IS_SEQUENCER(sequencer));

	if (g_strcmp0(sequencer->priv->pitch, pitch)) {
		g_free(sequencer->priv->pitch);
		sequencer->priv->pitch = g_strdup(pitch);
		
		for (list = sequencer->priv->tracks; list; list = list->next) {
			track = JAMMO_TRACK(list->data);
			JAMMO_TRACK_GET_CLASS(track)->set_pitch(track, pitch);
		}
		
		g_object_notify(G_OBJECT(sequencer), "pitch");
	}
}

/**
 * jammo_sequencer_get_time_signature_beats:
 * @sequencer: a #JammoSequencer
 *
 * Returns the number of beats in a bar.
 *
 * Return value: the number of beats in a bar.
 */
guint jammo_sequencer_get_time_signature_beats(JammoSequencer* sequencer) {
	g_return_val_if_fail(JAMMO_IS_SEQUENCER(sequencer), 0);

	return sequencer->priv->time_signature_beats;
}

/**
 * jammo_sequencer_get_time_signature_note_value:
 * @sequencer: a #JammoSequencer
 *
 * Returns the note value of beats in a bar.
 *
 * Return value: the note value of beats in a bar.
 */
guint jammo_sequencer_get_time_signature_note_value(JammoSequencer* sequencer) {
	g_return_val_if_fail(JAMMO_IS_SEQUENCER(sequencer), 0);

	return sequencer->priv->time_signature_note_value;
}

/**
 * jammo_sequencer_set_time_signature:
 * @sequencer: a #JammoSequencer
 * @time_signature_beats: the number of beats in a bar (uint)
 * @time_signature_note_value: the note value of beats in a bar (uint)
 *
 * Sets the time signature of the sequencer.
 */
void jammo_sequencer_set_time_signature(JammoSequencer * sequencer, guint time_signature_beats, guint time_signature_note_value) {
	GList* list;
	JammoTrack* track;
	gboolean changed=FALSE;

	g_return_if_fail(JAMMO_IS_SEQUENCER(sequencer));

	if (sequencer->priv->time_signature_beats != time_signature_beats) {
		sequencer->priv->time_signature_beats = time_signature_beats;
		changed=TRUE;
	}
	if (sequencer->priv->time_signature_note_value != time_signature_note_value) {
		sequencer->priv->time_signature_note_value = time_signature_note_value;
		changed=TRUE;
	}

	if (changed) {
		for (list = sequencer->priv->tracks; list; list = list->next) {
			track = JAMMO_TRACK(list->data);
			JAMMO_TRACK_GET_CLASS(track)->set_time_signature(track, time_signature_beats, time_signature_note_value);
		}
		g_object_notify(G_OBJECT(sequencer), "time-signature-beats");
		g_object_notify(G_OBJECT(sequencer), "time-signature-note-value");
	}
}

/**
 * jammo_sequencer_play:
 * @sequencer: a #JammoSequencer
 *
 * Starts playing all associated tracks simultaneously.
 */
void jammo_sequencer_play(JammoSequencer* sequencer) {
	g_return_if_fail(JAMMO_IS_SEQUENCER(sequencer));

	if (sequencer->priv->ready) {
		start_playing(sequencer);
	}
	sequencer->priv->playing = TRUE;
	g_object_notify(G_OBJECT(sequencer), "play");
}

/**
 * jammo_sequencer_stop:
 * @sequencer: a #JammoSequencer
 *
 * Stops playing started with #jammo_sequencer_play(). The
 * next #jammo_sequencer_play() call starts from the beginning.
 */
void jammo_sequencer_stop(JammoSequencer* sequencer) {
	g_return_if_fail(JAMMO_IS_SEQUENCER(sequencer));

	gst_element_set_state(sequencer->priv->pipeline, GST_STATE_NULL);
	g_signal_emit(sequencer, signals[STOPPED], 0);
	sequencer->priv->playing = FALSE;
	g_object_notify(G_OBJECT(sequencer), "play");
}

/**
 * jammo_sequencer_stop:
 * @sequencer: a #JammoSequencer
 *
 * Pauses playing started with #jammo_sequencer_play().
 * The next #jammo_sequencer_play() call start from the
 * position where the sequencer was paused.
 */
void jammo_sequencer_pause(JammoSequencer* sequencer) {
	g_return_if_fail(JAMMO_IS_SEQUENCER(sequencer));

	gst_element_set_state(sequencer->priv->pipeline, GST_STATE_PAUSED);
	g_signal_emit(sequencer, signals[PAUSED], 0);
	sequencer->priv->playing = FALSE;
	g_object_notify(G_OBJECT(sequencer), "play");
}

/**
 * jammo_sequencer_get_track_count:
 * @sequencer: a #JammoSequencer
 *
 * Return how many #JammoTracks are associated with the
 * sequencer.
 *
 * Return value: track count
 */
guint jammo_sequencer_get_track_count(JammoSequencer* sequencer) {
	g_return_val_if_fail(JAMMO_IS_SEQUENCER(sequencer), 0);

	return g_list_length(sequencer->priv->tracks);
}

/**
 * jammo_sequencer_get_track:
 * @sequencer: a #JammoSequencer
 * @index: the index of a requested track
 *
 * Returns a #JammoTrack by its index number.
 * The index must be lower than track count
 * (see #jammo_sequencer_get_track_count()).
 *
 * Return value: a #JammoTrack
 */
JammoTrack* jammo_sequencer_get_track(JammoSequencer* sequencer, guint index) {
	JammoTrack* track = NULL;
	GList* list;
	
	g_return_val_if_fail(JAMMO_IS_SEQUENCER(sequencer), NULL);

	if ((list = g_list_nth(sequencer->priv->tracks, index))) {
		track = JAMMO_TRACK(list->data);
	}
	
	return track;
}

/**
 * jammo_sequencer_add_track:
 * @sequencer: a #JammoSequencer
 * @track: a #JammoTrack
 *
 * Associates the given track to the sequencer.
 *
 * This function sinks the floating reference of
 * a #JammoTrack.
 */
void jammo_sequencer_add_track(JammoSequencer* sequencer, JammoTrack* track) {
	GstElement* element;
	
	g_return_if_fail(JAMMO_IS_SEQUENCER(sequencer));

	if (g_list_find(sequencer->priv->tracks, track)) {
		cem_add_to_log("A track is already added into a sequencer.",J_LOG_WARN);
	} else {
		sequencer->priv->tracks = g_list_prepend(sequencer->priv->tracks, g_object_ref_sink(track));
		_jammo_track_set_tempo(track, sequencer->priv->tempo);
		_jammo_track_set_pitch(track, sequencer->priv->pitch);
		_jammo_track_set_time_signature(track, sequencer->priv->time_signature_beats, sequencer->priv->time_signature_note_value);

		element = gst_element_factory_make("audioconvert", NULL);
		gst_bin_add(GST_BIN(sequencer->priv->bin), element);
		if (_jammo_track_setup_element(track, GST_PIPELINE(sequencer->priv->pipeline), GST_BIN(sequencer->priv->bin), element)) {
			gst_element_link(element, sequencer->priv->adder);
		} else {
			gst_bin_remove(GST_BIN(sequencer->priv->bin), element);
			
			/* Dirty fix: We know that the track is JammoRecordingTrack. Singing game requires
			 * addition buffers in order to work decently in Nokia N900, which has latency isses.
			 */
			cem_add_to_log("Added some buffers into pulse sink to help recording.", J_LOG_DEBUG);
			g_object_set(G_OBJECT(sequencer->priv->audiosink), "buffer-time", (gint64)80000, NULL);
		}

		sequencer->priv->duration = 0;
		g_signal_connect(track, "notify::duration", G_CALLBACK(on_duration_notify), sequencer);

		if (sequencer->priv->ready &&
		    jammo_track_get_duration(track) == JAMMO_DURATION_INVALID) {
			if (sequencer->priv->playing) {
				gst_element_set_state(sequencer->priv->pipeline, GST_STATE_NULL);
			}
			sequencer->priv->ready = FALSE;
			g_object_notify(G_OBJECT(sequencer), "ready");
		}
	}
}

/**
 * jammo_sequencer_get_duration:
 * @sequencer: a #JammoSequencer
 *
 * Calculates the total duration of the all tracks.
 * Specifically, the total duration is the duration 
 * of the longest track.
 *
 * Return value: duration in nanoseconds.
 */
guint64 jammo_sequencer_get_duration(JammoSequencer* sequencer) {
	GList* list;
	guint64 duration;
	
	g_return_val_if_fail(JAMMO_IS_SEQUENCER(sequencer), 0);

	if (sequencer->priv->duration == 0) {
		for (list = sequencer->priv->tracks; list; list = list->next) {
			if ((duration = jammo_track_get_duration(JAMMO_TRACK(list->data))) != JAMMO_DURATION_INVALID && duration > sequencer->priv->duration) {
				sequencer->priv->duration = duration;
			}
		}
	}
	
	return sequencer->priv->duration;
}

/**
 * jammo_sequencer_get_position:
 * @sequencer: a #JammoSequencer
 *
 * Queries the current position of playing.
 *
 * Return value: position in nanoseconds.
 */
guint64 jammo_sequencer_get_position(JammoSequencer* sequencer) {
	gint64 position;
	GstFormat format = GST_FORMAT_TIME;

	g_return_val_if_fail(JAMMO_IS_SEQUENCER(sequencer), 0);

	if (!gst_element_query_position(sequencer->priv->pipeline, &format, &position)) {
		position = 0;
	}
	
	return (guint64)position;
}

/**
 * jammo_sequencer_set_position:
 * @sequencer: a #JammoSequencer
 * @position: position in nanoseconds
 *
 * Seeks the position of playing which is needed in real-time pair jamming.
 *
 * Return value: boolean success or not.
 */

gboolean jammo_sequencer_set_position(JammoSequencer* sequencer, guint64 position) {
	gboolean success;

	g_return_val_if_fail(JAMMO_IS_SEQUENCER(sequencer), FALSE);

	success = gst_element_seek(sequencer->priv->pipeline, 1.0, GST_FORMAT_TIME, GST_SEEK_FLAG_ACCURATE | GST_SEEK_FLAG_FLUSH,
	                           GST_SEEK_TYPE_SET, (gint64)position, GST_SEEK_TYPE_SET, GST_CLOCK_TIME_NONE);

	return success;
}

/**
 * jammo_sequencer_get_output_filename:
 * @sequencer: a #JammoSequencer
 *
 * Gets the name of the output file if exists.
 *
 * Return value: The name of the output file (or NULL, if output is audible).
 */
const gchar* jammo_sequencer_get_output_filename(JammoSequencer* sequencer) {
	g_return_val_if_fail(JAMMO_IS_SEQUENCER(sequencer), NULL);

	return sequencer->priv->output_filename;
}

/**
 * jammo_sequencer_set_output_filename:
 * @sequencer: a #JammoSequencer
 * @output_filename: the name of the output audio file
 *
 * Sets sequencer to output a file (@output_filename is non-null), or normal audio via sound card
 * (output_filename is null).
 *
 * Return value: Whether the operation succeeded.
 */
gboolean jammo_sequencer_set_output_filename(JammoSequencer* sequencer, const gchar* output_filename) {
	gboolean retvalue = TRUE;
	GList * list;
	JammoTrack* track;
	
	g_return_val_if_fail(JAMMO_IS_SEQUENCER(sequencer), FALSE);

	if (g_strcmp0(sequencer->priv->output_filename, output_filename)) {
		jammo_sequencer_stop(sequencer);
	
		if (output_filename && sequencer->priv->filesink) {
			g_object_set(sequencer->priv->filesink, "location", output_filename, NULL);
		} else {
			/* Destroy current sinks. */
			if (sequencer->priv->audiosink) {
				gst_bin_remove_many(GST_BIN(sequencer->priv->bin), sequencer->priv->queue, sequencer->priv->audiosink, NULL);
				sequencer->priv->audiosink = NULL;
			}
			if (sequencer->priv->filesink) {
				gst_bin_remove_many(GST_BIN(sequencer->priv->bin), sequencer->priv->audioconvert, sequencer->priv->vorbisenc, sequencer->priv->oggmux, sequencer->priv->filesink, NULL);
				sequencer->priv->filesink = NULL;
			}

			/* Create a new sink. */
			if (output_filename) {
				retvalue = setup_filesink(sequencer, output_filename);
			} else {
				retvalue = setup_audiosink(sequencer);
			}

			for (list = sequencer->priv->tracks; list; list = list->next) {
				track = JAMMO_TRACK(list->data);
				_jammo_track_set_offline(track, output_filename != NULL);
			}	
		}
		
		g_free(sequencer->priv->output_filename);
		sequencer->priv->output_filename = g_strdup(output_filename);
		g_object_notify(G_OBJECT(sequencer), "output-filename");
	}
	
	return retvalue;
}

static void jammo_sequencer_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoSequencer* sequencer;
	
	sequencer = JAMMO_SEQUENCER(object);

	switch (prop_id) {
		case PROP_TEMPO:
			jammo_sequencer_set_tempo(sequencer, g_value_get_uint(value));
			break;
		case PROP_PITCH:
			jammo_sequencer_set_pitch(sequencer, g_value_get_string(value));
			break;
		case PROP_OUTPUT_FILENAME:
			jammo_sequencer_set_output_filename(sequencer, g_value_get_string(value));
			break;
		case PROP_PLAY:
			if (g_value_get_boolean(value)) {
				jammo_sequencer_play(sequencer);
			} else {
				jammo_sequencer_stop(sequencer);
			}
			break;
		case PROP_TIME_SIGNATURE_BEATS:
			jammo_sequencer_set_time_signature(sequencer, g_value_get_uint(value), sequencer->priv->time_signature_note_value);
			break;
		case PROP_TIME_SIGNATURE_NOTE_VALUE:
			jammo_sequencer_set_time_signature(sequencer, sequencer->priv->time_signature_beats, g_value_get_uint(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_sequencer_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoSequencer* sequencer;

	sequencer = JAMMO_SEQUENCER(object);

        switch (prop_id) {
		case PROP_READY:
			g_value_set_boolean(value, sequencer->priv->ready);
			break;
		case PROP_TEMPO:
			g_value_set_uint(value, sequencer->priv->tempo);
			break;
		case PROP_PITCH:
			g_value_set_string(value, sequencer->priv->pitch);
			break;
		case PROP_OUTPUT_FILENAME:
			g_value_set_string(value, sequencer->priv->output_filename);
			break;
		case PROP_PLAY:
			g_value_set_boolean(value, sequencer->priv->playing);
			break;
		case PROP_TIME_SIGNATURE_BEATS:
			g_value_set_uint(value, sequencer->priv->time_signature_beats);
			break;
		case PROP_TIME_SIGNATURE_NOTE_VALUE:
			g_value_set_uint(value, sequencer->priv->time_signature_note_value);
			break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static void jammo_sequencer_finalize(GObject* object) {
	JammoSequencer* sequencer;

	sequencer = JAMMO_SEQUENCER(object);

	g_free(sequencer->priv->pitch);

	G_OBJECT_CLASS(jammo_sequencer_parent_class)->finalize(object);
}

static void jammo_sequencer_dispose(GObject* object) {
	JammoSequencer* sequencer;

	sequencer = JAMMO_SEQUENCER(object);

	if (sequencer->priv->pipeline) {
		g_object_unref(sequencer->priv->pipeline);
		sequencer->priv->pipeline=NULL;
	}

	g_list_foreach(sequencer->priv->tracks, (GFunc)g_object_unref, NULL);
	g_list_free(sequencer->priv->tracks);
	sequencer->priv->tracks = NULL;

	G_OBJECT_CLASS(jammo_sequencer_parent_class)->dispose(object);
}

static void jammo_sequencer_class_init(JammoSequencerClass* klass) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(klass);

	gobject_class->finalize = jammo_sequencer_finalize;
	gobject_class->dispose = jammo_sequencer_dispose;
	gobject_class->set_property = jammo_sequencer_set_property;
	gobject_class->get_property = jammo_sequencer_get_property;

	/**
	 * JammoSequencer:ready:
	 *
	 * Whether the sequencer is ready to play or not.
	 */
	g_object_class_install_property(gobject_class, PROP_READY,
	                                g_param_spec_boolean("ready",
	                                "Ready",
	                                "Whether the sequencer is ready to play or not",
	                                TRUE,
	                                G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSequencer:tempo:
	 *
	 * The tempo in beats per minute for all tracks in this sequencer.
	 */
	g_object_class_install_property(gobject_class, PROP_TEMPO,
	                                g_param_spec_uint("tempo",
	                                "Tempo",
	                                "The tempo in beats per minute for all tracks in this sequencer",
	                                0, G_MAXUINT, 110,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSequencer:pitch:
	 *
	 * The pitch as a note for all tracks in this sequencer.
	 */
	g_object_class_install_property(gobject_class, PROP_PITCH,
	                                g_param_spec_string("pitch",
	                                "Pitch",
	                                "The pitch as a note for all tracks in this sequencer",
	                                NULL,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSequencer:time_signature_beats:
	 *
	 * The number of beats in a bar for all tracks in this sequencer.
	 */
	g_object_class_install_property(gobject_class, PROP_TIME_SIGNATURE_BEATS,
	                                g_param_spec_uint("time-signature-beats",
	                                "Time signature: beats",
	                                "The number of beats in a bar for all tracks in this sequencer",
	                                1, 32, 4,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSequencer:time_signature_note_value:
	 *
	 * The number of beats in a bar for all tracks in this sequencer.
	 */
	g_object_class_install_property(gobject_class, PROP_TIME_SIGNATURE_NOTE_VALUE,
	                                g_param_spec_uint("time-signature-note-value",
	                                "Time signature: note value",
	                                "The note value of beats in a bar for all tracks in this sequencer",
	                                1, 32, 4,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSequencer:output-filename:
	 *
	 * The name of the audio file that is created as an output of this sequencer. If NULL, the audio is outputted into
	 * a sound card (normal behaviour).
	 */
	g_object_class_install_property(gobject_class, PROP_OUTPUT_FILENAME,
	                                g_param_spec_string("output-filename",
	                                "Output filename",
	                                "The name of the audio file that is created as an output of this sequencer",
	                                NULL,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	/**
	 * JammoSequencer:play:
	 *
	 * This can be used for play/stop the sequencer. And it can be used to ask current playing-state of sequencer.
	 * TRUE=plaing/start_play
	 * FALSE=stopped/stop_play
	 */
	g_object_class_install_property(gobject_class, PROP_PLAY,
	                                g_param_spec_boolean("play",
	                                "Play or stop sequencer",
	                                "Play or stop sequencer",
	                                FALSE,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE |G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	/**
	 * JammoSequencer::started:
	 * @sequencer: the object which received the signal
	 *
	 * The ::started signal is emitted when the playing (of all tracks) has started.
	 */
	signals[STARTED] = g_signal_new("started", G_TYPE_FROM_CLASS(gobject_class),
					       G_SIGNAL_RUN_LAST, 0,
					       NULL, NULL,
					       g_cclosure_marshal_VOID__VOID,
					       G_TYPE_NONE, 0);
	/**
	 * JammoSequencer::stopped:
	 * @sequencer: the object which received the signal
	 *
	 * The ::stopped signal is emitted when the playing (of all tracks) has stopped.
	 */
	signals[STOPPED] = g_signal_new("stopped", G_TYPE_FROM_CLASS(gobject_class),
					       G_SIGNAL_RUN_LAST, 0,
					       NULL, NULL,
					       g_cclosure_marshal_VOID__VOID,
					       G_TYPE_NONE, 0);
	/**
	 * JammoSequencer::paused:
	 * @sequencer: the object which received the signal
	 *
	 * The ::paused signal is emitted when the playing (of all tracks) has paused.
	 */
	signals[PAUSED] = g_signal_new("paused", G_TYPE_FROM_CLASS(gobject_class),
					       G_SIGNAL_RUN_LAST, 0,
					       NULL, NULL,
					       g_cclosure_marshal_VOID__VOID,
					       G_TYPE_NONE, 0);

	g_type_class_add_private(gobject_class, sizeof(JammoSequencerPrivate));
}

static void jammo_sequencer_init(JammoSequencer* sequencer) {
	GstBus* bus;

	sequencer->priv = G_TYPE_INSTANCE_GET_PRIVATE(sequencer, JAMMO_TYPE_SEQUENCER, JammoSequencerPrivate);

	sequencer->priv->ready = TRUE;
	sequencer->priv->tempo = 110;
	sequencer->priv->time_signature_beats=4;
	sequencer->priv->time_signature_note_value=4;

	sequencer->priv->pipeline = jammo_pipeline_new(NULL);
	bus = gst_pipeline_get_bus(GST_PIPELINE(sequencer->priv->pipeline));
	gst_bus_add_watch(bus, on_bus_message, sequencer);
	gst_object_unref (bus);

	sequencer->priv->bin = gst_bin_new(NULL);
	gst_bin_add(GST_BIN(sequencer->priv->pipeline), sequencer->priv->bin);

	if ((sequencer->priv->adder = create_element("adder"))) {
		gst_bin_add(GST_BIN(sequencer->priv->bin), sequencer->priv->adder);
		setup_audiosink(sequencer);
	}
}

static gboolean jammo_sequencer_parse_custom_node(ClutterScriptable* scriptable, ClutterScript* script, GValue* value, const gchar* name, JsonNode* node) {
	gboolean retvalue = FALSE;
	GSList* slist = NULL;
	JsonArray* array;
	gint i;
	JsonNode* n;
	GObject* object;
	gchar* message;

	if (!g_strcmp0(name, "tracks")) {
		if (JSON_NODE_TYPE(node) != JSON_NODE_ARRAY) {
				message = g_strdup_printf("Expected JSON array for '%s', skipped.", name);
				cem_add_to_log(message,J_LOG_WARN);
				g_free(message);
		} else {
			array = json_node_get_array(node);
			for (i = json_array_get_length(array) - 1; i >= 0; i--) {
				n = json_array_get_element(array, i);
				if (JSON_NODE_TYPE(n) != JSON_NODE_VALUE || json_node_get_value_type(n) != G_TYPE_STRING) {
					message = g_strdup_printf("Expected a string value in '%s' array, skipped.", name);
					cem_add_to_log(message,J_LOG_WARN);
					g_free(message);
				} else if (!(object = clutter_script_get_object(script, json_node_get_string(n)))) {
					message = g_strdup_printf("A reference to non-existing object '%s' in '%s' array, skipped.", json_node_get_string(n), name);
					cem_add_to_log(message,J_LOG_WARN);
					g_free(message);
				} else {
					slist = g_slist_prepend(slist, object);
				}
			}
					
			g_value_init(value, G_TYPE_POINTER);
			g_value_set_pointer(value, slist);

			retvalue = TRUE;
		}
	} else if (parent_scriptable_iface->parse_custom_node) {
		retvalue = parent_scriptable_iface->parse_custom_node(scriptable, script, value, name, node);
	}
	
	return retvalue;
}

static void jammo_sequencer_set_custom_property(ClutterScriptable* scriptable, ClutterScript* script, const gchar* name, const GValue* value) {
	JammoSequencer* sequencer;
	GSList* slist;
	GSList* slist_item;
	
	sequencer = JAMMO_SEQUENCER(scriptable);
	
	if (!g_strcmp0(name, "tracks")) {
		g_return_if_fail(G_VALUE_HOLDS(value, G_TYPE_POINTER));

		slist = (GSList*)g_value_get_pointer(value);
		for (slist_item = slist; slist_item; slist_item = slist_item->next) {
			jammo_sequencer_add_track(sequencer, JAMMO_TRACK(slist_item->data));
		}
		g_slist_free(slist);	
	} else if (parent_scriptable_iface->set_custom_property) {
		parent_scriptable_iface->set_custom_property(scriptable, script, name, value);
	} else {
		g_object_set_property(G_OBJECT(scriptable), name, value);
	}
}

static void clutter_scriptable_iface_init(ClutterScriptableIface* iface) {
	if (!(parent_scriptable_iface = g_type_interface_peek_parent (iface))) {
		parent_scriptable_iface = g_type_default_interface_peek(CLUTTER_TYPE_SCRIPTABLE);
	}
	
	iface->parse_custom_node = jammo_sequencer_parse_custom_node;
	iface->set_custom_property = jammo_sequencer_set_custom_property;
}

static gboolean on_bus_message(GstBus* bus, GstMessage* message, gpointer data) {
	JammoSequencer* sequencer;
	GError* error;
	gchar* debug;
	//GstObject* source;
	GList* list;
	JammoTrack* track;
	
	sequencer = JAMMO_SEQUENCER(data);
	switch (GST_MESSAGE_TYPE(message)) {
		case GST_MESSAGE_EOS:
			gst_element_set_state(sequencer->priv->pipeline, GST_STATE_NULL);
			g_signal_emit(sequencer, signals[STOPPED], 0);
			sequencer->priv->playing=FALSE;
			g_object_notify(G_OBJECT(sequencer), "play");
			break;
		case GST_MESSAGE_ERROR:
			gst_message_parse_error(message, &error, &debug);
			cem_add_to_log(error->message,J_LOG_ERROR);
			g_error_free(error);
			g_free(debug);
			break;
		case GST_MESSAGE_ELEMENT:
			//source = GST_MESSAGE_SRC(message);
			for (list = sequencer->priv->tracks; list; list = list->next) {
				track = JAMMO_TRACK(list->data);
				if (_jammo_track_receive_message(track, message)) {
					break;
				}
			}
			break;
		default:
			break;
	}
	
	return TRUE;
}

static void on_duration_notify(GObject* object, GParamSpec* param_spec, gpointer user_data) {
	JammoSequencer* sequencer;
	JammoTrack* track;
	guint64 duration;
	gboolean ready;
	GList* list;

	sequencer = JAMMO_SEQUENCER(user_data);
	track = JAMMO_TRACK(object);

	if ((duration = jammo_track_get_duration(track)) == JAMMO_DURATION_INVALID) {
		sequencer->priv->duration = 0;
		if (sequencer->priv->ready) {
			if (sequencer->priv->playing) {
				gst_element_set_state(sequencer->priv->pipeline, GST_STATE_NULL);
			}
			sequencer->priv->ready = FALSE;
			g_object_notify(G_OBJECT(sequencer), "ready");
		}
	} else {
		if (!sequencer->priv->ready) {
			ready = TRUE;
			for (list = sequencer->priv->tracks; list; list = list->next) {
				if (list->data != (gpointer)track &&
				    jammo_track_get_duration(JAMMO_TRACK(list->data)) == JAMMO_DURATION_INVALID) {
					ready = FALSE;
					break;
				}
			}
			if (ready) {
				sequencer->priv->ready = TRUE;
				g_object_notify(G_OBJECT(sequencer), "ready");
				if (sequencer->priv->playing) {
					start_playing(sequencer);
				}
			}
		}	
	}
}

static GstElement* create_element(const gchar* name) {
	GstElement* element;

	if (!(element = gst_element_factory_make(name, NULL))) {
		g_critical("Could not create GStreamer element '%s'.", name);
	}
	
	return element;
}

static gboolean setup_audiosink(JammoSequencer* sequencer) {
	gboolean retvalue = FALSE;
	
	if ((sequencer->priv->queue = create_element("queue")) &&
	    (sequencer->priv->audiosink = create_element("pulsesink"))) {
		gst_bin_add_many(GST_BIN(sequencer->priv->bin), sequencer->priv->queue, sequencer->priv->audiosink, NULL);
		if (!gst_element_link_many(sequencer->priv->adder, sequencer->priv->queue, sequencer->priv->audiosink, NULL)) {
			g_critical("Failed to link pulsesink.");
		} else {
			retvalue = TRUE;
		}
	}
	
	return retvalue;
}

static gboolean setup_filesink(JammoSequencer* sequencer, const gchar* filename) {
	gboolean retvalue = FALSE;
	
	if ((sequencer->priv->audioconvert = create_element("audioconvert")) &&
	    (sequencer->priv->vorbisenc = create_element("vorbisenc")) &&
	    (sequencer->priv->oggmux = create_element("oggmux")) &&
	    (sequencer->priv->filesink = create_element("filesink"))) {
		g_object_set(G_OBJECT(sequencer->priv->vorbisenc), "quality", OGGQUALITY, NULL);
		g_object_set(G_OBJECT(sequencer->priv->filesink), "location", filename, NULL);

		gst_bin_add_many(GST_BIN(sequencer->priv->bin), sequencer->priv->audioconvert, sequencer->priv->vorbisenc, sequencer->priv->oggmux, sequencer->priv->filesink, NULL);
		if (!gst_element_link_many(sequencer->priv->adder, sequencer->priv->audioconvert, sequencer->priv->vorbisenc, sequencer->priv->oggmux, sequencer->priv->filesink, NULL)) {
			g_critical("Failed to link filesink.");
		} else {
			retvalue = TRUE;
		}
	}

	return retvalue;
}

static void start_playing(JammoSequencer* sequencer) {
	GstState state, pending;
	
	gst_element_set_state(sequencer->priv->pipeline, GST_STATE_PAUSED);
	gst_element_get_state(sequencer->priv->bin, &state, &pending, GST_CLOCK_TIME_NONE);

	gst_element_set_state(sequencer->priv->pipeline, GST_STATE_PLAYING);
	g_signal_emit(sequencer, signals[STARTED], 0);
}
