/*
 * jammo-recording-track.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */
 
#include "jammo-recording-track.h"
#include "jammo-meam-private.h"
#include <string.h>

G_DEFINE_TYPE(JammoRecordingTrack, jammo_recording_track, JAMMO_TYPE_TRACK);

enum {
	PROP_0,
	PROP_PITCH_DETECT,
	PROP_FILENAME,
	PROP_ENABLED
};

enum {
	PITCH_DETECTED,
	LAST_SIGNAL
};

struct _JammoRecordingTrackPrivate {
	gboolean pitch_detect;
	gchar* filename;
	gboolean enabled;
	GstElement* sink; //We wan't change target after construct
};

static guint signals[LAST_SIGNAL] = { 0 };

JammoRecordingTrack* jammo_recording_track_new(const gchar* filename) {

	return JAMMO_RECORDING_TRACK(g_object_new(JAMMO_TYPE_RECORDING_TRACK, "filename", filename, NULL));
}

JammoRecordingTrack* jammo_recording_track_new_with_pitch_detect(const gchar* filename) {

	return JAMMO_RECORDING_TRACK(g_object_new(JAMMO_TYPE_RECORDING_TRACK, "filename", filename, "pitch-detect", TRUE, NULL));
}

void jammo_recording_track_set_enabled(JammoRecordingTrack* recording_track, gboolean enable) {
	g_return_if_fail(JAMMO_IS_RECORDING_TRACK(recording_track));

	recording_track->priv->enabled = enable;

	//This can be called before sink is created
	if (recording_track->priv->sink) {
		if (enable) {
			g_object_set(recording_track->priv->sink, "location", recording_track->priv->filename, NULL);
		}
		else {
			g_object_set(recording_track->priv->sink, "location", "/dev/null", NULL);
		}
	}
}

static void jammo_recording_track_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoRecordingTrack* recording_track;
	
	recording_track = JAMMO_RECORDING_TRACK(object);

	switch (prop_id) {
		case PROP_PITCH_DETECT:
			recording_track->priv->pitch_detect = g_value_get_boolean(value);
			break;
		case PROP_FILENAME:
			if (recording_track->priv->filename)
				g_free(recording_track->priv->filename);
			recording_track->priv->filename = g_strdup(g_value_get_string(value));
			if (recording_track->priv->sink)
				g_object_set(recording_track->priv->sink, "location", recording_track->priv->filename, NULL);
			jammo_recording_track_set_enabled(recording_track, TRUE);
			break;
		case PROP_ENABLED:
			jammo_recording_track_set_enabled(recording_track, g_value_get_boolean(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_recording_track_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoRecordingTrack* recording_track;

	recording_track = JAMMO_RECORDING_TRACK(object);

        switch (prop_id) {
		case PROP_PITCH_DETECT:
			g_value_set_boolean(value, recording_track->priv->pitch_detect);
			break;
		case PROP_FILENAME:
			g_value_set_string(value, recording_track->priv->filename);
			break;
		case PROP_ENABLED:
			g_value_set_boolean(value, recording_track->priv->enabled);
		break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static guint64 jammo_recording_track_get_duration(JammoTrack* track) {
	guint64 duration = 0;

	/* TODO: Get real duration. */

	return duration;
}

static gboolean jammo_recording_track_receive_message(JammoTrack* track, GstMessage* message) {
	gboolean retvalue = FALSE;
	const GstStructure* structure;
	gfloat frequency;
	
	structure = gst_message_get_structure(message);
	if (structure && !strcmp(gst_structure_get_name(structure), "pitchdetect")) {
		frequency = g_value_get_float(gst_structure_get_value(structure, "frequency"));
		g_signal_emit(track, signals[PITCH_DETECTED], 0, frequency);

		retvalue = TRUE;
	}

	return retvalue;
}

static gboolean jammo_recording_track_setup_element(JammoTrack* track, GstPipeline* pipeline, GstBin* bin, GstElement* sink_element) {
	JammoRecordingTrack* recording_track;
	GstElement* source;
	GstElement* convert;
	GstElement* pitch = NULL;
	GstElement* pitch_convert = NULL;
	GstElement* encoder;
	GstElement* sink;

	recording_track = JAMMO_RECORDING_TRACK(track);
	source = gst_element_factory_make("pulsesrc", NULL);
	convert = gst_element_factory_make("audioconvert", NULL);
	if (recording_track->priv->pitch_detect) {
		if (!(pitch = gst_element_factory_make("pitchdetect", NULL))) {
			g_warning("Element type 'pitchdetect' is not available. Disabling pitch detecting.");
			recording_track->priv->pitch_detect = FALSE;
		} else {
			pitch_convert = gst_element_factory_make("audioconvert", NULL);
		}
	}
	encoder = gst_element_factory_make("wavenc", NULL);
	sink = gst_element_factory_make("filesink", NULL);
	recording_track->priv->sink = sink;

	if (recording_track->priv->enabled) {
		g_assert(recording_track->priv->filename);
		g_object_set(sink, "location", recording_track->priv->filename, NULL);
	}
	else {
		g_object_set(sink, "location", "/dev/null", NULL);
	}

	if (recording_track->priv->pitch_detect) {
		gst_bin_add_many(GST_BIN(pipeline), source, convert, pitch, pitch_convert, encoder, sink, NULL);
		gst_element_link_many(source, convert, pitch, pitch_convert, encoder, sink, NULL);
	} else {
		gst_bin_add_many(GST_BIN(pipeline), source, convert, encoder, sink, NULL);
		gst_element_link_many(source, convert, encoder, sink, NULL);
	}
	
	return FALSE;
}

static void jammo_recording_track_finalize(GObject* object) {
	JammoRecordingTrack* recording_track;
	
	recording_track = JAMMO_RECORDING_TRACK(object);
	g_free(recording_track->priv->filename);

	G_OBJECT_CLASS(jammo_recording_track_parent_class)->finalize(object);
}

static void jammo_recording_track_dispose(GObject* object) {
	G_OBJECT_CLASS(jammo_recording_track_parent_class)->dispose(object);
}

/*Because of virtual function set_pitch() */
static void jammo_recording_track_set_pitch(JammoTrack* track, const gchar* pitch) {
}
/*Because of virtual function set_tempo() */
static void jammo_recording_track_set_tempo(JammoTrack* track, guint tempo) {
}

static void jammo_recording_track_class_init(JammoRecordingTrackClass* recording_track_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(recording_track_class);
	JammoTrackClass* track_class = JAMMO_TRACK_CLASS(recording_track_class);

	track_class->get_duration = jammo_recording_track_get_duration;
	track_class->setup_element = jammo_recording_track_setup_element;
	track_class->receive_message = jammo_recording_track_receive_message;
	track_class->set_tempo = jammo_recording_track_set_tempo; //Dummy
	track_class->set_pitch = jammo_recording_track_set_pitch; //Dummy
	gobject_class->finalize = jammo_recording_track_finalize;
	gobject_class->dispose = jammo_recording_track_dispose;
	gobject_class->set_property = jammo_recording_track_set_property;
	gobject_class->get_property = jammo_recording_track_get_property;

	/**
	 * JammoRecordingTrack:pitch-detect:
	 */
	g_object_class_install_property(gobject_class, PROP_PITCH_DETECT,
	                                g_param_spec_boolean("pitch-detect",
	                                "Pitch detect",
	                                "Specifies whether pitch detecting is activated or not",
	                                FALSE,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoRecordingTrack:filename:
	 */
	g_object_class_install_property(gobject_class, PROP_FILENAME,
	                                g_param_spec_string("filename",
	                                "File name",
	                                "The file name of the recorded audio (wav)",
	                                NULL,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	/**
	 * JammoRecordingTrack:enabled:
	 */
	g_object_class_install_property(gobject_class, PROP_ENABLED,
	                                g_param_spec_boolean("enabled",
	                                "Enabled",
	                                "Specifies whether recording-track is recording or not",
	                                TRUE,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	/**
	 * JammoRecordingTrack::pitch-detected:
	 * @track: the object which received the signal
	 *
	 * The ::pitch-detected signal is emitted when the recording track has calculated
	 * the frequency of the recording.
	 */
	signals[PITCH_DETECTED] = g_signal_new("pitch-detected", G_TYPE_FROM_CLASS(gobject_class),
					       G_SIGNAL_RUN_LAST, 0,
					       NULL, NULL,
					       g_cclosure_marshal_VOID__FLOAT,
					       G_TYPE_NONE, 1,
					       G_TYPE_FLOAT);

	g_type_class_add_private(gobject_class, sizeof(JammoRecordingTrackPrivate));
}

static void jammo_recording_track_init(JammoRecordingTrack* recording_track) {
	recording_track->priv = G_TYPE_INSTANCE_GET_PRIVATE(recording_track, JAMMO_TYPE_RECORDING_TRACK, JammoRecordingTrackPrivate);
}
