/*
 * jammo-instrument-track.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */
 
#ifndef __JAMMO_INSTRUMENT_TRACK_H__
#define __JAMMO_INSTRUMENT_TRACK_H__

#include <glib.h>
#include <glib-object.h>
#include "jammo-playing-track.h"
#include "jammo-midi.h"

#define JAMMO_TYPE_INSTRUMENT_TRACK (jammo_instrument_track_get_type ())
#define JAMMO_INSTRUMENT_TRACK(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_INSTRUMENT_TRACK, JammoInstrumentTrack))
#define JAMMO_IS_INSTRUMENT_TRACK(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_INSTRUMENT_TRACK))
#define JAMMO_INSTRUMENT_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_INSTRUMENT_TRACK, JammoInstrumentTrackClass))
#define JAMMO_IS_INSTRUMENT_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_INSTRUMENT_TRACK))
#define JAMMO_INSTRUMENT_TRACK_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_INSTRUMENT_TRACK, JammoInstrumentTrackClass))

typedef struct _JammoInstrumentTrackPrivate JammoInstrumentTrackPrivate;

typedef struct _JammoInstrumentTrack {
	JammoPlayingTrack parent_instance;
	JammoInstrumentTrackPrivate* priv;
} JammoInstrumentTrack;

typedef struct _JammoInstrumentTrackClass {
	JammoPlayingTrackClass parent_class;
} JammoInstrumentTrackClass;

GType jammo_instrument_track_get_type(void);

typedef enum {
		JAMMO_INSTRUMENT_TYPE_FLUTE,
		JAMMO_INSTRUMENT_TYPE_DRUMKIT,
		JAMMO_INSTRUMENT_TYPE_UD,
		JAMMO_INSTRUMENT_TYPE_PIANO,
		JAMMO_INSTRUMENT_TYPE_DUMMY, //Keep this last
} JammoInstrumentType;
JammoInstrumentTrack* jammo_instrument_track_new(JammoInstrumentType instrument_type);

void jammo_instrument_track_set_realtime(JammoInstrumentTrack* instrument_track, gboolean state);
void jammo_instrument_track_set_live(JammoInstrumentTrack* instrument_track, gboolean state);
void jammo_instrument_track_add_jammo_midi_event(JammoInstrumentTrack* instrument_track, JammoMidiEvent* event);
void jammo_instrument_track_add_event(JammoInstrumentTrack* instrument_track, char note, JammmoMidiEventType type, GstClockTime timestamp);
void jammo_instrument_track_remove_event(JammoInstrumentTrack* instrument_track, char note, GstClockTime timestamp);
void jammo_instrument_track_clear_events(JammoInstrumentTrack* instrument_track);

void jammo_instrument_track_note_on_realtime(JammoInstrumentTrack* instrument_track, char note);
void jammo_instrument_track_note_off_realtime(JammoInstrumentTrack* instrument_track, char note);


GList * jammo_instrument_track_get_event_list(JammoInstrumentTrack* instrument_track);
void jammo_instrument_track_set_event_list(JammoInstrumentTrack* instrument_track, GList* eventlist);

void jammo_instrument_track_set_volume(JammoInstrumentTrack* instrument_track, gfloat volume);

void jammo_instrument_track_load_to_track_from_file (JammoInstrumentTrack* instrument_track, const gchar* filename);
void jammo_instrument_track_dump_notes(JammoInstrumentTrack* instrument_track);

#endif
