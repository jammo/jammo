/*
 * jammo-track.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */
 
#include "jammo-track.h"
#include "jammo-meam-private.h"

#include "jammo-track.h"
#include "jammo-meam.h"

/**
 * SECTION:jammo-track
 * @Short_description: One track in a sequencer
 * @Title: JammoTrack
 *
 * A #JammoTrack consists of samples, notes or similar played
 * sequentially. An user of this class must make sure
 * that content does not overlap.
 *
 * A new #JammoTrack has floating reference after instantiation.
 * The reference will be sunk by a #JammoSequencer. Thus,
 * an application does not need to handle reference counting for
 * the #JammoTrack object.
 */
 
G_DEFINE_ABSTRACT_TYPE(JammoTrack, jammo_track, G_TYPE_INITIALLY_UNOWNED);

enum {
	PROP_0,
	PROP_DURATION
};

/**
 * jammo_track_get_duration:
 * @track: a #JammoTrack
 *
 * The total duration of the track in nanoseconds, or
 * JAMMO_DURATION_INVALID if the duration is not known.
 *
 * Implemented in subclasses.
 *
 * Return value: duration in nanoseconds.
 */
guint64 jammo_track_get_duration(JammoTrack* track) {
	guint64 duration = JAMMO_DURATION_INVALID;
	JammoTrackClass* track_class;
	
	g_return_val_if_fail(JAMMO_IS_TRACK(track), 0);

	track_class = JAMMO_TRACK_GET_CLASS(track);
	if (track_class->get_duration) {
		duration = track_class->get_duration(track);
	}
	
	return duration;
}

/**
 * _jammo_track_setup_element:
 * @track: a #JammoTrack
 * @bin: a #GstBin of where elements must be added
 * @sink_element: a #GstElement to which a source element must be linked
 *
 * Internally used by a #JammoSequencer.
 */
gboolean _jammo_track_setup_element(JammoTrack* track, GstPipeline* pipeline, GstBin* bin, GstElement* sink_element) {
	gboolean retvalue = FALSE;
	JammoTrackClass* track_class;

	g_return_val_if_fail(JAMMO_IS_TRACK(track), FALSE);

	track_class = JAMMO_TRACK_GET_CLASS(track);
	if (track_class->setup_element) {
		retvalue = track_class->setup_element(track, pipeline, bin, sink_element);
	}
	
	return retvalue;
}

/**
 * _jammo_track_receive_message:
 * @track: a #JammoTrack
 * @message: a received #GstMessage
 *
 * Internally called by a #JammoSequencer when it receives
 * a new message.
 *
 * Return value: TRUE, if the message is handled, or FALSE if the message must be delivered to the next track in a line
 */
gboolean _jammo_track_receive_message(JammoTrack* track, GstMessage* message) {
	gboolean retvalue = FALSE;
	JammoTrackClass* track_class;

	g_return_val_if_fail(JAMMO_IS_TRACK(track), FALSE);

	track_class = JAMMO_TRACK_GET_CLASS(track);
	if (track_class->receive_message) {
		retvalue = track_class->receive_message(track, message);
	}
	
	return retvalue;
}

void _jammo_track_set_tempo(JammoTrack* track, guint tempo) {
	JammoTrackClass* track_class;

	g_return_if_fail(JAMMO_IS_TRACK(track));

	track_class = JAMMO_TRACK_GET_CLASS(track);
	if (track_class->set_tempo) {
		track_class->set_tempo(track, tempo);
	} else {
		g_warning("Virtual function set_tempo() is not implemented in %s.", G_OBJECT_TYPE_NAME(G_OBJECT(track)));
	}
}

void _jammo_track_set_pitch(JammoTrack* track, const gchar* pitch) {
	JammoTrackClass* track_class;

	g_return_if_fail(JAMMO_IS_TRACK(track));

	track_class = JAMMO_TRACK_GET_CLASS(track);
	if (track_class->set_pitch) {
		track_class->set_pitch(track, pitch);
	} else {
		g_warning("Virtual function set_pitch() is not implemented in %s.", G_OBJECT_TYPE_NAME(G_OBJECT(track)));
	}
}

void _jammo_track_set_offline(JammoTrack* track, gboolean offline) {
	JammoTrackClass* track_class;

	g_return_if_fail(JAMMO_IS_TRACK(track));

	track_class = JAMMO_TRACK_GET_CLASS(track);
	if (track_class->set_offline) {
		track_class->set_offline(track, offline);
	}
}

void _jammo_track_set_time_signature(JammoTrack* track, guint time_signature_beats, guint time_signature_note_value) {
	JammoTrackClass* track_class;

	g_return_if_fail(JAMMO_IS_TRACK(track));

	track_class = JAMMO_TRACK_GET_CLASS(track);
	if (track_class->set_time_signature) {
		track_class->set_time_signature(track, time_signature_beats, time_signature_note_value);
	}
}

static void jammo_track_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoTrack* track;

	track = JAMMO_TRACK(object);

        switch (prop_id) {
		case PROP_DURATION:
			g_value_set_uint64(value, jammo_track_get_duration(track));
			break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static void jammo_track_class_init(JammoTrackClass* track_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(track_class);

	gobject_class->get_property = jammo_track_get_property;
	
	/**
	 * JammoTrack:duration:
	 *
	 * The total duration of the track in nanoseconds.
	 */
	g_object_class_install_property(gobject_class, PROP_DURATION,
	                                g_param_spec_uint64("duration",
	                                "Duration",
	                                "The duration of the track",
	                                0, G_MAXUINT64, JAMMO_DURATION_INVALID,
	                                G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
}

static void jammo_track_init(JammoTrack* track) {
}
