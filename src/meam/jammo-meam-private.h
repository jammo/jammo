/*
 * jammo-meam-private.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */
 
/* Private functions not to be used outside the module implementation. */

#ifndef INCLUDED_JAMMO_MEAM_PRIVATE_H
#define INCLUDED_JAMMO_MEAM_PRIVATE_H
 
#include <gst/gst.h>
#include "jammo-sample.h"
#include "jammo-sequencer.h"
#include "jammo-track.h"

/* jammo-sample.c */
void _jammo_sample_set_editing_track(JammoSample* sample, JammoEditingTrack* editing_track);
guint64 _jammo_sample_get_position(JammoSample* position);
void _jammo_sample_set_position(JammoSample* sample, guint64 position);

/* jammo-track.c */
gboolean _jammo_track_setup_element(JammoTrack* track, GstPipeline* pipeline, GstBin* bin, GstElement* sink_element);
gboolean _jammo_track_receive_message(JammoTrack* track, GstMessage* message);
void _jammo_track_set_tempo(JammoTrack* track, guint tempo);
void _jammo_track_set_pitch(JammoTrack* track, const gchar* pitch);
void _jammo_track_set_offline(JammoTrack* track, gboolean offline);
void _jammo_track_set_time_signature(JammoTrack* track, guint time_signature_beats, guint time_signature_note_value);

#endif
