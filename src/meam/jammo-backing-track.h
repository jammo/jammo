/*
 * jammo-backing-track.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */
 
#ifndef __JAMMO_BACKING_TRACK_H__
#define __JAMMO_BACKING_TRACK_H__

#include <glib.h>
#include <glib-object.h>
#include "jammo-playing-track.h"

#define JAMMO_TYPE_BACKING_TRACK (jammo_backing_track_get_type ())
#define JAMMO_BACKING_TRACK(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_BACKING_TRACK, JammoBackingTrack))
#define JAMMO_IS_BACKING_TRACK(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_BACKING_TRACK))
#define JAMMO_BACKING_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_BACKING_TRACK, JammoBackingTrackClass))
#define JAMMO_IS_BACKING_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_BACKING_TRACK))
#define JAMMO_BACKING_TRACK_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_BACKING_TRACK, JammoBackingTrackClass))

typedef struct _JammoBackingTrackPrivate JammoBackingTrackPrivate;

typedef struct _JammoBackingTrack {
	JammoPlayingTrack parent_instance;
	JammoBackingTrackPrivate* priv;
} JammoBackingTrack;

typedef struct _JammoBackingTrackClass {
	JammoPlayingTrackClass parent_class;
} JammoBackingTrackClass;

GType jammo_backing_track_get_type(void);

JammoBackingTrack* jammo_backing_track_new(const gchar* filename);

const gchar* jammo_backing_track_get_filename(JammoBackingTrack* backing_track);
void jammo_backing_track_set_filename(JammoBackingTrack* backing_track, const gchar* filename);

#endif
