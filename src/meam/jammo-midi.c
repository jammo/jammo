#include <stdio.h>
#include <stdlib.h>
#include "jammo-midi.h"

/* midi format in files
 * this is used to make note files readable
 * c3+ 7801064844,
 * c#3+ 7801064844
 * where c = note, 3 = octave, + = type, 7801064844 = timestamp
 * notes: c,c#,d,d#,...,b (It can have one or two characters)
 * octaves: 0-7
 * types: + = on, - = off
 * timestamp as nanoseconds
    7801064844ns = 7801064.844µs = 7801.064844ms = 7.801064844s
*/

/**
 @param 0...96
 @return "c" / "c#" ... / "error"
*/
const char * jammomidi_note_to_char(unsigned char note) {
	note = note % 12;
	switch (note) {
		case 0: return "c";
		case 1: return "c#";
		case 2: return "d";
		case 3: return "d#";
		case 4: return "e";
		case 5: return "f";
		case 6: return "f#";
		case 7: return "g";
		case 8: return "g#";
		case 9: return "a";
		case 10: return "a#";
		case 11: return "b";
		default: return "error";
	}
	return "error";
}

/**
 @param  note = "c" / "c#", octave = 0...7
 @return 0...96 / other number in error cases
*/
unsigned char jammomidi_char_to_note(char * note, char octave) {
	char ret=0;

	if (strncmp(note, "c",1)==0) {ret=0;}
	else if (strncmp(note, "c#",2)==0) {ret=1;}
	else if (strncmp(note, "d",1)==0)  {ret=2;}
	else if (strncmp(note, "d#",2)==0) {ret=3;}
	else if (strncmp(note, "e",1)==0)  {ret=4;}
	else if (strncmp(note, "f",1)==0)  {ret=5;}
	else if (strncmp(note, "f#",2)==0) {ret=6;}
	else if (strncmp(note, "g",1)==0)  {ret=7;}
	else if (strncmp(note, "g#",2)==0) {ret=8;}
	else if (strncmp(note, "a",1)==0)  {ret=9;}
	else if (strncmp(note, "a#",2)==0) {ret=10;}
	else if (strncmp(note, "b",1)==0)  {ret=11;}
	else {ret=127;}

	if (octave>JAMMOMIDI_NUMBER_OF_NOTES/12) {
		ret = 127;
	}

	//Do not multiple error case value
	if (ret!=127) {
		ret+=octave*12;
	}

	return ret;
}

/**
Writes given GList (containing midi-events) to given filename.
*/
gint jammomidi_glist_to_file(GList * list, char * filename) {
	FILE * fp;

	fp = fopen(filename, "w");
	if (fp==NULL) {
		return -1;
	}

	GList * temp;
	char octave;
	char type;
	const char * note;

	for (temp=list;temp;temp=temp->next) {
		note=jammomidi_note_to_char(((JammoMidiEvent*)(temp->data))->note);
		octave=((((JammoMidiEvent*)(temp->data))->note) / 12);
		if (((JammoMidiEvent*)(temp->data))->type == JAMMOMIDI_NOTE_ON)
			type= '+';
		else
			type='-';
		fprintf(fp, "%s%d%c %" G_GUINT64_FORMAT "\n", note, octave, type,(unsigned long int)((JammoMidiEvent*)(temp->data))->timestamp);
	}
	fclose(fp);

	return 0;
}


/**
This should be only function loading midi events from file.
Will return NULL (=empty list), if filename is NULL, or file not found
 (or no access to read it...)
*/
GList* jammomidi_file_to_glist(const char* filename){
	FILE * fp;
	GList * list;

	list=NULL;
	if (filename==NULL){
		return NULL;
	}

	fp = fopen(filename, "r");
	if (fp==NULL) {
		return NULL;
	}

	unsigned long int timestamp=0; //safer than guint64
	char octave;
	char type;
	char note[3]; //there are one slot for \0
	memset(note, 0, 3);

	//note can be one character "c" or two characters "c#"
	while(fscanf(fp, "%2c", note) != EOF) {
		//printf("got '%c'\n",note[0]);
		//printf("got '%c'\n",note[1]);
		if (note[1]=='#') { //it was 'c#' so just read entire of line
			if (3!=fscanf(fp, "%c %c%" G_GUINT64_FORMAT "\n", &octave, &type, &timestamp)){
				printf("error case\n"); //TODO
			}
			octave-='0';      //cast character to integer
			note[2]=0;        //now this array can be used as string
		}
		else { //it was only one character
			octave=note[1] - '0';  //note[1] is actually octave number
			note[1]=0;             //now this array can be used as string
			if (2!=fscanf(fp, "%c%" G_GUINT64_FORMAT "\n", &type, &timestamp)){
				printf("error case\n"); //TODO
			}
		}

		JammoMidiEvent * event;
		event=malloc(sizeof(JammoMidiEvent));
		//printf("note='%s', octave='%d',type='%c', timestamp'%llu'\n",note,octave,type, timestamp);

		event->note=jammomidi_char_to_note(note, octave);
		if (type=='+')
			event->type=JAMMOMIDI_NOTE_ON;
		else
			event->type=JAMMOMIDI_NOTE_OFF;
		event->timestamp=timestamp;

		printf("Note from file:'%d' state'%s' time'%" G_GUINT64_FORMAT "'\n", event->note, event->type==JAMMOMIDI_NOTE_ON ? "start":"stop", (unsigned long int)event->timestamp);
		list = g_list_prepend(list, event);
		memset(note, 0, 3);
	}
	list = g_list_reverse(list);
	fclose(fp);

	return list;
}


// a function for freeing jammomidi GList
// memory allocated in creating the list is not freed automatically
void jammomidi_free_glist(GList ** list) {
	int i;
  for (i=0;i<g_list_length(*list);i++) {
		JammoMidiEvent * stored_event;
		gpointer data = g_list_nth_data(*list, i);
		if (data!=NULL) {
			stored_event=(JammoMidiEvent *)data;
			free(stored_event);
		}
		data=NULL;
	}
	g_list_free(*list);
	*list=NULL;
}

// a function for comparing timestamps of two midi events
// return values: -1 a is earlier, 1 b earlier, 0 the same
// note-on is processed before note-off if the timestamps are the same
int compare_event_time (gconstpointer a,gconstpointer b){
	if (((JammoMidiEvent *)a)->timestamp < ((JammoMidiEvent *)b)->timestamp)
		return -1;
	else if (((JammoMidiEvent *)a)->timestamp > ((JammoMidiEvent *)b)->timestamp)
		return 1;

	// compare types also if time, note and octave are the same
	// stop needs to be before start
	// because it probably means that previous note should stop at the same time
	// if start is processed first the second note will not be played at all
	if (((JammoMidiEvent *)a)->note == ((JammoMidiEvent *)b)->note) {
		if (((JammoMidiEvent *)a)->type > ((JammoMidiEvent *)b)->type)
			return -1;
		else if (((JammoMidiEvent *)a)->type < ((JammoMidiEvent *)b)->type)
			return 1;
	}
	
	return 0;
}

// a function for storing midi events in a GList
void jammomidi_store_event_to_glist(GList ** list, char note, JammmoMidiEventType type, guint64 timestamp) {
	JammoMidiEvent * event;
	event=malloc(sizeof(JammoMidiEvent));
	event->type=type;
	event->note=note;
	event->timestamp=timestamp;
	*list = g_list_insert_sorted(*list ,event,compare_event_time);
}

//For debug purpose
void jammomidi_print_event(JammoMidiEvent* event) {
	printf("Jammo-Midi-Event:note:%d time: %llu ns: ,type: %s \n", event->note, (unsigned long long)event->timestamp, event->type==JAMMOMIDI_NOTE_ON? "start":"stop");
}

