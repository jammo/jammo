/*
 * jammo-sequencer.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */
 
#ifndef __JAMMO_SEQUENCER_H__
#define __JAMMO_SEQUENCER_H__

#include <glib.h>
#include <glib-object.h>

#define JAMMO_TYPE_SEQUENCER (jammo_sequencer_get_type ())
#define JAMMO_SEQUENCER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_SEQUENCER, JammoSequencer))
#define JAMMO_IS_SEQUENCER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_SEQUENCER))
#define JAMMO_SEQUENCER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_SEQUENCER, JammoSequencerClass))
#define JAMMO_IS_SEQUENCER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_SEQUENCER))
#define JAMMO_SEQUENCER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_SEQUENCER, JammoSequencerClass))

typedef struct _JammoSequencerPrivate JammoSequencerPrivate;

typedef struct _JammoSequencer {
	GObject parent_instance;
	JammoSequencerPrivate* priv;
} JammoSequencer;

typedef struct _JammoSequencerClass {
	GObjectClass parent_class;
} JammoSequencerClass;

GType jammo_sequencer_get_type(void);

#include "jammo-track.h"

JammoSequencer* jammo_sequencer_new(void);

gboolean jammo_sequencer_is_ready(JammoSequencer* sequencer);

guint jammo_sequencer_get_tempo(JammoSequencer* sequencer);
void jammo_sequencer_set_tempo(JammoSequencer* sequencer, guint tempo);
const gchar* jammo_sequencer_get_pitch(JammoSequencer* sequencer);
void jammo_sequencer_set_pitch(JammoSequencer* sequencer, const gchar* pitch);
guint jammo_sequencer_get_time_signature_beats(JammoSequencer* sequencer);
guint jammo_sequencer_get_time_signature_note_value(JammoSequencer* sequencer);
void jammo_sequencer_set_time_signature(JammoSequencer * sequencer, guint time_signature_beats, guint time_signature_note_value);

void jammo_sequencer_play(JammoSequencer* sequencer);
void jammo_sequencer_stop(JammoSequencer* sequencer);
void jammo_sequencer_pause(JammoSequencer* sequencer);

guint jammo_sequencer_get_track_count(JammoSequencer* sequencer);
JammoTrack* jammo_sequencer_get_track(JammoSequencer* sequencer, guint index);
void jammo_sequencer_add_track(JammoSequencer* sequencer, JammoTrack* track);

guint64 jammo_sequencer_get_duration(JammoSequencer* sequencer);
guint64 jammo_sequencer_get_position(JammoSequencer* sequencer);
gboolean jammo_sequencer_set_position(JammoSequencer* sequencer, guint64 position);

const gchar* jammo_sequencer_get_output_filename(JammoSequencer* sequencer);
gboolean jammo_sequencer_set_output_filename(JammoSequencer* sequencer, const gchar* output_filename);

#endif
