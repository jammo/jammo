/*
 * jammo-sample.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */
 

#ifndef __JAMMO_SAMPLE_H__
#define __JAMMO_SAMPLE_H__

#include <glib.h>
#include <glib-object.h>

#define JAMMO_TYPE_SAMPLE (jammo_sample_get_type ())
#define JAMMO_SAMPLE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_SAMPLE, JammoSample))
#define JAMMO_IS_SAMPLE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_SAMPLE))
#define JAMMO_SAMPLE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_SAMPLE, JammoSampleClass))
#define JAMMO_IS_SAMPLE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_SAMPLE))
#define JAMMO_SAMPLE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_SAMPLE, JammoSampleClass))

typedef struct _JammoSamplePrivate JammoSamplePrivate;

typedef struct _JammoSample {
	GInitiallyUnowned parent_instance;
	JammoSamplePrivate* priv;
} JammoSample;

typedef struct _JammoSampleClass {
	GInitiallyUnownedClass parent_class;
} JammoSampleClass;

GType jammo_sample_get_type(void);

#include "jammo-editing-track.h"
#include "jammo-sequencer.h"

JammoSample* jammo_sample_new_from_file(const gchar* filename);
JammoSample* jammo_sample_new_from_file_with_sequencer(const gchar* filename, JammoSequencer* sequencer);
JammoSample* jammo_sample_new_from_existing(JammoSample* sample);

const gchar* jammo_sample_get_filename(JammoSample* sample);
const gchar* jammo_sample_get_real_filename(JammoSample* sample);
guint64 jammo_sample_get_duration(JammoSample* sample);
JammoEditingTrack* jammo_sample_get_editing_track(JammoSample* sample);

guint jammo_sample_get_tempo(JammoSample* sample);
void jammo_sample_set_tempo(JammoSample* sample, guint tempo);
const gchar* jammo_sample_get_pitch(JammoSample* sample);
void jammo_sample_set_pitch(JammoSample* sample, const gchar* pitch);

void jammo_sample_play(JammoSample* sample);
void jammo_sample_stop(JammoSample* sample);
void jammo_sample_stop_all(void);

#endif
