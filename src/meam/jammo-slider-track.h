/*
 * jammo-slider-track.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */
 
#ifndef __JAMMO_SLIDER_TRACK_H__
#define __JAMMO_SLIDER_TRACK_H__

#include <glib.h>
#include <glib-object.h>
#include "jammo-playing-track.h"
#include "jammo-slider-event.h"

#define JAMMO_TYPE_SLIDER_TRACK (jammo_slider_track_get_type ())
#define JAMMO_SLIDER_TRACK(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_SLIDER_TRACK, JammoSliderTrack))
#define JAMMO_IS_SLIDER_TRACK(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_SLIDER_TRACK))
#define JAMMO_SLIDER_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_SLIDER_TRACK, JammoSliderTrackClass))
#define JAMMO_IS_SLIDER_TRACK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_SLIDER_TRACK))
#define JAMMO_SLIDER_TRACK_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_SLIDER_TRACK, JammoSliderTrackClass))

typedef struct _JammoSliderTrackPrivate JammoSliderTrackPrivate;

typedef struct _JammoSliderTrack {
	JammoPlayingTrack parent_instance;
	JammoSliderTrackPrivate* priv;
} JammoSliderTrack;

typedef struct _JammoSliderTrackClass {
	JammoPlayingTrackClass parent_class;
} JammoSliderTrackClass;

GType jammo_slider_track_get_type(void);

typedef enum {
		JAMMO_SLIDER_TYPE_ORGAN,
		JAMMO_SLIDER_TYPE_FM_MODULATION,
		JAMMO_SLIDER_TYPE_KARPLUS,
		JAMMO_SLIDER_TYPE_DUMMY, //Keep this last
} JammoSliderType;

JammoSliderTrack* jammo_slider_track_new(JammoSliderType slider_type);

void jammo_slider_track_add_event(JammoSliderTrack* slider_track,float freq, JammoSliderEventType type, GstClockTime timestamp);
void jammo_slider_track_remove_event(JammoSliderTrack* slider_track,float freq, JammoSliderEventType type, GstClockTime timestamp);

void jammo_slider_track_set_frequency_realtime(JammoSliderTrack* slider_track,float frequency);
void jammo_slider_track_set_on_realtime(JammoSliderTrack* slider_track, float freq);
void jammo_slider_track_set_off_realtime(JammoSliderTrack* slider_track, float freq);

GList * jammo_slider_track_get_event_list(JammoSliderTrack* slider_track);
void jammo_slider_track_set_event_list(JammoSliderTrack* slider_track, GList* eventlist);

void jammo_slider_track_set_recording(JammoSliderTrack* slider_track, gboolean recording);

void jammo_slider_track_set_live(JammoSliderTrack* slider_track, gboolean state);

void jammo_slider_track_dump_events(JammoSliderTrack* slider_track);

void jammo_slider_track_clear_events(JammoSliderTrack* slider_track);

#endif
