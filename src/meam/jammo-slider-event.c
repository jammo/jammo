#include <stdio.h>
#include <stdlib.h>
#include "jammo-slider-event.h"

/* slider event format in files
 * this is used to make note files readable
 * 100.2 + 7801064844,
 * 130.1 m 7801264844,
 * 300.0 - 7801464844
 * where 100.2 = frequency, + = type, 7801064844 = timestamp
 * frequency: TODO define
 * types: + = on, - = off, m = motion
 * timestamp as nanoseconds
    7801064844ns = 7801064.844µs = 7801.064844ms = 7.801064844s
*/

/**
Writes given GList (containing slider-events) to given filename.
*/
gint jammo_slider_event_glist_to_file(GList * list, char * filename) {
	FILE * fp;

	fp = fopen(filename, "w");
	if (fp==NULL) {
		return -1;
	}

	GList * temp;
	char type;
	float frequency;

	for (temp=list;temp;temp=temp->next) {
		frequency = ((JammoSliderEvent*)(temp->data))->freq;
		if (((JammoSliderEvent*)(temp->data))->type == JAMMO_SLIDER_EVENT_ON)
			type= '+';
		else if (((JammoSliderEvent*)(temp->data))->type == JAMMO_SLIDER_EVENT_OFF)
			type='-';
		else
			type='m';


		gint buf_len = 10;
		gchar buffer[buf_len];
		gchar* frequency_str = g_ascii_dtostr (buffer, buf_len,frequency);

		fprintf(fp, "%s %c %" G_GUINT64_FORMAT "\n", frequency_str, type,(unsigned long)((JammoSliderEvent*)(temp->data))->timestamp);
	}
	fclose(fp);

	return 0;
}


/**
This should be only function loading slider events from file.
Will return NULL (=empty list), if filename is NULL, or file not found
 (or no access to read it...)
*/
GList * jammo_slider_event_file_to_glist(char * filename) {
	FILE * fp;
	GList * list;

	list=NULL;
	if (filename==NULL){
		return NULL;
	}

	fp = fopen(filename, "r");
	if (fp==NULL) {
		return NULL;
	}

	unsigned long timestamp=0; //safer than guint64
	float frequency;
	char type;

	char frequency_str[10];
	while(fscanf(fp, "%s %c %" G_GUINT64_FORMAT "", frequency_str, &type, &timestamp) != EOF) {

		frequency = atof(frequency_str);
		JammoSliderEvent * event;
		event=malloc(sizeof(JammoSliderEvent));

		event->freq=frequency;
		if (type=='+')
			event->type=JAMMO_SLIDER_EVENT_ON;
		else if (type=='-')
			event->type=JAMMO_SLIDER_EVENT_OFF;
		else
			event->type=JAMMO_SLIDER_EVENT_MOTION;
		event->timestamp=timestamp;

		printf("Event from file:'%f' state'%d' time'%" G_GUINT64_FORMAT " '\n", event->freq, event->type, (unsigned long)event->timestamp);
		list = g_list_prepend(list, event);
	}
	list = g_list_reverse(list);
	fclose(fp);

	return list;
}


// a function for freeing jammo_slider_event GList
// memory allocated in creating the list is not freed automatically
void jammo_slider_event_free_glist(GList ** list) {
	int i;
  for (i=0;i<g_list_length(*list);i++) {
		JammoSliderEvent * stored_event;
		gpointer data = g_list_nth_data(*list, i);
		if (data!=NULL) {
			stored_event=(JammoSliderEvent *)data;
			free(stored_event);
		}
		data=NULL;
	}
	g_list_free(*list);
	*list=NULL;
}

// a function for comparing timestamps of two slider events
// return values: -1 a is earlier, 1 b earlier, 0 the same
int jammo_slider_event_compare_event_time(gconstpointer a,gconstpointer b){
	if (((JammoSliderEvent *)a)->timestamp < ((JammoSliderEvent *)b)->timestamp)
		return -1;
	else if (((JammoSliderEvent *)a)->timestamp > ((JammoSliderEvent *)b)->timestamp)
		return 1;

	// compare types also if time is the same
	// ON is always before off and motion
	// OFF is always after on and motion
	if (((JammoSliderEvent *)a)->type == JAMMO_SLIDER_EVENT_ON && ((JammoSliderEvent *)b)->type != JAMMO_SLIDER_EVENT_ON)
		return -1;
	else if (((JammoSliderEvent *)b)->type == JAMMO_SLIDER_EVENT_ON && ((JammoSliderEvent *)a)->type != JAMMO_SLIDER_EVENT_ON)
		return 1;
	else if (((JammoSliderEvent *)a)->type == JAMMO_SLIDER_EVENT_OFF && ((JammoSliderEvent *)b)->type != JAMMO_SLIDER_EVENT_OFF)
		return 1;
	else if (((JammoSliderEvent *)b)->type == JAMMO_SLIDER_EVENT_OFF && ((JammoSliderEvent *)a)->type != JAMMO_SLIDER_EVENT_OFF)
		return -1;
	
	return 0;
}

// a function for storing slider events in a GList
void jammo_slider_event_store_event_to_glist(GList ** list, float freq, JammoSliderEventType type, guint64 timestamp) {
	JammoSliderEvent * event;
	event=malloc(sizeof(JammoSliderEvent));
	event->type=type;
	event->freq=freq;
	event->timestamp=timestamp;
	*list = g_list_insert_sorted(*list ,event,jammo_slider_event_compare_event_time);
}


