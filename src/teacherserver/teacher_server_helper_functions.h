/*
 * teacher_server_helper_functions.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 *	    Tommi Kallonen  <tommi.kallonen@lut.fi>
 */
 
 
#ifndef __TEACHER_SERVER_HELPER_FUNCTIONS_
#define __TEACHER_SERVER_HELPER_FUNCTIONS_


teacher_server_profile_data* load_database(teacher_server_profile_data* data);
void save_database(teacher_server_profile_data* data);

#endif
