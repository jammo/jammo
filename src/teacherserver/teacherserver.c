#include "../configure.h"
#include "../meam/jammo-meam.h"
#include <stdlib.h> 
#include <libgen.h>


#include <fcntl.h> 

#include <string.h>

#include "../cem/cem.h"
#include "../gems/gems.h"
#include "../gems/gems_teacher_server_utils.h"
#include "../gems/gems_service_profile.h"
#include "teacher_server_helper_functions.h"
#include "../gems/groupmanager.h"
#include "../gems/gems_message_functions.h"
 
#include <gtk/gtk.h>
 
enum
{
  LIST_ITEM = 0,
  ID_COLUMN = 1,
  LOCK_COLUMN = 2,
  N_COLUMNS = 3
};


GtkWidget *userlist;
GtkWidget *grouplist;
GtkWidget *logview;
GtkWidget  *mainwindow;
GtkWidget  *profile_window;
GtkWidget  *group_window;
GtkWidget *user_id_entry, *user_name_entry, *password_entry1, *password_entry2, *auth_level_entry, *first_name_entry, *last_name_entry, *age_entry, *avatar_id_entry, *radiobutton_pair, *radiobutton_group, *themeselection, *themelabel;
GList* connections;
teacher_server_profile_data* profile_database;


static guint waiting_for_profile_id=0;

static void init_user_list(GtkWidget *list);
static void add_to_userlist(GtkWidget *list, const gchar *str, guint32 id, gboolean locked);
static void add_to_grouplist(GtkWidget *list, const gchar *str);

// Prints log to textfield
void printlog(GtkWidget *widget, guint32 userid)
{
	gchar* contents;
	gsize length;
	GtkTextBuffer *buffer;
	GtkTextIter iter;



	gchar* filename = g_strdup_printf("%s/%d.log",configure_get_log_directory(),userid);

	if (!g_file_get_contents(filename, &contents, &length, NULL)) 
	{
		gchar* logmsg = g_strdup_printf("Could not load user log from file '%s'.", filename);
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(widget));
		gtk_text_buffer_set_text(buffer, logmsg, strlen(logmsg));
		gtk_text_buffer_get_end_iter(buffer, &iter);
		gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW(widget), &iter, 0.0, FALSE, 0, 0);
	} 
	else 
	{
		buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(widget));
		gtk_text_buffer_set_text(buffer, contents, length);
		gtk_text_buffer_get_end_iter(buffer, &iter);
		gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW(widget), &iter, 0.0, FALSE, 0, 0);
	}

}

void callback_new_profile(jammo_profile_container* new_profile)
{
	jammo_profile_container* prof;

	if(new_profile != NULL)
	{
		if(profile_database)
		{
			prof = gems_teacher_server_get_profile_for_user_id(profile_database,new_profile->userid);
				
			// found, update
			if(prof)
			{
				if(g_strcmp0(prof->username,new_profile->username) == 0)
				{
					prof->points = new_profile->points;
					prof->avatarid = new_profile->avatarid;
					cem_add_to_log("Points and avatar updated.",J_LOG_INFO);
				}
				else cem_add_to_log("Username changed, not updated",J_LOG_ERROR);
				if(waiting_for_profile_id !=prof->userid )
				{	
					gtk_entry_set_text(GTK_ENTRY(user_id_entry), g_strdup_printf("%d",prof->userid));
					gtk_entry_set_text(GTK_ENTRY(avatar_id_entry), g_strdup_printf("%d",prof->avatarid));
					gtk_entry_set_text(GTK_ENTRY(age_entry), g_strdup_printf("%d",prof->age));
					gtk_entry_set_text(GTK_ENTRY(user_name_entry), prof->username);
					gtk_entry_set_text(GTK_ENTRY(auth_level_entry), prof->authlevel);
					gtk_entry_set_text(GTK_ENTRY(first_name_entry), prof->firstname); 
					gtk_entry_set_text(GTK_ENTRY(last_name_entry), prof->lastname);	
					waiting_for_profile_id = 0;
				}	
			}
			else cem_add_to_log("Not a valid profile, ignoring",J_LOG_INFO);
		}
	}
	else cem_add_to_log("Received empty profile",J_LOG_INFO);
}


void list_users()
{
	if (connections)
		g_list_free(connections); // free the LIST do not touch the data

	// get COPY of a list
	connections = gems_list_jammo_connections();
	if(connections)
	{
		gtk_list_store_clear(GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(userlist))));
		GList* iter = NULL;
		gems_peer_profile* prof = NULL;
		for(iter = g_list_first(connections); iter; iter = g_list_next(iter))
		{
			prof = gems_get_profile((gems_connection*)iter->data); // Get profile
			
			
			// Access profile data
			if(prof) 
			{
				add_to_userlist(GTK_WIDGET (userlist), gems_profile_manager_get_username(prof), gems_profile_manager_get_userid(prof), FALSE);
			}
			else
			{
				//printf("NULL profile\n");
				add_to_userlist(GTK_WIDGET (userlist), "NULL profile\n", 0, FALSE);
			}
		}
		
	}
	else printf("No connected JamMos\n");
}

void list_groups()
{
	gems_components* data = gems_get_data();
	GList* iter = NULL;
	gtk_list_store_clear(GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(grouplist))));
	for(iter = g_list_first(data->service_group->other_groups); iter; iter = g_list_next(iter))
	{
		gems_group_info* info = (gems_group_info*)iter->data;
		gchar* group = g_strdup_printf("%d",info->id);
		add_to_grouplist(GTK_WIDGET (grouplist),group);
		g_free(group);
	}
	
}

gint updateloop()
{
	//check if there are another jammo running?
	gboolean is_another_jammo_running = FALSE;
	
	// get list of jammos
	GList* jammo_list =gems_list_jammo_connections();
	if (g_list_length(jammo_list)>0) {
		gems_peer_profile* prof = NULL;
		GtkTreeIter iter;
		GtkTreeModel *model;
		guint32 oldid;

		// there are jammos
		is_another_jammo_running = TRUE;
		//Check if list has changed
		GList* iter_newlist = NULL;
		if (g_list_length(jammo_list) != g_list_length(connections))
		{
			printf("jammo_list: %d, connections: %d\n",g_list_length(jammo_list), g_list_length(connections));
			list_users();
			g_list_free(jammo_list);
			return TRUE;
		}
;

		model = gtk_tree_view_get_model(GTK_TREE_VIEW(userlist));
		gtk_tree_model_get_iter_first (model, &iter);
		for(iter_newlist = g_list_first(jammo_list); iter_newlist; iter_newlist = g_list_next(iter_newlist))
		{
			
			prof = gems_get_profile((gems_connection*)iter_newlist->data); // Get profile


    			gtk_tree_model_get(model, &iter, ID_COLUMN, &oldid,  -1);
			
			if (oldid != gems_profile_manager_get_userid(prof) && prof)
			{
				list_users();
				g_list_free(jammo_list);
				return TRUE;
			}
			gtk_tree_model_iter_next (model, &iter);
		}
	}
	else {
		// no jammos
		is_another_jammo_running = FALSE;
		gtk_list_store_clear(GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(userlist))));
	}
	g_list_free(jammo_list);

	list_groups();
	return TRUE;
}




static void init_user_list(GtkWidget *list)
{

  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  GtkTreeViewColumn *column2;
  GtkTreeViewColumn *column3;
  GtkListStore *store;

  renderer = gtk_cell_renderer_text_new();
  column = gtk_tree_view_column_new_with_attributes("List Items",
          renderer, "text", LIST_ITEM, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(list), column);

  renderer = gtk_cell_renderer_text_new();
  column2 = gtk_tree_view_column_new_with_attributes("IDs",
          renderer, "text", ID_COLUMN, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(list), column2);

  renderer = gtk_cell_renderer_text_new();
  column3 = gtk_tree_view_column_new_with_attributes("Lock",
          renderer, "text", LOCK_COLUMN, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(list), column3);

  

  store = gtk_list_store_new(N_COLUMNS, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING);

  gtk_tree_view_set_model(GTK_TREE_VIEW(list), 
      GTK_TREE_MODEL(store));

  g_object_unref(store);
}

static void add_to_userlist(GtkWidget *list, const gchar *str, guint32 id, gboolean locked)
{
  GtkListStore *store;
  GtkTreeIter iter;
  char* lockinfo;

  if(locked) lockinfo = g_strdup_printf("*");
  else lockinfo = g_strdup_printf(" ");

  store = GTK_LIST_STORE(gtk_tree_view_get_model
      (GTK_TREE_VIEW(list)));

  gtk_list_store_append(store, &iter);
  gtk_list_store_set(store, &iter, LIST_ITEM, str, ID_COLUMN, id,LOCK_COLUMN, lockinfo, -1);
}

static void add_to_grouplist(GtkWidget *list, const gchar *str)
{
  GtkListStore *store;
  GtkTreeIter iter;

  store = GTK_LIST_STORE(gtk_tree_view_get_model
      (GTK_TREE_VIEW(list)));

  gtk_list_store_append(store, &iter);
  gtk_list_store_set(store, &iter, LIST_ITEM, str, -1);
}

static void create_group(GtkWidget *widget)
{
	GtkTreeIter iter;
	gems_group_info* newgroup;
	GList* selections = NULL;
	GtkTreeModel *model;
	guint32 ownerid, otherid;
	gems_message* msg;
	gems_connection* element;
	char* logmsg;
	gchar* song_info_data; 

	selections = gtk_tree_selection_get_selected_rows(gtk_tree_view_get_selection
(GTK_TREE_VIEW(userlist)), &model);

	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radiobutton_pair)))//Pair game
	{
	  	if (g_list_length(selections)>1) {//Create pair game
			//Get first used id
			gtk_tree_model_get_iter(model, &iter,g_list_first(selections)->data);
	    		gtk_tree_model_get(model, &iter, ID_COLUMN, &ownerid,  -1);
			//Get second used id g_list_next
			gtk_tree_model_get_iter(model, &iter,g_list_next(selections)->data);		
			gtk_tree_model_get(model, &iter, ID_COLUMN, &otherid,  -1);

			//Create group and add the two users
			GRand* grand = g_rand_new_with_seed(time(NULL));
			gint theme = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(themeselection));
			newgroup = gems_new_group_info(g_rand_int(grand), ownerid, GROUP_TYPE_PAIR, 2, 0, theme, 0);
			g_rand_free(grand);
			newgroup->peers[0] = otherid;

			printf("create_group: creating group %d for users %d and %d\n",newgroup->id,ownerid, otherid);
			print_group(newgroup);

			//send group information to users
			msg = gems_create_message_group_management_group_info_teacher(FORCE_GROUP, newgroup, 1);
			element = gems_communication_get_connection_with_userid(gems_get_data()->communication->connections, ownerid);
			if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
			{
				logmsg = g_strdup_printf("create_group: cannot send CURRENT_GROUP message to user %d",ownerid);
				cem_add_to_log(logmsg,J_LOG_ERROR);
				g_free(logmsg);
			}

			gems_clear_message(msg);
			newgroup->peers[0] = ownerid;
			msg = gems_create_message_group_management_group_info_teacher(FORCE_GROUP, newgroup, 1);
			element = gems_communication_get_connection_with_userid(gems_get_data()->communication->connections, otherid);
			if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
			{
				logmsg = g_strdup_printf("create_group: cannot send CURRENT_GROUP message to user %d",otherid);
				cem_add_to_log(logmsg,J_LOG_ERROR);
				g_free(logmsg);
			}
			gems_clear_message(msg);

			//Send song info to users
			//song_info_data = GEMS_EMPTY_SONG_INFO;
			song_info_data = g_strdup_printf("%d%d testing song info\n",1,1);
			msg = gems_create_message_collaboration_action_song_info(SONG_INFO, song_info_data);
			element = gems_communication_get_connection_with_userid(gems_get_data()->communication->connections, ownerid);
			if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
			{
				logmsg = g_strdup_printf("create_group: cannot send SONG_INFO message to user %d",ownerid);
				cem_add_to_log(logmsg,J_LOG_ERROR);
				g_free(logmsg);
			}
			element = gems_communication_get_connection_with_userid(gems_get_data()->communication->connections, otherid);
			if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
			{
				logmsg = g_strdup_printf("create_group: cannot send SONG_INFO message to user %d",otherid);
				cem_add_to_log(logmsg,J_LOG_ERROR);
				g_free(logmsg);
			}
			gems_clear_message(msg);
			//add_to_grouplist(grouplist,"Newgroup");
		}
	}
	else //Group game
	{
		gint player_ids[4];
		GList* listiter = NULL;
		gint no_players = g_list_length(selections);
		
		//Get ids
		listiter=g_list_first(selections);
		for(int i=0;i<no_players;i++)
		{
			gtk_tree_model_get_iter(model, &iter,listiter->data);
	    		gtk_tree_model_get(model, &iter, ID_COLUMN, &player_ids[i],  -1);

			listiter = g_list_next(listiter);
		}

		printf("Creating group game for %d players\n", no_players);
		ownerid= player_ids[0];

		//Create group and add the users
		GRand* grand = g_rand_new_with_seed(time(NULL));
		newgroup = gems_new_group_info(g_rand_int(grand), ownerid, GROUP_TYPE_WS_NON_RT, no_players, 0, 1, 0);
		g_rand_free(grand);

		//Create message and send to group owner
		for(int i=1;i<no_players;i++)
		{
			newgroup->peers[i-1] =player_ids[i];
			printf("player[%i] = %i\n",i,player_ids[i]);
		}
		//newgroup->peers[0] =1002;
		//newgroup->peers[1] =12346;
		//newgroup->peers[2] =12347;
		msg = gems_create_message_group_management_group_info_teacher(FORCE_GROUP, newgroup, 0);
		element = gems_communication_get_connection_with_userid(gems_get_data()->communication->connections, ownerid);
		if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
		{
			logmsg = g_strdup_printf("create_group: cannot send CURRENT_GROUP message to user %d",ownerid);
			cem_add_to_log(logmsg,J_LOG_ERROR);
			g_free(logmsg);
		}
		gems_clear_message(msg);
		//Messages for other users
		for(int i=1;i<no_players;i++)
		{
			int p=0;
			for(int j=0;j<no_players;j++)
			{
				if(i!=j)
				{
					newgroup->peers[p] =player_ids[j];
					printf("player[%i] = %i\n",p,player_ids[j]);					
					p++;
				}
			}
		

			msg = gems_create_message_group_management_group_info_teacher(FORCE_GROUP, newgroup, i);
			element = gems_communication_get_connection_with_userid(gems_get_data()->communication->connections, player_ids[i]);
			if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
			{
				logmsg = g_strdup_printf("create_group: cannot send CURRENT_GROUP message to user %d",player_ids[i]);
				cem_add_to_log(logmsg,J_LOG_ERROR);
				g_free(logmsg);
			}
			gems_clear_message(msg);
		}

	}
	gtk_widget_hide( group_window );
	
}

static void lock_device(GtkWidget *widget)
{
	GtkTreeIter treeiter;
	GtkTreeSelection *selection;
	GList* iter = NULL;
	GList* selections = NULL;
	GtkTreeModel *model;
	guint32 selected_id;
	gems_message* msg;
	gems_connection* element;
	char* logmsg;

	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(userlist));
	if (selection)
	{
		selections = gtk_tree_selection_get_selected_rows(selection, &model);
		if(selections)
		{
			msg = gems_create_message_control_mode(1,1,1);
			for(iter = g_list_first(selections); iter ; iter = g_list_next(iter))
			{
				//Get user id
				gtk_tree_model_get_iter(model, &treeiter,iter->data);
		    		gtk_tree_model_get(model, &treeiter, ID_COLUMN, &selected_id,  -1);

			

				element = gems_communication_get_connection_with_userid(gems_get_data()->communication->connections, selected_id);
				if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
				{
					logmsg = g_strdup_printf("lock device: cannot send message to user %d",selected_id);
					cem_add_to_log(logmsg,J_LOG_ERROR);
					g_free(logmsg);
				}
				
			}
			gems_clear_message(msg);
		}
	}

}


static void set_profile(GtkWidget *widget)
{
	jammo_profile_container* profile_container;
	guint32 userid, avatarid;
	guint16 age;
	gchar *username, *password, *authlevel, *firstname, *lastname;
	gems_message* msg;
	gems_connection* element = NULL;
	GList* selections = NULL;
	GtkTreeIter treeiter;
	GtkTreeModel *model;
	guint32 selected_id;
	char* logmsg;

	userid = atoi(gtk_entry_get_text (GTK_ENTRY(user_id_entry)));
	avatarid = atoi(gtk_entry_get_text (GTK_ENTRY(avatar_id_entry)));
	age = atoi(gtk_entry_get_text (GTK_ENTRY(age_entry)));
	username = g_strdup(gtk_entry_get_text (GTK_ENTRY(user_name_entry)));
	password = g_strdup(gtk_entry_get_text (GTK_ENTRY(password_entry1)));
	authlevel = g_strdup(gtk_entry_get_text (GTK_ENTRY(auth_level_entry)));
	firstname = g_strdup(gtk_entry_get_text (GTK_ENTRY(first_name_entry)));
	lastname = g_strdup(gtk_entry_get_text (GTK_ENTRY(last_name_entry)));

	profile_container = gems_teacher_server_create_profile(PROF_TYPE_REMOTE,userid, username, password, authlevel, firstname,lastname, age, avatarid);
	selections = gtk_tree_selection_get_selected_rows(gtk_tree_view_get_selection
(GTK_TREE_VIEW(userlist)), &model);


	
	if(selections) 
	{
		

		//Profile info
		msg = gems_create_message_teacher_service_profile_reply(profile_container->encrypted, profile_container->encrypted_length, username);

		gtk_tree_model_get_iter(model, &treeiter,g_list_first(selections)->data);
		gtk_tree_model_get(model, &treeiter, ID_COLUMN, &selected_id,  -1);

		if(selected_id !=0) //Not NULL profile
			element = gems_communication_get_connection_with_userid(gems_get_data()->communication->connections, selected_id);
		else //NULL profile - send to first user without profile
		{
			GList* iter = NULL;
			gems_peer_profile* prof = NULL;
			for(iter = g_list_first(connections); iter; iter = g_list_next(iter))
			{
				prof = gems_get_profile((gems_connection*)iter->data); // Get profile
			
				if(prof == NULL) 
				{
					element = (gems_connection*)iter->data;	
					break;
				}
			}
		}
		if(element != NULL)
		{

			if(gems_communication_write_encrypted_data(JAMMO_PACKET_ADMIN, element, msg) == FALSE)
			{
				logmsg = g_strdup_printf("lock device: cannot send message to user %d",selected_id);
				cem_add_to_log(logmsg,J_LOG_ERROR);
				g_free(logmsg);
			}
			gems_clear_message(msg);

			//Set new profile into login profile
			msg = gems_create_message_control_set_login_name(username);
			if(gems_communication_write_encrypted_data(JAMMO_PACKET_ADMIN, element, msg) == FALSE)
			{
				logmsg = g_strdup_printf("lock device: cannot send message to user %d",selected_id);
				cem_add_to_log(logmsg,J_LOG_ERROR);
				g_free(logmsg);
			}
			gems_clear_message(msg);

			//Login with new profile
			msg = gems_create_message_control_force_login(password, strlen(password));
			if(gems_communication_write_encrypted_data(JAMMO_PACKET_ADMIN, element, msg) == FALSE)
			{
				logmsg = g_strdup_printf("lock device: cannot send message to user %d",selected_id);
				cem_add_to_log(logmsg,J_LOG_ERROR);
				g_free(logmsg);
			}
			gems_clear_message(msg);


			if (element->profile == NULL)
			{
				element->profile = gems_new_peer_profile(username, userid, age, avatarid);
				logmsg = g_strdup_printf("Set profile to unknown user");
				cem_add_to_log(logmsg,J_LOG_DEBUG);
				g_free(logmsg);
			}
			else
			{
				gems_peer_profile_set_values(element->profile, username, userid, age, avatarid);
				logmsg = g_strdup_printf("Set profile to old user");
				cem_add_to_log(logmsg,J_LOG_DEBUG);
				g_free(logmsg);
			}
		}	
	}

	g_free(username);
	g_free(password);
	g_free(authlevel);
	g_free(firstname);
	g_free(lastname);
}

static void set_all_profiles(GtkWidget *widget)
{
	jammo_profile_container* profile_container;
	guint32 userid, avatarid;
	guint16 age;
	gchar *username, *password, *authlevel, *firstname, *lastname;
	gems_message* msg;
	gems_connection* element = NULL;
	//GList* selections = NULL;
	//GtkTreeIter treeiter;
	//GtkTreeModel *model;
	guint32 i;
	char* logmsg;



	//Send to all users without a profile
	GList* iter = NULL;
	gems_peer_profile* prof = NULL;
	i=1;
	for(iter = g_list_first(connections); iter; iter = g_list_next(iter))
	{
		prof = gems_get_profile((gems_connection*)iter->data); // Get profile
		
		if(prof == NULL) 
		{
			element = (gems_connection*)iter->data;	
			
			GRand* grand = g_rand_new_with_seed(time(NULL)+i);
			userid = g_rand_int(grand)%10000;
			//userid=1000+i;			
			g_rand_free(grand);
			avatarid = 1;
			age = 1;
			username = g_strdup_printf("defaultuser%d",i);
			password = g_strdup_printf("password");
			authlevel = g_strdup_printf("1");
			firstname = g_strdup_printf("default");
			lastname = g_strdup_printf("user");

			printf("Sending profile for %s\n",username);

			profile_container = gems_teacher_server_create_profile(PROF_TYPE_REMOTE,userid, username, password, authlevel, firstname,lastname, age, avatarid);
			
			//Profile info
			msg = gems_create_message_teacher_service_profile_reply(profile_container->encrypted, profile_container->encrypted_length, username);
			
			if(element != NULL)
			{

				if(gems_communication_write_encrypted_data(JAMMO_PACKET_ADMIN, element, msg) == FALSE)
				{
					logmsg = g_strdup_printf("lock device: cannot send message to all users");
					cem_add_to_log(logmsg,J_LOG_ERROR);
					g_free(logmsg);
				}
				gems_clear_message(msg);

				//Set new profile into login profile
				msg = gems_create_message_control_set_login_name(username);
				if(gems_communication_write_encrypted_data(JAMMO_PACKET_ADMIN, element, msg) == FALSE)
				{
					logmsg = g_strdup_printf("lock device: cannot send message to all users");
					cem_add_to_log(logmsg,J_LOG_ERROR);
					g_free(logmsg);
				}
				gems_clear_message(msg);

				//Login with new profile
				msg = gems_create_message_control_force_login(password, strlen(password));
				if(gems_communication_write_encrypted_data(JAMMO_PACKET_ADMIN, element, msg) == FALSE)
				{
					logmsg = g_strdup_printf("lock device: cannot send message to all users");
					cem_add_to_log(logmsg,J_LOG_ERROR);
					g_free(logmsg);
				}
				gems_clear_message(msg);


				if (element->profile == NULL)
				{
					element->profile = gems_new_peer_profile(username, userid, age, avatarid);
					logmsg = g_strdup_printf("Set profile to unknown user");
					cem_add_to_log(logmsg,J_LOG_DEBUG);
					g_free(logmsg);
				}
				else
				{
					gems_peer_profile_set_values(element->profile, username, userid, age, avatarid);
					logmsg = g_strdup_printf("Set profile to old user");
					cem_add_to_log(logmsg,J_LOG_DEBUG);
					g_free(logmsg);
				}
			}

			i++;
			g_free(username);
			g_free(password);
			g_free(authlevel);
			g_free(firstname);
			g_free(lastname);
		}
	}

}

static void get_profile(GtkWidget *widget)
{
	gems_message* msg;
	gems_connection* element;
	GList* selections = NULL;
	GtkTreeIter treeiter;
	GtkTreeModel *model;
	guint32 selected_id;
	char* logmsg;


	selections = gtk_tree_selection_get_selected_rows(gtk_tree_view_get_selection
(GTK_TREE_VIEW(userlist)), &model);

	
	if(selections)
	{

		msg = gems_create_message_profile_request(GET_PROFILE_FULL);

		gtk_tree_model_get_iter(model, &treeiter,g_list_first(selections)->data);
		gtk_tree_model_get(model, &treeiter, ID_COLUMN, &selected_id,  -1);
		element = gems_communication_get_connection_with_userid(gems_get_data()->communication->connections, selected_id);
		if(gems_communication_write_encrypted_data(JAMMO_PACKET_ADMIN, element, msg) == FALSE)
		{
			logmsg = g_strdup_printf("lock device: cannot send message to user %d",selected_id);
			cem_add_to_log(logmsg,J_LOG_ERROR);
			
		}
		else
		{
			logmsg = g_strdup_printf("Sending GET_PROFILE_FULL to %d",selected_id);
			cem_add_to_log(logmsg,J_LOG_DEBUG);
		}
		g_free(logmsg);
		gems_clear_message(msg);
	}
}


void  users_on_changed(GtkWidget *widget, gpointer label) 
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  guint32 value;
  GList* selections = NULL;

  selections = gtk_tree_selection_get_selected_rows(GTK_TREE_SELECTION(widget), &model);
  if (selections) {
    gtk_tree_model_get_iter(model, &iter,g_list_first(selections)->data);

    gtk_tree_model_get(model, &iter, ID_COLUMN, &value,  -1);
    
    gtk_label_set_text(GTK_LABEL(label),g_strdup_printf("%d",value));
    printlog(logview, value);
    
  }
  g_list_foreach (selections, (GFunc) gtk_tree_path_free, NULL);
  g_list_free (selections);
  
}
void  groups_on_changed(GtkWidget *widget, gpointer label) 
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  char *value;
  GList* selections = NULL;

  selections = gtk_tree_selection_get_selected_rows(GTK_TREE_SELECTION(widget), &model);
  if (selections) {
    gtk_tree_model_get_iter(model, &iter,g_list_first(selections)->data);
    gtk_tree_model_get(model, &iter, LIST_ITEM, &value,  -1);
    gtk_label_set_text(GTK_LABEL(label), value);
    g_free(value);
  }
  g_list_foreach (selections, (GFunc) gtk_tree_path_free, NULL);
  g_list_free (selections);

}

void  gametype_on_changed(GtkWidget *widget, gpointer data) 
{
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radiobutton_pair)))//Pair game
	{
		gtk_label_set_text(GTK_LABEL(themelabel), "Theme");
		gtk_widget_show(GTK_WIDGET(themeselection));
		gtk_spin_button_set_range(GTK_SPIN_BUTTON(themeselection),1,3);
		gtk_spin_button_set_increments(GTK_SPIN_BUTTON(themeselection),1,1);

	}
	else
	{
		gtk_label_set_text(GTK_LABEL(themelabel), " ");
		gtk_widget_hide(GTK_WIDGET(themeselection));
		//gtk_spin_button_set_range(GTK_SPIN_BUTTON(themeselection),1,4);
		//gtk_spin_button_set_increments(GTK_SPIN_BUTTON(themeselection),1,1);
	}
}



static void edit_profile(GtkWidget *widget)
{
	gtk_widget_show( profile_window );
}

static void open_group_editor(GtkWidget *widget)
{
	gtk_widget_show( group_window );
	gametype_on_changed(widget, NULL); 
}

gint close_main_window()
{
	gtk_main_quit();
	return(TRUE);
}

gint close_group_window()
{
	gtk_widget_hide( group_window );
	return(TRUE);
}

gint close_profile_window()
{
	gtk_widget_hide( profile_window );
	return(TRUE);
}
int main( int    argc,
      char **argv )
{
    	GtkBuilder *builder;
    	GError     *error = NULL;
	GtkTreeSelection *userselection; 
	GtkTreeSelection *groupselection; 
	gchar* logmsg = NULL;
	gint serviceport = 0;

	gems_components* components = gems_get_data();

	
 
	cem_set_log_level(J_LOG_DEBUG +J_LOG_INFO +J_LOG_FATAL +J_LOG_ERROR +J_LOG_WARN  +J_LOG_USER_ACTION +J_LOG_NETWORK +J_LOG_NETWORK_DEBUG);

	gchar* message = g_strdup_printf("JamMo teacher server-%s started",VERSION);


	cem_add_to_log(message, J_LOG_INFO);

	

	GRand* grand = g_rand_new_with_seed(time(NULL));
	//jammo_gems_init_no_services(argc, argv);
	jammo_gems_init(argc, argv);
	gems_profile_manager_login(g_rand_int(grand),NULL,NULL);
	printf("Selected random ID = %u\n",gems_profile_manager_get_userid(NULL));
	g_rand_free(grand);
	
	

	gems_enable_network_timeout_functions();

	components->service_mentor = gems_service_mentor_init();


	
	// Create Mentor service
	if(components->ph!=NULL)
	{
		if((serviceport = ph_c_register_service(components->ph,MENTOR_SERVICE_NAME,"application:jammo,method:mentor")) != 0)
		{
			components->service_mentor->enabled = TRUE; //enabled
			components->service_mentor->port = serviceport;
			logmsg = g_strdup_printf("gems_networking_register_services: registered %s service.",MENTOR_SERVICE_NAME);
		}
		else
		{
			components->service_mentor->enabled = FALSE;
			logmsg = g_strdup_printf("gems_networking_register_services: cannot register %s service.",MENTOR_SERVICE_NAME);
		}
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}

	g_timeout_add_full(G_PRIORITY_DEFAULT_IDLE,200,(GSourceFunc)updateloop, NULL, NULL);
	g_source_remove(gems_get_data()->timeout_functions[2]);//Stop searching for jammos

    	/* Init GTK+ */
    	gtk_init( &argc, &argv );
 
    	/* Create new GtkBuilder object */
    	builder = gtk_builder_new();
    	/* Load UI from file. If error occurs, report it and quit application.
     	* Replace "tut.glade" with your saved project. */
    	if( ! gtk_builder_add_from_file( builder, "data/teacher-jammo/teacher_gui.glade", &error ) )
    	{
        	g_warning( "%s", error->message );
        	g_free( error );
        	return( 1 );
    	}
 
    	/* Get main window pointer from UI */
   	mainwindow = GTK_WIDGET( gtk_builder_get_object( builder, "window1" ) );
	profile_window = GTK_WIDGET( gtk_builder_get_object( builder, "window2" ) );
	group_window = GTK_WIDGET( gtk_builder_get_object( builder, "window3" ) );
 
    	/* Connect signals */
    	gtk_builder_connect_signals( builder, NULL );
	
	//Window close buttons
	g_signal_connect(mainwindow, "destroy", G_CALLBACK(close_main_window), NULL);
	g_signal_connect(group_window, "destroy", G_CALLBACK(close_group_window), NULL);
	g_signal_connect(profile_window, "destroy", G_CALLBACK(close_profile_window), NULL);

	g_signal_connect(gtk_builder_get_object( builder, "create_group_button" ), "clicked", G_CALLBACK(open_group_editor), NULL);
	g_signal_connect(gtk_builder_get_object( builder, "create_group_confirmation" ), "clicked", G_CALLBACK(create_group), NULL);
	g_signal_connect(gtk_builder_get_object( builder, "edit_profile_button" ), "clicked", G_CALLBACK(edit_profile), NULL);
	g_signal_connect(gtk_builder_get_object( builder, "set_profile_button" ), "clicked", G_CALLBACK(set_profile), NULL);
	g_signal_connect(gtk_builder_get_object( builder, "get_profile_button" ), "clicked", G_CALLBACK(get_profile), NULL);
	g_signal_connect(gtk_builder_get_object( builder, "lock_device_button" ), "clicked", G_CALLBACK(lock_device), NULL);
	g_signal_connect(gtk_builder_get_object( builder, "set_all_profiles_button" ), "clicked", G_CALLBACK(set_all_profiles), NULL);

	//Profile editor entrys
	user_id_entry = GTK_WIDGET(gtk_builder_get_object( builder, "user_id_entry" ));
	user_name_entry = GTK_WIDGET(gtk_builder_get_object( builder, "user_name_entry" ));
	password_entry1 = GTK_WIDGET(gtk_builder_get_object( builder, "password_entry1" ));
	password_entry2 = GTK_WIDGET(gtk_builder_get_object( builder, "password_entry2" ));
	auth_level_entry = GTK_WIDGET(gtk_builder_get_object( builder, "auth_level_entry" ));
	first_name_entry = GTK_WIDGET(gtk_builder_get_object( builder, "first_name_entry" ));
	last_name_entry = GTK_WIDGET(gtk_builder_get_object( builder, "last_name_entry" ));
	age_entry = GTK_WIDGET(gtk_builder_get_object( builder, "age_entry" ));
	avatar_id_entry = GTK_WIDGET(gtk_builder_get_object( builder, "avatar_id_entry" ));

	//Group creator entrys
	radiobutton_pair = GTK_WIDGET(gtk_builder_get_object( builder, "radiobutton_pair" ));
	radiobutton_group = GTK_WIDGET(gtk_builder_get_object( builder, "radiobutton_group" ));
	themeselection = GTK_WIDGET(gtk_builder_get_object( builder, "themeselection" ));
	themelabel = GTK_WIDGET(gtk_builder_get_object( builder, "themelabel" ));

	//Treeviews
	userlist = GTK_WIDGET(gtk_builder_get_object( builder, "treeview1" ));
    	grouplist = GTK_WIDGET(gtk_builder_get_object( builder, "treeview2" ));


	userselection = gtk_tree_view_get_selection(GTK_TREE_VIEW(userlist));

  	g_signal_connect(userselection, "changed", G_CALLBACK(users_on_changed), gtk_builder_get_object( builder, "userlabel" ));

	groupselection = gtk_tree_view_get_selection(GTK_TREE_VIEW(grouplist));

  	g_signal_connect(groupselection, "changed", G_CALLBACK(groups_on_changed), gtk_builder_get_object( builder, "grouplabel" ));

	g_signal_connect(radiobutton_pair, "toggled", G_CALLBACK(gametype_on_changed), NULL);
	g_signal_connect(radiobutton_group, "toggled", G_CALLBACK(gametype_on_changed), NULL);
	

 	logview = GTK_WIDGET(gtk_builder_get_object( builder, "textview1" ));

	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW (userlist), FALSE);
	init_user_list(userlist);
	gtk_tree_selection_set_mode (gtk_tree_view_get_selection(GTK_TREE_VIEW (userlist)), GTK_SELECTION_MULTIPLE); 

	//add_to_userlist(userlist, "First",1, FALSE);
  	//add_to_userlist(userlist, "Second",2, FALSE);
  	//add_to_userlist(userlist, "Third",3, TRUE);

	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW (grouplist), FALSE);
	init_user_list(grouplist);

	//Set profile service callback for incoming profiles
	gems_service_profile_set_callback_profile(callback_new_profile);

	profile_database = gems_teacher_server_new_profile_data_container();
	profile_database = load_database(profile_database);

	/* Destroy builder, since we don't need it anymore */
    	g_object_unref( G_OBJECT( builder ) );
 
    	/* Show window. All other widgets are automatically shown by GtkBuilder */
    	gtk_widget_show( mainwindow );
 
    	/* Start main loop */
    	gtk_main();

	gems_profile_manager_logout();
	
	configure_release_directories();

#ifdef NETWORKING_ENABLED
	/* GEMS cleanup */
	jammo_gems_cleanup();
#endif

	jammo_cem_cleanup();
 
    	return( 0 );
}
