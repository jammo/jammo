/*
 * gems_teacher_helper_functions.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 * 	    Tommi Kallonen <tommi.kallonen@lut.fi>
 */
#include "../gems/gems_teacher_server_utils.h"
#include "../cem/cem.h"


teacher_server_profile_data* load_database(teacher_server_profile_data* data)
{
	// Read profile db
	switch(gems_teacher_server_read_profile_database(&data))
	{
		case READ_OK:
			cem_add_to_log("Teacher database read success!",J_LOG_INFO);
			break;
		case READ_FAIL_FILE_NOT_EXIST:
			cem_add_to_log("Teacher database read failure! No database!",J_LOG_INFO);
			// Create new if db does not exist
			data = gems_teacher_server_new_profile_data_container();
			return data;
		default:
			cem_add_to_log("Teacher database read failure!",J_LOG_INFO);
			return NULL;
	}
	
	// Decrypt data with password, TODO: clear database if cannot decrypt!
	if(gems_teacher_server_decrypt_profiles(data,"mypassword")) cem_add_to_log("Teacher database decryption success!",J_LOG_INFO);
	else cem_add_to_log("Teacher database decryption failure!",J_LOG_INFO);
	
	return data;
}

// Encrypt profiles and save them, TODO: react to errors
void save_database(teacher_server_profile_data* data)
{
	// Encrypt profiles (all!)
	if(gems_teacher_server_encrypt_profiles(data, "mypassword")) cem_add_to_log("Teacher database encryption success!", J_LOG_INFO);
	else cem_add_to_log("Teacher database encryption failure!", J_LOG_INFO);
	
	if(gems_teacher_server_write_profile_database(data) == WRITE_OK) cem_add_to_log("Teacher database saving success!", J_LOG_INFO);
	else cem_add_to_log("Teacher database saving failure!", J_LOG_INFO);
}

