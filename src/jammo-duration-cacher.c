
#include <meam/jammo-meam.h>
#include <meam/jammo-sample.h>

static GMainLoop* main_loop;
static gchar** filename;

static void set_next_filename(JammoSample* sample) {
	guint64 duration = 0;
	gchar* s;
	
	while (*filename && duration != JAMMO_DURATION_INVALID) {
		if (sample) {
			g_object_unref(sample);
		}
			
		g_printerr("%s...\n", *filename);

		s = g_strdup(*filename);
		sample = jammo_sample_new_from_file(s);
		g_free(s);
		if ((duration = jammo_sample_get_duration(sample)) == JAMMO_DURATION_INVALID) {
			g_signal_connect(sample, "notify::duration", G_CALLBACK(set_next_filename), NULL);
		}
		filename++;
	}
	
	if (!*filename && duration != JAMMO_DURATION_INVALID) {
		g_main_loop_quit(main_loop);
	}
}

int main(int argc, char** argv) {
	gchar* duration_cache_filename;
	gchar* current_dir;

	main_loop = g_main_loop_new (NULL, FALSE);
	
	if (argc < 3) {
		g_printerr("Usage: %s <duration_cache_filename> <audio_filename>...\n", (argc > 0 ? argv[0] : "jammo-duration-cacher"));
		
		return 1;
	}
	
	if (argv[1][0] == '/') {
		duration_cache_filename = g_strdup(argv[1]);
	} else {
		current_dir = g_get_current_dir();
		duration_cache_filename = g_build_filename(current_dir, argv[1], NULL);
		g_free(current_dir);
	} 

	jammo_meam_init(&argc, &argv, duration_cache_filename);

	filename = &argv[2];	
	set_next_filename(NULL);

	g_main_loop_run(main_loop);
	
	jammo_meam_cleanup();

	return 0;
}
