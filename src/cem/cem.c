/** cem.c is part of JamMo.
License: GPLv2, read more from COPYING

(from D2.4)
CEM is started as JamMo is started as it is used to log user actions in
all use cases. Mentor and games components only need to be started if
the use case is correct.
*/
#include <stdio.h>
#include "cem.h"
#include <glib.h>
#include <sys/timeb.h>
#include <string.h>

#include "../configure.h"

static int log_level=J_LOG_NONE; //Default.
static FILE *log_file = NULL;
static gboolean also_to_stdout = FALSE; //Write to file AND to STDOUT
void (*gems_callback_send_log_message_to_teacher)(char *message,int type)=NULL;

void cem_set_log_level(int new_level) {
	log_level=new_level;
	gchar* message = g_strdup_printf("Setting logging level to %d ",new_level);
	cem_add_to_log(message,J_LOG_DEBUG);
	g_free(message);
}

void cem_append_to_log_level(int level){
	int old_log_level = log_level;
	if (!(level & log_level)) //Not yet, so add
		log_level+=level;

	gchar* message = g_strdup_printf("Add '%d' to log_level %d. New log_level: %d \n",level, old_log_level, log_level);
	cem_add_to_log(message,J_LOG_DEBUG);
	g_free(message);
}

void cem_print_also_to_stdout(gboolean b) {
	also_to_stdout=b;
}

static gchar* type_to_word(int type) {
GString* string = g_string_new("");

if (type & J_LOG_DEBUG)
	string = g_string_append(string,"DEBUG         ");
if (type & J_LOG_INFO)
	string = g_string_append(string,"INFO          ");
if (type & J_LOG_FATAL)
	string = g_string_append(string,"FATAL         ");
if (type & J_LOG_ERROR)
	string = g_string_append(string,"ERROR         ");
if (type & J_LOG_WARN)
	string = g_string_append(string,"WARN          ");
if (type & J_LOG_USER_ACTION)
	string = g_string_append(string,"USER          ");
if (type & J_LOG_NETWORK)
	string = g_string_append(string,"NETWORK       ");
if (type & J_LOG_NETWORK_DEBUG)
	string = g_string_append(string,"NETWORK_DEBUG ");
if (type & J_LOG_MENTOR_SPEECH)
	string = g_string_append(string,"MENTOR_SPEECH ");

return g_string_free (string, FALSE);
}


void cem_get_time(char timestamp_now[]) {
	time_t rawtime;
	struct tm * timeinfo;
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	strftime (timestamp_now,80,"%Y.%m.%d_%H.%M.%S",timeinfo);
}


/*SMPTE
[HH:MM:SS:FF],FF is frames at 25fps
*/
static void cem_get_SMPTE_time(char timestamp_now[]) {
	time_t rawtime;
	struct tm * timeinfo;
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );

	struct timeb tp;
	ftime(&tp);
	//printf("time = %ld.%d\n",tp.time,tp.millitm);

	strftime (timestamp_now,80,"%H:%M:%S:",timeinfo);
	int millis = tp.millitm;

	//25fps -> we want value between 0...24
	//1000/25=40
	int fps = millis / 40;

	//timestamp_now contains "HH:MM:SS:" so we want write at index 9.
	if (fps<10)
		snprintf(&timestamp_now[9], 3, "0%d", fps);
	else
		snprintf(&timestamp_now[9], 3, "%d", fps);
}

/*
No need to add linebreaks.
Usage:
cem_add_to_log("error_message",J_LOG_ERROR);
or
cem_add_to_log("fatal+info_message",J_LOG_FATAL+J_LOG_INFO);
*/
void cem_add_to_log(const char* message,int type){
	gchar* name;
	char timestamp [80];
	cem_get_time(timestamp);

	//Initialize if NULL
	if (log_file==NULL){
		gchar* outputFilename;
		const gchar* log_directory=configure_get_log_directory();
		if (log_directory==NULL) //case: using CEM before configure is initialized (this is error case)
			configure_init_directories(TRUE);

		//check jammo_directory again
		log_directory=configure_get_log_directory();
		//if configure success
		if (log_directory) {
			outputFilename = g_strdup_printf("%s/%s.log",log_directory,timestamp);
			log_file = fopen(outputFilename, "a");
			if (log_file==NULL)
				printf("ERROR: CEM can't log to file '%s'\n",outputFilename);
		}
		else {
			outputFilename = g_strdup_printf(" "); //This is not used, but we want free it in every case
			printf("ERROR: CEM can't get jammo_directory\n");
		}

		if (log_file){
			//This will be very first row on log file (if first cem_add_to_log-call is set_log_level)
			gchar* msg = g_strdup_printf("Log started on file: '%s' ",outputFilename);
			cem_add_to_log(msg,J_LOG_INFO);
			g_free(msg);
		}

		g_free(outputFilename);
	}

	if (type & log_level) { //We are interested in this type of log-message
		name=type_to_word(type);
		char SMPTE_timestamp [12];
		cem_get_SMPTE_time(SMPTE_timestamp);

		gchar* final = g_strdup_printf("[%s] %s: \"%s\"\n",SMPTE_timestamp, name,message);

		//If user explicitely wants to print to STDOUT. Or if there are something wrong with log_file.
		if (also_to_stdout || !log_file){
			printf("%s",final);
		}
		//Write to log_file if file is working.
		if (log_file) {
			fprintf(log_file,"%s",final);
			fflush(log_file);
		}
		if(gems_callback_send_log_message_to_teacher!=NULL)//Also send log message to teacher
		{
			gems_callback_send_log_message_to_teacher(final, type);
		}

		g_free(name);
		g_free(final);
	}

}

void jammo_cem_cleanup() {
cem_add_to_log("Stopping logging",J_LOG_DEBUG);
if (log_file)
	fclose(log_file);
}

void cem_set_callback_send_log_message_to_teacher(void (*func_ptr)(char *message,int type) ) {
	gems_callback_send_log_message_to_teacher=func_ptr;
}
