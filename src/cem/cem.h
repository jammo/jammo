/** cem.h is part of JamMo.
License: GPLv2, read more from COPYING
*/
#ifndef _CEM_H_
#define _CEM_H_
#include <glib.h>
//Log-message types. Influenced by log4j-type levels.
//J_LOG_TRACE   More detailed information.  //any use of this?
enum {
J_LOG_NONE          =0,   /* Can be used to empty log-level*/
J_LOG_DEBUG         =1,   /*Detailed information on the flow through the system */
J_LOG_INFO          =2,   /*Interesting runtime events (startup/shutdown) */
J_LOG_FATAL         =4,   /*Severe errors that cause premature termination */
J_LOG_ERROR         =8,   /*Other runtime errors or unexpected conditions */
J_LOG_WARN          =16,  /*'Almost' errors, other runtime situations that are undesirable or unexpected */
J_LOG_USER_ACTION   =32,  /* Everything user is doing.*/
J_LOG_NETWORK       =64,  /*Networking info, e.g. new connection establishments, game starting etc. */
J_LOG_NETWORK_DEBUG =128,  /* Networking debug */
J_LOG_MENTOR_SPEECH =256  /* All what mentor tries to speak */
};

void cem_add_to_log(const char* message, int type);
void cem_set_log_level(int new_level);
void cem_append_to_log_level(int level);
void jammo_cem_cleanup();
void cem_get_time(char timestamp_now[]);
void cem_print_also_to_stdout(gboolean b);

//Callbacks
void cem_set_callback_send_log_message_to_teacher(void (*func_ptr)(char *message,int type) );
#endif  /* _CEM_H_ */
