
#ifndef _CONFIGURE_H_
#define _CONFIGURE_H_

#include <glib.h>
void configure_init_directories(gboolean small_game);
void configure_release_directories();

const gchar* configure_get_jammo_directory();
const gchar* configure_get_singings_directory();
const gchar* configure_get_compositions_directory();

const gchar* configure_get_projects_directory();
const gchar* configure_get_finalized_directory();
const gchar* configure_get_log_directory();
const gchar* configure_get_temp_directory();
/**
 * Defines default folders.
 */

#define THEMES_DIR DATA_DIR "/themes"
#define SONGS_DIR DATA_DIR "/songs"

//These are not heavily used
#define WIDTH_OF_WINDOW 800
#define HEIGHT_OF_WINDOW 480




#define _(string)    gettext(string)

#endif /* _CONFIGURE_H_ */
