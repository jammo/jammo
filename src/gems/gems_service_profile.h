/*
 * gems_service_profile.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#ifndef __GEMS_SERVICE_PROFILE_
#define __GEMS_SERVICE_PROFILE_

#include "gems_structures.h"
#include "gems_teacher_server_utils.h"

gems_service_profile* gems_service_profile_init();
void gems_service_profile_cleanup();
void gems_service_profile_cleanup_lists();

gboolean gems_service_profile_process_request(gems_connection* element);
gboolean gems_service_profile_process_profile_upload(gems_connection* element);

//Callback for informing teacher server about incoming profile (used after profile request)
void (*teacher_server_callback_new_profile)(jammo_profile_container* prof);
void gems_service_profile_set_callback_profile(void (*func_ptr)(jammo_profile_container* prof) );

#endif
