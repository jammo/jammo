/*
 * gems_definitions.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2011 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 *	    Tommi Kallonen  <tommi.kallonen@lut.fi>
 */
 
 
#ifndef __GEMS_DEFINITIONS_
#define __GEMS_DEFINITIONS_

/**
 * GemsEventListOperation:
 * @GEMS_EVENTLIST_REPLACE:
 *
 * Event list operation types for CHUM, with these CHUM can specify what to do
 * with a midi or slider event list
 */
typedef enum
{
	GEMS_EVENTLIST_REPLACE
} GemsEventListOperation;

/**
 * GemsGroupControlCallbackType:
 * @GEMS_GROUP_CONTROL_NOT_IN_GROUP:
 * @GEMS_GROUP_CONTROL_GROUP_NOT_ACTIVE:
 * @GEMS_GROUP_CONTROL_GROUP_FULL:
 * @GEMS_GROUP_CONTROL_ALREADY_IN_GROUP:
 * @GEMS_GROUP_CONTROL_GROUP_LOCKED:
 * @GEMS_GROUP_CONTROL_GROUP_ADD_ERROR:
 * @GEMS_GROUP_CONTROL_NOT_GROUP_OWNER:
 *
 * Group control types
 */
typedef enum
{
	GEMS_GROUP_CONTROL_NOT_IN_GROUP,
	GEMS_GROUP_CONTROL_GROUP_NOT_ACTIVE,
	GEMS_GROUP_CONTROL_GROUP_FULL,
	GEMS_GROUP_CONTROL_ALREADY_IN_GROUP,
	GEMS_GROUP_CONTROL_GROUP_LOCKED,
	GEMS_GROUP_CONTROL_GROUP_ADD_ERROR,
	GEMS_GROUP_CONTROL_NOT_GROUP_OWNER
} GemsGroupControlCallbackType;

/**
 * GemsGameCallbackType
 * @GEMS_GAME_JOINED_PAIR_COMPOSITION:
 *
 * Types for game callback
 */
typedef enum
{
	GEMS_GAME_JOINED_PAIR_COMPOSITION
} GemsGameCallbackType;

#define JAMMO_NETWORK_BUFFER_MAX_SIZE 2048
// TODO change to use sha256 -> 32 bytes
#define JAMMO_MESSAGE_DIGEST_SIZE 20 // 160 bit SHA1
// If no reply received the request is being sent every X seconds
#define PROFILE_REQUEST_INTERVAL 5.00
// Group info request interval, 15 seconds 
#define GROUP_REQUEST_INTERVAL 15.00
// Group adverting interval, 3 seconds
#define GROUP_ADVERT_INTERVAL 3.00
// Limit for removing group (from last advert)
#define GROUP_REMOVE_LIMIT 4*GROUP_ADVERT_INTERVAL
// Limit for removing from rejected list - 5 min
#define REJECTED_LIST_REMOVE_LIMIT 300.00
// Limit for errorous connections
#define CONNECTION_ERRORLIMIT 5
// Basic interval for connection attempts, each error increases this by 1 second
#define CONNECTION_INTERVAL 5.00
// Idle limit for connecting devices (in jammo service verification phase)
#define IDLE_CONNECTION_LIMIT 10.00
// High and low for random connection delay
#define AC_DELAY_HIGH 1.50
#define AC_DELAY_LOW 0.10
// Max size for group
#define GROUP_MAX_SIZE 4
// Group not assigned
#define FREE_SLOT 0
// Group not active
#define NOT_ACTIVE 0
// Timer limit for teacher profile requests
#define TEACHER_PROFILE_REQUEST_LIMIT 2.00
#define TEACHER_PROFILE_REQUEST_MAX_COUNT 2

/**
 * connection_search:
 * @NOT_IN_LIST: Not found from any of the lists
 * @IN_INITIALIZED_LIST: In accepted connections list
 * @IN_JAMMO_SERVICE_LIST: In the JamMo service handshake process
 * @IN_CONNECTING_LIST: In connections initialized to other devices
 * @IN_CONNECTED_LIST: In connected list
 * @IN_REJECTED_LIST: In rejected list
 *
 * Used as return values when searching connections from various lists
 */
typedef enum {
	NOT_IN_LIST = 0,
	IN_INITIALIZED_LIST,
	IN_JAMMO_SERVICE_LIST,
	IN_CONNECTING_LIST,
	IN_CONNECTED_LIST,
	IN_REJECTED_LIST,
} connection_search;

/**
 * GemsGroupAction:
 * @ERROR_CANNOT_EXEC: Error, cannot execute the requested action
 * @ADDED_TO_GROUP: User was added to group
 * @REQUEST_SENT: Request was sent to recipient
 * @ALREADY_IN_GROUP: User already in the group
 * @CANNOT_ADD: It is not possible to add user
 * @REMOVED_OK: User succesfully removed from group
 * @NOT_IN_GROUP: User is not in group
 * @GROUP_LOCKED: Group is locked, it is not adverted nor it can be joined
 * @CANNOT_CONNECT_TO_OWNER: Cannot connect to owner or no such connection 
 * exists
 * @NO_OWNER: There is no owner set for group
 * @NO_MATCHING_GROUP: No group with given id is found
 * @GROUP_FULL: Group is full
 * @REQUEST_NOT_SENT: The request was not sent
 *
 * Group action return values. Tells the result of action.
 */
typedef enum {
	ERROR_CANNOT_EXEC = -1,
	ADDED_TO_GROUP = 0,
	REQUEST_SENT,
	ALREADY_IN_GROUP,
	CANNOT_ADD,
	REMOVED_OK,
	NOT_IN_GROUP,
	GROUP_LOCKED,
	CANNOT_CONNECT_TO_OWNER,
	NO_OWNER,
	NO_MATCHING_GROUP,
	GROUP_FULL,
	REQUEST_NOT_SENT,
} GemsGroupAction;

/**
 * GemsGroupStatus:
 * @IN_GROUP: User is in the group
 * @JOINING: User is joining to group, request is sent.
 * @LEAVING: User is leaving from group, request is sent.
 * @NONE: equals to #NOT_ACTIVE
 *
 * Group status
 */
typedef enum {
	IN_GROUP = 111,
	JOINING = 112,
	LEAVING = 113,
	NONE = NOT_ACTIVE, // NOT_ACTIVE in group = not in group
} GemsGroupStatus;

/**
 * GemsConnErrType:
 * @ERROR_CONNECTION: Connection error, connection could not be established
 * @ERROR_REJECTED: Connection was rejected because of version mismatch
 * @ERROR_ALREADY_CONNECTED: Connection rejected since a connection already
 * exists
 *
 * Error types.
 */
typedef enum {
	ERROR_CONNECTION = 21,
	ERROR_REJECTED,
	ERROR_ALREADY_CONNECTED,
} GemsConnErrType;

/* NETWORK DEFINITIONS */

#define JAMMO_PACKET_PRIVATE 10
#define JAMMO_PACKET_GROUP 11
#define JAMMO_PACKET_ADMIN 12

/*-----------  Service ID's ----------*/
#define JAMMO_SERVICE_ID 100
#define AA_SERVICE_ID 101
#define PROFILE_SERVICE_ID 102
#define GROUP_SERVICE_ID 103
#define COLLABORATION_SERVICE_ID 104
#define SONGBANK_SERVICE_ID 105
#define DATATRANSFER_SERVICE_ID 106
#define MENTOR_SERVICE_ID 107
#define CONTROL_SERVICE_ID 108
#define PROBE_SERVICE_ID 109
#define TEACHER_PROFILE_SERVICE_ID PROFILE_SERVICE_ID


/*-----------  Service Names ----------*/
#define JAMMO_SERVICE_NAME "JamMo"
#define AA_SERVICE_NAME JAMMO_SERVICE_NAME ":AuthenticationAndAuthorization"
#define PROFILE_SERVICE_NAME JAMMO_SERVICE_NAME ":Profile"
#define GROUP_SERVICE_NAME JAMMO_SERVICE_NAME ":GroupManagement"
#define COLLABORATION_SERVICE_NAME JAMMO_SERVICE_NAME ":Collaboration"
#define SONGBANK_SERVICE_NAME JAMMO_SERVICE_NAME ":Songbank"
#define DATATRANSFER_SERVICE_NAME JAMMO_SERVICE_NAME ":DataTransfer"
#define MENTOR_SERVICE_NAME JAMMO_SERVICE_NAME ":Mentor"
#define CONTROL_SERVICE_NAME JAMMO_SERVICE_NAME ":Control"
#define PROBE_SERVICE_NAME JAMMO_SERVICE_NAME ":ProbeVerification"

// Generic teacher server name
#define TEACHER_SERVICE_NAME JAMMO_SERVICE_NAME ":TeacherServer"
// Teacher profile service
#define TEACHER_PROFILE_SERVICE_NAME TEACHER_SERVICE_NAME ":Profile"



/*-----------  Command types ----------*/

// ERROR
#define ERROR_MSG 10000
#define ERROR_NO_ERROR "No error" // When error id is invalid

// JAMMO SERVICE
#define CONNECT_REQ 1001
#define VERSION_REQ 1002
#define VERSION_REPLY 1003
#define CONNECT_REPLY 1004

// AUTHENTICATION & AUTHORIZATION
#define AUTH_CONNECTION 1011
#define GET_CERTIFICATE 1012
#define PUBLISH_CERTIFICATE 1013
#define CERTIFICATE_OK 1014
#define START_PEER_KEY_EXCHANGE 1015
#define KEY_DATA 1016
#define KEY_DATA_REPLY 1017
#define KEY_OK 1018

// PROFILE SERVICE
#define GET_PROFILE 1021
#define PROFILE_PUB 1022
#define PROFILE_FULL 1023
#define GET_PROFILE_PUB 1024
#define GET_PROFILE_FULL 1025
#define REQ_ENC_PROFILE 1026
#define PROFILE_ENC 1027
#define PROFILE_SET_UNAME 1028
#define PROFILE_PASSCHANGED 1029

// GROUP SERVICE
#define REQUEST_MEMBERS 1030
#define OFFER_GROUP 1031
#define REQUEST_GROUP_INFO 1032
#define JOIN_GROUP 1033
#define MEMBER_LIST 1034
#define NEW_MEMBER 1035
#define LEAVE_GROUP 1036
#define REMOVED_FROM_GROUP 1037
#define MEMBER_DROPPED 1038
#define CURRENT_GROUP 1039
#define GROUP_IS_UNLOCKED 1028 // Unused by profile service messages
#define GROUP_IS_LOCKED 1029 // Unused by profile service messages
#define FORCE_GROUP 1065

// COLLABORATION SERVICE
#define ACTION_VI_SINGLE 1040
#define ACTION_CONFIRMED 1041
#define SONG_INFO 1042
#define ACTION_LOOP 1043
#define ACTION_LOOP_SYNC 1044
#define ACTION_MIDI 1045
#define ACTION_SLIDER 1046

#define ADD_LOOP 1048
#define REMOVE_LOOP 1049
#define REPLACE_MIDI_SERIES 1050
#define REPLACE_SLIDER_SERIES 1051

// CONTROL SERVICE
#define CONTROL_MODE 1060
#define CONTROL_OK 1061
#define CONTROL_SET_LOGIN 1062
#define CONTROL_GET_LOGIN 1063
#define CONTROL_REPLY_LOGIN 1064
#define CONTROL_FORCE_LOGIN 1065

// MENTOR SERVICE
#define LOG_MESSAGE 1070

//SONGBANK SERVICE
#define SEARCH_FILE_REQ 1080
#define SEARCH_FILE_REPLY 1081
#define	FILE_REQ 1082
#define	FILE_HEADER 1083
#define FILE_PART 1084

// Group types
#define GROUP_TYPE_PAIR 10311
#define GROUP_TYPE_CHAIN 10312
#define GROUP_TYPE_WS_NON_RT 10313
#define GROUP_TYPE_WS_RT 10314
#define GROUP_TYPE_WS_NET 10315


// Group themes
// TODO define group themes

/*-----------  ERROR MESSAGES ----------*/
#define ERROR_ALREADY_CONNECTED_TYPE 10001
#define ERROR_VERSION_MISMATCH_TYPE 10002
#define ERROR_INVALID_STATE_TYPE 10003
#define ERROR_INVALID_SERVICE_ID_TYPE 10004
#define ERROR_SERVICE_NOT_RUNNING_TYPE 10005
#define ERROR_CONNECTION_REJECTED_TYPE 10006
#define ERROR_NOT_IN_GROUP_TYPE 10007
#define ERROR_GROUP_NOT_ACTIVE_TYPE 10008
#define ERROR_INVALID_GROUP_ID_TYPE 10009
#define ERROR_GROUP_FULL_TYPE 10010
#define ERROR_ALREADY_IN_GROUP_TYPE 10011
#define ERROR_GROUP_LOCKED_TYPE 10012
#define ERROR_GROUP_ADD_ERROR_TYPE 10013
#define ERROR_NOT_GROUP_OWNER_TYPE 10014

#define ERROR_INVALID_ACCESS_TYPE 10036
#define ERROR_NO_PROFILE_FOR_USERNAME_TYPE 10037

#define ERROR_ALREADY_CONNECTED_MSG "Already connected or trying to connect."
#define ERROR_VERSION_MISMATCH_MSG "Versions mismatch."
#define ERROR_INVALID_STATE_MSG "Wrong message in current state."
#define ERROR_INVALID_SERVICE_ID_MSG "Invalid service id."
#define ERROR_SERVICE_NOT_RUNNING_MSG "Service with requested id is not running."
#define ERROR_CONNECTION_REJECTED_MSG "Connection was previously rejected."
#define ERROR_NOT_IN_GROUP_MSG "Not in group."
#define ERROR_GROUP_NOT_ACTIVE_MSG "Group is not active."
#define ERROR_INVALID_GROUP_ID_MSG "Given group id is not valid."
#define ERROR_GROUP_FULL_MSG "Group is full."
#define ERROR_ALREADY_IN_GROUP_MSG "User is already in the active group."
#define ERROR_GROUP_LOCKED_MSG "Group cannot be joined, group is locked."
#define ERROR_GROUP_ADD_ERROR_MSG "Cannot add to group."
#define ERROR_NOT_GROUP_OWNER_MSG "Not the owner of the group."

#define ERROR_INVALID_ACCESS_MSG "Not enough privileges to get full profile"
#define ERROR_NO_PROFILE_FOR_USERNAME_MSG "No profile exists for requested username."

/*----------- PROFILE MANAGER ----------*/
#define JAMMOPROFILE "profiles"
#define PROFILETYPE ".csv"
// new encrypted profile type
#define PROFILE_FILE_EXT ".jpf"

// Minimum compatible profile version
#define JAMMO_PROFILE_VERSION 1

// Profile parameters
#define PROF_PARAM_USERID "userID"
#define PROF_PARAM_USERNAME "username"
#define PROF_PARAM_AUTHLEVEL "authLevel"
#define PROF_PARAM_FIRSTNAME "firstName"
#define PROF_PARAM_LASTNAME "lastName"
#define PROF_PARAM_POINTS "gamePoints"
#define PROF_PARAM_AGE "age"
#define PROF_PARAM_AVATARID "avatarID"
// versions from 1,2,3 ... 
#define PROF_PARAM_VERSION "profileVersion"
#define PROF_PARAM_TYPE	"profileType"

// "Local" profile, only on the device
#define PROF_TYPE_LOCAL 1
// Profile that should be updated either to teacher or to community
#define PROF_TYPE_REMOTE 2

#define PROF_TOKENS 9

#define PROF_DEFAULT_AVATARID 260

//Songbank file types
#define FILE_SONGLIST 15000
#define FILE_SONG 15001
#define FILE_LOOPLIST 15002
#define FILE_LOOP 15003
#define FILE_MESSAGE 15004
#define FILE_THREAD 15005
#define FILE_QUESTIONLIST 15006
#define FILE_THREADLIST 15007
#define FILE_USER 15008
#define FILE_LABEL 15009
#define FILE_GENERAL 15010

//Songbank return values
#define FILE_FOUND 1
#define FILE_NOT_FOUND -1

/**
 * GemsEnvelopeCheck:
 * @SECURITY_OK: Envelope is valid
 * @SECURITY_CORRUPT_MESSAGE: hash value in envelope doesn't match the content
 * @SECURITY_VERIFY_FAILED: cannot verify the signature of hash
 *
 * Security - extraction of envelope return values
 */
typedef enum {
	SECURITY_OK = 10190,
	SECURITY_CORRUPT_MESSAGE = 10191, // hash value in envelope doesn't match the content
	SECURITY_VERIFY_FAILED = 10192, // cannot verify the signature of hash
} GemsEnvelopeCheck;

/**
 * GemsLoginAction:
 * @LOGIN_PROFILE_READ_ERROR: Cannot login, profile file cannot be read
 * @LOGIN_INVALID_PROFILE: Cannot login, profile for user is invalid
 * @LOGIN_NO_PROFILE: Cannot login, no such profile exists
 * @LOGIN_OK: Login success
 * @LOGIN_INVALID_PARAMS: Cannot login, given parameters are invalid
 * @LOGIN_CANNOT_INIT_PASSWD: Cannot login, password cannot be created
 * @LOGIN_CANNOT_INIT_SECURITY: Cannot login, security contexts cannot be 
 * initilialized
 * @LOGIN_INVALID_PASSWORD: Cannot login, password is invalid
 * @LOGIN_PROFILE_HASH_FAIL: Cannot login, hashing of content failed
 * @LOGIN_PROFILE_CORRUPT: Cannot login, the profile file is corrupt/unreadable
 *
 * authentication returnvalues
 */
typedef enum {
	LOGIN_PROFILE_READ_ERROR = -3,
	LOGIN_INVALID_PROFILE = -2,
	LOGIN_NO_PROFILE = -1,
	LOGIN_OK = 0,
	LOGIN_INVALID_PARAMS,
	LOGIN_CANNOT_INIT_PASSWD,
	LOGIN_CANNOT_INIT_SECURITY,
	LOGIN_INVALID_PASSWORD,
	LOGIN_PROFILE_HASH_FAIL,
	LOGIN_PROFILE_CORRUPT,
} GemsLoginAction;

/**
 * GemsChangeUserAction:
 * @CHANGE_USER_NO_PROFILE: Cannot change user, no such profile exists
 * @CHANGE_USER_OK: User changed
 * @CHANGE_USER_WAITING_PROFILE: Changing user, waiting the profile from the
   teacher, user is changed after profile is retrieved
 * @CHANGE_USER_LOGOUT_REQUIRED: Cannot change user, previous user is already
 * logged in
 * @CHANGE_USER_NO_USERNAME: Cannot change user, no username set
 *
 * Return values for user change action.
 */
typedef enum {
	CHANGE_USER_NO_PROFILE = -1,
	CHANGE_USER_OK = 0,
	CHANGE_USER_WAITING_PROFILE,
	CHANGE_USER_LOGOUT_REQUIRED,
	CHANGE_USER_NO_USERNAME,
} GemsChangeUserAction;

/**
 * GemsProfileLoadAction:
 * @PROFILE_LOADED: Profile was loaded.
 * @PROFILE_READ_FAILURE: Profile cannot be loaded, file cannot be read.
 * @PROFILE_NOT_FOUND: Profile file was not found.
 * @PROFILE_VERSION_MISMATCH: Profile version is not compatible with the
 * application
 * @PROFILE_REOAD_FAILED: Cannot reload profile with same name
 * @PROFILE_RELOAD_FAILED_NOT_AUTHENTICATED: Profile reload failed because
 * user was not authenticated.
 *
 * Return values of profile loading.
 */
typedef enum {
	PROFILE_LOADED = 0,
	PROFILE_READ_FAILURE,
	PROFILE_NOT_FOUND,
	PROFILE_VERSION_MISMATCH,
	PROFILE_RELOAD_FAILED,
	PROFILE_RELOAD_FAILED_NOT_AUTHENTICATED,
} GemsProfileLoadAction;


// Security vars
#define AES_ROUNDS 5
#define AES_SALT_LENGTH 8
#define PASSLEN 64
#define SHA_HASH_LENGTH 32

// Default strings for games
#define GEMS_EMPTY_SONG_INFO "empty song info"

#endif //__GEMS_DEFINITIONS_
