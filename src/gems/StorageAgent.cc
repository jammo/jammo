/*
 * StorageAgent.cc
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2011 Lappeenranta University of Technology
 *
 * Authors: Janne Parkkila
 * 					Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */

#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <glib/gstdio.h>
#include <stdlib.h>
#include <string.h>
#include "gems_definitions.h"
#include "StorageAgent.h"
extern "C" {
	#include "../cem/cem.h"
	#include "../configure.h"
	#include <openssl/aes.h>
}



using namespace std;

// Constructor
StorageAgent::StorageAgent()
{
	loadeduser = NULL;
	salt = NULL;
	password_hash = NULL;
	profile_hash = NULL;
	encrypted_profile = NULL;
	encrypted_profile_size = 0;
	version = 0;
	authenticated = FALSE;
	profileloaded = FALSE;
	
	gchar* logmsg = NULL;
	gchar* path = g_build_filename(configure_get_jammo_directory(),JAMMOPROFILE,NULL);
	
	// If profile directory exists
	if(g_file_test(path,G_FILE_TEST_EXISTS))
	{
		// If it is not a directory
		if(!g_file_test(path,G_FILE_TEST_IS_DIR))
		{
			// Try to remove
			if(g_remove(path) == 0)
			{
				logmsg = g_strdup_printf("StorageAgent: removed non-directory reserving profile directory name (%s)", path);
				cem_add_to_log(logmsg,J_LOG_DEBUG);
				g_free(logmsg);
			}
			// Cannot remove
			else
			{
				logmsg = g_strdup_printf("StorageAgent: cannot remove non-directory reserving profile directory name (%s)", path);
				cem_add_to_log(logmsg,J_LOG_ERROR);
				g_free(logmsg);
				g_free(path);
				return;	
			}
		}
		// Was a directory
		else
		{
			g_free(path);
			return;
		}
	}
	
	// Try to create directory
	if(g_mkdir(path,0777) == -1)
	{
		logmsg = g_strdup_printf("StorageAgent: cannot create profile directory \"%s\"",path);
		cem_add_to_log(logmsg, J_LOG_ERROR);
		g_free(logmsg);
	}
	else
	{
		logmsg = g_strdup_printf("StorageAgent: created profile directory to %s", path);
		cem_add_to_log(logmsg, J_LOG_DEBUG);
		g_free(logmsg);
	}
	g_free(path);
}

// Destructor
StorageAgent::~StorageAgent()
{
	// Just to make sure
	profileDetails.clear();
	
	clearEncryptedData();
}

// Synchronizes profile data
gboolean StorageAgent::Synchronize()
{
	// TODO: Synchronizing missing
	return TRUE;
}

// Removes the unneccesary lines from the files read
gint StorageAgent::Tokenize(const string& str,
			  vector<string>& tokens,
			  const string& delimiters)
{
	gint elements = 0;
	// Skip delimiters at beginning.
	string::size_type lastPosition = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	string::size_type position  = str.find_first_of(delimiters, lastPosition);

	while (string::npos != position || string::npos != lastPosition)
	{
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPosition, position - lastPosition));
		// Skip delimiters.  Note the "not_of"
		lastPosition = str.find_first_not_of(delimiters, position);
		// Find next "non-delimiter"
		position = str.find_first_of(delimiters, lastPosition);
		elements++;
	}
	return elements;
}

// Retrieves user profile data
gboolean StorageAgent::Retrieve(guint32 userID)
{
	gchar* logmsg = g_strdup_printf("StorageAgent::Retrieve() : attempted to use deprecated function!");
	cem_add_to_log(logmsg, J_LOG_ERROR);
	g_free(logmsg);
	return FALSE;
	
	// All the user information is currently saved to a 
	// .csv file, so it is possible to save and edit
	// details 
	
	//ifstream profileFile;
	//string line;
	//string profilename;
	
	// change the userID to string
	//stringstream user;
	//user << userID;
	
	// Append all the neccesary parts to the filename
//	profileCSV.append(JAMMOPROFILE);
//	profilename.clear();
//	profilename.append(user.str());
//	profilename.append(PROFILETYPE);
//	gchar* pfile = g_build_filename(configure_get_jammo_directory(),JAMMOPROFILE,profilename.c_str(),NULL);
	
	// Change the string to char* for reading as a file	
	//char *pfile = (char*)profileCSV.c_str();
	
	// Open the file containing the profiles
//	profileFile.open(pfile);
	
//	if (!profileFile)
//	{
//		gchar* logmsg = g_strdup_printf("StorageAgent::Retrieve : Unable to open profile file \"%s\".",pfile);
//		cem_add_to_log(logmsg,J_LOG_ERROR);
//		g_free(logmsg);
		
		// Use some default values.
//		profileDetails[PROF_PARAM_USERID] = user.str();
//		profileDetails[PROF_PARAM_USERNAME] = PROF_PARAM_USERNAME;
//		// TODO fix the auth level to correspond the REAL level
//		profileDetails[PROF_PARAM_AUTHLEVEL] = "none";
//		profileDetails[PROF_PARAM_FIRSTNAME] = PROF_PARAM_FIRSTNAME;
//		profileDetails[PROF_PARAM_LASTNAME] = PROF_PARAM_LASTNAME;
//		profileDetails[PROF_PARAM_POINTS] = "0";
//		profileDetails[PROF_PARAM_AGE] = "0";
//		return FALSE;
//	}

	// Get the line and remove the ,
//	getline (profileFile, line);
	
	//cout << line << endl;
	
	// Create a vector to hold the user info
	// and take the unneccesary markups off
//	vector<string> tokens;
//	Tokenize(line, tokens, ",");
	
	// Save all the details to profileDetails container
//	profileDetails[PROF_PARAM_USERID] = tokens[0];
//	profileDetails[PROF_PARAM_USERNAME] = tokens[1];
//	profileDetails[PROF_PARAM_AUTHLEVEL] = tokens[2];
//	profileDetails[PROF_PARAM_FIRSTNAME] = tokens[3];
//	profileDetails[PROF_PARAM_LASTNAME] = tokens[4];
//	profileDetails[PROF_PARAM_POINTS] = tokens[5];
//	profileDetails[PROF_PARAM_AGE] = tokens[6];
		
	// Close the file
//	profileFile.close();

//	return TRUE;
}

// Stores user profile data
gboolean StorageAgent::Store(guint32 userID)
{
	gchar* logmsg = g_strdup_printf("StorageAgent::Store() : attempted to use deprecated function!");
	cem_add_to_log(logmsg, J_LOG_ERROR);
	g_free(logmsg);
	return FALSE;
	
	// TODO:  Store function for profile saving
	// Since there is no real database yet, this
	// uses .csv text file :-D Yippii!

//	ofstream profileFile;
//	string line;
//	string profilename;
	
	// change the userID to string
//	stringstream user;
//	user << userID;
	
	// Append all the neccesary parts to the filename
//	profilename.clear();
//	profilename.append(user.str());
//	profilename.append(PROFILETYPE);
//	gchar* pfile = g_build_filename(configure_get_jammo_directory(),JAMMOPROFILE,profilename.c_str(),NULL);
	
	// Change the string to char* for reading as a file	
	//char *pfile = (char*)profileCSV.c_str();
	
	// cout << "STORE!" << endl;
	// Open the file containing the profiles, empty the previous
	// one just to be sure that there is no more than 1 line.
//	profileFile.open(pfile, ios::trunc);
//	if (!profileFile)
//	{
//		gchar* logmsg = g_strdup_printf("StorageAgent::Store : Unable to open profile file \"%s\".",pfile);
//		cem_add_to_log(logmsg,J_LOG_ERROR);
//		g_free(logmsg);
		// TODO report what kind of error
//		return FALSE;
//	}

	// Save all the details to a string
//	string details = profileDetails[PROF_PARAM_USERID] + "," +
//		profileDetails[PROF_PARAM_USERNAME] + "," +
//	  profileDetails[PROF_PARAM_AUTHLEVEL] + "," +
//	  profileDetails[PROF_PARAM_FIRSTNAME] + "," +
//	  profileDetails[PROF_PARAM_LASTNAME] + "," + 
//	  profileDetails[PROF_PARAM_POINTS] + "," +
//	  profileDetails[PROF_PARAM_AGE];
	
	// Write the string details to a file
//	profileFile << details << endl;
	// and close the file
//	profileFile.close();

//	return TRUE;
}

string StorageAgent::getDetails()
{
	// A variable where the profileListing is saved
	string profileListing;
	// Iterator goes through all the profile information
	// and saves everything to the profileListing
	for (profileIterator = profileDetails.begin();
			profileIterator != profileDetails.end(); ++profileIterator)
	{
		profileListing += profileIterator->first + "=" + profileIterator->second + ",";
	}
	// ProfileListing is returned
	//cout << "StorageAgent::getDetails: details returned." << endl;
	return profileListing;

}

// Get one detail
string StorageAgent::getDetail(string& detail)
{
	// Check if such detail exists
	if( (profileIterator = profileDetails.find(detail)) == profileDetails.end())
	{
		return "";
	}
	else return profileDetails[detail];
}

gboolean StorageAgent::setDetails(string& detail, string& newValue)
{
	// The iterator goes through the profile details saved in the map
	// If there is no detail the user tries to edit, it will inform the user
	if ( (profileIterator = profileDetails.find(detail)) == profileDetails.end() )
		{
			//cout << "StorageAgent::setDetails: no " << detail << "-parameter" << endl;
			return FALSE;
		}
	// Else change the detail to the new wanted value
	else
	{
		profileDetails[detail] = newValue;
	}

	return TRUE;
}

gboolean StorageAgent::clearDetails()
{
	// Clears the map to be empty
	profileDetails.clear();
	authenticated = FALSE;
	return TRUE;
}

gboolean StorageAgent::isAuthenticated()
{
	return authenticated;
}

gboolean StorageAgent::setAuthenticated()
{
	authenticated = TRUE;
	return TRUE;
}

gboolean StorageAgent::isProfileLoaded()
{
	return profileloaded;
}


gboolean StorageAgent::deserializeProfile(gchar* profile)
{
	vector<string> tokens;
	
	if(profile == NULL) return FALSE;
	
	if(Tokenize(string(profile), tokens, ",") != PROF_TOKENS)	return FALSE;
	
	// Save all the details to profileDetails container
	profileDetails[PROF_PARAM_TYPE] = tokens[0]; // Profile type (PROF_TYPE_LOCAL or PROF_TYPE_REMOTE)
	profileDetails[PROF_PARAM_USERID] = tokens[1];
	profileDetails[PROF_PARAM_USERNAME] = tokens[2];
	profileDetails[PROF_PARAM_AUTHLEVEL] = tokens[3];
	profileDetails[PROF_PARAM_FIRSTNAME] = tokens[4];
	profileDetails[PROF_PARAM_LASTNAME] = tokens[5];
	profileDetails[PROF_PARAM_POINTS] = tokens[6];
	profileDetails[PROF_PARAM_AGE] = tokens[7];
	profileDetails[PROF_PARAM_AVATARID] = tokens[8];
	
	return TRUE;
}

gchar* StorageAgent::serializeProfile()
{
	// Save all the details to a string
	string serialized = profileDetails[PROF_PARAM_TYPE] + "," +
		profileDetails[PROF_PARAM_USERID] + "," +
		profileDetails[PROF_PARAM_USERNAME] + "," +
	  profileDetails[PROF_PARAM_AUTHLEVEL] + "," +
	  profileDetails[PROF_PARAM_FIRSTNAME] + "," +
	  profileDetails[PROF_PARAM_LASTNAME] + "," + 
	  profileDetails[PROF_PARAM_POINTS] + "," +
	  profileDetails[PROF_PARAM_AGE] + "," + 
	  profileDetails[PROF_PARAM_AVATARID];
	  
	gchar* c_serialized = g_strdup(serialized.c_str());
	return c_serialized;
}

gchar* StorageAgent::getLoadedUser()
{
	return loadeduser;
}

guchar* StorageAgent::getSalt()
{
	return salt;
}

guchar* StorageAgent::getPasswordHash()
{
	return password_hash;
}

guchar* StorageAgent::getProfileHash()
{
	return profile_hash;
}

guchar* StorageAgent::getEncryptedProfile()
{
	return encrypted_profile;
}

guint StorageAgent::getEncryptedProfileSize()
{
	return encrypted_profile_size;
}

guint8 StorageAgent::getProfileVersion()
{
	return version;
}

gboolean StorageAgent::setSalt(guchar* _salt)
{
	// Setting empty salt not allowed
	if(_salt == NULL) return FALSE;
	
	// Clear old salt
	if(salt != NULL) g_free(salt);

	salt = g_new0(guchar,sizeof(guchar)*AES_SALT_LENGTH);

	g_memmove(&salt[0],_salt,AES_SALT_LENGTH);
	
	return TRUE;
}

gboolean StorageAgent::setPasswordHash(guchar* _password_hash)
{
	// Setting empty password hash not allwed
	if(_password_hash == NULL) return FALSE;
	
	// Clear old hash
	if(password_hash != NULL) g_free(password_hash);
	
	password_hash = g_new0(guchar,sizeof(guchar)*SHA_HASH_LENGTH);

	g_memmove(&password_hash[0],_password_hash,SHA_HASH_LENGTH);
	
	return TRUE;
}

gboolean StorageAgent::setEncryptedProfile(guchar* _profile_hash, guchar* _encrypted_profile, guint _e_profile_size)
{	
	// Check params
	if(_profile_hash == NULL || _encrypted_profile == NULL || _e_profile_size == 0) return FALSE;
	
	if(profile_hash != NULL) g_free(profile_hash);
	profile_hash = g_new0(guchar,sizeof(guchar)*SHA_HASH_LENGTH);

	g_memmove(&profile_hash[0],_profile_hash,SHA_HASH_LENGTH);
	
	if(encrypted_profile != NULL) g_free(encrypted_profile);
	encrypted_profile = g_new0(guchar,sizeof(guchar)*_e_profile_size);

	g_memmove(&encrypted_profile[0],_encrypted_profile,_e_profile_size);
	
	encrypted_profile_size = _e_profile_size;
	
	return TRUE;
}

void StorageAgent::clearEncryptedData()
{
	if(loadeduser != NULL)
	{
		g_free(loadeduser);
		loadeduser = NULL;
	}
	
	if(salt != NULL)
	{
		g_free(salt);
		salt = NULL;
	}
	
	if(password_hash != NULL)
	{
		g_free(password_hash);
		password_hash = NULL;
	}
	
	if(profile_hash != NULL)
	{
		g_free(profile_hash);
		profile_hash = NULL;
	}
	
	if(encrypted_profile != NULL)
	{
		g_free(encrypted_profile);
		encrypted_profile = NULL;
	}
		
	encrypted_profile_size = 0;
	profileloaded = FALSE;
}

/* Read encrypted profile - raw
*  parameters: username, password hash, salt, profile data hash, encrypted profile data, profile length
*/
GemsProfileLoadAction StorageAgent::readProfile(const gchar* username)
{
	gchar* profilefile = g_strjoin(NULL,username,PROFILE_FILE_EXT,NULL);
	gchar* filename = g_build_filename(configure_get_jammo_directory(),JAMMOPROFILE,profilefile,NULL);
	gchar* contents = NULL;
	guint length = 0;
	GError* error = NULL;
	gchar* logmsg = NULL;
	
	// Test if profile file exists;
	if(g_file_test(filename,G_FILE_TEST_IS_REGULAR))
	{
		// Get the file content
		if(!g_file_get_contents(filename,&contents,(gsize*)&length,&error))
		{
			// Cannot read, report error.
			if(error)
			{
				logmsg = g_strdup_printf("StorageAgent::readProfile : Cannot read profile for user %s, reason: %s",username, error->message);
				cem_add_to_log(logmsg,J_LOG_ERROR);
				g_free(logmsg);
				g_error_free(error);
				if(profilefile) g_free(profilefile);
				if(filename) g_free(filename);
				return PROFILE_READ_FAILURE;
			}
		}
		else
		{
			// version + SALT + PASS HASH + PROFILE HASH + AES_BLOCK_SIZE
			guint min_length = sizeof(guint8) + AES_SALT_LENGTH + SHA_HASH_LENGTH + SHA_HASH_LENGTH + AES_BLOCK_SIZE;
			
			if(length < min_length)
			{
				logmsg = g_strdup_printf("StorageAgent::readProfile : Profile is corrupt, length is less than minimum. Profile length = %d, minimum length = %d. Profile is not loaded.",length,min_length);
				cem_add_to_log(logmsg,J_LOG_ERROR);
				g_free(logmsg);
				profileloaded = FALSE;
				if(contents) g_free(contents);
				if(profilefile) g_free(profilefile);
				if(filename) g_free(filename);
				return PROFILE_READ_FAILURE;
			}
			// Free existing
			if(salt) g_free(salt);
			
			// Extract version
			version = *(guint8*)&contents[0];
			
			// TODO check if the profile can be converted to current version !
			if(version != JAMMO_PROFILE_VERSION)
			{
				logmsg = g_strdup_printf("StorageAgent::readProfile : Profile version is not compatible, read version = %d, JAMMO_PROFILE_VERSION = %d. Profile is not loaded!",
					version, JAMMO_PROFILE_VERSION);
				cem_add_to_log(logmsg,J_LOG_ERROR);
				g_free(logmsg);
				profileloaded = FALSE;
				if(profilefile) g_free(profilefile);
				if(filename) g_free(filename);
				if(contents) g_free(contents);
				return PROFILE_VERSION_MISMATCH;
			}
			else
			{
				// Extract salt
				salt = g_new0(guchar,sizeof(guchar) * AES_SALT_LENGTH);

				*(guint32*)&salt[0] = *(guint32*)&contents[1];
				*(guint32*)&salt[sizeof(guint32)] = *(guint32*)&contents[sizeof(guint32)+1];
			
				// Free existing
				if(password_hash) g_free(password_hash);
			
				// Extract password hash
				password_hash = g_new0(guchar,sizeof(guchar) * (SHA_HASH_LENGTH + 1));
				g_memmove(password_hash,&contents[AES_SALT_LENGTH + 1],SHA_HASH_LENGTH);
			
				// Free existing
				if(profile_hash) g_free(profile_hash);
			
				// Extract profile hash
				profile_hash = g_new0(guchar,sizeof(guchar) * (SHA_HASH_LENGTH + 1));
				g_memmove(profile_hash,&contents[AES_SALT_LENGTH + SHA_HASH_LENGTH + 1],SHA_HASH_LENGTH);
			
				// Free existing
				if(encrypted_profile) g_free(encrypted_profile);
			
				// Extract encrypted profile
				encrypted_profile_size = length - SHA_HASH_LENGTH - SHA_HASH_LENGTH - AES_SALT_LENGTH - 1;
				encrypted_profile = g_new0(guchar,sizeof(guchar) * (encrypted_profile_size));
				g_memmove(encrypted_profile,&contents[AES_SALT_LENGTH + SHA_HASH_LENGTH + SHA_HASH_LENGTH + 1],encrypted_profile_size);
	
				// copy the username
				if(loadeduser) g_free(loadeduser);
				loadeduser = g_strdup(username);

				profileloaded = TRUE;
			}
		}
	}
	else
	{
		logmsg = g_strdup_printf("StorageAgent::readProfile : Profile for user %s doesn't exist.", username);
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		if(filename) g_free(filename);
		if(profilefile) g_free(profilefile);
		return PROFILE_NOT_FOUND;
	}
  
	if(contents) g_free(contents);
	if(filename) g_free(filename);
	if(profilefile) g_free(profilefile);

	return PROFILE_LOADED;
}

// Write encrypted profile - raw
// username, password hash, encrypted profile data, profile data hash
gint StorageAgent::writeProfile(const gchar* username)
{
	gchar* profilefile = g_strjoin(NULL,username,PROFILE_FILE_EXT,NULL);
	gchar* filename = g_build_filename(configure_get_jammo_directory(),JAMMOPROFILE,profilefile,NULL);
	guint length = sizeof(guint8) + AES_SALT_LENGTH + SHA_HASH_LENGTH + SHA_HASH_LENGTH + encrypted_profile_size;
	GError* error = NULL;
	gchar* logmsg = NULL;
	
	/* Profile file will contain (in order, size in bytes)
	*  version 1
	*  salt 8
	*  h(pass) 32
	*  h(profile) 32
	*  e(profile) rest
	*/
	
	if(salt == NULL) return 2;
	if(password_hash == NULL) return 3;
	if(profile_hash == NULL) return 4;
	if(encrypted_profile == NULL) return 5;
	if(encrypted_profile_size == 0) return 6;
	if(version != JAMMO_PROFILE_VERSION) return 7;	
	
	gchar* content = g_new0(gchar,sizeof(gchar) * length);
	
	// Set version
	*(guint8*)&content[0] = version;
	
	// Set salt
	*(guint32*)&content[1] = *(guint32*)&salt[0];
	*(guint32*)&content[sizeof(guint32) + 1] = *(guint32*)&salt[sizeof(guint32)];
	
	// Set password hash
	g_memmove(&content[AES_SALT_LENGTH + 1],password_hash,SHA_HASH_LENGTH);
	g_memmove(&content[AES_SALT_LENGTH + SHA_HASH_LENGTH + 1],profile_hash,SHA_HASH_LENGTH);
	g_memmove(&content[AES_SALT_LENGTH + SHA_HASH_LENGTH + SHA_HASH_LENGTH + 1],encrypted_profile,encrypted_profile_size);
	
	// Check for previous profile file
	if(g_file_test(filename,(G_FILE_TEST_EXISTS)))
	{
		if(g_remove(filename) == 0)
		{
			logmsg = g_strdup_printf("StorageAgent::writeProfile : Removed existing profile file for user %s.",username);
			cem_add_to_log(logmsg,J_LOG_DEBUG);
			g_free(logmsg);
		}
		else
		{
			logmsg = g_strdup_printf("StorageAgent::writeProfile : Cannot remove existing profile file for user %s. Saving of profile information failed.",username);
			cem_add_to_log(logmsg,J_LOG_ERROR);
			g_free(logmsg);
			g_free(filename);
			g_free(profilefile);
			g_free(content);
			return -1;
		}
	}

  if(!g_file_set_contents(filename,content,length,&error))
  {
  	if(error)
		{
			logmsg = g_strdup_printf("StorageAgent::writeProfile : Cannot save profile for user %s, reason: %s",username, error->message);
			cem_add_to_log(logmsg,J_LOG_ERROR);
			g_free(logmsg);
			g_error_free(error);
			g_free(filename);
			g_free(profilefile);
			g_free(content);
			return -1;
		}
  }
  else
  {
  	logmsg = g_strdup_printf("StorageAgent::writeProfile : Profile of user %s successfully saved (%d bytes written).",username, length);
  	cem_add_to_log(logmsg, J_LOG_DEBUG);
  	g_free(logmsg);
  }
  
	g_free(content);
	g_free(filename);
	g_free(profilefile);
	
	return 0;
}

