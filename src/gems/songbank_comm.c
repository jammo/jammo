#include"songbank_comm.h"

/*
// always add these 2 lines when creating something related to PeerHood, be it service or client
#include <AbstractPlugin.h>
map<const char*, MAbstractPlugin*> pluginMap;
//
#define DWORD unsigned int;
*/

int SongBankComm_init( SongBankComm* _sbComm, MPeerHood* _peerHood ){//gems_service_songbank _mySBobj, 

	_sbComm = malloc( sizeof( SongBankComm ) );
	sbComm = _sbComm;
	sbComm->peerHood = _peerHood;
//	sbComm->connection = NULL;
	sbComm->connections = NULL;	//TODO initialize the list properly!

if(DEBUG)printf("registering songbank...\n");
	sbComm->sbPort = ph_c_register_service( sbComm->peerHood, SONGBANK_SERVICE_NAME, "");

	if( ! sbComm->sbPort ) {
		printf("Registering songbank failed... proceeding without network.\n");
	    return FALSE;
	}
	else{
		printf( "Songbank registered on port:%i\n", sbComm->sbPort );
		g_timeout_add_full( SB_COMM_PRIORITY, SB_COMM_INTERVAL, (GSourceFunc)gems_service_songbank_serveRequest, sbComm, SB_COMM_DESTROY_NOTIFY);
		return TRUE;
	}	

}

int SongBankComm_clean(){//SongBankComm* sbComm){

	if( sbComm ){
		printf( "Unregistering songbank...\n" );
		int res = ph_c_unregister_service(sbComm->peerHood, SONGBANK_SERVICE_NAME);
		printf( "Done unregistering songbank.\n" );
//******THREAD_FIX
		while( sbComm->connections != NULL && g_list_first(sbComm->connections) != NULL )
			sbComm->connections = g_list_remove( sbComm->connections, g_list_first(sbComm->connections) );

		if(sbComm->connections != NULL) g_list_free(sbComm->connections);
		printf( "Connection list freed.\n" );
//******THREAD_FIX
		free( sbComm );
		return res;
	}
	else 
		return TRUE;
	
}

int GetFileFromNetwork(const char* filename, const char* author, const char* saveToFile){
	
	int fileFound = FALSE, gotFile = FALSE;

	TServiceList* services = ph_c_get_localservicelist(sbComm->peerHood);
	printf("services found: %i\n", ph_c_servicelist_size(services) );
	MAbstractConnection* conn;
	gint connSocket;

	if( ph_c_servicelist_is_empty(services) == FALSE ){

		ServiceIterator* si = ph_c_new_service_iterator(services);
		CService* service;

		while( fileFound == FALSE && ph_c_service_iterator_is_last( si, services ) == FALSE ){

			service = ph_c_service_iterator_get_service( si );

			if( strcmp( SONGBANK_SERVICE_NAME, ph_c_service_get_name( service ) ) == 0 && sbComm->sbPort != ph_c_service_get_port( service )   ){

				conn = ph_c_connect_localservice( sbComm->peerHood, service );
				connSocket = ph_c_connection_get_fd( conn );
				fileFound = AskFileFromDevice( connSocket, filename, author );
				if( fileFound ){
					if(DEBUG)printf("filefound on deviceSocket:%i\n", connSocket);
					gotFile = RecieveFile( connSocket, saveToFile );
				}

			}
			ph_c_service_iterator_next_iterator( si );
		}
		ph_c_delete_service_iterator(si);
	}

	ph_c_delete_servicelist(services);
	return gotFile;
}

/*	from remote devices
int GetFileFromNetwork(const char* filename, const char* author){

		int fileFound = FALSE;//, gotFile = FALSE;
TDeviceList* list = ph_c_get_devicelist_with_services( sbComm->peerHood, SONGBANK_SERVICE_NAME );
//if( DEBUG )printf( "got songbank device-list... is empty?:%i\n", ph_c_devicelist_is_empty(list) );
    
	if(ph_c_devicelist_is_empty(list) == FALSE)	{

		DeviceIterator* i = ph_c_new_device_iterator(list);
		MAbstractConnection* conn;
		gint connSocket;
							if( DEBUG )printf("Found songbank devices on network. Requesting file... \n\n\n");
		while( fileFound == FALSE && ph_c_device_iterator_is_last(i,list) == FALSE ){

			conn = ph_c_connect_remoteservice( sbComm->peerHood, ph_c_device_iterator_get_device(i), SONGBANK_SERVICE_NAME );
			connSocket = ph_c_connection_get_fd( conn );

			fileFound = AskFileFromDevice( connSocket, filename, author );
			if( fileFound )
				;//TODO	gotFile = RecieveFile( connSocket, mySBobj->getBaseDirectory() + filename );

			ph_c_connection_disconnect( conn );
			ph_c_device_iterator_next_iterator(i);
		}
		ph_c_delete_device_iterator(i);
		ph_c_delete_devicelist(list);
	}

	return fileFound;//return gotFile;
}*/

int AskFileFromDevice(gint connSocket, const char* filename, const char* author ){

	gchar *request = NULL;	int _index;
	request = malloc( strlen("SEARCH:") + strlen(filename) + strlen(",") + strlen(author) + 1 );

	memcpy( request, "SEARCH:", strlen("SEARCH:") );		_index = strlen("SEARCH:");
	memcpy( request+_index, filename , strlen(filename) );	_index += strlen(filename);
	memcpy( request+_index, ",", strlen(",") );				_index += strlen(",");
	memcpy( request+_index, author, strlen(author) );		_index += strlen(author);
	request[_index] = '\0';

	gems_message* msg = malloc( sizeof(gems_message) );
	msg->message = request;
	msg->length = strlen(request) + 1; //The '\0'

	int bytesSent = gems_communication_write_data( connSocket, msg );
	free(request);

	if( bytesSent == msg->length ){	//request sent

		char* response = malloc( sizeof(char) );
		if( gems_communication_read_data( connSocket, response, 1) > 0 && response[0] == 'y' ){
			return TRUE;
		}
		else{
			if( DEBUG )printf("file not available on device on socket %i; response:%s\n", connSocket, response);
			return FALSE;
		}
	}
	else
		return FALSE;//TODO LOG REQUEST NOT WRITTEN
}

void NewSBConnection( const unsigned short aPort, MAbstractConnection *aConnection, int aConnectionId ){

	//******THREAD_FIX
	if( g_list_length( sbComm->connections ) >= MAX_SB_CONNECTIONS ){
		printf( "dropping connection because too many connections already\n" );
		ph_c_connection_disconnect(aConnection);
		return;
	}

	if( DEBUG )printf( "Adding clientConnection to the end of list...\n" );
	sbComm->connections = g_list_prepend( sbComm->connections, aConnection );
	sbComm->connections = g_list_reverse( sbComm->connections );
	//******THREAD_FIX
}

void ParseRequest( char* request, char* name, int nameLength, char* author, int authorLength ){

	//strstr(s1,s2) returns a pointer to the first instance of string s2 in s1.  Returns a NULL pointer if s2 is not encountered in s1.
	char *temp  = request;
	temp = temp + strlen("SEARCH:");
	
	memcpy(name, temp, nameLength-1);
	name[nameLength-1] = '\0';

	temp = temp + nameLength;
	author = memcpy( author, temp, authorLength-1 );
	author[authorLength] = '\0';

	if( DEBUG )printf( "ParseResult - filename:%s,author:%s\n", name, author );
}

int RecieveRequest( int connSocket, char* request, int* nameLength, int* authorLength ){

	int reqLength = gems_communication_read_data( connSocket, request, REQ_BUFFER_SIZE );
	if(DEBUG)printf( "REQrecieved:%s\n", request );

	if( reqLength < (strlen("SEARCH:,") + 2) ){
		if(DEBUG)printf("incomplete request - cannot process");
		return FALSE;
	}
	//strstr(s1,s2) returns a pointer to the first instance of string s2 in s1.  Returns a NULL pointer if s2 is not encountered in s1.
	char *temp  = strstr( request,  "SEARCH:" );
	if( ! temp )	return FALSE;	//TODO maybe LOG this

	temp = temp + strlen("SEARCH:");
	
	//strchr(s,c) returns a pointer to the first instance of c in s.  Returns a NULL pointer if c is not encountered in the string.
	char* seprator = strchr( temp,  ',');
	if( ! seprator )	return FALSE;//TODO maybe LOG this

	*nameLength = seprator - temp + 1;
	temp = temp + *nameLength;
	*authorLength = strlen(temp) + 1;

	return TRUE;
}

int	SendResponse( int connSocket, const char* filePath ){

	char* response = malloc(1);
	gems_message* msg = malloc( sizeof(gems_message) );
	msg->message = response;	
	msg->length = 1;

	if( filePath ){
		response[0] = 'y';	if( DEBUG )printf("file Path:%s. sending to client...\n", filePath);
	}
	else{
		response[0] = 'n';	if( DEBUG )printf("file not found. telling client...\n");
	}

	int bytesSent = gems_communication_write_data( connSocket, msg );
	int sentSuccess = ( msg->length == bytesSent );
	if(DEBUG)printf( "bytesSent:%i,sentSuccess:%i\n", bytesSent, sentSuccess );

	free( msg );
	free( response );
	return sentSuccess;	
}

gboolean gems_service_songbank_serveRequest(gpointer* ptr){

	printf("IN ServeRequest... \n");
	SongBankComm *sbc = ( SongBankComm* )ptr;
	MAbstractConnection* conn = g_list_first( sbc->connections )->data;
	if( conn == NULL ){
		if( DEBUG ) printf("Connections list empty... returning. \n");
		return TRUE;
	}

	int connSocket = ph_c_connection_get_fd( conn );
	printf("ServeRequest-connected socket:%i\n", connSocket);

	char *request = malloc(REQ_BUFFER_SIZE);
	int nameLength, authorLength;
	if( ! RecieveRequest( connSocket, request, &nameLength, &authorLength ) ){
		printf("Failed to recieve file request!\n");
		free( request );
		return TRUE;
	}
	if(DEBUG)printf("namelength:%i, authorlength:%i; parsing...\n", nameLength, authorLength);

	char *name, *author, *filePath;
	name = malloc( nameLength );	
	author = malloc( authorLength );

	ParseRequest( request, name, nameLength, author, authorLength );
	free( request );	if( DEBUG )printf("request parsed... searching file!\n");

	filePath = SearchFileLocal( name, author );
	if( SendResponse(connSocket, filePath) && filePath ){
		if( DEBUG )printf("ready to send file:%s\n", filePath );
		SendFile(connSocket, filePath);
	}
	free( name ); free( author );

	if(DEBUG)printf("Disconnecting, removing and freeing connection.");
	ph_c_connection_disconnect( conn );	//	ph_c_connection_disconnect( sbc->connection );
	sbc->connections = g_list_remove( sbc->connections, conn );
	if( conn != NULL ) free( conn );
	return TRUE;
}

int SendFile( int connSocket, const char* filePath ){

	char buff[FILE_BUFFER_SIZE];
	int bytesSent = 0, error = FALSE;

	gems_message* msg = malloc( sizeof(gems_message) );

	FILE * songfile = fopen( filePath, "r");

	while( !feof( songfile ) && !error  ){
		
		msg->length = fread(buff, sizeof(char), FILE_BUFFER_SIZE, songfile);
		msg->message = buff;
		bytesSent = gems_communication_write_data( connSocket, msg );

		if( ferror(songfile) || (bytesSent != msg->length) )
			error = TRUE;
	}
	if( error )	printf("Error Sending File\n");
	else printf("File Sent Success!\n");

	fclose( songfile );
	return !error;
}

int RecieveFile( int connSocket, const char* fileSavePath ){

/*	char* fileSavePath = malloc( strlen(baseDirectory) + strlen(filename) + 2);
	memcpy( fileSavePath, baseDirectory, strlen(baseDirectory) );
	fileSavePath[strlen(baseDirectory)] = '/';
	memcpy( fileSavePath + strlen(baseDirectory) + 1, filename, strlen(filename) );
	fileSavePath[strlen(baseDirectory) + strlen(filename)+1] = '\0';
*/	if(DEBUG)printf( "reciving and saving to:%s\n", fileSavePath );

	char buff[FILE_BUFFER_SIZE];
	int recvLength = 0, writeCount = 0, error = FALSE;

	FILE* songfile = fopen( fileSavePath, "w");
	do{
		recvLength = gems_communication_read_data( connSocket, buff, FILE_BUFFER_SIZE );
		writeCount = fwrite( (void *)buff, sizeof( char ), recvLength, songfile );
		if( (writeCount != recvLength) || ferror(songfile) )
			error = TRUE;
	}
	while( recvLength==FILE_BUFFER_SIZE && !error );

	fclose( songfile );
	return !error;
}


/*

int SendFile(const char* filePath){

	FILE * songfile = fopen( filePath, "r");

	const int BUFFSIZE = 1024;
	char buff[BUFFSIZE];
	int sentLength = 0;

	while( !songfile.eof() ){
//fread(buff, sizeof(char), fLength, file);
		songfile.read(buff, BUFFSIZE);
		sentLength = connection->Write( buff, songfile.gcount() );
		if( sentLength != songfile.gcount() )
			cout<<"ERROR - not all data was correctly sent. sending:"<<songfile.gcount()<<"sent:"<<sentLength<<endl;
		if(songfile.gcount() != BUFFSIZE)
			break;
	}
	songfile.close();
//			if( DEBUG )printf("File Sent\n");
	return true;
	
}

int RecieveRequest( int connSocket, char* name, char* author ){

	char *request = malloc(200);
	int reqLength = gems_communication_read_data( connSocket, request, 200);
	if(DEBUG)printf( "REQrecieved:%s\n parsing...\n", request );

	if( reqLength < (strlen("SEARCH:,") + 2) ){
		if(DEBUG)printf("incomplete request - cannot process");
		return FALSE;
	}
	//strstr(s1,s2) returns a pointer to the first instance of string s2 in s1.  Returns a NULL pointer if s2 is not encountered in s1.
	char *temp  = strstr( request,  "SEARCH:" );
	if( ! temp )	return FALSE;	// maybe LOG this

	temp = temp + strlen("SEARCH:");
	
	//strchr(s,c) returns a pointer to the first instance of c in s.  Returns a NULL pointer if c is not encountered in the string.
	char* seprator = strchr( temp,  ',');
	if( ! seprator )	return FALSE;// maybe LOG this

	int nameLength = seprator - temp + 1;
	name = malloc( nameLength );
	memcpy(name, temp, nameLength);
	name[nameLength-1] = '\0';

	temp = temp + nameLength;
	author = malloc( strlen(temp) + 1 );
	author = memcpy( author, temp, strlen(temp));
	author[strlen(temp)] = '\0';

	if( DEBUG )printf( "ParseResult - filename:%s,author:%s\n",name,author);
	free( request );
	return TRUE;
}
*/
