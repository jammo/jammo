/** collaboration.h is part of JamMo.
License: GPLv2, read more from COPYING
*/
#ifndef _COLLABORATION_H_
#define _COLLABORATION_H_


#include <gst/gst.h>
#include <clutter/clutter.h>
#include "gems_structures.h"
#include "../meam/jammo-editing-track.h"
#include "../meam/jammo-sample.h"



void collaboration_start_pair_game(gint16 theme, gint16 variation, gems_components* data);//Start a new group and game
void collaboration_pair_game_joined(gems_components* data);//Join existing group and game
void collaboration_got_song_info(gems_components* data, const gchar song_info[]);
void collaboration_member_list_updated(gems_components* data, gems_connection* element, gboolean owner);
int collaboration_create_game();

//Functions for creating and sending messages to other users
void collaboration_add_new_sample_to_track(guint loop_id, guint slot);
void collaboration_remove_sample_from_slot(guint slot);
void collaboration_loop_sync(GList * list);
void collaboration_midi_events_to_track(GemsEventListOperation operation, gint16 instrument_id, GList * list);
void collaboration_slider_events_to_track(GemsEventListOperation operation, gint16 instrument_id, GList * list);

//Functions for decoding messages from other users
int decode_add_new_sample_to_track(char* message,int length);
int decode_remove_sample_from_slot(char* message,int length);
int decode_theme_info(char* message,int length, int id);
int decode_start_game_request(char* message,int length, int id);
int decode_start_game_response(char* message,int length, int id);

// functions for storing song file
gchar* collaboration_get_song_info();
void collaboration_set_song_info(gchar * new_song_info);
void collaboration_clear_song_info();

#endif  /* _COLLABORATION_H_ */

