/*
 * gems_service_collaboration.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Tommi Kallonen <tommi.kallonen@lut.fi>
 */

#ifndef __JAMMO_GEMS_SERVICE_COLLABORATION
#define __JAMMO_GEMS_SERVICE_COLLABORATION
 
#include "gems_structures.h"

#include "gems_definitions.h"

gboolean gems_service_collaboration_process_request(gems_connection* element);

gems_service_collaboration* gems_service_collaboration_init();

void gems_service_collaboration_cleanup();

void gems_service_collaboration_cleanup_lists();

void gems_service_collaboration_handle_action_vi_single(gems_components* data, gems_connection* element);
void gems_service_collaboration_handle_action_confirmed(gems_components* data, gems_connection* element);
void gems_service_collaboration_handle_song_info(gems_components* data, gems_connection* element);
void gems_service_collaboration_handle_action_loop(gems_components* data, gems_connection* element);
void gems_service_collaboration_handle_action_loop_sync(gems_components* data, gems_connection* element);
void gems_service_collaboration_handle_action_midi(gems_components* data, gems_connection* element);
void gems_service_collaboration_handle_action_slider(gems_components* data, gems_connection* element);

#endif
