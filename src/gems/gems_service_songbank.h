#include"gems_structures.h"
#include "gems_definitions.h"
#include"songbank_comm.h"

#ifndef __GEMS_SERVICE_SONGBANK_H__
#define __GEMS_SERVICE_SONGBANK_H__

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

#define DEBUG 1

gems_service_songbank* gems_service_songbank_init(gems_components* data, const gchar* _baseDirectory);
void gems_service_songbank_clean();

gboolean gems_service_songbank_process_request(gems_connection* element);

void gems_service_songbank_handle_search_file_req(gems_components* data, gems_connection* element);
void gems_service_songbank_handle_search_file_reply(gems_components* data, gems_connection* element);
void gems_service_songbank_handle_file_req(gems_components* data, gems_connection* element);
void gems_service_songbank_handle_file_header(gems_components* data, gems_connection* element);
void gems_service_songbank_handle_file_part(gems_components* data, gems_connection* element);

/*
 * Searchs primarily on local home directory and then, if not found, over the network
 * @param: name of the file to search. 
 * @param: author of the songfile (as defined in the xml/json structure). If NULL, considers
 * the file is not a songfile and doesn't try to check the author. 
 * @returns The path of the file on the local device (possibly after copying it from the network)
 */
char* gems_service_songbank_getFilePath(const char* filename, const char* author);
void gems_service_songbank_newConnection(const unsigned short aPort, MAbstractConnection* , int , void* );

//private methods
char* GetAuthorNameXML( char* filepath );
char* GetAuthorNameJSON( char* filepath );		//TODO write this method.
FILE* execFindCommand(const char* filename);
char* SearchFileLocal( const char* filename, const char* author );
char* SearchFileNetwork( const char* filename, const char* author );

//gems_service_songbank* sb;

//TODO callback mechanism for notifying the application of file downloaded etc.	

//TODO save to server
//TODO get local file list
//TODO get file list from some other device
//TODO filename or path in song.xml???	

#endif // __SONGBANKCOMM_H__
