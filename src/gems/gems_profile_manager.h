/*
 * gems_profile_manager.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#ifndef __GEMS_PROFILE_MANAGER_H_
#define __GEMS_PROFILE_MANAGER_H_

#include "gems_structures.h"
#include <glib.h>


/* API for Profile Manager */

// Login
gboolean gems_profile_manager_login(guint32 userId, gchar* username, gchar* passworddata);

GemsLoginAction gems_profile_manager_authenticate_default_user(const gchar* passworddata);

GemsLoginAction gems_profile_manager_authenticate_user(const gchar* username, const gchar* passworddata);

GemsChangeUserAction gems_profile_manager_set_user(const gchar* username);

gboolean gems_profile_manager_change_password(const gchar* passworddata);

gboolean gems_profile_manager_create_default_user_profile(const gchar* passworddata, guint16 age);

// Logout
gboolean gems_profile_manager_logout();

/* Get userid, if parameter is NULL return own, otherwise return value from profile 
 *
 * Parameter: profile - profile to use or NULL to retrieve own
 *
 * Returns:
 */
guint32 gems_profile_manager_get_userid(gems_peer_profile* profile);

/* Get username, if parameter is NULL return own, otherwise return value from profile 
 *
 * Parameter: profile - profile to use or NULL to retrieve own
 *
 * Returns:
 */
const gchar* gems_profile_manager_get_username(gems_peer_profile* profile);

/* Get age, if parameter is NULL return own, otherwise return value from profile 
 *
 * Parameter: profile - profile to use or NULL to retrieve own
 *
 * Returns:
 */
guint16 gems_profile_manager_get_age(gems_peer_profile* profile);

/* Get avatar id,  if parameter is NULL return own, otherwise return value from profile 
 *
 * Parameter: profile - profile to use or NULL to retrieve own
 *
 * Returns:
 */
guint32 gems_profile_manager_get_avatar_id(gems_peer_profile* profile);

/* Set avatar id,  if parameter is NULL return own, otherwise return value from profile 
 *
 * Parameter: profile - profile to use or NULL to retrieve own
 *
 * Returns: TRUE if set
 */
gboolean gems_profile_manager_set_avatar_id(guint32 avatarid);

/* Get profile of user with given id
 *
 * Parameter: id - user id 
 * 
 * Returns: profile or NULL if not found
 */
gems_peer_profile* gems_profile_manager_get_profile_of_user(guint32 id);

/* Get own firstname 
 * 
 * Returns: const gchar* - firstname
 */
const gchar* gems_profile_manager_get_firstname();

/* Get own lastname 
 * 
 * Returns: const gchar* - lstname
 */
const gchar* gems_profile_manager_get_lastname();

/* Get own points  
 * 
 * Returns: guint32 own points
 */
guint32 gems_profile_manager_get_points();

/* Add points 
 * 
 * Parameter: _value - amount of points to add 
 *
 * Returns: TRUE if changed, FALSE if not
 */
gboolean gems_profile_manager_add_points(guint32 _value);

/* Remove points 
 * 
 * Parameter: _value - amount of points to remove 
 *
 * Returns: TRUE if changed, FALSE if not
 */
gboolean gems_profile_manager_remove_points(guint32 _value);

/* Reset points (set to zero) */
void gems_profile_manager_reset_points();

/* Check if user is authenticated
 *
 * Returns: TRUE if authenticated
 */
gboolean gems_profile_manager_is_authenticated();

/* Returns the username whose profile is currently loaded
 *
 * Returns: username
 */
const gchar* gems_profile_manager_get_loaded_username();

/* Returns the profile type (PROF_TYPE_LOCAL or PROF_TYPE_REMOTE)
 * 
 * Returns: PROF_TYPE_LOCAL or PROF_TYPE_REMOTE or 0 if errors.
 */
guint8 gems_profile_manager_get_profile_type();

/* Returns the version of this profile
 * 
 * Returns: profile version as single integer
 */
guint8 gems_profile_manager_get_profile_version();

#endif
