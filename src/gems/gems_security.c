/*
 * gems_security.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */

#include "gems.h"
#include "gems_security.h"
#include "../cem/cem.h"

/**
 * gems_security_create_envelope:
 * @type: Type of the envelope
 * @element: Connection where the envelope will be sent to
 * @original: Original message
 *
 * Creates new #gems_message used for storing the envelope information.
 * Original message content is hashed and the content is added to the end of
 * the envelope (encryption not yet applied).
 *
 * Returns: new envelope as #gems_message
 */
gems_message* gems_security_create_envelope(guint16 type,gems_connection* element, gems_message* original)
{
	if(!element || !original) return NULL;
	
	gems_message* envelope = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	envelope->length = sizeof(guint16) + sizeof(guint32) + JAMMO_MESSAGE_DIGEST_SIZE + 1 + original->length;
	
	envelope->message = g_new0(gchar,sizeof(gchar) * envelope->length);
	
	// Packet type 16 bits
	*(guint16*)&envelope->message[position] = g_htons(type);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&envelope->message[position] = g_htonl(envelope->length);
	position = position + sizeof(guint32);
	
	// Hash /*Casting by Aapo, because of compile time warnings!*/
	guchar* hash = SHA1((const guchar*)(original->message),(guint32)original->length,NULL);
	g_memmove(&envelope->message[position],(gchar*)hash,JAMMO_MESSAGE_DIGEST_SIZE);
	position = position + JAMMO_MESSAGE_DIGEST_SIZE + 1;
	
	// Copy original message
	g_memmove(&envelope->message[position],original->message,original->length);
	
	return envelope;
}

/**
 * gems_security_extract_envelope
 * @element: Connection element which contains the full envelope message in its
 * network buffer
 *
 * Extract the protocol packet from the envelope. Check that the hashed content
 * matches the hash in the envelope. The message is copied from the network 
 * buffer back into the network buffer of the given @element.
 *
 * Returns: Result of the extraction as value of #GemsEnvelopeCheck
 */
GemsEnvelopeCheck gems_security_extract_envelope(gems_connection* element)
{
	gint rval = SECURITY_OK;
	
	if(!element) return -1;
	
	// [id:2|lenght:4|hash|srvid:2|length:4|cmd:2|...]
	guint32 length = g_ntohl(gems_connection_get_32(element,sizeof(guint16)));
	
	guchar* storedhash = (guchar*)gems_connection_get_data(element,sizeof(guint16)+sizeof(guint32),JAMMO_MESSAGE_DIGEST_SIZE);
	
	g_memmove(&element->nwbuffer[0],
		&element->nwbuffer[sizeof(guint16) + sizeof(guint32) + JAMMO_MESSAGE_DIGEST_SIZE + 1],
		(length - sizeof(guint16) - sizeof(guint32) - JAMMO_MESSAGE_DIGEST_SIZE - 1));

	guchar* calchash = SHA1((const guchar*)(&(element->nwbuffer)),g_ntohl(gems_connection_get_32(element,sizeof(guint16))),NULL);
	if(!gems_security_verify_hash(storedhash,calchash,JAMMO_MESSAGE_DIGEST_SIZE))
	{
		cem_add_to_log("gems_security_extract_envelope: message corrupt - hashes do not match!", J_LOG_NETWORK_DEBUG);
		rval = SECURITY_CORRUPT_MESSAGE;
	}
	else rval = SECURITY_OK;
	
	g_free(storedhash);
	return rval;
}

/**
 * gems_security_init_security:
 * @password: Salted password created with gems_security_create_password()
 * @salt: Salt used for password and also for security contexts
 *
 * Initialize the OPENSSL security components. Encryption and decryption
 * contexts are initialized using the salted @password and the @salt using
 * the gems_security_init_aes() function, hash is initialized with 
 * gems_security_init_sha().
 *
 * Returns: TRUE when all is initialized, FALSE otherwise (or when either of
 * the input parameters is NULL).
 */
gboolean gems_security_init_security(const guchar* password, const guchar* salt)
{
	cem_add_to_log("Security contexts initialization.", J_LOG_DEBUG);
	
	if(!password || !salt) return FALSE;
	
	gems_components* components = gems_get_data();
	gems_security_context* security = NULL;
	
	guint32 passlen = PASSLEN;
	
	// Not set - allocate struct
	if(components->security_context == NULL) components->security_context = g_new(gems_security_context,sizeof(gems_security_context));
	
	security = gems_get_security_contexts();
	security->encryption = OPENSSL_malloc(sizeof *security->encryption);
	security->decryption = OPENSSL_malloc(sizeof *security->decryption); //(EVP_CIPHER_CTX*)g_malloc(sizeof(EVP_CIPHER_CTX));
	// According to EVP documentation, this initializes also the content, so EVP_MD_CTX_Init() is not necessary
	security->digest = EVP_MD_CTX_create(); 
	
	// initialize aes
	// TODO cleanup contexts if errors occur
	if(!gems_security_init_aes(security->encryption, security->decryption, salt, password, passlen))
	{
		// Failures, cleanup contexts
		OPENSSL_free(security->encryption);
		security->encryption = NULL;
		
		OPENSSL_free(security->decryption);
		security->decryption = NULL;
		
		return FALSE;
	}
	
	if(!gems_security_init_sha(security->digest))
	{
		OPENSSL_free(security->digest);
		security->digest = NULL;
		return FALSE;
	}
	
	return TRUE;
}

/**
 * gems_security_change_password:
 * @password: new, salted password
 * @salt: new salt
 *
 * Initialize the security with new @password and @salt. Clears the existing
 * security contexts and calls gems_security_init_security().
 *
 * Returns: TRUE when all is initialized, FALSE otherwise (or when either of
 * the input parameters is NULL).
 */
gboolean gems_security_change_password(const guchar* password, const guchar* salt)
{
	gems_security_clear_security();
	if(gems_security_init_security(password,salt))
	{
		cem_add_to_log("gems_security_change_password() : success", J_LOG_DEBUG);
		return TRUE;
	}
	// TODO copy previous contexts in case of errors!
	else
	{
		cem_add_to_log("gems_security_change_password() : failure - re-initialize security!", J_LOG_ERROR);
		return FALSE;
	}
}

/**
 * gems_security_create_password:
 * @password_data: New plain password to be used for creating the salted
 * password
 * @salt: salt to be used with password
 *
 * Concatenates the salt into the end of the password to make rainbow table
 * attacks to be not so effective. Password must not exceed the %PASSLEN - 
 * %AES_SALT_LENGTH (56 bytes).
 *
 * Returns: new salted password
 */
guchar* gems_security_create_password(const gchar* password_data, const guchar* salt)
{
	guchar* passwd = NULL;
	
	if(password_data == NULL || salt == NULL) return NULL;
	if((strlen(password_data) + AES_SALT_LENGTH) > PASSLEN) return NULL;
	
	passwd = g_new0(guchar,sizeof(guchar)*PASSLEN);
	g_memmove(&passwd[0],password_data,strlen(password_data));
	g_memmove(&passwd[strlen(password_data)],salt,AES_SALT_LENGTH);
	
	return passwd;
}

/**
 * gems_security_clear_security:
 *
 * Clear all OPENSSL security contexts. Frees the #gems_security_context
 * structure.
 *
 * Returns: void
 */
void gems_security_clear_security()
{
	gchar* logmsg = NULL;
	gems_security_context* security = gems_get_security_contexts();
	
	// No need to clear 
	if(security == NULL) return;
	
	if(security->encryption != NULL)
	{
		if(EVP_CIPHER_CTX_cleanup(security->encryption) != 1)
		{
			logmsg = g_strdup_printf("gems_security_clear_security() : Cannot cleanup encryption context");
			cem_add_to_log(logmsg,J_LOG_ERROR);
			g_free(logmsg);
		}
		OPENSSL_free(security->encryption);
		security->encryption = NULL;
	}
	
	if(security->decryption != NULL)
	{
		if(EVP_CIPHER_CTX_cleanup(security->decryption) != 1)
		{
			logmsg = g_strdup_printf("gems_security_clear_security() : Cannot cleanup decryption context");
			cem_add_to_log(logmsg,J_LOG_ERROR);
			g_free(logmsg);
		}
		OPENSSL_free(security->decryption);
		security->decryption = NULL;
	}
	
	if(security->digest != NULL)
	{
		if(EVP_MD_CTX_cleanup(security->digest) != 1)
		{
			logmsg = g_strdup_printf("gems_security_clear_security() : Cannot cleanup message digest");
			cem_add_to_log(logmsg,J_LOG_ERROR);
			g_free(logmsg);
		}
		//OPENSSL_free(security->digest);
		// Destroy and free the memory (according to EVP documentation)
		EVP_MD_CTX_destroy(security->digest);
		security->digest = NULL;
	}
	
	g_free(gems_get_data()->security_context);
	gems_get_data()->security_context = NULL;
	
	cem_add_to_log("Security contexts cleanup.", J_LOG_DEBUG);
}

/**
 * gems_security_create_password_salt:
 *
 * Create new random salt for password. Salt is %AES_SALT_LENGTH long and is
 * created from two random 32bit integers.
 *
 * Returns: new random salt.
 */
guchar* gems_security_create_password_salt()
{
	guchar* salt = g_new0(guchar,sizeof(guchar) * AES_SALT_LENGTH);

	GRand* grand = g_rand_new_with_seed(time(NULL));

	*(guint32*)&salt[0] = g_rand_int(grand);
	*(guint32*)&salt[sizeof(guint32)] = g_rand_int(grand);

	g_free(grand);
	
	return salt;
}

/**
 * gems_security_init_aes:
 * @enc: Allocated encryption EVP context
 * @dec: Allocated decryption EVP context
 * @salt: Salt to use in context initializations
 * @keydata: Salted password data
 * @keylength: Length of the salted password data
 *
 * Initialize en- and decryption contexts using AES 256 with CBC. SHA256 is
 * used as cryptographic hash. 
 *
 * Returns: TRUE when both encryption and decryption contexts are initialized,
 * FALSE otherwise.
 */
gboolean gems_security_init_aes(EVP_CIPHER_CTX* enc, EVP_CIPHER_CTX* dec, const guchar* salt, const guchar* keydata, guint keylength)
{
	guchar key[32], iv[32];
	gchar* logmsg = NULL;
	gboolean returnvalue = TRUE;
	
	gems_guaranteed_memset(&key,0,32);
	gems_guaranteed_memset(&iv,0,32);
	
	if(EVP_BytesToKey(EVP_aes_256_cbc(),EVP_sha256(), salt, keydata, keylength, AES_ROUNDS, key, iv) != 32)
	{
		logmsg = g_strdup_printf("gems_security_init_aes() : Cannot set bytes to key.");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		returnvalue =  FALSE;
	}
		
	// Encryption context
	EVP_CIPHER_CTX_init(enc);
	if(EVP_EncryptInit_ex(enc, EVP_aes_256_cbc(), NULL, key, iv) == 0)
	{
		logmsg = g_strdup_printf("gems_security_init_aes() : Cannot initialize aes_256_cbc encryption context.");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		returnvalue =  FALSE;
	}
	
	// Decryption contexxt
	EVP_CIPHER_CTX_init(dec);
	if(EVP_DecryptInit_ex(dec, EVP_aes_256_cbc(), NULL, key, iv) == 0)
	{
		logmsg = g_strdup_printf("gems_security_init_aes() : Cannot initialize aes_256_cbc decryption context.");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		returnvalue =  FALSE;
	}
	
	gems_guaranteed_memset(&key,0,32);
	gems_guaranteed_memset(&iv,0,32);
	
	return returnvalue;
}

/**
 * gems_security_init_sha:
 * @md: Allocated EVP message digest context
 *
 * Initialize cryptographic hashing (SHA256).
 * 
 * Returns: TRUE when success, FALSE otherwise.
 */
gboolean gems_security_init_sha(EVP_MD_CTX* md)
{
	//EVP_MD_CTX_init(md); Not needed since md is created with EVP_MD_CTX_Create
	if(md == NULL) return FALSE;
	if(EVP_DigestInit_ex(md, EVP_sha256(), NULL) == 0)
	{
		gchar* logmsg = g_strdup_printf("gems_security_init_sha() : Cannot init sha digest.");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		return FALSE;
	}
	return TRUE;
}

/**
 * gems_security_calculate_hash:
 * @data: Data where hash is to be calculated
 * @data_length: Length of the data, length of the hash result is set into this
 *
 * Calculate hash from given data using SHA256 context.
 *
 * Returns: calculated hash, length is stored into @data_length.
 */
guchar* gems_security_calculate_hash(const guchar* data, guint* data_length)
{
	guint hash_length = 0;
	guchar* hash = NULL;
	gchar* logmsg = NULL;
	
	if(data == NULL)
	{
		cem_add_to_log("gems_security_calculate_hash() : Cannot calculate hash, input data is NULL.",J_LOG_ERROR);
		return NULL;
	}
	
	if((*data_length) == 0)
	{
		cem_add_to_log("gems_security_calculate_hash() : Cannot calculate hash, input data length is 0.",J_LOG_ERROR);
		return NULL;
	}
	
	gems_security_context* security = gems_get_security_contexts();
	
	if(security == NULL)
	{
		logmsg = g_strdup_printf("gems_security_calculate_hash() : Cannot calculate hash, security contexts not initialized.");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		return NULL;
	}
	
	if(security->digest == NULL)
	{
		logmsg = g_strdup_printf("gems_security_calculate_hash() : Cannot calculate hash, hash context not initialized.");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		return NULL;
	}
	
	hash = g_new0(guchar,sizeof(guchar)*EVP_MAX_MD_SIZE);
	
	// Failure, cannot initialize
	if(EVP_DigestInit_ex(security->digest, EVP_sha256(), NULL) == 0) 
	{
		logmsg = g_strdup_printf("gems_security_calculate_hash() : Cannot calculate hash, cannot initialize digest.");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		return NULL;
	}
	
	EVP_DigestUpdate(security->digest,data,(*data_length));
	EVP_DigestFinal_ex(security->digest,hash,&hash_length);
	*data_length = hash_length;
	
	return hash;
}

/**
 * gems_security_verify_hash:
 * @hash1: First hash
 * @hash2: Second hash
 * @hashlength: Length of the hashes @hash1 and @hash2 (both must be equal in 
 * length)
 *
 * Compare the hashes @hash1 and @hash2. Return TRUE if they are identical.
 *
 * Returns: TRUE when hashes are identical, FALSE otherwise.
 */
gboolean gems_security_verify_hash(const guchar* hash1, const guchar* hash2, guint32 hashlength)
{
	if(hash1 == NULL || hash2 == NULL) return FALSE;
	
	for(guint i = 0; i < hashlength ; i++) if(hash1[i] != hash2[i]) return FALSE;
	return TRUE;
}

/**
 * gems_security_encrypt_data:
 * @profile_data: Serialized profile data to enrcypt
 * @data_length: The length of the serialized profile data
 *
 * Encrypt the given serialized @profile_data using the initialized security
 * contexts and private password.
 *
 * Returns: Encrypted profile or NULL in case or errors.
 */
guchar* gems_security_encrypt_data(const guchar* profile_data, guint* data_length)
{
	EVP_CIPHER_CTX* enc = NULL;
	gint32 enc_length = 0;
	gint32 final_length = 0;
	guchar* encrypted_profile = NULL;
	gchar* logmsg = NULL;
	
	if(!profile_data)
	{
		cem_add_to_log("gems_security_encrypt_data() : Input data is not set, cannot encrypt.",J_LOG_ERROR);
		return NULL;
	}
	
	if(*data_length == 0)
	{
		cem_add_to_log("gems_security_encrypt_data() : Input data length is 0, cannot encrypt.",J_LOG_ERROR);
		return NULL;
	}
	
	if(gems_get_security_contexts() == NULL)
	{
		logmsg = g_strdup_printf("gems_security_encrypt_data() : Cannot encrypt, security contexts not initialized.");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		return NULL;
	}
	if((enc = gems_get_security_contexts()->encryption) == NULL)
	{
		logmsg = g_strdup_printf("gems_security_encrypt_data() : Cannot encrypt, decryption contexts not initialized.");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		return NULL;
	}
	
	enc_length = (*data_length) + AES_BLOCK_SIZE;
	encrypted_profile = g_new0(guchar,sizeof(guchar)*(enc_length));
	
	EVP_EncryptInit_ex(enc,NULL,NULL,NULL,NULL);
	EVP_EncryptUpdate(enc, encrypted_profile, &enc_length, profile_data, (*data_length));
	EVP_EncryptFinal_ex(enc, encrypted_profile + enc_length, &final_length);
	
	*data_length = enc_length + final_length;
	
	return encrypted_profile;
}

/**
 * gems_security_encrypt_data:
 * @profile_data: Encrypted profile data to dercypt
 * @data_length: The length of the encrypted profile data
 *
 * Decrypt the given encrypted @profile_data using the initialized security
 * contexts and private password.
 *
 * Returns: Decrypted profile in serialized form or NULL in case or errors.
 */
guchar* gems_security_decrypt_data(const guchar* profile_data, guint* data_length)
{
	EVP_CIPHER_CTX* dec = NULL;
	gint32 dec_length = 0;
	gint32 final_length = 0;
	guchar* decrypted_profile = NULL;
	gchar* logmsg = NULL;
	
	if(!profile_data)
	{
		cem_add_to_log("gems_security_decrypt_data() : Encrypted data is not set, cannot decrypt.",J_LOG_ERROR);
		return NULL;
	}
	
	if(*data_length == 0)
	{
		cem_add_to_log("gems_security_decrypt_data() : Encrypted data length is 0, cannot decrypt.",J_LOG_ERROR);
		return NULL;
	}
	
	if(gems_get_security_contexts() == NULL)
	{
		logmsg = g_strdup_printf("gems_security_decrypt_data() : Cannot decrypt, security contexts not initialized");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		return NULL;
	}
	if((dec = gems_get_security_contexts()->decryption) == NULL)
	{
		logmsg = g_strdup_printf("gems_security_decrypt_data() : Cannot decrypt, decryption context not initialized");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		return NULL;
	}
	
	dec_length = (*data_length) + AES_BLOCK_SIZE;
	decrypted_profile = g_new0(guchar,sizeof(guchar)*(dec_length));
	
	EVP_DecryptInit_ex(dec,NULL,NULL,NULL,NULL);
	EVP_DecryptUpdate(dec, decrypted_profile, &dec_length, profile_data, (*data_length));
	EVP_DecryptFinal_ex(dec, decrypted_profile + dec_length, &final_length);
	
	*data_length = dec_length + final_length;
	
	return decrypted_profile;
}

