/*
 * groupmanager.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2011 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#include <peerhood/PeerHood.h>
#include "groupmanager.h"
#include "gems_definitions.h"
#include "communication.h"
#include "gems_message_functions.h"
#include "../cem/cem.h"

/**
 * gems_new_group_info:
 * @id: Id for the group
 * @owner: User id of the group owner (creators id)
 * @type: Group type
 * @size: Amount of uses in this group
 * @spaces: Open spaces for other users
 * @theme: Id of the theme for this group
 * @starttimer: Whether to start the group advert timer or not (TRUE = start)
 *
 * Create new #gems_group_info structure with given information. Start the
 * advert timer if @starttimer is TRUE.
 *
 * Returns: A pointer to created #gems_group_info.
 */
// TODO add group lock type as parameter
gems_group_info* gems_new_group_info(guint32 id, guint32 owner, gint16 type, gint16 size, gint16 spaces, gint16 theme, gboolean starttimer)
{
	gint i = 0;
	gems_group_info* info = g_new(gems_group_info,sizeof(gems_group_info));
	
	info->id = id;
	info->owner = owner;
	info->type = type;
	info->theme = theme;
	info->size = size;
	info->is_locked = FALSE;
	info->own_state = NOT_ACTIVE;
	if(spaces == -1) info->spaces = info->size - 1;
	else info->spaces = spaces;
	
	if(starttimer == TRUE) info->group_advert_timer = g_timer_new();
	else info->group_advert_timer = NULL;
	
	for(i = 0; i < (GROUP_MAX_SIZE - 1); i++) info->peers[i] = FREE_SLOT;
	
	return info;
}

/**
 * gems_clear_group_info:
 * @info: Group info to clear
 *
 * Free group information struct.
 *
 * Returns: void
 */
void gems_clear_group_info(gems_group_info* info)
{
	if(info->group_advert_timer != NULL) g_timer_destroy(info->group_advert_timer);
	g_free(info);
	info = NULL;
}

/**
 * gems_group_get_spaces:
 *
 * Get the amount of free spaces on the current group of the user.
 *
 * Returns: Amount of free spaces in the current group or -1 if user is not in
 * any group or group information does not exist.
 */
gint gems_group_get_spaces()
{
	/*gint16 spaces = info->size - 1; // Remove self
	gint i = 0;
	for(i = 0; i < (info->size - 1); i++)	if(info->peers[i] != FREE_SLOT) spaces--;*/
	if(gems_get_data()->service_group != NULL)
	{
		gems_group_info* info = gems_get_data()->service_group->group_info;
		if(info != NULL) return info->spaces;
		else return -1;
	}
	else return -1;
}

/**
 * gems_reset_group_info:
 *
 * Reset the active group information. Sets all to default values and own state
 * to %NOT_ACTIVE.
 *
 * Returns: void
 */
void gems_reset_group_info()
{
	// TODO if the group is active or there are other people in the group inform them
	// TODO set some other as new owner if we were the owner
	gint i = 0;
	
	if(gems_get_data()->service_group == NULL) return;
	
	gems_group_info* info = gems_get_data()->service_group->group_info;
	
	if(info == NULL) return;
	
	info->id = NOT_ACTIVE;
	info->owner = 0;
	info->type = 0;
	info->theme = 0;
	info->size = 0;
	info->spaces = 0;
	info->is_locked = FALSE;
	info->own_state = NOT_ACTIVE;
	
	if(info->group_advert_timer != NULL)
	{
		g_timer_destroy(info->group_advert_timer);
		info->group_advert_timer = NULL;
	}
	
	for(i = 0; i < (GROUP_MAX_SIZE - 1); i++) info->peers[i] = FREE_SLOT;
	
	gchar* logmsg = g_strdup_printf("gems_reset_group_info: group information was reset.");
	cem_add_to_log(logmsg, J_LOG_INFO);
	g_free(logmsg);
}

/**
 * gems_group_is_in_group:
 * @id: User id to search from group
 *
 * Check if the user with given @id is in the current group.
 *
 * Returns: TRUE if user is in the group, FALSE otherwise.
 */
gboolean gems_group_is_in_group(guint32 id)
{
	if(gems_get_data()->service_group == NULL) return FALSE;
	
	gems_group_info* info = gems_get_data()->service_group->group_info;
	
	if(info == NULL) return FALSE;
	
	gint i = 0;
	for(i = 0; i < (GROUP_MAX_SIZE - 1); i++) if(info->peers[i] == id) return TRUE;
	return FALSE;
}

/**
 * gems_group_add_to_group:
 * @id: User to add to group
 *
 * Add new user with @id to group. User will be added if there are spaces left.
 *
 * Returns: #GemsGroupAction types: %CANNOT_ADD, %GROUP_FULL, %ADDED_TO_GROUP
 * or %ALREADY_IN_GROUP.
 */
GemsGroupAction gems_group_add_to_group(guint32 id)
{
	gint i = 0;
	
	if(gems_get_data()->service_group == NULL) return CANNOT_ADD;
	
	switch(gems_group_get_spaces())
	{
		case 0:
	 		return GROUP_FULL; // No space
	 	case -1:
	 		return CANNOT_ADD; // Failure
	 	default:
	 		break;
	}
	
	if(gems_group_is_in_group(id)) return ALREADY_IN_GROUP; // Already in group
	
	gems_group_info* info = gems_get_data()->service_group->group_info;
	
	if(info == NULL) return CANNOT_ADD; // No group info set - shouldn't happen
	
	for(i = 0; i < (info->size - 1); i++)
	{
		if(info->peers[i] == FREE_SLOT)
		{
			info->peers[i] = id; // add
			info->spaces--;
			return ADDED_TO_GROUP;
		}
	}
	
	return CANNOT_ADD; // No free slot found - should not happen! 
}

/**
 * gems_group_remove_from_group:
 * @id: User to be removed from group
 *
 * Remove user with @id from the current group. Free space counter is
 * incremented with one after successful removal of user.
 *
 * Returns: #GemsGroupAction types: %ERROR_CANNOT_EXEC, %REMOVED_OK or 
 * %NOT_IN_GROUP.
 */
GemsGroupAction gems_group_remove_from_group(guint32 id)
{
	if(gems_get_data()->service_group == NULL) return ERROR_CANNOT_EXEC;
	
	gems_group_info* info = gems_get_data()->service_group->group_info;
	
	if(info == NULL) return ERROR_CANNOT_EXEC;
	
	gint i = 0;
	for(i = 0; i < (info->size -1); i++)
	{
		if(info->peers[i] == id)
		{
			info->peers[i] = FREE_SLOT;
			info->spaces += 1;
			return REMOVED_OK;
		}
	}
	return NOT_IN_GROUP;
}

/**
 * gems_group_lock_group:
 * @locked: Lock state for group, TRUE = lock, FALSE = unlock
 *
 * Set new @locked state for the active group. Nothing will be done if the user
 * is not the owner of the current group. A notification is sent to every other
 * group member about the locking state change.
 *
 * Returns: void
 */
void gems_group_lock_group(gboolean locked)
{
	gchar* logmsg = NULL;
	
	if(gems_get_data()->service_group == NULL) return;
	
	gems_group_info* info = gems_get_data()->service_group->group_info;
	
	if(info == NULL) return;
	
	// Group not active
	if(gems_group_active() == FALSE)
	{
		logmsg = g_strdup_printf("gems_group_lock_group: tried to lock group which is not active, nothing is done.");
		cem_add_to_log(logmsg, J_LOG_INFO);
		g_free(logmsg);
		return;
	}
	
	// Are we the owner?
	if(info->owner == gems_profile_manager_get_userid(NULL))
	{
		info->is_locked = locked;

		gems_message* msg	= NULL;
	
		if (info->is_locked) msg = gems_create_message_group_management_notify(GROUP_IS_LOCKED,info->id,0);
		else msg = gems_create_message_group_management_notify(GROUP_IS_UNLOCKED,info->id,0);
	
		logmsg = g_strdup_printf("gems_group_lock_group: group %s", info->is_locked ? "locked" : "unlocked");
		cem_add_to_log(logmsg,J_LOG_INFO);
		g_free(logmsg);
	
		if(gems_group_send_to_group(msg) == -1)
		{
			logmsg = g_strdup_printf("gems_group_lock_group: cannot send group locking info message");
			cem_add_to_log(logmsg,J_LOG_ERROR);
			g_free(logmsg);
		}
		
		// TODO add last adverting of group with new values if locking
	
		gems_clear_message(msg);
	}
	// Allow to lock but do not inform others -> should not apply
	else
	{
		logmsg = g_strdup_printf("gems_group_lock_group: tried to %s group as other than owner", info->is_locked ? "lock" : "unlock");
		cem_add_to_log(logmsg,J_LOG_INFO);
		g_free(logmsg);
	}
	
	// TODO notify chum that group lock has changed
}

/**
 * gems_group_is_locked:
 *
 * Check if the current group is locked.
 *
 * Returns: TRUE when group is locked, FALSE when not locked.
 */
gboolean gems_group_is_locked()
{
	if(gems_get_data()->service_group == NULL) return FALSE;

	gems_group_info* info = gems_get_data()->service_group->group_info;
	
	if(info == NULL) return FALSE;
	
	return info->is_locked;
}

/**
 * gems_group_active:
 *
 * Check if the user is in active group (in state %IN_GROUP).
 *
 * Returns: TRUE when user is in active group, FALSE otherwise.
 */
gboolean gems_group_active()
{
	if(gems_get_data()->service_group == NULL) return FALSE;
	
	gems_group_info* info = gems_get_data()->service_group->group_info;
	
	if(info == NULL) return FALSE;
	
	if(info->id == NOT_ACTIVE) return FALSE; // Group not active
	else if(info->own_state == IN_GROUP) return TRUE; // Group active but we're not in it yet
	else return FALSE;
}

/**
 * gems_group_send_to_group:
 * @msg: Message to be sent to group members
 *
 * Sends a message @msg to every group member.
 *
 * Returns: 1 if no errors occur, -1 otherwise.
 */
gint gems_group_send_to_group(gems_message* msg)
{
	gint i = 0;
	gint rval = 1;
	gchar* logmsg = NULL;
	gems_components* data = gems_get_data();
	if (gems_get_peerhood() == NULL)
	{
		//TODO: Check should networking work or not. If user is playing alone, do not log error.
		/*
		logmsg = g_strdup_printf("gems_group_send_to_group: networking unavailable");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		*/
		return -1;
	}
	
	if(data->service_group == NULL)
	{
		logmsg = g_strdup_printf("gems_group_send_to_group: group service unavailable");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		return -1;
	}
	
	if(data->service_group->group_info == NULL)
	{
		logmsg = g_strdup_printf("gems_group_send_to_group: group info not set (NULL)");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		g_free(logmsg);
		return -1;
	}

	if(data->service_group->group_info->own_state != IN_GROUP)
	{
		logmsg = g_strdup_printf("gems_group_send_to_group: not accepted to group, cannot send");
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
		return -1;
	}
	
	for(i = 0; i < (data->service_group->group_info->size -1); i++)
	{
		if(data->service_group->group_info->peers[i] != FREE_SLOT)
		{
			gems_connection* element = gems_communication_get_connection_with_userid(data->communication->connections,data->service_group->group_info->peers[i]);
			
			if(element != NULL)
			{
				// TODO change to use JAMMO_PACKET_GROUP
				if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
				{
					logmsg = g_strdup_printf("gems_group_send_to_group: cannot send group message to device %s/%u user %u",
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection), 
						data->service_group->group_info->peers[i]);
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
				}
			}
			else
			{
				logmsg = g_strdup_printf("gems_group_send_to_group: no connection to user %u", data->service_group->group_info->peers[i]);
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
			}
		}
	}
	return rval; // TODO create real return values to correspond error situations
}

/**
 * gems_group_update_group_list:
 * @list: Group list to update, must contain pointers to #gems_group_info
 * structures
 * @gid: Group to update
 * @sender: Sender of the message (owner)
 * @type: Type of the group
 * @size: Group size
 * @space: Available spaces on group
 * @theme: Id of the theme of the group
 *
 * Add new or update existing group with @gid to @list of group advertisements.
 *
 * Returns: A pointer to given @list after modifications.
 */
// TODO add group lock type to parameters
GList* gems_group_update_group_list(GList* list, guint32 gid, guint32 sender, gint16 type, gint16 size, gint16 space, gint16 theme)
{
	GList* iterator = NULL;
	
	// Search the list
	for(iterator = g_list_first(list); iterator; iterator = g_list_next(iterator))
	{
		gems_group_info* info = (gems_group_info*)iterator->data;
		
		if(info == NULL) continue;
		
		// If found, update remaining space and record time
		if(info->id == gid)
		{
			info->spaces = space;
			if(info->group_advert_timer == NULL) info->group_advert_timer = g_timer_new();
			else g_timer_start(info->group_advert_timer);
			return list;
		}
	}
	
	// Not found, create new
	list = g_list_prepend(list, gems_new_group_info(gid, sender, type, size, space, theme,TRUE));
	
	return list;
}

/**
 * gems_group_list_other_groups:
 *
 * Get a copy of the group advetisement list. The list can be free'd as it is
 * only a copy, data must be left intact.
 *
 * Returns: A pointer to shallow copy of the group advertisement list.
 */
GList* gems_group_list_other_groups()
{
	if(gems_get_data()->service_group == NULL) return NULL;
	
	// Return the copy of the list - safer if the data is not touched
	return g_list_copy(gems_get_data()->service_group->other_groups);
}

/**
 * gems_group_get_owner_for_group:
 * @list: List containing group advertisements (#gems_group_info)
 * @gid: Group id to use for searching
 *
 * Search owner for given group with @gid from the @list.
 *
 * Returns: Group owners id or 0 if not found.
 */
guint32 gems_group_get_owner_for_group(GList* list, guint32 gid)
{
	GList* iterator = NULL;
	
	// Search the list
	for(iterator = g_list_first(list); iterator; iterator = g_list_next(iterator))
	{
		gems_group_info* info = (gems_group_info*)iterator->data;
		
		if(info == NULL) continue;
		
		if(info->id == gid) return info->owner;
	}
	
	return 0;
}

/**
 * gems_group_create_pair_game:
 * @theme: Id of the theme to use
 *
 * Initialize group for pair game with @theme.
 *
 * Returns: TRUE when group was created, FALSE otherwise.
 */
gboolean gems_group_create_pair_game(gint16 theme)
{
	return gems_group_init_group(GROUP_TYPE_PAIR,2,theme);
}

/**
 * gems_group_create_workshop_non_rt:
 * @theme: Id of the theme to use
 *
 * Initialize group for non realtime workshop with @theme using 4 slots.
 *
 * Returns: TRUE when group was created, FALSE otherwise.
 */
gboolean gems_group_create_workshop_non_rt(gint16 theme)
{
	return gems_group_init_group(GROUP_TYPE_WS_NON_RT,4,theme);
}

/**
 * gems_group_create_workshop_rt:
 * @theme: Id of the theme to use
 * 
 * Initialize group for realtime workshop with @theme using 2 slots.
 *
 * Returns: TRUE when group was created, FALSE otherwise.
 */
gboolean gems_group_create_workshop_rt(gint16 theme)
{
	return gems_group_init_group(GROUP_TYPE_WS_RT,2,theme);
}

/**
 * gems_group_create_workshop_net:
 * @theme: Id of the theme to use
 * 
 * Initialize group for networked workshop with @theme using 4 slots.
 *
 * Returns: TRUE when group was created, FALSE otherwise.
 */
gboolean gems_group_create_workshop_net(gint16 theme)
{
	return gems_group_init_group(GROUP_TYPE_WS_NET,4,theme);
}

/**
 * gems_group_create_chain_game:
 * @theme: Id of the theme to use
 *
 * NOT IMPLEMENTED.
 *
 * Returns: FALSE
 */
gboolean gems_group_create_chain_game(gint16 theme)
{
	return FALSE;
}

/**
 * gems_group_init_group:
 * @type: Type of the group to initialize
 * @size: Size of the group to initialize
 * @theme: Id of the theme of the group to initialize
 *
 * Initialize group with given information. Start the group adverting function
 * (add it to Glib main loop for periodic execution).
 * 
 * Returns: TRUE when group was created, FALSE otherwise.
 */
gboolean gems_group_init_group(gint16 type, gint16 size, gint16 theme)
{
	gems_components* data = gems_get_data();
	
	if(data->service_group == NULL) return FALSE;
	
	if(data->service_group->group_info->id != NOT_ACTIVE) return FALSE;
	else
	{
		// for now create a random guint32 for group id
		GRand* grand = g_rand_new_with_seed(time(NULL));
		data->service_group->group_info->id = g_rand_int(grand);
		g_rand_free(grand);
		
		data->service_group->group_info->owner = gems_profile_manager_get_userid(NULL);
		data->service_group->group_info->type = type;
		data->service_group->group_info->size = size;
		data->service_group->group_info->spaces = size - 1;
		data->service_group->group_info->theme = theme;
		data->service_group->group_info->own_state = IN_GROUP;
		data->service_group->group_info->group_advert_timer = NULL;
		
		// Start advert
		
		data->timeout_functions[11] = g_timeout_add_full(G_PRIORITY_HIGH,200,(GSourceFunc)gems_communication_advert_group, data,NULL);
		
		return TRUE;
	}
}

/** gems_group_join_to_group:
 * @gid: group id where to join
 *
 * Join to given group with @gid. Sends a join request to group owner and sets
 * the user into %JOINING state.
 *
 * Returns: #GemsGroupAction types %GROUP_LOCKED, %ALREADY_IN_GROUP, 
 * %NO_MATCHING_GROUP, %NO_OWNER, %CANNOT_CONNECT_TO_OWNER, %REQUEST_SENT,
 * %REQUEST_NOT_SENT or %ERROR_CANNOT_EXEC.
 */
GemsGroupAction gems_group_join_to_group(guint32 gid)
{	
	gems_components* data = gems_get_data();
	
	if(data->service_group == NULL) return ERROR_CANNOT_EXEC;
	if(data->service_group->group_info == NULL) return ERROR_CANNOT_EXEC;
	
	if(data->service_group->group_info->is_locked == TRUE) return GROUP_LOCKED;
	
	// We are in a group
	if(data->service_group->group_info->own_state != NOT_ACTIVE) return ALREADY_IN_GROUP;
	
	if(gid == 0) return NO_MATCHING_GROUP;
	
	// Get owner
	guint32 owner = gems_group_get_owner_for_group(data->service_group->other_groups,gid);
	
	// Owner not set
	if(owner == 0) return NO_OWNER;
	
	gems_connection* element = gems_communication_get_connection_with_userid(data->communication->connections,owner);
	
	// No connection to owner (profile not found)
	if(element == NULL) return CANNOT_CONNECT_TO_OWNER;
	
	gems_message* msg = gems_create_message_group_management_notify(JOIN_GROUP,gid,0);
	
	if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,element,msg) == FALSE)
	{
		gchar* logmsg = g_strdup_printf("gems_group_join_to_group: cannot send JOIN_GROUP to group %u owner %u (%s/%u)", 
			gid, owner, ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
		gems_clear_message(msg);
		return REQUEST_NOT_SENT;
	}
	else
	{
		data->service_group->group_info->id = gid;
		data->service_group->group_info->owner = owner;
		data->service_group->group_info->own_state = JOINING;
		
		print_group(data->service_group->group_info);
		gems_clear_message(msg);
		cem_add_to_log("Join request was succesfully sent.",J_LOG_NETWORK);
		return REQUEST_SENT;
	}
}

/** gems_group_leave_from_group:
 *
 * Leave from current group. Sends a leave request to group owner and sets the
 * user to %LEAVING state.
 *
 * Returns: #GemsGroupAction types %NOT_IN_GROUP, %CANNOT_CONNECT_TO_OWNER,
 * %REQUEST_SENT, %REQUEST_NOT_SENT or %ERROR_CANNOT_EXEC.
 */
GemsGroupAction gems_group_leave_from_group()
{
	gems_components* data = gems_get_data();
	gems_message* msg = NULL;
	
	if(data->service_group == NULL) return ERROR_CANNOT_EXEC;
	if(data->service_group->group_info == NULL) return ERROR_CANNOT_EXEC;
	
	// Group not active
	if(data->service_group->group_info->id == NOT_ACTIVE) return NOT_IN_GROUP;
	
	// Cannot leave until we're in that group
	if(data->service_group->group_info->own_state != IN_GROUP) return NOT_IN_GROUP;
	
	// Leaving as owner
	if(data->service_group->group_info->owner == gems_profile_manager_get_userid(NULL))
	{
		// If group is empty (empty spaces = size - us)
		if(data->service_group->group_info->spaces == (data->service_group->group_info->size - 1))
		{
			gems_reset_group_info(data->service_group->group_info);
			cem_add_to_log("Left from empty group as group owner.",J_LOG_INFO);
			return REQUEST_SENT;
		}
		
		// Send the latest member list
		msg = gems_create_message_group_management_group_info(MEMBER_LIST);
		
		gems_group_send_to_group(msg);
		
		gems_clear_message(msg);
		
		// Set new leader
		gint i = 0;
		
		for(i = 0; i < (data->service_group->group_info->size - 1); i++)
		{
			if(data->service_group->group_info->peers[i] != FREE_SLOT)
			{
				data->service_group->group_info->owner = data->service_group->group_info->peers[i];
				break;
			}
		}
		
		// Updated group info 
		msg = gems_create_message_group_management_group_info(CURRENT_GROUP);
		
		// Send to group members - they update the new owner
		gems_group_send_to_group(msg);
		
		cem_add_to_log("Leaving as owner of the group, updated group info sent to other members",J_LOG_NETWORK);
		
		gems_clear_message(msg);
	}
	
	// Get owner - the NEW owner if leaving as previous owner
	gems_connection* element = gems_communication_get_connection_with_userid(data->communication->connections, data->service_group->group_info->owner);
	
	if(element == NULL) return CANNOT_CONNECT_TO_OWNER;
	
	msg = gems_create_message_group_management_notify(LEAVE_GROUP,data->service_group->group_info->id,0);
	
	if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,element,msg) == FALSE)
	{
		gchar* logmsg = g_strdup_printf("gems_group_leave_from_group: cannot send LEAVE_GROUP to group %u owner %u (%s/%u)",
			data->service_group->group_info->owner, data->service_group->group_info->id, ph_c_connection_get_remote_address(element->connection),
			ph_c_connection_get_device_checksum(element->connection));
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
		gems_clear_message(msg);
		return REQUEST_NOT_SENT;
	}
	// own_status = leaving
	else 
	{
		data->service_group->group_info->own_state = LEAVING;
		gems_clear_message(msg);
		cem_add_to_log("Leave request was succesfully sent.",J_LOG_NETWORK);
		return REQUEST_SENT;
	}
}

/**
 * gems_group_disconnected_member_detected:
 * @id: User id of the disconnected member
 *
 * Remove the user with @id from the active group when the connection to the
 * user was disconnected. Inform other members about a disconnection.
 *
 * Returns: void
 */
void gems_group_disconnected_member_detected(guint32 id)
{
	gchar* logmsg = NULL;
	
	if(gems_get_data()->service_group == NULL) return;
	
	gems_group_info* info = gems_get_data()->service_group->group_info;
	
	if(info == NULL) return;
	
	// Remove user from group
	if(gems_group_remove_from_group(id) == REMOVED_OK)
	{
		gems_message* msg = gems_create_message_group_management_notify(MEMBER_DROPPED, info->id, id);
		
		// Notify other members
		if(gems_group_send_to_group(msg) == 1) logmsg = g_strdup_printf("gems_group_disconnected_member: sent notification to group about disconnected member: %u.",id);
		else logmsg = g_strdup_printf("gems_group_disconnected_member: cannot send notification to group about disconnected member (%u), not in group.",id);
	}
	else logmsg = g_strdup_printf("gems_group_disconnected_member: sent notification to group about disconnected member: %u.",id);
	
	if(logmsg)
	{
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
}

/**
 * gems_group_get_gid:
 * @group: Group info where to get the information or NULL if requesting own
 * active group information.
 *
 * Get id from the given @group or from own active group if @group is NULL.
 *
 * Returns: Id of the group as #guint32 or 0 if the information cannot be
 * accessed.
 */
guint32 gems_group_get_gid(gems_group_info* group)
{
	if(group == NULL)
	{
		if(gems_get_data()->service_group == NULL) return 0;
		if(gems_get_data()->service_group->group_info == NULL) return 0;
		return gems_get_data()->service_group->group_info->id;
	}
	else return group->id;
}

/**
 * gems_group_get_owner:
 * @group: Group info where to get the information or NULL if requesting own
 * active group information.
 *
 * Get the id of the owner for given @group or for own active group if @group
 * is NULL.
 *
 * Returns: Id of the owner of the group as #guint32 or 0 if the data cannot be
 * accessed.
 */
guint32 gems_group_get_owner(gems_group_info* group)
{
	if(group == NULL)
	{
		if(gems_get_data()->service_group == NULL) return 0;
		if(gems_get_data()->service_group->group_info == NULL) return 0;
		return gems_get_data()->service_group->group_info->owner;
	}
	else return group->owner;
}

/**
 * gems_group_get_type:
 * @group: Group info where to get the information or NULL if requesting own
 * active group information.
 *
 * Get the type of the given @group or from own active group if @group is NULL.
 *
 * Returns: Group type as #gint16 or 0 if the information cannot be accessed.
 */
gint16 gems_group_get_type(gems_group_info* group)
{
	if(group == NULL)
	{
		if(gems_get_data()->service_group == NULL) return 0;
		if(gems_get_data()->service_group->group_info == NULL) return 0;
		return gems_get_data()->service_group->group_info->type;
	}
	else return group->type;
}

/**
 * gems_group_get_theme:
 * @group: Group info where to get the information or NULL if requesting own
 * active group information.
 *
 * Get the theme of the given @group or from own active group if @group is 
 * NULL.
 *
 * Returns: Group theme as #gint16 or 0 if the information cannot be accessed.
 */
gint16 gems_group_get_theme(gems_group_info* group)
{
	if(group == NULL)
	{
		if(gems_get_data()->service_group == NULL) return 0;
		if(gems_get_data()->service_group->group_info == NULL) return 0;
		return gems_get_data()->service_group->group_info->theme;
	}
	else return group->theme;
}

/**
 * gems_group_get:
 * @group: Group info where to get the information or NULL if requesting own
 * active group information.
 *
 * Get the size of the given @group or the size of own active group if @group
 * is NULL.
 *
 * Returns: Group size as #gint16 or 0 if the information cannot be accessed.
 */
gint16 gems_group_get_size(gems_group_info* group)
{
	if(group == NULL)
	{
		if(gems_get_data()->service_group == NULL) return 0;
		if(gems_get_data()->service_group->group_info == NULL) return 0;
		return gems_get_data()->service_group->group_info->size;
	}
	else return group->size;
}

/**
 * gems_group_get:
 * @group: Group info where to get the information or NULL if requesting own
 * active group information.
 *
 * Get the amount of free spaces in the given @group or from own active group
 * if the @group is NULL.
 *
 * Returns: The amount of free spaces left, 0 also when information cannot be
 * accessed.
 */
gint16 gems_group_get_spaces_left(gems_group_info* group)
{
	if(group == NULL)
	{
		if(gems_get_data()->service_group == NULL) return 0;
		if(gems_get_data()->service_group->group_info == NULL) return 0;
		return gems_get_data()->service_group->group_info->spaces;
	}
	else return group->spaces;
}

/**
 * gems_group_get_group_members:
 *
 * Get all members in the active group.
 *
 * Returns: A pointer to #guint32 array of user ids in the active group or NULL
 * if the information cannot be accessed.
 */
guint32* gems_group_get_group_members()
{
	if(gems_get_data()->service_group == NULL) return NULL;
	if(gems_get_data()->service_group->group_info == NULL) return NULL;
	return gems_get_data()->service_group->group_info->peers;
}

