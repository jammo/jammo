/*
 * gems_service_control.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Tommi Kallonen <tommi.kallonen@lut.fi>
 */

#ifndef __JAMMO_GEMS_SERVICE_CONTROL
#define __JAMMO_GEMS_SERVICE_CONTROL
 
#include "gems_structures.h"
#include "gems_definitions.h"

gboolean gems_service_control_process_request(gems_connection* element);

gems_service_control* gems_service_control_init();

void gems_service_control_cleanup();

void gems_service_control_handle_control_mode(gems_components* data, gems_connection* element);
void gems_service_control_handle_control_ok(gems_components* data, gems_connection* element);
void gems_service_control_handle_set_login(gems_components* data, gems_connection* element);
void gems_service_control_handle_get_login(gems_components* data, gems_connection* element);
void gems_service_control_handle_force_login(gems_components* data, gems_connection* element);

#endif
