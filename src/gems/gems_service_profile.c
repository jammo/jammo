/*
 * gems_service_profile.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2011 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#include <glib.h>
#include <string.h>
#include "gems_service_profile.h"
#include "gems_message_functions.h"
#include "gems_security.h"
#include "gems_teacher_server_utils.h"
#include "communication.h"
#include "../cem/cem.h"

static void empty_callback_profile(jammo_profile_container* prof){}

/**
 * gems_service_profile_init:
 *
 * Initialize the #gems_service_profile structure and its data.
 *
 * Returns: pointer to initialized struct
 */
gems_service_profile* gems_service_profile_init()
{
	teacher_server_callback_new_profile=empty_callback_profile;
	gems_service_profile* data = g_new(gems_service_profile,sizeof(gems_service_profile));
	data->enabled = FALSE;
	data->port = 0;
	return data;
}

/**
 * gems_service_profile_cleanup():
 *
 * Cleanup the data of the profile service.
 *
 * Returns: void
 */
void gems_service_profile_cleanup()
{
	gems_service_profile* data = gems_get_data()->service_profile;
	gems_service_profile_cleanup_lists();
	g_free(data);
	data = NULL;
}

/**
 * gems_service_profile_cleanup_lists:
 *
 * Empty.
 *
 * Returns: void
 */
void gems_service_profile_cleanup_lists()
{
	// TODO clear what?
	return;
}

/**
 * gems_service_profile_process_request:
 * @element: Connection element to process.
 *
 * Process the profile request or reply to request sent by connection @element.
 * If the message is a request reply with full or public profile depending on
 * the request type and the type of the requester, only teacher can request for
 * full profile. If the message is a reply to request sent the public profile 
 * data is added to the profile of the connection @element. If the profile is a
 * full profile it is targeted to teacher and callback is called to notify a 
 * new full profile.
 * 
 * Returns: result of the processing operation as #gboolean, FALSE if the
 * request is invalid or trying to request a profile with invalid auth level.
 */
gboolean gems_service_profile_process_request(gems_connection* element)
{
	gboolean success = TRUE;
	gems_message* profilemsg = NULL;
	guint16 command = gems_connection_get_16(element,(sizeof(guint16)+sizeof(guint32)));
	guint position = 0;
	gchar* logmsg = NULL;
	
	switch(command)
	{
		case GET_PROFILE:
		
			if(gems_connection_get_16(element,(sizeof(guint16)+sizeof(guint32)+sizeof(guint16))) == GET_PROFILE_PUB)
			{
				profilemsg = gems_create_message_service_profile(PROFILE_PUB);
				
				if(profilemsg != NULL)
				{					
					// Send
					if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,element,profilemsg) == FALSE)
					{
						logmsg = g_strdup_printf("gems_service_profile_process_request: cannot write PROFILE_PUB to %s/%u",
							ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
					}
					// Free message
					gems_clear_message(profilemsg);
				}
			}
			else if(gems_connection_get_16(element,(sizeof(guint16)+sizeof(guint32)+sizeof(guint16))) == GET_PROFILE_FULL)
			{
				gems_teacher_connection* tc = gems_get_teacher_connection();
	
				// Connection to teacher exists
				if(tc)
				{
					// Is connected
					if(gems_teacher_connection_is_connected())
					{
						// Message is coming from teacher
						if(ph_c_connection_get_device_checksum(tc->connection->connection) == ph_c_connection_get_device_checksum(element->connection))
						{
							// TODO check that is authenticated as real teacher!
							
							profilemsg = gems_create_message_service_profile(PROFILE_FULL);
							
							if(profilemsg)
							{
								// Send
								if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,element,profilemsg) == FALSE)
									cem_add_to_log("gems_service_profile_process_request: cannot write PROFILE_FULL to teacher.",J_LOG_ERROR);
					
								// Free message
								gems_clear_message(profilemsg);
							}
						}
						else
						{
							cem_add_to_log("gems_service_profile_process_request: PROFILE_FULL request from other than teacher, ignored",J_LOG_NETWORK_DEBUG);
							success = FALSE;
						}
					}
					else
					{
						cem_add_to_log("gems_service_profile_process_request: PROFILE_FULL request received, no teacher connected, ignored",J_LOG_NETWORK_DEBUG);
						success = FALSE;
					}
				}
				else
				{
					cem_add_to_log("gems_service_profile_process_request: PROFILE_FULL request received, no teacher is set, ignored",J_LOG_NETWORK_DEBUG);
					success = FALSE;
				}
				
				success = TRUE;
			}
			else
			{
				logmsg = g_strdup_printf("gems_service_profile_process_request: requesting other than PROFILE_PUB/PROFILE_FULL");
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
			}
			break;
			
		case PROFILE_PUB:
			// stop timer
			g_timer_stop(element->profile_request_timer);
			
			position = sizeof(guint16) + sizeof(guint32) + sizeof(guint16);
			// Username
			gchar* uname = gems_connection_get_char(element,position);
			position = position + strlen(uname) + 1; // username is followed by '\0'
			
			// User id
			guint32 uid = gems_connection_get_32(element,position);
			position = position + sizeof(guint32);
			
			// age
			guint16 age = gems_connection_get_16(element,position);
			position = position + sizeof(guint16);
			
			// avatar id
			guint32 aid = gems_connection_get_32(element,position);
			
			// Already created?
			if(element->profile == NULL) element->profile = gems_new_peer_profile(uname,uid,age,aid);
			else gems_peer_profile_set_values(element->profile,uname,uid,age,aid);
			
			logmsg = g_strdup_printf("gems_service_profile_process_request: Got profile, [Id:%u|Name:%s|Age:%d|Avtr:%u]",
				element->profile->id,element->profile->username,element->profile->age,element->profile->avatarid);
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
			
			success = TRUE;
			break;
		case PROFILE_FULL:
			position = sizeof(gint16)+sizeof(gint32)+sizeof(gint16);
			
			gchar* username = gems_connection_get_char(element,position); // do not free, points to username in buffer!!
			position = position + strlen(username) + 1;
			
			guint32 userid = gems_connection_get_32(element,position);
			position = position + sizeof(guint32);
			
			guint16 uage = gems_connection_get_16(element,position);
			position = position + sizeof(guint16);
			
			guint32 avatarid = gems_connection_get_32(element,position);
			position = position + sizeof(guint32);
			
			gchar* firstname = gems_connection_get_char(element,position); // do not free, points to username in buffer!!
			position = position + strlen(firstname) + 1;
			
			gchar* lastname = gems_connection_get_char(element,position); // do not free, points to username in buffer!!
			position = position + strlen(lastname) + 1;
			
			guint32 points = gems_connection_get_32(element,position);
			
			logmsg = g_strdup_printf("Got full profile: userid:%u username: %s age:%d fullname:%s %s points:%u avatar:%u", 
					userid,username,uage,firstname,lastname,points,avatarid);
			cem_add_to_log(logmsg,J_LOG_INFO);
			g_free(logmsg);


			gchar* bs=g_strdup_printf("1");//to be used in gems_teacher_server_create_profile() as password and authlevel
			jammo_profile_container* prof = gems_teacher_server_create_profile(PROF_TYPE_REMOTE, userid, username, bs, bs, firstname,lastname, uage, avatarid);
			prof->points = points;
			teacher_server_callback_new_profile(prof);
			success = TRUE;
			
			break;
		default:
			success = FALSE;
			break;
	}
	
	return success;
}

/**
 * gems_service_profile_process_profileu_upload:
 * @element: Connection element to teacher containing the profile data
 *
 * Process the profile upload message incoming from the teacher; the message
 * is accepted from teacher only. The profile is in the message in encrypted
 * form in the same format as the encrypted profiles saved on the disk of the
 * device. The received profile is saved to the disk and loaded if there is no
 * user logged in. If the profile was a response to profile request sent by
 * this device the request counters and timeout functions are reset.
 * 
 * Returns: result of the processing operation as #gboolean, FALSE if the
 * request is invalid or trying to request a profile with invalid auth level.
 */
gboolean gems_service_profile_process_profile_upload(gems_connection* element)
{
	gboolean success = TRUE;
	
	gems_teacher_connection* tc = gems_get_teacher_connection();
	
	// Connection to teacher exists
	if(tc)
	{
		// Is connected
		if(gems_teacher_connection_is_connected())
		{
			// Message is coming from teacher
			if(ph_c_connection_get_device_checksum(tc->connection->connection) == ph_c_connection_get_device_checksum(element->connection))
			{
				// TODO check that is authenticated as real teacher!
				
				// Whole msg length
				guint position = sizeof(guint16);
				guint32 msglen = gems_connection_get_32(element,position);
				
				// Username length
				position = position + sizeof(guint32)+sizeof(guint16);
				guint16 unamelen = gems_connection_get_16(element,position);
				
				// Username
				position = position + sizeof(guint16);
				gchar* username = gems_connection_get_data(element,position,unamelen);
				
				// Profile length and profile
				position = position + unamelen;
				guint profilelen = msglen - position;
				gchar* profile = gems_connection_get_data(element,position,profilelen);
				
				// if logged in with same username, do not allow!
				if(gems_profile_manager_is_authenticated() && g_strcmp0(username,gems_profile_manager_get_username(NULL)) == 0)
				{
					cem_add_to_log("Got profile from teacher for user who has already logged in, discarding profile.",J_LOG_DEBUG);
				}
				else
				{
					// Write profile
					if(gems_teacher_server_save_encrypted_profile(username, profile, profilelen))
					{
						cem_add_to_log("Wrote encrypted profile to disk",J_LOG_DEBUG);
						
						// Same profile already loaded
						if(gems_profile_manager_get_loaded_username() != NULL)
						{
							if(g_strcmp0(username,gems_profile_manager_get_loaded_username()) == 0)
							{
								cem_add_to_log("Got profile from teacher for user whose profile is already loaded, clearing loaded.",J_LOG_DEBUG);
								switch(profilemanager_storage_reload_profile(gems_get_profile_manager()))
								{
									case PROFILE_LOADED:
										cem_add_to_log("Profile reloaded.",J_LOG_DEBUG);
										break;
									case PROFILE_READ_FAILURE:
										cem_add_to_log("Profile reload failure, cannot read profile.",J_LOG_DEBUG);
										break;
									case PROFILE_NOT_FOUND:
										cem_add_to_log("Cannot reload profile, profile not found ?",J_LOG_DEBUG);
										break;
									case PROFILE_VERSION_MISMATCH:
										cem_add_to_log("Cannot reload profile, version mismatch.",J_LOG_DEBUG);
										break;
									case PROFILE_RELOAD_FAILED:
										cem_add_to_log("Cannot reload profile",J_LOG_DEBUG);
										break;
									case PROFILE_RELOAD_FAILED_NOT_AUTHENTICATED:
										cem_add_to_log("Cannot reload profile, not authenticated.",J_LOG_DEBUG);
										break;
									default:
										break;
								}
							}
						}
						
						// Check if this is a response for the request
						if(gems_is_waiting_profile_from_teacher())
						{
							gchar* loginname = gems_get_login_username();
							if(loginname)
							{
								// Got the profile for our request
								if(g_strcmp0(loginname,username))
								{
									// Make sure that timeout function is removed
									if(gems_get_data()->profile_request_timeout_sid != 0) g_source_remove(gems_get_data()->profile_request_timeout_sid);
									
									// Destroy timer and set to NULL
									if(gems_get_data()->teacher_profile_request_timer)
									{
										g_timer_destroy(gems_get_data()->teacher_profile_request_timer);
										gems_get_data()->teacher_profile_request_timer = NULL;
									}
									
									// Allow to continue with login
									gems_unset_waiting_profile_from_teacher();
								}
							}
						}
					}
					else cem_add_to_log("Could not write encrypted profile to disk",J_LOG_ERROR);
				}
				
				g_free(profile);
				g_free(username);
			}
			else success = FALSE;
		}
		else success = FALSE;
	}
	else success = FALSE;
	
	return success;
}

/**
 * gems_service_profile_set_callback_profile:
 * @func_ptr: Function pointer to callback function
 *
 * Set teacher new profile callback function @func_ptr.
 *
 * Returns: void
 */
void gems_service_profile_set_callback_profile(void (*func_ptr)(jammo_profile_container* prof) ) {
	teacher_server_callback_new_profile=func_ptr;
}
