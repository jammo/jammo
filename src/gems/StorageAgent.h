/*
 * StorageAgent.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2011 Lappeenranta University of Technology
 *
 * Authors: Janne Parkkila
 * 					Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#ifndef __PM_STORAGEAGENT_H_
#define __PM_STORAGEAGENT_H_

#ifdef __cplusplus

#include <map>
#include <iostream>
#include <vector>
#include <iterator>
#include <glib.h>

extern "C" {
	#include "gems_definitions.h"
}

using namespace std;

class StorageAgent 
{
public:
	// Constructor
	StorageAgent();
	// Destructor
	~StorageAgent();
	
	// Functions
	// Synchronize the data
	gboolean Synchronize();	
	
	// Retrieves the user data from the database
	// currently just creates static data
	gboolean Retrieve(guint32 userID); 
	
	// Stores the user profile data to database
	gboolean Store(guint32 userID);
	
	// Gets the details 
	string getDetails();
	
	// Gets one detail
	string getDetail(string& detail);
	
	// Set a detail
	gboolean setDetails(string& detail, string& newvalue);
	
	// Clear all the user details
	gboolean clearDetails();
	
	gboolean isAuthenticated();
	gboolean setAuthenticated();
	gboolean isProfileLoaded();
	
	gboolean deserializeProfile(gchar* profile);
	gchar* serializeProfile();
	
	gchar* getLoadedUser();
	guchar* getSalt();
	guchar* getPasswordHash();
	guchar* getProfileHash();
	guchar* getEncryptedProfile();
	guint getEncryptedProfileSize();
	guint8 getProfileVersion();
	
	gboolean setSalt(guchar* _salt);
	gboolean setPasswordHash(guchar* _password_hash);
	gboolean setEncryptedProfile(guchar* _profile_hash, guchar* _encrypted_profile, guint _e_profile_size);
	
	void clearEncryptedData();

	GemsProfileLoadAction readProfile(const gchar* username);
	gint writeProfile(const gchar* username);
	
private:
	
	
	// The map is for storing key-value pairs of user
	// profile info and an iterator for going through the data
	map<string, string> profileDetails;
	map<string, string>::const_iterator profileIterator;
	
	guint8 version;
	gchar* loadeduser;
	guchar* salt;
	guchar* password_hash;
	guchar* profile_hash;
	guchar* encrypted_profile;
	guint encrypted_profile_size;
	
	gboolean authenticated;
	gboolean profileloaded;
	
	// This function removes the unneccesary symbols
	// From the lines read - returns amount of elements
	gint Tokenize(const string& str,
			  vector<string>& tokens,
			  const string& delimiters);
};

#endif //__cplusplus

#endif /*__PM_STORAGEAGENT_H_*/

