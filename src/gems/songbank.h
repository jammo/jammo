#ifndef __SONGBANK_H__
#define __SONGBANK_H__

#include <glib.h>
#include "communication.h"

guint GetFileFromTeacher(guint16 filetype, gchar* filename);//note! filetype currently ignored
guint SendFileToTeacher(guint16 filetype, gchar* filename);//note! filetype currently ignored
gint songbank_file_sender_loop();

#endif // __SONGBANK_H__
