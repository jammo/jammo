/*
 * gems_message_functions.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#include <glib.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>

#include "gems_message_functions.h"
#include "gems_definitions.h"
#include "groupmanager.h"
#include "gems.h"

#include "../meam/jammo-midi.h"
#include "../meam/jammo-slider-event.h"
#include "../meam/jammo-loop.h"

/**
 * gems_create_error_message:
 * @errortype: Error message type
 * @serviceid: Target/Sender service
 *
 * Create new error message with the given @errortype
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_error_message(guint16 errortype, guint16 serviceid)
{	
	// allocate struct
	gems_message* errormsg = g_new(gems_message,sizeof(gems_message));
	
	// error message = error + length + service id + error type + message
	errormsg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint16) + strlen(get_errormessage(errortype)) + 1;
	
	errormsg->message = g_new0(gchar,sizeof(gchar) * errormsg->length);
	
	guint16 error = ERROR_MSG;
	guint position = 0;
	
	*(guint16*)&errormsg->message[position] = g_htons(error); // ERROR_MSG
	position = position + sizeof(guint16);
	
	*(guint32*)&errormsg->message[position] = g_htonl(errormsg->length); // LENGTH
	position = position + sizeof(guint32);
	
	*(guint16*)&errormsg->message[position] = g_htons(serviceid); // Service id
	position = position + sizeof(guint16);
	
	*(guint16*)&errormsg->message[position] = g_htons(errortype); // Error type
	position = position + sizeof(guint16);
	
	g_strlcat(&errormsg->message[position],get_errormessage(errortype),errormsg->length - position); // Error message
	
	return errormsg;
}

/**
 * gems_clear_message:
 * @message: Message to be cleared
 *
 * Clear the given @message, free the allocated data portion and free the
 * structure.
 * 
 * Returns: void
 */
void gems_clear_message(gems_message* message)
{
	if(message != NULL)
	{
		if(message->message != NULL) g_free(message->message);
		message->message = NULL;
		g_free(message);
		message = NULL;
	}
}

/**
 * get_errormessage:
 * @errortype: Error type to get
 *
 * Gets the error message charstring of the given @errortype.
 *
 * Returns: Charstring representation of the @errortype.
 */
const gchar* get_errormessage(guint16 errortype)
{
	switch(errortype)
	{
		case ERROR_ALREADY_CONNECTED_TYPE:
			return ERROR_ALREADY_CONNECTED_MSG;
			
		case ERROR_VERSION_MISMATCH_TYPE:
			return ERROR_VERSION_MISMATCH_MSG;
			
		case ERROR_INVALID_STATE_TYPE:
			return ERROR_INVALID_STATE_MSG;
			
		case ERROR_INVALID_SERVICE_ID_TYPE:
			return ERROR_INVALID_SERVICE_ID_MSG;
			
		case ERROR_SERVICE_NOT_RUNNING_TYPE:
			return ERROR_SERVICE_NOT_RUNNING_MSG;
			
		case ERROR_INVALID_ACCESS_TYPE:
			return ERROR_INVALID_ACCESS_MSG;
			
		case ERROR_CONNECTION_REJECTED_TYPE:
			return ERROR_CONNECTION_REJECTED_MSG;
			
		case ERROR_NOT_IN_GROUP_TYPE:
			return ERROR_NOT_IN_GROUP_MSG;
		
		case ERROR_GROUP_NOT_ACTIVE_TYPE:
			return ERROR_GROUP_NOT_ACTIVE_MSG;
			
		case ERROR_INVALID_GROUP_ID_TYPE:
			return ERROR_INVALID_GROUP_ID_MSG;
			
		case ERROR_GROUP_FULL_TYPE:
			return ERROR_GROUP_FULL_MSG;
			
		case ERROR_ALREADY_IN_GROUP_TYPE:
			return ERROR_ALREADY_IN_GROUP_MSG;
			
		case ERROR_GROUP_LOCKED_TYPE:
			return ERROR_GROUP_LOCKED_MSG;
		
		case ERROR_GROUP_ADD_ERROR_TYPE:
			return ERROR_GROUP_ADD_ERROR_MSG;
			
		case ERROR_NOT_GROUP_OWNER_TYPE:
			return ERROR_NOT_GROUP_OWNER_MSG;
			
		case ERROR_NO_PROFILE_FOR_USERNAME_TYPE:
			return ERROR_NO_PROFILE_FOR_USERNAME_MSG;
			
		default:
			return ERROR_NO_ERROR;
	}
}


/**
 * gems_create_message_service_jammo:
 * @type: Message type for JamMo service
 *
 * Create new message to be sent to "JamMo" service. With type %VERSION_REPLY
 * the version of the application is added to message.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_service_jammo(guint16 type)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16);
	
	// Adding version info
	if(type == VERSION_REPLY) msg->length = msg->length + strlen(VERSION) + 1;
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(JAMMO_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(type);
	position = position + sizeof(guint16);
	
	// Adding version info
	if(type == VERSION_REPLY) g_strlcat(&msg->message[position],VERSION,msg->length - position);
	
	return msg;
}

/**
 * gems_create_message_service_profile:
 * @type: Profile service message type
 *
 * Create new message to be sent to Profile service. Usually a message for
 * replying either public (%PROFILE_PUB) or full (%PROFILE_FULL) profile is
 * created. Can be used create reply for teacher request by giving the 
 * %CONTROL_LOGIN_REPLY as type.
 *
 * Returns: new #gems_message or NULL if the message cannot be created or the
 * parameters are incorrect.
 */
gems_message* gems_create_message_service_profile(guint16 type)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	const gchar* fname = NULL;
	const gchar* lname = NULL;
	
	// TODO remove this and implement enabling of profile service only when authenticated
	if(gems_profile_manager_is_authenticated() == FALSE)
	{
		g_free(msg);
		return NULL;
	}
	
	const gchar* username = gems_profile_manager_get_username(NULL);
	
	if(!username)
	{
		g_free(msg);
		return NULL;
	}
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16);
	
	// Public parts, basic header + username length + \0 + user id + age + avatar id
	msg->length = msg->length + strlen(username) + 1 + sizeof(guint32) + sizeof(guint16) + sizeof(guint32);
	
	// Full profile
	if(type == PROFILE_FULL)
	{
		fname = gems_profile_manager_get_firstname();
		lname = gems_profile_manager_get_lastname();
		
		if(!fname || !lname)
		{
			g_free(msg);
			return NULL;
		}
		
		// Firstname + lastname + points length
		msg->length = msg->length + strlen(fname) + 1 + strlen(lname) + 1 + sizeof(guint32);
	}
		
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(PROFILE_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(type);
	position = position + sizeof(guint16);
	
	// Username
	g_strlcat(&msg->message[position],username,msg->length - position);
	position = position + strlen(username) + 1; //Add the '\0'
	
	// User id
	guint32 userid = gems_profile_manager_get_userid(NULL);
	*(guint32*)&msg->message[position] = g_htonl(userid);
	position = position + sizeof(guint32);
	
	// Age
	guint16 age = gems_profile_manager_get_age(NULL);
	*(guint16*)&msg->message[position] = g_htons(age);
	position = position + sizeof(guint16);
	
	// Avatar id
	guint32 avatarid = gems_profile_manager_get_avatar_id(NULL);
	*(guint32*)&msg->message[position] = g_htonl(avatarid);
	position = position + sizeof(guint32);
	
	if(type == PROFILE_PUB) return msg;
	if(type == CONTROL_REPLY_LOGIN) return msg;
	// add full profile info
	else if(type == PROFILE_FULL)
	{
		// firstname
		g_strlcat(&msg->message[position],fname,msg->length - position);
		position = position + strlen(fname) + 1;
		
		// lastname
		g_strlcat(&msg->message[position],lname,msg->length - position);
		position = position + strlen(lname) + 1;
		
		// points
		guint32 points = gems_profile_manager_get_points();
		*(guint32*)&msg->message[position] = g_htonl(points);
		
		return msg;
	}
	else
	{
		gems_clear_message(msg);
		return NULL; // Invalid type
	}
}

/**
 * gems_create_message_profile_request:
 * @type: Type of the profile request
 *
 * Create new profile request message to be sent to Profile service.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_profile_request(guint16 type)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint16);
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(PROFILE_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(GET_PROFILE);
	position = position + sizeof(guint16);
	
	// Type
	*(guint16*)&msg->message[position] = g_htons(type);
	
	return msg;
}

/**
 * gems_create_message_teacher_service_profile_request:
 * @username: Username of the profile to request
 *
 * Creates new profile request message to be sent to teacher.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_teacher_service_profile_request(gchar* username)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + strlen(username) + 1;
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(TEACHER_PROFILE_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(REQ_ENC_PROFILE);
	position = position + sizeof(guint16);
	
	// Username
	g_strlcat(&msg->message[position],username,msg->length - position);
	position = position + strlen(username) + 1;
	
	return msg;
}

/**
 * gems_create_message_teacher_service_profile_reply:
 * @data: Encrypted profile data
 * @length: Amount of bytes in the encrypted @data
 * @username: Username whose profile is in encrypted @data
 *
 * Create a profile reply to be sent to device who has requested this profile.
 * The profile is in encrypted format in @data which is sent in the message.
 * Usually the receiver saves the @data to disk with @username.jpf.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_teacher_service_profile_reply(gchar* data, guint length, gchar* username)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	guint16 unamelen = strlen(username) + 1; // 1 NULL to separate username and encrypted profile
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint16) + unamelen + length ;
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(TEACHER_PROFILE_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(PROFILE_ENC);
	position = position + sizeof(guint16);
	
	// Username length
	*(guint16*)&msg->message[position] = g_htons(unamelen);
	position = position + sizeof(guint16);
	
	// Username
	g_strlcat(&msg->message[position],username,msg->length - position); 
	position = position + unamelen;
	
	// Encrypted profile data
	g_memmove(&msg->message[position],data,length);
	position = position + length;
	
	return msg;
}

/**
 * gems_create_message_password_changed:
 * @newpasshash: SHA256 hash of the new password
 * @encnewpass: New password encrypted with the old password (password without
 * salt)
 * @encpasslen: Length of the encrypted new password
 * @pwlen: The length of the new password
 *
 * Create password change message to be sent to teacher. The hash and the 
 * password in encrypted form are added to message. Create the encrypted 
 * password with gems_security_encrypt_data() from saltless password. The
 * teacher server uses the decrypted password for re-encrypting the profile
 * on the teacher server.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_password_changed(guchar* newpasshash, guchar* encnewpass, guint encpasslen, guint8 pwlen)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint32) + SHA_HASH_LENGTH + encpasslen + sizeof(guint8) ;
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(TEACHER_PROFILE_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(PROFILE_PASSCHANGED);
	position = position + sizeof(guint16);
	
	// Userid 32 bits
	*(guint32*)&msg->message[position] = g_htonl(gems_profile_manager_get_userid(NULL));
	position = position + sizeof(guint32);
	
	// length of the plain password, not the best way..
	*(guint8*)&msg->message[position] = pwlen;
	position++;
	
	// hash of the new password
	g_memmove(&msg->message[position],newpasshash,SHA_HASH_LENGTH);
	position = position + SHA_HASH_LENGTH;
	
	// new password encrypted with old password
	g_memmove(&msg->message[position],encnewpass,encpasslen);
	position = position + encpasslen;
	
	return msg;
}

/**
 * gems_create_message_group_management_notify:
 * @type: Type of the group management message, use types %REQUEST_GROUP_INFO,
 * %REQUEST_MEMBERS, %JOIN_GROUP, %MEMBER_DROPPED, %NEW_MEMBER, %GROUP_UNLOCKED
 * or %GROUP_LOCKED.
 * @groupid: Id of the group this message is targetted to. This isn't necessary
 * when requesting information (%REQUEST_GROUP_INFO) or group members 
 * (%REQUEST_MEMBERS).
 * @member: Id of the member, will be used only in messages of type %NEW_MEMBER
 * or %MEMBER_DROPPED.
 *
 * Create group notify message for requesting information or delivering info.
 * Use with notify messages which do not need the group information.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_group_management_notify(guint16 type, guint32 groupid, guint32 member)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));	
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16);
	if(type != REQUEST_GROUP_INFO || type != REQUEST_MEMBERS)
	{
		msg->length = msg->length + sizeof(guint32);
		if(type == MEMBER_DROPPED || type == NEW_MEMBER) msg->length = msg->length + sizeof(guint32);
	}
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(GROUP_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(type);
	position = position + sizeof(guint16);
	
	// If not requesting group info or members, message is longer
	if(type != REQUEST_GROUP_INFO || type != REQUEST_MEMBERS)
	{
		// Group id
		*(guint32*)&msg->message[position] = g_htonl(groupid);
		position = position + sizeof(guint32);
		
		// If notifying about dropped or new member, add member
		if(type == MEMBER_DROPPED || type == NEW_MEMBER) *(guint32*)&msg->message[position] = g_htonl(member);
	}
					
	return msg;
}


/**
 * gems_create_message_group_info:
 * @type: Group information type: %OFFER_GROUP, %CURREPT_GROUP or %MEMBER_LIST
 *
 * Create group information message with given @type. Use with info messages
 * which include the group information.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_group_management_group_info(guint16 type)
{
	gems_group_info* info = gems_get_data()->service_group->group_info;
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	// service id + packet length + command id + group id
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint32);
	
	switch(type)
	{
		case OFFER_GROUP:
			// type + size + spaces + theme
			msg->length = msg->length + sizeof(guint16) + sizeof(guint16) + sizeof(guint16) + sizeof(guint16);
			break;
		case CURRENT_GROUP:
			// owner + type + size + spaces + theme
			msg->length = msg->length + sizeof(guint32) + sizeof(guint16) + sizeof(guint16) + sizeof(guint16) + sizeof(guint16);
			break;
		case MEMBER_LIST:
			// owner + member + member + member
			msg->length = msg->length + sizeof(guint32) + sizeof(guint32) + sizeof(guint32) + sizeof(guint32);
			break;
		default:
			break;
	}
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(GROUP_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(type);
	position = position + sizeof(guint16);
	
	// Group id 32 bits
	*(guint32*)&msg->message[position] = g_htonl(info->id);
	position = position + sizeof(guint32);
	
	switch(type)
	{
		case OFFER_GROUP:
			// type
			*(guint16*)&msg->message[position] = g_htons(info->type);
			position = position + sizeof(guint16);
			
			// size
			*(guint16*)&msg->message[position] = g_htons(info->size);
			position = position + sizeof(guint16);
			
			// spaces
			*(guint16*)&msg->message[position] = g_htons(gems_group_get_spaces(info));
			position = position + sizeof(guint16);
			
			// theme
			*(guint16*)&msg->message[position] = g_htons(info->theme);
			
			// TODO add group lock type into packet
			
			break;
		case CURRENT_GROUP:
			// owner
			*(guint32*)&msg->message[position] = g_htonl(info->owner);
			position = position + sizeof(guint32);
			
			// type
			*(guint16*)&msg->message[position] = g_htons(info->type);
			position = position + sizeof(guint16);
			
			// size
			*(guint16*)&msg->message[position] = g_htons(info->size);
			position = position + sizeof(guint16);
			
			// spaces
			*(guint16*)&msg->message[position] = g_htons(gems_group_get_spaces(info));
			position = position + sizeof(guint16);
			
			// theme
			*(guint16*)&msg->message[position] = g_htons(info->theme);
			
			// TODO add group lock type into packet

			break;
		case MEMBER_LIST:
			// owner
			*(guint32*)&msg->message[position] = g_htonl(info->owner);
			position = position + sizeof(guint32);
			
			// member
			*(guint32*)&msg->message[position] = g_htonl(info->peers[0]);
			position = position + sizeof(guint32);
			
			// member
			*(guint32*)&msg->message[position] = g_htonl(info->peers[1]);
			position = position + sizeof(guint32);
			
			// member
			*(guint32*)&msg->message[position] = g_htonl(info->peers[2]);

			break;
		default:
			break;
	}
	
	return msg;
}

/**
 * gems_create_message_group_info_teacher:
 * @type: Type of the group information
 * @groupinfo: Group info structure
 * @track_id: Track id to send
 *
 * Create a force group message to be sent by teacher. With the information
 * provided in this message the selected devices form a group with group
 * details provided.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_group_management_group_info_teacher(guint16 type, gems_group_info* groupinfo, guint16 track_id)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	// service id + packet length + command id + group id
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint32);

	// owner + type + size + spaces + theme + member + member + member + track_id
	msg->length = msg->length + sizeof(guint32) + sizeof(guint16) + sizeof(guint16) + sizeof(guint16) + sizeof(guint16) + sizeof(guint32) + sizeof(guint32) + sizeof(guint32) + sizeof(guint16);
	
	printf("creating force group message of length %d\n",msg->length);
	print_group(groupinfo);


	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(GROUP_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(type);
	position = position + sizeof(guint16);
	
	// member
	*(guint32*)&msg->message[position] = g_htonl(groupinfo->peers[0]);
	position = position + sizeof(guint32);
	
	// member
	*(guint32*)&msg->message[position] = g_htonl(groupinfo->peers[1]);
	position = position + sizeof(guint32);
			
	// member
	*(guint32*)&msg->message[position] = g_htonl(groupinfo->peers[2]);
	position = position + sizeof(guint32);

	// Group id 32 bits
	*(guint32*)&msg->message[position] =g_htonl(groupinfo->id);
	position = position + sizeof(guint32);

	// owner
	*(guint32*)&msg->message[position] = g_htonl(groupinfo->owner);
	position = position + sizeof(guint32);
			
	// type
	*(guint16*)&msg->message[position] = g_htons(groupinfo->type);
	position = position + sizeof(guint16);
			
	// size
	*(guint16*)&msg->message[position] = g_htons(groupinfo->size);
	position = position + sizeof(guint16);
			
	// spaces
	*(guint16*)&msg->message[position] = g_htons(gems_group_get_spaces(groupinfo));
	position = position + sizeof(guint16);
			
	// theme
	*(guint16*)&msg->message[position] = g_htons(groupinfo->theme);
	position = position + sizeof(guint16);
			
	
	

	// track_id
	*(guint16*)&msg->message[position] = g_htons(track_id);
	
	return msg;

}

/**
 * gems_create_message_collaboration_action_confirmed:
 *
 * Create action confirmed message for collaboration service.
 *
 * Returns: new #gems_message 
 */
gems_message* gems_create_message_collaboration_action_confirmed()
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));	
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16);

	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(COLLABORATION_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(ACTION_CONFIRMED);
	position = position + sizeof(guint16);
	
	
	return msg;

}

/**
 * gems_create_message_collaboration_action_vi_single:
 * @type: Type of the action
 * @png_name: Name of the png file
 * @wav_name: Name of the wav file
 * @slot: Id of the slot
 *
 * Message for adding or removing a loop - Note - currently using paths of
 * image and wav files!
 *
 * Returns: new #gems_message 
 */
gems_message* gems_create_message_collaboration_action_vi_single(guint16 type, const gchar png_name[], const gchar wav_name[], guint16 slot)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));	
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint16);

	msg->length = msg->length + strlen(png_name) + 1 + strlen(wav_name) + 1 + sizeof(guint16);
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);

	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(COLLABORATION_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(ACTION_VI_SINGLE);
	position = position + sizeof(guint16);

	// Type 16 bits
	*(guint16*)&msg->message[position] = g_htons(type);
	position = position + sizeof(guint16);
	
	// png_name TODO should this be id?
	g_strlcat(&msg->message[position],png_name,msg->length - position);
	position = position + strlen(png_name)+1 ;

	// wav_name TODO should this be id?
	g_strlcat(&msg->message[position],wav_name,msg->length - position);
	position = position + strlen(wav_name)+1 ;

	// slot
	*(guint16*)&msg->message[position] = g_htons(slot);
	position = position + sizeof(guint16);
	
	return msg;
}

/**
 * gems_create_message_collaboration_action_song_info: 
 * @type: Not used
 * @song_info: Song file data
 *
 * Message for transferring the song file - used when starting a game (or
 * synchronizing after disconnection?) 
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_collaboration_action_song_info(guint16 type, const gchar song_info[])
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));	
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16);

	msg->length = msg->length + strlen(song_info) + 1;
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);

	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(COLLABORATION_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(SONG_INFO);
	position = position + sizeof(guint16);
	
	// song_info
	g_strlcat(&msg->message[position],song_info,msg->length - position);
	position = position + strlen(song_info)+1 ;

	
	return msg;
}

/**
 * gems_create_message_collaboration_action_loop:
 * @type: Type of the loop action
 * @loop_id: Loop id to add or remove
 * @slot: The position
 *
 * Message for adding or removing a loop in a game. Message to be sent to
 * collaboration service.
 *
 * Returns: new #gems_message 
 */
gems_message* gems_create_message_collaboration_action_loop(guint16 type, guint loop_id, guint slot)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));	
	guint position = 0;

	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint) + sizeof(guint) + sizeof(guint16) + sizeof(guint);
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);

	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(COLLABORATION_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(ACTION_LOOP);
	position = position + sizeof(guint16);

	// User id 32 bits
	*(guint*)&msg->message[position] = g_htonl(gems_profile_manager_get_userid(NULL));
	position = position + sizeof(guint);

	// TODO LOOP ID
	// slot 32 bits
	*(guint*)&msg->message[position] = g_htonl(loop_id);
	position = position + sizeof(guint);

	// Type 16 bits
	*(guint16*)&msg->message[position] = g_htons(type);
	position = position + sizeof(guint16);
	
	// slot 32 bits
	*(guint*)&msg->message[position] = g_htonl(slot);
	position = position + sizeof(guint);
	
	return msg;
}

/**
 * gems_create_message_collaboration_action_loop_sync:
 * @list: List of #JammoLoop:s
 *
 * Create a message for collaboration service for synchronizing the song
 * contents. The @list will iterated and each loop id and slot number is
 * added to message.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_collaboration_action_loop_sync(GList * list)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));	
	guint position = 0;

	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint) + g_list_length(list) * (2*sizeof(guint32));
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);

	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(COLLABORATION_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(ACTION_LOOP_SYNC);
	position = position + sizeof(guint16);

	// User id 32 bits
	*(guint*)&msg->message[position] = g_htonl(gems_profile_manager_get_userid(NULL));
	position = position + sizeof(guint);

	// list, one loop is 8 bytes
	GList * temp;
	JammoLoop * loop;
	for (temp=g_list_first(list);temp;temp=temp->next) {
		loop = temp->data;
		// loop id
		*(guint32 *)&msg->message[position] = g_htonl(loop->loop_id);
		position = position + sizeof(guint32);
		// slot
		*(guint32 *)&msg->message[position] = g_htonl(loop->slot);
		position = position + sizeof(guint32);
	}
	
	return msg;
}

/**
 * gems_create_message_collaboration_action_midi_series:
 * @type: Type of the midi action
 * @instrument_id: Id of the midi instrument that was used
 * @list: List of #JammoMidiEvent events 
 *
 * Creates a midi series message to be sent to collaboration service. Adds
 * every element data in the @list into message.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_collaboration_action_midi_series(guint16 type, guint16 instrument_id, GList * list)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));	
	guint position = 0;

	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint) + sizeof(guint16) + sizeof(guint16) + g_list_length(list) * (sizeof(unsigned char)+sizeof(GstClockTime)+sizeof(JammmoMidiEventType));
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);

	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(COLLABORATION_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(ACTION_MIDI);
	position = position + sizeof(guint16);

	// User id 32 bits
	*(guint*)&msg->message[position] = g_htonl(gems_profile_manager_get_userid(NULL));
	position = position + sizeof(guint);

	// TODO INSTRUMENT ID
	// 16 bits
	*(guint*)&msg->message[position] = g_htons(instrument_id);
	position = position + sizeof(guint16);

	// Type 16 bits
	*(guint16*)&msg->message[position] = g_htons(type);
	position = position + sizeof(guint16);
	
	// list, one event is 13 bytes
	GList * temp;
	JammoMidiEvent * event;
	for (temp=g_list_first(list);temp;temp=temp->next) {
		event = temp->data;
		// note
		msg->message[position] = event->note;
		position = position + sizeof(unsigned char);
		// timestamp
		*(GstClockTime *)&msg->message[position] = GUINT64_TO_BE(event->timestamp);
		position = position + sizeof(GstClockTime);
		// type
		*(JammmoMidiEventType *)&msg->message[position] = g_htonl(event->type);
		position = position + sizeof(JammmoMidiEventType);		
	}
	
	return msg;
}

/**
 * gems_create_message_collaboration_action_slider_series:
 * @type: Type of the action
 * @instrument_id: Id if the slider instrument used
 * @list: List of #JammoSliderEvent:s
 *
 * Create a slider event series message to be sent to collaboration service.
 * Every provided #JammoSliderEvent is added into the message.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_collaboration_action_slider_series(guint16 type, guint16 instrument_id, GList * list)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));	
	guint position = 0;

	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint) + sizeof(guint16) + sizeof(guint16) + g_list_length(list) * (sizeof(guint32)+sizeof(GstClockTime)+sizeof(JammoSliderEventType));
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);

	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(COLLABORATION_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(ACTION_SLIDER);
	position = position + sizeof(guint16);

	// User id 32 bits
	*(guint*)&msg->message[position] = g_htonl(gems_profile_manager_get_userid(NULL));
	position = position + sizeof(guint);

	// TODO INSTRUMENT ID
	// 16 bits
	*(guint16*)&msg->message[position] = g_htons(instrument_id);
	position = position + sizeof(guint16);

	// Type 16 bits
	*(guint16*)&msg->message[position] = g_htons(type);
	position = position + sizeof(guint16);
	
	// list, one event is 16 bytes
	GList * temp;
	JammoSliderEvent * event;
	for (temp=g_list_first(list);temp;temp=temp->next) {
		event = temp->data;
		// freq, float converted to guint32 and packed
		*(guint32*)&msg->message[position] = htonf(event->freq);
		position = position + sizeof(guint32);
		// timestamp
		*(GstClockTime *)&msg->message[position] = GUINT64_TO_BE(event->timestamp);
		position = position + sizeof(GstClockTime);
		// type
		*(JammoSliderEventType *)&msg->message[position] = g_htonl(event->type);
		position = position + sizeof(JammoSliderEventType);		
	}
	
	return msg;
}

/**
 * gems_create_message_control_mode:
 * @type: Control mode type
 * @cntrl_type: Lock type
 * @lock_mode: Lock mode
 *
 * Create new mode control message.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_control_mode(guint16 type, guint16 cntrl_type, guint16 lock_mode)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));	
	guint position = 0;

	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint16) + sizeof(guint16);

	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);

	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(CONTROL_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(CONTROL_MODE);
	position = position + sizeof(guint16);

	// Control type 16 bits
	*(guint16*)&msg->message[position] = g_htons(cntrl_type);
	position = position + sizeof(guint16);

	// Lock mode 16 bits
	*(guint16*)&msg->message[position] = g_htons(lock_mode);
	position = position + sizeof(guint16);

	return msg;
}

/**
 * gems_create_message_control_ok:
 * @type: Reply type
 *
 * Create a reply to %CONTROL_MODE message.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_control_ok(guint16 type)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));	
	guint position = 0;

	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16);

	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);

	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(CONTROL_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(CONTROL_OK);
	position = position + sizeof(guint16);

	return msg;
}

/**
 * gems_create_message_control_set_login_name:
 * @username: Username to set
 *
 * Create control message for setting the login username on receiver.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_control_set_login_name(gchar* username)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + strlen(username) + 1;
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(CONTROL_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(CONTROL_SET_LOGIN);
	position = position + sizeof(guint16);
	
	// Username
	g_strlcat(&msg->message[position],username,msg->length - position);
	position = position + strlen(username) + 1;
	
	return msg;
}

/**
 * gems_create_message_control_force_login:
 * @password: PLAIN password
 * @passlen: Length of the password
 *
 * Create a force login message with PLAIN password to be used for logging the
 * user in on the receiver device.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_control_force_login(gchar* password, guint8 passlen)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + passlen + 1 + 1;
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(CONTROL_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(CONTROL_FORCE_LOGIN);
	position = position + sizeof(guint16);
	
	// passlength
	*(guint8*)&msg->message[position] = passlen;
	position++;

	// password
	g_memmove(&msg->message[position],password,passlen);
	
	return msg;
}

/**
 * gems_create_message_control_get_login:
 *
 * Create message for requesting the login username from the device. Used by
 * the teacher server.
 *
 * Returns: new #gems_message or NULL if the message cannot be created or the
 * parameters are incorrect.
 */
gems_message* gems_create_message_control_get_login()
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16);
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(CONTROL_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(CONTROL_GET_LOGIN);
	position = position + sizeof(guint16);
	
	return msg;
}

/**
 * gems_create_message_mentor_log_message:
 * @type: Type of the logging message
 * @message: The log message
 *
 * Create a logging message to be sent to the mentor service on teacher server.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_message_mentor_log_message(guint16 type, char *message)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + strlen(message) + 1;
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(MENTOR_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(type);
	position = position + sizeof(guint16);
	
	// Log message
	g_strlcat(&msg->message[position],message,msg->length - position);
	
	return msg;


}

gems_message* gems_create_message_songbank_search_file_req(guint16 type, char *filename)//Type = filetype (gems_definitions.h)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint16) + strlen(filename) + 1;
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(SONGBANK_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(SEARCH_FILE_REQ);
	position = position + sizeof(guint16);

	// Filetype 16 bits
	*(guint16*)&msg->message[position] = g_htons(type);
	position = position + sizeof(guint16);
	
	// File name
	g_strlcat(&msg->message[position],filename,msg->length - position);
	
	return msg;
}

gems_message* gems_create_message_songbank_search_file_reply(guint16 type, guint16 status)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16);
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(SONGBANK_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(SEARCH_FILE_REPLY);
	position = position + sizeof(guint16);

	// Status 16 bits
	*(guint16*)&msg->message[position] = g_htons(status);
	
	return msg;
}

gems_message* gems_create_message_songbank_file_req(guint16 type, char *filename)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint16)+ strlen(filename) + 1;
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(SONGBANK_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(FILE_REQ);
	position = position + sizeof(guint16);

	// Filetype 16 bits
	*(guint16*)&msg->message[position] = g_htons(type);
	position = position + sizeof(guint16);
	
	// File name
	g_strlcat(&msg->message[position],filename,msg->length - position);
	
	return msg;
}

gems_message* gems_create_message_songbank_file_header(guint16 type, guint16 size, char *filename)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint16) + sizeof(guint16) + strlen(filename) + 1;
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(SONGBANK_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(FILE_HEADER);
	position = position + sizeof(guint16);

	// Filetype 16 bits
	*(guint16*)&msg->message[position] = g_htons(type);
	position = position + sizeof(guint16);

	// File size 16 bits
	*(guint16*)&msg->message[position] = g_htons(size);
	position = position + sizeof(guint16);
	
	// File name
	g_strlcat(&msg->message[position],filename,msg->length - position);
	
	return msg;
}

gems_message* gems_create_message_songbank_file_part(guint16 type, guint16 size, guint16 id,char *filecontents)
{
	gems_message* msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint16) + sizeof(guint16) + size + 1 + 1;
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(SONGBANK_SERVICE_ID);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(FILE_PART);
	position = position + sizeof(guint16);
	
	// Length of file contents in this message 16 bits
	*(guint16*)&msg->message[position] = g_htons(size);
	position = position + sizeof(guint16);

	// File contents id 16 bits
	*(guint16*)&msg->message[position] = g_htons(id);
	position = position + sizeof(guint16);

	// file contents
	g_memmove(&msg->message[position],filecontents,size);
	
	return msg;
}

/**
 * gems_create_general_message:
 * @msg: Already allocated message or NULL when new #gems_message will be
 * allocated
 * @serviceid: Service id where the message is targetted to (2 first bytes)
 * @length: Length of the whole message to be initialized (4 next bytes)
 * @type: Command type of the message to be initialized (2 next bytes)
 *
 * Creates the basic #gems_message with given @serviceid and @type, allocated
 * to withold @length amount of bytes. Sets the 8 first bytes of the message
 * according to protocol.
 *
 * Returns: new #gems_message
 */
gems_message* gems_create_general_message(gems_message* msg, guint16 serviceid, guint32 length, guint16 type)
{
	if(msg == NULL) msg = g_new(gems_message,sizeof(gems_message));
	guint position = 0;
	
	if(length == 0) msg->length = sizeof(guint16) + sizeof(guint32) + sizeof(guint16);
	else msg->length = length;
	
	msg->message = g_new0(gchar,sizeof(gchar) * msg->length);
	
	// Service id 16 bits
	*(guint16*)&msg->message[position] = g_htons(serviceid);
	position = position + sizeof(guint16);
	
	// Length 32 bits
	*(guint32*)&msg->message[position] = g_htonl(msg->length);
	position = position + sizeof(guint32);
	
	// Command 16 bits
	*(guint16*)&msg->message[position] = g_htons(type);
	
	return msg;
}
