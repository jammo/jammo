/*
 * gems_service_group_management.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */

#ifndef __JAMMO_GEMS_SERVICE_GROUP_MANAGEMENT
#define __JAMMO_GEMS_SERVICE_GROUP_MANAGEMENT
 
#include "gems_structures.h"

#include "gems_definitions.h"

gboolean gems_service_group_process_request(gems_connection* element);

gems_service_group* gems_service_group_init();

void gems_service_group_cleanup();
void gems_service_group_cleanup_lists();

void gems_service_group_handle_offer_group(gems_components* data, gems_connection* element);
void gems_service_group_handle_current_group(gems_components* data, gems_connection* element);
void gems_service_group_handle_request_members(gems_components* data, gems_connection* element);
void gems_service_group_handle_request_group_info(gems_components* data, gems_connection* element);
void gems_service_group_handle_join_group(gems_components* data, gems_connection* element);
void gems_service_group_handle_leave_group(gems_components* data, gems_connection* element);
void gems_service_group_handle_removed_from_group(gems_components* data, gems_connection* element);
void gems_service_group_handle_member_dropped(gems_components* data, gems_connection* element);
void gems_service_group_handle_new_member(gems_components* data, gems_connection* element);
void gems_service_group_handle_member_list(gems_components* data, gems_connection* element);
void gems_service_group_handle_unlocked(gems_components* data, gems_connection* element);
void gems_service_group_handle_locked(gems_components* data, gems_connection* element);
void gems_service_group_handle_force_group(gems_components* data, gems_connection* element);
#endif
