/*
 * gems_service_control.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2011 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Tommi Kallonen <tommi.kallonen@lut.fi>
 *          Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */


#include "gems_service_control.h"
#include "gems_message_functions.h"
#include "communication.h"
#include "../cem/cem.h"

/**
 * gems_service_control_process_request:
 * @element: Connection to process
 *
 * Process incoming messages from teacher to control service. Calls handler
 * functions to react to the message based on the command in the message.
 *
 * Returns: TRUE when connection to teacher did exist and the message was
 * processed, FALSE otherwise.
 */
gboolean gems_service_control_process_request(gems_connection* element)
{
	gems_components* data = gems_get_data();
	
	gems_teacher_connection* tc = gems_get_teacher_connection();
	
	// Connection to teacher exists, control messages should be coming from teacher only!
	if(tc)
	{
		// Is connected
		if(ph_c_connection_is_connected(tc->connection->connection) == TRUE)
		{
			// Message is coming from teacher
			if(ph_c_connection_get_device_checksum(tc->connection->connection) == ph_c_connection_get_device_checksum(element->connection))
			{	
				switch(gems_connection_get_16(element,6)) // command
				{
					case CONTROL_MODE:
						gems_service_control_handle_control_mode(data,element);
						break;
					case CONTROL_OK:
						gems_service_control_handle_control_ok(data,element);
						break;
					case CONTROL_SET_LOGIN:
						gems_service_control_handle_set_login(data,element);
						break;
					case CONTROL_GET_LOGIN:
						gems_service_control_handle_get_login(data,element);
						break;
					case CONTROL_FORCE_LOGIN:
						gems_service_control_handle_force_login(data,element);
						break;
					default:
						break;
				}
			}
			else
			{
				cem_add_to_log("gems_service_control_process_request: Got control message from some other than teacher, ignoring.",J_LOG_NETWORK_DEBUG);
				return FALSE;
			}
		}
		else
		{
			cem_add_to_log("gems_service_control_process_request: Got control message but no teacher is connected, ignoring.",J_LOG_NETWORK_DEBUG);
			return FALSE;
		}
	}
	else
	{
		cem_add_to_log("gems_service_control_process_request: Got control message but teacher is not initialized, ignoring.",J_LOG_NETWORK_DEBUG);
		return FALSE;
	}
	
	return TRUE;
}

/**
 * gems_service_control_init:
 *
 * Initialize control service data structures.
 * 
 * Returns: a pointer to created #gems_service_control struct
 */
gems_service_control* gems_service_control_init()
{
	gems_service_control* data = g_new(gems_service_control,sizeof(gems_service_control));
	
	data->enabled = FALSE;

	data->active=FALSE;
	
	data->port = 0;

	return data;
}

/**
 * gems_service_control_cleanup:
 *
 * Clean the control service data structure.
 *
 * Returns: void
 */
void gems_service_control_cleanup()
{
	gems_service_control* data = gems_get_data()->service_control;
	
	
	if(data != NULL)
	{
		g_free(data);
		data = NULL;
	}
}

/**
 * gems_service_control_handle_control_mode:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process the control mode change message.
 *
 * Returns: void
 */
void gems_service_control_handle_control_mode(gems_components* data, gems_connection* element)
{
	gchar* logmsg = NULL;
	guint position=8;
	guint16 cntrl_type;
	guint16 lock_mode;

	// Control type
	cntrl_type = gems_connection_get_16(element,position);
	position = position + sizeof(guint16);

	// Lock mode
	lock_mode = gems_connection_get_16(element,position);
	position = position + sizeof(guint16);

	// TODO do something with new control mode
	// TODO use gems_set_waiting_for_teacher() when lock_mode == lock
	// TODO use gems_unset_waiting_for_teacher() when lock_mode == unlock

	logmsg = g_strdup_printf("gems_service_control_handle_control_mode: got CONTROL_MODE ");
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
}

/**
 * gems_service_control_handle_:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process the control ok message.
 *
 * Returns: void
 */
void gems_service_control_handle_control_ok(gems_components* data, gems_connection* element)
{
	gchar* logmsg = NULL;

	logmsg = g_strdup_printf("gems_service_control_handle_control_ok: got CONTROL_OK ");
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
}

/**
 * gems_service_control_handle_:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process the set login message. If the user is not authenticated load the
 * profile with username provided in the message. If the user is authenticated
 * only the login name is changed at the moment and logout is not forced hence
 * no callback for it.
 *
 * Returns: void
 */
void gems_service_control_handle_set_login(gems_components* data, gems_connection* element)
{
	gchar* logmsg = NULL;
	guint position = sizeof(guint16)+sizeof(guint32)+sizeof(guint16);
	guint32 length = gems_connection_get_32(element,sizeof(guint16)) - position; // length - header
	
	gchar* loginname = gems_connection_get_data(element,position,length);
	
	// TODO if logged in, logout the user AND INFORM ! -> CALLBACK TO CHUM? Return to login screen and stop all activity?
	// if(gems_profile_manager_logout()) cem_add_to_log("gems_service_control_handle_set_login: logged out previous user",J_LOG_DEBUG);
	
	if(!gems_profile_manager_is_authenticated())
	{
		logmsg = g_strdup_printf("gems_service_control_handle_set_login: username \"%s\" will be used as login name.",loginname);
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
		
		// load profile with loginname
		switch(gems_profile_manager_set_user(loginname))
		{
			// Found & loaded
			case CHANGE_USER_OK:
				cem_add_to_log("gems_service_control_handle_set_login: profile found",J_LOG_DEBUG);
				break;
			// Not found but request sent
			case CHANGE_USER_WAITING_PROFILE:
				cem_add_to_log("gems_service_control_handle_set_login: profile requested.",J_LOG_DEBUG);
				break;
			// Should not happen!
			case CHANGE_USER_NO_USERNAME:
				cem_add_to_log("gems_service_control_handle_set_login: teacher sent username as NULL.",J_LOG_DEBUG);
				break;
			default:
				break;
		}
	}
	else
	{
		gems_set_login_username(loginname,TRUE);
		cem_add_to_log("gems_service_control_handle_set_login: user logged in, the username will be changed after logout",J_LOG_DEBUG);
	}
	g_free(loginname);
}

/**
 * gems_service_control_handle_get_login:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process get login request message. Reply with %CONTROL_REPLY_LOGIN type
 * profile service message. If user is not logged in create a message which
 * contains only the basic header portion of the packet - this should tell
 * the teacher server that user is not yet logged in.
 *
 * Returns: void
 */
void gems_service_control_handle_get_login(gems_components* data, gems_connection* element)
{
	gems_message* msg = gems_create_message_service_profile(CONTROL_REPLY_LOGIN);
	
	// not logged in, msg is NULL -> create a general message with basic header only
	if(msg == NULL) msg = gems_create_general_message(NULL,PROFILE_SERVICE_ID,0,CONTROL_REPLY_LOGIN);
		
	if(!gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,element,msg))
		cem_add_to_log("gems_service_control_handle_get_login: cannot write login name reply as public profile to teacher",J_LOG_NETWORK_DEBUG);
	else cem_add_to_log("gems_service_control_handle_get_login: wrote login name reply to teacher", J_LOG_NETWORK_DEBUG);

}

/**
 * gems_service_control_handle_force_login:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process force login message. Get the password from the message and use the
 * set username and password pair to log the user in.
 *
 * Returns: void
 */
void gems_service_control_handle_force_login(gems_components* data, gems_connection* element)
{
	guint position = sizeof(guint16)+sizeof(guint32)+sizeof(guint16);
	guint8 length = *(guint8*)&element->nwbuffer[position];
	position++;
	
	gchar* password = gems_connection_get_data(element,position,length);

	if(gems_get_login_username())
	{
		if(gems_profile_manager_authenticate_user(gems_get_login_username(),password) == LOGIN_OK) cem_add_to_log("forced login: succesfully logged in",J_LOG_DEBUG);
		else cem_add_to_log("forced login: cannot login",J_LOG_DEBUG);
	}

	g_free(password);
}

