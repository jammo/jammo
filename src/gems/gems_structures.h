/*
 * gems_structures.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2011 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 * 	    Tommi Kallonen  <tommi.kallonen@lut.fi>		
 */

#ifndef __GEMS_STRUCTURES_
#define __GEMS_STRUCTURES_

#include <peerhood/PeerHood.h>
#include <peerhood/IteratorWrapper.h>
#include <glib.h>
#include <glib-object.h>
#include <netinet/in.h>
#include <openssl/evp.h>
#include "gems_definitions.h"
#include "ProfileManager.h"

#define TO_FUNCTIONS 12

/**
 * jammo_state:
 * @JAMMO_CONNECTING: New connection added, waiting for CONNECT_REQ
 * @JAMMO_WAITING_VERSION: VERSION_REQ sent, waiting for VERSION_REPLY
 * @JAMMO_WAITING_REQUEST: CONNECT_REPLY sent, waiting for VERSION_REQ
 * @JAMMO_WAITING_REPLY: VERSION_REPLY sent, waiting for CONNECT_REPLY
 * @JAMMO_VERSION_MISMATCH: versions mismatch - ERROR is sent
 * @JAMMO_CONNECTED_NO_AUTH: CONNECT_REPLY received, device is connected
 * @JAMMO_CONNECTED_AUTH: Authenticated - ready to roll
 *
 * States of connecting devices
 */
typedef enum {
	JAMMO_CONNECTING, 				// Added, waiting for CONNECT_REQ
	JAMMO_WAITING_VERSION, 		// VERSION_REQ sent, waiting for VERSION_REPLY
	JAMMO_WAITING_REQUEST,		// CONNECT_REPLY sent, waiting for VERSION_REQ
	JAMMO_WAITING_REPLY,			// VERSION_REPLY sent, waiting for CONNECT_REPLY
	JAMMO_VERSION_MISMATCH,		// versions mismatch - ERROR is sent
	JAMMO_CONNECTED_NO_AUTH,	// CONNECT_REPLY received, device is connected
	JAMMO_CONNECTED_AUTH			// Authenticated - ready to roll
	// TODO authentication states
} jammo_state;

/**
 * gems_peer_profile:
 * @id: user id #guint32
 * @username: username
 * @avatarid: id of the users avatar as #guint32
 * @age: age of the user as #guint16
 *
 * Profile of some other user
 */
typedef struct {
	guint32 id;
	gchar* username;
	guint32 avatarid;
	guint16 age;
} gems_peer_profile;


/**
 * gems_group_info:
 * @id: Id of the group as #guint32
 * @owner: Id of the group owner as #guint32
 * @type: Group type as #guint16 , types defined in gems_definitions.h
 * @theme: Theme used by the group
 * @size: The size of the group as #guint16 . Is always less or equal to 
 * #GROUP_MAX_SIZE
 * @peers: Array of ids of other group members, size is #GROUP_MAX_SIZE - 1
 * @spaces: Amount of free slots in the group
 * @is_locked: a #gboolean for group locking state
 * @own_state: Own status in this group as #GemsGroupStatus
 *
 * Group information struct. Used for storing adverted group info as well as
 * own group information. The information can be accessed using the
 * gems_group_get_* functions defined in groupmanager.h.
 */
// Group info
typedef struct {
	// Group id
	guint32 id;
	// Group owner;
	guint32 owner;
	// Group type, TODO change type #defines into enum and use it here
	gint16 type;
	// Group theme id, TODO change theme #defines into enum and use it here
	gint16 theme;
	// Size of the group (2 = pair game -> 1 available, 4 = group -> 3 available)
	gint16 size;
	// Other members
	guint32 peers[GROUP_MAX_SIZE-1]; // TODO change to use gems_connection* - less list operations
	// free spaces
	gint16 spaces;
	// Locked?
	gboolean is_locked;
	// own status in this group (IN_GROUP, JOINING, LEAVING or NONE )
	GemsGroupStatus own_state;
	// For periodic group advertizing and checking when the last advert was received
	GTimer* group_advert_timer; 
} gems_group_info;


/**
 * gems_connection: 
 * @connection: The connection object, struct which points to PeerHood
 * MAbstractConnection C++ object. Contains the connection socket.
 * @game_connection: The game connection object, struct which points to PeerHood
 * MAbstractConnection C++ object. Contains the connection socket.
 * @port: Port used when connected to this peer. Unused.
 * @connectionid: Connection id gotten when connected to this peer. Unused.
 * @connection_state: State of the connection as #jammo_state
 * @jammoid: UNUSED
 * @profile: Public profile of this peer, for accessing this structure use
 * gems_profile_manager_get_* functions defined in gems_profile_manager.h
 * @profile_requests: Amount of profile requests sent to this peer as #guint16
 * @profile_request_timer: Timer for profile requests as #GTimer
 * @group_request_timer: Timer for group requests as #GTimer
 * @last_action: Timer for last action for this connection as #GTimer
 * @nwbuffer: Network buffer, size is JAMMO_NETWORK_BUFFER_MAX_SIZE
 * @add_position: Position to add data into network buffer as #guint
 *
 * Connection item, contains data for one single connection to a peer.
 */
typedef struct {
	MAbstractConnection* connection;
	MAbstractConnection* game_connection; // For collaboration / games 
	unsigned short port; // TODO necessary?
	int connectionid; // TODO necessary?
	
	jammo_state connection_state; // State of the connecting device
	guint32 jammoid; // TODO ERASE THIS!! 
	
	/*** PROFILE ***/
	gems_peer_profile* profile; // Profile of peer
	gint16 profile_requests;
	GTimer* profile_request_timer;
	
	/*** GROUP REQUEST TIMER ***/
	GTimer* group_request_timer;
	
	/*** LAST ACTION TIMER ***/
	GTimer* last_action; // Use for last actions & time in rejected list
	
	/* TODO Add last action made (what was sent) and timeout for possible re-send
	   - use also for removing items from rejected list after some time to keep
	   list size fairly small. Service & device as parameters. */
	
	/* network buffering */
	gchar nwbuffer[JAMMO_NETWORK_BUFFER_MAX_SIZE]; // buffer for partial reads
	guint add_position; // adding position, should be 0 when there is no data!
} gems_connection;

/**
 * gems_teacher_connection:
 * @connection: Connection structure for teacher as #gems_connection
 *
 * Contains connection to teacher.
 */
typedef struct {
	gems_connection* connection;
	// TODO certificates + other stuff here
} gems_teacher_connection;

/**
 * gems_service_jammo:
 * @connections: a #GList of pointers to #gems_connection structs
 * @enabled: a #gboolean defining if the service is enabled or not
 * @port: Port number for this service as #gint
 *
 * JamMo service data
 */
typedef struct {
	GList* connections; // gems_connection x N
	gboolean enabled;
	gint port;
} gems_service_jammo;

/**
 * gems_service_profile:
 * @enabled: a #gboolean defining if the service is enabled or not
 * @port: Port number for this service as #gint
 *
 * Profile service data
 */
typedef struct {
	gboolean enabled;
	gint port;
} gems_service_profile;

/**
 * gems_service_collaboration
 * @enabled: a #gboolean defining if the service is enabled or not
 * @active: a #gboolean defining if there exists a connection to teacher (?)
 * @port: Port number for this service as #gint
 * @collaboration_game: Pointer to game #GObject (?)
 *
 * Collaboration service data
 */
typedef struct {
	gboolean enabled;
	gboolean active;
	gint port;
	// collaboration_game can be any multiplayer game
	GObject * collaboration_game;
} gems_service_collaboration;

/**
 * gems_service_group
 * @enabled: a #gboolean defining if the service is enabled or not
 * @group_info: Own group information as #gems_group_info
 * @other_groups: a #GList of other groups as #gems_group_info
 * @port: Port number for this service as #gint
 *
 * Group management service data
 */
typedef struct {
	gboolean enabled;
	gems_group_info* group_info;
	GList* other_groups; // contains gems_group_info
	gint port;
} gems_service_group;

/**
 * gems_service_control
 *
 * @enabled: a #gboolean defining if the service is enabled or not
 * @active: is there a connection to teacher
 * @port: Port number for this service as #gint
 *
 * Control service data
 */
typedef struct {
	gboolean enabled;
	gboolean active; //is there a connection to teacher
	gint port;
} gems_service_control;

/**
 * gems_service_mentor
 *
 * @enabled: a #gboolean defining if the service is enabled or not
 * @port: Port number for this service as #gint
 *
 * Mentor service data
 */
// 
typedef struct {
	gboolean enabled;
	gint port;
} gems_service_mentor;

/**
 * gems_communicationd_data:
 * @connections: Active connections in a #GList
 * @connecting: A #GList of connections this device has initialized
 * @rejected: All rejected connections.
 *
 * Communication lists, each list is a #GList containing pointers to 
 * #gems_connection structs. Contains list for established connections,
 * connections which has been initialized by this device and a list of
 * rejected connections.
 */
typedef struct {
	GList* connections; // contains gems_connection, connected items
	GList* connecting; // contains gems_connection, connecting items
	GList* rejected; // contains gems_connection, rejected/version mismatch
} gems_communication_data;

/**
 * SongBankComm:
 * @peerHood: should not be here... should be accessed with #gems_get_peerhood()
 * @sbPort: ?
 * @connections: no, no, no
 *
 * What, who, why, where?
 */
typedef struct{
//	gems_service_songbank* mySBobj;	
	MPeerHood* peerHood;	
	unsigned short sbPort; //songbank service port on this device
	GList* connections;    // connections to SongBankComm
//	MAbstractConnection *connection;
//	pthread_t connThread;
//	int threadId;
}SongBankComm;

/**
 * gems_service_songbank:
 * @baseDirectory: where to search for files
 * @sbComm: The connection to song bank?
 *
 * Songbank service data
 */ 
typedef struct{
	gboolean enabled;
	gchar* baseDirectory;
	gchar* searchfile;
	gchar* receivefile;
	guint16 receivesize;
	guint16 receive_id;
	guint16 totalsize;
	//For file to be sent
	gchar* sendfilename;
	gems_connection* targetconnection;
	guint16 location;
	guint16 part_id;
	gboolean active;
	SongBankComm* sbComm;
	gint port;
}gems_service_songbank;

/**
 * gems_connection_error:
 * @checksum: Checksum of the device
 * @connection_errors: Amount of connection errors as #guint
 * @version_mismatch: Tells if the connection had errors because of the version
 * mismatch. Value as a boolean value ( #gboolean )
 * @already_connected: Tells if the connection was rejected because there
 * already exists a connection between peers, value as #gboolean.
 * @already_connected_delay: The delay for new connection attempt if the
 * connection was rejected because of an existing connection, delay is in 
 * seconds as #gdouble 
 * @connection_timer: Timer for delaying connection attempts.
 *
 * Connection error items, every errorous connection to a device
 * increases the errorcount, more errors -> increase connection interval
 * to the device. Use gems_connection_error* functions defined in 
 * gems_structures.h for manipulation.
 */
typedef struct {
	guint checksum;
	guint connection_errors;
	gboolean version_mismatch;
	gboolean already_connected;
	gdouble already_connected_delay;
	// TODO add "invalid messages" counter
	GTimer* connection_timer;
} gems_connection_error;

/**
 * gems_security_context:
 * @encryption: Encryption contexts as #EVP_CIPHER_CTX
 * @decryption: Decryption contexts as #EVP_CIPHER_CTX
 * @digest: Message digest for calculating hash as #EVP_MD_CTX
 *
 * Security contexts for GEMS, contains contexts for encryption, decryption and
 * for hashing content.
 */
typedef struct {
	EVP_CIPHER_CTX* encryption;
	EVP_CIPHER_CTX* decryption;
	EVP_MD_CTX* digest;
} gems_security_context;

/**
 * gems_components:
 * @timeout_functions: an array of timeout function source ids, maximum size is
 * #TO_FUNCTIONS
 * @pm: Pointer to Profile Manager struct which refers to a ProfileManager C++ 
 * object
 * @ph: Pointer to PeerHood library connection struct which refers to a
 * MPeerHood C++ object
 * @ph_on: a #gboolean which tells if there exists an connection to peerhood
 * @ph_cb: PeerHood callback, pointer to C_Callback struct which refers to a
 * C_Callback C++ object
 * @initialized_connections: A #GList of accepted initialized connections
 * established by peers, contains pointers to #gems_connection structures
 * @communication: Communication struct
 * @security_context: Security context struct
 * @service_jammo: Jammo service data struct
 * @service_profile: Profile service data struct
 * @service_group: Group management service data struct
 * @service_collaboration: Collacboration service data struct
 * @service_control: Control service data struct
 * @service_mentor: Mentor service data struct
 * @connection_errorlist: A #GList of #gems_connection_error structures, 
 * contains all connections having some kind of error
 * @teacher_profile_request_timer: Timer for profile requests to be sent to
 * teacher as #GTimer
 * @profile_request_timeout_sid: Source id of the profile request timeout
 * function
 * @teacher_profile_requests: Amount of profile requests sent to teacher as
 * #guint
 * @waiting_for_teacher: A #gboolean telling if the user has to wait for
 * teacher in order to do something
 * @waiting_profile_from_teacher: A #gboolean telling if the user is waiting
 * for a profile from teacher
 * @username_forced: A #gboolean telling if the username was forced by teacher
 * @username_to_login: Username to be used as loginname (forced by teacher)
 * @teacher_connection: Teacher connection structure
 * @service_songbank: Songbank service data struct
 *
 * The core structure for GEMS, contains lots of pointers to various other
 * structures, as well as pointer to PeerHood. Accessing functions are defined
 * in gems.h.
 */
typedef struct {
	guint timeout_functions[TO_FUNCTIONS];
	// Profile Manager
	ProfileManager* pm;
	
	/* PeerHood */
	MPeerHood* ph;
	gboolean ph_on;
	C_Callback* ph_cb;
	
	/* Initialized connections - gems_connection items*/
	GList* initialized_connections;
	
	/* Communication */
	gems_communication_data* communication;
	
	gems_security_context* security_context;
	
	/* Services*/
	gems_service_jammo* service_jammo;
	//gems_service_aa* service_aa;
	gems_service_profile* service_profile;

	gems_service_group* service_group;

	gems_service_collaboration* service_collaboration;

	gems_service_control* service_control;

	gems_service_mentor* service_mentor;
	
	GList* connection_errorlist; // Contains gems_connection_error items

	/*** TEACHER PROFILE REQUEST TIMER ***/
	GTimer* teacher_profile_request_timer;
	guint profile_request_timeout_sid;
	guint teacher_profile_requests;
	gboolean waiting_for_teacher;
	gboolean waiting_profile_from_teacher;
	gboolean username_forced;
	gchar* username_to_login;

	gems_teacher_connection* teacher_connection;
	// Set values to .jammo-profiles/<id>.csv

	gems_service_songbank* service_songbank;
	
} gems_components;

/**
 * gems_message:
 * @message: Message content
 * @length: Length of the message content
 *
 * Generic GEMS message.
 */
typedef struct {
	gchar* message;
	guint32 length;
} gems_message;



void gems_clear_gems_connection(gems_connection* element);

void gems_connection_clear_buffer(gems_connection* element);

void gems_connection_set_action(gems_connection* element);
double gems_connection_get_last_action(gems_connection* element);
void gems_connection_clear_last_action(gems_connection* element);

void gems_connection_add_16(gems_connection* element, guint16 value);
void gems_connection_add_32(gems_connection* element, guint32 value);
void gems_connection_add_64(gems_connection* element, guint64 value);
void gems_connection_add_float(gems_connection* element, gfloat value);
void gems_connection_add_char(gems_connection* element, gchar* value, guint length, gboolean pad);
void gems_connection_add_padding(gems_connection* element);
void gems_connection_add_data(gems_connection* element, gchar* data, guint length);
guint16 gems_connection_get_16(gems_connection* element, guint from);
guint32 gems_connection_get_32(gems_connection* element, guint from);
guint64 gems_connection_get_64(gems_connection* element, guint from);
gfloat gems_connection_get_float(gems_connection* element, guint from);
gfloat gems_connection_get_float_for_localization(gems_connection* element, guint from);
gchar* gems_connection_get_char(gems_connection* element, guint from);
gchar* gems_connection_get_data(gems_connection* element, guint from, guint length);

void gems_connection_set_content(gems_connection* element, gchar* newcontent, guint length);

guint gems_connection_partial_data_amount(gems_connection* element);

void gems_connection_localize_data_in_buffer(gems_connection* element);

gems_connection* gems_new_gems_connection(MAbstractConnection* newconn,
	unsigned short newport,
	int newconnectionid,
	jammo_state newstate,
	gint32 newjammoid);

gems_peer_profile* gems_new_peer_profile(gchar* username, guint32 id, guint16 age, guint32 avatarid);
void gems_peer_profile_set_values(gems_peer_profile* profile, gchar* username, guint32 id, guint16 age, guint32 avatarid);

void gems_clear_peer_profile(gems_peer_profile* profile);

GList* gems_cleanup_connections_from_list(GList* list);

GList* gems_connection_error_add(GList* list, guint device_checksum, GemsConnErrType errortype);
gems_connection_error* gems_connection_error_get(GList* list, guint device_checksum);
guint gems_connection_error_get_connection_error_count(GList* list, guint device_cheksum);
gboolean gems_connection_error_attempt_connection(GList* list, guint device_checksum);
GList* gems_connection_error_remove(GList* list, guint device_checksum);
void gems_clear_connection_error(gems_connection_error* element);

GList* gems_process_rejected(GList* list);

guint32 htonf(gfloat value);
gfloat ntohf(guint32 value);

#endif // __GEMS_STRUCTURES
