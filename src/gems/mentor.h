/*
 * mentor.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Tommi Kallonen <tommi.kallonen@lut.fi>
 */
 
#ifndef __MENTOR_H_
#define __MENTOR_H_

void mentor_send_log_message_to_teacher(char *message,int type);

#endif
