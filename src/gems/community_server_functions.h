/* community_server_functions.h is part of JamMo.
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Tommi Kallonen <tommi.kallonen@lut.fi>
 */
#ifndef _COMMUNITY_SERVER_FUNCTIONS_H_
#define _COMMUNITY_SERVER_FUNCTIONS_H_
#include <glib.h>


gboolean getJammoData(char *songname, char *userid, char *filename, char *description, char *date, char *json);
gboolean getJammoSongData(char *jammoSongID, char **songname, char **userid, char **filename, char **path, char **description, char **date, char **json);
gboolean sendJammoFile(gchar *filename);

#endif  /* _COMMUNITY_SERVER_FUNCTIONS_H_ */
