/*
 * gems_teacher_server_utils.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#ifndef __GEMS_TEACHER_SERVER_UTILS_H_
#define __GEMS_TEACHER_SERVER_UTILS_H_

/**
 * GemsTUWrite:
 * @WRITE_OK: profile was written to file
 * @WRITE_FAIL_CONTENT_NOT_SET: invalid parameters
 * @WRITE_FAIL_CANNOT_REMOVE_FILE: previous profile cannot be removed
 * @WRITE_FAIL_CANNOT_SET_FILE_CONTENT: cannot write the encrypted data to file
 *
 * Return values for teacher server write operations. 
 */
typedef enum {
	WRITE_OK = 0,
	WRITE_FAIL_CONTENT_NOT_SET,
	WRITE_FAIL_CANNOT_REMOVE_FILE,
	WRITE_FAIL_CANNOT_SET_FILE_CONTENT,
} GemsTUWrite;

/**
 * GemsTURead:
 * @READ_OK: encrypted database is successfully read
 * @READ_FAIL_CANNOT_READ: the database is unreadable
 * @READ_FAIL_FILE_NOT_EXIST: there is no profile file, proceed with empty 
 * database
 *
 * Return values for teacher server read operations.
 */
typedef enum {
	READ_OK = 0,
	READ_FAIL_CANNOT_READ,
	READ_FAIL_FILE_NOT_EXIST,
} GemsTURead;


#define TSRV_DATABASE "teacherprofiles.db"

#include <glib.h>
#include "gems_definitions.h"
#include "gems_utils.h"

/**
 * jammo_profile_container:
 * @version: Profile version
 * @type: Profile type
 * @userid: Id of the user
 * @username: Username of the user
 * @password: Plain password for profile en-/decryption
 * @salt: Password and security initialization salt
 * @firstname: First name of the user
 * @lastname: Last name of the user
 * @authlevel: Authentication level of the user
 * @age: Age of the user
 * @points: Points gained by the user
 * @avatarid: Avatar id of the user
 * @encrypted: Encrypted profile data
 * @encrypted_length: Length of the encrypted profile data
 *
 * Container for the full profile. Used mainly by the teacher server.
 */
typedef struct {
	guint8 version;
	guint8 type;	
	guint32 userid;
	gchar* username;
	gchar* password;
	guchar* salt;
	gchar* firstname;
	gchar* lastname;
	gchar* authlevel;
	guint16 age;
	guint32 points;
	guint32 avatarid;
	gchar* encrypted; // Contains the whole encrypted profile (salt+pwhash+phash+profile)
	guint encrypted_length; // Length of encrypted -field
} jammo_profile_container;

/**
 * teacher_server_profile_data:
 * @profiles: List of pointers to #jammo_profile_container structs
 * @salt: Password and security initialization salt
 * @pwhash: Teacher password hash
 * @datahash: Hash of the whole database
 * @data: Encrypted data, all profiles in the @profiles list
 * @datalength: Length of the encrypted data
 *
 * Teacher database struct. Contains all profiles (#jammo_profile_container)
 * and the whole database in encrypted form including the password and profile
 * hashes.
 */
typedef struct {
	GList* profiles; // contains jammo_profile_container*
	guchar* salt;
	guchar* pwhash;
	guchar* datahash;
	guchar* data; // encrypted data - all profiles
	guint datalength;
} teacher_server_profile_data;

jammo_profile_container* gems_teacher_server_new_profile_container();

teacher_server_profile_data* gems_teacher_server_new_profile_data_container();

void gems_teacher_server_clear_profile_container(jammo_profile_container* container);

void gems_teacher_server_clear_profile_data_container(teacher_server_profile_data* data);

jammo_profile_container* gems_teacher_server_create_profile(guint8 _type, guint32 _userid, const gchar* _username, const gchar* _password,
	const gchar* _authlevel, const gchar* _firstname, const gchar* _lastname, guint16 _age, guint32 _avatarid);
	
jammo_profile_container* gems_teacher_server_setup_profile(guint8 _type, guint32 _userid, const gchar* _username, const gchar* _password,
	guchar* _salt, const gchar* _authlevel, const gchar* _firstname, const gchar* _lastname, guint16 _age, guint32 _points, guint32 _avatarid);

jammo_profile_container* gems_teacher_server_get_profile_for_user(teacher_server_profile_data* data, gchar* username);

jammo_profile_container* gems_teacher_server_get_profile_for_user_id(teacher_server_profile_data* data, guint32 userid);

gboolean gems_teacher_server_encrypt_profile(jammo_profile_container* container);

gboolean gems_teacher_server_change_password(jammo_profile_container* container, gchar* password);

GemsTUWrite gems_teacher_server_write_profile(jammo_profile_container* container);

gboolean gems_teacher_server_save_encrypted_profile(gchar* username, gchar* profiledata, guint datalength);

gchar* gems_teacher_server_serialize_all_profiles(GList* list, guint* length);

GList* gems_teacher_server_deserialize_profiles(GList* list, gchar* serialized_data, guint length);

void gems_teacher_server_clear_profile_list(GList* list);

void gems_teacher_server_print_profile_list(GList* list);

void gems_teacher_server_print_profile(jammo_profile_container* profile);

gboolean gems_teacher_server_encrypt_profiles(teacher_server_profile_data* data, const gchar* passwd);

gboolean gems_teacher_server_decrypt_profiles(teacher_server_profile_data* data, const gchar* passwd);

GemsTUWrite gems_teacher_server_write_profile_database(teacher_server_profile_data* data);

GemsTURead gems_teacher_server_read_profile_database(teacher_server_profile_data** data);

#endif
