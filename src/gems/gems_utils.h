/*
 * gems_utils.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2011 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#ifndef __GEMS_UTILS_H_
#define __GEMS_UTILS_H_

#include <glib.h>

// Define illegal chars here
#define ILLEGAL_CHARS ",;.:*-_!#£$%&{}()=[]+?/\\\'\"\n\t^~"
#define ILLEGAL_CHAR_COUNT G_N_ELEMENTS(ILLEGAL_CHARS)

/* Check if all characters are integers/digits
 * 
 * Param:
 *  c - character input
 * Returns:
 *  TRUE if all are integers
 *  FALSE if one non-digit is found
 */
gboolean gems_all_integers(gchar* c);

/* Ereases first occurence of newline character from input
 * 
 * Param:
 * c - character input
 */
void gems_erase_newline(gchar* c);

/* Checks for illegal character from given input
 *
 * Param:
 *  data - character input
 * Returns:
 *  TRUE if illegal characters found
 *  FALSE if no illegal characters found
 */
gboolean gems_check_illegal_characters(const gchar* data);

/* Returns a list of profiles saved on disk, use gems_free_list_saved_profiles()
 * to free if necessary - the list is free'd when cleaning up GEMS.
 *
 * Param:
 *  count - will contain the count of profiles found after returning
 * Returns:
 *  a list of profile names as double pointer
 *  NULL when errors accessing directory
 */
gchar** gems_list_saved_profiles(guint* count);

/* Free the list of saved profiles if it exists */
void gems_free_list_saved_profiles();

gboolean gems_check_profile_version(gchar* filename);

/* Guarantee that memory is wiped out
 *
 * Param:
 *   data - buffer to empty
 *   value - value used for filling
 *   length - length of data
 */
void *gems_guaranteed_memset(void *data, gint value, guint length);

#endif
