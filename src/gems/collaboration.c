#include "collaboration.h"
#include "communication.h"
#include "groupmanager.h"
#include "messages.h"
#include "gems_structures.h"
#include "gems_message_functions.h"
#include "../cem/cem.h"
#include <stdlib.h>
#include <string.h>

int groupwork=0;//0=stand-alone, other=number of users in game
int classroom=0;//0=not connected to teacher, 1=is connected to teacher

// TODO move song info to some other place
static gchar* song_info = NULL;
gchar* collaboration_get_song_info() {
	return song_info;
}

void collaboration_set_song_info(gchar * new_song_info) {
	if (song_info!=NULL) {
		g_free(song_info);
		song_info=NULL;
	}
	song_info = g_strdup(new_song_info);
}

void collaboration_clear_song_info() {
	if (song_info!=NULL) {
		g_free(song_info);
		song_info=NULL;
	}
}

void collaboration_add_new_sample_to_track(guint loop_id, guint slot)
{
	gems_message * msg = gems_create_message_collaboration_action_loop(ADD_LOOP, loop_id, slot);
		
	gems_group_send_to_group(msg);
	gems_clear_message(msg);
}
void collaboration_remove_sample_from_slot(guint slot)
{
	gems_message * msg = gems_create_message_collaboration_action_loop(REMOVE_LOOP, 0, slot);
		
	gems_group_send_to_group(msg);
	gems_clear_message(msg);
}
void collaboration_loop_sync(GList * list)
{
	gems_message * msg = gems_create_message_collaboration_action_loop_sync(list);
		
	gems_group_send_to_group(msg);
	gems_clear_message(msg);	
}
void collaboration_midi_events_to_track(GemsEventListOperation operation, gint16 instrument_id, GList * list)
{
	int type=0;
	if (operation==GEMS_EVENTLIST_REPLACE)
	{
		type = REPLACE_MIDI_SERIES;
	}
	gems_message * msg = gems_create_message_collaboration_action_midi_series(type, instrument_id, list);

	gems_group_send_to_group(msg);
	gems_clear_message(msg);	
}
void collaboration_slider_events_to_track(GemsEventListOperation operation,gint16 instrument_id, GList * list)
{
	int type=0;
	if (operation==GEMS_EVENTLIST_REPLACE)
	{
		type = REPLACE_SLIDER_SERIES;
	}
	gems_message * msg = gems_create_message_collaboration_action_slider_series(type, instrument_id, list);

	gems_group_send_to_group(msg);
	gems_clear_message(msg);	
}


int decode_add_new_sample_to_track(char* message,int length)//length==size of received data
{
	gint32 track_number;
	gchar* srcLocation;
	guint64 startTime;
	char location;
	gint16 messagelength; //Real size of message
	
	location=sizeof(gint16);//MessageID
	memcpy(&messagelength,message+location,sizeof(gint16));
	location=location+sizeof(gint16);
	if(length<g_ntohs(messagelength))	
		return -1;

	memcpy(&track_number,message+location,sizeof(gint32));
	location=location+sizeof(gint32);

	srcLocation=(gchar *)malloc(g_utf8_strlen(message+location,-1)+1);
	memcpy(srcLocation,message+location,g_utf8_strlen(message+location,-1)+1);
	location=location+g_utf8_strlen(message+location,-1)+1;
	memcpy(&startTime,message+location,sizeof(guint64));
	location=location+sizeof(guint64);

	//call upper level function with these parameters
	printf("\nTrack number: %d, id: %s, starttime: %" G_GINT64_FORMAT ", message length: %d\n",track_number, srcLocation, startTime, location);
	//chum_add_new_sample_to_track_remote(g_ntohl(track_number), srcLocation, GUINT64_FROM_BE(startTime));
	
	return location;
}


int decode_remove_sample_from_slot(char* message,int length)
{
	gint32 track_number;
	gint32 slot;
	char location;
	gint16 messagelength; //Real size of message



	location=sizeof(gint16);//MessageID
	memcpy(&messagelength,message+location,sizeof(gint16));
	location=location+sizeof(int);
	if(length<g_ntohs(messagelength))	
		return -1;

	memcpy(&track_number,message+location,sizeof(gint32));
	location=location+sizeof(gint32);

	memcpy(&slot,message+location,sizeof(gint32));
	location=location+sizeof(gint32);

	//call upper level function with these parameters
	printf("\nTrack number: %d, slot: %d, message length: %d",track_number, slot, location);
	//chum_remove_sample_from_slot_remote(g_ntohl(track_number),g_ntohl(slot));
	return location;
}



int decode_theme_info(char* message,int length, int id)
{
	char location;
	char *themename;
	gint16 messagelength;
	gint32 variation, var;

	location=sizeof(gint16);//MessageID
	memcpy(&messagelength,message+location,sizeof(gint16));
	location=location+sizeof(gint16);
	if(length<g_ntohs(messagelength))	
		return -1;

	themename=(char *)malloc(strlen(message+location)+1);
	memcpy(themename,message+location,strlen(themename+location)+1);
	location=location+strlen(themename+location)+1;

	memcpy(&var,message+location,sizeof(gint32));
	location=location+sizeof(gint32);
	variation=g_ntohl(var);

	//call chum? function with themename and variation
return 1;
}


int decode_start_game_request(char* message,int length, int id)
{
	//Nothing to decode
	//Send information about request to chum?
	return 1;
}

int decode_start_game_response(char* message,int length, int id)
{
	char location;
	gint16 messagelength;
	gint16 stat_temp, status;

	location=sizeof(gint16);//MessageID
	memcpy(&messagelength,message+location,sizeof(gint16));
	location=location+sizeof(gint16);
	if(length<g_ntohs(messagelength))	
		return -1;
	
	memcpy(&stat_temp,message+location,sizeof(gint16));
	location=location+sizeof(gint16);
	status=g_ntohs(stat_temp);


	//Send information about request to chum?
	return 1;
}

void collaboration_start_pair_game(gint16 theme, gint16 variation, gems_components* data)//TODO do we need variation?
{	

	//Start new group
	if(gems_group_create_pair_game(theme)) printf("group created\n");

	//Create game object
	//data->service_collaboration->collaboration_pair_composition = jammo_collaboration_pair_composition_new();
	//TODO set real wav files 
	//jammo_collaboration_pair_composition_set_backing_track_location(collaboration_pair_composition,"/opt/jammo/songs/easy/boat/comping.ogg");
	//jammo_collaboration_pair_composition_set_mix_location(collaboration_pair_composition,"pair_composition_out.wav");

	//Playing may start?
	data->service_collaboration->active=TRUE;
}
void collaboration_pair_game_joined(gems_components* data)
{
	//Create game object
	//data->service_collaboration->collaboration_pair_composition = jammo_collaboration_pair_composition_new();
	
	//Still waiting for song file
}

// TODO verify that this doesn't require the GLOBAL song_info - input parameter name changed because of shadowing
void collaboration_got_song_info(gems_components* data, const gchar song_info_inp[])
{
	gchar* logmsg = NULL;

	logmsg = g_strdup_printf("collaboration_got_song_info: Got song file, ready to start game");
	cem_add_to_log(logmsg,J_LOG_DEBUG);
	g_free(logmsg);

	gchar * songinfo = g_strdup(song_info_inp);
	collaboration_set_song_info(songinfo);
	g_free(songinfo);

	//TODO handle the file contents and start correct UI
	if (data->service_group->group_info->type==GROUP_TYPE_PAIR) {
		chum_callback_game(data->service_collaboration->collaboration_game,
			GEMS_GAME_JOINED_PAIR_COMPOSITION, song_info_inp);
	}

	data->service_collaboration->active=TRUE;
}

// Changes in member list
void collaboration_member_list_updated(gems_components* data, gems_connection* element, gboolean owner)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	gchar* song_info_data = collaboration_get_song_info();
	if (song_info_data==NULL)
	{
		song_info_data=GEMS_EMPTY_SONG_INFO;
	}

	if(owner)//If we created this group->send song file to new member
	{
		msg = gems_create_message_collaboration_action_song_info(SONG_INFO, song_info_data);
		if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
		{
			logmsg = g_strdup_printf("collaboration_member_list_updated: cannot send group message to device %d user %d",
				ph_c_connection_get_device_checksum(element->connection), element->jammoid);
			cem_add_to_log(logmsg,J_LOG_ERROR);
			g_free(logmsg);
		}
		else
		{	
			logmsg = g_strdup_printf("collaboration_member_list_updated: New user joined group");
			cem_add_to_log(logmsg,J_LOG_DEBUG);
			g_free(logmsg);
			// inform game about joined peer
			chum_callback_game(data->service_collaboration->collaboration_game,
				GEMS_GAME_JOINED_PAIR_COMPOSITION, NULL);
		}
		gems_clear_message(msg);
	}
	else
	{
		logmsg = g_strdup_printf("collaboration_member_list_updated: New user joined group");
		cem_add_to_log(logmsg,J_LOG_DEBUG);
		g_free(logmsg);
		//TODO Inform the user about new members?
	}
}

