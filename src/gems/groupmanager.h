/*
 * groupmanager.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */

#ifndef _GROUPMANAGER_H_
#define _GROUPMANAGER_H_

#include "gems_structures.h"

gems_group_info* gems_new_group_info(guint32 id, guint32 owner, gint16 type, gint16 size, gint16 spaces, gint16 theme, gboolean starttimer);

/* Part of "API */

/* Convenience functions to create different group types */

/* Pair game */
gboolean gems_group_create_pair_game(gint16 theme);

/* Workshop non real time 
   NOT IMPLEMENTED */
gboolean gems_group_create_workshop_non_rt(gint16 theme);

/* Workshop real time - limited to 2 players only: jamming? 
   NOT IMPLEMENTED */
gboolean gems_group_create_workshop_rt(gint16 theme);

/* Workshop through network NOT IMPLEMENTED */
gboolean gems_group_create_workshop_net(gint16 theme);

/* Chain composition game NOT IMPLEMENTED */
gboolean gems_group_create_chain_game(gint16 theme);

/*
 * Initialize group
 *
 * Parameters: type - group type
 *             size - size of the group (2-4)
 *             theme - id of the used theme
 *
 * Returns: TRUE when group can be initialized, FALSE when group is already active (or joining / leaving)
 */ 
gboolean gems_group_init_group(gint16 type, gint16 size, gint16 theme);

/*
 * Reset group info
 *
 * Clear all group information
 */
void gems_reset_group_info();

/* 
 * Lock group for games - advert is stopped when locked.
 */
void gems_group_lock_group(gboolean locked);

/*
 *
 * Send message to group, currently sent as JAMMO_PACKET_PRIVATE,
 * to be changed to JAMMO_PACKET_GROUP when encryption keys for
 * groups are implemented.
 *
 * Parameter: msg - gems_message to be sent
 *
 * Returns: 1 - always atm 
 */
gint gems_group_send_to_group(gems_message* msg);

/* Join to given group
 *
 * Parameter: gid - group id
 *
 * Returns: GROUP_LOCKED, ALREADY_IN_GROUP, NO_MATCHING_GROUP, NO_OWNER, CANNOT_CONNECT_TO_OWNER, 
 *          REQUEST_SENT, REQUEST_NOT_SENT
 */
GemsGroupAction gems_group_join_to_group(guint32 gid);

/* gems_group_leave_from_group - Leave from current group
 *
 * Returns: NOT_IN_GROUP, CANNOT_CONNECT_TO_OWNER, REQUEST_SENT, REQUEST_NOT_SENT
 */
GemsGroupAction gems_group_leave_from_group();

/*
 * Check if the group is locked
 *
 * Returns: TRUE - locked, FALSE - not locked
 */
gboolean gems_group_is_locked();

/* 
 * Check if the group is active
 *
 * Returns: TRUE - active, FALSE - not active
 */
gboolean gems_group_active();

/* 
 * Get a copy of the list of other groups offered by surrounding devices and requested by us.
 * List contains pointers to gems_group_info elements (structs), free the list after usage.
 * Do not modify/delete the data inside list elements.
 *
 * Returns: first element of the list, each list item is type gems_group_info
 */
GList* gems_group_list_other_groups();

/* Get group id
 * 
 * Parameter: group - where to get information, if NULL info from own group is returned
 *
 * Returns: group id
 */
guint32 gems_group_get_gid(gems_group_info* group);

/* Get group owner
 * 
 * Parameter: group - where to get information, if NULL info from own group is returned
 *
 * Returns: group owner
 */
guint32 gems_group_get_owner(gems_group_info* group);

/* Get group type
 * 
 * Parameter: group - where to get information, if NULL info from own group is returned
 *
 * Returns: group type
 */
gint16 gems_group_get_type(gems_group_info* group);

/* Get group theme id
 * 
 * Parameter: group - where to get information, if NULL info from own group is returned
 *
 * Returns: group theme id
 */
gint16 gems_group_get_theme(gems_group_info* group);

/* Get group size
 * 
 * Parameter: group - where to get information, if NULL info from own group is returned
 *
 * Returns: group size
 */
gint16 gems_group_get_size(gems_group_info* group);

/* Get spaces left in particular group
 * 
 * Parameter: group - where to get information, if NULL info from own group is returned
 *
 * Returns: spaces left
 */
gint16 gems_group_get_spaces_left(gems_group_info* group);

/* Get members in our group
 *
 * Returns: array of user id:s, if id == FREE_SLOT no user in that slot. The size of the
 * array doesn't exceed GROUP_MAX_SIZE - 1
 */
guint32* gems_group_get_group_members();

/* / END OF API */

void gems_clear_group_info(gems_group_info* info);
gint gems_group_get_spaces();
gboolean gems_group_is_in_group(guint32 id);
GemsGroupAction gems_group_add_to_group(guint32 id);
GemsGroupAction gems_group_remove_from_group(guint32 id);

GList* gems_group_update_group_list(GList* list, guint32 gid, guint32 sender, gint16 type, gint16 size, gint16 space, gint16 theme);
guint32 gems_group_get_owner_for_group(GList* list, guint32 gid);

void gems_group_disconnected_member_detected(guint32 id);

#endif  /* _GROUPMANAGER_H_ */
