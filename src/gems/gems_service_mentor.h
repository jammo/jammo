/*
 * gems_service_mentor.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Tommi Kallonen <tommi.kallonen@lut.fi>
 */

#ifndef __JAMMO_GEMS_SERVICE_MENTOR
#define __JAMMO_GEMS_SERVICE_MENTOR
 
#include "gems_structures.h"
#include "gems_definitions.h"

gboolean gems_service_mentor_process_request(gems_connection* element);

gems_service_mentor* gems_service_mentor_init();

void gems_service_mentor_cleanup();

void gems_service_mentor_handle_log_message(gems_components* data, gems_connection* element);

#endif
