/*
 * gems_service_jammo.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#ifndef __JAMMO_GEMS_SERVICE_JAMMO
#define __JAMMO_GEMS_SERVICE_JAMMO

#include <peerhood/PeerHood.h>
#include <glib.h>

#include "communication.h"

#include "gems_structures.h"

#include "gems_definitions.h"

// Add new connection item 
void gems_service_jammo_process_new(gems_connection* connection);

// Check the connections for messages, state changes etc.
gint gems_service_jammo_process_connections();

GList* gems_service_jammo_process_list(GList* list, gboolean service);

gboolean gems_service_jammo_process_data(gems_connection *element, gboolean service);

gboolean gems_service_jammo_process_error(gems_connection *element, gboolean service);

gems_service_jammo* gems_service_jammo_init();

void gems_service_jammo_cleanup();

void gems_service_jammo_cleanup_lists();

#endif // __JAMMO_GEMS_SERVICE_JAMMO
