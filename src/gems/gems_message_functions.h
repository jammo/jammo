/*
 * gems_message_functions.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#ifndef __GEMS_MESSAGE_FUNCTIONS_
#define __GEMS_MESSAGE_FUNCTIONS_

#include "gems_structures.h"

gems_message* gems_create_error_message(guint16 errortype, guint16 serviceid);

void gems_clear_message(gems_message* message);

const gchar* get_errormessage(guint16 errortype);

gems_message* gems_create_message_service_jammo(guint16 type);

gems_message* gems_create_message_service_profile(guint16 type);

gems_message* gems_create_message_profile_request(guint16 type);

gems_message* gems_create_message_teacher_service_profile_request(gchar* username);

gems_message* gems_create_message_teacher_service_profile_reply(gchar* data, guint length, gchar* username);

gems_message* gems_create_message_password_changed(guchar* newpasshash, guchar* encnewpass, guint encpasslen, guint8 pwlen);

gems_message* gems_create_message_group_management_notify(guint16 type, guint32 groupid, guint32 member);

//For user created groups
gems_message* gems_create_message_group_management_group_info(guint16 type);

//For teacher created groups
gems_message* gems_create_message_group_management_group_info_teacher(guint16 type, gems_group_info* newgroup, guint16 track_id);

gems_message* gems_create_message_collaboration_action_confirmed();

gems_message* gems_create_message_collaboration_action_vi_single(guint16 type, const gchar png_name[], const gchar wav_name[], guint16 slot);

gems_message* gems_create_message_collaboration_action_song_info(guint16 type, const gchar song_info[]);

gems_message* gems_create_message_collaboration_action_loop(guint16 type, guint loop_id, guint slot);
gems_message* gems_create_message_collaboration_action_loop_sync(GList * list);

gems_message* gems_create_message_collaboration_action_midi_series(guint16 type, guint16 instrument_id, GList * list);
gems_message* gems_create_message_collaboration_action_slider_series(guint16 type, guint16 instrument_id, GList * list);

gems_message* gems_create_message_control_mode(guint16 type, guint16 cntrl_type, guint16 lock_mode);
gems_message* gems_create_message_control_ok(guint16 type);
gems_message* gems_create_message_control_set_login_name(gchar* username);
gems_message* gems_create_message_control_force_login(gchar* password, guint8 passlen);
gems_message* gems_create_message_control_get_login();

gems_message* gems_create_message_mentor_log_message(guint16 type, char *message);

gems_message* gems_create_message_songbank_search_file_req(guint16 type, char *filename);//Type = filetype (gems_definitions.h)
gems_message* gems_create_message_songbank_search_file_reply(guint16 type, guint16 status);
gems_message* gems_create_message_songbank_file_req(guint16 type, char *filename);
gems_message* gems_create_message_songbank_file_header(guint16 type, guint16 size, char *filename);
gems_message* gems_create_message_songbank_file_part(guint16 type, guint16 size, guint16 id,char *filecontents);

gems_message* gems_create_general_message(gems_message* msg, guint16 serviceid, guint32 length, guint16 type);
#endif // __GEMS_MESSAGE_FUNCTIONS
