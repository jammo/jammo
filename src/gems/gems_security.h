/*
 * gems_security.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#ifndef __GEMS_SECURITY_
#define __GEMS_SECURITY_

#include <openssl/evp.h>
#include <openssl/aes.h>
#include <openssl/sha.h>
#include <arpa/inet.h>
#include <string.h>
#include "gems_structures.h"


gems_message* gems_security_create_envelope(guint16 type,gems_connection* element, gems_message* original);

GemsEnvelopeCheck gems_security_extract_envelope(gems_connection* element);

gboolean gems_security_init_security(const guchar* password, const guchar* salt);

void gems_security_clear_security();

gboolean gems_security_change_password(const guchar* password, const guchar* salt);

guchar* gems_security_create_password(const gchar* password_data, const guchar* salt);

guchar* gems_security_create_password_salt();

// Initialize AES encryption and decryption contexts with given key
gboolean gems_security_init_aes(EVP_CIPHER_CTX* enc, EVP_CIPHER_CTX* dec, const guchar* salt, const guchar* keydata, guint keylength);

// Initialize SHA256 hash context
gboolean gems_security_init_sha(EVP_MD_CTX* md);

// Calculate hash from given data
guchar* gems_security_calculate_hash(const guchar* data, guint* data_length);

// Check that two given hashes match
gboolean gems_security_verify_hash(const guchar* hash1, const guchar* hash2,guint32 hashlength);

// Encrypt given data with given encryption context
guchar* gems_security_encrypt_data(const guchar* profile_data, guint* data_length);

// Decrypt data with given decryption context
guchar* gems_security_decrypt_data(const guchar* profile_data, guint* data_length);


#endif
