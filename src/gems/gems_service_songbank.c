#include "gems_service_songbank.h"
#include "gems_definitions.h"
#include "gems_message_functions.h"
#include "../cem/cem.h"
#include "songbank.h"
#include "../configure.h"

gboolean gems_service_songbank_process_request(gems_connection* element)
{
	gems_components* data = gems_get_data();
	
	switch(gems_connection_get_16(element,6)) // command
	{
		case SEARCH_FILE_REQ:
			gems_service_songbank_handle_search_file_req(data,element);
			break;
		case SEARCH_FILE_REPLY:
			gems_service_songbank_handle_search_file_reply(data,element);
			break;
		case FILE_REQ:
			gems_service_songbank_handle_file_req(data,element);
			break;
		case FILE_HEADER:
			gems_service_songbank_handle_file_header(data,element);
			break;
		case FILE_PART:
			gems_service_songbank_handle_file_part(data,element);
			break;
		default:
			break;
	}
	return TRUE;
}

void gems_service_songbank_handle_search_file_req(gems_components* data, gems_connection* element)
{
	FILE* searchfile=NULL;
	gems_message* msg = NULL;
	gchar* logmsg = NULL;

	gchar* filename =gems_connection_get_char(element,10);
	searchfile = execFindCommand(filename);
	if(searchfile != NULL)
	{
		msg = gems_create_message_songbank_search_file_reply(FILE_GENERAL, FILE_FOUND);
		if(msg != NULL)
		{					
			// Send
			if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,element,msg) == FALSE)
			{
				logmsg = g_strdup_printf("gems_service_songbank_handle_search_file_req: cannot write SEARCH_FILE_REPLY to %s/%u",
					ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
			}
			// Free message
			gems_clear_message(msg);
		}
		fclose(searchfile);
		free(searchfile);
	}
	else
	{
		msg = gems_create_message_songbank_search_file_reply(FILE_GENERAL, FILE_NOT_FOUND);
		if(msg != NULL)
		{					
			// Send
			if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,element,msg) == FALSE)
			{
				logmsg = g_strdup_printf("gems_service_songbank_handle_search_file_req: cannot write SEARCH_FILE_REPLY to %s/%u",
					ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
			}
			// Free message
			gems_clear_message(msg);
		}

	}
}

void gems_service_songbank_handle_search_file_reply(gems_components* data, gems_connection* element)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;

	guint16 status = gems_connection_get_16(element,8);

	if(status == FILE_FOUND)
	{
		msg = gems_create_message_songbank_file_req(FILE_GENERAL, gems_get_data()->service_songbank->searchfile);
		if(msg != NULL)
		{					
			// Send
			if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,element,msg) == FALSE)
			{
				logmsg = g_strdup_printf("gems_service_songbank_handle_search_file_reply: cannot write FILE_REQ to %s/%u",
					ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
			}
			// Free message
			gems_clear_message(msg);
		}
	}
	else
	{
		logmsg = g_strdup_printf("gems_service_songbank_handle_search_file_reply: file not found on %s/%u",ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
}

void gems_service_songbank_handle_file_req(gems_components* data, gems_connection* element)
{
	FILE* searchfile=NULL;
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	gchar* filepath;
	guint16 filesize;

	
	gchar* filename =gems_connection_get_char(element,10);

	filepath = SearchFileLocal(filename, NULL);
	

	if(filepath != NULL)
	{
		searchfile=fopen( filepath, "r");
		if(searchfile != NULL)
		{
			fseek(searchfile, 0L, SEEK_END);
			filesize = ftell(searchfile);
			printf("sending file %s, size %d\n",filename,filesize);	
			msg = gems_create_message_songbank_file_header(FILE_GENERAL, filesize, filename);
			if(msg != NULL)
			{					
				// Send
				if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,element,msg) == FALSE)
				{
					logmsg = g_strdup_printf("gems_service_songbank_handle_file_req: cannot write FILE_HEADER to %s/%u",
								ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
				}
				else//Header sent succesfully
				{
					printf("File header sent\n");
					data->service_songbank->sendfilename = filepath;
					data->service_songbank->targetconnection = element;
					data->service_songbank->location = 0;
					data->service_songbank->part_id = 0;
					data->service_songbank->active = TRUE;
				}
						// Free message
				gems_clear_message(msg);
			}
			fclose(searchfile);
		}		
	
	}
	else
	{
		logmsg = g_strdup_printf("gems_service_songbank_handle_search_file_req: requested file not found: %s",filename);
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);

		msg = gems_create_message_songbank_search_file_reply(FILE_GENERAL, FILE_NOT_FOUND);
		if(msg != NULL)
		{					
			// Send
			if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,element,msg) == FALSE)
			{
				logmsg = g_strdup_printf("gems_service_songbank_handle_search_file_req: cannot write SEARCH_FILE_REPLY to %s/%u",
					ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
			}
			// Free message
			gems_clear_message(msg);
		}

	}
}

void gems_service_songbank_handle_file_header(gems_components* data, gems_connection* element)
{
	gchar* saveFileName;
	gchar* logmsg;

	data->service_songbank->totalsize = gems_connection_get_16(element,10);
	saveFileName = g_strdup_printf("%s/%s",configure_get_temp_directory(), gems_connection_get_char(element,12));	
	data->service_songbank->receivefile = saveFileName;
	
	logmsg = g_strdup_printf("gems_service_songbank_handle_search_file_header: file header received. File name %s",gems_connection_get_char(element,12));
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);	
}

void gems_service_songbank_handle_file_part(gems_components* data, gems_connection* element)
{
	FILE* fp;
	gchar* logmsg;

	

	guint16 size = gems_connection_get_16(element,8);
	guint16 id = gems_connection_get_16(element,10);
	
	gchar* filepart =gems_connection_get_char(element,12);

	data->service_songbank->receivesize = data->service_songbank->receivesize + size;
	data->service_songbank->receive_id = id;

	printf("File part received. Saving %s\n. size %d/%d\n", data->service_songbank->receivefile, data->service_songbank->receivesize, data->service_songbank->totalsize);
	fp = fopen ( data->service_songbank->receivefile , "a" );
	fwrite (filepart , 1 , size , fp );
	fclose(fp);
	//g_free(filepart);

	if(data->service_songbank->receivesize == data->service_songbank->totalsize)
	{
		g_free(data->service_songbank->receivefile);
		data->service_songbank->receivesize=0;
		data->service_songbank->receive_id=0;
		data->service_songbank->totalsize=0;
	
		logmsg = g_strdup_printf("gems_service_songbank_handle_search_file_part: file receive finished");
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
}


gems_service_songbank* gems_service_songbank_init(gems_components* data, const gchar* _baseDirectory){

	
	//sb = data->service_songbank;

	//sb = malloc( sizeof(gems_service_songbank) );
	
	gems_service_songbank* sb = g_new(gems_service_songbank,sizeof(gems_service_songbank));
	
	sb->enabled = FALSE;
	
	sb->port = 0;

	sb->baseDirectory = g_strdup(_baseDirectory);

	printf("service songbank init. base directory %s\n",sb->baseDirectory);

	//if( data == NULL || data->ph == NULL ) 
	//	return FALSE; // no PeerHood = no network

	sb->searchfile=NULL;
	sb->sendfilename = NULL;
	sb->receivefile = NULL;
	sb->receivesize = 0;
	sb->receive_id = 0;
	sb->targetconnection = NULL;
	sb->location = 0;
	sb->part_id = 0;
	sb->active = FALSE;
	sb->totalsize = 0;
	

	/*if( SongBankComm_init( sb->sbComm, data->ph ) == TRUE ){
		printf("SBComm start SUCCESS");
		return TRUE;
	}
	else{
		printf("SBComm start FAILURE");
		free(sb->sbComm);
		sb->sbComm = NULL;
		return FALSE;
	}*/
	
	return sb;
}

void gems_service_songbank_clean(){
	gems_service_songbank* sb = gems_get_data()->service_songbank;

	g_free(sb->searchfile);
	g_free(sb->sendfilename);
	g_free(sb->receivefile);
	g_free(sb->baseDirectory);
	SongBankComm_clean();
	free(sb);
}

char* GetAuthorNameXML( char* filepath ){

	char* authorName = NULL;
	xmlDocPtr doc;
	xmlNodePtr cur;

	doc = xmlParseFile(filepath);

	if ( doc != NULL	&&	(cur = xmlDocGetRootElement(doc)) != NULL ) {
		do{
			if( xmlStrcmp(cur->name, (const xmlChar *) "Song") == 0 ){
				authorName = (char*)xmlGetProp(cur, (xmlChar *)"Author");
				if(DEBUG)printf("author: %s\n", authorName);
				break;
			}
			cur = cur->next;
	  	}while( cur );
	}
	return authorName;
}

char* GetAuthorNameJSON( char* filepath ){

	char* authorName = NULL;
	//TODO read the author's name from the json file.
	return authorName;
}

FILE* execFindCommand(const char* filename){
	gems_service_songbank* sb = gems_get_data()->service_songbank;
	char *command = NULL;	
	int _index;

	//"find " + sb->baseDirectory + " -name " + filename;
	command = malloc( strlen("find ") + strlen(sb->baseDirectory) + strlen(" -name ") + strlen(filename) );

	memcpy( command, "find ", strlen("find ") );
	_index = strlen("find ");
//	if( DEBUG )printf("\n%s", command );

	memcpy( command+_index, sb->baseDirectory , strlen(sb->baseDirectory) );
	_index += strlen(sb->baseDirectory);
//	if( DEBUG )printf("\n%s", command );

	memcpy( command+_index, " -name ", strlen(" -name ") );
	_index += strlen(" -name ");
//	if( DEBUG )printf("\n%s", command );

	memcpy( command+_index, filename, strlen(filename) );	
//	if( DEBUG )printf("\n%s", command );

	_index += strlen(filename);
	command[_index] = '\0';
	if( DEBUG )printf("final command:%s\n\n", command );

	FILE* cmdLine = popen (command, "r");
	free(command);
	return cmdLine;
}

char* SearchFileLocal( const char* filename, const char* author ){
	if( DEBUG )printf("\nSearching file localy...%s %s\n", filename, author );

	char *buff = NULL, *filepath = NULL;
	int ret = 0;
	size_t size = 0;

	FILE *cmdLine = execFindCommand(filename);

	while ( ret != -1 ){

		size = 10;
		buff = ( char* )malloc(size);
		ret = getline( &buff, &size, cmdLine );
		if( DEBUG )printf( "bufflength:%i,%i\nbuff:%s", (int) strlen(buff), (int) size, buff );

		if( ret != -1 && size > 2 ){

			buff[ strlen(buff) - 1 ] = '\0';
			char* tmpAuthor = GetAuthorNameXML( buff );
//TODO		char* tmpAuthor = GetAuthorNameJSON( buff );

			if( author == NULL || strcmp( tmpAuthor, author ) == 0 ){
				filepath = buff;
				if( tmpAuthor != NULL )free(tmpAuthor);
				break;
			}
			else{
				free(buff);
				if( tmpAuthor != NULL )free(tmpAuthor);
			}
		}
	}

	if( ! filepath )printf( "file not found on device\n" );
	else printf( "file found on device\n" );
	fclose( cmdLine );
	return filepath;
}


char* gems_service_songbank_getFilePath(const char* filename, const char* author){
	gems_service_songbank* sb = gems_get_data()->service_songbank;
	char* filepath = SearchFileLocal( filename, author );

	if( filepath == NULL && sb->sbComm != NULL ){

		if(DEBUG)printf("searching network...\n");
		filepath = SearchFileNetwork(filename, author);
	}
	return filepath;
}

void gems_service_songbank_newConnection(const unsigned short aPort, MAbstractConnection* aConnection, int aConnectionId, void* aData){
	NewSBConnection( aPort, aConnection, aConnectionId );
}

char* SearchFileNetwork(const char* filename, const char* author){
	gems_service_songbank* sb = gems_get_data()->service_songbank;
	if(sb->sbComm){

		char* saveToFile = malloc( strlen(sb->baseDirectory) + strlen(filename) + 2);
		memcpy( saveToFile, sb->baseDirectory, strlen(sb->baseDirectory) );
		saveToFile[strlen(sb->baseDirectory)] = '/';
		memcpy( saveToFile + strlen(sb->baseDirectory) + 1, filename, strlen(filename) );
		saveToFile[strlen(sb->baseDirectory) + strlen(filename)+1] = '\0';

		if( GetFileFromNetwork(filename, author, saveToFile) )
			return saveToFile;
		else 
			return NULL;
	}
	else 
		return NULL;
}
/*
void main(void){
	char *filename = "song.xml", *author = "blabla";

	SongBank_setup("/home/fawad/jammo/jammo/src/gems/");
	char* result = GetFilePath( filename, author );
	
	if( !result )
		result = "no result";
	else
		printf( "result: %s\n", result );
	free(result);

}*/
