/*
 * ProfileManager.cc
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2011 Lappeenranta University of Technology
 *
 * Authors: Janne Parkkila
 * 					Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <sstream>
#include <cstdlib>
#include "ProfileManager.h"
#include "gems_definitions.h"

extern "C" {
	#include "../cem/cem.h"
}

using namespace std;

ProfileManager* ProfileManager::iInstance = NULL;

ProfileManager* ProfileManager::GetInstance() 
{
	// No instance->create
	if (iInstance == NULL) iInstance = new ProfileManager;
	
	return iInstance;
}

// Constructor
ProfileManager::ProfileManager()
{
	iSavingRequired = FALSE;
	iStorageAgent = new StorageAgent();
	// process agent not yet used.
	//iProcessAgent = NULL;
}

// Destructor
ProfileManager::~ProfileManager()
{
	if(iStorageAgent != NULL)
	{
		delete iStorageAgent;
		iStorageAgent = NULL;
	}
	/*if(iProcessAgent != NULL)
	{
		delete iProcessAgent;
		iProcessAgent = NULL;
	}*/
}

// Login, calls StorageAgent to retrieve the user
// profile data
gboolean ProfileManager::Login(guint32 userID)
{
	gchar* logmsg = g_strdup_printf("ProfileManager::Login() : attempted to use deprecated function!");
	log_wrap(logmsg, J_LOG_ERROR);
	g_free(logmsg);
	return FALSE;
	// Store the id
	//iUser = userID;
	
	//string msg = "User Profile ";
	// C++ type int to string conversion
	//stringstream user;
	//user << userID;
	//msg.append(user.str());
	
	// If profile can be retrieved correctly,
	// create a server for displaying the profile
	// to other users
	/*if (iStorageAgent->Retrieve(iUser) == true)
	{
		msg.append(" succesfully retrieved");
		log_wrap((char*)msg.c_str(),J_LOG_INFO);
		return true;
	}
	else
	{
		msg.append(" cannot be found.");
		log_wrap((char*)msg.c_str(),J_LOG_INFO);
		return false;
	}*/
	
}

// Logout
gboolean ProfileManager::Logout()
{
	// Clear the user from the memory
	iUser = 0;
	// Clear data (sets authenticated to FALSE)
	iStorageAgent->clearDetails();
	// Clear encrypted data, should be saved before logout
	iStorageAgent->clearEncryptedData();

	// Not logged in, cannot log out but clear the data just to avoid leaking
	if(!isAuthenticated()) return FALSE;
	else return TRUE;
}

string ProfileManager::ViewDetails()
{
	// check if authenticated
	if(isAuthenticated()) return iStorageAgent->getDetails();
	else return "";
}

string ProfileManager::ViewDetail(string& detail)
{
	if(isAuthenticated()) return iStorageAgent->getDetail(detail);
	else return "";
}

// Edit user details. Takes the name of the detail and the new Value
// as a string
gboolean ProfileManager::EditDetails(string& detail, string& newValue)
{
	// Cannot set details if not authenticated
	if(!isAuthenticated()) return FALSE;
	
	// If the detail changing operation is failure, function return false
	if(!iStorageAgent->setDetails(detail, newValue)) return FALSE;
	// Changed, requires saving
	iSavingRequired = TRUE;
	return TRUE;
}

// This function is used to Force another user in to the
// profile manager. Requires admin level in the profile
gboolean ProfileManager::ForceUser(guint32 userID)
{
	string auth;
	// Get detail authLevel from the profile
	// if it is admin, the user is allowed to force
	// another user to the machine
	string param = string("authLevel");
	auth = iStorageAgent->getDetail(param);
	if (auth == "admin")
	{
		// Logout the admin and login wit the
		// wanted userID
		if (Logout() == true)
		{
			Login(userID);
			return TRUE;
		}
		else return FALSE;
	}
	else return FALSE;
}

guint32 ProfileManager::GetId()
{
	return iUser;
}

GemsProfileLoadAction ProfileManager::loadEncryptedProfile(const gchar* _username)
{
	if(iStorageAgent->isProfileLoaded())
	{
		// Requesting same user
		if(g_strcmp0(_username,iStorageAgent->getLoadedUser()) == 0) return PROFILE_LOADED;
		// User changed, clear previous data
		else 
		{
			iStorageAgent->clearEncryptedData();
			iStorageAgent->clearDetails();
		}
	}
	
	return iStorageAgent->readProfile(_username);
}

GemsProfileLoadAction ProfileManager::reloadEncryptedProfile()
{
	// Do not allow to reload if authenticated
	if(isAuthenticated()) return PROFILE_RELOAD_FAILED_NOT_AUTHENTICATED;
	
	if(iStorageAgent->isProfileLoaded())
	{
		if(iStorageAgent->getLoadedUser())
		{
			/* Duplicate since getLoadedUser returns a pointer to username, which will be free'd
			   after succesfull loading of the profile */
			gchar* uname = g_strdup(iStorageAgent->getLoadedUser());
			iStorageAgent->clearEncryptedData();
			GemsProfileLoadAction rval = loadEncryptedProfile(uname);
			g_free(uname);
			return rval;
		}
		else return PROFILE_RELOAD_FAILED;
	}
	else return PROFILE_RELOAD_FAILED;
}

gboolean ProfileManager::saveEncryptedProfile()
{
	string param = string(PROF_PARAM_USERNAME);
	gchar* username = (gchar*)ViewDetail(param).c_str();

	if(username == NULL) return FALSE;
	
		switch(iStorageAgent->writeProfile(username))
		{
			// Errors accessing file
			case -1:
				iSavingRequired = TRUE;
				return FALSE;
			// OK
			case 0:
				iSavingRequired = FALSE;
				return TRUE;
			
			// Salt missing
			case 2:
			// Password hash missing
			case 3:
			// Profile hash missing
			case 4:
			// encrypted profile missing
			case 5:
			// encrypted profile size missing
			case 6:
			// profile version is invalid
			case 7:
			default:
				iSavingRequired = TRUE;
				return FALSE;
		}
}

gboolean ProfileManager::changeLoadedUser(const gchar* _username)
{
	// clear previous user data
	iStorageAgent->clearEncryptedData();
	iStorageAgent->clearDetails();
	// Load new
	return loadEncryptedProfile(_username);
}

void ProfileManager::clearLoadedUser()
{
	// clear previous user data
	iStorageAgent->clearEncryptedData();
	iStorageAgent->clearDetails();
}

gboolean ProfileManager::deserializeProfile(gchar* _profile)
{
	if(!isAuthenticated()) return FALSE;
	gboolean rval = iStorageAgent->deserializeProfile(_profile);
	
	if(rval)
	{
		string param = string(PROF_PARAM_USERID);
		iUser = (guint32)strtoul(iStorageAgent->getDetail(param).c_str(),NULL,10);
	}
	return rval;
}

gchar* ProfileManager::serializeProfile()
{
	return iStorageAgent->serializeProfile();
}

gchar* ProfileManager::getLoadedUsername()
{
	if(iStorageAgent->isProfileLoaded()) return iStorageAgent->getLoadedUser();
	else return NULL;
}

guchar* ProfileManager::getSalt()
{
	return iStorageAgent->getSalt();
}

guchar* ProfileManager::getPasswordHash()
{
	return iStorageAgent->getPasswordHash();
}

guchar* ProfileManager::getProfileHash()
{
	return iStorageAgent->getProfileHash();
}

guchar* ProfileManager::getEncryptedProfile()
{
	return iStorageAgent->getEncryptedProfile();
}

guint ProfileManager::getEncryptedProfileSize()
{
	return iStorageAgent->getEncryptedProfileSize();
}

guint8 ProfileManager::getProfileVersion()
{
	return iStorageAgent->getProfileVersion();
}

gboolean ProfileManager::setSalt(guchar* _salt)
{
	if(iStorageAgent->isAuthenticated()) return iStorageAgent->setSalt(_salt);
	else return FALSE;
}

gboolean ProfileManager::setPasswordHash(guchar* _password_hash)
{
	if(iStorageAgent->isAuthenticated()) return iStorageAgent->setPasswordHash(_password_hash);
	else return FALSE;
}

gboolean ProfileManager::setEncryptedProfile(guchar* _profile_hash, guchar* _encrypted_profile, guint _e_profile_size)
{
	if(iStorageAgent->isAuthenticated())
	{
		if(iStorageAgent->setEncryptedProfile(_profile_hash, _encrypted_profile, _e_profile_size))
		{
			iSavingRequired = FALSE;
			return TRUE;
		}
		else
		{
			iSavingRequired = TRUE;
			return FALSE;
		}
	}
	else return FALSE;
}

gboolean ProfileManager::isSavingRequired()
{
	return iSavingRequired;
}

gboolean ProfileManager::isAuthenticated()
{
	return iStorageAgent->isAuthenticated();
}

// TODO add checks for this
gboolean ProfileManager::setAuthenticated()
{
	return iStorageAgent->setAuthenticated();
}
	

void log_wrap(char* msg, int info)
{
	cem_add_to_log(msg,info);
	//printf("message: %s (%d)\n",msg,info);
}

/*//////////////////////////////////////////////////////
||||||||||| DO NOT USE THESE DIRECTLY !!!! |||||||||||||
||||||| USE FUNCTIONS IN gems_profile_manager.c  |||||||
////////////////////////////////////////////////////////*/

/**
 * profilemanager_get_manager:
 *
 * Get profile manager object (a pointer to #ProfileManager struct pointing to
 * actual C++ object). Always returns the same profile manager because of
 * Singleton Pattern implementation in the C++ constructors. NEVER returns a
 * NULL pointer. C-Wrapper for #ProfileManager::GetInstance().
 *
 * Returns: A pointer to #ProfileManager struct.
 */ 
ProfileManager* profilemanager_get_manager()
{
	return ProfileManager::GetInstance();
}

/**
 * profilemanager_delete_manager:
 * @_aManager: ProfileManager object to delete
 *
 * Delete given manager object (@_aManager) calling the C++ delete.
 *
 * Returns: void
 */
void profilemanager_delete_manager(ProfileManager* _aManager)
{
	if(_aManager) delete _aManager;
}

/**
 * profilemanager_login:
 * @_userId: Id to use for login
 * 
 * Login to manager. Deprecated. C-Wrapper for #ProfileManager::Login().
 *
 * Returns: TRUE if successfull. 
 *
 * Deprecated: Do not use. Calling of #ProfileManager::Login() does nothing and
 * returns FALSE. 
 */
gboolean profilemanager_login(guint32 _userId)
{
	return profilemanager_get_manager()->Login(_userId);
}

/**
 * profilemanager_logout:
 *
 * Logout from profile manager. C-Wrapper for #ProfileManager::Logout(). Clears
 * all profile data.
 * 
 * Returns: TRUE when user was logged, FALSE if there was problems in logout or
 * the user was not logged in.
 */
gboolean profilemanager_logout()
{
	return profilemanager_get_manager()->Logout();
}

/**
 * profilemanager_view_info:
 * @_parameter: Profile information search parameter
 *
 * View one information. Do not use this directly. Use specialized functions
 * for requesting a particular information from profile. C-Wrapper for
 * #ProfileManager::ViewDetail(). Allowed only for authenticated user.
 *
 * Returns: The found information from the profile or an empty charstring if
 * not found.
 */
const gchar* profilemanager_view_info(gchar* _parameter)
{
	string param = string(_parameter);
	return (profilemanager_get_manager()->ViewDetail(param)).c_str();
}

/**
 * profilemanager_edit_info:
 * @_parameter: Profile parameter to change
 * @_value: New value for profile @_parameter
 *
 * Set new @_value for profile @_parameter. Do not use this directly. Use
 * specialized functionsfor requesting a particular information from profile.
 * C-Wrapper for #ProfileManager::EditDetails(). Allowed only for authenticated
 * user.
 *
 * Returns: TRUE when @_parameter was changed to @_value, FALSE otherwise.
 */
gboolean profilemanager_edit_info(gchar* _parameter, gchar* _value)
{
	string param = string(_parameter);
	string value = string(_value);
	return profilemanager_get_manager()->EditDetails(param,value);
}

/**
 * profilemanager_get_userid:
 *
 * Get own user id from own profile. Allowed only for authenticated user. 
 * C-Wrapper for #ProfileManager::GetId().
 *
 * Returns: Own user id as #guint32.
 */
guint32 profilemanager_get_userid()
{
	return profilemanager_get_manager()->GetId();
}

/**
 * profilemanager_get_username:
 *
 * Get own username. Allowed only for authenticated user. Calls 
 * #ProfileManager::ViewDetail() with parameter %PROF_PARAM_USERNAME.
 *
 * Returns: The username.
 */
const gchar* profilemanager_get_username()
{	
	string param = string(PROF_PARAM_USERNAME);
	return (profilemanager_get_manager()->ViewDetail(param)).c_str();
}

/**
 * profilemanager_get_firstname:
 *
 * Get own first name. Allowed only for authenticated user. Calls 
 * #ProfileManager::ViewDetail() with parameter %PROF_PARAM_FIRSTNAME.
 *
 * Returns: The first name.
 */
const gchar* profilemanager_get_firstname()
{
	string param = string(PROF_PARAM_FIRSTNAME);
	return (profilemanager_get_manager()->ViewDetail(param)).c_str();
}

/**
 * profilemanager_get_lastname:
 *
 * Get own last name. Allowed only for authenticated user. Calls 
 * #ProfileManager::ViewDetail() with parameter %PROF_PARAM_LASTNAME.
 *
 * Returns: The last name.
 */
const gchar* profilemanager_get_lastname()
{
	string param = string(PROF_PARAM_LASTNAME);
	return (profilemanager_get_manager()->ViewDetail(param)).c_str();
}

/**
 * profilemanager_get_age:
 *
 * Get own age. Allowed only for authenticated user. Calls 
 * #ProfileManager::ViewDetail() with parameter %PROF_PARAM_AGE. Charstring is
 * converted to int using atoi().
 *
 * Returns: The age of the user as #guint16.
 */
guint16 profilemanager_get_age()
{
	string param = string(PROF_PARAM_AGE);
	return atoi((profilemanager_get_manager()->ViewDetail(param)).c_str());
}

/**
 * profilemanager_get_points:
 *
 * Get own points. Allowed only for authenticated user. Calls 
 * #ProfileManager::ViewDetail() with parameter %PROF_PARAM_POINTS. Charstring
 * is converted to int using strtoul().
 *
 * Returns: Points gained by the user as #guint32.
 */
guint32 profilemanager_get_points()
{
	string param = string(PROF_PARAM_POINTS);
	return (guint32)strtoul((profilemanager_get_manager()->ViewDetail(param)).c_str(),NULL,10);
}

/**
 * profilemanager_get_avatarid:
 *
 * Get own avatar id. Allowed only for authenticated user. Calls 
 * #ProfileManager::ViewDetail() with parameter %PROF_PARAM_AVATARID. 
 * Charstring is converted to int using strtoul().
 *
 * Returns: The avatar id as #guint32.
 */
guint32 profilemanager_get_avatarid()
{
	string param = string(PROF_PARAM_AVATARID);
	return (guint32)strtoul((profilemanager_get_manager()->ViewDetail(param)).c_str(),NULL,10);
}

/**
 * profilemanager_get_profile_type:
 *
 * Get own profile type. Allowed only for authenticated user. Calls 
 * #ProfileManager::ViewDetail() with parameter %PROF_PARAM_TYPE. Charstring is
 * converted to int using atoi().
 *
 * Returns: The type of the profile as #guint8, %PROF_TYPE_LOCAL if profile was
 * locallycreated, %PROF_TYPE_REMOTE if profile was created by teacher or 0 if 
 * profile is invalid.
 */
guint8 profilemanager_get_profile_type()
{
	string param = string(PROF_PARAM_TYPE);
	guint8 type = (guint8)atoi(profilemanager_get_manager()->ViewDetail(param).c_str());
	
	// Currently only 2 types of profiles
	if(type < PROF_TYPE_LOCAL || type > PROF_TYPE_REMOTE) return 0;
	else return type;
}

/**
 * profilemanager_get_profile_version:
 *
 * Get the version of the currently loaded profile. C-wrapper for function
 * #ProfileManager::getProfileVersion().
 *
 * Returns: The version of the profile as #guint8.
 */
guint8 profilemanager_get_profile_version()
{
	return profilemanager_get_manager()->getProfileVersion();
}

/**
 * profilemanager_set_username:
 * @_value: New value to set for username
 *
 * Set new own username. Allowed only for authenticated user. Calls 
 * #ProfileManager::EditDetails() with parameter%PROF_PARAM_USERNAME and 
 * @_value.
 *
 * Returns: TRUE when new @_value was set, FALSE otherwise.
 */
gboolean profilemanager_set_username(gchar* _value)
{
	string param = string(PROF_PARAM_USERNAME);
	string value = string(_value);
	return profilemanager_get_manager()->EditDetails(param,value);
}

/**
 * profilemanager_set_firstname:
 * @_value: New value to set for first name
 *
 * Set new own first name. Allowed only for authenticated user. Calls 
 * #ProfileManager::EditDetails() with parameter %PROF_PARAM_FIRSTNAME and 
 * @_value.
 *
 * Returns: TRUE when new @_value was set, FALSE otherwise.
 */
gboolean profilemanager_set_firstname(gchar* _value)
{
	string param = string(PROF_PARAM_FIRSTNAME);
	string value = string(_value);
	return profilemanager_get_manager()->EditDetails(param,value);
}

/**
 * profilemanager_set_lastname:
 * @_value: New value to set for last name
 *
 * Set new own last name. Allowed only for authenticated user. Calls 
 * #ProfileManager::EditDetails() with parameter %PROF_PARAM_LASTNAME and 
 * @_value.
 *
 * Returns: TRUE when new @_value was set, FALSE otherwise.
 */
gboolean profilemanager_set_lastname(gchar* _value)
{
	string param = string(PROF_PARAM_LASTNAME);
	string value = string(_value);
	return profilemanager_get_manager()->EditDetails(param,value);
}

/**
 * profilemanager_set_age:
 * @_value: New value to set for age
 *
 * Set age into profile. Allowed only for authenticated user. Calls 
 * #ProfileManager::EditDetails() with parameter %PROF_PARAM_AGE and 
 * @_value. The given @_value is converted into C++ string.
 *
 * Returns: TRUE when new @_value was set, FALSE otherwise.
 */
gboolean profilemanager_set_age(guint16 _value)
{
	stringstream strvalue;
	string param = string(PROF_PARAM_AGE);
	
	if(_value > 120) return FALSE;
	
	strvalue << _value;
	string value = strvalue.str();
	
	return profilemanager_get_manager()->EditDetails(param,value);
}

/**
 * profilemanager_set_avatarid:
 * @_value: New value to set for 
 *
 * Set avatar id into profile. Allowed only for authenticated user. Calls 
 * #ProfileManager::EditDetails() with parameter %PROF_PARAM_AVATARID and 
 * @_value. The given @_value is converted into C++ string.
 *
 * Returns: TRUE when new @_value was set, FALSE otherwise.
 */
gboolean profilemanager_set_avatarid(guint32 _value)
{
	string param = string(PROF_PARAM_AVATARID);
	
	stringstream strvalue;
	strvalue << _value;
	
	string value = strvalue.str();
	
	return profilemanager_get_manager()->EditDetails(param,value);
}

/**
 * profilemanager_add_points:
 * @_value: Amount of points to add
 *
 * Increase the points in the profile with @_value. Allowed only for 
 * authenticated user. Calls #ProfileManager::EditDetails() with parameter 
 * %PROF_PARAM_POINTS and @_value as C++ string.
 *
 * Returns: TRUE when new @_value amount of points was added, FALSE otherwise.
 */
gboolean profilemanager_add_points(guint32 _value)
{
	string param = string(PROF_PARAM_POINTS);
	guint32 points = (guint32)strtoul((profilemanager_get_manager()->ViewDetail(param)).c_str(),NULL,10);
	
	points += _value;
	
	stringstream strvalue;
	strvalue << points;
	
	string value = strvalue.str();
	
	return profilemanager_get_manager()->EditDetails(param,value);
}

/**
 * profilemanager_remove_points:
 * @_value: Amount of points to remove
 *
 * Decrease the points in the profile with @_value. Allowed only for 
 * authenticated user. Calls #ProfileManager::EditDetails() with parameter 
 * %PROF_PARAM_POINTS and @_value as C++ string.
 *
 * Returns: TRUE when new @_value amount of points was removed, FALSE otherwise
 */
gboolean profilemanager_remove_points(guint32 _value)
{
	string param = string(PROF_PARAM_POINTS);
	guint32 points = (guint32)strtoul((profilemanager_get_manager()->ViewDetail(param)).c_str(),NULL,10);
	
	if((points - _value) < 0) points = 0;
	else points = points - _value;
	
	stringstream strvalue;
	strvalue << points;
	
	string value = strvalue.str();
	
	return profilemanager_get_manager()->EditDetails(param,value);	
}

/**
 * profilemanager_reset_points:
 *
 * Reset the points in the profile. Allowed only for authenticated user. Calls
 * #ProfileManager::EditDetails() with parameter %PROF_PARAM_POINTS. 
 *
 * Returns: void
 */
void profilemanager_reset_points()
{
	string param = string(PROF_PARAM_POINTS);
	string value = "0";
	profilemanager_get_manager()->EditDetails(param,value);
}

/**
 * profilemanager_storage_load_profile:
 * @_username: Username to use for profile file search
 *
 * Load profile with given @_username. C-wrapper for 
 * #ProfileManager::loadEncryptedProfile(). Only loads the profile from file
 * into #StorageAgent, does not attempt to decrypt.
 *
 * Returns: GemsProfileLoadAction types: %PROFILE_LOADED, %PROFILE_NOT_FOUND,
 * %PROFILE_READ_FAILURE or %PROFILE_VERSION_MISMATCH.
 */
GemsProfileLoadAction profilemanager_storage_load_profile(const gchar* _username)
{
	return profilemanager_get_manager()->loadEncryptedProfile(_username);
}

/**
 * profilemanager_storage_reload_profile:
 *
 * Reload profile for currently loaded user. Authenticated user cannot reload
 * own profile. C-wrapper for #ProfileManager::reloadEncryptedProfile(). 
 *
 * Returns: GemsProfileLoadAction types: %PROFILE_LOADED, %PROFILE_NOT_FOUND,
 * %PROFILE_READ_FAILURE, %PROFILE_VERSION_MISMATCH, %PROFILE_RELOAD_FAILED or
 * %PROFILE_RELOAD_FAILED_NOT_AUTHENTICATED.
 */
GemsProfileLoadAction profilemanager_storage_reload_profile()
{
	return profilemanager_get_manager()->reloadEncryptedProfile();
}

/**
 * profilemanager_storage_save_profile:
 *
 * Save the encrypted profile to disk. Profile file is named using the username
 * of the user who is currently logged in. All profile data must be set
 * (password hash, profile hash and encrypted profile) before saving. C-wrapper
 * for #ProfileManager::saveEncryptedProfile(). 
 *
 * Returns: TRUE when profile was saved, FALSE when profile cannot be saved
 * because of missing data / file cannot be written.
 */
gboolean profilemanager_storage_save_profile()
{
	return profilemanager_get_manager()->saveEncryptedProfile();
}

/**
 * profilemanager_storage_change_profile:
 * @_username: New user whose profile is to be loaded
 *
 * Clears the previously loaded profile from memory and loads the profile for
 * given @_username. C-wrapper for #ProfileManager::changeLoadedUser(). 
 *
 * Returns: TRUE when profile was changed, FALSE otherwise.
 */
gboolean profilemanager_storage_change_profile(gchar* _username)
{
	return profilemanager_get_manager()->changeLoadedUser(_username);
}

/**
 * profilemanager_storage_clear_profile:
 *
 * Clear loaded profile data. Clears also the authentication for the current
 * user. Called by logout function. C-wrapper for 
 * #ProfileManager::clearLoadedUser().
 *
 * Returns: void
 */
void profilemanager_storage_clear_profile()
{
	profilemanager_get_manager()->clearLoadedUser();
}

/**
 * profilemanager_storage_get_password_salt:
 *
 * Get password salt stored in loaded profile. C-wrapper for 
 * #ProfileManager::getSalt(). 
 *
 * Returns: Pointer to salt as #guchar or NULL if no profile is loaded.
 */
guchar* profilemanager_storage_get_password_salt()
{
	return profilemanager_get_manager()->getSalt();
}

/**
 * profilemanager_storage_get_password_hash:
 *
 * Get password hash stored in loaded profile. C-wrapper for 
 * #ProfileManager::getPasswordHash(). 
 *
 * Returns: Pointer to password hash as #guchar or NULL if no profile is loaded
 */
guchar* profilemanager_storage_get_password_hash()
{
	return profilemanager_get_manager()->getPasswordHash();
}

/**
 * profilemanager_storage_get_profile_hash:
 *
 * Get hash for the loaded profile data. C-wrapper for 
 * #ProfileManager::getProfileHash(). 
 *
 * Returns: Pointer to profile hash as #guchar or NULL if no profile is loaded.
 */
guchar* profilemanager_storage_get_profile_hash()
{
	return profilemanager_get_manager()->getProfileHash();
}

/**
 * profilemanager_storage_get_encrypted_profile:
 *
 * Get the encrypted profile data. C-wrapper for 
 * #ProfileManager::getEncryptedProfile(). 
 *
 * Returns: Pointer to the encrypted profile data or NULL if no profile is
 * loaded.
 */
guchar* profilemanager_storage_get_encrypted_profile()
{
	return profilemanager_get_manager()->getEncryptedProfile();
}

/**
 * profilemanager_storage_get_profile_size:
 *
 * Get the length of the encrypted profile data. C-wrapper for 
 * #ProfileManager::getEncryptedProfileSize(). 
 *
 * Returns: Length of the encrypted profile data as #guint or 0 iof no profile
 * is loaded.
 */
guint profilemanager_storage_get_encrypted_profile_size()
{
	return profilemanager_get_manager()->getEncryptedProfileSize();
}

/**
 * profilemanager_storage_set_password_salt:
 * @_salt: New salt to set
 *
 * Set new @_salt into profile. Allowed only for authenticated user. C-wrapper 
 * for #ProfileManager::setSalt().
 *
 * Returns: TRUE if the new @_salt was set, FALSE otherwise.
 */
gboolean profilemanager_storage_set_password_salt(guchar* _salt)
{
	return profilemanager_get_manager()->setSalt(_salt);
}

/**
 * profilemanager_storage_set_password_hash:
 * @_hash: New password hash
 *
 * Set new password @_hash, use after changing the password so the new password
 * can be verified when loading. Hash must be %SHA_HASH_LENGTH. Allowed only 
 * for authenticated user. C-wrapper for #ProfileManager::setPasswordHash(). 
 *
 * Returns: TRUE if new @_hash was set, FALSE otherwise.
 */
gboolean profilemanager_storage_set_password_hash(guchar* _hash)
{
	return profilemanager_get_manager()->setPasswordHash(_hash);
}

/**
 * profilemanager_storage_set_encrypted_profile:
 * @_profile_hash: New profile hash of the @_encrypted_profile data, must be
 * %SHA_HASH_LENGTH
 * @_encrypted_profile: New encrypted profile data encrypted with
 * gems_security_encrypt_data()
 * @_e_profile_size: Length of the @_encrypted_profile
 *
 * Set new encrypted profile, use after password is changed (profile re-
 * encrypted) or profile content is changed (re-encrypted). The provided
 * @_profile_hash must be %SHA_HASH_LENGTH. The @_encrypted_profile must be
 * encrypted with gems_security_encrypt_data(). Allowed only for authenticated
 * user. C-wrapper for #ProfileManager::setEncryptedProfile(). 
 *
 * Returns: TRUE when the new profile data was set, FALSE otherwise.
 */
gboolean profilemanager_storage_set_encrypted_profile(guchar* _profile_hash, guchar* _encrypted_profile, guint _e_profile_size)
{
	return profilemanager_get_manager()->setEncryptedProfile(_profile_hash, _encrypted_profile, _e_profile_size);
}

/**
 * profilemanager_storage_deserialize_profile:
 * @_profile: Decrypted serialized profile data to be deserialized
 *
 * Deserialize given decrypted serialized @_profile. Called after succesfull 
 * authentication to set the data in the @_profile to the current profile of
 * the loaded user. Data in @_profile is copied to profile data fiels. 
 * C-wrapper for #ProfileManager::deserializeProfile(). 
 *
 * Returns: TRUE when the given @_profile data could be deserialized and set
 * to profile, FALSE otherwise.
 */
gboolean profilemanager_storage_deserialize_profile(guchar* _profile)
{
	return profilemanager_get_manager()->deserializeProfile((gchar*)_profile);
}

/**
 * profilemanager_storage_serialize_profile:
 *
 * Serialize the loaded profile into one #gchar. The profile is serialized 
 * using commas as profile data field separators. The returned #gchar must be
 * free'd by the caller. Use this to get the proper form of profile to be sent
 * to gems_security_encrypt_data() function when profile is to be encrypted. 
 * C-wrapper for #ProfileManager::serializeProfile(). 
 *
 * Returns: A pointer to new #gchar containing the serialized form of the
 * profile or commas, if the profile was not set (decrypted and set into
 * profile manager data). 
 */
gchar* profilemanager_storage_serialize_profile()
{
	return profilemanager_get_manager()->serializeProfile();
}

/**
 * profilemanager_storage_set_authenticated:
 *
 * Set the user into authenticated state, called after the password is verified
 * and the profile is successfully decrypted and set. C-wrapper for 
 * #ProfileManager::setAuthenticated(). 
 *
 * Returns: TRUE if the user could be set into authenticated state, FALSE
 * otherwise.
 */
gboolean profilemanager_set_authenticated()
{
	return profilemanager_get_manager()->setAuthenticated();
}

/**
 * profilemanager_storage_is_authenticated:
 *
 * Check if the current loaded user is authenticaed. C-wrapper for 
 * #ProfileManager::isAuthenticated(). 
 *
 * Returns: TRUE if user is authenticated.
 */
gboolean profilemanager_is_authenticated()
{
	return profilemanager_get_manager()->isAuthenticated();
}

/**
 * profilemanager_storage_is_saving_required:
 *
 * Check if the saving is required for the profile. The saving to disk of
 * encrypted profile is required after something has been changed in the 
 * profile. C-wrapper for #ProfileManager::isSavingRequired(). 
 *
 * Returns: TRUE if the saving is required, FALSE otherwise.
 */
gboolean profilemanager_is_saving_required()
{
	return profilemanager_get_manager()->isSavingRequired();
}

/**
 * profilemanager_storage_get_loaded_username:
 *
 * Get the username whose profile is loaded. C-wrapper for 
 * #ProfileManager::getLoadedUsername(). 
 *
 * Returns: Name of the user whose profile is loaded or NULL if no profile is
 * loaded.
 */
const gchar* profilemanager_get_loaded_username()
{
	return profilemanager_get_manager()->getLoadedUsername();
}

