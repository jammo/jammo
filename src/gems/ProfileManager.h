/*
 * ProfileManager.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2011 Lappeenranta University of Technology
 *
 * Authors: Janne Parkkila
 * 					Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#ifndef __GEMS_PROFILEMANAGER_H_
#define __GEMS_PROFILEMANAGER_H_

#include "StorageAgent.h"
#ifdef __cplusplus
#include <iostream>
#include <string>
#include <glib.h>

using namespace std;

class ProfileManager {
public:
	
	// Constructor, creates a server
	// for sharing profile info over peerhood
	static ProfileManager* GetInstance();
	
	// Destructor
	~ProfileManager();
	
	// Login with certain userID
	gboolean Login(guint32 userId);
	// Logout
	gboolean Logout();
	// Show user details
	string ViewDetails();
	// Get single detail
	string ViewDetail(string& detail);
	// Edit own details
	gboolean EditDetails(string& detail, string& newValue);
	// Push another user to service
	gboolean ForceUser(guint32 userID);
	// Get id
	guint32 GetId();
	
	GemsProfileLoadAction loadEncryptedProfile(const gchar* _username);
	GemsProfileLoadAction reloadEncryptedProfile();
	gboolean saveEncryptedProfile();
	gboolean changeLoadedUser(const gchar* _username);
	void clearLoadedUser();
	gboolean deserializeProfile(gchar* _profile);
	gchar* serializeProfile();
	
	gchar* getLoadedUsername();
	guchar* getSalt();
	guchar* getPasswordHash();
	guchar* getProfileHash();
	guchar* getEncryptedProfile();
	guint getEncryptedProfileSize();
	guint8 getProfileVersion();
	
	gboolean setSalt(guchar* _salt);
	gboolean setPasswordHash(guchar* _password_hash);
	gboolean setEncryptedProfile(guchar* _profile_hash, guchar* _encrypted_profile, guint _e_profile_size);
	
	gboolean isSavingRequired();
	gboolean isAuthenticated();
	gboolean setAuthenticated();

private:
	
	// Constructor, creates a server
	// for sharing profile info over peerhood
	ProfileManager();

	static ProfileManager* iInstance;
	StorageAgent *iStorageAgent;
	//ProcessAgent *iProcessAgent;
	guint32 iUser;
	gboolean iSavingRequired;
};

#else
/**
 * ProfileManager:
 * 
 * A structure for accessing a C++ object ProfileManager.
 */
	typedef
		struct ProfileManager
			ProfileManager;
#endif // __cplusplus

#ifdef __cplusplus
extern "C" {
#endif

void log_wrap(char* msg, int info);

/*//////////////////////////////////////////////////////
||||||||||| DO NOT USE THESE DIRECTLY !!!! |||||||||||||
||||||| USE FUNCTIONS IN gems_profile_manager.c  |||||||
////////////////////////////////////////////////////////*/

// ---------------- C wrappers -------------------------//

/* Create new profile manager interface
 *
 * Wrapper for ProfileManager::ProfileManager() 
 *
 * Returns: a pointer to struct ProfileManager
 *
 * Use the struct only through functions described here!
 */
ProfileManager* profilemanager_get_manager();


/* Delete profile manager
 *
 * Wrapper for ProfileManager::~ProfileManager() 
 *
 * Params:
 *  ProfileManager* _aManager - object to be destroyed
 */ 
void profilemanager_delete_manager(ProfileManager* _aManager);


/* Login to ProfileManager
 *
 * Wrapper for bool ProfileManager::Login(int)
 *
 * Params:
 *  int _userId - id of the user
 *
 * Returns: int, 0 if false, 1 if true
 */
gboolean profilemanager_login(guint32 _userId);


/* Login to ProfileManager
 *
 * Wrapper for bool ProfileManager::Logout()
 *
 * Returns: int, 0 if false, 1 if true
 */
gboolean profilemanager_logout();


/* View a single detail
 *
 * Wrapper for string ProfileManager::ViewDetail(string)
 * 
 * Params:
 *  char* _parameter - parameter to retrieve
 *
 * Returns: char* containing the value, or empty if not found
 */
const gchar* profilemanager_view_info(gchar* _parameter);


/* Edit own detail
 * 
 * Wrapper for bool ProfileManager::EditDetails(string, string);
 *
 * Params:
 *  char* _detail - Detail to be changed
 *  char* _value - Value for the detail to be changed
 *
 * Returns: int, 0 - detail was not found, 1 - detail was found (val. changed)
 */
gboolean profilemanager_edit_info( gchar* _parameter,	gchar* _value);

guint32 profilemanager_get_userid();

/* Get own username */
const gchar* profilemanager_get_username();

/* Get firstname */
const gchar* profilemanager_get_firstname();

/* Get lastname */
const gchar* profilemanager_get_lastname();

/* Get age */
guint16 profilemanager_get_age();

/* Get points */
guint32 profilemanager_get_points();

/* Get avatar id */
guint32 profilemanager_get_avatarid();

/* Get profile type */
guint8 profilemanager_get_profile_type();

/* Get profile version */
guint8 profilemanager_get_profile_version();

/* Set own username */
gboolean profilemanager_set_username(gchar* _value);

/* Set firstname */
gboolean profilemanager_set_firstname(gchar* _value);

/* Set lastname */
gboolean profilemanager_set_lastname(gchar* _value);

/* Set age - limit is 120 */
gboolean profilemanager_set_age(guint16 _value);

/* Get avatar id */
gboolean profilemanager_set_avatarid(guint32 _value);

/* Add points */
gboolean profilemanager_add_points(guint32 _value);

/* Remove points */
gboolean profilemanager_remove_points( guint32 _value);

/* Reset points (set to zero) */
void profilemanager_reset_points();

/* For encrypted profile data */

GemsProfileLoadAction profilemanager_storage_load_profile(const gchar* _username);
GemsProfileLoadAction profilemanager_storage_reload_profile();
gboolean profilemanager_storage_save_profile();
gboolean profilemanager_storage_change_profile(gchar* _username);
void profilemanager_storage_clear_profile();
guchar* profilemanager_storage_get_password_salt();
guchar* profilemanager_storage_get_password_hash();
guchar* profilemanager_storage_get_profile_hash();
guchar* profilemanager_storage_get_encrypted_profile();
guint profilemanager_storage_get_encrypted_profile_size();
gboolean profilemanager_storage_set_password_salt(guchar* _salt);
gboolean profilemanager_storage_set_password_hash(guchar* _hash);
gboolean profilemanager_storage_set_encrypted_profile(guchar* _profile_hash, guchar* _encrypted_profile, guint _e_profile_size);

gboolean profilemanager_storage_deserialize_profile(guchar* _profile);
gchar* profilemanager_storage_serialize_profile();

gboolean profilemanager_set_authenticated();
gboolean profilemanager_is_authenticated();

gboolean profilemanager_is_saving_required();
const gchar* profilemanager_get_loaded_username();
#ifdef __cplusplus
}
#endif

#endif /*__GEMS_PROFILEMANAGER_H_ */

