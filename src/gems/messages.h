/** communication.h is part of JamMo.
License: GPLv2, read more from COPYING
*/
#ifndef _MESSAGES_H_
#define _MESSAGES_H_

#define ADD_NEW_SAMPLE_TO_TRACK			100
#define ADD_NEW_SAMPLE_TO_TRACK_NTH_SLOT	101
#define REMOVE_SAMPLE_FROM_SLOT			102
#define USER_INFO_REQUEST			103
#define USER_INFO_RESPONSE			104
#define HELLO					105
#define	START_GAME_REQUEST			106
#define	START_GAME_RESPONSE			107
#define THEME_INFO				108


#endif  /* _MESSAGES_H_ */
