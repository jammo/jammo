/*
 * gems_service_jammo.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#include <stdlib.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>

#include "gems_service_jammo.h"
#include "gems_message_functions.h"
#include "../cem/cem.h"

/**
 * gems_service_jammo_process_new:
 * @connection: new connection item to add into connection lists.
 * 
 * Add new @connection to list of JamMo service connections for the handshake
 * procedure and application version verification.
 *
 * Returns: void
 */ 
void gems_service_jammo_process_new(gems_connection* connection)
{
	if(!connection) return;
	
	gems_service_jammo* data = gems_get_data()->service_jammo;
	connection->connection_state = JAMMO_CONNECTING;
	data->connections = g_list_append(data->connections, connection);
	gchar* logmsg = g_strdup_printf("gems_service_jammo_process_new: Added new");
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
}

/**
 * gems_service_jammo_process_connections:
 *
 * Process messages from connections in the handshake and version verification
 * process. A timeout function executed periodically by the Glib main loop.
 * Will remove self from main loop when the service is disabled or there is no
 * connection to PeerHood. Calls gems_service_jammo_process_list() to process
 * the incoming data from the connections in the list.
 *
 * Returns: Integer value 0 (FALSE) or 1 (TRUE), 0 when errors or the function
 * is required to be removed from Glib main loop.
 */
gint gems_service_jammo_process_connections()
{	
	gems_components* data = gems_get_data();
	if(data == NULL) return FALSE;
	if(gems_get_peerhood() == NULL) return FALSE;
	if(data->service_jammo->enabled == FALSE) return FALSE;
	
	data->service_jammo->connections = gems_service_jammo_process_list(data->service_jammo->connections, TRUE);
	
	return TRUE;
}

/**
 * gems_service_jammo_process_list:
 * @list: List of #gems_connection elements to process
 * @service: The role in the handshake process, TRUE = service role
 * 
 * Read the incoming data from connections in the @list. Goes through ever
 * connection element (#gems_connection) in the list and reads the incoming
 * data in pieces into the network buffer of the connection. Removes the
 * connections which have sent invalid data. Disconnects connections that have
 * been idle for too long to avoid deadlocks. If the message is an error
 * message it will be forwarded to gems_service_jammo_process_error() and all
 * other proper messages are forwarded to gems_service_jammo_process_data(). If
 * the connection #jammo_state is %JAMMO_CONNECTED_NO_AUTH the handshake and
 * version verification process was successfully executed and the connection is
 * moved to connected list and is ready to be used by other services. The
 * connections which are in %JAMMO_VERSION_MISMATCH state are moved to rejected
 * list.
 *
 * Returns: Pointer to the list that was given as parameter after it was
 * processed.
 */ 
GList* gems_service_jammo_process_list(GList* list, gboolean service)
{
	gboolean cleanup_required = FALSE;
	gems_connection* element = NULL;
	GList* iterator = NULL;
	GList* tempiter = NULL;
	guint32 bytesread = 0;
	guint32 dataleft = 0;
	gchar* msgdata = NULL;
	gchar* logmsg = NULL;
	
	iterator = g_list_first(list);
	while(iterator)
	{
		element = (gems_connection*)iterator->data;
		
		if(!element)
		{
			iterator = g_list_next(iterator);
			continue;
		}
		
		// Was idle for too long
		if(gems_connection_get_last_action(element) > IDLE_CONNECTION_LIMIT)
		{
			cem_add_to_log("gems_service_jammo_process_connections: Disconnected a idle connection",J_LOG_NETWORK_DEBUG);
			ph_c_connection_disconnect(element->connection);
			cleanup_required = TRUE;
		}
		else if((ph_c_connection_is_connected(element->connection)) == TRUE && 
			(ph_c_connection_has_data(element->connection) == TRUE))
		{	
			// Something happened with this connection, set new time
			gems_connection_set_action(element);	
			
			// Partial data
			if(gems_connection_partial_data_amount(element) > 0)
			{
				// If 2 bytes = Service id or error read -> read length
				if(gems_connection_partial_data_amount(element) == sizeof(guint16))
				{
					guint32 packetlength = 0;
					
					switch ((bytesread = gems_communication_read_data(ph_c_connection_get_fd(element->connection), &packetlength, sizeof(guint32))))
					{
						/* CORRECT AMOUNT */
						case sizeof(guint32):
							logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): read length: %u", (service == TRUE ? 's' : 'c'), ntohl(packetlength));
							cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
							if(ntohl(packetlength) > (JAMMO_NETWORK_BUFFER_MAX_SIZE - sizeof(guint32) - sizeof(guint16)))
							{
								logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): packet is too big, disconnecting.", (service == TRUE ? 's' : 'c'));
								cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
								g_free(logmsg);
								ph_c_connection_disconnect(element->connection);
								cleanup_required = TRUE;
							}
							else gems_connection_add_32(element,ntohl(packetlength)); // Add length
							break;
						case 0:
						case -1:
						default:
							logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): length not read correctly (bytesread = %d). Disconnecting", 
								(service == TRUE ? 's': 'c') , bytesread);
							cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
							ph_c_connection_disconnect(element->connection);
							cleanup_required = TRUE;
							break;
					}
				}
				
				// If 6 bytes read = Service id and length read -> read command or service id (error)
				else if(gems_connection_partial_data_amount(element) == (sizeof(guint16) + sizeof(guint32)))
				{
					guint16 command = 0;
					
					switch((bytesread = gems_communication_read_data(ph_c_connection_get_fd(element->connection), &command, sizeof(guint16))))
					{
						case sizeof(guint16):
							logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): command: %d, user in state: %d",
								(service == TRUE ? 's': 'c'), ntohs(command), element->connection_state);
							cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
							gems_connection_add_16(element,ntohs(command));
							break;

						case 0:
						case -1:
						default:
							logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): command not read correctly (bytesread = %d). Disconnecting",
								(service == TRUE ? 's': 'c'), bytesread);
							cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
							ph_c_connection_disconnect(element->connection);
							cleanup_required = TRUE;
							break;
					}
				}
				
				// If 8 bytes read and there is data to read
				else if((gems_connection_partial_data_amount(element) == (sizeof(guint16) + sizeof(guint32) + sizeof(guint16))) &&
					(gems_connection_partial_data_amount(element) != gems_connection_get_32(element,sizeof(guint16))))
				{
					dataleft = gems_connection_get_32(element, sizeof(guint16)) - (sizeof(guint16) + sizeof(guint32) + sizeof(guint16));
					guint16 errortype = 0;
					
					// error message
					if(gems_connection_get_16(element,0) == ERROR_MSG)
					{
						// Read the error type
						if((bytesread	= gems_communication_read_data(ph_c_connection_get_fd(element->connection), &errortype, sizeof(guint16))) == sizeof(guint16))
						{
							logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): error type = %d, bytesread = %d, dataleft = %d",
								(service == TRUE ? 's' : 'c'), ntohs(errortype), bytesread, dataleft);
							cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
							gems_connection_add_16(element,ntohs(errortype));
							dataleft = dataleft - bytesread; // reduce the read amount
						}
						else
						{
							logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): error type couldn't be read, bytesread = %d, dataleft = %d",
								(service == TRUE ? 's' : 'c'), bytesread, dataleft);
							cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
							ph_c_connection_disconnect(element->connection);
							cleanup_required = TRUE;
						}
					}
					
					msgdata = g_new0(gchar,sizeof(gchar) * dataleft);
			
					// Read the rest - should be chars
					if((bytesread = gems_communication_read_data(ph_c_connection_get_fd(element->connection),msgdata,dataleft)) != dataleft)
					{
						logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): not fully read! bytesread = %d, dataleft = %d",
							(service == TRUE ? 's': 'c'), bytesread, dataleft);
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						gems_connection_add_char(element, msgdata, bytesread, FALSE); // Add partial, pad = FALSE
					}
					else if(bytesread == -1 || bytesread == 0)
					{
						logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): error reading rest of data, error: %d",
							(service == TRUE ? 's': 'c'),bytesread);
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						ph_c_connection_disconnect(element->connection);
						cleanup_required = TRUE;
					}
					else
					{
						// Add data
						logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): read data: %s, total %d bytes (dataleft = %d)",
							(service == TRUE ? 's': 'c'), msgdata, bytesread, dataleft);
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						gems_connection_add_char(element, msgdata, bytesread, TRUE);
					}
					g_free(msgdata);
					msgdata = NULL;
				}
				// Read remaining data
				else
				{
					// left = packet length - read amount
					dataleft = gems_connection_get_32(element, sizeof(guint16)) - gems_connection_partial_data_amount(element);
					msgdata = g_new(gchar,sizeof(gchar) * dataleft);
					
					if((bytesread = gems_communication_read_data(ph_c_connection_get_fd(element->connection),msgdata, dataleft)) < dataleft)
					{
						logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): reading rest, not fully read! bytesread = %d, dataleft = %d",
							(service == TRUE ? 's': 'c'), bytesread, dataleft);
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						gems_connection_add_char(element, msgdata, bytesread, FALSE); // Add partial, pad = FALSE
					}
					else
					{
						logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): read rest data: %s, total %d bytes (dataleft = %d)",
							(service == TRUE ? 's': 'c'),msgdata, bytesread, dataleft);
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						gems_connection_add_char(element, msgdata, bytesread, TRUE);
					}
					g_free(msgdata);
					msgdata = NULL;
				}
				
				// All read? Data read = length parameter in buffer[2]
				if(gems_connection_partial_data_amount(element) == gems_connection_get_32(element,sizeof(guint16)))
				{
					logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): full packet received (%d bytes).",
						(service == TRUE ? 's': 'c'),gems_connection_get_32(element,sizeof(guint16)));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
					
					if(gems_connection_get_16(element,0) == JAMMO_SERVICE_ID)
					{
						// Process the element data
						if(gems_service_jammo_process_data(element, service) == FALSE)
						{
							ph_c_connection_disconnect(element->connection);
							cleanup_required = TRUE;
						}
					}
					else if(gems_connection_get_16(element,0) == ERROR_MSG)
					{
						if(gems_service_jammo_process_error(element, service) == FALSE)
						{
							ph_c_connection_disconnect(element->connection);
							cleanup_required = TRUE;
						}
					}
					else
					{
						logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): unknown packet received, type: %d.",
							(service == TRUE ? 's': 'c'),gems_connection_get_16(element,0));
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
					}
				}
			}
			
			// No partial data - read service id
			else
			{
				guint16 serviceid = 0;
			
				switch((bytesread = gems_communication_read_data(ph_c_connection_get_fd(element->connection), &serviceid, sizeof(guint16))))
				{
					/* CORRECT AMOUNT */
					case sizeof(guint16):
						if((ntohs(serviceid) == JAMMO_SERVICE_ID) || (ntohs(serviceid) == ERROR_MSG))
						{
							gems_connection_add_16(element, ntohs(serviceid));
						}
						else // Invalid service id
						{
							logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): Invalid service id %d",
								(service == TRUE ? 's': 'c'), serviceid);
							cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
							
							gems_message* err = gems_create_error_message(ERROR_INVALID_SERVICE_ID_TYPE, JAMMO_SERVICE_ID);
							
							if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
							{
								logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): cannot write ERROR_INVALID_SERVICE_ID",
									(service == TRUE ? 's': 'c'));
								cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
								g_free(logmsg);
							}
							
							gems_clear_message(err);
							ph_c_connection_disconnect(element->connection);
							cleanup_required = TRUE;
						} 
						break;
					
					case 0:
					case -1:
					default:
						logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): service id read error(%d)",
							(service == TRUE ? 's': 'c'),bytesread);
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						ph_c_connection_disconnect(element->connection);
						cleanup_required = TRUE;
						break;
				}
			}
		}
		
		if(element->connection_state == JAMMO_CONNECTED_NO_AUTH)
		{
			gems_connection_clear_last_action(element);
			tempiter = iterator; // temporary containing wanted data
			iterator = g_list_next(iterator);
		
			list = g_list_remove_link(list,tempiter);
			gems_communication_process_new((gems_connection*)tempiter->data); // To connected list
			g_list_free_1(tempiter);
			logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): moved one to connected, list size now: %d",
				(service == TRUE ? 's': 'c'), g_list_length(list));
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
		}
		// Versions do not match
		else if(element->connection_state == JAMMO_VERSION_MISMATCH)
		{
			gems_connection_clear_last_action(element);
			tempiter = iterator; // temporary containing wanted data
			iterator = g_list_next(iterator);
		
			list = g_list_remove_link(list,tempiter);
			gems_communication_process_new_rejected((gems_connection*)tempiter->data); // To rejected list
			g_list_free_1(tempiter);
			logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): client with checksum %d moved to rejected list, list size now: %d",
				(service == TRUE ? 's': 'c'), ph_c_connection_get_device_checksum(element->connection), g_list_length(list));
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
		}
		else
		{
			// Next iterator
			iterator = g_list_next(iterator);
		}
	}
	
	if(cleanup_required == TRUE) list = gems_cleanup_connections_from_list(list);
	
	return list;
}

/**
 * gems_service_jammo_process_data:
 * @element: Connection element which has incoming data in its network buffer
 * @service: The role in the handshake process, TRUE = service role
 *
 * Process the message in the network buffer of the @element in the given role
 * (@service). This is the implementation of the state machine for verifying 
 * the version between devices. The state of the connection is changed based on
 * the incoming data.
 *
 * Returns: A #gboolean with value TRUE if there are no errors with the
 * connection @element, FALSE when the connection @element should be removed
 * from list.
 */ 
gboolean gems_service_jammo_process_data(gems_connection *element, gboolean service)
{
	gboolean wrongstate = FALSE;
	gboolean success = TRUE;
	gchar* logmsg = NULL;
	
	if(!element) return TRUE;

	switch(element->connection_state)
	{
		case JAMMO_CONNECTING:
			if(service == FALSE) break; // In client mode this state should be never active
			
			// Command is CONNECT_REQ
			if(gems_connection_get_16(element,(sizeof(guint16) + sizeof(guint32))) == CONNECT_REQ)
			{
				gems_message* msg = gems_create_message_service_jammo(VERSION_REQ);
				if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
				{
					logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): cannot write VERSION_REQ to %s/%u",
						(service == TRUE ? 's': 'c'), ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
					success = FALSE;
				}
				else
				{
					logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): wrote bytes: %d [%d|%u|%d]", (service == TRUE ? 's': 'c'),
						msg->length, ntohs(*(guint16*)&msg->message[0]),ntohl(*(guint32*)&msg->message[2]),ntohs(*(guint16*)&msg->message[6]));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
							
					element->connection_state = JAMMO_WAITING_VERSION; // Waiting for VERSION_REPLY
					logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): State changed to JAMMO_WAITING_VERSION for %s/%u",
						(service == TRUE ? 's': 'c'), ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
				}
				gems_clear_message(msg);
			}
			else wrongstate = TRUE;
			break;
		
		case JAMMO_WAITING_VERSION:
			// Command is VERSION_REPLY
			if(gems_connection_get_16(element,(sizeof(guint16) + sizeof(guint32))) == VERSION_REPLY)
			{
				// TODO check the versions!
				if(g_strcmp0(gems_connection_get_char(element, (sizeof(guint16) + sizeof(guint32) + sizeof(guint16))),VERSION) == 0)
				{
					gems_message* msg = gems_create_message_service_jammo(CONNECT_REPLY);
					if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
					{
						logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): cannot write CONNECT_REPLY to %s/%u",
							(service == TRUE ? 's': 'c'), ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						success = FALSE;
					}
					else
					{
						logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): wrote bytes: %d [%d|%u|%d]", (service == TRUE ? 's': 'c'),
							msg->length, g_ntohs(*(guint16*)&msg->message[0]),g_ntohl(*(guint32*)&msg->message[2]),g_ntohs(*(guint16*)&msg->message[6]));
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						
						if(service == TRUE) // Service
						{
							element->connection_state = JAMMO_WAITING_REQUEST; // Waiting for VERSION_REQ
							logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): State changed to JAMMO_WAITING_REQUEST for %s/%u",
								(service == TRUE ? 's': 'c'), ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
							cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
						}
						else // Client
						{
							element->connection_state = JAMMO_CONNECTED_NO_AUTH; // Connected
							logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): State changed to JAMMO_CONNECTED_NO_AUTH for %s/%u",
								(service == TRUE ? 's': 'c'), ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
							cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
						}
					}
					gems_clear_message(msg);
				}
				else
				{
					gems_message* err = gems_create_error_message(ERROR_VERSION_MISMATCH_TYPE, JAMMO_SERVICE_ID);
					if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
					{
						logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): cannot write ERROR_VERSION_MISMATCH to %s/%u",
							(service == TRUE ? 's': 'c'), ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
					}
					element->connection_state = JAMMO_VERSION_MISMATCH;
					logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): State changed to JAMMO_VERSION_MISMATCH for %s/%u",
						(service == TRUE ? 's': 'c'), ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
				
					gems_clear_message(err);
				}
			}
			else wrongstate = TRUE;
			break;
			
		case JAMMO_WAITING_REQUEST:
			// Command is VERSION_REQ
			if(gems_connection_get_16(element,(sizeof(guint16) + sizeof(guint32))) == VERSION_REQ)
			{
				gems_message* msg = gems_create_message_service_jammo(VERSION_REPLY);
				if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
				{
					logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): cannot write VERSION_REPLY to %s/%u", 
						(service == TRUE ? 's': 'c'), ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
					success = FALSE;
				}
				else
				{
					logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): wrote bytes: %d [%d|%u|%d|%s]", (service == TRUE ? 's': 'c'),
						msg->length, ntohs(*(guint16*)&msg->message[0]),ntohl(*(guint32*)&msg->message[2]),ntohs(*(guint16*)&msg->message[6]), &msg->message[8]);
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
					
					element->connection_state = JAMMO_WAITING_REPLY; // Waiting for CONNECT_REPLY
					logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): State changed to JAMMO_WAITING_REPLY for %s/%u",
						(service == TRUE ? 's': 'c'), ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
				}
				gems_clear_message(msg);
			}
			else wrongstate = TRUE;
			break;
			
		case JAMMO_WAITING_REPLY:
			// Command is CONNECT_REPLY
			if(gems_connection_get_16(element,(sizeof(guint16) + sizeof(guint32))) == CONNECT_REPLY)
			{
				if(service == TRUE)
				{
					element->connection_state = JAMMO_CONNECTED_NO_AUTH; // Connected
					logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): State changed to JAMMO_CONNECTED_NO_AUTH for %s/%u",
						(service == TRUE ? 's': 'c'), ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
				}
				else // Client
				{
					gems_message* msg = gems_create_message_service_jammo(VERSION_REQ);
					if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
					{
						logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): cannot write VERSION_REQ to %s/%u",
							(service == TRUE ? 's': 'c'), ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						success = FALSE;
					}
					else
					{
						logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): wrote bytes: %d [%d|%u|%d]", (service == TRUE ? 's': 'c'), 
							msg->length, ntohs(*(guint16*)&msg->message[0]),ntohl(*(guint32*)&msg->message[2]),ntohs(*(guint16*)&msg->message[6]));
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						
						element->connection_state = JAMMO_WAITING_VERSION; // Waiting for VERSION_REPLY
						logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): State changed to JAMMO_WAITING_VERSION for %s/%u",
							(service == TRUE ? 's' : 'c'), ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
					}
					gems_clear_message(msg);
				}
			}
			else wrongstate = TRUE;
			break;
		case JAMMO_VERSION_MISMATCH:
			// Handled in gems_service_jammo_process_list
			break;
		default:
			break;
	}
	
	if(wrongstate == TRUE)
	{
		logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): invalid command in state: %d from %s/%u",
			(service == TRUE ? 's': 'c'), element->connection_state, ph_c_connection_get_remote_address(element->connection), 
			ph_c_connection_get_device_checksum(element->connection));
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
		
		gems_message* err = gems_create_error_message(ERROR_INVALID_STATE_TYPE, JAMMO_SERVICE_ID);
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
		{
			logmsg = g_strdup_printf("gems_service_jammo_process_connections (%c): cannot write ERROR_INVALID_STATE to %s/%u",
				(service == TRUE ? 's': 'c'), ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
		}
		gems_clear_message(err);
	}
	
	// Clear buffer
	gems_connection_clear_buffer(element);
	
	return success;
}

/**
 * gems_service_jammo_:
 * @element: Connection element which has incoming data in network buffer
 * @service: The role in the handshake process, TRUE = service role
 *
 * Process an error message in the network buffer of connection @element.
 * Processes the error messages created in the handshake and version
 * verification process in the given role (@service).
 *
 * Returns: A #gboolean with value TRUE if there are no errors with the
 * connection @element, FALSE when the connection @element should be removed
 * from list.
 */ 
gboolean gems_service_jammo_process_error(gems_connection *element, gboolean service)
{
	gems_components* data = gems_get_data();
	gboolean success = TRUE;
	guint16 errortype = 0;
	gchar* logmsg = NULL;
	
	if(!element) return TRUE;
	
	// Check what service sent
	if(gems_connection_get_16(element, (sizeof(guint16)+sizeof(guint32))) == JAMMO_SERVICE_ID)
	{
		switch((errortype = gems_connection_get_16(element, (sizeof(guint16)+sizeof(guint32)+sizeof(guint16)))))
		{
			case ERROR_ALREADY_CONNECTED_TYPE:
				logmsg = g_strdup_printf("gems_service_jammo_process_error: ERROR_ALREADY_CONNECTED from %s/%u", 
					ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
				gems_get_data()->connection_errorlist = gems_connection_error_add(gems_get_errorlist(),ph_c_connection_get_device_checksum(element->connection),ERROR_ALREADY_CONNECTED);
				
				success = FALSE; // remove this connection in gems_service_jammo_process_list
				//data->connection_errorlist = gems_connection_error_add(data->connection_errorlist,ph_c_connection_get_device_checksum(element->connection), ERROR_ALREADY_CONNECTED);
				break;
			case ERROR_VERSION_MISMATCH_TYPE:
				logmsg = g_strdup_printf("gems_service_jammo_process_error: ERROR_VERSION_MISMATCH from %s/%u", 
					ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
				element->connection_state = JAMMO_VERSION_MISMATCH; // add to rejected list in gems_service_jammo_process_list
				break;
			case ERROR_INVALID_SERVICE_ID_TYPE:
				// TODO handle this
				logmsg = g_strdup_printf("gems_service_jammo_process_error: ERROR_INVALID_SERVICE_ID from %s/%u", 
					ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
				success = FALSE;
				break;
			case ERROR_INVALID_STATE_TYPE:
				// TODO check what is current state - revert to previous state?
				logmsg = g_strdup_printf("gems_service_jammo_process_error: ERROR_INVALID_STATE from %s/%u", 
					ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
				break;
			case ERROR_CONNECTION_REJECTED_TYPE:
				logmsg = g_strdup_printf("gems_service_jammo_process_error: ERROR_CONNECTION_REJECTED_TYPE from %s/%u", 
					ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
				data->connection_errorlist = gems_connection_error_add(data->connection_errorlist,ph_c_connection_get_device_checksum(element->connection), ERROR_REJECTED);
				break;
			default:
				logmsg = g_strdup_printf("gems_service_jammo_process_error: unexpected error type: %d from %s/%u", 
					errortype, ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
				break;
		}
	}
	else
	{
		logmsg = g_strdup_printf("gems_service_jammo_process_error: unexpected service type: %d from %s/%u", 
			gems_connection_get_16(element, (sizeof(guint16)+sizeof(guint32))), 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
		// TODO send invalid service id ?
	}
	
	// Clear buffer
	gems_connection_clear_buffer(element);
	
	return success;
}

/**
 * gems_service_jammo_init:
 *
 * Initialize JamMo service data.
 *
 * Returns: pointer to new new #gems_service_jammo struct
 */ 
gems_service_jammo* gems_service_jammo_init()
{
	// JamMo service
	gems_service_jammo* data = g_new(gems_service_jammo,sizeof(gems_service_jammo));
	
	// Initialize JamMo servicelist
	data->connections = NULL;
	
	// Disabled by default
	data->enabled = FALSE;
	
	data->port = 0;
		
	return data;
}

/**
 * gems_service_jammo_cleanup:
 *
 * Clean and delete allocated #gems_service_jammo struct and its data.
 *
 * Returns: void
 */ 
void gems_service_jammo_cleanup()
{
	gchar* logmsg = NULL;
	gems_service_jammo* data = gems_get_data()->service_jammo;
	
	if(!data) return;
	logmsg = g_strdup_printf("gems_service_jammo_cleanup");
	cem_add_to_log(logmsg,J_LOG_DEBUG);
	g_free(logmsg);
	
	gems_service_jammo_cleanup_lists();
	
	data->enabled = FALSE;
	g_free(data);
	data = NULL;
}

/**
 * gems_service_jammo_cleanup_lists:
 *
 * Delete connections in the initialized connections list.
 *
 * Returns: void
 */ 
void gems_service_jammo_cleanup_lists()
{
	gems_service_jammo* data = gems_get_data()->service_jammo;

	if(data->connections != NULL)
	{
		g_list_foreach(data->connections,(GFunc)gems_clear_gems_connection,NULL);
		g_list_free(data->connections);
		data->connections = NULL;
	}
}

