/*
 * gems.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Tommi Kallonen <tommi.kallonen@lut.fi>
 *          Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#ifndef _GEMS_H_
#define _GEMS_H_

#include <gst/gst.h>

#include "gems_structures.h"
#include "gems_service_jammo.h"
#include "gems_service_profile.h"
#include "gems_service_group_management.h"
#include "gems_profile_manager.h"
#include "gems_service_collaboration.h"
#include "gems_service_control.h"
#include "gems_service_mentor.h"
#include "gems_service_songbank.h"


void jammo_gems_init(int argc, char* argv[]);
void jammo_gems_cleanup();
void jammo_gems_cleanup_lists();

// Functions for using gems elements
gems_components* gems_get_data();
void gems_free_data();

ProfileManager* gems_get_profile_manager();

MPeerHood* gems_get_peerhood();
gboolean gems_peerhood_enabled();

GList* gems_get_initialized_connections();
gems_communication_data* gems_get_communication();

gems_security_context* gems_get_security_contexts();

gems_service_jammo* gems_get_service_jammo();
//gems_service_aa* gems_get_service_aa();
gems_service_profile* gems_get_service_profile();
gems_service_group* gems_get_service_group();
gems_service_collaboration* gems_get_service_collaboration();
gems_service_control* gems_get_service_control();
gems_service_mentor* gems_get_service_mentor();
gems_teacher_connection* gems_get_teacher_connection();
gboolean gems_teacher_connection_is_connected();

gboolean gems_is_waiting_for_teacher();
void gems_set_waiting_for_teacher();
void gems_unset_waiting_for_teacher();

gboolean gems_is_waiting_profile_from_teacher();
void gems_set_waiting_profile_from_teacher();
void gems_unset_waiting_profile_from_teacher();

void gems_unset_teacher_profile_timeout_sid(gpointer param);
void gems_increase_teacher_profile_request_counter();
void gems_reset_teacher_profile_request_counter();
guint gems_get_teacher_profile_request_count();
void gems_set_login_username(const gchar* username, gboolean forced);
gchar* gems_get_login_username();
void gems_clear_login_username();
gboolean gems_login_username_forced();
GList* gems_get_errorlist();

void gems_list_profile_parameters();//Testing only

/*Communication interface - See D2.4*/
/*The user interface calls these to inform about user actions */

void gems_add_new_sample_to_track(guint loop_id, guint slot);
void gems_remove_sample_from_slot(guint slot);
void gems_loop_sync(GList * list);
void gems_midi_events_to_track(GemsEventListOperation operation, gint16 instrument_id, GList * list);
void gems_slider_events_to_track(GemsEventListOperation operation, gint16 instrument_id, GList * list);
void gems_start_pair_game(gint16 theme, gint16 variation);
int gems_create_game_server(int port);//For testing
int gems_create_game_client(char *address, int port);//For testing

// List other JamMos which we have connected to
GList* gems_list_jammo_connections();

// Get a connection to user with given id
gems_connection* gems_get_connection_with_user_id(guint32 userid);

// Get pointer to profile of an connection element
gems_peer_profile* gems_get_profile(gems_connection* element);

/* PeerHood - */
void gems_ph_callback_notify(short aEvent, const char* aAddress,void* aData);
void gems_ph_callback_newconnection(const unsigned short aPort, MAbstractConnection* aConnection, int aConnectionId, void* aData);

// Process connections
gint gems_process_connections();

// Show content of connection lists
void gems_connection_debugging();

void gems_debugging_show_jammos();

//CALLBACKS for CHUM
void (*chum_callback_add_loop_to_slot)(GObject * game, guint user_id, guint id, guint slot);
void (*chum_callback_remove_loop_from_slot)(GObject * game, guint user_id, guint slot);
void (*chum_callback_loop_sync)(GObject * game, guint user_id, GList * list);
void (*chum_callback_midi_list)(GObject * game, guint user_id, gint16 instrument_id, GemsEventListOperation operation, GList * list);
void (*chum_callback_group_control)(gint type);
void (*chum_callback_game)(GObject * game, gint type, const gchar* song_info);
void (*chum_callback_game_starter)(gpointer data, gint16 track_id);
void (*chum_callback_slider_event_list)(GObject * game, guint user_id, gint16 instrument_id, GemsEventListOperation operation, GList * list);
void gems_set_callback_add_loop(void (*func_ptr)(GObject * game, guint user_id, guint id, guint slot) );
void gems_set_callback_remove_loop(void (*func_ptr)(GObject * game, guint user_id, guint slot) );
void gems_set_callback_loop_sync(void (*func_ptr)(GObject * game, guint user_id, GList * list) );
void gems_set_callback_midi_list(void (*func_ptr)(GObject * game, guint user_id, gint16 instrument_id, GemsEventListOperation operation, GList * list) );
void gems_set_callback_slider_event_list(void (*func_ptr)(GObject * game, guint user_id, gint16 instrument_id, GemsEventListOperation operation, GList * list) );
void gems_set_callback_group_control(void (*func_ptr)(gint type) );
void gems_set_callback_game(void (*func_ptr)(GObject * game, gint type, const gchar* song_info) );
void gems_set_callback_game_starter(void (*func_ptr)(gpointer data, gint16 track_id) );

void gems_init_peerhood_callback();
void gems_enable_network_timeout_functions();
void gems_register_services();

#endif  /* _GEMS_H_ */

