/*
 * gems.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Tommi Kallonen <tommi.kallonen@lut.fi>
 *          Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */

#include "gems.h"
#include "../cem/cem.h"
#include "collaboration.h"
#include "../configure.h"

#include "messages.h"
#include "messagehandler.h"
#include "gems_message_functions.h"
#include "groupmanager.h"
#include "gems_utils.h"
#include <glib.h>
#include <arpa/inet.h>
#include <string.h>
#include "songbank.h"

static gems_components* gems_data = NULL;

// empty callbacks for CHUM. These are used to avoid problems when calling an unset callback
static void empty_callback_add_loop_to_slot(GObject * game, guint user_id, guint id, guint slot){}
static void empty_callback_remove_loop_from_slot(GObject * game, guint user_id, guint slot){}
static void empty_callback_loop_sync(GObject * game, guint user_id, GList * list){}
static void empty_callback_midi_list(GObject * game, guint user_id, gint16 instrument_id, GemsEventListOperation operation, GList * list){}
static void empty_callback_slider_event_list(GObject * game, guint user_id, gint16 instrument_id, GemsEventListOperation operation, GList * list){}
static void empty_callback_group_control(gint type){}
static void empty_callback_game(GObject * game, gint type, const gchar* song_info){}
static void empty_callback_game_starter(gpointer data, gint16 track_id){}

/**
 * gems_get_data:
 *
 * Get the main GEMS struct, #gems_components. If the structure is not yet
 * initialized new is created and all data is set with default values. Works
 * almost in the same way as the Singleton pattern.
 *
 * Returns: a pointer to the main GEMS structure
 */
gems_components* gems_get_data()
{
	if(!gems_data)
	{
		// Allocate memory and set internal struct pointers to NULL
		gems_data = g_new0(gems_components,sizeof(gems_components));
		gems_data->pm = NULL;
		gems_data->ph = NULL;
		gems_data->ph_on = FALSE;
		gems_data->ph_cb = NULL;
		gems_data->communication = NULL;
		gems_data->security_context = NULL;
		gems_data->service_jammo = NULL;
		gems_data->service_profile = NULL;
		gems_data->service_group = NULL;
		gems_data->service_control = NULL;
		gems_data->service_mentor = NULL;
		gems_data->service_songbank = NULL;
		gems_data->initialized_connections = NULL;
		gems_data->connection_errorlist = NULL;
		gems_data->service_collaboration = NULL;
		gems_data->teacher_connection = NULL;
		gems_data->teacher_profile_request_timer = NULL;
		gems_data->profile_request_timeout_sid = 0;
		gems_data->teacher_profile_requests = 0;
		gems_data->waiting_for_teacher = FALSE;
		gems_data->waiting_profile_from_teacher = FALSE;
		gems_data->username_to_login = NULL;
		gems_data->username_forced = FALSE;
	}
	return gems_data;
}

/**
 * gems_get_profile_manager:
 *
 * Get the profile manager
 *
 * Returns: a pointer to ProfileManager struct pointing to C++ object
 */
ProfileManager* gems_get_profile_manager()
{
	return profilemanager_get_manager();
}

/**
 * gems_get_peerhood:
 *
 * Get PeerHood if it is enabled, NULL otherwise.
 *
 * Returns: a pointer to MPeerHood struct pointing to C++ object or NULL if it
 * is not enabled.
 */
MPeerHood* gems_get_peerhood()
{
	if(gems_peerhood_enabled()) return gems_get_data()->ph;
	else return NULL;
}

/**
 * gems_peerhood_enabled:
 *
 * Check if the connection to PeerHood exists.
 *
 * Returns: TRUE when connection exists, FALSE otherwise.
 */
gboolean gems_peerhood_enabled()
{
	return gems_get_data()->ph_on;
}

/**
 * gems_get_initialized_connections:
 *
 * Get the list of initialized connections (connections initialized by this
 * device).
 *
 * Returns: a pointer to GList containing pointers to #gems_connection structs.
 */
GList* gems_get_initialized_connections()
{
	return gems_get_data()->initialized_connections;
}

/**
 * gems_get_communication
 *
 * Get the communication data struct.
 *
 * Returns: a pointer to #gems_communication_data structure.
 */
gems_communication_data* gems_get_communication()
{
	return gems_get_data()->communication;
}

/**
 * gems_get_security_contexts:
 *
 * Get the security context data struct.
 *
 * Returns: a pointer to #gems_security_context structure.
 */
gems_security_context* gems_get_security_contexts()
{
	return gems_get_data()->security_context;
}

/**
 * gems_get_service_jammo:
 *
 * Get the JamMo service data struct.
 *
 * Returns: a pointer to #gems_service_jammo structure.
 */
gems_service_jammo* gems_get_service_jammo()
{
	return gems_get_data()->service_jammo;
}

// TODO when AA service is implemented
/*gems_service_aa* gems_get_service_aa()
{
	return NULL; 
}*/

/**
 * gems_get_service_profile:
 *
 * Get the Profile service data struct.
 *
 * Returns: a pointer to #gems_service_profile structure.
 */
gems_service_profile* gems_get_service_profile()
{
	return gems_get_data()->service_profile;
}

/**
 * gems_get_service_group:
 *
 * Get the Group Management service data struct.
 *
 * Returns: a pointer to #gems_service_group structure.
 */
gems_service_group* gems_get_service_group()
{
	return gems_get_data()->service_group;
}

/**
 * gems_get_service_collaborarion:
 *
 * Get the Collaboration service data struct.
 *
 * Returns: a pointer to #gems_service_collaboration structure.
 */
gems_service_collaboration* gems_get_service_collaboration()
{
	return gems_get_data()->service_collaboration;
}

/**
 * gems_get_service_control:
 *
 * Get the Control service data struct.
 *
 * Returns: a pointer to #gems_service_control structure.
 */
gems_service_control* gems_get_service_control()
{
	return gems_get_data()->service_control;
}

/**
 * gems_get_service_mentor:
 *
 * Get the Mentor service data struct.
 *
 * Returns: a pointer to #gems_service_mentor structure.
 */
gems_service_mentor* gems_get_service_mentor()
{
	return gems_get_data()->service_mentor;
}

/**
 * gems_get_errorlist
 *
 * Get the errorlist containing #gems_connection_error structures.
 *
 * Returns: a #GList of error connections.
 */
GList* gems_get_errorlist()
{
	return gems_get_data()->connection_errorlist;
}

/**
 * gems_get_teacher_connection:
 *
 * Get the teacher connection data structure.
 *
 * Returns: pointer to #gems_teacher_connection struct.
 */
gems_teacher_connection* gems_get_teacher_connection()
{
	return gems_get_data()->teacher_connection;
}

/**
 * gems_is_waiting_for_teacher:
 *
 * Check if this device is set for waiting for teacher.
 *
 * Returns: TRUE when waiting, FALSE otherwise.
 */
gboolean gems_is_waiting_for_teacher()
{
	return gems_get_data()->waiting_for_teacher;
}

/**
 * gems_set_waiting_for_teacher:
 *
 * Set the device to wait for teacher.
 *
 * Returns: void
 */
void gems_set_waiting_for_teacher()
{
	// TODO call CHUM to "draw the curtain"
	gems_get_data()->waiting_for_teacher = TRUE;
}

/**
 * gems_unset_waiting_for_teacher:
 *
 * Set the device to not to wait for teacher.
 *
 * Returns: void
 */
void gems_unset_waiting_for_teacher()
{
	// TODO call CHUM to "remove curtain"
	gems_get_data()->waiting_for_teacher = FALSE;
}

/**
 * gems_is_waiting_profile_from_teacher:
 *
 * Check if the device is set to wait for a profile from teacher.
 *
 * Returns: TRUE if is set, FALSE otherwise.
 */
gboolean gems_is_waiting_profile_from_teacher()
{
	return gems_get_data()->waiting_profile_from_teacher;
}

/**
 * gems_set_waiting_profile_from_teacher:
 *
 * Set the device to wait profile from teacher. Will be set after the profile
 * is requested from teacher if it was not found on the local device.
 *
 * Returns: void.
 */
void gems_set_waiting_profile_from_teacher()
{
	gems_get_data()->waiting_profile_from_teacher = TRUE;
}

/**
 * gems_unset_waiting_profile_from_teacher:
 *
 * Unset the device waiting for profile boolean. Reset the profile request
 * counter.
 *
 * Returns: void
 */
void gems_unset_waiting_profile_from_teacher()
{
	gems_get_data()->waiting_profile_from_teacher = FALSE;
	gems_reset_teacher_profile_request_counter();
}

/**
 * gems_unset_teacher_profile_timeout_sid:
 * @param: unused parameter
 *
 * Remote the profile request timeout function from Glib main loop and reset
 * the profile request counter.
 *
 * Returns: void.
 */
void gems_unset_teacher_profile_timeout_sid(gpointer param)
{
	cem_add_to_log("Unset teacher profile timeout",J_LOG_NETWORK_DEBUG);
	gems_get_data()->profile_request_timeout_sid = 0;
	gems_reset_teacher_profile_request_counter();
}

/**
 * gems_increase_teacher_profile_request_counter:
 *
 * Increase the profile request counter with one.
 *
 * Returns: void
 */
void gems_increase_teacher_profile_request_counter()
{
	gems_get_data()->teacher_profile_requests++;
}

/**
 * gems_reset_teacher_profile_request_counter:
 *
 * Reset the profile request counter.
 *
 * Returns: void
 */
void gems_reset_teacher_profile_request_counter()
{
	gems_get_data()->teacher_profile_requests = 0;
}

/**
 * gems_get_teacher_profile_request_count:
 *
 * Get the amount of profile requests sent
 *
 * Returns: the amount of profile requests
 */
guint gems_get_teacher_profile_request_count()
{
	return gems_get_data()->teacher_profile_requests;
}

/**
 * gems_set_login_username:
 * @username: New login username, duplicate is created
 * @forced: TRUE when username set by teacher, FALSE if set by user
 *
 * Set the @username to be used for login. First clears the old username if it
 * was set and then creates a duplicate from the given @username.
 *
 * Returns: void
 */
void gems_set_login_username(const gchar* username, gboolean forced)
{
	gems_clear_login_username();
	gems_get_data()->username_to_login = g_strdup(username);
	gems_get_data()->username_forced = forced;
}

/**
 * gems_get_login_username:
 *
 * Get the username set for login
 *
 * Returns: the username as #gchar
 */
gchar* gems_get_login_username()
{
	return gems_get_data()->username_to_login;
}

/**
 * gems_clear_login_username:
 *
 * Erase the login username if it was set.
 *
 * Returns: void
 */
void gems_clear_login_username()
{
	if(gems_get_data()->username_to_login)
	{
		g_free(gems_get_data()->username_to_login);
		gems_get_data()->username_to_login = NULL;
	}
}

/**
 * gems_login_username_forced:
 *
 * Check if the login username was forced.
 *
 * Returns: TRUE when forced, FALSE otherwise.
 */
gboolean gems_login_username_forced()
{
	return gems_get_data()->username_forced;
}

/**
 * gems_teacher_connection_is_connected:
 *
 * Check if the connection to teacher exists.
 *
 * Returns: TRUE if it exists, FALSE otherwise.
 */
gboolean gems_teacher_connection_is_connected()
{
	if(gems_get_teacher_connection() != NULL)
	{
		if(gems_get_teacher_connection()->connection != NULL)
		{
			if(gems_get_teacher_connection()->connection->connection != NULL)
			{
				// Teacher is connected?
				if(ph_c_connection_is_connected(gems_get_teacher_connection()->connection->connection)) return TRUE;
			}
		}
	}
	return FALSE;
}

/**
 * gems_free_data:
 *
 * Free the #gems_components data structure.
 *
 * Returns: void
 */
void gems_free_data()
{
	if(gems_data)
	{
		g_free(gems_data);
		gems_data = NULL;
	}
}
 
/**
 * jammo_gems_init:
 * @argc: argument count
 * @argv: array of arguments
 *
 * Initialize GEMS. Initializes internal structures, services, connection to
 * PeerHood and callbacks.
 *
 * Returns: void
 */
void jammo_gems_init(int argc, char* argv[])
{
	gchar* logmsg = NULL;
	
	// All components
	gems_components* components = gems_get_data();
	
	// Initialize timeout function id table
	for(gint i = 0; i < TO_FUNCTIONS; i++) components->timeout_functions[i] = 0;
	
	// Profile manager
	components->pm = profilemanager_get_manager();
	
	// Initialize communication
	components->communication = gems_communication_init(components->communication);
	
	// Initialize JamMo service
	components->service_jammo = gems_service_jammo_init();
	
	// Initialize Profile service
	components->service_profile = gems_service_profile_init(components->service_profile);
	
	// Initialize Group management service
	components->service_group = gems_service_group_init();

	// Initialize Collaboration service
	components->service_collaboration = gems_service_collaboration_init();

	// Initialize Control service
	components->service_control = gems_service_control_init();

	// Initialize Songbank service
	components->service_songbank = gems_service_songbank_init(components, configure_get_jammo_directory());
	
	// Initialize teacher connection
	components->teacher_connection = gems_teacher_connection_init();
	
	// Create callback
	gems_init_peerhood_callback();
	
	// Get PeerHood instance
	components->ph = ph_c_get_instance(components->ph_cb);
	
	if(ph_c_init(components->ph,argc,argv) == TRUE)
	{
		gems_register_services();
		components->ph_on = TRUE;
	}
	else
	{
		components->ph_on = FALSE;
		logmsg = g_strdup_printf("No PeerHood running, probing of peerhood enabled.");
		cem_add_to_log(logmsg,J_LOG_NETWORK);
		g_free(logmsg);
		
		components->service_jammo->enabled = FALSE;
		components->service_profile->enabled = FALSE;
		components->service_group->enabled = FALSE;
		components->service_collaboration->enabled = FALSE;
		components->service_control->enabled = FALSE;
		components->service_songbank->enabled = FALSE;
		
		/* No need to delete these...
		ph_c_delete_peerhood(components->ph);
		components->ph = NULL;
		
		ph_c_delete_callback(components->ph_cb);
		components->ph_cb = NULL;*/
		
		components->timeout_functions[10] = g_timeout_add_full(G_PRIORITY_HIGH,2000,(GSourceFunc)gems_communication_probe_for_peerhood, NULL, NULL);
	}
	

	//Init CHUM Callbacks.
	chum_callback_add_loop_to_slot=empty_callback_add_loop_to_slot;
	chum_callback_remove_loop_from_slot=empty_callback_remove_loop_from_slot;
	chum_callback_loop_sync=empty_callback_loop_sync;
	chum_callback_midi_list=empty_callback_midi_list;
	chum_callback_slider_event_list=empty_callback_slider_event_list;
	chum_callback_group_control=empty_callback_group_control;
	chum_callback_game=empty_callback_game;
	chum_callback_game_starter=empty_callback_game_starter;

	logmsg = g_strdup_printf("jammo_gems_init: done");
	cem_add_to_log(logmsg,J_LOG_DEBUG);
	g_free(logmsg);
	//return components;
}

/**
 * jammo_gems_cleanup:
 * 
 * Delete the components of GEMS and free the structs. Disconnect from Peerhood
 *
 * Returns: void
 */
void jammo_gems_cleanup()
{
	gchar* logmsg = NULL;
	gems_components* components = gems_get_data();
	
	if(components->service_group->group_info->id != NOT_ACTIVE && 
		components->service_group->group_info->own_state == IN_GROUP)	gems_group_leave_from_group();
	
	logmsg = g_strdup_printf("jammo_gems_cleanup: start");
	cem_add_to_log(logmsg,J_LOG_DEBUG);
	g_free(logmsg);
	
	// Remove timeout functions
	for(gint i = 0; i < TO_FUNCTIONS; i++) if(components->timeout_functions[i] != 0) g_source_remove(components->timeout_functions[i]);
	
	if(components->pm != NULL) profilemanager_delete_manager(components->pm);
	
	// Clean lists
	jammo_gems_cleanup_lists();
	
	// Clear communication
	gems_communication_cleanup();
	
	// Clear Profile service related
	if(components->service_profile)
	{
		if(components->service_profile->enabled == TRUE && gems_peerhood_enabled()) ph_c_unregister_service(components->ph,PROFILE_SERVICE_NAME);
		gems_service_profile_cleanup();
	}
	
	// Clear JamMo service related
	if(components->service_jammo)
	{
		if(components->service_jammo->enabled == TRUE && gems_peerhood_enabled()) ph_c_unregister_service(components->ph,JAMMO_SERVICE_NAME);
		gems_service_jammo_cleanup();
	}
	
	// Clear Group management service related
	if(components->service_group)
	{
		if(components->service_group->enabled == TRUE && gems_peerhood_enabled()) ph_c_unregister_service(components->ph,GROUP_SERVICE_NAME);
		gems_service_group_cleanup();
	}
	
	// Clear Collaboration service related
	if(components->service_collaboration)
	{
		if(components->service_collaboration->enabled == TRUE && gems_peerhood_enabled()) ph_c_unregister_service(components->ph,COLLABORATION_SERVICE_NAME);
		gems_service_collaboration_cleanup();
	}
	
	if(components->service_control)
	{
		if(components->service_control->enabled == TRUE && gems_peerhood_enabled()) ph_c_unregister_service(components->ph,CONTROL_SERVICE_NAME);
		gems_service_control_cleanup();
	}

	if(components->service_mentor)
	{
		if(components->service_mentor->enabled == TRUE && gems_peerhood_enabled()) ph_c_unregister_service(components->ph,MENTOR_SERVICE_NAME);
		gems_service_mentor_cleanup();
	}
	if(components->service_songbank)
	{
		if(components->service_songbank->enabled == TRUE && gems_peerhood_enabled()) ph_c_unregister_service(components->ph,SONGBANK_SERVICE_NAME);
		gems_service_songbank_clean();
	}
	// Clear the profile request timer
	if(components->teacher_profile_request_timer)
	{
		g_timer_destroy(components->teacher_profile_request_timer);
		components->teacher_profile_request_timer = NULL;
	}
	
	if(components->profile_request_timeout_sid != 0) g_source_remove(components->profile_request_timeout_sid);
	if(components->username_to_login) g_free(components->username_to_login);
	gems_clear_teacher_connection();
	
	// delete peerhood
	if(components->ph != NULL)
	{
		ph_c_delete_peerhood(components->ph);
		components->ph = NULL;
	}
	
	// delete callback
	if(components->ph_cb != NULL)
	{
		ph_c_delete_callback(components->ph_cb);
		components->ph_cb = NULL;
	}
	
	// Free the structure
	gems_free_data();
	
	logmsg = g_strdup_printf("jammo_gems_cleanup: done");
	cem_add_to_log(logmsg,J_LOG_DEBUG);
	g_free(logmsg);
}

/**
 * jammo_gems_cleanup_lists:
 *
 * Clean the initialized connections and error lists.
 * 
 * Returns: void
 */
void jammo_gems_cleanup_lists()
{
	gems_components* components = gems_get_data();
	
	gems_free_list_saved_profiles();
	
	if(components->initialized_connections != NULL)
	{
		// Initialized connections
		g_list_foreach(components->initialized_connections, (GFunc)gems_clear_gems_connection,NULL);
		g_list_free(components->initialized_connections);
		components->initialized_connections = NULL;
	}
	
	if(components->connection_errorlist != NULL)
	{
		// Error list
		g_list_foreach(components->connection_errorlist,(GFunc)gems_clear_connection_error,NULL);
		g_list_free(components->connection_errorlist);
		components->connection_errorlist = NULL;
	}
}

void gems_list_profile_parameters()
{
	printf("OWN PROFILE (%u):\nusername:\t%s\nfull name:\t%s %s\nuser age:\t%d\ntotal points:\t%d\navatar:\t%u\n",
		gems_profile_manager_get_userid(NULL),
		gems_profile_manager_get_username(NULL),
		gems_profile_manager_get_firstname(),
		gems_profile_manager_get_lastname(),
		gems_profile_manager_get_age(NULL),
		gems_profile_manager_get_points(),
		gems_profile_manager_get_avatar_id(NULL));
}

/**
 * gems_add_new_sample_to_track:
 * @loop_id:
 * @slot:
 *
 * Forward the messages to collaboration module
 *
 * Returns: void
 */
void gems_add_new_sample_to_track(guint loop_id, guint slot)
{
	collaboration_add_new_sample_to_track(loop_id, slot);
}

/**
 * gems_remove_sample_from_slot:
 * @slot:
 *
 * Forward the messages to collaboration module
 *
 * Returns: void
 */
void gems_remove_sample_from_slot(guint slot)
{
	collaboration_remove_sample_from_slot(slot);
}

/**
 * gems_loop_sync:
 * @list:
 *
 * Synchronize loop list? Forward the messages to collaboration module.
 *
 * Returns: void
 */
void gems_loop_sync(GList * list)
{
	collaboration_loop_sync(list);
}

/**
 * gems_midi_events_to_track:
 * @operation:
 * @instrument_id:
 * @list:
 *
 * Forward the messages to collaboration module
 *
 * Returns: void
 */
void gems_midi_events_to_track(GemsEventListOperation operation, gint16 instrument_id, GList * list)
{
	collaboration_midi_events_to_track(operation, instrument_id, list);
}

/**
 * gems_slider_events_to_track:
 * @operation:
 * @instrument_id:
 * @list:
 *
 * Forward the messages to collaboration module
 */
void gems_slider_events_to_track(GemsEventListOperation operation, gint16 instrument_id, GList * list) {
	collaboration_slider_events_to_track(operation, instrument_id, list);
}

/**
 * gems_start_pair_game:
 * @theme: theme id to be used in the game
 * @variation:
 *
 * Start pair game. Forward the messages to collaboration module.
 *
 * Returns: void
 */
void gems_start_pair_game(gint16 theme, gint16 variation)
{
		gems_components* data = gems_get_data();
		collaboration_start_pair_game(1, 1, data);
}

/**
 * gems_create_game_server:
 * @port:
 *
 * Unused. Does nothing.
 *
 * Returns: 0
 */
int gems_create_game_server(int port)
{
	return 0;
	//return collaboration_create_game_server(port);
}

/**
 * gems_create_game_client:
 * @address:
 * @port:
 *
 * Unused. Does nothing.
 *
 * Returns: 0
 */
int gems_create_game_client(char *address, int port)
{
	return 0;
	//return collaboration_create_game_client(address, port);
}


/**
 * gems_list_jammo_connections:
 *
 * List other JamMos which this device is connected to. Returns a COPY of the
 * internal list with same data. Data is not copied, only the pointers. The
 * list must be free'd by the caller but the data MUST be left intact.
 *
 * Returns: a pointer to #GList containing pointers to #gems_connection structs
 * or NULL if the data cannot be accessed or the list is empty.
 */
GList* gems_list_jammo_connections()
{
	gems_components* data = gems_get_data();
	
	if(data == NULL) return NULL;
	if(data->communication == NULL) return NULL;
	if(data->communication->connections == NULL) return NULL;
	else
	{
		return g_list_copy(data->communication->connections);
	}
}

/**
 * gems_get_connection_with_user_id:
 * @userid: User id to be used for searching
 * 
 * Get a connection with selected @userid, returns the #gems_connection struct
 * if found or NULL otherwise.
 *
 * Returns: a pointer to the connection structure or NULL if not found.
 */
gems_connection* gems_get_connection_with_user_id(guint32 userid)
{
	gems_components* data = gems_get_data();
	
	if(data == NULL) return NULL;
	if(data->communication == NULL) return NULL;
	if(data->communication->connections == NULL) return NULL;
	
	return gems_communication_get_connection_with_userid(data->communication->connections,userid);
}

/**
 * gems_get_profile:
 * @element: Connection element to use
 *
 * Get pointer to profile from the given connection @element.
 *
 * Returns: a pointer to #gems_peer_profile or NULL if profile is not retrieved
 */
gems_peer_profile* gems_get_profile(gems_connection* element)
{
	if(element == NULL) return NULL;
	else return element->profile;
}

/**
 * gems_ph_callback_notify:
 * @aEvent: Event id
 * @aAddress: Address of the device causing the event
 * @aData: User data
 *
 * Notify callback - called by C_Callback when notify event occurs. Used only 
 * by the Peerhood library callback. Is registered into the C_Callback as
 * function pointer.
 * 
 * Returns: void.
 */
void gems_ph_callback_notify(short aEvent, const char* aAddress,void* aData)
{
	gchar* logmsg = g_strdup_printf("gems_ph_callback_notify: event notify (%d) from %s.", aEvent,aAddress);
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
}


/**
 * gems_ph_callback_newconnection:
 * @aPort: Port which was connected
 * @aConnection: New connection object
 * @aConnectionId: Id of the connection
 * @aData: User data
 *
 * New connection callback - called by C_Callback when new connection is 
 * established. Used only by the Peerhood library callback. Is registered into
 * the C_Callback as function pointer. Checks if the connection is established
 * into correct port (correct service), only "JamMo" and "Mentor" services 
 * accept new connections, additionally the "Collaboration" service accepts a
 * new connection if there is an connection between the devices and the users
 * are in the same group. Only one "basic" connection between device pairs are
 * allowed, otherwise the created connection is disconnected. If the device 
 * checksum is found from error lists as rejected connection it will not be
 * accepted. New accepted connections are set into %JAMMO_CONNECTING
 * #jammo_state and moved into initialized connections list.
 *
 * Returns: void
 */

void gems_ph_callback_newconnection(const unsigned short aPort, MAbstractConnection* aConnection, int aConnectionId, void* aData)
{
	gems_components* components = NULL;
	gems_message* err = NULL;
	gems_message* message = NULL;
	gint isconnected = 0;
	gchar* logmsg = NULL;
	gchar* logerror = NULL;
	
	if(aData == NULL) return;
	
	components = (gems_components*)aData;

	// TODO add check that the other end is using WLAN atm
	
	// If there is no connection to Peerhood daemon all inbound connections are dropped
	if(!gems_peerhood_enabled())
	{
		cem_add_to_log("gems_ph_callback_newconnection: Got new connection while connection to peerhoodd does not exist, disconnecting",
			J_LOG_NETWORK_DEBUG);
		ph_c_connection_delete(aConnection);
		return;
	}

	if(components != NULL)
	{
		// Connecting to JamMo
		if(aPort == components->service_jammo->port)
		{
			if(components->service_jammo->enabled == FALSE)
			{
				err = gems_create_error_message(ERROR_SERVICE_NOT_RUNNING_TYPE, JAMMO_SERVICE_ID);
					
				if(gems_communication_write_data(ph_c_connection_get_fd(aConnection),err) != err->length)
				{
					logerror = g_strdup_printf("gems_ph_callback_newconnection: cannot write ERROR_SERVICE_NOT_RUNNING (%s) to %s/%u",
						JAMMO_SERVICE_NAME, ph_c_connection_get_remote_address(aConnection),ph_c_connection_get_device_checksum(aConnection));
					cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
					g_free(logerror);
				}
				else
				{
					logmsg = g_strdup_printf("gems_ph_callback_newconnection: Wrote ERROR_SERVICE_NOT_RUNNING (%s) to %s/%u",
						JAMMO_SERVICE_NAME, ph_c_connection_get_remote_address(aConnection),ph_c_connection_get_device_checksum(aConnection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
				}
			
				gems_clear_message(err);
				ph_c_connection_delete(aConnection);
				return;
			}
		}
		
		// Connecting to Collaboration (game connection)
		else if(aPort == components->service_collaboration->port)
		{
			if(components->service_collaboration->enabled == FALSE)
			{
				err = gems_create_error_message(ERROR_SERVICE_NOT_RUNNING_TYPE, COLLABORATION_SERVICE_ID);
					
				if(gems_communication_write_data(ph_c_connection_get_fd(aConnection),err) != err->length)
				{
					logerror = g_strdup_printf("gems_ph_callback_newconnection: cannot write ERROR_SERVICE_NOT_RUNNING (%s) to %s/%u",
						COLLABORATION_SERVICE_NAME, ph_c_connection_get_remote_address(aConnection),ph_c_connection_get_device_checksum(aConnection));
					cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
					g_free(logerror);
				}
				else
				{
					logmsg = g_strdup_printf("gems_ph_callback_newconnection: Wrote ERROR_SERVICE_NOT_RUNNING (%s) to %s/%u",
						COLLABORATION_SERVICE_NAME, ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
				}
			
				gems_clear_message(err);
				ph_c_connection_delete(aConnection);
				return;
			}
		}
			
		// Connecting to control (teacher)
		else if(aPort == components->service_control->port)
		{
			if(components->service_control->enabled == FALSE)
			{
				err = gems_create_error_message(ERROR_SERVICE_NOT_RUNNING_TYPE, CONTROL_SERVICE_ID);
					
				if(gems_communication_write_data(ph_c_connection_get_fd(aConnection),err) != err->length)
				{
					logerror = g_strdup_printf("gems_ph_callback_newconnection: cannot write ERROR_SERVICE_NOT_RUNNING (%s) to %s/%u", 
						CONTROL_SERVICE_NAME, ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
					cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
					g_free(logerror);
				}
				else
				{
					logmsg = g_strdup_printf("gems_ph_callback_newconnection: Wrote ERROR_SERVICE_NOT_RUNNING (%s) to %s/%u",
						CONTROL_SERVICE_NAME, ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
				}
			
				gems_clear_message(err);
				ph_c_connection_delete(aConnection);
				return;
			}
			else
			{
				if(components->teacher_connection != NULL)
				{
					if(components->teacher_connection->connection == NULL)
					{
						// TODO set proper teacher authentication state
						components->teacher_connection->connection = gems_new_gems_connection(aConnection, aPort, aConnectionId, JAMMO_CONNECTED_NO_AUTH, 0);
						logmsg = g_strdup_printf("gems_ph_callback_newconnection: TEACHER CONNECTED from %s", ph_c_connection_get_remote_address(aConnection));
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
					}
					// Connection was established before
					else
					{
						err = gems_create_error_message(ERROR_CONNECTION_REJECTED_TYPE, CONTROL_SERVICE_ID);
				
						if(gems_communication_write_data(ph_c_connection_get_fd(aConnection),err) != err->length)
						{
							logerror = g_strdup_printf("gems_ph_callback_newconnection: cannot write ERROR_CONNECTION_REJECTED (%s) to \"TEACHER\" (%s/%u)", 
								CONTROL_SERVICE_NAME, ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
							cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
							g_free(logerror);
						}
						else
						{
							logmsg = g_strdup_printf("gems_ph_callback_newconnection: Wrote ERROR_CONNECTION_REJECTED (%s) to \"TEACHER\" %s/%u",
								CONTROL_SERVICE_NAME, ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
							cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
						}
						
						gems_clear_message(err);
						
						logmsg = g_strdup_printf("gems_ph_callback_newconnection: Connection to %s from %s/%u, TEACHER is already connected!",
							CONTROL_SERVICE_NAME, ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						
						ph_c_connection_delete(aConnection);
					}
					
				}
				return;
			}
		}
		
		// Invalid port
		else
		{
			logmsg = g_strdup_printf("gems_ph_callback_newconnection: %s/%u connecting to wrong port (connecting port %u)", 
				ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection), aPort);
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
			
			err = gems_create_error_message(ERROR_CONNECTION_REJECTED_TYPE, 0);
				
			if(gems_communication_write_data(ph_c_connection_get_fd(aConnection),err) != err->length)
			{
				logerror = g_strdup_printf("gems_ph_callback_newconnection: cannot write ERROR_CONNECTION_REJECTED to %s/%u",
					ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
				cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
				g_free(logerror);
			}
			else
			{
				logmsg = g_strdup_printf("gems_ph_callback_newconnection: Wrote ERROR_CONNECTION_REJECTED to %s/%u", 
					ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
			}
			
			gems_clear_message(err);
			ph_c_connection_delete(aConnection);
			return;
		}
		
		// Not in any list - new connection
		if((isconnected = gems_communication_already_connected(ph_c_connection_get_device_checksum(aConnection))) == NOT_IN_LIST)
		{
			logmsg = g_strdup_printf("gems_ph_callback_newconnection: Adding a connection with cid %d, address %s and checksum %u", 
				aConnectionId, ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
		
			// Initialize connection object struct
			gems_connection* init_connection = gems_new_gems_connection(aConnection, aPort, aConnectionId, JAMMO_CONNECTING, 0);
		
			// Add to initialized list
			components->initialized_connections = g_list_append(components->initialized_connections,(init_connection));
			
			// Connection success, remove from errors. If no such device in the list, nothing is done
			components->connection_errorlist = gems_connection_error_remove(components->connection_errorlist,ph_c_connection_get_device_checksum(aConnection));
		}
		else if(isconnected == IN_REJECTED_LIST)
		{
			logmsg = g_strdup_printf("gems_ph_callback_newconnection: connection to %s/%u is in rejected list", 
				ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
			
			message = gems_create_error_message(ERROR_CONNECTION_REJECTED_TYPE,JAMMO_SERVICE_ID);
			
			switch(gems_communication_write_data(ph_c_connection_get_fd(aConnection),message))
			{
				case -1:
					logerror = g_strdup_printf("gems_ph_callback_newconnection: Cannot write error ERROR_CONNECTION_REJECTED to %s/%u",
						ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
					cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
					g_free(logerror);
					break;
				case 0:
					logerror = g_strdup_printf("gems_ph_callback_newconnection: Wrote nothing to %s/%u",
						ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
					cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
					g_free(logerror);
					break;
				default:
					logmsg = g_strdup_printf("gems_ph_callback_newconnection: Wrote ERROR_CONNECTION_REJECTED message data to %s/%u",
						ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
					break;
			}
			gems_clear_message(message);
			ph_c_connection_delete(aConnection); // Disconnects and deletes
			
		}
		
		else // Already in some list
		{
			// Connected AND connecting to collaboration service AND group active - might try to establish collaboration connection (for games)
			if((isconnected == IN_CONNECTED_LIST))
			{
				// Collaboration service was checked earlier
				if(aPort == components->service_collaboration->port)
				{
					// Is group active?
					if(gems_group_active() == TRUE)
					{
						// Get connection for this checksum
						gems_connection* element = gems_communication_get_connection(gems_get_data()->communication->connections,ph_c_connection_get_device_checksum(aConnection));
		
						if(element != NULL) // Found - should be always found
						{
							if(element->profile != NULL) // Profile requested
							{
								if(gems_group_is_in_group(element->profile->id) == TRUE) // In our group
								{
									element->game_connection = aConnection; // Add as game connection
									logmsg = g_strdup_printf("gems_ph_callback_newconnection: Added Game connection for Collaboration (%s/%u)",
										ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
									cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
									g_free(logmsg);
									return;
								}
								// Not in our group - disconnect
								else
								{
									logerror = g_strdup_printf("gems_ph_callback_newconnection: Connection from %s/%u to %s who is not in the same group, disconnecting.", 
									ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection), COLLABORATION_SERVICE_NAME);
									cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
									g_free(logerror);
									
									err = gems_create_error_message(ERROR_NOT_IN_GROUP_TYPE,COLLABORATION_SERVICE_ID);
		
									if(gems_communication_write_data(ph_c_connection_get_fd(aConnection),err) != err->length)
									{
										logerror = g_strdup_printf("gems_ph_callback_newconnection: error sending ERROR_NOT_IN_GROUP to %s/%u",
											ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
										cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
										g_free(logerror);
									}
									gems_clear_message(err);
						
									ph_c_connection_delete(aConnection);
									return;
								}
							}
							else
							{
								// TODO error - no profile
								logerror = g_strdup_printf("gems_ph_callback_newconnection: Connection from %s/%u to %s - no profile retrieved yet, disconnecting.", 
								ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection), COLLABORATION_SERVICE_NAME);
								cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
								g_free(logerror);
							
								ph_c_connection_delete(aConnection);
								return;
							}
						}
					}
					else
					{
						//error - group not active
						logerror = g_strdup_printf("gems_ph_callback_newconnection: Connection from %s/%u to %s when group was not active.", 
						ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection), COLLABORATION_SERVICE_NAME);
						cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
						g_free(logerror);
					
						err = gems_create_error_message(ERROR_GROUP_NOT_ACTIVE_TYPE,COLLABORATION_SERVICE_ID);

						if(gems_communication_write_data(ph_c_connection_get_fd(aConnection),err) != err->length)
						{
							logerror = g_strdup_printf("gems_ph_callback_newconnection: error sending ERROR_GROUP_NOT_ACTIVE to %s/%u",
								ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
							cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
							g_free(logerror);
						}

						gems_clear_message(err);
				
						ph_c_connection_delete(aConnection);
						return;
					}
				}
			}
			
			// Otherwise already connected, send error
			logmsg = g_strdup_printf("gems_ph_callback_newconnection: device (%s/%u) is already connected",
				ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
			
			message = gems_create_error_message(ERROR_ALREADY_CONNECTED_TYPE,JAMMO_SERVICE_ID);
		
			switch(gems_communication_write_data(ph_c_connection_get_fd(aConnection),message))
			{
				case -1:
					logerror = g_strdup_printf("gems_ph_callback_newconnection: Cannot write error ERROR_ALREADY_CONNECTED to %s/%u",
						ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
					cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
					g_free(logerror);
					break;
				case 0:
					logerror = g_strdup_printf("gems_ph_callback_newconnection: Wrote nothing to %s/%u",
						ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
					cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
					g_free(logerror);
					break;
				default:
					logmsg = g_strdup_printf("gems_ph_callback_newconnection: Wrote ERROR_ALREADY_CONNECTED message data to %s/%u", 
						ph_c_connection_get_remote_address(aConnection), ph_c_connection_get_device_checksum(aConnection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
					break;
			}
			gems_clear_message(message);
			ph_c_connection_delete(aConnection); // Disconnects and deletes
		}
	}
	else perror("NULL reference!");
	
	//ph_c_connection_delete(aConnection);
}

/**
 * gems_process_connections:
 *
 * Process the initialized connections created by other devices. Check if there
 * is data awaiting and read it. Pass connections to "JamMo" service which have
 * sent correct data, process error messages and disconnect connections which
 * have invalid data. A timeout function executed periodically by Glib main
 * loop.
 *
 * Returns: Integer value 0 (FALSE) or 1 (TRUE), 0 when errors or the function
 * is required to be removed from Glib main loop.
 */
gint gems_process_connections()
{	
	gems_components* data = gems_get_data();
	
	gboolean cleanup_required = FALSE;
	if(data == NULL) return FALSE;
	if(gems_get_peerhood() == NULL) return FALSE; // No PeerHood - no networking

	gems_connection* element = NULL;
	GList* iterator = NULL;
	GList* tempiter = NULL;
	gchar* logmsg = NULL;
	gchar* logerror = NULL;
	
	/* Process initialized connections */
	iterator = g_list_first(data->initialized_connections);
	
	while(iterator)
	{
		element = (gems_connection*)iterator->data;
		
		if(!element) continue;
		
		// Connected and there is data to read
		if((ph_c_connection_is_connected(element->connection) == TRUE) && 
			(ph_c_connection_has_data(element->connection) == TRUE))
		{
			gshort type = -1;
			
			// read service id
			gint length = gems_communication_read_data(ph_c_connection_get_fd(element->connection),&type,sizeof(type));
		
			// Connection lost
			if(length == 0 || length == -1)
			{
				ph_c_connection_disconnect(element->connection);
				cleanup_required = TRUE;
				logmsg = g_strdup_printf("gems_process_connections: Disconnected/error, received %d bytes from %s/%u.",
					length, ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
			}
			// Read size does not match
			else if(length != sizeof(type))
			{
				logerror = g_strdup_printf("gems_process_connections: Invalid size (%d).", length);
				// TODO react?
			}
			// New devices will have to verify version first from JamMo service - errors are also handled in JamMo service
			else if(ntohs(type) == JAMMO_SERVICE_ID || ntohs(type) == ERROR_MSG)
			{
					if(ntohs(type) == JAMMO_SERVICE_ID)	logmsg = g_strdup_printf("gems_process_connections: got JAMMO_SERVICE_ID from %s/%u", 
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
					else logmsg = g_strdup_printf("gems_process_connections: got ERROR_MSG from from %s/%u", 
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
					
					if(data->service_jammo->enabled == TRUE)
					{	
						// Clear the buffer (should be first in the buffer)
						gems_connection_clear_buffer(element);
					
						// Add 16bit int to buffer
						gems_connection_add_16(element,ntohs(type));
					
						tempiter = iterator; // temporary containing wanted data
						iterator = g_list_next(iterator);
					
						data->initialized_connections = g_list_remove_link(data->initialized_connections,tempiter);
						gems_service_jammo_process_new((gems_connection*)tempiter->data);
						g_list_free_1(tempiter);
					
						if(logmsg != NULL)
						{
							cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
							logmsg = NULL;
						}
						
						continue; // next round
					}
					else
					{
						gems_message* err = gems_create_error_message(ERROR_SERVICE_NOT_RUNNING_TYPE, JAMMO_SERVICE_ID);
						
						if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
							logerror = g_strdup_printf("gems_process_connections: cannot write ERROR_SERVICE_NOT_RUNNING to %s/%u", 
								ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
							
						gems_clear_message(err);
					
						ph_c_connection_disconnect(element->connection);
						cleanup_required = TRUE;
					}
			}
			else
			{
					logmsg = g_strdup_printf("Invalid service id \"%d\" from %s/%u", type, ph_c_connection_get_remote_address(element->connection), 
						ph_c_connection_get_device_checksum(element->connection));
					
					// Send error
					gems_message* err = gems_create_error_message(ERROR_INVALID_SERVICE_ID_TYPE, JAMMO_SERVICE_ID);
	
					if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
						logerror = g_strdup_printf("gems_process_connections: cannot write ERROR_INVALID_SERVICE_ID to %s/%u", 
							ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));

					gems_clear_message(err);
					
					ph_c_connection_disconnect(element->connection);
					cleanup_required = TRUE;
			}
		}
		if(logmsg != NULL)
		{
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
			logmsg = NULL;
		}
		
		if(logerror != NULL)
		{
			cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
			g_free(logerror);
			logerror = NULL;
		}
		
		// next in line
		iterator = g_list_next(iterator);
	}
	// Do cleanup of disconnected connections
	if(cleanup_required == TRUE) data->initialized_connections = gems_cleanup_connections_from_list(data->initialized_connections);

#ifdef DEBUG
	gems_connection_debugging(data);
#endif
	
	return TRUE;
	//logmsg = g_strdup_printf("Done processing connections");
}

/**
 * gems_connection_debugging:
 *
 * For testing, shows contents of connection lists if enter is pressed.
 *
 * Returns: void
 *
 * Deprecated: Only for debugging.
 */
void gems_connection_debugging()
{
	gems_components* data = gems_get_data();
	
	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
	gchar buffer[11];
	gint i = 0;
  fd_set set;
  FD_ZERO(&set);
  FD_SET(0, &set);

  if(select(0 + 1, &set, NULL, NULL, &timeout) > 0)
  {
  	gchar temp = getc(stdin);
  	switch(temp)
  	{
  		case 'c':
				printf("\n======= INITIALIZED ======\n");
				print_list(data->initialized_connections);
				printf("\n======= JAMMO CONN =======\n");
				print_list(data->service_jammo->connections);
				printf("\n======= CONNECTING =======\n");
				print_list(data->communication->connecting);
				printf("\n======= CONNECTIONS ======\n");
				print_list(data->communication->connections);
				printf("\n======= REJECTED =========\n");
				print_list(data->communication->rejected);
				getc(stdin); // read '\n'
				break;
			case 'g':
				printf("\n======== OWN GROUP INFO ===\n");
				print_group(data->service_group->group_info);
				printf("\n======== OFFERED GROUPS ===\n");
				g_list_foreach(data->service_group->other_groups,(GFunc)print_group,NULL);
				getc(stdin); // read '\n'
				break;
			case 'p': // play - lock
				gems_group_lock_group(TRUE);
				getc(stdin); // read '\n'
				break;
			case 'q': // stop play - unlock
				gems_group_lock_group(FALSE);
				getc(stdin); // read '\n'
				break;
			case 'i': // init group
				if(gems_group_init_group(GROUP_TYPE_PAIR,2,444)) printf("group initialized\n\nCurrent group:\n");
				print_group(data->service_group->group_info);
				getc(stdin); // read '\n'
				break;
			case 'j': // join group
				i = 0;
				memset(&buffer,'\0',11);
				for(temp = getc(stdin); g_ascii_isalnum(temp); temp = getc(stdin))
				{
					if(i < 10)
					{
						buffer[i] = temp;
						i++;
					}
				}
				printf("attempting to join group %u (%s)\n",(guint32)strtoul(buffer,NULL,10),buffer);
				if((i = gems_group_join_to_group((guint32)strtoul(buffer,NULL,10))) == REQUEST_SENT) printf("join request sent\n");
				else printf("cannot send join request, return value = %d\n",i);
				break;
			case 'l': // leave group
				printf("attempting to leave group %u\n",data->service_group->group_info->id);
				if((i = gems_group_leave_from_group()) == REQUEST_SENT) printf("leave request sent\n");
				else printf("cannot send leave request, return value = %d\n",i);
				getc(stdin); // read '\n'
				break;
			case 'r':
				gems_reset_group_info();
				getc(stdin); // read '\n'
				break;
			case 'o':
				gems_list_profile_parameters();
				getc(stdin);
				break;
			case '\n':
				break;
			case 'a' :
				// theme, variation
				collaboration_start_pair_game(1, 1, data);
				break;
			case 'f':
				gems_debugging_show_jammos();
				break;
			case 'h':
			default:
				printf("Commands:\nc\tlist connections\ng\tlist offered groups\n");
				printf("i\tinit group\nj\tjoin group (e.g j123 ->join group id=123\nl\tleave current group\n");
				printf("p\tstart play (lock)\nq\tstop play (unlock)\nr\treset group\no\tshow own profile\n");
				printf("a\tcreate pair game\nf\tlist connected JamMos\n");
				break;
  	}
  }
}


/**
 * gems_set_callback_add_loop:
 * @func_ptr: New function pointer to set
 *
 * Set #chum_callback_add_loop_to_slot callback function.
 *
 * Returns: void
 */
void gems_set_callback_add_loop(void (*func_ptr)(GObject * game, guint user_id, guint id, guint slot) ) {
	chum_callback_add_loop_to_slot=func_ptr;
}

/**
 * gems_set_callback_remove_loop:
 * @func_ptr: New function pointer to set
 *
 * Set #chum_callback_remove_loop_to_slot callback function.
 *
 * Returns: void
 */
void gems_set_callback_remove_loop(void (*func_ptr)(GObject * game, guint user_id, guint slot) ) {
	chum_callback_remove_loop_from_slot=func_ptr;
}

/**
 * gems_set_callback_loop_sync:
 * @func_ptr: New function pointer to set
 *
 * Set #chum_callback_loop_sync callback function.
 *
 * Returns: void
 */
void gems_set_callback_loop_sync(void (*func_ptr)(GObject * game, guint user_id, GList * list) ) {
	chum_callback_loop_sync=func_ptr;
}

/**
 * gems_set_callback_midi_list:
 * @func_ptr: New function pointer to set
 *
 * Set #chum_callback_midi_list callback function.
 *
 * Returns: void
 */
void gems_set_callback_midi_list(void (*func_ptr)(GObject * game, guint user_id, gint16 instrument_id, GemsEventListOperation operation, GList * list) ) {
	chum_callback_midi_list=func_ptr;
}

/**
 * gems_set_callback_slider_event_list:
 * @func_ptr: New function pointer to set
 *
 * Set #chum_callback_slider_event_list callback function.
 *
 * Returns: void
 */
void gems_set_callback_slider_event_list(void (*func_ptr)(GObject * game, guint user_id, gint16 instrument_id, GemsEventListOperation operation, GList * list) ) {
	chum_callback_slider_event_list=func_ptr;
}

/**
 * gems_set_callback_group_control:
 * @func_ptr: New function pointer to set
 *
 * Set #chum_callback_group_control callback function.
 *
 * Returns: void
 */
void gems_set_callback_group_control(void (*func_ptr)(gint type) ) {
	chum_callback_group_control=func_ptr;
}

/**
 * gems_set_callback_game:
 * @func_ptr: New function pointer to set
 *
 * Set #chum_callback_game callback function.
 *
 * Returns: void
 */
void gems_set_callback_game(void (*func_ptr)(GObject * game, gint type, const gchar* song_info) ) {
	chum_callback_game=func_ptr;
}

/**
 * gems_set_callback_game_starter:
 * @func_ptr: New function pointer to set
 *
 * Set #chum_callback_game_starter callback function.
 *
 * Returns: void
 */
void gems_set_callback_game_starter(void (*func_ptr)(gpointer data, gint16 track_id) ) {
	chum_callback_game_starter=func_ptr;
}

/**
 * gems_debugging_show_jammos:
 *
 * Debugging. Show all connections, print them into stdout.
 *
 * Returns: void
 *
 * Deprecated: Debugging only.
 */
void gems_debugging_show_jammos()
{
	// get COPY of a list
	GList* conns = gems_list_jammo_connections();
	if(conns)
	{
		printf("======= OTHER JAMMOS =====\n");
		GList* iter = NULL;
		gems_peer_profile* prof = NULL;
		for(iter = g_list_first(conns); iter; iter = g_list_next(iter))
		{
			prof = gems_get_profile((gems_connection*)iter->data); // Get profile
			
			// Access profile data
			if(prof) printf("Profile: id %u, username \"%s\"\n",gems_profile_manager_get_userid(prof), gems_profile_manager_get_username(prof));
			else printf("NULL profile\n");
		}
		g_list_free(conns); // free the LIST do not touch the data
		//SendFileToTeacher(FILE_GENERAL, "/opt/jammo/4-players.json");
		//GetFileFromTeacher(FILE_GENERAL, "jammo.config");
	}
	else printf("No connected JamMos\n");
}

/**
 * gems_init_peerhood_callback:
 *
 * Initialize PeerHood callback, set callback function pointers and create new
 * callback.
 *
 * Returns: void
 */
void gems_init_peerhood_callback()
{
	void (*notify)(short, const char*,void*) = NULL;
	notify = &gems_ph_callback_notify;

	void (*newconnection)(const unsigned short, MAbstractConnection*, int,void*) = NULL;
	newconnection = &gems_ph_callback_newconnection;
	
	gems_get_data()->ph_cb = ph_c_create_callback(notify, newconnection,(void*)gems_get_data(),(void*)gems_get_data());
	
	gchar* logmsg = g_strdup_printf("gems_networking_init_peerhood_callback: new callback created.");
	cem_add_to_log(logmsg,J_LOG_DEBUG);
	g_free(logmsg);
}

/**
 * gems_enable_network_timeout_functions:
 *
 * Enable network timeout functions. All timeout functions related to networked
 * components are inside this function. This will be called if connection to 
 * PeerHood is lost and it is found again.
 *
 * Return: void
 */
void gems_enable_network_timeout_functions()
{
	gems_get_data()->timeout_functions[0] = g_timeout_add_full(G_PRIORITY_HIGH,200,(GSourceFunc)gems_process_connections, NULL, NULL);
	gems_get_data()->timeout_functions[1] = g_timeout_add_full(G_PRIORITY_HIGH,200,(GSourceFunc)gems_service_jammo_process_connections, NULL, NULL);
	gems_get_data()->timeout_functions[2] = g_timeout_add_full(G_PRIORITY_HIGH,200,(GSourceFunc)gems_communication_search_jammos, NULL, NULL);
	gems_get_data()->timeout_functions[3] = g_timeout_add_full(G_PRIORITY_HIGH,300,(GSourceFunc)gems_communication_retrieve_profiles, NULL, NULL);
	gems_get_data()->timeout_functions[4] = g_timeout_add_full(G_PRIORITY_HIGH,200,(GSourceFunc)gems_communication_process_connecting, NULL, NULL);
	gems_get_data()->timeout_functions[5] = g_timeout_add_full(G_PRIORITY_HIGH,100,(GSourceFunc)gems_communication_process_connected, NULL, NULL);
	gems_get_data()->timeout_functions[6] = g_timeout_add_full(G_PRIORITY_HIGH,500,(GSourceFunc)gems_communication_process_rejected, NULL, NULL);
	gems_get_data()->timeout_functions[7] = g_timeout_add_full(G_PRIORITY_HIGH,200,(GSourceFunc)gems_communication_request_groups, NULL, NULL);
	gems_get_data()->timeout_functions[8] = g_timeout_add_full(G_PRIORITY_HIGH,350,(GSourceFunc)gems_communication_sanitize_grouplist, NULL, NULL);
	//gems_get_data()->timeout_functions[9] = g_timeout_add_full(G_PRIORITY_HIGH,1000,(GSourceFunc)gems_communication_search_teacher, NULL, NULL);
}

/**
 * gems_register_services:
 *
 * Register all services into PeerHood. Set them enabled if registration is
 * successful.
 *
 * Return: void
 */
void gems_register_services()
{
	gems_components* components = gems_get_data();
	gchar* logmsg = NULL;
	gint serviceport = 0;
	
	// Create JamMo service
	if((serviceport = ph_c_register_service(components->ph,JAMMO_SERVICE_NAME,"application:jammo,method:versioncheck")) != 0)
	{
		components->service_jammo->enabled = TRUE; //enabled
		components->service_jammo->port = serviceport;
		logmsg = g_strdup_printf("gems_networking_register_services: registered %s service.",JAMMO_SERVICE_NAME);
	}
	else
	{
		components->service_jammo->enabled = FALSE;
		logmsg = g_strdup_printf("gems_networking_register_services: cannot register %s service.",JAMMO_SERVICE_NAME);
	}
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
	
	// Create JamMo service
	if((serviceport = ph_c_register_service(components->ph,PROFILE_SERVICE_NAME,"application:jammo,method:getprofile")) != 0)
	{
		components->service_profile->enabled = TRUE; //enabled
		components->service_profile->port = serviceport;
		logmsg = g_strdup_printf("gems_networking_register_services: registered %s service.",PROFILE_SERVICE_NAME);
	}
	else
	{
		components->service_profile->enabled = FALSE;
		logmsg = g_strdup_printf("gems_networking_register_services: cannot register %s service.",PROFILE_SERVICE_NAME);
	}
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
	
	// Create JamMo service
	if((serviceport = ph_c_register_service(components->ph,GROUP_SERVICE_NAME,"application:jammo,method:groupmanager")) != 0)
	{
		components->service_group->enabled = TRUE; //enabled
		components->service_group->port = serviceport;
		logmsg = g_strdup_printf("gems_networking_register_services: registered %s service.",GROUP_SERVICE_NAME);
	}
	else
	{
		components->service_group->enabled = FALSE;
		logmsg = g_strdup_printf("gems_networking_register_services: cannot register %s service.",GROUP_SERVICE_NAME);
	}
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);

	// Create Collaboration service
	if((serviceport = ph_c_register_service(components->ph,COLLABORATION_SERVICE_NAME,"application:jammo,method:collaboration")) != 0)
	{
		components->service_collaboration->enabled = TRUE; //enabled
		components->service_collaboration->port = serviceport;
		logmsg = g_strdup_printf("gems_networking_register_services: registered %s service.",COLLABORATION_SERVICE_NAME);
	}
	else
	{
		components->service_collaboration->enabled = FALSE;
		logmsg = g_strdup_printf("gems_networking_register_services: cannot register %s service.",COLLABORATION_SERVICE_NAME);
	}
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
	
	// Create Control service
	if((serviceport = ph_c_register_service(components->ph,CONTROL_SERVICE_NAME,"application:jammo,method:control")) != 0)
	{
		components->service_control->enabled = TRUE; //enabled
		components->service_control->port = serviceport;
		logmsg = g_strdup_printf("gems_networking_register_services: registered %s service.",CONTROL_SERVICE_NAME);
	}
	else
	{
		components->service_control->enabled = FALSE;
		logmsg = g_strdup_printf("gems_networking_register_services: cannot register %s service.",CONTROL_SERVICE_NAME);
	}
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);

	// Create Songbank service
	if((serviceport = ph_c_register_service(components->ph,SONGBANK_SERVICE_NAME,"application:jammo,method:songbank")) != 0)
	{
		components->service_songbank->enabled = TRUE; //enabled
		components->service_songbank->port = serviceport;
		logmsg = g_strdup_printf("gems_networking_register_services: registered %s service.",SONGBANK_SERVICE_NAME);
	}
	else
	{
		components->service_songbank->enabled = FALSE;
		logmsg = g_strdup_printf("gems_networking_register_services: cannot register %s service.",SONGBANK_SERVICE_NAME);
	}
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
}
