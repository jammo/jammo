/*
 * gems_profile_manager.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */

#include "gems_profile_manager.h"
#include "gems_security.h"
#include "gems.h"
#include "../cem/cem.h"

#include "gems_teacher_server_utils.h"
#include "gems_message_functions.h"
#include "gems.h"

/**
 * gems_profile_manager_login:
 * @userId: uid 
 * @username: username
 * @passworddata: the password
 *
 * Login
 *
 * Returns: FALSE
 *
 * Deprecated: DO NOT USE!!
 */
gboolean gems_profile_manager_login(guint32 userId, gchar* username, gchar* passworddata)
{
	return profilemanager_login(userId);
}

/**
 * gems_profile_manager_authenticate_default_user:
 * @passworddata: user provided plain password
 *
 * Authenticate the default user with "defaultuser" username. Used when "Guest"
 * account is selected in the user selection login or when logging in with the
 * plain pen gesture. Calls the gems_profile_manager_authenticate_user() with
 * "defaultuser" as the username.
 *
 * Returns: Result of the login as value of #GemsLoginAction types: 
 * %LOGIN_OK, %LOGIN_INVALID_PARAMS, %LOGIN_NO_PROFILE, %LOGIN_INVALID_PROFILE,
 * %LOGIN_PROFILE_READ_ERROR, %LOGIN_CANNOT_INIT_PASSWD, 
 * %LOGIN_INVALID_PASSWORD, %LOGIN_CANNOT_INIT_SECURITY, 
 * %LOGIN_PROFILE_HASH_FAIL or %LOGIN_PROFILE_CORRUPT.
 */
GemsLoginAction gems_profile_manager_authenticate_default_user(const gchar* passworddata)
{
	profilemanager_storage_clear_profile();
	gboolean rval = gems_profile_manager_authenticate_user("defaultuser",passworddata);

	return rval;
}

/**
 * gems_profile_manager_authenticate_user:
 * @username: user to authenticate
 * @passworddata: user provided plain password
 *
 * Authenticate the user with the provided @username. Loads the profile with
 * the provided @username and set to authenticated if password matches to the
 * saved one (hashes are compared).
 *
 * Returns: Result of the login as value of #GemsLoginAction types: 
 * %LOGIN_OK, %LOGIN_INVALID_PARAMS, %LOGIN_NO_PROFILE, %LOGIN_INVALID_PROFILE,
 * %LOGIN_PROFILE_READ_ERROR, %LOGIN_CANNOT_INIT_PASSWD, 
 * %LOGIN_INVALID_PASSWORD, %LOGIN_CANNOT_INIT_SECURITY, 
 * %LOGIN_PROFILE_HASH_FAIL or %LOGIN_PROFILE_CORRUPT.
 */
GemsLoginAction gems_profile_manager_authenticate_user(const gchar* username, const gchar* passworddata)
{
	GemsLoginAction rval = LOGIN_OK;
	guint passlength = 0;
	guint proflength = 0;
	guint decprofhashlength = 0;
	guchar* passwd = NULL;
	guchar* pwhash = NULL;
	guchar* decrypted = NULL;
	guchar* decprofhash = NULL;
	
	if(!username)
	{
		cem_add_to_log("Authentication failure, username not given", J_LOG_ERROR);
		return LOGIN_INVALID_PARAMS;
	}
	
	// Try to load profile
	switch(profilemanager_storage_load_profile(username))
	{
		case PROFILE_LOADED:
			cem_add_to_log("Profile loaded.", J_LOG_DEBUG);
			break;
		case PROFILE_READ_FAILURE:
			cem_add_to_log("Authentication failure, profile cannot be read.", J_LOG_ERROR);
			return LOGIN_PROFILE_READ_ERROR;
		case PROFILE_NOT_FOUND:
			cem_add_to_log("Authentication failure, profile not found.", J_LOG_ERROR);
			return LOGIN_NO_PROFILE;
		case PROFILE_VERSION_MISMATCH:
			cem_add_to_log("Authentication failure, profile version is not compatible.", J_LOG_ERROR);
			return LOGIN_INVALID_PROFILE;
		default:
			cem_add_to_log("Authentication - unknown state when loading profile ?",J_LOG_ERROR);
			break;
	}

	// Create password from data
	if((passwd = gems_security_create_password(passworddata,profilemanager_storage_get_password_salt())) == NULL)
	{
		gchar* logmsg = g_strdup_printf("Authentication failure, given %s is NULL.", passworddata ? "password" : "salt");
		cem_add_to_log(logmsg, J_LOG_ERROR);
		return LOGIN_CANNOT_INIT_PASSWD;
	}
	
	// Initialize security
	if(gems_security_init_security(passwd,profilemanager_storage_get_password_salt()))
	{
		cem_add_to_log("Security contexts initialized", J_LOG_DEBUG);
		
		// hash password
		passlength = PASSLEN;
		pwhash = gems_security_calculate_hash(passwd,&passlength);
		if(passlength != SHA_HASH_LENGTH) cem_add_to_log("Authentication might fail, password hash length is not 256 bits (32 bytes)!", J_LOG_ERROR);

		// verify passwords
		if(gems_security_verify_hash(pwhash,profilemanager_storage_get_password_hash(),SHA_HASH_LENGTH))
		{
			cem_add_to_log("Password ok", J_LOG_DEBUG);
			
			// Decrypt profile
			proflength = profilemanager_storage_get_encrypted_profile_size();
	
			decrypted = gems_security_decrypt_data(profilemanager_storage_get_encrypted_profile(), &proflength);
	
			// Calculate hash from profile
			decprofhashlength = proflength;
			decprofhash = gems_security_calculate_hash(decrypted,&decprofhashlength);
			if(decprofhashlength != SHA_HASH_LENGTH) cem_add_to_log("Authentication might fail, profile hash length is not 256 bits (32 bytes)!", J_LOG_ERROR);
			
			// verify profile hashes
			if(gems_security_verify_hash(decprofhash,profilemanager_storage_get_profile_hash(),SHA_HASH_LENGTH))
			{
				cem_add_to_log("Profile verified!", J_LOG_DEBUG);
		
				// Set to authenticated
				profilemanager_set_authenticated();
		
				// parse profile data to manager
				if(!profilemanager_storage_deserialize_profile(decrypted))
				{
					cem_add_to_log("Unable to set profile data, data is invalid.",J_LOG_ERROR);
					rval = LOGIN_PROFILE_CORRUPT;
				}
				else cem_add_to_log("Authentication success!", J_LOG_DEBUG);
			}
			else
			{
				cem_add_to_log("Authentication failure: decrypted profile content differs from stored!", J_LOG_ERROR);
				rval = LOGIN_PROFILE_HASH_FAIL;
			}
		}
		else
		{
			cem_add_to_log("Authentication failure: invalid password", J_LOG_ERROR);
			rval = LOGIN_INVALID_PASSWORD;
		} 
	}
	else
	{
		cem_add_to_log("Authentication methods not available, couldn't initialize security.", J_LOG_ERROR);
		rval = LOGIN_CANNOT_INIT_SECURITY;
	}
	
	if(rval != LOGIN_OK) gems_security_clear_security();
	if(rval == LOGIN_PROFILE_CORRUPT) profilemanager_storage_clear_profile(); // Clear profile - sets to not authenticated
	
	if(passwd)
	{
		gems_guaranteed_memset(passwd,0,PASSLEN);
		g_free(passwd);
	}
	if(pwhash) g_free(pwhash);
	if(decrypted) g_free(decrypted);
	if(decprofhash) g_free(decprofhash);
	
	return rval;
}

/**
 * gems_profile_manager_set_user:
 * @username: Username of the new user
 *
 * Change the user who has logged in. If the user is logged in logout will be
 * required for that user. Otherwise loads the profile of the new user and
 * makes required changes. If the profile is not found it will be requested
 * from the teacher if such connection exists. This is a timeout function added
 * to Glib main loop when the connection to teacher exists and we're requesting
 * the profile from teacher. When the profile is changed the function is
 * removed from the main loop.
 *
 * Returns: Result of the action as value of #GemsChangeUserAction types: 
 * %CHANGE_USER_NO_USERNAME, %CHANGE_USER_LOGOUT_REQUIRED,
 * %CHANGE_USER_WAITING_PROFILE, %CHANGE_USER_NO_PROFILE or %CHANGE_USER_OK.
 */
GemsChangeUserAction gems_profile_manager_set_user(const gchar* username)
{
	// if authenticated, cannot change user before logout, logout clears also the loaded data and sets load check to FALSE
	if(profilemanager_is_authenticated()) return CHANGE_USER_LOGOUT_REQUIRED;
	else 
	{
		// Was the request name set
		if(gems_get_login_username() == NULL)
		{
			if(username) gems_set_login_username(username,TRUE); // creates a duplicate so given can be free'd
			else return CHANGE_USER_NO_USERNAME;
		}
		
		// Profile does not exist
		switch(profilemanager_storage_load_profile(gems_get_login_username()))
		{
			case PROFILE_NOT_FOUND:
			{
				if(gems_get_data()->teacher_profile_request_timer)
				{
					// Not enough time spent
					if(g_timer_elapsed(gems_get_data()->teacher_profile_request_timer,NULL) < TEACHER_PROFILE_REQUEST_LIMIT) return CHANGE_USER_WAITING_PROFILE;
				}
				// create new timer
				else gems_get_data()->teacher_profile_request_timer = g_timer_new(); // when first run this should be reached
			
				// New requests can be made & connection to teacher exists and request was sent
				if((gems_get_teacher_profile_request_count() < TEACHER_PROFILE_REQUEST_MAX_COUNT) && 
					(gems_communication_do_profile_request_to_teacher(gems_get_login_username())))
				{
					// set waiting for teacher - no logins should be possible!
					gems_set_waiting_profile_from_teacher();
					gems_increase_teacher_profile_request_counter();
				
					// restart timer
					g_timer_start(gems_get_data()->teacher_profile_request_timer);
				
					// Add timed function if not set
					if(gems_get_data()->profile_request_timeout_sid == 0)
					{
						gems_get_data()->profile_request_timeout_sid = 
							g_timeout_add_full(G_PRIORITY_HIGH,100,(GSourceFunc)gems_profile_manager_set_user, NULL, gems_unset_teacher_profile_timeout_sid);
					}
					return CHANGE_USER_WAITING_PROFILE; // Equals TRUE, the timed function remains
				}
			
				// no teacher or cannot send request or limit reached -> no profile for user
				else
				{
					// Destroy timer and set to NULL
					if(gems_get_data()->teacher_profile_request_timer)
					{
						g_timer_destroy(gems_get_data()->teacher_profile_request_timer);
						gems_get_data()->teacher_profile_request_timer = NULL;
					}
				
					// Make sure that source is removed
					if(gems_get_data()->profile_request_timeout_sid != 0) g_source_remove(gems_get_data()->profile_request_timeout_sid);
					gems_reset_teacher_profile_request_counter();
				
					// No profile for this username, clear it
					gems_clear_login_username();
				
					return CHANGE_USER_NO_PROFILE;
				}
			}
			// profile exists and is loaded
			case PROFILE_LOADED:
			{
				// Allow to continue
				gems_unset_waiting_profile_from_teacher();
			
				// Destroy timer and set to NULL
				if(gems_get_data()->teacher_profile_request_timer)
				{
					g_timer_destroy(gems_get_data()->teacher_profile_request_timer);
					gems_get_data()->teacher_profile_request_timer = NULL;
				}
			
				// Equals to FALSE (0) so the timeout function should be removed
				return CHANGE_USER_OK;
			}
			// TODO handle this - should not be possible
			case PROFILE_READ_FAILURE:
				return CHANGE_USER_NO_PROFILE;
			// TODO handle this - should not be possible
			case PROFILE_VERSION_MISMATCH:
				return CHANGE_USER_NO_PROFILE;
			default:
				return FALSE;
		}
	}
}

/**
 * gems_profile_manager_change_password:
 * @passworddata: New plain password
 *
 * Change the password of the logged in user. Re-encrypts the current profile
 * with the new password and saves it to disk. If the profile type is 
 * %PROF_TYPE_REMOTE it will be sent to teacher if there is such connection.
 *
 * Returns: TRUE when password is changed, FALSE if password is invalid or the
 * user is not logged in.
 */
gboolean gems_profile_manager_change_password(const gchar* passworddata)
{
	gboolean rval = TRUE;
	
	if(!passworddata) return FALSE;
	
	// TODO allow only one change if new password is not sent to teacher!!
	// Do not allow password change if not authenticated
	if(profilemanager_is_authenticated())
	{
		// Create salt and password
		guchar* salt = gems_security_create_password_salt();
		guchar* passwd = gems_security_create_password(passworddata,salt);
		
		// Hash password
		guint hashlen = PASSLEN;
		guchar* passhash = gems_security_calculate_hash(passwd,&hashlen);
		if(hashlen != SHA_HASH_LENGTH) cem_add_to_log("gems_profile_manager_change_password: Next authentication might fail, password hash length is not 256 bits (32 bytes)!", J_LOG_ERROR);
		
		guint newpasshashlen = strlen(passworddata);
		guint newpasslen = strlen(passworddata);
		
		// Hash the plain password WITHOUT salt
		guchar* newpass_hash = gems_security_calculate_hash((guchar*)passworddata,&newpasshashlen);
		if(newpasshashlen != SHA_HASH_LENGTH) cem_add_to_log("gems_profile_manager_change_password: Profile password might not be set correctly to teacher server, password hash length is not 256 bits (32 bytes)!", J_LOG_ERROR);
		
		guchar* pwdata = NULL;
		guint pwdatalen = newpasslen;
		
		// If less than one AES block reserve the size of AES_BLOCK_SIZE
		if(newpasslen < AES_BLOCK_SIZE)	pwdata = g_new0(guchar,sizeof(guchar)*AES_BLOCK_SIZE);
		else pwdata = g_new0(guchar,sizeof(guchar)*newpasslen);
		
		g_memmove(pwdata,passworddata,newpasslen);
		
		// Pad
		if(newpasslen < AES_BLOCK_SIZE)
		{
			pwdatalen = AES_BLOCK_SIZE;
			memset(&pwdata[newpasslen],'_',AES_BLOCK_SIZE-newpasslen);
		}
		
		// Encrypt pass with current context
		guchar* newpass_enc = gems_security_encrypt_data(pwdata,&pwdatalen);
		guint newpass_enclen = pwdatalen;

		// decrypt pass
		guchar* newpass_dec = gems_security_decrypt_data(newpass_enc,&newpass_enclen);
		newpasshashlen = newpass_enclen;
		
		// Get the real length without padding bytes
		guchar* newpass_dec_real = g_new0(guchar,sizeof(guchar)*newpasslen);
		g_memmove(newpass_dec_real,newpass_dec,newpasslen);
		guint newpass_real_len = newpasslen;
		
		// hash "stripped" decrypted and compare hashes
		guchar* newpass_dechash = gems_security_calculate_hash(newpass_dec_real,&newpass_real_len);
		if(newpass_real_len != SHA_HASH_LENGTH) cem_add_to_log("gems_profile_manager_change_password: Profile password might not be set correctly to teacher server, decrypted password hash length is not 256 bits (32 bytes)!", J_LOG_ERROR);
		
		if(!gems_security_verify_hash(newpass_dechash,newpass_hash,SHA_HASH_LENGTH))
		{
			cem_add_to_log("gems_profile_manager_change_password: Password change encryption/decryption error: Hashes of given password and decrypted password do not match. Probable password mismatch after sending password to teacher, aborting.",J_LOG_ERROR);
			rval = FALSE;
		}
		else
		{
			// Set new security contexts
			gems_security_change_password(passwd,salt);
		
			// Set new salt and password
			if(profilemanager_storage_set_password_salt(salt) && profilemanager_storage_set_password_hash(passhash))
			{
				cem_add_to_log("gems_profile_manager_change_password: Password change success.", J_LOG_DEBUG);
		
				// Serialize profile
				guchar* profile = (guchar*)profilemanager_storage_serialize_profile();
				guint profilelength = strlen((gchar*)profile)+1;
			
				// Hash profile
				guint hashlength = profilelength;
				guchar* profilehash = gems_security_calculate_hash(profile,&hashlength);
				if(hashlength != SHA_HASH_LENGTH) cem_add_to_log("gems_profile_manager_change_password: Next authentication might fail, profile hash length is not 256 bits (32 bytes)!", J_LOG_ERROR);
			
				// Encrypt profile
				guchar* encrypted = gems_security_encrypt_data(profile,&profilelength);
				guint enc_length = profilelength;
			
				// Decrypt profile
				guchar* decrypted = gems_security_decrypt_data(encrypted,&enc_length);
				hashlength = enc_length;
				guchar* decryptedhash = gems_security_calculate_hash(decrypted,&hashlength);
				if(hashlength != SHA_HASH_LENGTH) cem_add_to_log("gems_profile_manager_change_password: Next authentication might fail, decrypted profile hash length is not 256 bits (32 bytes)!", J_LOG_ERROR);
		
				// Verify that the encryption and decryption works
				if(gems_security_verify_hash(profilehash,decryptedhash,SHA_HASH_LENGTH))
				{
					// Set profile (encrypted with new password)
					if(profilemanager_storage_set_encrypted_profile(profilehash,encrypted,profilelength))
					{
						// Try to save profile
						if(profilemanager_storage_save_profile())	cem_add_to_log("gems_profile_manager_change_password: New encrypted profile saved.", J_LOG_DEBUG);
						else
						{
							rval = FALSE;
							cem_add_to_log("gems_profile_manager_change_password: New encrypted profile not saved.", J_LOG_ERROR);
						}
					}
				
					if(gems_profile_manager_get_profile_type() == PROF_TYPE_REMOTE)
					{
						// Send passhash + encnewpw to teacher if teacher is present
						if(gems_security_verify_hash(newpass_hash,newpass_dechash,SHA_HASH_LENGTH))
						{
							if(gems_teacher_connection_is_connected())
							{
								// TODO check this earlier and react
								if(newpasslen > G_MAXUINT8) cem_add_to_log("gems_profile_manager_change_password; Given password length is more than max for guint8!.",J_LOG_ERROR);
						
								gems_message *pwcmsg = gems_create_message_password_changed(newpass_hash,newpass_enc,pwdatalen,(guint8)newpasslen);
								if(pwcmsg)
								{
									if(!gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,gems_get_teacher_connection()->connection,pwcmsg))
									{
										cem_add_to_log("gems_profile_manager_change_password: Password change cannot be sent. Invalid write.",J_LOG_ERROR);
										// TODO resend?
									}
									else cem_add_to_log("gems_profile_manager_change_password: Password change sent succesfully!",J_LOG_DEBUG);
									gems_clear_message(pwcmsg);
								}
							}
							// TODO If no teacher, the encnewpw (+ its length) should be saved somewhere for sending it later!
							else cem_add_to_log("gems_profile_manager_change_password: Password change cannot be sent, no teacher present.",J_LOG_DEBUG);
					
						}
						else cem_add_to_log("gems_profile_manager_change_password: Errors while creating password to be sent to teacher, hashes do not match.",J_LOG_ERROR);
					}
				
				}
				else
				{
					rval = FALSE;
					cem_add_to_log("gems_profile_manager_change_password: Errors while encrypting profile with new password!", J_LOG_ERROR);
				}
				if(pwdata)
				{
					gems_guaranteed_memset(pwdata,0,newpasslen < AES_BLOCK_SIZE ? AES_BLOCK_SIZE : newpasslen);
					g_free(pwdata);
				}
				if(newpass_hash) g_free(newpass_hash);
				if(newpass_enc) g_free(newpass_enc);
				if(newpass_dec)
				{
					gems_guaranteed_memset(newpass_dec,0,newpasslen < AES_BLOCK_SIZE ? AES_BLOCK_SIZE : newpasslen);
					g_free(newpass_dec);
				}
				if(newpass_dec_real)
				{
					gems_guaranteed_memset(newpass_dec_real,0,newpasslen);
					g_free(newpass_dec_real);
				}
				if(newpass_dechash) g_free(newpass_dechash);
		
				if(profile != NULL) g_free(profile);
				if(profilehash != NULL) g_free(profilehash);
				if(encrypted != NULL) g_free(encrypted);
				if(decrypted != NULL) g_free(decrypted);
				if(decryptedhash != NULL) g_free(decryptedhash);
			
			}
			else
			{
				rval = FALSE;
				cem_add_to_log("gems_profile_manager_change_password: Password not changed.",J_LOG_ERROR);
			}
		}
		
		if(salt) g_free(salt);
		if(passwd)
		{
			gems_guaranteed_memset(passwd,0,PASSLEN);
			g_free(passwd);
		}
		if(passhash) g_free(passhash);
	}
	// Not authenticated
	else
	{
		rval = FALSE;
		cem_add_to_log("gems_profile_manager_change_password: User not authenticated, password will not be changed!", J_LOG_DEBUG);
	}
	
	return rval;
}

/**
 * gems_profile_manager_create_default_user_profile:
 * @passworddata: User provided plain password
 * @age: Age of the user for whom the profile is created to
 *
 * Create default user profile. Profile is created for user "defaultuser" and
 * encrypted with the provided password, profile is saved to disk. Uses teacher
 * utils for creating the profile.
 *
 * Returns: TRUE when profile was created, FALSE otherwise.
 */
gboolean gems_profile_manager_create_default_user_profile(const gchar* passworddata, guint16 age)
{
	gboolean rval = TRUE;
	GRand* grand = g_rand_new_with_seed(time(NULL));
	
	// Create profile with default informationm, except id (random), password (given) and age (given)
	jammo_profile_container* profile = gems_teacher_server_create_profile(PROF_TYPE_LOCAL,g_rand_int(grand),"defaultuser",passworddata,"none","FirstName","LastName",age,PROF_DEFAULT_AVATARID);

	if(profile)
	{
		if(gems_teacher_server_write_profile(profile) != WRITE_OK)
		{
			cem_add_to_log("Cannot create profile for user \"defaultuser\", profile cannot be written.", J_LOG_ERROR);
			rval = FALSE;
		}
		else cem_add_to_log("Created profile for user \"defaultuser\"", J_LOG_DEBUG);
		gems_teacher_server_clear_profile_container(profile);
	}
	else
	{
		cem_add_to_log("Cannot initialize profile for user \"defaultuser\", profile cannot be created!",J_LOG_ERROR);
		rval = FALSE;
	}
	
	g_free(grand);
	return rval;
}

/**
 * gems_profile_manager_logout:
 * 
 * Logout from the profile manager. If there exists an connection to teacher
 * the profile is sent to teacher. If profile contains changes it will be
 * saved to disk. Security and login username is cleared.
 * 
 * Returns: TRUE when logout was done completely, FALSE otherwise.
 */
gboolean gems_profile_manager_logout()
{	
	/* TODO add checks for each call (for NULL's) */
	 
	// Saving and updating can be done as authenticated
	if(profilemanager_is_authenticated())
	{
		// Send profile to teacher if it is remote profile
		if(gems_profile_manager_get_profile_type() == PROF_TYPE_REMOTE)
		{
			if(gems_communication_send_profile_to_teacher()) cem_add_to_log("Profile updated to teacher server.",J_LOG_DEBUG);
			else cem_add_to_log("Profile was not updated to teacher.", J_LOG_DEBUG);
		}
		
		// Is it necessary to save profile
		if(profilemanager_is_saving_required())
		{
			// Get all profile information to one line
			guchar* profile = (guchar*)profilemanager_storage_serialize_profile();
			guint profilelength = strlen((gchar*)profile)+1;
			guint hashlength = profilelength;
		
			// Calculate hash
			guchar* profilehash = gems_security_calculate_hash(profile,&hashlength);
			if(hashlength != SHA_HASH_LENGTH) cem_add_to_log("Next authentication might fail, profile hash length is not 256 bits (32 bytes)!", J_LOG_ERROR);
		
			// Encrypt profile
			guchar* encrypted = gems_security_encrypt_data(profile,&profilelength);
		
			guint enc_length = profilelength;
		
			// Decrypt the encrypted
			guchar* decrypted = gems_security_decrypt_data(encrypted,&enc_length);
			hashlength = enc_length;
			guchar* decryptedhash = gems_security_calculate_hash(decrypted,&hashlength);
			if(hashlength != SHA_HASH_LENGTH) cem_add_to_log("Next authentication might fail, decrypted profile hash length is not 256 bits (32 bytes)!", J_LOG_ERROR);
		
			// Verify that the profile can be decrypted with same password
			if(gems_security_verify_hash(profilehash,decryptedhash,SHA_HASH_LENGTH))
			{
				// Save the profile to file
				if(profilemanager_storage_set_encrypted_profile(profilehash,encrypted,profilelength))
				{
					if(profilemanager_storage_save_profile())
					{
						cem_add_to_log("Profile saved.", J_LOG_DEBUG);
					}
					else
					{
						cem_add_to_log("Profile not saved.", J_LOG_DEBUG);
					}
				}
			}
			else cem_add_to_log("Problems with encrypting new profile, hashes do not match. Profile not saved",J_LOG_ERROR);
		
			if(profile != NULL) g_free(profile);
			if(profilehash != NULL) g_free(profilehash);
			if(encrypted != NULL) g_free(encrypted);
			if(decrypted != NULL) g_free(decrypted);
			if(decryptedhash != NULL) g_free(decryptedhash);
		}
		else cem_add_to_log("No changes in profile, profile not saved.", J_LOG_DEBUG);
	}
	else cem_add_to_log("Not authenticated, nothing done for logout.", J_LOG_DEBUG);
	
	gems_security_clear_security();
	gems_clear_login_username();
	
	return profilemanager_logout();
}

/* PROFILE MANAGER API*/

/**
 * gems_profile_manager_get_userid:
 * @profile: profile to use, if NULL corresponding own info will be returned
 *
 * Get the user id from the given @profile or own user id if @profile is NULL.
 *
 * Returns: User id as #guint32 or 0 if not logged in and @profile is NULL.
 */
guint32 gems_profile_manager_get_userid(gems_peer_profile* profile)
{
	if(!profile) return profilemanager_get_userid();
	else return profile->id;
}

/**
 * gems_profile_manager_get_username:
 * @profile: profile to use, if NULL corresponding own info will be returned
 *
 * Get the user name from the given @profile or own user name if @profile is
 * NULL.
 *
 * Returns: Username or NULL if not logged in and @profile is NULL.
 */
const gchar* gems_profile_manager_get_username(gems_peer_profile* profile)
{
	if(!profile) return profilemanager_get_username();
	else return profile->username;
}

/**
 * gems_profile_manager_get_firstname:
 *
 * Get firstname from own profile.
 *
 * Returns: Firstname or NULL if not logged in and @profile is NULL.
 */
const gchar* gems_profile_manager_get_firstname()
{
	return profilemanager_get_firstname();
}

/**
 * gems_profile_manager_get_lastname:
 *
 * Get lastname from own profile.
 *
 * Returns: Lastname or NULL if not logged in and @profile is NULL.
 */
const gchar* gems_profile_manager_get_lastname()
{
	return profilemanager_get_lastname();
}

/**
 * gems_profile_manager_get_age:
 * @profile: profile to use, if NULL corresponding own info will be returned
 *
 * Get the user age from the given @profile or own age if @profile is NULL.
 *
 * Returns: Age of the user as #guint16 or 0 if not logged in and @profile is 
 * NULL.
 */
guint16 gems_profile_manager_get_age(gems_peer_profile* profile)
{
	if(!profile) return profilemanager_get_age();
	else return profile->age;
}

/**
 * gems_profile_manager_get_avatar_id:
 * @profile: profile to use, if NULL corresponding own info will be returned
 *
 * Get the user avatar id from the given @profile or own avatar id if @profile
 * is NULL.
 *
 * Returns: Avatar id as #guint32 or 0 if not logged in and @profile is NULL.
 */
guint32 gems_profile_manager_get_avatar_id(gems_peer_profile* profile)
{
	if(!profile) return profilemanager_get_avatarid();
	else return profile->avatarid;
}

/**
 * gems_profile_manager_set_avatar_id:
 * @avatarid: New avatar id
 * 
 * Change avatar, replace old avatar id with the provided new @avatarid.
 *
 * Returns: TRUE when success, FALSE when user is not logged in and @profile is
 * NULL.
 */
gboolean gems_profile_manager_set_avatar_id(guint32 avatarid)
{
	return profilemanager_set_avatarid(avatarid);
}

/**
 * gems_profile_manager_get_points:
 *
 * Get the amount of points user has gained.
 *
 * Returns: Amount of points gained as #guint32 or 0 if user is not logged in.
 */
guint32 gems_profile_manager_get_points()
{
	return profilemanager_get_points();
}

/**
 * gems_profile_manager_add_points:
 * @_value: Points to be added
 * 
 * Add the @_value amount of points for the user.
 *
 * Returns: TRUE when success, FALSE when user is not logged in.
 */
gboolean gems_profile_manager_add_points(guint32 _value)
{
	return profilemanager_add_points(_value);
}

/**
 * gems_profile_manager_remove_points:
 * @_value: Amount of points to remove
 *
 * Removes the @_value amount of points from the profile.
 *
 * Returns: TRUE when success, FALSE when user is not logged in.
 */
gboolean gems_profile_manager_remove_points(guint32 _value)
{
	return profilemanager_remove_points(_value);
}

/**
 * gems_profile_manager_reset_points:
 *
 * Set the points to 0.
 *
 * Returns: void
 */
void gems_profile_manager_reset_points()
{
	profilemanager_reset_points();
}

/**
 * gems_profile_manager_get_profile_of_user:
 * @id: Id of the user used for search
 * 
 * Get the public profile (#gems_peer_profile) of the user with @id. If there
 * is no connection to a user with @id NULL is returned.
 *
 * Returns: the public profile of user with @id or NULL if not found or
 * connection does not exist.
 */
gems_peer_profile* gems_profile_manager_get_profile_of_user(guint32 id)
{
	if(gems_get_data()->communication == NULL) return NULL; // Communication not initialized
	gems_connection* element = gems_communication_get_connection_with_userid(gems_get_data()->communication->connections,id);
	
	if(element == NULL) return NULL;
	else return element->profile; // If not set it is still NULL
}

/**
 * gems_profile_manager_is_authenticated:
 *
 * Check if the user is authenticated.
 *
 * Returns: TRUE when authenticated, FALSE otherwise.
 */
gboolean gems_profile_manager_is_authenticated()
{
	return profilemanager_is_authenticated();
}

/**
 * gems_profile_manager_get_loaded_username:
 *
 * Get the username whose profile is currently loaded. Will return NULL if no
 * profile is loaded.
 *
 * Returns: Username or NULL if no profile is loaded.
 */
const gchar* gems_profile_manager_get_loaded_username()
{
	return profilemanager_get_loaded_username();
}

/**
 * gems_profile_manager_get_profile_type:
 *
 * Get the type of the loaded profile. Returns 0 when no profile is loaded or
 * user is not authenticated (profile is not decrypted -> no access).
 *
 * Returns: Type of the profile as #guint8 or 0 if no profile is loaded or the 
 * user is not authenticated.
 */
guint8 gems_profile_manager_get_profile_type()
{
	return profilemanager_get_profile_type();
}

/**
 * gems_profile_manager_get_profile_version:
 *
 * Get the version of the loaded profile. Returns 0 if no profile is loaded.
 *
 * Returns: Version number of the profile or 0 if no profile is loaded.
 */
guint8 gems_profile_manager_get_profile_version()
{
	return profilemanager_get_profile_version();
}
