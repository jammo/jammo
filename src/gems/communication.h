/*
 * communication.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Tommi Kallonen <tommi.kallonen@lut.fi>
 *          Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#include "messages.h"
#include "gems_definitions.h"
#include "gems_structures.h"
#include "gems.h"
#include "gems_service_profile.h"


#include <netinet/in.h>

#define FILE_BUFFER_SIZE 512 //send files in packages of up to X bytes

int communication_init();
int communication_list_users();

gems_communication_data* gems_communication_init();
void gems_communication_cleanup();
void gems_communication_cleanup_lists();

connection_search gems_communication_already_connected(guint checksum);

gboolean gems_communication_search_from_list(GList* list, guint checksum);

gems_connection* gems_communication_get_connection(GList* list, guint checksum);
gems_connection* gems_communication_get_connection_with_userid(GList* list, guint32 userid);


void print_list(GList* list);
void print_group(gems_group_info* info);

gint gems_communication_search_jammos();
gint gems_communication_retrieve_profiles();

gint gems_communication_request_groups();
gint gems_communication_advert_group();
gint gems_communication_sanitize_grouplist();

gint gems_communication_process_connecting();
gint gems_communication_process_connected();
gint gems_communication_process_rejected();

gboolean gems_communication_process_data(gems_connection* element);
void gems_communication_process_error(gems_connection* element);

gboolean gems_communication_establish_connection(MAbstractDevice* device);

void gems_communication_process_new(gems_connection* element);
void gems_communication_process_new_rejected(gems_connection* element);

// Write data as envelope to element
gboolean gems_communication_write_encrypted_data(guint16 type, gems_connection* element, gems_message* data);

// Write message to socket
gint gems_communication_write_data(gint sock_fd, gems_message* data);

// Read data from socket, place data in gpointer* data
gint gems_communication_read_data(gint sock_fd, void* data, gint length);

gint gems_communication_probe_for_peerhood();
void gems_communication_peerhood_close_detected();

gems_teacher_connection* gems_teacher_connection_init();
gboolean gems_teacher_connection_disconnected();
void gems_clear_teacher_connection();

gboolean gems_communication_do_profile_request_to_teacher(gchar* username);
gboolean gems_communication_send_profile_to_teacher();
gint gems_communication_search_teacher();

#endif  /* _COMMUNICATION_H_ */
