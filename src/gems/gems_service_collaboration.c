/*
 * gems_service_collaboration.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Tommi Kallonen <tommi.kallonen@lut.fi>
 */
 
#include <string.h>
#include "gems_service_collaboration.h"
#include "gems_message_functions.h"
#include "communication.h"
#include "ProfileManager.h"
#include "collaboration.h"
#include "../cem/cem.h"
#include "../chum/jammo-sample-button.h"
#include "../meam/jammo-midi.h"
#include "../meam/jammo-slider-event.h"
#include "../meam/jammo-loop.h"

gboolean gems_service_collaboration_process_request(gems_connection* element)
{
	gems_components* data = gems_get_data();
	
	switch(gems_connection_get_16(element,6)) // command
	{
		case ACTION_VI_SINGLE:
			gems_service_collaboration_handle_action_vi_single(data,element);
			break;
		case ACTION_LOOP:
			gems_service_collaboration_handle_action_loop(data, element);
			break;
		case ACTION_LOOP_SYNC:
			gems_service_collaboration_handle_action_loop_sync(data, element);
			break;
		case ACTION_MIDI:
			gems_service_collaboration_handle_action_midi(data, element);
			break;
		case ACTION_SLIDER:
			gems_service_collaboration_handle_action_slider(data, element);
			break;
		case ACTION_CONFIRMED:
			gems_service_collaboration_handle_action_confirmed(data,element);
			break;
		case SONG_INFO:
			gems_service_collaboration_handle_song_info(data,element);
			break;
		default:
			break;
	}
	return TRUE;
}

void gems_service_collaboration_handle_action_vi_single(gems_components* data, gems_connection* element)
{
	gchar* logmsg = NULL;
	gchar* png_name=NULL;
	gchar* wav_name=NULL;
	guint16 slot;
	guint position=8;
	//ClutterActor* sample_button;
	gint16 type;
			
	// Type add/remove/..
	type = gems_connection_get_16(element,position);
	position = position + sizeof(gint16);

	// png_name
	png_name = gems_connection_get_char(element,position);
	position = position + strlen(png_name) + 1; // png_name is followed by '\0'

	// wav_name
	wav_name = gems_connection_get_char(element,position);
	position = position + strlen(wav_name) + 1; // wav_name is followed by '\0'

	// slot
	slot = gems_connection_get_16(element,position);
	position = position + sizeof(guint16);

	// Commented out because causes a linker error and is not used anyway
	//Create sample button based on the image and wav
	//sample_button = jammo_sample_button_new_from_files(png_name, wav_name);

	// Log and call CHUM with image and loop information
	if (type == ADD_LOOP)
	{
		logmsg = g_strdup_printf("gems_service_group_handle_action_vi_single: got ADD_LOOP ");
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);

		//Call CHUM
	}
	if (type == REMOVE_LOOP)
	{
		logmsg = g_strdup_printf("gems_service_group_handle_action_vi_single: got REMOVE_LOOP ");
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);

		//Call CHUM
	}
}

void gems_service_collaboration_handle_action_loop(gems_components* data, gems_connection* element)
{
	gchar* logmsg = NULL;
	guint user_id, loop_id, slot;
	guint position=8;
	gint16 type;
			
	// user id
	user_id = gems_connection_get_32(element,position);
	position = position + sizeof(guint);

	// loop id
	loop_id = gems_connection_get_32(element,position);
	position = position + sizeof(guint);

	// type
	type = gems_connection_get_16(element,position);
	position = position + sizeof(gint16);

	// slot
	slot = gems_connection_get_32(element,position);
	position = position + sizeof(guint);


	// Log and call CHUM with loop_id information
	if (type == ADD_LOOP)
	{
		logmsg = g_strdup_printf("gems_service_group_handle_action_loop: got ADD_LOOP ");
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);

		chum_callback_add_loop_to_slot(data->service_collaboration->collaboration_game, user_id,loop_id, slot);

	}
	if (type == REMOVE_LOOP)
	{
		logmsg = g_strdup_printf("gems_service_group_handle_action_loop: got REMOVE_LOOP ");
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);

		chum_callback_remove_loop_from_slot(data->service_collaboration->collaboration_game, user_id, slot);
	}
}

void gems_service_collaboration_handle_action_loop_sync(gems_components* data, gems_connection* element)
{
	gchar* logmsg = NULL;
	guint user_id, length;
	guint position=8;
	GList * looplist=NULL;
	guint32 loop_id, slot;
	
	length = gems_connection_get_32(element,2);
			
	// user id
	user_id = gems_connection_get_32(element,position);
	position = position + sizeof(guint);

	// loops
	for (;position<length;)
	{
		// loop id
		loop_id = gems_connection_get_32(element,position);
		position = position + sizeof(guint32);
		// slot
		slot = gems_connection_get_32(element,position);
		position = position + sizeof(guint32);
		jammo_loop_store_loop_to_glist(&looplist, loop_id, slot);
	}

	// Log and call CHUM with loop list
	logmsg = g_strdup_printf("gems_service_group_handle_action_loop_sync: got LOOP_SYNC");
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);

	chum_callback_loop_sync(data->service_collaboration->collaboration_game, user_id, looplist);

	// free list
	jammo_loop_free_glist(&looplist);
}

void gems_service_collaboration_handle_action_midi(gems_components* data, gems_connection* element)
{
	gchar* logmsg = NULL;
	guint user_id, length;
	guint position=8;
	gint16 type, instrument_id;
	GList * eventlist=NULL;
	gchar note;
	guint64 timestamp;
	JammmoMidiEventType eventtype;
	
	length = gems_connection_get_32(element,2);
			
	// user id
	user_id = gems_connection_get_32(element,position);
	position = position + sizeof(guint);

	// instrument id
	instrument_id = gems_connection_get_16(element,position);
	position = position + sizeof(gint16);

	// type
	type = gems_connection_get_16(element,position);
	position = position + sizeof(gint16);

	// midi events
	for (;position<length;)
	{
		// note
		note = element->nwbuffer[position];
		position = position + sizeof (unsigned char);
		// timestamp
		timestamp = gems_connection_get_64(element,position);
		position = position + sizeof(guint64);
		// type
		eventtype = gems_connection_get_32(element,position);
		position = position + sizeof(guint32);
		jammomidi_store_event_to_glist(&eventlist, note, eventtype, timestamp);
	}

	// Log and call CHUM with midi list
	if (type == REPLACE_MIDI_SERIES)
	{
		logmsg = g_strdup_printf("gems_service_group_handle_action_midi: got REPLACE_MIDI_SERIES ");
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);

		chum_callback_midi_list(data->service_collaboration->collaboration_game, user_id,instrument_id, GEMS_EVENTLIST_REPLACE, eventlist);
	}

	// free list
	jammomidi_free_glist(&eventlist);
}

void gems_service_collaboration_handle_action_slider(gems_components* data, gems_connection* element)
{
	gchar* logmsg = NULL;
	guint user_id, length;
	guint position=8;
	gint16 type, instrument_id;
	GList * eventlist=NULL;
	gfloat freq;
	guint64 timestamp;
	JammoSliderEventType eventtype;
	
	length = gems_connection_get_32(element,2);
			
	// user id
	user_id = gems_connection_get_32(element,position);
	position = position + sizeof(guint);

	// instrument id
	instrument_id = gems_connection_get_16(element,position);
	position = position + sizeof(gint16);

	// type
	type = gems_connection_get_16(element,position);
	position = position + sizeof(gint16);

	// slider events
	for (;position<length;)
	{
		// note
		freq =  gems_connection_get_float(element,position);
		position = position + sizeof(gfloat);
		// timestamp
		timestamp = gems_connection_get_64(element,position);
		position = position + sizeof(guint64);
		// type
		eventtype = gems_connection_get_32(element,position);
		position = position + sizeof(guint32);
		jammo_slider_event_store_event_to_glist(&eventlist, freq, eventtype, timestamp);
	}

	// Log and call CHUM with midi list
	if (type == REPLACE_SLIDER_SERIES)
	{
		logmsg = g_strdup_printf("gems_service_group_handle_action_slider: got REPLACE_SLIDER_SERIES ");
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);

		chum_callback_slider_event_list(data->service_collaboration->collaboration_game, user_id,instrument_id, GEMS_EVENTLIST_REPLACE, eventlist);
	}
	
	// free list
	jammo_slider_event_free_glist(&eventlist);
}

void gems_service_collaboration_handle_action_confirmed(gems_components* data, gems_connection* element)
{
	gchar* logmsg = NULL;
		


	logmsg = g_strdup_printf("gems_service_group_handle_action_confirmed: got ACTION_CONFIRMED ");
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
}

void gems_service_collaboration_handle_song_info(gems_components* data, gems_connection* element)
{
	gchar* logmsg = NULL;
	gchar* song_info=NULL;
	guint position=8;

	// Song file contents
	song_info = gems_connection_get_char(element,position);

	//Inform collaboration about the data
	collaboration_got_song_info(data, song_info);
	

	logmsg = g_strdup_printf("gems_service_group_handle_song_info: got SONG_INFO. Data: %s ", song_info);
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
}

gems_service_collaboration* gems_service_collaboration_init()
{
	gems_service_collaboration* data = g_new(gems_service_collaboration,sizeof(gems_service_collaboration));
	
	data->enabled = FALSE;

	data->active=FALSE;
	
	data->port = 0;

	return data;
}

void gems_service_collaboration_cleanup()
{
	gems_service_collaboration* data = gems_get_data()->service_collaboration;
	
	gems_service_collaboration_cleanup_lists();
	
	if(data != NULL)
	{
		g_free(data);
		data = NULL;
	}
}

void gems_service_collaboration_cleanup_lists()
{
	// TODO clear what?
	return;
}
