/*
 * gems_structures.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2011 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <string.h>
#include <arpa/inet.h>
#include "gems_structures.h"
#include "groupmanager.h"
#include "gems_profile_manager.h"
#include "../cem/cem.h"

/**
 * htonf:
 * @value: float value to pack into 32bit integer
 * 
 * Pack host float to guint32 for sending over network. Performs host to
 * network type operation for given float.
 *
 * Returns: a #guint32 holding the float @value.
 */
guint32 htonf(gfloat value)
{
	guint32 packed;
	guint32 sign;

	if (value < 0)
	{
		sign = 1;
		value = -value;
	}
	else
	{
		sign = 0;
	}
        
	packed = ((((guint32)value)&0x7fff)<<16) | (sign<<31); // whole part and sign
	packed |= (guint32)(((value - (gint)value) * 65536.0f))&0xffff; // fraction
	return packed;
}

/**
 * htonf:
 * @value: float value to pack into 32bit integer
 * 
 * Unpack the float from the network order integer @value witholding the float
 * value. Performs network to host type operation for given integer.
 *
 * Returns: a #gfloat in the @value.
 */
gfloat ntohf(guint32 value)
{
	gfloat ret;
  ret = ((value>>16)&0x7fff); // whole part
  ret += (value&0xffff) / 65536.0f; // fraction

  if (((value>>31)&0x1) == 0x1) { ret = -ret; } // sign bit set
  
  return ret;
}

/**
 * gems_clear_gems_connection:
 * @element: Connection element to clear and free
 *
 * Disconnect, clear and free the given connection @element. If the user in a 
 * group as reqular member and the user on this device is the owner of the
 * group inform other members about a disconnected member. Disconnect all 
 * connections (data and game connections) and destroy all data in the struct.
 *
 * Returns: void
 */
void gems_clear_gems_connection(gems_connection* element)
{
	gchar* logmsg = NULL;
	if(!element) return;
	
	if(element->connection != NULL)
	{
		if(element->profile != NULL)
		{
			/* TODO add to protocol: a notify from group member to owner if a disconnected member is detected
			   by some other than group owner -> group owner should check the connection */
			// Group is active and we are the owner
			if(gems_group_active() && (gems_group_get_owner(NULL) == gems_profile_manager_get_userid(NULL)))
			{
				// If disconnecting user is in our group
				if(gems_group_is_in_group(element->profile->id)) gems_group_disconnected_member_detected(element->profile->id);
			}
		}
		
		if(ph_c_connection_disconnect(element->connection) == TRUE)
		{
			logmsg = g_strdup_printf("Disconnected device %s/%u", ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
		}
		ph_c_connection_delete(element->connection);
	}
	
	if(element->game_connection != NULL)
	{
		if(ph_c_connection_disconnect(element->game_connection) == TRUE)
		{
			logmsg = g_strdup_printf("Disconnected game connection to %s/%u", ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
		}
		ph_c_connection_delete(element->game_connection);
	}
	
	memset(&element->nwbuffer,'\0',JAMMO_NETWORK_BUFFER_MAX_SIZE);
	
	gems_clear_peer_profile(element->profile);
	
	g_timer_destroy(element->profile_request_timer);
	g_timer_destroy(element->group_request_timer);
	
	if(element->last_action != NULL) g_timer_destroy(element->last_action);
	
	g_free(element);
	element = NULL;
}

/**
 * gems_new_gems_connection:
 * @newconn: The new PeerHood connection object
 * @newport: Used port number for the connection
 * @newconnectionid: Id of the connection
 * @newstate: State of the connection
 * @newjammoid: JamMo id - unused!
 *
 * Create new #gems_connection structure with given parameters and initialize
 * its data. New timers are started for the connection and network buffer is
 * cleared.
 *
 * Returns: a pointer to new initialized #gems_connection struct
 */
gems_connection* gems_new_gems_connection(MAbstractConnection* newconn,
	unsigned short newport,
	int newconnectionid,
	jammo_state newstate,
	gint32 newjammoid)
{
	gems_connection* new_item = g_new(gems_connection,sizeof(gems_connection));
	
	new_item->connection = newconn;
	new_item->game_connection = NULL;
	new_item->port = newport;
	new_item->connectionid = newconnectionid;
	new_item->connection_state = newstate;
	new_item->jammoid = newjammoid;
	new_item->profile = NULL;
	new_item->profile_requests = 0;
	new_item->profile_request_timer = g_timer_new();
	new_item->group_request_timer = g_timer_new();
	new_item->last_action = NULL;
	
	gems_connection_clear_buffer(new_item);
	
	return new_item;
}

/**
 * gems_connection_clear_buffer:
 * @element: Connection element which buffer needs to be cleared
 * 
 * Clear the networking buffer of the @element. Set adding position to 0.
 *
 * Returns: void
 */
void gems_connection_clear_buffer(gems_connection* element)
{
	if(element == NULL) return;
	memset(&element->nwbuffer,'\0',JAMMO_NETWORK_BUFFER_MAX_SIZE);
	element->add_position = 0;
}

/**
 * gems_connection_set_action:
 * @element: Connection element which had some activity
 *
 * Record the time of the last action for the connection @element.
 *
 * Returns: void
 */
void gems_connection_set_action(gems_connection* element)
{
	if(element == NULL) return;
	if(element->last_action == NULL) element->last_action = g_timer_new();
	else g_timer_start(element->last_action);
}

/**
 * gems_connection_get_last_action:
 * @element: Connection element to use
 *
 * Get the time it has passed since last event on the connection @element.
 *
 * Returns: Time since last activity on the connection @element.
 */
double gems_connection_get_last_action(gems_connection* element)
{
	if(element == NULL) return 0.0;
	// Wasn't set, set now
	if(element->last_action == NULL) gems_connection_set_action(element);
	return g_timer_elapsed(element->last_action,NULL);
}

/**
 * gems_connection_clear_last_action:
 * @element: connection element to use
 *
 * Clear and delete the last action timer on the connection @element.
 *
 * Returns: void
 */
void gems_connection_clear_last_action(gems_connection* element)
{
	if(element)
	{
		if(element->last_action)
		{
			g_timer_destroy(element->last_action);
			element->last_action = NULL;
		}
	}
}

/**
 * gems_connection_add_16: 
 * @element: Connection element where data will be added
 * @value: Value to add to network buffer
 *
 * Add 16bit integer @value to the network buffer of connection @element.
 *
 * Returns: void
 */
void gems_connection_add_16(gems_connection* element, guint16 value)
{
	if(element == NULL) return;
	
	// TODO report / handle if buffer exceeded
	if((element->add_position + sizeof(guint16)) >= JAMMO_NETWORK_BUFFER_MAX_SIZE) return;
	
	*(guint16*)&element->nwbuffer[element->add_position] = value;
	element->add_position = element->add_position + sizeof(guint16);
}

/**
 * gems_connection_add_32:
 * @element: Connection element where data will be added
 * @value: Value to add to the network buffer
 *
 * Add 32bit integer @value to the network buffer of connection @element.
 *
 * Returns: void
 */
void gems_connection_add_32(gems_connection* element, guint32 value)
{
	if(element == NULL) return;
	
	// TODO report / handle if buffer exceeded
	if((element->add_position + sizeof(guint32)) >= JAMMO_NETWORK_BUFFER_MAX_SIZE) return;
	
	*(guint32*)&element->nwbuffer[element->add_position] = value;
	element->add_position = element->add_position + sizeof(guint32);
}

/**
 * gems_connection_add_64:
 * @element: Connection element where data will be added
 * @value: Value to add to the network buffer 
 *
 * Add 64bit integer @value to the network buffer of connection @element.
 *
 * Returns: void
 */
void gems_connection_add_64(gems_connection* element, guint64 value)
{
	if(element == NULL) return;
	
	// TODO report / handle if buffer exceeded
	if((element->add_position + sizeof(guint64)) >= JAMMO_NETWORK_BUFFER_MAX_SIZE) return;
	
	*(guint64*)&element->nwbuffer[element->add_position] = value;
	element->add_position = element->add_position + sizeof(guint64);
}

/**
 * gems_connection:
 * @element: Connection element where data will be added
 * @value: Value to add to network buffer
 *
 * Add 32bit float @value to network buffer of connection @element. This can be
 * done because message has been received and it will not sent anymore so no
 * need to take care of host vs. network order.
 *
 * Returns: void
 */
void gems_connection_add_float(gems_connection* element, gfloat value)
{
	if(element == NULL) return;
	
	// TODO report / handle if buffer exceeded
	if((element->add_position + sizeof(gfloat)) >= JAMMO_NETWORK_BUFFER_MAX_SIZE) return;
	
	*(gfloat*)&element->nwbuffer[element->add_position] = value;
	element->add_position = element->add_position + sizeof(gfloat);
}

/**
 * gems_connection_add_char:
 * @element: Connection element where data will be added
 * @value: Value to add to network buffer
 * @length: length of the @value in bytes
 * @pad: Use padding byte at the end of the added value (at adding position + 
 * length + 1)
 *
 * Add characters (amount = @length) to the network buffer of connection
 * @element, define need for padding with last value (@pad)
 *
 * Returns: void
 */
void gems_connection_add_char(gems_connection* element, gchar* value, guint length, gboolean pad)
{
	if(element == NULL || value == NULL) return;
	
	// TODO report / handle if buffer exceeded
	if((element->add_position + length) >= JAMMO_NETWORK_BUFFER_MAX_SIZE) return;
	
	g_strlcat(&element->nwbuffer[element->add_position], value, element->add_position + length);
	element->add_position = element->add_position + length;
	
	// Adding partial read, erase the \0 character
	if(pad == FALSE) element->add_position = element->add_position - 1;
}

/**
 * gems_connection_add_padding:
 * @element: Connection element where data will be added
 *
 * Add padding byte (NULL) to the network buffer of the connection @element.
 *
 * Returns: void
 */
void gems_connection_add_padding(gems_connection* element)
{
	if(element == NULL) return;
	
	if((element->add_position + 1) >= JAMMO_NETWORK_BUFFER_MAX_SIZE) return;
	
	element->nwbuffer[element->add_position] = '\0';
	element->add_position = element->add_position + 1;
}

/**
 * gems_connection_add_data:
 * @element: Connection element where data will be added
 * @data: data to add
 * @length: length of @data in bytes
 *
 * Add @data to the end of the network buffer of the connection @element with
 * given @length.
 *
 * Returns: void
 */
void gems_connection_add_data(gems_connection* element, gchar* data, guint length)
{
	if(element == NULL || data == NULL) return;
	
	if(element->add_position + length >= JAMMO_NETWORK_BUFFER_MAX_SIZE) return;
	
	memcpy(&element->nwbuffer[element->add_position],data,length);
	element->add_position = element->add_position + length;
}

/**
 * gems_connection_get_16:
 * @element: Connection element which has data in its network buffer
 * @from: Data position in the network buffer of the connection @element
 *
 * Get 16 bit integer from the network buffer of the connection @element at
 * position @from.
 *
 * Returns: 16 bit integer value at position @from in the network buffer.
 */
guint16 gems_connection_get_16(gems_connection* element, guint from)
{
	if(element == NULL) return 0;
	
	if(from > element->add_position) return 0;
	
	return *(guint16*)&element->nwbuffer[from];
}

/**
 * gems_connection_get_32:
 * @element: Connection element which has data in its network buffer
 * @from: Data position in the network buffer of the connection @element
 *
 * Get 32 bit integer from the network buffer of the connection @element at
 * position @from.
 *
 * Returns: 32 bit integer value at position @from in the network buffer.
 */
guint32 gems_connection_get_32(gems_connection* element, guint from)
{
	if(element == NULL) return 0;
	
	if(from > element->add_position) return 0;
	
	return *(guint32*)&element->nwbuffer[from];
}

/**
 * gems_connection_get_64:
 * @element: Connection element which has data in its network buffer
 * @from: Data position in the network buffer of the connection @element
 *
 * Get 64 bit integer from the network buffer of the connection @element at
 * position @from.
 *
 * Returns: 64 bit integer value at position @from in the network buffer.
 */
guint64 gems_connection_get_64(gems_connection* element, guint from)
{
	if(element == NULL) return 0;
	
	if(from > element->add_position) return 0;
	
	return *(guint64*)&element->nwbuffer[from];
}

/**
 * gems_connection_get_float_for_localization:
 * @element: Connection element which has data in its network buffer
 * @from: Data position in the network buffer of the connection @element
 *
 * Get float from the network buffer of the connection @element at position
 * @from. The float value is as #guint32 in the network buffer and is unpacked
 * with ntohf() before returning the #gfloat. To get the float "as-it-is" use
 * gems_connection_get_float() and use this function when localization for the
 * float is required.
 *
 * Returns: A #gfloat alue at position @from in the network buffer.
 */
gfloat gems_connection_get_float_for_localization(gems_connection* element, guint from)
{
	if(element == NULL) return 0;
	
	gfloat ret;
	guint32 value;

	if(from > element->add_position) return 0;
	
	value = *(guint32*)&element->nwbuffer[from];
	 
  ret = ntohf(value);

  return ret;
}

/**
 * gems_connection_get_float:
 * @element: Connection element which has data in its network buffer
 * @from: Data position in the network buffer of the connection @element
 *
 * Get float from the network buffer of the connection @element at position
 * @from.
 *
 * Returns: A #gfloat value at position @from in the network buffer.
 */
gfloat gems_connection_get_float(gems_connection* element, guint from)
{
	if(element == NULL) return 0;
	
	gfloat value;

	if(from > element->add_position) return 0;
	
	value = *(gfloat*)&element->nwbuffer[from];

  return value;
}

/**
 * gems_connection_get_char:
 * @element: Connection element which has data in its network buffer
 * @from: Data position in the network buffer of the connection @element
 *
 * Get characters from the network buffer of the connection @element onwards
 * from position @from until the first NULL character. This is not a safe
 * way to get data, use gems_connection_get_data() to more safe way of getting
 * data from network buffer.
 *
 * Returns: A #gchar pointer to the network buffer at position @from. Do not
 * attempt free this value (it has a will of its own)!
 */
gchar* gems_connection_get_char(gems_connection* element, guint from)
{
	if(element == NULL) return NULL;
	
	// TODO do a real check - copy until \0 character is found !
	return (gchar*)&element->nwbuffer[from];
}

/**
 * gems_connection_get_data:
 * @element: Connection element which has data in its network buffer
 * @from: Data position in the network buffer of the connection @element
 * @length: Amount of data to be retrieved from the network buffer  in bytes
 *
 * Get @length amount of data from the network buffer of the connection
 * @element at position @from. Allocates new #gchar to hold the copy of the
 * requested data.
 *
 * Returns: A new #gchar containing data with @length from position @from in
 * the network buffer. Free this after used!
 */
gchar* gems_connection_get_data(gems_connection* element, guint from, guint length)
{
	if(element == NULL) return NULL;
	
	gchar* buffer = g_new0(gchar,sizeof(gchar)*length);

	memcpy(buffer, &element->nwbuffer[from],length);
		
	return buffer;
}

/**
 * gems_connection_set_content:
 * @element: Connection element which network buffer is used
 * @newcontent: New network buffer content to set
 * @length: Length of the @newcontent in bytes
 *
 * Copy the given content (@newcontent) to the network buffer of the given
 * connection @element. Clears the network buffer from old data and replaces
 * the content with @newcontent.
 *
 * Returns: void
 */
void gems_connection_set_content(gems_connection* element, gchar* newcontent, guint length)
{
	if(element == NULL) return;
	
	gems_connection_clear_buffer(element);
	memcpy(&element->nwbuffer,newcontent,length);
}

/**
 * gems_connection_partial_data_amount:
 * @element: Connection element which network buffer is questioned for partial
 * data
 *
 * Return the amount of data in the network buffer of the connection @element.
 *
 * Returns: Amount of bytes in the network buffer as #guint.
 */
guint gems_connection_partial_data_amount(gems_connection* element)
{
	if(element == NULL) return 0;
	return element->add_position;
}

/**
 * gems_cleanup_connections_from_list:
 * @list: The #GList containing pointers to #gems_connection elements
 *
 * Clear and free all disconnected #gems_connection elements from given @list.
 *
 * Returns: A pointer to given @list after processing.
 */
GList* gems_cleanup_connections_from_list(GList* list)
{
	if(list == NULL) return NULL;
	
	GList* iterator = g_list_first(list);
	GList* tempiter = NULL;
	
	while(iterator)
	{
		// Disconnected from our end ?
		if(ph_c_connection_is_connected((MAbstractConnection*)((gems_connection*)iterator->data)->connection) == FALSE)
		{
			tempiter = iterator;
			iterator = g_list_next(iterator);
			
			list = g_list_remove_link(list,tempiter);
			gems_clear_gems_connection((gems_connection*)tempiter->data);
			g_list_free_1(tempiter);
			gchar* logmsg = g_strdup_printf("gems_cleanup_connections_from_list: erased one");
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
			continue;
		}
	
		iterator = g_list_next(iterator);
	}
	return list;
}

/**
 * gems_new_peer_profile:
 * @username: Username in the profile
 * @id: User id in the profile
 * @age: Age of the user
 * @avatarid: Id of the users avatar
 *
 * Create new #gems_peer_profile struct and fill it with given information.
 * 
 * Returns: A pointer to created and set up #gems_peer_profile struct.
 */
gems_peer_profile* gems_new_peer_profile(gchar* username, guint32 id, guint16 age, guint32 avatarid)
{
	if(username == NULL) return NULL;
	 
	gems_peer_profile* profile = g_new(gems_peer_profile,sizeof(gems_peer_profile));
	
	profile->id = id;
	profile->username = g_strdup(username);
	profile->avatarid = avatarid;
	profile->age = age;
	
	return profile;
}

/**
 * gems_peer_profile_set_values:
 * @profile: Profile to fill with new data
 * @username: Username in the profile
 * @id: User id in the profile
 * @age: Age of the user
 * @avatarid: Id of the users avatar
 *
 * Set data to #gems_peer_profile struct.
 * 
 * Returns: void
 */
void gems_peer_profile_set_values(gems_peer_profile* profile, gchar* username, guint32 id, guint16 age, guint32 avatarid)
{
	if(profile == NULL || username == NULL) return;
	
	profile->id = id;
	profile->username = g_strdup(username);
	profile->avatarid = avatarid;
	profile->age = age;
}

/**
 * gems_clear_peer_profile:
 * @profile: Profile to clear and free
 *
 * Clear and free the given @profile.
 *
 * Returns: void
 */
void gems_clear_peer_profile(gems_peer_profile* profile)
{
	if(profile == NULL) return;
	g_free(profile->username);
	profile->username = NULL;
	g_free(profile);
	profile = NULL;
}

/**
 * gems_connection_localize_data_in_buffer:
 * @element: Connection element which network buffer contains data to be
 * localized
 *
 * Localize (from network to host) the message in the network buffer of the
 * connection @element.
 *
 * Returns: void.
 */
void gems_connection_localize_data_in_buffer(gems_connection* element)
{
	if(element == NULL) return;
	
	gchar* logmsg = NULL;
	int temp_position = element->add_position;
	// first 8 bytes should be always the same - even with error messages
	element->add_position = 0;
	gems_connection_add_16(element,g_ntohs(gems_connection_get_16(element,element->add_position))); // pos should be 0
	gems_connection_add_32(element,g_ntohl(gems_connection_get_32(element,element->add_position))); // pos should be 2
	gems_connection_add_16(element,g_ntohs(gems_connection_get_16(element,element->add_position))); // pos should be 6
	
	switch(gems_connection_get_16(element,0))
	{
		case ERROR_MSG:
			// convert error type at 8
			gems_connection_add_16(element,g_ntohs(gems_connection_get_16(element,element->add_position))); // pos should be 8
			// TODO convert the error message (chars from 10->)
			break;
		case JAMMO_SERVICE_ID:
			logmsg = g_strdup_printf("gems_connection_localize_data_in_buffer: got JAMMO_SERVICE_ID from %s/%u - should not happen!!", 
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
			break;
		case AA_SERVICE_ID:
			break;
		case PROFILE_SERVICE_ID:
			switch(gems_connection_get_16(element,6)) // the service command from position 6
			{
				case GET_PROFILE:
					// request type, add_position should be 8 and type should be in position 8 as 16bit int
					gems_connection_add_16(element,g_ntohs(gems_connection_get_16(element,element->add_position)));
					break;
				case PROFILE_PUB:
					// TODO convert username
					// Username -> add_position = add_position + Username length + tailing '\0'
					element->add_position = element->add_position + strlen(gems_connection_get_char(element,element->add_position)) + 1;
					// User id
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element,element->add_position)));
					// age
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element,element->add_position)));
					// avatar id
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element,element->add_position)));
					break;
				case PROFILE_FULL:
					// Username -> add_position = add_position + Username length + tailing '\0'
					element->add_position = element->add_position + strlen(gems_connection_get_char(element,element->add_position)) + 1;
					// User id
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element,element->add_position)));
					// age
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element,element->add_position)));
					// avatar id
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element,element->add_position)));

					// firstname and lastname
					element->add_position = element->add_position + strlen(gems_connection_get_char(element,element->add_position)) + 1;
					element->add_position = element->add_position + strlen(gems_connection_get_char(element,element->add_position)) + 1;
					// points
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element,element->add_position)));
					break;
				case REQ_ENC_PROFILE:
					// Should contain username after 16+32+16 bits, no conversion
					break;
				case PROFILE_ENC:
					// Username length
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element,element->add_position)));
					break;
				case PROFILE_SET_UNAME:
					break;
				case PROFILE_PASSCHANGED:
					// Userid
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element,element->add_position)));
					break;
				case CONTROL_REPLY_LOGIN:
					// Username -> add_position = add_position + Username length + tailing '\0'
					element->add_position = element->add_position + strlen(gems_connection_get_char(element,element->add_position)) + 1;
					// User id
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element,element->add_position)));
					// age
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element,element->add_position)));
					// avatar id
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element,element->add_position)));
					break;
				default:
					break;
			}
		case GROUP_SERVICE_ID:
			switch(gems_connection_get_16(element,6)) // command
			{
				case REQUEST_MEMBERS:
					break;
				case REQUEST_GROUP_INFO:
					break;
				case OFFER_GROUP:
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					// TODO add group lock type handling
					break;
				case CURRENT_GROUP:
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					// TODO add group lock type handling
					break;
				case JOIN_GROUP:
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					break;
				case MEMBER_LIST:
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					break;
				case NEW_MEMBER:
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					break;
				case LEAVE_GROUP:
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					break;
				case REMOVED_FROM_GROUP:
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					break;
				case MEMBER_DROPPED:
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					break;
				case FORCE_GROUP:
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					break;
				default:
					break;
			}
			break;
		case COLLABORATION_SERVICE_ID:
			switch (gems_connection_get_16(element,6)) {
				case ACTION_LOOP:
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					break;
				case ACTION_LOOP_SYNC:
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					for (; element->add_position < gems_connection_get_32(element, 2);) {
						// loop id
						gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
						// slot
						gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					}
					break;
				case ACTION_MIDI:
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					for (; element->add_position < gems_connection_get_32(element, 2);) {
						// note is a char. no need to do anything
						element->add_position += sizeof(unsigned char);
						// timestamp
						gems_connection_add_64(element, GUINT64_FROM_BE(gems_connection_get_64(element, element->add_position)));
						// type
						gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					}
					break;
				case ACTION_SLIDER:
					gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					for (; element->add_position < gems_connection_get_32(element, 2);) {
						// freq is float. it is received as an 32bit integer and localized to a float
						// see add float and get float functions for details
						gems_connection_add_float(element, gems_connection_get_float_for_localization(element, element->add_position));
						// timestamp
						gems_connection_add_64(element, GUINT64_FROM_BE(gems_connection_get_64(element, element->add_position)));
						// type
						gems_connection_add_32(element, g_ntohl(gems_connection_get_32(element, element->add_position)));
					}
					break;
				default:
					break;
			}
			break;
		case SONGBANK_SERVICE_ID:
			switch(gems_connection_get_16(element,6)) // command
			{
				case SEARCH_FILE_REQ:
					//  Filetype 16 bits
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					break;
				case SEARCH_FILE_REPLY:
					//  Status 16 bits
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					break;
				case FILE_REQ:
					//  Filetype 16 bits
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					break;
				case FILE_HEADER:
					//  Filetype 16 bits
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					//  File size 16 bits
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					break;
				case FILE_PART:
					//  File part length 16 bits
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					//  id 16 bits
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					break;
			}
		case DATATRANSFER_SERVICE_ID:
			break;
		case MENTOR_SERVICE_ID:
			break;
		case CONTROL_SERVICE_ID:
			switch(gems_connection_get_16(element,6)) // command
			{
				case CONTROL_OK:
					break;
				case CONTROL_MODE:
					// Control type 16 bits
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					// Lock mode 16 bits
					gems_connection_add_16(element, g_ntohs(gems_connection_get_16(element, element->add_position)));
					break;
				case CONTROL_SET_LOGIN:
					break;
				case CONTROL_GET_LOGIN:
					break;
				case CONTROL_REPLY_LOGIN:
					break;
				case CONTROL_FORCE_LOGIN:
					break;
				default:
					break;
			}
			break;
		case PROBE_SERVICE_ID:
			break;
		default:
			break;
	}
	
	element->add_position = temp_position;
}

/**
 * gems_connection_error_add:
 * @list: A #GList containing pointers to #gems_connection_error structs
 * @device_checksum: Checksum of the device to be added into error list
 * @errortype: The classification of the error related to the device
 *
 * Add error to device with #device_checkum into given @list, if the device is
 * found from the @list update information, otherwise add new device as 
 * #gems_connection_error type struct with @errortype. Initialize timers for
 * the device if the error is related to existing connection. For "plain"
 * errors increase the counter for errors. 
 *
 * Returns: A pointer to the @list after modifications.
 */
GList* gems_connection_error_add(GList* list, guint device_checksum, GemsConnErrType errortype)
{
	gchar* logmsg = NULL;
	gems_connection_error* item = NULL;
	GRand* grand = NULL;
	
	if(errortype == ERROR_ALREADY_CONNECTED) grand = g_rand_new_with_seed(time(NULL));
	
	// Not found -> create new and add
	if((item = gems_connection_error_get(list,device_checksum)) == NULL)
	{
		item = g_new(gems_connection_error,sizeof(gems_connection_error));
		item->checksum = device_checksum;
		
		if(errortype == ERROR_CONNECTION) item->connection_errors = 1;
		else item->connection_errors = 0;
		
		if(errortype == ERROR_REJECTED) item->version_mismatch = TRUE;
		else item->version_mismatch = FALSE;
		
		if(errortype == ERROR_ALREADY_CONNECTED)
		{
			item->already_connected = TRUE;
			item->already_connected_delay = g_rand_double_range(grand,AC_DELAY_LOW,AC_DELAY_HIGH);
		}
		else
		{
			item->already_connected = FALSE;
			item->already_connected_delay = 0.0;
		}
		
		item->connection_timer = g_timer_new();
		g_timer_start(item->connection_timer);
		list = g_list_prepend(list,item);
		logmsg = g_strdup_printf("gems_connection_error_add: added new device %u, list length: %d", item->checksum, g_list_length(list));
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
	else // found
	{
		switch(errortype)
		{
			case ERROR_CONNECTION:
				// Limit reached, do not add since the delay is 1min
				if(item->connection_errors < CONNECTION_ERRORLIMIT)	item->connection_errors += 1; // Add a error
				break;
			case ERROR_REJECTED:
				item->version_mismatch = TRUE;
				break;
			case ERROR_ALREADY_CONNECTED:
			{
				item->already_connected = TRUE;
				item->already_connected_delay = g_rand_double_range(grand,AC_DELAY_LOW,AC_DELAY_HIGH);
				break;
			}
			default:
				break;
		}
		g_timer_start(item->connection_timer);
		logmsg = g_strdup_printf("gems_connection_error_add: added to existing device %u (errors : %d)", item->checksum, item->connection_errors);
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
	
	if(grand) g_rand_free(grand);
	return list;
}

/**
 * gems_connection_error_get:
 * @list: a #GList containing pointers to #gems_connection_error structs
 * @device_checksum: device which error data is to be retrieved from @list
 *
 * Searches the given @list for occurances of devices with @device_checksum.
 * Returns the error data or NULL if not found.
 *
 * Returns: A pointer to the error data or NULL if not found.
 */
gems_connection_error* gems_connection_error_get(GList* list, guint device_checksum)
{
	GList* iterator = NULL;
	if(list == NULL) return NULL;
	if(g_list_length(list) == 0) return NULL;
	
	// Search the list
	for(iterator = g_list_first(list); iterator; iterator = g_list_next(iterator))
	{
		gems_connection_error* element = (gems_connection_error*)iterator->data;
		
		if(element == NULL) continue;
		
		// Match - return element
		if(element->checksum == device_checksum) return element;
	}
	return NULL;	
}

/**
 * gems_connection_error_get_connection_error_count:
 * @list: a #GList containing pointers to #gems_connection_error structs
 * @device_checksum: device which error data is to be retrieved from @list
 *
 * Get error count for given @device_checksum in the @list.
 *
 * Returns: Amount of connection errors for the device with #device_checksum. 0
 * will be returned if not found.
 */
guint gems_connection_error_get_connection_error_count(GList* list, guint device_checksum)
{
	gems_connection_error* item = gems_connection_error_get(list,device_checksum);
	if(item == NULL) return 0;
	else return item->connection_errors;
}

/**
 * gems_connection_error_attempt_connection:
 * @list: a #GList containing pointers to #gems_connection_error structs
 * @device_checksum: device which the connection is attempted to, searched from
 * the @list
 *
 * Ask if the device with #device_checksum can be connected to. Will return
 * FALSE if enough time has not passed from previous errorous attempt or the
 * delay for existing connection is not passed. FALSE is returned always if the
 * connection to device was previously rejected.
 * 
 * Returns: TRUE if the connection could be attempted, FALSE if it is a 
 * rejected connection, or enough time has not passed.
 */
gboolean gems_connection_error_attempt_connection(GList* list, guint device_checksum)
{
	gems_connection_error* item = gems_connection_error_get(list,device_checksum);
	gdouble err_time = 0.0;
	
	if(item == NULL)
	{
		//logmsg = g_strdup_printf("gems_connection_error_attempt_connection: no device yet: %d", device_checksum);	
		return TRUE; // Not set
	}
	else
	{
		// reached errorlimit -> longer interval
		if(item->connection_errors > CONNECTION_ERRORLIMIT)	err_time = CONNECTION_INTERVAL * 10 + item->connection_errors;
		// Each error gives 1s more delay
		else err_time = item->connection_errors + CONNECTION_INTERVAL;
		
		//logmsg = g_strdup_printf("gems_connection_error_attempt_connection: checking time for %d, time = %f, errors = %d", item->checksum, time, item->connection_errors);
		
		if(item->version_mismatch) return FALSE;
		if(item->already_connected)
		{
			// Connection delay expired
			if(g_timer_elapsed(item->connection_timer,NULL) > item->already_connected_delay) return TRUE;
			else return FALSE;
		}
		
		// less than 'time' elapsed?
		if(g_timer_elapsed(item->connection_timer,NULL) < err_time)
		{
			//logmsg = g_strdup_printf("gems_connection_error_attempt_connection: %f passed ", g_timer_elapsed(item->connection_timer,NULL));
			return FALSE;
		}
		else
		{
			//logmsg = g_strdup_printf("gems_connection_error_attempt_connection: more than %f elapsed", time);
			g_timer_start(item->connection_timer);
			return TRUE;
		}
	}
	
	return TRUE;
}

/**
 * gems_connection_error_remove:
 * @list: a #GList containing pointers to #gems_connection_error structs
 * @device_checksum: Device to remove from the given errorlist @list
 *
 * Remove the error data of the device with #device_checksum from the given 
 * @list. Usually called after a succesfull connection or the device has been
 * in the list long enough.
 *
 * Returns: A pointer to given @list after processing.
 */
GList* gems_connection_error_remove(GList* list, guint device_checksum)
{
	GList* iterator = NULL;
	if(list == NULL) return list;
	if(g_list_length(list) == 0) return list;
	
	// Search the list
	for(iterator = g_list_first(list); iterator; iterator = g_list_next(iterator))
	{
		gems_connection_error* element = (gems_connection_error*)iterator->data;
		
		if(element == NULL) continue;
		
		// Match
		if(element->checksum == device_checksum)
		{
			gchar* logmsg = g_strdup_printf("gems_connection_error_remove: removed %u",device_checksum);
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
			
			list = g_list_remove_link(list,iterator);
			gems_clear_connection_error((gems_connection_error*)iterator->data);
			g_list_free_1(iterator);
			return list;
		}
	}
	return list;
}

/**
 * gems_clear_connection_error:
 * @element: Connection error element to clear and free
 *
 * Clear and free the given connection error @element.
 *
 * Returns: void
 */
void gems_clear_connection_error(gems_connection_error* element)
{
	if(element == NULL) return;
	g_timer_destroy(element->connection_timer);
	g_free(element);
	element = NULL;
	return;
}

/**
 * gems_process_rejected:
 * @list: A #GList to be processed for rejected connections
 *
 * Remove rejected connections from @list which have been in the list for more
 * than %REJECTED_LIST_REMOVE_LIMIT time.
 * 
 * Returns: A pointer to given @list after processing.
 */
GList* gems_process_rejected(GList* list)
{
	if(list == NULL) return NULL;
	
	GList* iterator = g_list_first(list);
	
	while(iterator)
	{
		gems_connection* element = (gems_connection*)iterator->data;
		
		if(element == NULL) continue;
		
		if(element->last_action != NULL)
		{
			// In list for over 5 minutes?
			if(gems_connection_get_last_action(element) > REJECTED_LIST_REMOVE_LIMIT)
			{
				GList* tempiter = iterator;
				iterator = g_list_next(iterator);
				
				list = g_list_remove_link(list,tempiter);
				gems_clear_gems_connection(element);
				g_list_free_1(tempiter);
				
				gchar* logmsg = g_strdup_printf("gems_process_rejected: Removed one from rejected list.");
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
				
				continue;
			}
		}
		
		iterator = g_list_next(iterator);
	}
	return list;
}

