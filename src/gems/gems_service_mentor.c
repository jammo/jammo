/*
 * gems_service_mentor.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Tommi Kallonen <tommi.kallonen@lut.fi>
 */


#include "gems_service_mentor.h"
#include "gems_message_functions.h"
#include "../configure.h"
#include "communication.h"
#include "../cem/cem.h"
#include "mentor.h"


gboolean gems_service_mentor_process_request(gems_connection* element)
{
	gems_components* data = gems_get_data();
	
	switch(gems_connection_get_16(element,6)) // command
	{
		case LOG_MESSAGE:
			gems_service_mentor_handle_log_message(data,element);
			break;
		default:
			break;
	}
	return TRUE;
}

gems_service_mentor* gems_service_mentor_init()
{
	gems_service_mentor* data = g_new(gems_service_mentor,sizeof(gems_service_mentor));
	
	data->enabled = FALSE;
	
	data->port = 0;

	//Inform cem of teacher logging function
	void (*teacher_log_function)(char *message,int type) = NULL;
	teacher_log_function = &mentor_send_log_message_to_teacher;
	cem_set_callback_send_log_message_to_teacher(teacher_log_function);

	return data;
}

void gems_service_mentor_cleanup()
{
	gems_service_mentor* data = gems_get_data()->service_mentor;
	
	
	if(data != NULL)
	{
		g_free(data);
		data = NULL;
	}
}

void gems_service_mentor_handle_log_message(gems_components* data, gems_connection* element)
{
	guint position=sizeof(gint16)+sizeof(gint32)+sizeof(gint16);

	gchar* message = gems_connection_get_char(element,position);

	FILE* fp;
	gchar* logFileName;

	gems_peer_profile* prof = NULL;
		
	prof = gems_get_profile(element); // Get sender profile

	logFileName = g_strdup_printf("%s/%d.log",configure_get_log_directory(), gems_profile_manager_get_userid(prof));

	fp = fopen(logFileName, "a");
	fflush(fp);
	g_free(logFileName);
	g_return_if_fail(fp != NULL);

	fprintf(fp,"%s\n",message);
	fclose(fp);
}

