#include "songbank.h"
#include "gems_service_songbank.h"
#include "gems_definitions.h"
#include "gems_message_functions.h"
#include "../cem/cem.h"

guint GetFileFromTeacher(guint16 filetype, gchar* filename)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	gems_teacher_connection* tc = gems_get_teacher_connection();
	gboolean success = TRUE;

	gems_get_data()->service_songbank->searchfile = filename;
	
	if(tc)
	{
		// Is connected
		if(gems_teacher_connection_is_connected())
		{
			msg = gems_create_message_songbank_file_req(FILE_GENERAL, filename);
			if(msg != NULL)
			{					
				// Send
				if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,tc->connection,msg) == FALSE)
				{
					logmsg = g_strdup_printf("GetFileFromTeacher: cannot write FILE_REQ to %s/%u",
						ph_c_connection_get_remote_address(tc->connection->connection), ph_c_connection_get_device_checksum(tc->connection->connection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
				}
				// Free message
				gems_clear_message(msg);
			}

		}
		else success=FALSE;
	}
	else success=FALSE;

	if(!success)
	{
		logmsg = g_strdup_printf("GetFileFromTeacher: no connection to teacher");
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
		return -1;	
	}

	return 0;
}

guint SendFileToTeacher(guint16 filetype, gchar* filename)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	FILE* searchfile=NULL;
	guint16 filesize;
	gboolean success = TRUE;
	gems_teacher_connection* tc = gems_get_teacher_connection();

	if(tc)
	{
		// Is connected
		if(gems_teacher_connection_is_connected())
		{
			//searchfile = execFindCommand(filename);
			searchfile=fopen( filename, "r");
			if(searchfile != NULL)
			{
	
				fseek(searchfile, 0L, SEEK_END);
				filesize = ftell(searchfile);
				
				msg = gems_create_message_songbank_file_header(FILE_GENERAL, filesize, g_strrstr(filename, "/")+1);
				if(msg != NULL)
				{					
					// Send
					if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,tc->connection,msg) == FALSE)
					{
						logmsg = g_strdup_printf("SendFileToTeacher: cannot write FILE_HEADER to %s/%u",
							ph_c_connection_get_remote_address(tc->connection->connection), ph_c_connection_get_device_checksum(tc->connection->connection));
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
					}
					else//Header sent succesfully
					{
						logmsg = g_strdup_printf("SendFileToTeacher: file header sent to %s/%u",
							ph_c_connection_get_remote_address(tc->connection->connection), ph_c_connection_get_device_checksum(tc->connection->connection));
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						gems_get_data()->service_songbank->sendfilename = filename;
						gems_get_data()->service_songbank->targetconnection = tc->connection;
						gems_get_data()->service_songbank->location = 0;
						gems_get_data()->service_songbank->part_id = 0;
						gems_get_data()->service_songbank->active = TRUE;
					}
					// Free message
					gems_clear_message(msg);
				}
				fclose(searchfile);
				//free(searchfile);
				
			}	
		}
		else success=FALSE;
	}
	else success=FALSE;

	if(!success)
	{
		logmsg = g_strdup_printf("GetFileFromTeacher: no connection to teacher");
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
		return -1;	
	}
	return 0;
}

gint songbank_file_sender_loop()
{
	char buff[FILE_BUFFER_SIZE];
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	FILE* searchfile=NULL;
	guint16 partsize;

	if(gems_get_data()->service_songbank->active)
	{
		if(ph_c_connection_is_connected(gems_get_data()->service_songbank->targetconnection->connection))
		{
			//searchfile = execFindCommand(gems_get_data()->service_songbank->sendfilename);
			searchfile=fopen( gems_get_data()->service_songbank->sendfilename, "r");
			if(searchfile != NULL)
			{
				fseek(searchfile, gems_get_data()->service_songbank->location, SEEK_SET);
				partsize = fread(buff, sizeof(char), FILE_BUFFER_SIZE, searchfile);
				msg = gems_create_message_songbank_file_part(FILE_PART, partsize, gems_get_data()->service_songbank->part_id, buff);
				if(msg != NULL)
				{					
					// Send
					if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, gems_get_data()->service_songbank->targetconnection ,msg) == FALSE)
					{
						logmsg = g_strdup_printf("songbank_file_sender_loop: cannot write FILE_PART to %s/%u",
							ph_c_connection_get_remote_address(gems_get_data()->service_songbank->targetconnection->connection), ph_c_connection_get_device_checksum(gems_get_data()->service_songbank->targetconnection->connection));
						cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						gems_get_data()->service_songbank->sendfilename = NULL;
						gems_get_data()->service_songbank->targetconnection = NULL;
						gems_get_data()->service_songbank->location = 0;
						gems_get_data()->service_songbank->part_id = 0;
						gems_get_data()->service_songbank->active = FALSE;
					}
					else//part sent, update information
					{
						printf("File part sent\n");
						if(partsize<FILE_BUFFER_SIZE)//last package sent
						{
							gems_get_data()->service_songbank->sendfilename = NULL;
							gems_get_data()->service_songbank->targetconnection = NULL;
							gems_get_data()->service_songbank->location = 0;
							gems_get_data()->service_songbank->part_id = 0;
							gems_get_data()->service_songbank->active = FALSE;
						}
						else //More to be sent
						{
							gems_get_data()->service_songbank->location = gems_get_data()->service_songbank->location + partsize;
							gems_get_data()->service_songbank->part_id++;

						}
					}
					// Free message
					gems_clear_message(msg);
				}

			}
			fclose(searchfile);

		}
		else
		{
			logmsg = g_strdup_printf("songbank_file_sender_loop: no connection to target");
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);			
			gems_get_data()->service_songbank->sendfilename = NULL;
			gems_get_data()->service_songbank->targetconnection = NULL;
			gems_get_data()->service_songbank->location = 0;
			gems_get_data()->service_songbank->part_id = 0;
			gems_get_data()->service_songbank->active = FALSE;
		}
	}
return 0;
}
