/*
 * communication.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Tommi Kallonen <tommi.kallonen@lut.fi>
 *          Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#include "communication.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <glib.h>
#include <peerhood/IteratorWrapper.h>

#include "gems_message_functions.h"
#include "gems_security.h"
#include "groupmanager.h"
#include "../cem/cem.h"
#include "gems_service_collaboration.h"
#include "gems_service_control.h"
#include "collaboration.h"
#include "songbank.h"

#define SOCKET_ERROR        -1
#define HOST_NAME_SIZE      255
#define QUEUE_SIZE          5
#define BUFFER_SIZE         1000
#define JAMMOPORT		5555

/**
 * gems_communication_init:
 *
 * Initialize GEMS communication structures. Initializes the 
 * #gems_communication_data structure and lists in the structure.
 *
 * Returns: Pointer to initialized structure.
 */
gems_communication_data* gems_communication_init()
{
	// Communication data struct
	gems_communication_data* data = g_new0(gems_communication_data,sizeof(gems_communication_data));
	
	// Communication list
	data->connections = NULL;
	
	// Connecting list
	data->connecting = NULL;
	
	// Rejected list
	data->rejected = NULL;
	
	return data;
}

/**
 * gems_communication_cleanup:
 *
 * Clean all GEMS communication data. Cleans up the lists by calling 
 * gems_communication_cleanup_lists() and frees the #gems_communication_data 
 * structure retrieved with gems_get_data() .
 *
 * Returns: void
 */
void gems_communication_cleanup()
{	
	gems_communication_data* data = gems_get_data()->communication;
	
	if(data == NULL) return;
	
	gems_communication_cleanup_lists();
	
	g_free(data);
	data = NULL;
}

/**
 * gems_communication_cleanup_lists:
 *
 * Clean all lists inside the #gems_communication_data structure. Destroys every
 * #gems_connection structure pointed the lists. 
 *
 * Returns: void
 */
void gems_communication_cleanup_lists()
{
	gems_communication_data* data = gems_get_data()->communication;
	
	if(data->connections != NULL)
	{
		g_list_foreach(data->connections, (GFunc)gems_clear_gems_connection, NULL);
		g_list_free(data->connections);
		data->connections = NULL;
	}
	
	if(data->connecting != NULL)
	{
		g_list_foreach(data->connecting, (GFunc)gems_clear_gems_connection, NULL);
		g_list_free(data->connecting);
		data->connecting = NULL;
	}
	
	if(data->rejected != NULL)
	{
		g_list_foreach(data->rejected, (GFunc)gems_clear_gems_connection, NULL);
		g_list_free(data->rejected);
		data->rejected = NULL;
	}
}

/**
 * gems_communication_search_jammos:
 *
 * Search new JamMos from environment. This function is added to Glib main loop
 * and is executed periodically. Requests a list of devices having "JamMo"
 * service enabled from PeerHood. If the list returned by PeerHood library is 
 * NULL the connection to PeerHood is determined to be closed and function 
 * gems_communication_peerhood_close_detected() is called to enable the probing
 * of Peerhood. When the list is not empty or null a connection is attempted
 * to be established to every new device. The connection will be attempted to
 * be establish with gems_communication_establish_connection() if the device is
 * not found from any of the internal lists, function 
 * gems_communication_already_connected() is used for checking.
 *
 * Returns: Integer values 0 (FALSE) or 1 (TRUE), 0 in case of errors when the
 * function is removed from the Glib main loop. Triggering errors are: data 
 * components not initialized or set, or the connection to PeerHood is lost.
 */
gint gems_communication_search_jammos()
{
	gchar* logmsg = NULL;
	gems_components* data = gems_get_data();
	
	if(data == NULL) return FALSE;
	if(gems_get_peerhood() == NULL) return FALSE; // no PeerHood = no network
	
	// Get surrounding devices with service JAMMO_SERVICE_NAME 
	TDeviceList* list = ph_c_get_devicelist_with_services(gems_get_peerhood(),JAMMO_SERVICE_NAME);
	
	// Got a NULL reference - error with PeerHood -> stop it
	if(list == NULL)
	{
		logmsg = g_strdup_printf("gems_communication_search_jammos: connection to PeerHood daemon was lost.");
		cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
		gems_communication_peerhood_close_detected();
		return FALSE;
	}
	
	if(ph_c_devicelist_is_empty(list) == FALSE)
	{
		// Initialize wrapper for iterator for list
		DeviceIterator* iterator = ph_c_new_device_iterator(list);
		
		if(iterator == NULL)
		{
			ph_c_delete_devicelist(list);
			return TRUE;
		}

		// Go through list
		for( ; ph_c_device_iterator_is_last(iterator,list) != TRUE; ph_c_device_iterator_next_iterator(iterator))
		{
			// Get the device
			MAbstractDevice* dev = ph_c_device_iterator_get_device(iterator);
			
			// Through WLAN and has peerhood? - all devices in the list should have ph enabled
			if((strcmp(ph_c_device_get_prototype(dev),"wlan-base") == 0) && (ph_c_device_has_peerhood(dev) == TRUE))
			{
				if(gems_communication_already_connected(ph_c_device_get_checksum(dev)) == NOT_IN_LIST)
				{
					if(gems_communication_establish_connection(dev) == TRUE)
					{
							logmsg = g_strdup_printf("gems_communication_search_jammos: Connected to new JamMo enabled device %s/%u",
								ph_c_device_get_name(dev),ph_c_device_get_checksum(dev));
							cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
					}
				}
			}
		}
		// Delete the iterator
		ph_c_delete_device_iterator(iterator);
	}
	// Delete the list of devices
	ph_c_delete_devicelist(list);
	
	return TRUE;
}

/**
 * gems_communication_establish_connection:
 * @device: Device to establish connection to, a pointer to structure which
 * points to the actual C++ object that is handled by PeerHood library.
 *
 * Attempts to establish connection to given @device if there aren't any errors
 * preventing the connection, uses gems_connection_error_attempt_connection() 
 * to check. When connection establishment is succesfull the connection will
 * be added to initialized connections list in #gems_communication_data ,
 * otherwise the checksum of the device will be added to connection errorlist
 * and the connection is disconnected and deleted. The #jammo_state will be set
 * to %JAMMO_WAITING_REQUEST after succesfull send of %CONNECT_REQ message.
 *
 * Returns: a #gboolean , TRUE when connection was established, FALSE
 * otherwise.
 */
gboolean gems_communication_establish_connection(MAbstractDevice* device)
{
	gchar* logmsg = NULL;
	gboolean rval = FALSE;
	gems_components* data = gems_get_data();
	
	MAbstractConnection* conn = NULL;

	if(gems_connection_error_attempt_connection(data->connection_errorlist,ph_c_device_get_checksum(device)) == TRUE)
	{
		if((conn = ph_c_connect_remoteservice(gems_get_peerhood(), device, JAMMO_SERVICE_NAME)) != NULL)
		{
			// Connection success, remove from errors. If no such device in the list, nothing is done
			data->connection_errorlist = gems_connection_error_remove(data->connection_errorlist,ph_c_device_get_checksum(device)); 
			
			gems_message* msg = gems_create_message_service_jammo(CONNECT_REQ);

			if(gems_communication_write_data(ph_c_connection_get_fd(conn),msg) != msg->length)
			{
				logmsg = g_strdup_printf("gems_communication_establish_connection: Error writing CONNECT_REQ to JamMo service on %s/%u",
					ph_c_connection_get_remote_address(conn),ph_c_connection_get_device_checksum(conn));
				cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
			
				data->connection_errorlist = gems_connection_error_add(data->connection_errorlist,ph_c_device_get_checksum(device), ERROR_CONNECTION);
				ph_c_connection_delete(conn); // disconnects and deletes
				conn = NULL;
				rval = FALSE;
			}
			else
			{
				logmsg = g_strdup_printf("gems_communication_establish_connection: Wrote CONNECT_REQ to JamMo service in %s",
					ph_c_connection_get_remote_address(conn));
				cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
			
				logmsg = g_strdup_printf("bytes: %d [%d|%d|%d]", msg->length, ntohs(*(guint16*)&msg->message[0]),ntohl(*(guint32*)&msg->message[2]),ntohs(*(guint16*)&msg->message[6]));
				cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
			
				// New item in JAMMO_WAITING_REQUEST state
				gems_connection* new_item = gems_new_gems_connection(conn, 0, 0, JAMMO_WAITING_REQUEST, 0);

				if(ph_c_device_has_service(device, MENTOR_SERVICE_NAME)==TRUE)
				{
					// No connection to teacher
					if(!gems_teacher_connection_is_connected())	gems_get_data()->teacher_connection->connection=new_item;
					// connection to teacher exists
					else
					{
						cem_add_to_log("Teacher device tried to connect when connection to a teacher already exists", J_LOG_DEBUG);
						data->connection_errorlist = gems_connection_error_add(data->connection_errorlist,ph_c_device_get_checksum(device), ERROR_CONNECTION);
						gems_clear_gems_connection(new_item);
						return rval;
					}
				}

				data->communication->connecting = g_list_append(data->communication->connecting, new_item);

				rval = TRUE;
			}
			gems_clear_message(msg);
			return rval;
		}
		else
		{
			data->connection_errorlist = gems_connection_error_add(data->connection_errorlist,ph_c_device_get_checksum(device), ERROR_CONNECTION);
			return rval; // by default rval = FALSE
		}
	}
	else
	{
		//printf("gems_communication_establish_connection: timer has not passed");
		return rval; // by default rval = FALSE
	}
}

/**
 * gems_communication_retrieve_profiles:
 *
 * Requests the public profile from surrounding devices. A timeout function 
 * executed periodically by the Glib main loop. The request is sent to
 * connected devices which are in %JAMMO_CONNECTED_AUTH #jammo_state if the 
 * public profile is not received yet. The request is sent using a timer with
 * interval defined in %PROFILE_REQUEST_INTERVAL if the profile isn't received.
 * In case of errorous write the connection is disconnected to the particular
 * device.
 *
 * Returns: Integer value 0 (FALSE) or 1 (TRUE), 0 when errors or the function
 * is required to be removed from Glib main loop.
 */
gint gems_communication_retrieve_profiles()
{
	gems_components* data = gems_get_data();
	
	if(data == NULL) return FALSE;
	if(gems_get_peerhood() == NULL) return FALSE;
	if(data->communication == NULL) return FALSE;
	
	gboolean cleanup_required = FALSE;
	gems_connection* element = NULL;
	gchar* logmsg = NULL;
	
	if (data->communication->connections != NULL)	
	{
		GList* iter = NULL;
		// Go through list
		for(iter = g_list_first(data->communication->connections); iter; iter = g_list_next(iter))
		{
			element = (gems_connection*)iter->data;
			// If we're already connected we can send profile request
			if(element != NULL)
			{
				// Connected and authenticated & no profile requested
				if(element->connection_state == JAMMO_CONNECTED_AUTH && element->profile == NULL)
				{
					// Request already done -> timer should have been set
					if((element->profile_requests > 0) && (g_timer_elapsed(element->profile_request_timer,NULL) < PROFILE_REQUEST_INTERVAL)) continue;

					//printf("gems_communication_retrieve_profiles: sending GET_PROFILE_PUB (%f)",g_timer_elapsed(element->profile_request_timer,NULL));

					gems_message* request = gems_create_message_profile_request(GET_PROFILE_PUB);

					if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, request) == FALSE)
					{
						logmsg = g_strdup_printf("gems_communication_retrieve_profiles: cannot write GET_PROFILE_PUB to %s/%u",
							ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
						cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						ph_c_connection_disconnect(element->connection);
						cleanup_required = TRUE;
					}
					else
					{
						/*printf("gems_communication_retrieve_profiles: wrote GET_PROFILE_PUB [%d|%d|%d|%d]",
							ntohs(*(gint16*)&request->message[0]),
							ntohl(*(gint32*)&request->message[2]),
							ntohs(*(gint16*)&request->message[6]),
							ntohs(*(gint16*)&request->message[8]));*/
						element->profile_requests += 1;
						g_timer_start(element->profile_request_timer);
					}

					gems_clear_message(request);
				}
			}
		}
	}
	
	if(cleanup_required == TRUE) data->communication->connections = gems_cleanup_connections_from_list(data->communication->connections);
	
	return TRUE;
}

/**
 * gems_communication_request_groups:
 * 
 * Request group info from connected devices. A timeout function executed
 * periodically by the Glib main loop. Sends group information requests to
 * every connected (in %JAMMO_CONNECTED_AUTH #jammo_state) device with interval
 * defined in %GROUP_REQUEST_INTERVAL . In case of errorous write the 
 * connection is disconnected to the particular device.
 *
 * Returns: Integer value 0 (FALSE) or 1 (TRUE), 0 when errors or the function
 * is required to be removed from Glib main loop.
 */
gint gems_communication_request_groups()
{
	gems_components* data = gems_get_data();

	if(data == NULL) return FALSE;
	if(gems_get_peerhood() == NULL) return FALSE;
	if(data->communication == NULL) return FALSE;
	
	gboolean cleanup_required = FALSE;
	gems_connection* element = NULL;
	gchar* logmsg = NULL;
		
	// List exists
	if (data->communication->connections != NULL)	
	{
		GList* iter = NULL;
		// Go through list
		for(iter = g_list_first(data->communication->connections); iter; iter = g_list_next(iter))
		{
			element = (gems_connection*)iter->data;

			if(element != NULL)
			{
				// Connected and authenticated & profile is requested
				if(element->connection_state == JAMMO_CONNECTED_AUTH && element->profile != NULL)
				{
					// Do not send the request to group members
					if(gems_group_is_in_group(element->profile->id) == FALSE)
					{
						// Request already done -> timer should have been set
						if(g_timer_elapsed(element->group_request_timer,NULL) < GROUP_REQUEST_INTERVAL) continue;

						//printf("gems_communication_request_groups: sending REQUEST_GROUP_INFO (%f)",g_timer_elapsed(element->group_request_timer,NULL));

						gems_message* request = gems_create_message_group_management_notify(REQUEST_GROUP_INFO,0,0);

						if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, request) == FALSE)
						{
							logmsg = g_strdup_printf("gems_communication_request_groups: cannot write REQUEST_GROUP_INFO to %s/%u",
								ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
							cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
							ph_c_connection_disconnect(element->connection);
							cleanup_required = TRUE;
						}
						else
						{
							/*printf("gems_communication_request_groups: wrote REQUEST_GROUP_INFO [%d|%d|%d]",
								ntohs(*(gint16*)&request->message[0]),
								ntohl(*(gint32*)&request->message[2]),
								ntohs(*(gint16*)&request->message[6]));*/
							g_timer_start(element->group_request_timer);
						}

						gems_clear_message(request);
					}
				}
			}
		}
	}
	
	if(cleanup_required == TRUE) data->communication->connections = gems_cleanup_connections_from_list(data->communication->connections);
	
	return TRUE;
}

/**
 * gems_communication_advert_group:
 *
 * Advert group info to connected devices. A timeout function executed
 * periodically by the Glib main loop. Sends group advertizements to
 * every connected (in %JAMMO_CONNECTED_AUTH #jammo_state) device with interval
 * defined in %GROUP_ADVERT_INTERVAL .
 *
 * Returns: Integer value 0 (FALSE) or 1 (TRUE), 0 when errors or the function
 * is required to be removed from Glib main loop.
 */
gint gems_communication_advert_group()
{
	gems_components* data = gems_get_data();
	
	if(data == NULL) return FALSE;
	if(gems_get_peerhood() == NULL) return FALSE;
	if(data->communication == NULL) return FALSE;
	
	gchar* logmsg = NULL;
	
	// Group not enabled
	if(data->service_group->group_info->id == NOT_ACTIVE)
	{
		// Destroy the timer and remove self from glib main loop
		if(data->service_group->group_info->group_advert_timer != NULL)
		{
			g_timer_destroy(data->service_group->group_info->group_advert_timer);
			data->service_group->group_info->group_advert_timer = NULL;
		}
		return FALSE;
	}
	// Group locked - do not advert but do not remove this since group is still active
	else if(data->service_group->group_info->is_locked) return TRUE;
	else
	{
		// Was set up
		if(data->service_group->group_info->group_advert_timer != NULL)
		{
			// Enough time has not passed from previous advert -> return
			if(g_timer_elapsed(data->service_group->group_info->group_advert_timer,NULL) < GROUP_ADVERT_INTERVAL) return TRUE;
		}
		else data->service_group->group_info->group_advert_timer = g_timer_new(); // Start the timer
		
		gems_message* msg = gems_create_message_group_management_group_info(OFFER_GROUP);
		
		// TODO after applying GObject to get the necessary data (gems_components) change to use g_list_foreach
		// Send message to all
		GList* iterator = NULL;
		
		for(iterator = g_list_first(data->communication->connections); iterator ; iterator = g_list_next(iterator))
		{
			gems_connection* element = (gems_connection*)iterator->data;
			
			if(element->profile != NULL)
			{
				if(gems_group_is_in_group(element->profile->id) == FALSE)
				{					
					if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
					{
						logmsg = g_strdup_printf("gems_communication_advert_group: cannot send OFFER_GROUP to device %u with user %u",
							ph_c_connection_get_device_checksum(element->connection), element->profile->id);
						cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						// TODO add error handling - add connection error to errorlist
					}
					// TODO add last action for this user, timestamp etc?
				}
			}
		}
		
		g_timer_start(data->service_group->group_info->group_advert_timer); // Set new time
		
		gems_clear_message(msg);
	}
	
	return TRUE;
}

/**
 * gems_communication_sanitize_grouplist:
 *
 * Function for removing old group information from the group list. A timeout
 * function executed periodically by Glib main loop. Removes group information
 * from the list if it is not updated for %GROUP_REMOVE_LIMIT seconds.
 *
 * Returns: 1 (TRUE) always.
 */
gint gems_communication_sanitize_grouplist()
{
	gems_components* data = gems_get_data();
	if(data == NULL) return FALSE;
	if(gems_get_peerhood() == NULL) return FALSE;
	if(data->communication == NULL) return FALSE;
	if(data->service_group == NULL) return FALSE;
	
	GList* iterator = g_list_first(data->service_group->other_groups);
	
	while(iterator)
	{
		gems_group_info* info = (gems_group_info*)iterator->data;
		
		if(info == NULL) continue;
		
		if(info->group_advert_timer != NULL)
		{
			if(g_timer_elapsed(info->group_advert_timer,NULL) > GROUP_REMOVE_LIMIT)
			{
				gchar* logmsg = g_strdup_printf("Group %u is past remove limit (%f elapsed), removing.",info->id, g_timer_elapsed(info->group_advert_timer,NULL));
				cem_add_to_log(logmsg, J_LOG_DEBUG);
				g_free(logmsg);
				
				GList* tempiter = iterator;
				iterator = g_list_next(iterator);
				
				data->service_group->other_groups = g_list_remove_link(data->service_group->other_groups,tempiter);
				gems_clear_group_info(info);
				g_list_free_1(tempiter);
				
				continue;
			}
		}
		
		iterator = g_list_next(iterator);
	}

	return TRUE;
}

/**
 * gems_communication_process_connecting:
 *
 * Process connections intialized to other devices. A timeout function executed
 * periodically by the Glib main loop. Uses the "JamMo" service connection
 * processing function (gems_service_jammo_process_list()) to execute the
 * handshake procedure between connecting devices.
 *
 * Returns: Integer value 0 (FALSE) or 1 (TRUE), 0 when errors or the function
 * is required to be removed from Glib main loop.
 */
gint gems_communication_process_connecting()
{
	gems_components* data = gems_get_data();
	if(data == NULL) return FALSE;
	if(gems_get_peerhood() == NULL) return FALSE;
	if(data->communication == NULL) return FALSE;
	
	// Check connecting list
	data->communication->connecting = gems_service_jammo_process_list(data->communication->connecting, FALSE);
	
	return TRUE;
}

/**
 * gems_communication_process_rejected:
 *
 * Process rejected connections list. A timeout function executed periodically
 * by the Glib main loop. Calls gems_process_rejected() to check the list of
 * rejected devices.
 *
 * Returns: Integer value 0 (FALSE) or 1 (TRUE), 0 when errors or the function
 * is required to be removed from Glib main loop.
 */
gint gems_communication_process_rejected()
{
	gems_components* data = gems_get_data();
	if(data == NULL) return FALSE;
	if(gems_get_peerhood() == NULL) return FALSE;
	if(data->communication == NULL) return FALSE;
	
	// Check rejected list
	data->communication->rejected = gems_process_rejected(data->communication->rejected);
	
	return TRUE;
}

/**
 * print_list:
 * @list: list to print, must contain only pointers to #gems_connection structs
 *
 * Print the contents of given list to stdout. List must contain only pointers
 * to #gems_connection structs. Used only for debug.
 *
 * Returns: void
 *
 * Deprecated: 0.5: Do not use.
 */
void print_list(GList* list)
{
	GList* iterator = NULL;
	
	if(list == NULL)
	{
		printf("LIST NULL\n");
		return;
	}
	
	if(g_list_length(list) == 0)
	{
		printf("LIST EMPTY\n");
	}
	// Search the list
	for(iterator = g_list_first(list); iterator; iterator = g_list_next(iterator))
	{
		gems_connection* element = (gems_connection*)iterator->data;
		
		if(element == NULL) continue;
		
		printf("%d: state: %d (data in buffer: %d bytes) | %s / %d\n",
			g_list_position(list,iterator), element->connection_state, gems_connection_partial_data_amount(element),
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		if(element->profile != NULL) printf("\tProfile: %u:%s, %dyo avatarid: %u\n",
			element->profile->id,element->profile->username,element->profile->age,element->profile->avatarid);
	}
}

/**
 * print_group:
 * @info: group info structure to print
 *
 * Print the contents of given #gems_group_info to stdout. Used only for debug.
 *
 * Returns: void
 *
 * Deprecated: 0.6: Do not use.
 */
void print_group(gems_group_info* info)
{
	printf("gid:\t%u\nowner:\t%u\ntype:\t%d\ntheme:\t%d\nsize:\t%d\nspaces\t%d\nlocked:\t%s\n",
		info->id,
		info->owner,
		info->type,
		info->theme,
		info->size,
		info->spaces,
		(info->is_locked ? "yes" : "no" ));
	if(info->group_advert_timer != NULL) printf("advert:\t%f s ago\n",g_timer_elapsed(info->group_advert_timer,NULL));
	switch(info->own_state)
	{
		case NOT_ACTIVE:
			printf("state:\tNOT_ACTIVE\n");
			break;
		case IN_GROUP:
			printf("state:\tIN_GROUP\n");
			break;
		case JOINING:
			printf("state:\tJOINING\n");
			break;
		case LEAVING:
			printf("state:\tLEAVING\n");
			break;
		default:
			break;
	}	
	guint32* members = gems_group_get_group_members();
	gint i = 0;
	printf("Peers:\n");
	for(i = 0; i < (GROUP_MAX_SIZE - 1); i++)	printf("(%d)\t%u\n",i,members[i]);
}

/**
 * gems_communication_already_connected:
 * @checksum: Checksum of the device to search from lists.
 *
 * Check if the given device with @checksum is already in some list. Uses
 * gems_communication_search_from_list() to check every possible list which
 * contains pointers to #gems_connection structs.
 *
 * Returns: the result of the list search as #connection_search enum value.
 */
connection_search gems_communication_already_connected(guint checksum)
{
	gems_components* data = gems_get_data();
	
	if(data == NULL)
	{
		gchar* logmsg = g_strdup_printf("gems_communication_already_connected: data == NULL");
		cem_add_to_log(logmsg, J_LOG_ERROR);
		g_free(logmsg);
		return NOT_IN_LIST;	
	}

	//printf("gems_communication_already_connected: going through lists to search %d", checksum);
	
	if(gems_communication_search_from_list(data->initialized_connections, checksum) == TRUE) return IN_INITIALIZED_LIST;
	if(gems_communication_search_from_list(data->service_jammo->connections, checksum) == TRUE) return IN_JAMMO_SERVICE_LIST;
	if(gems_communication_search_from_list(data->communication->connecting, checksum) == TRUE) return IN_CONNECTING_LIST;
	if(gems_communication_search_from_list(data->communication->connections, checksum) == TRUE) return IN_CONNECTED_LIST;
	if(gems_communication_search_from_list(data->communication->rejected, checksum) == TRUE) return IN_REJECTED_LIST;
	
	//printf("gems_communication_already_connected: No match %d", checksum);
	return NOT_IN_LIST;
}

/**
 * gems_communication_search_from_list:
 * @list: the list to search, must contain only pointers to #gems_connection
 * structs
 * @checksum: Device checksum to search from given list
 *
 * Search a connection with given @checksum from the given @list.
 *
 * Returns: a #gboolean, TRUE if found, FALSE if not found or list is empty.
 */
gboolean gems_communication_search_from_list(GList* list, guint checksum)
{
	GList* iterator = NULL;
	if(!list) return FALSE; // No list given
	if(g_list_length(list) == 0) return FALSE;
	// Search the list
	for(iterator = g_list_first(list); iterator; iterator = g_list_next(iterator))
	{
		gems_connection* element = (gems_connection*)iterator->data;
		
		if(element == NULL) continue;
		
		// Match - return TRUE
		if(ph_c_connection_get_device_checksum(element->connection) == checksum )
		{
			//printf("gems_communication_search_from_list: match found for %d!", checksum);
			return TRUE;
		}
	}
	return FALSE;
}

/**
 * gems_communication_get_connection:
 * @list: the list to search, must contain only pointers to #gems_connection
 * structs
 * @checksum: Device checksum to search from given list
 *
 * Search a connection with given @checksum from the given @list and return it.
 *
 * Returns: a pointer to #gems_connection structure if found, NULL otherwise.
 */
gems_connection* gems_communication_get_connection(GList* list, guint checksum)
{
	GList* iterator = NULL;
	if(!list) return NULL; // No list given
	if(g_list_length(list) == 0) return NULL; // empty
	
	// Search the list
	for(iterator = g_list_first(list); iterator; iterator = g_list_next(iterator))
	{
		gems_connection* element = (gems_connection*)iterator->data;
		
		if(element == NULL) continue;
		
		// Match - return element
		if(ph_c_connection_get_device_checksum(element->connection) == checksum ) return element;
	}
	return NULL; // Not found
}

/**
 * gems_communication_get_connection_with_userid:
 * @list: the list to search, must contain only pointers to #gems_connection
 * structs
 * @userid: Id fof the user to search from given list
 *
 * Search a connection from the given @list where the user id in the public
 * profile (#gems_peer_profile) matches the given @userid. When found return a
 * pointer to that #gems_connection.
 *
 * Returns: a pointer to #gems_connection structure if found, NULL otherwise.
 */
gems_connection* gems_communication_get_connection_with_userid(GList* list, guint32 userid)
{
	GList* iterator = NULL;
	if(!list) return NULL; // No list given
	if(g_list_length(list) == 0) return NULL; // empty
	
	// Search the list
	for(iterator = g_list_first(list); iterator; iterator = g_list_next(iterator))
	{
		gems_connection* element = (gems_connection*)iterator->data;
		
		if(element == NULL) continue;
		
		// Profile is requested
		if(element->profile != NULL )
		{
			// Matching id
			if(element->profile->id == userid) return element;
		}
	}
	return NULL; // Not found
}

/**
 * gems_communication_process_new:
 * @element: New connection element to add.
 *
 * Add new #gems_connection to list of established connections.
 *
 * Returns: void
 */
void gems_communication_process_new(gems_connection* element)
{
	if(element)
	{
		gems_communication_data* data = gems_get_data()->communication;
		data->connections = g_list_append(data->connections, element);
		gchar* logmsg = g_strdup_printf("gems_communication_process_new: added new (%s/%u)",
			ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
		cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
}

/**
 * gems_communication_process_new_rejected:
 * @element: New rejected connection element to add.
 *
 * Add new rejected connection (#gems_connection) to list of established 
 * connections.
 *
 * Returns: void
 */
void gems_communication_process_new_rejected(gems_connection* element)
{
	if(element)
		{
		gems_communication_data* data = gems_get_data()->communication;
		gems_connection_set_action(element); // Record the time this connection was rejected
		data->rejected = g_list_append(data->rejected, element);
		gchar* logmsg = g_strdup_printf("gems_communication_process_new_rejected: added new element (%s/%u) to rejected list",
			ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
		cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
}

/**
 * gems_communiaction_write_encrypted_data:
 * @type: Type of the encrypted message, can be either %JAMMO_PACKET_PRIVATE or
 * %JAMMO_PACKET_ADMIN (%JAMMO_PACKET_GROUP not implemented).
 * @element: The connection element where data is written to.
 * @data: The message to be written, length and content must be appropriately
 * set.
 *
 * Write given message @data to connection @element inside encrypted envelope.
 * The envelope is created using the given message @data using 
 * gems_security_create_envelope() . If the full length of the encrypted
 * message cannot be written FALSE is returned. The message @data is not
 * touched and must be free'd by the caller function. Write operation is 
 * performed by calling gems_communication_write_data() .
 *
 * Returns: a #gboolean, TRUE when the envelope was created and all data was
 * written. FALSE when either of the operations failed.
 */
gboolean gems_communication_write_encrypted_data(guint16 type, gems_connection* element, gems_message* data)
{
	gboolean success = TRUE;
	gems_message* envelope = gems_security_create_envelope(type, element, data);
	
	if(envelope)
	{
		if((gems_communication_write_data(ph_c_connection_get_fd(element->connection), envelope)) != envelope->length) success = FALSE;
		gems_clear_message(envelope);
	}
	else success = FALSE;
	
	return success;
}

/**
 * gems_communication_write_data:
 * @sock_fd: Socket where the data is written to
 * @data: Message to write
 *
 * Write given message (@data) to given socket (@sock_fd).
 *
 * Returns: Amount of bytes written, -1 in case of errors.
 */
gint gems_communication_write_data(gint sock_fd, gems_message* data)
{
	if(!data) return -1;
	if(sock_fd < 0) return -1;
	return write(sock_fd, (gchar*)data->message, data->length);
}

/**
 * gems_communication_read_data:
 * @sock_fd: Socket where to read from.
 * @data: Buffer to store read data, must be at least the length of @length
 * parameter, data is read as #gchar:s into buffer.
 * @length: Amount of data to read into @data.
 *
 * Read data from socket, place read bytes in @data. Data is read as #gchar:s.
 *
 * Returns: Amount of bytes read.
 */
gint gems_communication_read_data(gint sock_fd, void* data, gint length)
{
	if(!data || length == 0) return -1;
	if(sock_fd < 0) return -1;
	return read(sock_fd, (gchar*)data, length);
}
/**
 * gems_communication_process_connected:
 *
 * Processes all connections which have inbound data, reads the data in pieces
 * and forwards full packets for to gems_communication_process_data() for 
 * processing. The teacher connection (if exists) is checked first then other
 * devices are processed. A timeout function which is executed periodically by
 * Glib main loop, removes itself from the loop in case of errors with internal
 * data. Disconnects all connections which have either invalid header or the
 * data cannot be read from connection.
 *
 * Returns: Integer value 0 (FALSE) or 1 (TRUE), 0 when errors or the function
 * is required to be removed from Glib main loop.
 */
gint gems_communication_process_connected()
{
	gems_components* data = gems_get_data();
	gboolean cleanup_required = FALSE;
	gboolean teacher_cleanup_required = FALSE;
	gboolean do_teacher_check = FALSE;
	gems_connection* element = NULL;
	GList* iterator = NULL;
	gchar* logmsg = NULL;
	gint bytesread = -1;
	
	if(data == NULL) return FALSE;
	if(gems_get_peerhood() == NULL) return FALSE;
	if(data->communication == NULL) return FALSE;
	
	// Check if we have a teacher connection
	if(gems_teacher_connection_is_connected()) do_teacher_check = FALSE;
	
	iterator = g_list_first(data->communication->connections);
	while(iterator || do_teacher_check)
	{
		// If we have a active connection to teacher, check for incoming data
		if(do_teacher_check) element = data->teacher_connection->connection;
		else element = (gems_connection*)iterator->data;
		
		if(!element)
		{
			if(do_teacher_check) do_teacher_check = FALSE;
			else iterator = g_list_next(iterator);
			continue;
		}
		
		// TODO implement AA service
		if(element->connection_state == JAMMO_CONNECTED_NO_AUTH) element->connection_state = JAMMO_CONNECTED_AUTH;
		
		/* TODO modify to read the rest of the packet IF there is data available,
		   which might speed up the handling process */
		if((ph_c_connection_is_connected(element->connection)) == TRUE && 
			(ph_c_connection_has_data(element->connection) == TRUE))
		{		
			// Partial data
			if(gems_connection_partial_data_amount(element) > 0)
			{
				// If 2 bytes = Service id/packet type read -> read length
				if(gems_connection_partial_data_amount(element) == sizeof(guint16))
				{
					guint32 packetlength = 0;
					
					switch ((bytesread = gems_communication_read_data(ph_c_connection_get_fd(element->connection), &packetlength, sizeof(guint32))))
					{
						/* CORRECT AMOUNT */
						case sizeof(guint32):
							//printf("gems_communication_process_list: read length: %d", ntohl(packetlength));
							// Check that length is in limits
							if(ntohl(packetlength) > (JAMMO_NETWORK_BUFFER_MAX_SIZE - sizeof(guint32) - sizeof(guint16)))
							{
								logmsg = g_strdup_printf("gems_communication_process_list: packet is too big (%u bytes), disconnecting.",ntohl(packetlength));
								cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
								g_free(logmsg);
								ph_c_connection_disconnect(element->connection);
								cleanup_required = TRUE;
							}
							else gems_connection_add_32(element,packetlength); // Add length
							break;
						case 0:
						case -1:
						default:
							logmsg = g_strdup_printf("gems_communication_process_list: length not read correctly (bytesread = %d). Disconnecting", bytesread);
							cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
							ph_c_connection_disconnect(element->connection);
							if(do_teacher_check) teacher_cleanup_required = TRUE;
							else cleanup_required = TRUE;
							break;
					}
				}
				
				// If 6 bytes read = Service id and length read && Not authenticated -> read command
				else if(gems_connection_partial_data_amount(element) == (sizeof(guint16) + sizeof(guint32)) &&
					(element->connection_state == JAMMO_CONNECTED_NO_AUTH))
				{
					guint16 command = 0;
					
					switch((bytesread = gems_communication_read_data(ph_c_connection_get_fd(element->connection), &command, sizeof(guint16))))
					{
						case sizeof(guint16):
							//printf("gems_communication_process_list : command: %d, user in state: %d", ntohs(command), element->connection_state);
							gems_connection_add_16(element,command);
							break;

						case 0:
						case -1:
						default:
							logmsg = g_strdup_printf("gems_communication_process_list : command not read correctly (bytesread = %d). Disconnecting", bytesread);
							cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
							g_free(logmsg);
							
							ph_c_connection_disconnect(element->connection);
							if(do_teacher_check) teacher_cleanup_required = TRUE;
							else cleanup_required = TRUE;
							break;
					}
				}
				
				// Authenticated, read 6 or more bytes -> read rest
				else if(element->connection_state == JAMMO_CONNECTED_AUTH &&
					(gems_connection_partial_data_amount(element) >= (sizeof(guint16) + sizeof(guint32))))
				{
					// remaining = length - read bytes
					guint remaining = ntohl(gems_connection_get_32(element, sizeof(guint16))) - gems_connection_partial_data_amount(element);
					gchar remainingdata[remaining];
					memset(&remainingdata,'\0', remaining);
					
					// All read?
					if((bytesread = gems_communication_read_data(ph_c_connection_get_fd(element->connection), &remainingdata, remaining)) == remaining)
					{
						//printf("gems_communication_process_list : data fully read, read %d bytes.", bytesread);
						gems_connection_add_data(element,remainingdata,bytesread);
					}
					// Partial read
					else if((bytesread > 0) && (bytesread < remaining))
					{
						//printf("gems_communication_process_list : hash not fully read, read %d bytes.", bytesread);
						gems_connection_add_data(element,remainingdata,bytesread);
						/*printf("gems_communication_process_list : bytes remaining = %d",
							ntohl(gems_connection_get_32(element,sizeof(gint16))) - gems_connection_partial_data_amount(element));*/
					}
					// Errorous read
					else
					{
						logmsg = g_strdup_printf("gems_communication_process_list : error reading rest of data (%d), disconnecting", bytesread);
						cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						
						ph_c_connection_disconnect(element->connection);
						if(do_teacher_check) teacher_cleanup_required = TRUE;
						else cleanup_required = TRUE;
					}
				}
				
				// All read? Data read = length parameter in buffer[2]
				if(gems_connection_partial_data_amount(element) == ntohl(gems_connection_get_32(element,sizeof(guint16))))
				{
					//printf("gems_communication_process_list: full packet received (%d bytes).", ntohl(gems_connection_get_32(element,sizeof(gint16))));
					if(gems_communication_process_data(element) == FALSE)
					{
						ph_c_connection_disconnect(element->connection);
						if(do_teacher_check) teacher_cleanup_required = TRUE;
						else cleanup_required = TRUE;
					}
				}
				
				if(gems_connection_partial_data_amount(element) > ntohl(gems_connection_get_32(element,sizeof(guint16))))
				{					
					logmsg = g_strdup_printf("gems_communication_process_list: more than full packet! ERROR?");
					cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
					
					ph_c_connection_disconnect(element->connection);
					if(do_teacher_check) teacher_cleanup_required = TRUE;
					else cleanup_required = TRUE;
				}
			}
			
			// No partial data - read first 16 bits
			else
			{
				guint16 identifier = 0;
			
				switch((bytesread = gems_communication_read_data(ph_c_connection_get_fd(element->connection), &identifier, sizeof(guint16))))
				{
					/* CORRECT AMOUNT */
					case sizeof(guint16):
						// Authenticated - expect a encrypted envelope
						if(element->connection_state == JAMMO_CONNECTED_AUTH)
						{
							switch(ntohs(identifier))
							{
								// "Private" message for all other than group work
								case JAMMO_PACKET_PRIVATE:
									gems_connection_add_16(element,identifier);
									break;
								// Group/pair work message
								case JAMMO_PACKET_GROUP:
									gems_connection_add_16(element,identifier);
									break;
								case JAMMO_PACKET_ADMIN:
									gems_connection_add_16(element,identifier);
									break;
								case ERROR_MSG:
									gems_connection_add_16(element,identifier);
									break;
								default:
									/* After a device is authenticated everything should be sent as encrypted data inside envelope
									 * or as an error message, handle errorous packet type by disconnecting the connection */
									logmsg = g_strdup_printf("gems_communication_process_list: invalid packet type \"%d\" from device %u.",
										ntohs(identifier), ph_c_connection_get_device_checksum(element->connection));
									cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
									g_free(logmsg);
									ph_c_connection_disconnect(element->connection);
									cleanup_required = TRUE;
									break;
							}
						}
						else if(element->connection_state == JAMMO_CONNECTED_NO_AUTH)
						{
							if(ntohs(identifier) != AA_SERVICE_ID)
							{
								logmsg = g_strdup_printf("gems_communication_process_list: invalid service id \"%d\" user in state %d",
									ntohs(identifier), element->connection_state);
								cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
								g_free(logmsg);
								
								gems_message* err = gems_create_error_message(ERROR_INVALID_SERVICE_ID_TYPE, AA_SERVICE_ID);
								if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
								{
									logmsg = g_strdup_printf("gems_service_jammo_process_connections: cannot write ERROR_INVALID_SERVICE_ID (%s/%u)",
										ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
									cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
									g_free(logmsg);
								}
								gems_clear_message(err);
								ph_c_connection_disconnect(element->connection);
								if(do_teacher_check) teacher_cleanup_required = TRUE;
								else cleanup_required = TRUE;
							}
							// TODO check that AA service is running
						}
						break;
					
					case 0:
					case -1:
					default:
						logmsg = g_strdup_printf("gems_communication_process_list: service id read error (%d) from %s/%u", bytesread,
							ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
						cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
						ph_c_connection_disconnect(element->connection);
						if(do_teacher_check) teacher_cleanup_required = TRUE;
						else cleanup_required = TRUE;
						break;
				}
			}
		}
		
		// Done once during this function execution
		if(do_teacher_check) do_teacher_check = FALSE;
		else iterator = g_list_next(iterator);
	}
	if(cleanup_required == TRUE) data->communication->connections = gems_cleanup_connections_from_list(data->communication->connections);
	if(teacher_cleanup_required) gems_teacher_connection_disconnected();
	songbank_file_sender_loop();
	return TRUE;
}

/**
 * gems_communication_process_data:
 * @element: connection which has a full packet waiting
 *
 * Processes the full packet waiting at the network buffer of connection
 * @element. First the packet is extracted from the envelope with
 * gems_security_extract_envelope() and the content is localized with 
 * gems_connection_localize_data_in_buffer(). If the service id matches to
 * certain handler the connection @element will be passed to that handler
 * for processing the message content. Packets with invalid service id will
 * be dropped, packets to services which are not enabled set the return value
 * to FALSE. Connection @element:s containing error messages are forwareded to
 * gems_communication_process_error(). The network buffer of the given @element
 * will be cleared after processing.
 *
 * Returns: TRUE when the data was in proper form, FALSE if invalid.
 */
gboolean gems_communication_process_data(gems_connection* element)
{
	gems_components* data = gems_get_data();
	gboolean success = TRUE;
	gems_message* err = NULL;
	gchar* logmsg = NULL;
	gems_teacher_connection* tc = NULL;
	gboolean teacher_message=FALSE;
	
	if(data == NULL) return FALSE;
	if(data->service_profile == NULL) return FALSE;
	if(element == NULL) return FALSE;
	
	// If not for AA_SERVICE_ID (state == JAMMO_CONNECTED_NO_AUTH) or ERROR, message is in envelope
	if(element->connection_state == JAMMO_CONNECTED_AUTH)
	{
		switch(ntohs(gems_connection_get_16(element,0)))
		{
			case ERROR_MSG:
				break;
			case JAMMO_PACKET_PRIVATE:
				/*printf("gems_communication_process_data: buffer contains: [%d|%d|<-hash->|%d|%d|%d]",
					ntohs(gems_connection_get_16(element,0)),
					ntohl(gems_connection_get_32(element,2)),
					ntohs(gems_connection_get_16(element,6+JAMMO_MESSAGE_DIGEST_SIZE+1)),
					ntohl(gems_connection_get_32(element,6+JAMMO_MESSAGE_DIGEST_SIZE+1+2)),
					ntohs(gems_connection_get_16(element,6+JAMMO_MESSAGE_DIGEST_SIZE+1+6)));*/
		
				// TODO react to security rval
				switch(gems_security_extract_envelope(element))
				{
					case SECURITY_OK:
						break;
					case SECURITY_VERIFY_FAILED:
						break;
					case SECURITY_CORRUPT_MESSAGE:
						break;
					default:
						break;
				}
			// Decrypt content
			// Verify hash
			// Check content service id
			// Check connection state?
				break;
			case JAMMO_PACKET_GROUP:
				break;
			case JAMMO_PACKET_ADMIN:
				switch(gems_security_extract_envelope(element))
				{
					case SECURITY_OK:
						break;
					case SECURITY_VERIFY_FAILED:
						break;
					case SECURITY_CORRUPT_MESSAGE:
						break;
					default:
						break;
				}
				break;
			default:
				logmsg = g_strdup_printf("gems_communication_process_data: unknown envelope id (%d) from authenticated user",ntohs(gems_connection_get_16(element,0)));
				cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
				// TODO create error message and send
				break;
		}
	}
	
	gems_connection_localize_data_in_buffer(element);
	
	logmsg = g_strdup_printf("gems_communication_process_data: element contains: [%d|%u|%d] from %s/%u",
		gems_connection_get_16(element,0),
		gems_connection_get_32(element,2),
		gems_connection_get_16(element,6),
		ph_c_connection_get_remote_address(element->connection),
		ph_c_connection_get_device_checksum(element->connection));
	cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
	
	switch(gems_connection_get_16(element,0))
	{
		case AA_SERVICE_ID:
			// Pass element to authentication for key exchange
			logmsg = g_strdup_printf("gems_communication_process_data: AA");
			cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
			
			err = gems_create_error_message(ERROR_SERVICE_NOT_RUNNING_TYPE, AA_SERVICE_ID);
			if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
			{
				logmsg = g_strdup_printf("gems_service_jammo_process_connections: cannot write ERROR_SERVICE_NOT_RUNNING to %s/%u",
					ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
				cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
			}
			gems_clear_message(err);
			success = FALSE; // TODO change to add a "invalid message" to errors
			break;
		case PROFILE_SERVICE_ID:
			if(gems_connection_get_16(element,sizeof(guint16)+sizeof(guint32)) == PROFILE_ENC)
			{
				// If the message cannot be processed it from some one else than teacher
				if(!gems_service_profile_process_profile_upload(element))
				{
					err = gems_create_error_message(ERROR_INVALID_ACCESS_TYPE, TEACHER_PROFILE_SERVICE_ID);
					if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
					{
						logmsg = g_strdup_printf("gems_service_jammo_process_connections: cannot write ERROR_INVALID_ACCESS_TYPE to %s/%u",
							ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
						cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
					}
					gems_clear_message(err);
					success = FALSE;
				}
			}
			else
			{
				// Check that service is running
				if(data->service_profile->enabled == TRUE) success = gems_service_profile_process_request(element);
				else
				{
					err = gems_create_error_message(ERROR_SERVICE_NOT_RUNNING_TYPE, PROFILE_SERVICE_ID);
					if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
					{
						logmsg = g_strdup_printf("gems_service_jammo_process_connections: cannot write ERROR_SERVICE_NOT_RUNNING to %s/%u",
							ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
						cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
						g_free(logmsg);
					}
					gems_clear_message(err);
					success = FALSE; // TODO change to add a "invalid message" to errors
				}
			}
			break;
		case GROUP_SERVICE_ID:
			tc = gems_get_teacher_connection();
			// Connection to teacher exists
			if(tc)
			{
				// Is connected
				if(gems_teacher_connection_is_connected())
				{
					// Message is coming from teacher
					if(ph_c_connection_get_device_checksum(tc->connection->connection) == ph_c_connection_get_device_checksum(element->connection))
					teacher_message=TRUE;
				}
			}
			// check that profile is requested! (or message from teacher)
			// TODO create a better system - request profile directly
			if(element->profile == NULL && !teacher_message) break;
			// Check that service is running
			if(data->service_group->enabled == TRUE) success = gems_service_group_process_request(element);
			else
			{
				err = gems_create_error_message(ERROR_SERVICE_NOT_RUNNING_TYPE, GROUP_SERVICE_ID);
				if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
				{
					logmsg = g_strdup_printf("gems_service_jammo_process_connections: cannot write ERROR_SERVICE_NOT_RUNNING to %s/%u",
						ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
				}
				gems_clear_message(err);
				success = FALSE; // TODO change to add a "invalid message" to errors
			}
			break;
		case COLLABORATION_SERVICE_ID:
			// Check that service is running
			if(data->service_collaboration->enabled == TRUE) success = gems_service_collaboration_process_request(element);
			else
			{
				err = gems_create_error_message(ERROR_SERVICE_NOT_RUNNING_TYPE, COLLABORATION_SERVICE_ID);
				if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
				{
					logmsg = g_strdup_printf("gems_service_jammo_process_connections: cannot write ERROR_SERVICE_NOT_RUNNING, to %s/%u",
						ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg, J_LOG_ERROR);
					g_free(logmsg);
				}
				gems_clear_message(err);
				success = FALSE; // TODO change to add a "invalid message" to errors
			}
			break;
		case CONTROL_SERVICE_ID:
			// Check that service is running
			if(data->service_control->enabled == TRUE) success = gems_service_control_process_request(element);
			else
			{
				err = gems_create_error_message(ERROR_SERVICE_NOT_RUNNING_TYPE, CONTROL_SERVICE_ID);
				if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
				{
					logmsg = g_strdup_printf("gems_service_jammo_process_connections: cannot write ERROR_SERVICE_NOT_RUNNING to %s/%u",
						ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg, J_LOG_ERROR);
					g_free(logmsg);
				}
				gems_clear_message(err);
				success = FALSE; // TODO change to add a "invalid message" to errors
			}
			
			break;
		case MENTOR_SERVICE_ID:
			// Check that service is running
			if(data->service_mentor->enabled == TRUE) success = gems_service_mentor_process_request(element);
			else
			{
				err = gems_create_error_message(ERROR_SERVICE_NOT_RUNNING_TYPE, MENTOR_SERVICE_ID);
				if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
				{
					logmsg = g_strdup_printf("gems_service_jammo_process_connections: cannot write ERROR_SERVICE_NOT_RUNNING to %s/%u",
						ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg, J_LOG_ERROR);
					g_free(logmsg);
				}
				gems_clear_message(err);
				success = FALSE; // TODO change to add a "invalid message" to errors
			}
			
			break;
		case SONGBANK_SERVICE_ID:
			// Check that service is running
			if(data->service_songbank->enabled == TRUE) success = gems_service_songbank_process_request(element);
			else
			{
				err = gems_create_error_message(ERROR_SERVICE_NOT_RUNNING_TYPE, SONGBANK_SERVICE_ID);
				if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
				{
					logmsg = g_strdup_printf("gems_service_jammo_process_connections: cannot write ERROR_SERVICE_NOT_RUNNING to %s/%u",
						ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg, J_LOG_ERROR);
					g_free(logmsg);
				}
				gems_clear_message(err);
				success = FALSE; // TODO change to add a "invalid message" to errors
			}
			break;
		/*case DATATRANSFER_SERVICE_ID:
			break;
		case PROBE_SERVICE_ID:
			break;*/
		case ERROR_MSG:
			logmsg = g_strdup_printf("gems_communication_process_data: error message, type = %d, message: %s",
				gems_connection_get_16(element,8), gems_connection_get_char(element,10));
			cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
			gems_communication_process_error(element);
			break;
		default:
			logmsg = g_strdup_printf("gems_communication_process_data: invalid service id \"%d\" user in state %d",
				gems_connection_get_16(element,0), element->connection_state);
			cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
			
			err = gems_create_error_message(ERROR_INVALID_SERVICE_ID_TYPE, JAMMO_SERVICE_ID);
			if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),err) != err->length)
			{
				logmsg = g_strdup_printf("gems_service_jammo_process_connections: cannot write ERROR_INVALID_SERVICE_ID to %s/%u",
					ph_c_connection_get_remote_address(element->connection),ph_c_connection_get_device_checksum(element->connection));
				cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
			}
			gems_clear_message(err);
			//success = FALSE;
			break;
	}

	// Clear buffer
	gems_connection_clear_buffer(element);
	
	return success;
}

/**
 * gems_communication_process_error:
 * @element: Connection element containing error message in its network buffer
 *
 * Process the error message in the connection @element network buffer.
 *
 * Returns: void
 */
void gems_communication_process_error(gems_connection* element)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	gems_connection* owner_elem = NULL;
	gems_teacher_connection* tc = NULL;
	
	switch(element->connection_state)
	{
		case JAMMO_CONNECTED_AUTH:
			// Check the sending service
			switch(gems_connection_get_16(element,6))
			{
				case PROFILE_SERVICE_ID:
					// Check the error type
					switch(gems_connection_get_16(element,8))
					{
						case ERROR_SERVICE_NOT_RUNNING_TYPE:
							break;
						case ERROR_INVALID_ACCESS_TYPE:
							break;
						/* This should be reached only when user requests a specific username,
						   if teacher has set a login name this should be never received*/
						case ERROR_NO_PROFILE_FOR_USERNAME_TYPE:
							tc = gems_get_teacher_connection();
	
							// Connection to teacher exists
							if(tc)
							{
								// Is connected
								if(gems_teacher_connection_is_connected())
								{
									// Message is coming from teacher
									if(ph_c_connection_get_device_checksum(tc->connection->connection) == ph_c_connection_get_device_checksum(element->connection))
									{
										// At least one request was made
										if(gems_get_teacher_profile_request_count() > 0)
										{
											if(gems_get_data()->profile_request_timeout_sid != 0) g_source_remove(gems_get_data()->profile_request_timeout_sid);
											if(gems_get_data()->teacher_profile_request_timer) g_timer_destroy(gems_get_data()->teacher_profile_request_timer);
											gems_unset_waiting_profile_from_teacher();
										
											cem_add_to_log("gems_communication_process_error: No profile for requested username on teacher server",J_LOG_NETWORK);
										
											// TODO add callback to CHUM : profile not found
										}
									}
								}
							}
							break;
						default:
							break;
					}
					break;
					
				case GROUP_SERVICE_ID:
					switch(gems_connection_get_16(element,8))
					{
						case ERROR_SERVICE_NOT_RUNNING_TYPE:
							break;
						case ERROR_NOT_IN_GROUP_TYPE:
							/* if this is sent by a member in the active group the group owner should 
							synchronize group information with all members ! -> send MEMBER_LIST  */
							break;
						case ERROR_GROUP_NOT_ACTIVE_TYPE:
							/* Add tag for gems_connection* to indicate this kind of response,
							-> increase the interval between group requests and remove the tag
							when response or offer is received */
							
							/* If trying to join or leave check that this is sent by group owner,
							if not send the previous request to owner - if owner sent this, the
							group info might be corrupt -> reset */
							
							// Sender is in our group
							if(gems_group_is_in_group(element->profile->id))
							{
								switch(gems_get_data()->service_group->group_info->own_state)
								{
									case JOINING:
										// Fallthrough to next case
									case LEAVING:
										// Sent by owner?
										if(element->profile->id == gems_group_get_owner(NULL))
										{
											logmsg = g_strdup_printf("gems_communication_process_error: processing ERROR_GROUP_NOT_ACTIVE, sent by owner, group is being reset.");
											cem_add_to_log(logmsg,J_LOG_NETWORK);
											g_free(logmsg);
										
											// TODO notify chum through callback
											
											gems_reset_group_info();
										}
										// Sent by some other
										else
										{
											owner_elem = gems_communication_get_connection_with_userid(gems_get_data()->communication->connections,gems_group_get_owner(NULL));
										
											if(owner_elem != NULL)
											{
												logmsg = g_strdup_printf("gems_communication_process_error: processing ERROR_GROUP_NOT_ACTIVE, LAST ACTION NOT IMPLEMENTED, nothing done.");
												cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
												g_free(logmsg);
											}
											else
											{
												logmsg = g_strdup_printf("gems_communication_process_error: processing ERROR_GROUP_NOT_ACTIVE, no connection to group owner");
												cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
												g_free(logmsg);
											}
										}
										break;
									
									case IN_GROUP:
										// sent by owner?
										if(element->profile->id == gems_group_get_owner(NULL))
										{
											// TODO notify chum through callback
											
											gems_reset_group_info();
										}
										// Otherwise ignore
										break;
									
									case NONE:
										// IGNORE
										break;
									
									default:
										break;
								}
							}
							else
							{
								// TODO increase interval for requests to this connection
							}
							break;
							
						case ERROR_INVALID_GROUP_ID_TYPE:
							/* Try to request group info from group owner */
							break;
							
						case ERROR_ALREADY_IN_GROUP_TYPE:
							/* Request member list from group owner */
							
							// Are we in a active group and the sender is in our group
							if(gems_group_active() && gems_group_is_in_group(element->profile->id))
							{
								owner_elem = element;
								
								// Msg is not from owner
								if(owner_elem->profile->id != gems_group_get_owner(NULL)) 
								{
									// Get the group owner
									owner_elem = gems_communication_get_connection_with_userid(gems_get_data()->communication->connections, gems_group_get_owner(NULL));
									
									if(owner_elem == NULL) // Not found
									{
										logmsg = g_strdup_printf("gems_communication_process_error: processing ERROR_ALREADY_IN_GROUP, no connection to group owner");
										cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
										g_free(logmsg);
										break;
									}
									else
									{
										// Create request
										msg = gems_create_message_group_management_notify(REQUEST_MEMBERS, 0, 0);
							
										// Send request
										if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, owner_elem, msg) == FALSE)
										{
											logmsg = g_strdup_printf("gems_communication_process_error: processing ERROR_ALREADY_IN_GROUP, cannot write REQUEST_MEMBERS to group owner");
											cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
											g_free(logmsg);
										}
						
										gems_clear_message(msg);
									}
								}	
							}
							else
							{
								logmsg = g_strdup_printf("gems_communication_process_error: processing ERROR_ALREADY_IN_GROUP (group %s), got from %u - %s group", 
									gems_group_active() ? "active" : "not active", element->profile->id, gems_group_is_in_group(element->profile->id) ? "in" : "not in");
								cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
								g_free(logmsg);
							}
							break;
							
						case ERROR_GROUP_FULL_TYPE:
							// From owner
							if(element->profile->id == gems_group_get_owner(NULL))
							{
								/* Remove self from the group and reset the group info */
								if(gems_get_data()->service_group->group_info->own_state == JOINING) gems_reset_group_info();
								// TODO notify chum through callback
							}
							// From other
							else
							{
								logmsg = g_strdup_printf("gems_communication_process_error: processing ERROR_GROUP_FULL, from other than owner (uid: %u)",element->profile->id);
								cem_add_to_log(logmsg,J_LOG_ERROR);
								g_free(logmsg);
							}
							break;
						
						/* TODO implement after last action recording is made -> resend to correct owner */
						case ERROR_NOT_GROUP_OWNER_TYPE:
							// In our group
							if(gems_group_is_in_group(element->profile->id))
							{
								// Not from owner
								if(element->profile->id != gems_group_get_owner(NULL))
								{
									owner_elem = gems_communication_get_connection_with_userid(gems_get_data()->communication->connections,gems_group_get_owner(NULL));
							
									if(owner_elem == NULL)
									{
										logmsg = g_strdup_printf("gems_communication_process_error: processing ERROR_NOT_GROUP_OWNER, no connection to group owner");
										cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
										g_free(logmsg);
									}
									else
									{
										logmsg = g_strdup_printf("gems_communication_process_error: processing ERROR_NOT_GROUP_OWNER, LAST ACTION NOT YET IMPLEMENTED, doing nothing.");
										cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
										g_free(logmsg);
									}
								}
								// This shouldn't happen
								else
								{
									logmsg = g_strdup_printf("gems_communication_process_error: processing ERROR_NOT_GROUP_OWNER returned by owner!");
									cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
									g_free(logmsg);
								}
							}
							break;
							
						case ERROR_GROUP_LOCKED_TYPE:
							// TODO add last actions to check that last message was join request
							if(element->profile->id == gems_group_get_owner(NULL))
							{
								if(gems_get_data()->service_group->group_info->own_state == JOINING) gems_reset_group_info();
								// TODO notify chum through callback
							}
							else
							{
								logmsg = g_strdup_printf("gems_communication_process_error: processing ERROR_GROUP_LOCKED returned by other than owner! (uid: %u)", element->profile->id);
								cem_add_to_log(logmsg,J_LOG_ERROR);
								g_free(logmsg);
							}
							break;
						default:
							break;
					}
					break;
					
				//case COLLABORATION_SERVICE_ID:
				//	break;
				//case SONGBANK_SERVICE_ID:
				//	break;
				//case DATATRANSFER_SERVICE_ID:
				//	break;
				//case MENTOR_SERVICE_ID:
				//	break;
				//case CONTROL_SERVICE_ID:
				//	break;	
				//case PROBE_SERVICE_ID:
				//	break;
				// Invalid sender
				default:
					break;
			}
			break;
		case JAMMO_CONNECTED_NO_AUTH:
			switch(gems_connection_get_16(element,6)) // Get sending service id
			{
				case JAMMO_SERVICE_ID:
					/* If tried to connect to some other than JamMo service, the ERROR_INVALID_SERVICE_ID
					   will be sent by JAMMO_SERVICE_ID only */
					switch(gems_connection_get_16(element,8)) // Get error
					{
						case ERROR_INVALID_SERVICE_ID_TYPE:
							break;
						default:
							break;
					}
					break;
				case AA_SERVICE_ID:
					/* Authentication errors */
					// invalid service id type
					switch(gems_connection_get_16(element,8)) // Get error
					{
						case ERROR_SERVICE_NOT_RUNNING_TYPE:
							break;
						default:
							break;
					}
					break;
				/* Other services should not send errors to user who is not authenticated.
				   If user tries to connect to service with 
				*/
				default:
					break;
			}
			break;
		default:
			break;
	}
}

/**
 * gems_communication_probe_for_peerhood:
 *
 * Attempt to establish a new connection to PeerHood daemon via PeerHood
 * library. A timeout function enabled only when the connection to PeerHood 
 * has been lost and is executed periodically by Glib main loop. Re-enables the
 * other timeout functions (gems_enable_network_timeout_functions()) and
 * re-registers all services (gems_register_services()). When the connection is
 * re-established it will remove self from the main loop.
 *
 * Returns: Integer value 0 (FALSE) or 1 (TRUE), 0 when errors or the function
 * is required to be removed from Glib main loop.
 */
gint gems_communication_probe_for_peerhood()
{
	gems_components* data = gems_get_data();
	
	if(data == NULL) return FALSE;
	
	gchar* logmsg = g_strdup_printf("gems_communication_probe_for_peerhood");
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
	
	// Initialize callback if it was not set (PeerHood not found at start)
	if(data->ph_cb == NULL) gems_init_peerhood_callback();
	
	// Get PeerHood instance if not set
	if(data->ph == NULL) data->ph = ph_c_get_instance(data->ph_cb);
	
	// Cannot get instance - should not happen
	if(data->ph == NULL)
	{
		logmsg = g_strdup_printf("gems_communication_probe_for_peerhood: Got PeerHood NULL reference.");
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
		return TRUE; // Got NULL reference, try again
	}
	
	gchar** args = g_new0(gchar*,1);
	args[0] = "./jammo";
	if(ph_c_init(data->ph,1,args) == TRUE)
	{
		gems_register_services();
		data->ph_on = TRUE;
		gems_enable_network_timeout_functions();
		logmsg = g_strdup_printf("gems_networking_probe_for_peerhood: initialized and re-enabled timeout functions.");
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
		g_free(args);
		if(data->timeout_functions[10] != 0) g_source_remove(data->timeout_functions[10]);
		return FALSE; // Got connection, remove
	}
	else
	{
		logmsg = g_strdup_printf("gems_networking_probe_for_peerhood: cannot initialize peerhood.");
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
		g_free(args);
		return TRUE;
	}
}

/**
 * gems_communication_peerhood_close_detected:
 *
 * Called when connection to PeerHood daemon is lost. Disables all services and
 * cleans up the lists of services. Removes all timeout functions from Glib
 * main loop. Enables the PeerHood probing timeout function.
 *
 * Returns: void
 */
void gems_communication_peerhood_close_detected()
{
	gchar* logmsg = NULL;
	gems_components* data = gems_get_data();
	
	if(data == NULL) return;
	
	logmsg = g_strdup_printf("gems_networking_peerhood_close_detected: detected peerhood daemon close. Disabling services and to-functions. ");
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
	
	// Set Peerhood "off"
	data->ph_on = FALSE;
	
	// Disable services (cannot unregister hence no connection)
	data->service_jammo->enabled = FALSE;
	data->service_profile->enabled = FALSE;
	data->service_group->enabled = FALSE;
	data->service_collaboration->enabled = FALSE;
	data->service_control->enabled = FALSE;
	
	// Cleanup lists from previous connections - not valid anymore
	// TODO Check the behaviour if lists are not cleared - connections are not daemon dependant
	jammo_gems_cleanup_lists();
	gems_communication_cleanup_lists();
	gems_service_jammo_cleanup_lists();
	gems_service_profile_cleanup_lists();
	gems_service_group_cleanup_lists();
	gems_service_collaboration_cleanup_lists();
	
	// Remove timeout functions
	for(gint i = 0; i < TO_FUNCTIONS; i++) if(data->timeout_functions[i] != 0) g_source_remove(data->timeout_functions[i]);
	
	data->timeout_functions[10] = g_timeout_add_full(G_PRIORITY_HIGH,2000,(GSourceFunc)gems_communication_probe_for_peerhood, NULL, NULL);
	logmsg = g_strdup_printf("gems_networking_peerhood_close_detected: probing for peerhood started.");
	cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	g_free(logmsg);
	
}

/**
 * gems_teacher_connection_init:
 *
 * Initialize new empty #gems_teacher_connection struct.
 *
 * Returns: a pointer to created #gems_teacher_connection struct.
 */
gems_teacher_connection* gems_teacher_connection_init()
{
	gems_teacher_connection* tc = g_new0(gems_teacher_connection,sizeof(gems_teacher_connection));
	
	tc->connection = NULL;
	
	return tc;
}

/**
 * gems_teacher_connection_disconnected:
 *
 * Empties the connection to teacher after disconnection. Does not free the
 * structure containing teacher connection (#gems_teacher_connection).
 * 
 * Returns: TRUE when the connection did exist, FALSE otherwise.
 */
gboolean gems_teacher_connection_disconnected()
{
	gems_components* data = gems_get_data();
	
	if(data == NULL) return FALSE;
	if(data->teacher_connection == NULL) return FALSE; 
	
	// gems_connection exists
	if(data->teacher_connection->connection != NULL)
	{
		//gems_clear_gems_connection(data->teacher_connection->connection);
		data->teacher_connection->connection = NULL;
		
		//data->timeout_functions[9] = g_timeout_add_full(G_PRIORITY_HIGH,100,(GSourceFunc)gems_communication_search_teacher, NULL, NULL);
		
		gchar* logmsg = g_strdup_printf("gems_teacher_connection_disconnected: connection to teacher was disconnected. Teacher search re-enabled.");
		cem_add_to_log(logmsg,J_LOG_NETWORK);
		g_free(logmsg);
		
		return TRUE;
	}
	else return FALSE;
}

/**
 * gems_clear_teacher_connection:
 *
 * Clear the teacher connection and delete the struct.
 * 
 * Returns: void
 */
void gems_clear_teacher_connection()
{
	gems_components* data = gems_get_data();
	
	if(data == NULL) return;
	if(data->teacher_connection == NULL) return; 
	
	// gems_connection exists
	if(data->teacher_connection->connection != NULL)
	{
		//gems_clear_gems_connection(data->teacher_connection->connection);
		data->teacher_connection->connection = NULL;
		
		gchar* logmsg = g_strdup_printf("gems_clear_teacher_connection: connection to teacher was disconnected");
		cem_add_to_log(logmsg,J_LOG_NETWORK);
		g_free(logmsg);
	}
	
	g_free(data->teacher_connection);
	data->teacher_connection = NULL;
	
}

/**
 * gems_communication_do_profile_request_to_teacher:
 * @username: Username to use in profile request
 *
 * Send a profile request to teacher if the connection exists and teacher is
 * connected. Use the given @username in the message.
 * 
 * Returns: TRUE if the request was created and sent, FALSE otherwise.
 */
gboolean gems_communication_do_profile_request_to_teacher(gchar* username)
{
	gboolean rval = TRUE;
	gems_message* msg = gems_create_message_teacher_service_profile_request(username);
	if(gems_teacher_connection_is_connected()) rval = gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,gems_get_teacher_connection()->connection,msg);
	else
	{
		cem_add_to_log("No connection to teacher. Cannot send profile request.",J_LOG_NETWORK);
		rval = FALSE;
	}
	gems_clear_message(msg);
	
	return rval;
}

/**
 * gems_communication_send_profile_to_teacher:
 * 
 * Send the full profile to teacher if the connection to teacher exists.
 *
 * Returns: TRUE if the message was sent, FALSE otherwise (or if there is no 
 * connection to teacher).
 */
gboolean gems_communication_send_profile_to_teacher()
{
	if(gems_teacher_connection_is_connected())
	{
		gems_message* msg = gems_create_message_service_profile(PROFILE_FULL);
		gboolean rval = gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE,gems_get_teacher_connection()->connection,msg);
		gems_clear_message(msg);
		if(rval == FALSE) cem_add_to_log("Cannot send profile to teacher, writing failed",J_LOG_NETWORK_DEBUG);
		return rval;
	}
	else
	{
		cem_add_to_log("No connection to teacher. Cannot send profile",J_LOG_NETWORK);
		return FALSE;
	}
}

/**
 * gems_communication_search_teacher:
 *
 * A timeout function for searching teacher device from the surroundings. 
 * Requests the list of devices with "JamMo:Mentor" service from PeerHood and
 * attempts to create connection to first one that was found. 
 *
 * Returns: Integer value 0 (FALSE) or 1 (TRUE), 0 when errors or the function
 * is required to be removed from Glib main loop.
 *
 * Deprecated: Not used.
 */
gint gems_communication_search_teacher()
{
	gchar* logmsg = NULL;
	gems_components* data = gems_get_data();	
	
	if(data == NULL) return FALSE;
	if(gems_get_peerhood() == NULL) return FALSE; // no PeerHood = no network
	if(gems_teacher_connection_is_connected()) return FALSE; // Already connected
	
	// Get surrounding devices with service JAMMO_SERVICE_NAME 
	TDeviceList* list = ph_c_get_devicelist_with_services(gems_get_peerhood(),MENTOR_SERVICE_NAME);
	// Got a NULL reference - error with PeerHood -> stop it
	if(list == NULL)
	{
		logmsg = g_strdup_printf("gems_communication_search_teacher: connection to PeerHood daemon was lost.");
		cem_add_to_log(logmsg, J_LOG_NETWORK);
		g_free(logmsg);
		gems_communication_peerhood_close_detected();
		return FALSE;
	}
	
	if(ph_c_devicelist_is_empty(list) == FALSE)
	{
		// Initialize wrapper for iterator for list
		DeviceIterator* iterator = ph_c_new_device_iterator(list);
		
		if(iterator == NULL)
		{
			ph_c_delete_devicelist(list);
			return FALSE;
		}
		
		// Go through list - there should be only one teacher
		for( ; ph_c_device_iterator_is_last(iterator,list) != TRUE; ph_c_device_iterator_next_iterator(iterator))
		{
			//Teacher connection is one of the existing jammo connections - mark it as data->teacher_connection
			MAbstractDevice* dev = ph_c_device_iterator_get_device(iterator);
			data->teacher_connection->connection=gems_communication_get_connection(data->communication->connections, ph_c_device_get_checksum(dev));

			logmsg = g_strdup_printf("gems_communication_search_jammos: Connected to new Mentor enabled device %s/%d", ph_c_device_get_name(dev),ph_c_device_get_checksum(dev));
			cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
			/*
			// Get the device
			MAbstractDevice* dev = ph_c_device_iterator_get_device(iterator);
			
			// Through WLAN and has peerhood? - all devices in the list should have ph enabled
			if((strcmp(ph_c_device_get_prototype(dev),"wlan-base") == 0) && (ph_c_device_has_peerhood(dev) == TRUE))
			{
				MAbstractConnection* conn = NULL;
				if((conn = ph_c_connect_remoteservice(data->ph, dev, MENTOR_SERVICE_NAME)) != NULL)
				{
					// set connection
					gems_get_teacher_connection()->connection = gems_new_gems_connection(conn, 0, 0, JAMMO_CONNECTED_NO_AUTH, 0);
					
					logmsg = g_strdup_printf("gems_communication_search_teacher: Connected to new teacher device %s/%u",
						ph_c_device_get_name(dev),ph_c_device_get_checksum(dev));
					cem_add_to_log(logmsg, J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
					
					// Connection established, leave
					break;
				}
			}*/
		} // for
		// Delete the iterator
		ph_c_delete_device_iterator(iterator);
	}
	// Delete the list of devices
	ph_c_delete_devicelist(list);
	
	return TRUE;
}
