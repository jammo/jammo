/*
 * gems_service_group_management.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 *	    Tommi Kallonen <tommi.kallonen@lut.fi>
 */
 
#include "gems_service_group_management.h"
#include "gems_message_functions.h"
#include "communication.h"
#include "groupmanager.h"
#include "ProfileManager.h"
#include "../cem/cem.h"
#include "collaboration.h"

/**
 * gems_service_group_process_request:
 * @element: Connection to process
 *
 * Process incoming messages sent to group management service. Calls handler
 * functions to react to the message based on the command in the message.
 *
 * Returns: TRUE when connection to teacher did exist and the message was
 * processed, FALSE otherwise.
 */
gboolean gems_service_group_process_request(gems_connection* element)
{
	gems_components* data = gems_get_data();
	
	if(!data || !element) return FALSE;
	
	switch(gems_connection_get_16(element,6)) // command
	{
		case OFFER_GROUP:
			gems_service_group_handle_offer_group(data,element);
			break;
			
		case CURRENT_GROUP:
			gems_service_group_handle_current_group(data,element);
			break;
		
		case REQUEST_MEMBERS:
			gems_service_group_handle_request_members(data,element);
			break;
			
		case REQUEST_GROUP_INFO:
			gems_service_group_handle_request_group_info(data,element);
			break;
			
		case JOIN_GROUP:
			gems_service_group_handle_join_group(data,element);
			break;
			
		case LEAVE_GROUP:
			gems_service_group_handle_leave_group(data,element);
			break;
			
		case REMOVED_FROM_GROUP:
			gems_service_group_handle_removed_from_group(data,element);
			break;
			
		case MEMBER_DROPPED:
			gems_service_group_handle_member_dropped(data,element);
			break;
			
		case NEW_MEMBER:
			gems_service_group_handle_new_member(data,element);
			break;
		case MEMBER_LIST:
			gems_service_group_handle_member_list(data,element);
			break;
		case GROUP_IS_UNLOCKED:
			gems_service_group_handle_unlocked(data, element);
			break;
		case GROUP_IS_LOCKED:
			gems_service_group_handle_locked(data, element);
			break;
		case FORCE_GROUP:
			gems_service_group_handle_force_group(data, element);
			break;
		default:
			break;
	}
	return TRUE;
}

/**
 * gems_service_group_handle_offer_group:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process group offer messages. Update the list of group advertisements or add
 * a new group into list. If the group id is the same as our current groups id
 * update own group information with the received information only if the
 * sender of the information is the group owner.
 *
 * Returns: void
 */
void gems_service_group_handle_offer_group(gems_components* data, gems_connection* element)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	gchar* logerror = NULL;
	
	if(!data || !element) return;
	
	// If same group as ours & we're accepted to group and sender is the owner
	if((gems_connection_get_32(element,8) == data->service_group->group_info->id) && 
		(data->service_group->group_info->own_state == IN_GROUP) && 
		(element->profile->id == data->service_group->group_info->owner))
	{
		// we should update own group info (type, size, spaces, theme)
		data->service_group->group_info->type = gems_connection_get_16(element,12);
		data->service_group->group_info->size = gems_connection_get_16(element,14);
		data->service_group->group_info->spaces = gems_connection_get_16(element,16);
		data->service_group->group_info->theme = gems_connection_get_16(element,18);
		
		// TODO get the group lock type
		
		msg = gems_create_message_group_management_notify(REQUEST_MEMBERS, 0, 0);
		
		// TODO add some timer for these requests, e.g if member list is 30s old, request members
		if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
		{
			logerror = g_strdup_printf("gems_service_group_handle_offer_group: cannot write REQUEST_MEMBERS to group owner id %u (%s/%u)",
				data->service_group->group_info->owner, ph_c_connection_get_remote_address(element->connection),
				ph_c_connection_get_device_checksum(element->connection));
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_offer_group: wrote REQUEST_MEMBERS to %s/%u",
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		gems_clear_message(msg);
	}
	else
	{
		data->service_group->other_groups = gems_group_update_group_list(data->service_group->other_groups,
			gems_connection_get_32(element,8),   // group id
			element->profile->id,                // sender is owner
			gems_connection_get_16(element,12),  // type
			gems_connection_get_16(element,14),  // size
			gems_connection_get_16(element,16),  // spaces left
			gems_connection_get_16(element,18)); // theme
			
			// TODO get the group lock type
	}
	
	if(logmsg)
	{
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
	if(logerror)
	{
		cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
		g_free(logerror);
	}
}

/**
 * gems_service_group_handle_current_group:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process current group messages. These messages are replies to group info
 * requests sent to peers. If the group is not the same as the user is in and
 * if the sender is non-owner just update the list of groups. Otherwise if the
 * group id is the same as the current users group id update the information of
 * the current group. If the message contains different owner id, change the
 * owner of the group. If the user has been selected to be the new owner and is
 * not yet approved to the group the status is changed to #GemsGroupStatus 
 * %IN_GROUP.
 *
 * Returns: void
 */
void gems_service_group_handle_current_group(gems_components* data, gems_connection* element)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	gchar* logerror = NULL;
	
	if(!data || !element) return;
	
	// If same group as ours
	if(gems_connection_get_32(element,8) == data->service_group->group_info->id)
	{
		// Sent by owner
		if(element->profile->id == data->service_group->group_info->owner)
		{
			// If group owner differs from the one in our data
			if(gems_connection_get_32(element,12) != data->service_group->group_info->owner)
			{
				// Update owner - it can change when owner decides to quit and assigns ownership to some other user
				data->service_group->group_info->owner = gems_connection_get_32(element,12);
				// TODO if we're assigned as owner notify user ?
			}
		
			// Update other
			data->service_group->group_info->type = gems_connection_get_16(element,16);
			data->service_group->group_info->size = gems_connection_get_16(element,18);
			data->service_group->group_info->spaces = gems_connection_get_16(element,20);
			data->service_group->group_info->theme = gems_connection_get_16(element,22);
			// TODO get group lock type
		
			// If owner is some other than us - request updated member list
			if(data->service_group->group_info->owner != gems_profile_manager_get_userid(NULL))
			{	
				// Get owner of the group
				gems_connection* owner_element = gems_communication_get_connection_with_userid(data->communication->connections, data->service_group->group_info->owner);
		
				msg = gems_create_message_group_management_notify(REQUEST_MEMBERS, 0, 0);
			
				if(owner_element != NULL)
				{
					if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, owner_element, msg) == FALSE)
					{
						logerror = g_strdup_printf("gems_service_group_handle_current_group: cannot write REQUEST_MEMBERS to group owner id %u (%s/%u)", 
							data->service_group->group_info->owner, ph_c_connection_get_remote_address(owner_element->connection), 
							ph_c_connection_get_device_checksum(owner_element->connection));
					}
				}
				else logmsg = g_strdup_printf("gems_service_group_handle_current_group: wrote REQUEST_MEMBERS to %s/%u", 
					ph_c_connection_get_remote_address(owner_element->connection), ph_c_connection_get_device_checksum(owner_element->connection));

				gems_clear_message(msg);
			}
			// We are the new owner (the member list should have been sent by the previous owner before leaving)
			else
			{
				logmsg = g_strdup_printf("gems_service_group_handle_current_group: got group %u ownership from %u", data->service_group->group_info->id, element->profile->id);
				
				switch(data->service_group->group_info->own_state)
				{
					// We were joining - set to belong in group
					case JOINING:
						data->service_group->group_info->own_state = IN_GROUP;
						break;
					// We were leaving, set to belong in group and leave again
					case LEAVING:
						data->service_group->group_info->own_state = IN_GROUP;
						gems_group_leave_from_group();
						break;
					default:
						break;
				}
			}
		}
		// Otherwise ignore - group info is updated by owner only
		else logmsg = g_strdup_printf("gems_service_group_handle_current_group: got CURRENT_GROUP from non-owner (%u, owner: %u)", element->profile->id, data->service_group->group_info->owner);
	}
	else
	{
		data->service_group->other_groups = gems_group_update_group_list(data->service_group->other_groups,
			gems_connection_get_32(element,8),   // group id
			gems_connection_get_32(element,12),  // owner
			gems_connection_get_16(element,16),  // type
			gems_connection_get_16(element,18),  // size
			gems_connection_get_16(element,20),  // spaces left
			gems_connection_get_16(element,22)); // theme
			// TODO get group lock type
	}
	
	if(logmsg)
	{
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
	if(logerror)
	{
		cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
		g_free(logerror);
	}
}

/**
 * gems_service_group_handle_request_members:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process member request from a group member. If the message is from some
 * other user than from a member of the current group it is ignored. If the
 * receiver is not the owner it will send an error, otherwise a updated list
 * of group members is sent back.
 *
 * Returns: void
 */
void gems_service_group_handle_request_members(gems_components* data, gems_connection* element)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	gchar* logerror = NULL;
	
	if(!data || !element) return;
	
	// If we are not in a active group
	if(data->service_group->group_info->id == NOT_ACTIVE)
	{
		msg = gems_create_error_message(ERROR_GROUP_NOT_ACTIVE_TYPE,GROUP_SERVICE_ID);

		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_request_members: error sending ERROR_GROUP_NOT_ACTIVE to device: %u, user: %u",
				ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_request_members: wrote ERROR_GROUP_NOT_ACTIVE (gid = %u)",data->service_group->group_info->id);

		gems_clear_message(msg);
	}
	// Are we the owner
	else if(gems_profile_manager_get_userid(NULL) != data->service_group->group_info->owner)
	{
		msg = gems_create_error_message(ERROR_NOT_GROUP_OWNER_TYPE,GROUP_SERVICE_ID);
		
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_request_members: error sending ERROR_NOT_GROUP_OWNER to device: %u, user: %u",
				ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else
		{
			logmsg = g_strdup_printf("gems_service_group_handle_request_members: wrote ERROR_NOT_GROUP_OWNER (onwer = %u, own id = %u)",
				data->service_group->group_info->owner, gems_profile_manager_get_userid(NULL));
		}
		gems_clear_message(msg);
	}
	// Check if a member of the group
	else if(gems_group_is_in_group(element->profile->id) == FALSE)
	{
		msg = gems_create_error_message(ERROR_NOT_IN_GROUP_TYPE,GROUP_SERVICE_ID);
		
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_request_members: error sending ERROR_NOT_IN_GROUP to device: %u, user: %u",
				ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else
		{
			logmsg = g_strdup_printf("gems_service_group_handle_request_members: wrote ERROR_NOT_IN_GROUP (sender = %u)",
				element->profile->id);
		}
		gems_clear_message(msg);
	}
	else
	{
		msg = gems_create_message_group_management_group_info(MEMBER_LIST);
		
		if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
		{
			logerror = g_strdup_printf("gems_service_group_handle_request_members: error sending MEMBER_LIST to device: %u, user: %u",
				ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_request_members: wrote MEMBER_LIST to %s/%u", 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		
		gems_clear_message(msg);
	}
	
	if(logmsg)
	{
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
	if(logerror)
	{
		cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
		g_free(logerror);
	}
}

/**
 * gems_service_group_handle_request_group_info:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process group info request. Send the current group info (%CURRENT_GROUP) if
 * the user is in a some group. Otherwise send %ERROR_GROUP_NOT_ACTIVE_TYPE
 * message.
 *
 * Returns: void
 */
void gems_service_group_handle_request_group_info(gems_components* data, gems_connection* element)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	gchar* logerror = NULL;
	
	if(!data || !element) return;
	
	if(data->service_group->group_info->id == NOT_ACTIVE)
	{
		msg = gems_create_error_message(ERROR_GROUP_NOT_ACTIVE_TYPE,GROUP_SERVICE_ID);
		
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_request_group_info: error sending ERROR_GROUP_NOT_ACTIVE to device: %u, user: %u",
				ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_request_group_info: wrote ERROR_GROUP_NOT_ACTIVE to %s/%u", 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		gems_clear_message(msg);
	}
	else
	{
		msg = gems_create_message_group_management_group_info(CURRENT_GROUP);
		
		if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
		{
			logerror = g_strdup_printf("gems_service_group_handle_request_group_info: error sending response (CURRENT_GROUP) to REQUEST_GROUP_INFO by device: %u, user: %u",
				ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_request_group_info: wrote CURRENT_GROUP to %s/%u", 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		gems_clear_message(msg);
	}
	
	if(logmsg)
	{
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
	if(logerror)
	{
		cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
		g_free(logerror);
	}
}

/**
 * gems_service_group_handle_join_group:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process join to group message. Add the user to group if there is space left
 * in the current group and report back with %MEMBER_LIST and %OFFER_GROUP 
 * messages. When new user is added other members of the group are informed
 * about the arrival of new user with a group notify. If the group id is
 * invalid, user is not owner, group is full, user is already in the group, 
 * group is locked an error message is sent back.
 *
 * Returns: void
 */
void gems_service_group_handle_join_group(gems_components* data, gems_connection* element)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	gchar* logerror = NULL;
	
	if(!data || !element) return;
	
	// If we are not in a active group
	if(data->service_group->group_info->id == NOT_ACTIVE)
	{
		msg = gems_create_error_message(ERROR_GROUP_NOT_ACTIVE_TYPE,GROUP_SERVICE_ID);

		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_join_group: error sending ERROR_GROUP_NOT_ACTIVE to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_join_group: wrote ERROR_GROUP_NOT_ACTIVE to %s/%u", 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		gems_clear_message(msg);
	}
	// Are we the owner
	else if(gems_profile_manager_get_userid(NULL) != data->service_group->group_info->owner)
	{
		msg = gems_create_error_message(ERROR_NOT_GROUP_OWNER_TYPE,GROUP_SERVICE_ID);
		
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_join_group: error sending ERROR_NOT_GROUP_OWNER to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_join_group: wrote ERROR_NOT_GROUP_OWNER (owner = %u, own id = %u) to %s/%u",
			data->service_group->group_info->owner, gems_profile_manager_get_userid(NULL), ph_c_connection_get_remote_address(element->connection), 
			ph_c_connection_get_device_checksum(element->connection));

		gems_clear_message(msg);
	}
	// Locked
	else if(gems_group_is_locked())
	{
		msg = gems_create_error_message(ERROR_GROUP_LOCKED_TYPE,GROUP_SERVICE_ID);
		
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_join_group: error sending ERROR_GROUP_LOCKED to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_join_group: wrote ERROR_GROUP_LOCKED to %s/%u", 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		
		gems_clear_message(msg);
	}
	// Not active
	else if(data->service_group->group_info->id == NOT_ACTIVE)
	{
		msg = gems_create_error_message(ERROR_GROUP_NOT_ACTIVE_TYPE,GROUP_SERVICE_ID);
		
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_join_group: error sending ERROR_GROUP_NOT_ACTIVEto device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_join_group: wrote ERROR_GROUP_NOT_ACTIVE to %s/%u", 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		gems_clear_message(msg);
	}
	// Invalid gid
	else if(gems_connection_get_32(element,8) != data->service_group->group_info->id)
	{
		msg = gems_create_error_message(ERROR_INVALID_GROUP_ID_TYPE,GROUP_SERVICE_ID);
		
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_join_group: error sending ERROR_INVALID_GROUP_ID to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_join_group: wrote ERROR_INVALID_GROUP_ID (local gid: %u, remote gid: %u) to %s/%u",
			data->service_group->group_info->id, gems_connection_get_32(element,8), ph_c_connection_get_remote_address(element->connection), 
			ph_c_connection_get_device_checksum(element->connection));
		
		gems_clear_message(msg);
	}
	else
	{
		switch(gems_group_add_to_group(element->profile->id))
		{
			case ADDED_TO_GROUP:
				
				msg = gems_create_message_group_management_group_info(MEMBER_LIST);
				
				if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
				{
					logerror = g_strdup_printf("gems_service_group_handle_join_group: error sending MEMBER_LIST to device: %s/%u, user: %u",
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
					cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
					g_free(logerror);
					logerror = NULL;
				}
				else
				{
					logmsg = g_strdup_printf("gems_service_group_handle_join_group: new member to group accepted, wrote MEMBER_LIST to %s/%u, user %u", 
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
					logmsg = NULL;
				}
				gems_clear_message(msg);
				
				msg = gems_create_message_group_management_group_info(OFFER_GROUP);
				
				if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
				{
					logerror = g_strdup_printf("gems_service_group_handle_join_group: error sending OFFER_GROUP to device: %s/%u, user: %u",
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
					cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
					g_free(logerror);
					logerror = NULL;
				}
				else
				{
					logmsg = g_strdup_printf("gems_service_group_handle_join_group: new member to group accepted, wrote OFFER_GROUP to %s/%u", 
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
					logmsg = NULL;
				}
				gems_clear_message(msg);
				
				// Create a new member notification
				msg = gems_create_message_group_management_notify(NEW_MEMBER,data->service_group->group_info->id, element->profile->id);
				gems_group_send_to_group(msg); // TODO handle return values
		
				gems_clear_message(msg);
				// Notify collaboration about new groupmember
				collaboration_member_list_updated(data, element, TRUE);
				break;
				
			case GROUP_FULL:
				msg = gems_create_error_message(ERROR_GROUP_FULL_TYPE,GROUP_SERVICE_ID);
		
				if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
				{
					logerror = g_strdup_printf("gems_service_group_handle_join_group: error sending ERROR_GROUP_FULL to device: %s/%u, user: %u",
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
					cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
					g_free(logerror);
					logerror = NULL;
				}
				else
				{
					logmsg = g_strdup_printf("gems_service_group_handle_join_group: wrote ERROR_GROUP_FULL to %s/%u", 
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
					logmsg = NULL;	
				}
				gems_clear_message(msg);
				break;
				
			case ALREADY_IN_GROUP:
				msg = gems_create_error_message(ERROR_ALREADY_IN_GROUP_TYPE,GROUP_SERVICE_ID);
		
				if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
				{
					logerror = g_strdup_printf("gems_service_group_handle_join_group: error sending ERROR_ALREADY_IN_GROUP to device: %s/%u, user: %u",
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
					cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
					g_free(logerror);
					logerror = NULL;
				}
				else
				{
					logmsg = g_strdup_printf("gems_service_group_handle_join_group: wrote ERROR_ALREADY_IN_GROUP to %s/%u", 
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
					logmsg = NULL;	
				}
		
				gems_clear_message(msg);
				break;
				
			case CANNOT_ADD:
				// This should not happen
				msg = gems_create_error_message(ERROR_GROUP_ADD_ERROR_TYPE,GROUP_SERVICE_ID);
		
				if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
				{
					logerror = g_strdup_printf("gems_service_group_handle_join_group: error sending ERROR_GROUP_ADD_ERROR to device: %s/%u, user: %u",
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
					cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
					g_free(logerror);
					logerror = NULL;
				}
				else
				{
					logmsg = g_strdup_printf("gems_service_group_handle_join_group: wrote ERROR_GROUP_ADD_ERROR to %s/%u", 
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
					cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
					g_free(logmsg);
					logmsg = NULL;	
				}
		
				gems_clear_message(msg);
				break;
				
			default:
				break;
		}
	}
	
	if(logmsg)
	{
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
	if(logerror)
	{
		cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
		g_free(logerror);
	}
}

/**
 * gems_service_group_handle_leave_group:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process leave group request. If the user is in the group and the receiver is
 * the owner (and group ids match) a %REMOVED_FROM_GROUP is sent back and other
 * group members are notified with %MEMBER_DROPPED message. Otherwise an error
 * message is sent back.
 *
 * Returns: void
 */
void gems_service_group_handle_leave_group(gems_components* data, gems_connection* element)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	gchar* logerror = NULL;
	
	if(!data || !element) return;
	
	// If we are not in a active group
	if(data->service_group->group_info->id == NOT_ACTIVE)
	{
		msg = gems_create_error_message(ERROR_GROUP_NOT_ACTIVE_TYPE,GROUP_SERVICE_ID);

		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_leave_group: error sending ERROR_GROUP_NOT_ACTIVE to device: %s/%u, user: %u",
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_leave_group: wrote ERROR_GROUP_NOT_ACTIVE to %s/%u", 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		gems_clear_message(msg);
	}
	// Are we the owner
	else if(gems_profile_manager_get_userid(NULL) != data->service_group->group_info->owner)
	{
		msg = gems_create_error_message(ERROR_NOT_GROUP_OWNER_TYPE,GROUP_SERVICE_ID);
		
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_leave_group: error sending ERROR_NOT_GROUP_OWNER to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_leave_group: wrote ERROR_NOT_GROUP_OWNER to %s/%u", 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		gems_clear_message(msg);
	}
	// group id is correct?
	else if(gems_connection_get_32(element,8) != data->service_group->group_info->id)
	{
		msg = gems_create_error_message(ERROR_INVALID_GROUP_ID_TYPE,GROUP_SERVICE_ID);
		
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_leave_group: error sending ERROR_INVALID_GROUP_ID to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_leave_group: wrote ERROR_INVALID_GROUP_ID (local gid = %u, remote gid = %u) to %s/%u",
			data->service_group->group_info->id, gems_connection_get_32(element,8),ph_c_connection_get_remote_address(element->connection), 
			ph_c_connection_get_device_checksum(element->connection));
		
		gems_clear_message(msg);
	}
	else if(gems_group_remove_from_group(element->profile->id) == REMOVED_OK)
	{
		logmsg = g_strdup_printf("User %u removed from group.",element->profile->id);
		cem_add_to_log(logmsg,J_LOG_INFO);
		g_free(logmsg);
		logmsg = NULL;
	
		msg = gems_create_message_group_management_notify(REMOVED_FROM_GROUP, data->service_group->group_info->id, element->profile->id);
		
		if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
		{
			logerror = g_strdup_printf("gems_service_group_handle_leave_group: error sending REMOVED_FROM_GROUP to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_leave_group: wrote REMOVED_FROM_GROUP (uid = %u) to %s/%u",
			element->profile->id, ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
				
		gems_clear_message(msg);
		
		// Send member dropped to others
		msg = gems_create_message_group_management_notify(MEMBER_DROPPED,data->service_group->group_info->id, element->profile->id);
		gems_group_send_to_group(msg); // TODO handle return values
		
		gems_clear_message(msg);
	}
	// Otherwise user was not in the group
	else
	{
		msg = gems_create_error_message(ERROR_NOT_IN_GROUP_TYPE,GROUP_SERVICE_ID);
		
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_leave_group: error sending ERROR_NOT_IN_GROUP to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_leave_group: wrote ERROR_NOT_IN_GROUP (uid = %u) to %s/%u",
			element->profile->id,ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));

		gems_clear_message(msg);
	}
	if(logmsg)
	{
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
	if(logerror)
	{
		cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
		g_free(logerror);
	}
}

/**
 * gems_service_group_handle_removed_from_group:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process the removed from group message (%REMOVED_FROM_GROUP). A member can
 * be removed from the group only by the group owner, these messages from
 * others are ignored. If the user is in %LEAVING #GemsGroupState user received
 * a confirmation to leave request, otherwise user was kicked from the group. 
 * When user is removed from the group the group information will be reset.
 *
 * Returns: void
 */
void gems_service_group_handle_removed_from_group(gems_components* data, gems_connection* element)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	gchar* logerror = NULL;
	
	if(!data || !element) return;
	
	// If we are not in a active group
	if(data->service_group->group_info->id == NOT_ACTIVE)
	{
		msg = gems_create_error_message(ERROR_GROUP_NOT_ACTIVE_TYPE,GROUP_SERVICE_ID);

		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_removed_from_group: error sending ERROR_GROUP_NOT_ACTIVE to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_removed_from_group: wrote ERROR_GROUP_NOT_ACTIVE to %s/%u", 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		gems_clear_message(msg);
	}
	// correct gid
	else if(gems_connection_get_32(element,8) == data->service_group->group_info->id)
	{
		// owner is the sender
		if(element->profile->id == data->service_group->group_info->owner)
		{
			if(data->service_group->group_info->own_state == LEAVING)
			{
				// TODO notify about confirmation for LEAVE_GROUP
				cem_add_to_log("Leaving from group confirmed.",J_LOG_INFO);
				logmsg = g_strdup_printf("gems_service_group_process_request: leaving confirmed from group %u", data->service_group->group_info->id);
			}
			// kicked / group leader stopped
			else
			{
				// TODO notify user
				cem_add_to_log("Removed from group with force (kicked or group owner stopped).",J_LOG_INFO);
				logmsg = g_strdup_printf("gems_service_group_process_request: removed from group %u", data->service_group->group_info->id);
			}
			gems_reset_group_info(data->service_group->group_info);
		}
	}
	
	if(logmsg)
	{
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
	if(logerror)
	{
		cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
		g_free(logerror);
	}
}

/**
 * gems_service_group_handle_member_dropped:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process a message containing member dropped information. Update own group
 * member information by removing the dropped user from the group. If this user
 * is removed reset the group information. Messages are allowed from owner only
 * and these messages from others are ignored.
 *
 * Returns: void
 */
void gems_service_group_handle_member_dropped(gems_components* data, gems_connection* element)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	gchar* logerror = NULL;
	
	if(!data || !element) return;
	
	// If we are not in a active group
	if(data->service_group->group_info->id == NOT_ACTIVE)
	{
		msg = gems_create_error_message(ERROR_GROUP_NOT_ACTIVE_TYPE,GROUP_SERVICE_ID);

		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_member_dropped: error sending ERROR_GROUP_NOT_ACTIVE to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_member_dropped: wrote ERROR_GROUP_NOT_ACTIVE to %s/%u", 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		gems_clear_message(msg);
	}
	// correct gid
	else if(gems_connection_get_32(element,8) == data->service_group->group_info->id)
	{
		// Sent by owner
		if(element->profile->id == data->service_group->group_info->owner)
		{
			// Check if we left or were we kicked? 
			if(gems_connection_get_32(element,12) == gems_profile_manager_get_userid(NULL))
			{
				// TODO notify user
				cem_add_to_log("Group owner removed me from group.",J_LOG_INFO);
				
				logmsg = g_strdup_printf("gems_service_group_process_request: removed from group %u", data->service_group->group_info->id);
				gems_reset_group_info(data->service_group->group_info);
			}
			// couldn't remove the member given
			else if(gems_group_remove_from_group(gems_connection_get_32(element,12)) == NOT_IN_GROUP)
			{
				msg = gems_create_error_message(ERROR_NOT_IN_GROUP_TYPE,GROUP_SERVICE_ID);
	
				if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
				{
					logerror = g_strdup_printf("gems_service_group_process_request: error sending ERROR_NOT_IN_GROUP to device: %s/%u, user: %u",
						ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
				}
				else logmsg = g_strdup_printf("gems_service_group_handle_member_dropped: wrote ERROR_NOT_IN_GROUP to %s/%u", 
					ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));

				gems_clear_message(msg);
			}
			else logmsg = g_strdup_printf("gems_service_group_handle_member_dropped: removed user %u", gems_connection_get_32(element,12));
		}
		// Otherwise removed - no reaction needed
	}
	// Wrong id
	else
	{
		msg = gems_create_error_message(ERROR_INVALID_GROUP_ID_TYPE,GROUP_SERVICE_ID);
	
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_process_request: error sending ERROR_INVALID_GROUP_ID to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_member_dropped: wrote ERROR_INVALID_GROUP_ID (local gid = %u, remote gid = %u) to %s/%u",
			data->service_group->group_info->id,gems_connection_get_32(element,8), ph_c_connection_get_remote_address(element->connection), 
			ph_c_connection_get_device_checksum(element->connection));
	
		gems_clear_message(msg);
	}
	
	if(logmsg)
	{
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
	if(logerror)
	{
		cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
		g_free(logerror);
	}
}

/**
 * gems_service_group_handle_new_member:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process new member notification. Add the new user into group information if
 * the sender is the group owner and group ids match. If the group member list
 * on this device is full updated member list is requested from the group 
 * owner. If the message is incoming when group is locked an error is sent 
 * back.
 *
 * Returns: void
 */
void gems_service_group_handle_new_member(gems_components* data, gems_connection* element)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	gchar* logerror = NULL;
	
	if(!data || !element) return;
	
	// If we are not in a active group
	if(data->service_group->group_info->id == NOT_ACTIVE)
	{
		msg = gems_create_error_message(ERROR_GROUP_NOT_ACTIVE_TYPE,GROUP_SERVICE_ID);

		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_new_member: error sending ERROR_GROUP_NOT_ACTIVE to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_new_member: wrote ERROR_GROUP_NOT_ACTIVE to %s/%u", 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		gems_clear_message(msg);
	}
	// Locked - cannot add
	else if(gems_group_is_locked())
	{
		msg = gems_create_error_message(ERROR_GROUP_LOCKED_TYPE,GROUP_SERVICE_ID);
		
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_new_member: error sending ERROR_GROUP_LOCKED to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_new_member: wrote ERROR_GROUP_LOCKED to %s/%u", 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		gems_clear_message(msg);
	}
	
	// Gid is correct
	else if(gems_connection_get_32(element,8) == data->service_group->group_info->id)
	{
		// Sender is the group owner and the new user is not us
		if((element->profile->id == data->service_group->group_info->owner) &&
			(gems_connection_get_32(element,12) != gems_profile_manager_get_userid(NULL)))
		{
			switch(gems_group_add_to_group(gems_connection_get_32(element,12)))
			{
				case ADDED_TO_GROUP:
					logmsg = g_strdup_printf("gems_service_group_handle_new_member: new user %u joined to group %u", gems_connection_get_32(element,12), data->service_group->group_info->id);
					// Notify collaboration about new groupmember
					collaboration_member_list_updated(data, element,FALSE);
					break;
					
				case ALREADY_IN_GROUP: // do nothing
					logmsg = g_strdup_printf("gems_service_group_handle_new_member: existing user %u in group %u", gems_connection_get_32(element,12), data->service_group->group_info->id);
					break;
					
				case GROUP_FULL:
					// Request members from owner
					logmsg = g_strdup_printf("gems_service_group_handle_new_member: group was full, requesting members");
					msg = gems_create_message_group_management_notify(REQUEST_MEMBERS, 0, 0);
		
					// Get owner of the group
					gems_connection* owner_element = gems_communication_get_connection_with_userid(data->communication->connections, data->service_group->group_info->owner);
		
					if(owner_element != NULL)
					{
						if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, owner_element, msg) == FALSE)
						{
							logerror = g_strdup_printf("gems_service_group_handle_new_member: cannot write REQUEST_MEMBERS to group owner id %u (%s/%u)", 
								data->service_group->group_info->owner,ph_c_connection_get_remote_address(owner_element->connection), ph_c_connection_get_device_checksum(owner_element->connection));
						}
					}
					gems_clear_message(msg);
					break;
					
				default:
					break;
			}
		}
		// Otherwise do not react - discard
	}
	else
	{
		msg = gems_create_error_message(ERROR_INVALID_GROUP_ID_TYPE,GROUP_SERVICE_ID);
		
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_new_member: error sending ERROR_INVALID_GROUP_ID to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_new_member: wrote ERROR_INVALID_GROUP_ID (local gid = %u, remote gid = %u) to %s/%u",
			data->service_group->group_info->id,gems_connection_get_32(element,8), ph_c_connection_get_remote_address(element->connection), 
			ph_c_connection_get_device_checksum(element->connection));
		
		gems_clear_message(msg);
	}
	
	if(logmsg)
	{
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
	if(logerror)
	{
		cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
		g_free(logerror);
	}
}

/**
 * gems_service_group_handle_member_list:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process the member list message. Update own member list with the information
 * in the message only if the sender is the owner, group ids match and group is
 * not locked. If user receives this message in %JOINING #GemsGroupState it is
 * accepted to group and state changed accordingly. If the id of this user is
 * not found from the message group information will be reset (removed for some
 * reason).
 *
 * Returns: void
 */
void gems_service_group_handle_member_list(gems_components* data, gems_connection* element)
{
	gems_message* msg = NULL;
	gchar* logmsg = NULL;
	gchar* logerror = NULL;
	
	if(!data || !element) return;
	
	// If we are not in a active group
	if(data->service_group->group_info->id == NOT_ACTIVE)
	{
		msg = gems_create_error_message(ERROR_GROUP_NOT_ACTIVE_TYPE,GROUP_SERVICE_ID);

		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_member_list: error sending ERROR_GROUP_NOT_ACTIVE to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_member_list: wrote ERROR_GROUP_NOT_ACTIVE to %s/%u", 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		gems_clear_message(msg);
	}
	// Locked - cannot add
	else if(gems_group_is_locked())
	{
		msg = gems_create_error_message(ERROR_GROUP_LOCKED_TYPE,GROUP_SERVICE_ID);
		
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_member_list: error sending ERROR_GROUP_LOCKED to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_member_list: wrote ERROR_GROUP_LOCKED to %s/%u", 
			ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection));
		gems_clear_message(msg);
	}
	
	// Gid is correct
	else if(gems_connection_get_32(element,8) == data->service_group->group_info->id)
	{
		// Sender is the group owner
		if(element->profile->id == data->service_group->group_info->owner)
		{
			data->service_group->group_info->peers[0] = gems_connection_get_32(element,12); // Group owner is a peer
			guint i = sizeof(guint16) + sizeof(guint32) + sizeof(guint16) + sizeof(guint32) + sizeof(guint32); // 16
			guint pos = 1;
			gboolean ownfound = FALSE;
			while((i < 28) && (pos < (GROUP_MAX_SIZE - 1)))
			{
				// Not our id
				if(gems_connection_get_32(element,i) != gems_profile_manager_get_userid(NULL))
				{
					// Add id to position
					data->service_group->group_info->peers[pos] = gems_connection_get_32(element,i);
					pos++; // next position in peers[]
				}
				else
				{
					ownfound = TRUE;
					// If we were joining, change state
					if(data->service_group->group_info->own_state == JOINING)
					{
						cem_add_to_log("Joined to group succesfully",J_LOG_INFO);
						data->service_group->group_info->own_state = IN_GROUP;
						// inform collaboration about succesful join
						collaboration_pair_game_joined(data);
						logmsg = g_strdup_printf("gems_service_group_handle_member_list: accepted to group %u", data->service_group->group_info->id);
					}
				}
				i = i + sizeof(guint32); // to next field
			}
			
			// Couldn't find self - we were kicked? or not accepted?
			if(ownfound == FALSE)
			{
				// TODO notify user if our state is something else than LEAVING (or NOT_ACTIVE)
				cem_add_to_log("Group owner removed me from the group (id was not in member list).",J_LOG_INFO);
				
				gems_reset_group_info(data->service_group->group_info);
				logerror = g_strdup_printf("gems_service_group_handle_member_list: we weren't in the list, removed/kicked");
			}
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_member_list: got MEMBER_LIST from other than owner, sender = %u (%s/%u), owner = %u",
			element->profile->id, ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection), 
			data->service_group->group_info->owner);
	}
	// wrong gid
	else
	{
		msg = gems_create_error_message(ERROR_INVALID_GROUP_ID_TYPE,GROUP_SERVICE_ID);
		
		if(gems_communication_write_data(ph_c_connection_get_fd(element->connection),msg) != msg->length)
		{
			logerror = g_strdup_printf("gems_service_group_handle_member_list: error sending ERROR_INVALID_GROUP_ID to device: %s/%u, user: %u",
				ph_c_connection_get_remote_address(element->connection), ph_c_connection_get_device_checksum(element->connection),element->profile->id);
		}
		else logmsg = g_strdup_printf("gems_service_group_handle_member_list: wrote ERROR_INVALID_GROUP_ID (local gid = %u, remote gid = %u) to %s/%u",
			data->service_group->group_info->id,gems_connection_get_32(element,8), ph_c_connection_get_remote_address(element->connection), 
			ph_c_connection_get_device_checksum(element->connection));
		
		gems_clear_message(msg);
	}
	
	if(logmsg)
	{
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
		g_free(logmsg);
	}
	if(logerror)
	{
		cem_add_to_log(logerror,J_LOG_NETWORK_DEBUG);
		g_free(logerror);
	}
}

/**
 * gems_service_group_init:
 *
 * Initialize group service data structures. Initialize an empty group
 * information for this user with #GemsGroupStatus %NONE.
 * 
 * Returns: a pointer to created #gems_service_group struct
 */
gems_service_group* gems_service_group_init()
{
	gems_service_group* data = g_new(gems_service_group,sizeof(gems_service_group));
	
	data->enabled = FALSE;
	
	data->group_info = gems_new_group_info(NOT_ACTIVE,0,0,0,0,0,FALSE);
	
	data->other_groups = NULL;
	
	data->port = 0;

	return data;
}

/**
 * gems_service_group_cleanup:
 *
 * Clean the group service data structure.
 *
 * Returns: void
 */
void gems_service_group_cleanup()
{
	gems_service_group* data = gems_get_data()->service_group;
	
	if(data != NULL)
	{
		if(data->group_info != NULL) gems_clear_group_info(data->group_info);
		
		gems_service_group_cleanup_lists();
		
		g_free(data);
		data = NULL;
	}
}

/**
 * gems_service_group_cleanup_lists:
 *
 * Empty and free the group list.
 *
 * Returns: void
 */
void gems_service_group_cleanup_lists()
{
	gems_service_group* data = gems_get_data()->service_group;

	if(data->other_groups != NULL)
	{
		g_list_foreach(data->other_groups,(GFunc)gems_clear_group_info,NULL);
		g_list_free(data->other_groups);
		data->other_groups = NULL;
	}
}

/**
 * gems_service_group_handle_unlocked:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process the group unlocked message. Unlock the group if the sender is the
 * group owner.
 *
 * Returns: void
 */
void gems_service_group_handle_unlocked(gems_components* data, gems_connection* element)
{
	gchar* logmsg = NULL;
	
	if(!data || !element) return;
	
	if(gems_group_active(NULL)) // group is active and we're in it
	{
		// Group id is correct
		if(gems_connection_get_32(element,8) == gems_group_get_gid(NULL))
		{
			// sender is the owner
			if(element->profile->id == gems_group_get_owner(NULL))
			{
				gems_get_data()->service_group->group_info->is_locked = FALSE;
				cem_add_to_log("gems_service_group_handle_unlocked : group unlocked",J_LOG_INFO);
			}
			else
			{
				logmsg = g_strdup_printf("gems_service_group_handle_unlocked : Got unlock message from other (%u) than group owner %u, sender device %s/%u",
					element->profile->id, gems_group_get_owner(NULL), ph_c_connection_get_remote_address(element->connection),
					ph_c_connection_get_device_checksum(element->connection));
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
			}
		}
		else
		{
			logmsg = g_strdup_printf("gems_service_group_handle_unlocked : Got unlock message for other group (%u) than ours %u, sender device %s/%u",
				gems_connection_get_32(element,8), gems_group_get_gid(NULL), ph_c_connection_get_remote_address(element->connection),
				ph_c_connection_get_device_checksum(element->connection));
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
		}
	}
}

/**
 * gems_service_group_handle_locked:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process the group locked message. Lock the group if the sender is the group
 * owner.
 *
 * Returns: void
 */
void gems_service_group_handle_locked(gems_components* data, gems_connection* element)
{
	gchar* logmsg = NULL;
	
	if(!data || !element) return;
	
	if(gems_group_active(NULL)) // group is active and we're in it
	{
		// Group id is correct
		if(gems_connection_get_32(element,8) == gems_group_get_gid(NULL))
		{
			// sender is the owner
			if(element->profile->id == gems_group_get_owner(NULL))
			{
				gems_get_data()->service_group->group_info->is_locked = TRUE;
				cem_add_to_log("gems_service_group_handle_locked : group locked",J_LOG_INFO);
			}
			else
			{
				logmsg = g_strdup_printf("gems_service_group_handle_locked : Got lock message from other (%u) than group owner %u, sender device %s/%u",
					element->profile->id, gems_group_get_owner(NULL), ph_c_connection_get_remote_address(element->connection),
					ph_c_connection_get_device_checksum(element->connection));
				cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
				g_free(logmsg);
			}
		}
		else
		{
			logmsg = g_strdup_printf("gems_service_group_handle_locked : Got lock message for other group (%u) than ours %u, sender device %s/%u",
				gems_connection_get_32(element,8), gems_group_get_gid(NULL), ph_c_connection_get_remote_address(element->connection),
				ph_c_connection_get_device_checksum(element->connection));
			cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
			g_free(logmsg);
		}
	}
}

/**
 * gems_service_group_handle_force_group:
 * @data: pointer to the main data structure
 * @element: Connection which has the message in network buffer
 *
 * Process the force group message. This message is sent only by the teacher,
 * set the group information to correspond the content of the message if the
 * user is not in any group. Informs CHUM about the new group information by
 * starting the game with the parameters.
 *
 * Returns: void
 */
void gems_service_group_handle_force_group(gems_components* data, gems_connection* element)
{
	gchar* logmsg = NULL;
	guint16 track_id;
	
	if(!data || !element) return;

	//Only force group if we are not in one already(?)
	if(data->service_group->group_info->id == NOT_ACTIVE)
	{
		data->service_group->group_info->peers[0] = gems_connection_get_32(element,8);
		data->service_group->group_info->peers[1] = gems_connection_get_32(element,12);
		data->service_group->group_info->peers[2] = gems_connection_get_32(element,16);
		data->service_group->group_info->id = gems_connection_get_32(element,20);
		data->service_group->group_info->owner = gems_connection_get_32(element,24);
		data->service_group->group_info->type = gems_connection_get_16(element,28);
		data->service_group->group_info->size = gems_connection_get_16(element,30);
		data->service_group->group_info->spaces = gems_connection_get_16(element,32);
		data->service_group->group_info->theme = gems_connection_get_16(element,34);
		track_id = gems_connection_get_16(element,36);

		//set group as active
		data->service_group->group_info->own_state=IN_GROUP;
		//Inform CHUM of the game
		chum_callback_game_starter(data->service_group->group_info, track_id);

		logmsg = g_strdup_printf("gems_service_group_handle_force_group: got FORCE_GROUP with users owner = %u, peers[0] = %u", data->service_group->group_info->owner, data->service_group->group_info->peers[0]);
		cem_add_to_log(logmsg,J_LOG_NETWORK_DEBUG);
	
	}
	else cem_add_to_log("gems_service_group_handle_force_group : tried to set a teacher controlled group while current group is active.", J_LOG_INFO);
	
	g_free(logmsg);
}


