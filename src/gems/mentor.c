/*
 * mentor.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Tommi Kallonen <tommi.kallonen@lut.fi>
 */

#include "mentor.h"
#include "../cem/cem.h"
#include "communication.h"
#include "gems_message_functions.h"

void mentor_send_log_message_to_teacher(char *message,int type)
{
	gems_connection* element = NULL;
	gems_message * msg =NULL;
	gchar* logmsg = NULL;
	 
		
	gems_components* data = gems_get_data();
	if(gems_teacher_connection_is_connected()) 
	{
		element = data->teacher_connection->connection;
		msg = gems_create_message_mentor_log_message(LOG_MESSAGE, message);
	
		if(gems_communication_write_encrypted_data(JAMMO_PACKET_PRIVATE, element, msg) == FALSE)
					{
						logmsg = g_strdup_printf("Sending log message to teacher failed");
						cem_add_to_log(logmsg,J_LOG_ERROR);
						g_free(logmsg);
					}
		gems_clear_message(msg);
	}	


	
}
