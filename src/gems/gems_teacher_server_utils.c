/*
 * gems_teacher_server_utils.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */

#include <glib/gstdio.h>
#include "gems_teacher_server_utils.h"
#include "ProfileManager.h"
#include "gems_security.h"
#include "../configure.h"

/**
 * gems_teacher_server_new_profile_container:
 *
 * Create and initialize new empty #jammo_profile_container for teacher server.
 * The same container is also used for storing the full profile when creating a
 * temporary profile on the mobile device.
 *
 * Returns: A pointer to new #jammo_profile_container 
 */
jammo_profile_container* gems_teacher_server_new_profile_container()
{
	jammo_profile_container* container = g_new(jammo_profile_container,sizeof(jammo_profile_container));
	// Allocation failed?
	if(container == NULL) return NULL;
	
	container->type = 0;
	container->version = JAMMO_PROFILE_VERSION;
	container->userid = 0;
	container->username = NULL;
	container->password = NULL;
	container->salt = NULL;
	container->firstname = NULL;
	container->lastname = NULL;
	container->authlevel = NULL;
	container->age = 0;
	container->points = 0;
	container->avatarid = PROF_DEFAULT_AVATARID;
	container->encrypted = NULL;
	container->encrypted_length = 0;
	return container;	
}

/**
 * gems_teacher_server_new_profile_data_container:
 *
 * Create new teacher server database container structure 
 * (#teacher_server_profile_data) and initialize its data.
 *
 * Returns: A pointer to new #teacher_server_profile_data structure.
 */
teacher_server_profile_data* gems_teacher_server_new_profile_data_container()
{
	teacher_server_profile_data* data = g_new(teacher_server_profile_data,sizeof(teacher_server_profile_data));
	
	if(data == NULL) return NULL;
	
	data->profiles = NULL;
	data->salt = NULL;
	data->pwhash = NULL;
	data->datahash = NULL;
	data->data = NULL;
	data->datalength = 0;
	
	return data;
}

/**
 * gems_teacher_server_clear_profile_container:
 * @container: Container to clear and free
 *
 * Clear all conteint in the #jammo_profile_container and free it.
 *
 * Returns: void
 */
void gems_teacher_server_clear_profile_container(jammo_profile_container* container)
{
	if(container)
	{
		if(container->username) g_free(container->username);
		if(container->password) g_free(container->password);
		if(container->salt) g_free(container->salt);
		if(container->firstname) g_free(container->firstname);
		if(container->lastname) g_free(container->lastname);
		if(container->authlevel) g_free(container->authlevel);
		if(container->encrypted) g_free(container->encrypted);
		
		g_free(container);
		container = NULL;
	}
}

/**
 * gems_teacher_server_clear_profile_data_container:
 * @Data: Data container to clear and free
 *
 * Clear all conteint in the #teacher_server_profile_data and free it.
 *
 * Returns: void
 */
void gems_teacher_server_clear_profile_data_container(teacher_server_profile_data* data)
{
	if(data)
	{
		if(data->profiles) gems_teacher_server_clear_profile_list(data->profiles);
		if(data->salt) g_free(data->salt);
		if(data->pwhash) g_free(data->pwhash);
		if(data->datahash) g_free(data->datahash);
		if(data->data) g_free(data->data);
		
		g_free(data);
		data = NULL;
	}	
}

/**
 * gems_teacher_server_create_profile:
 * @_type: Type of the profile (%PROF_TYPE_LOCAL when creating a local profile
 * on the device or %PROF_TYPE_REMOTE when creating profile on teacher server
 * @_userid: User id to assign to profile
 * @_username: Username to assign to profile
 * @_password: Plain password for en-/decrypting this profile
 * @_authlevel: Authorization level to assign to profile
 * @_firstname: First name of the user to assign to profile
 * @_lastname: Last name of the user to assign to profile
 * @_age: Age of the user to assign to profile
 * @_avatarid: Id of the users avatar to assign to profile
 *
 * Create new profile with 0 points and fill it with given information. Calls
 * gems_teacher_server_setup_profile() to create the actual profile.
 *
 * Returns: A pointer to new #jammo_profile_container or NULL if some data is
 * not set or is invalid or the profile cannot be encrypted.
 */
jammo_profile_container* gems_teacher_server_create_profile(guint8 _type, guint32 _userid, const gchar* _username, const gchar* _password,
	const gchar* _authlevel, const gchar* _firstname, const gchar* _lastname, guint16 _age, guint32 _avatarid)
{
	// Call setup function with NULL salt and 0 points, salt will be generated when NULL
	return gems_teacher_server_setup_profile(_type, _userid,_username,_password,NULL,_authlevel,_firstname,_lastname,_age,0,_avatarid);
}

/**
 * gems_teacher_server_setup_profile:
 * @_type: Type of the profile (%PROF_TYPE_LOCAL when creating a local profile
 * on the device or %PROF_TYPE_REMOTE when creating profile on teacher server
 * @_userid: User id to assign to profile
 * @_username: Username to assign to profile
 * @_password: Plain password for en-/decrypting this profile
 * @_salt: Salt for password and encryption context initializations, if salt is
 * NULL new random salt will be generated
 * @_authlevel: Authorization level to assign to profile
 * @_firstname: First name of the user to assign to profile
 * @_lastname: Last name of the user to assign to profile
 * @_age: Age of the user to assign to profile
 * @_points: Points to set for the user
 * @_avatarid: Id of the users avatar to assign to profile
 *
 * Create new profile and fill it with given information. If the salt is NULL
 * generate new and call gems_teacher_server_encrypt_profile() to initialize
 * the security for encrypting the profile data.
 *
 * Returns: A pointer to new #jammo_profile_container or NULL if some data is
 * not set or is invalid or the profile cannot be encrypted.
 */
jammo_profile_container* gems_teacher_server_setup_profile(guint8 _type, guint32 _userid, const gchar* _username, const gchar* _password,
	guchar* _salt, const gchar* _authlevel, const gchar* _firstname, const gchar* _lastname, guint16 _age, guint32 _points, guint32 _avatarid)
{
	// check input
	if((_userid == 0) || (_username == NULL) || (_password == NULL) || (_authlevel == NULL) 
		|| (_firstname == NULL) || (_lastname == NULL) || (_age == 0)) return NULL;
	
	// Check params for illegal characters
	if(gems_check_illegal_characters(_username)) return NULL;
	if(gems_check_illegal_characters(_password)) return NULL;
	if(gems_check_illegal_characters(_authlevel)) return NULL;
	if(gems_check_illegal_characters(_firstname)) return NULL;
	if(gems_check_illegal_characters(_lastname)) return NULL;
	if(_type < PROF_TYPE_LOCAL || _type > PROF_TYPE_REMOTE) return NULL;

	jammo_profile_container* container = gems_teacher_server_new_profile_container();
	
	// Set details
	container->type = _type;
	container->userid = _userid;
	container->age = _age;
	container->points = _points;
	container->avatarid = _avatarid;
	container->username = g_strdup(_username);
	container->firstname = g_strdup(_firstname);
	container->lastname = g_strdup(_lastname);
	container->authlevel = g_strdup(_authlevel);
	container->password = g_strdup(_password);
	
	// No salt, used probably through gems_teacher_server_create_profile
	if(_salt == NULL) container->salt = gems_security_create_password_salt();
	else
	{
		// set salt
		container->salt = g_new0(guchar,sizeof(guchar)*AES_SALT_LENGTH);
		g_memmove(&container->salt[0],_salt,AES_SALT_LENGTH);
	}

	if(gems_teacher_server_encrypt_profile(container)) return container;
	else
	{
		gems_teacher_server_clear_profile_container(container);
		return NULL;
	}
}

/**
 * gems_teacher_server_get_profile_for_user:
 * @data: Teacher server "database" to use for searching the profile
 * @username: Searches a profile for this username
 *
 * Search the database @data for occurance of profile with @username. Return a
 * pointer to it or NULL if not found.
 *
 * Returns: A pointer to #jammo_profile_container or NULL if not found.
 */
jammo_profile_container* gems_teacher_server_get_profile_for_user(teacher_server_profile_data* data, gchar* username)
{
	GList* iter = NULL;
	if(!data) return NULL;
	if(!username) return NULL;
	if(!data->profiles) return NULL;
	
	for(iter = g_list_first(data->profiles); iter ; iter = g_list_next(iter))
	{
		jammo_profile_container* profile = (jammo_profile_container*)iter->data;
		if(g_strcmp0(username,profile->username) == 0) return profile;
	}
	return NULL;
}

/**
 * gems_teacher_server_get_profile_for_user_id:
 * @data: Teacher server "database" to use for searching the profile
 * @userid: Searches a profile for this userid
 *
 * Search the database @data for occurance of profile the @userid. Return a
 * pointer to it or NULL if not found. Useful for checking for duplicate ids.
 *
 * Returns: A pointer to #jammo_profile_container or NULL if not found.
 */
jammo_profile_container* gems_teacher_server_get_profile_for_user_id(teacher_server_profile_data* data, guint32 userid)
{
	GList* iter = NULL;
	if(!data) return NULL;
	if(userid == 0) return NULL;
	if(!data->profiles) return NULL;
	
	for(iter = g_list_first(data->profiles); iter ; iter = g_list_next(iter))
	{
		jammo_profile_container* profile = (jammo_profile_container*)iter->data;
		if(profile->userid == userid) return profile;
	}
	return NULL;
}

/**
 * gems_teacher_server_encrypt_profile:
 * @container: The container containing data to encrypt
 *
 * Encrypt the content of the @container and set the encrypted profile into the
 * encrypted profile data field of the #jammo_profile_container. Initialize the
 * security contexts with the password and salt in the @container and encrypt
 * the profile. Salted password and the profile are hashed. The  encrypted
 * profile is in the same format as the profile that is stored to disk 
 * (version, salt, password hash, profile hash and encrypted profile). Normally
 * used through other functions (create profile & change password) - no checks 
 * for container data.
 *
 * Returns: TRUE if the profile was encrypted and hashed, password hashed and 
 * encrypted data set, FALSE otherwise.
 */
gboolean gems_teacher_server_encrypt_profile(jammo_profile_container* container)
{
	// vars
	guint encproflen = 0, hashlen = 0, profilelen = 0;
	gchar* type = NULL;
	gchar* userid = NULL;
	gchar* age = NULL;
	gchar* points = NULL;
	gchar* avatarid = NULL;
	guchar* profilehash = NULL;
	guchar* encprofile = NULL;
	guchar* decprofile = NULL;
	guchar* decprofhash = NULL;
	guchar* profile = NULL;
	guchar* passwd = NULL;
	guchar* passhash = NULL;
	gboolean rval = TRUE;	

	if(container)
	{
		// Type out of range
		if(container->type < PROF_TYPE_LOCAL || container->type > PROF_TYPE_REMOTE) return FALSE;
		
		// Set profile data
		type = g_strdup_printf("%d",container->type);
		userid = g_strdup_printf("%u",container->userid);
		age = g_strdup_printf("%u",container->age);
		points = g_strdup_printf("%u",container->points);
		avatarid = g_strdup_printf("%u",container->avatarid);
		profile = (guchar*)g_strjoin(",",type,userid, container->username, container->authlevel, 
			container->firstname, container->lastname, points, age, avatarid, NULL);
		profilelen = strlen((gchar*)profile)+1;
	
		// Create salt and password
		if(container->salt == NULL) container->salt = gems_security_create_password_salt();
		passwd = gems_security_create_password(container->password,container->salt);
		passhash = NULL;
	
		// Initialize security
		if(gems_security_init_security(passwd,container->salt))
		{
			hashlen = PASSLEN;
			passhash = gems_security_calculate_hash(passwd,&hashlen);
			if(hashlen != SHA_HASH_LENGTH) rval = FALSE; // Invalid size hash, profile might not be valid
			
			hashlen = profilelen;
			profilehash = gems_security_calculate_hash(profile,&hashlen);
			if(hashlen != SHA_HASH_LENGTH) rval = FALSE; // Invalid size hash, profile might not be valid
			
			encprofile = gems_security_encrypt_data(profile,&profilelen);
			
			encproflen = profilelen;
			decprofile = gems_security_decrypt_data(encprofile,&encproflen);
			
			hashlen = encproflen;
			decprofhash = gems_security_calculate_hash(decprofile,&hashlen);
			if(hashlen != SHA_HASH_LENGTH) rval = FALSE; // Invalid size hash, profile might not be valid
		
			if(gems_security_verify_hash(profilehash,decprofhash,SHA_HASH_LENGTH))
			{
				container->encrypted_length = 1 + AES_SALT_LENGTH + 32 + 32 + profilelen;
				
				// Allocate content, free old if it was allocated
				if(container->encrypted != NULL) g_free(container->encrypted);
				container->encrypted = g_new0(gchar,sizeof(gchar) * container->encrypted_length);
	
				// set into beginning
				*(guint8*)&container->encrypted[0] = container->version;
				
				// Set salt
				*(guint32*)&container->encrypted[1] = *(guint32*)&container->salt[0];
				*(guint32*)&container->encrypted[sizeof(guint32)+1] = *(guint32*)&container->salt[sizeof(guint32)];
	
				// Set password hash
				g_memmove(&container->encrypted[AES_SALT_LENGTH + 1],passhash,32);
				// Set profile hash
				g_memmove(&container->encrypted[AES_SALT_LENGTH + 32 + 1],profilehash,32);
				// Set profile
				g_memmove(&container->encrypted[AES_SALT_LENGTH + 64 + 1],encprofile,profilelen);
				
			}
			else rval = FALSE;
		}
		else rval = FALSE;

		// Free all
		if(type != NULL) g_free(type);
		if(userid != NULL) g_free(userid);
		if(age != NULL) g_free(age);
		if(points != NULL) g_free(points);
		if(avatarid != NULL) g_free(avatarid);
		if(profile!= NULL) g_free(profile);
		if(profilehash != NULL) g_free(profilehash);
		if(encprofile != NULL) g_free(encprofile);
		if(decprofile != NULL) g_free(decprofile);
		if(decprofhash != NULL) g_free(decprofhash);
		if(passwd != NULL) g_free(passwd);
		if(passhash != NULL) g_free(passhash);
	
		gems_security_clear_security();
	}
	else rval = FALSE;
		
	return rval;
}

/**
 * gems_teacher_server_change_password:
 * @container: Profile container for which the password will be changed
 * @password: New password
 *
 * Change the password for profile in @container. Set the given plain @password
 * to @container and re-encrypt the profile with the new password.
 *
 * Returns: Result of gems_teacher_server_encrypt_profile().
 */
gboolean gems_teacher_server_change_password(jammo_profile_container* container, gchar* password)
{	
	if(container)
	{
		if(container->password != NULL) g_free(container->password);
		container->password = g_strdup(password);
		return gems_teacher_server_encrypt_profile(container);
	}
	else return FALSE;
}

/**
 * gems_teacher_server_write_profile:
 * @container: Profile to be written to disk
 *
 * Write the encrypted profile in the @container to disk. Profile is saved to
 * the subfolder %JAMMOPROFILE of the folder returned by 
 * configure_get_jammo_directory(). The profile file name is username.jpf.
 *
 * Returns: #GemsTUWrite type: WRITE_FAIL_CONTENT_NOT_SET for invalid 
 * parameters, WRITE_FAIL_CANNOT_REMOVE_FILE if previous profile cannot be
 * removed, WRITE_FAIL_CANNOT_SET_FILE_CONTENT if cannot write the encrypted
 * data to file, WRITE_OK when profile was written to file.
 */
GemsTUWrite gems_teacher_server_write_profile(jammo_profile_container* container)
{
	if(!container) return WRITE_FAIL_CONTENT_NOT_SET;
	if(!container->username) return WRITE_FAIL_CONTENT_NOT_SET;
	if(!container->encrypted) return WRITE_FAIL_CONTENT_NOT_SET;
	
	gchar* profilefile = g_strjoin(NULL,container->username,PROFILE_FILE_EXT,NULL);
	gchar* filename = g_build_filename(configure_get_jammo_directory(),JAMMOPROFILE,profilefile,NULL);
	GError* error = NULL;
	
	// Check for previous profile file
	if(g_file_test(filename,(G_FILE_TEST_EXISTS)))
	{
		if(g_remove(filename) != 0)
		{
			// Error, change username?
			g_free(filename);
			g_free(profilefile);
			return WRITE_FAIL_CANNOT_REMOVE_FILE;
		}
	}

	// Set content
  if(!g_file_set_contents(filename,container->encrypted,container->encrypted_length,&error))
  {
  	if(error)
		{
			// print error
			g_error_free(error);
			g_free(filename);
			g_free(profilefile);
			return WRITE_FAIL_CANNOT_SET_FILE_CONTENT;
		}
  }
  
	g_free(filename);
	g_free(profilefile);
	
	return WRITE_OK;
}

/**
 * gems_teacher_server_save_encrypted_profile:
 * @username: Username on the encrypted profile data
 * @profiledata: Encrypted profile data
 * @datalength: Length of the encrypted data.
 *
 * Save a single profile to disk with given encrypted data. Creates a 
 * #jammo_profile_container withencrypted data and calls 
 * gems_teacher_write_profile() to write this single profile onto disk.
 *
 * Returns: TRUE when profile was written FALSE if profile couldn't be written
 * to disk.
 */
gboolean gems_teacher_server_save_encrypted_profile(gchar* username, gchar* profiledata, guint datalength)
{
	gboolean rval = TRUE;
	jammo_profile_container* container = gems_teacher_server_new_profile_container();
	
	container->username = g_strdup(username);
	container->encrypted = g_new0(gchar,sizeof(gchar)*datalength);
	g_memmove(container->encrypted,profiledata,datalength);
	container->encrypted_length = datalength;

	if(gems_teacher_server_write_profile(container) != WRITE_OK) rval = FALSE;
				
	gems_teacher_server_clear_profile_container(container);
	
	return rval;
}

/**
 * gems_teacher_server_serialize_all_profiles:
 * @list: A #GList containing pointers to #jammo_profile_container structs
 * @length: Variable to store the length of the returned #gchar
 *
 * Serialize ALL profiles into one big #gchar. Integers are stored as they are
 * based on their size, charstrings will have a 8 bit integer in before the
 * string for storing the length of the charstring (NOTE: limitation for 
 * charstring length is atm 255). This function will be called when the 
 * encryption of the database is performed.
 *
 * Returns: A #gchar containing all profiles or NULL if there is not enough 
 * memory to allocate.
 */
gchar* gems_teacher_server_serialize_all_profiles(GList* list, guint* length)
{
	gchar* all_profiles = NULL;
	jammo_profile_container* profile = NULL;
	guint total_length = 0, position = 0;
	GList* iter = NULL;
	
	for(iter = g_list_first(list); iter; iter = g_list_next(iter))
	{
		profile = (jammo_profile_container*)iter->data;
		
		// Calculate length for the huge gchar*
		total_length = total_length + sizeof(guint8) + sizeof(guint32) + sizeof(guint8) + 
			strlen(profile->username) + 1 + sizeof(guint8) + strlen(profile->password) + 1 + AES_SALT_LENGTH + 
			sizeof(guint8) + strlen(profile->firstname) + 1 + sizeof(guint8) + strlen(profile->lastname) + 1 + 
			sizeof(guint8) + strlen(profile->authlevel) + 1 + sizeof(guint16) + sizeof(guint32) + sizeof(guint32);
	}
	
	if((all_profiles = (gchar*)g_try_malloc0(sizeof(gchar*)*total_length)) == NULL) return NULL;
	
	for(iter = g_list_first(list); iter; iter = g_list_next(iter))
	{
		profile = (jammo_profile_container*)iter->data;
		
		//type
		*(guint8*)&all_profiles[position] = profile->type;
		position++;
		
		// id
		*(guint32*)&all_profiles[position] = profile->userid;
		position = position + sizeof(guint32);
		
		// username length 
		all_profiles[position] = (guint8)strlen(profile->username) + 1;
		position++;
		
		// username
		g_memmove(&all_profiles[position],profile->username,strlen(profile->username) + 1);
		position = position + strlen(profile->username) + 1;
		
		// password length
		all_profiles[position] = (guint8)strlen(profile->password) + 1;
		position++;
		
		// password
		g_memmove(&all_profiles[position],profile->password,strlen(profile->password) + 1);
		position = position + strlen(profile->password) + 1;
		
		// salt - fixed size
		g_memmove(&all_profiles[position],profile->salt,AES_SALT_LENGTH);
		position = position + AES_SALT_LENGTH;
		
		// firstname length
		all_profiles[position] = (guint8)strlen(profile->firstname) + 1;
		position++;
		
		// firstname
		g_memmove(&all_profiles[position],profile->firstname,strlen(profile->firstname) + 1);
		position = position + strlen(profile->firstname) + 1;
		
		// lastname length
		all_profiles[position] = (guint8)strlen(profile->lastname) + 1;
		position++;
		
		// lastname
		g_memmove(&all_profiles[position],profile->lastname,strlen(profile->lastname) + 1);
		position = position + strlen(profile->lastname) + 1;
		
		// authlevel length
		all_profiles[position] = (guint8)strlen(profile->authlevel) + 1;
		position++;
		
		// authlevel
		g_memmove(&all_profiles[position],profile->authlevel,strlen(profile->authlevel) + 1);
		position = position + strlen(profile->authlevel) + 1 ;
		
		// age
		*(guint16*)&all_profiles[position] = profile->age;
		position = position + sizeof(guint16);
		
		// points
		*(guint32*)&all_profiles[position] = profile->points;
		position = position + sizeof(guint32);
		
		// avatar id
		*(guint32*)&all_profiles[position] = profile->avatarid;
		position = position + sizeof(guint32);
	}
	
	*length = total_length;
	return all_profiles;
}

/**
 * gems_teacher_server_deserialize_profiles:
 * @list: List where deserialized profiles (#jammo_profile_container) are added
 * @serialized_data: Profiles in serialized format
 * @length: Length of the @serialized_data
 *
 * Deserialize profiles and add profiles as #jammo_profile_container to @list.
 * This will be called after database is decrypted with 
 * gems_teacher_server_decrypt_profiles().
 *
 * Returns: Pointer to given @list after modifications.
 */
GList* gems_teacher_server_deserialize_profiles(GList* list, gchar* serialized_data, guint length)
{
	jammo_profile_container* profile = NULL;
	
	guint8 type = 0;
	guint32 id = 0;
	gchar* username = NULL;
	gchar* password = NULL;
	guchar* salt = NULL;
	gchar* firstname = NULL;
	gchar* lastname = NULL;
	gchar* authlevel = NULL;
	guint16 age = 0;
	guint32 points = 0;
	guint32 avatarid = 0;
	
	guint position = 0;
	guint8 valuelength = 0;
	
	while(position < length)
	{
		type = *(guint8*)&serialized_data[position];
		position++;
		
		// user id
		id = *(guint32*)&serialized_data[position];
		position = position + sizeof(guint32);
		
		// length of username
		valuelength = *(guint8*)&serialized_data[position];
		position++;
		
		// username
		username = g_new0(gchar,sizeof(gchar)*(valuelength));
		g_memmove(username,&serialized_data[position],valuelength);
		position = position + valuelength;
		
		// length of password
		valuelength = *(guint8*)&serialized_data[position];
		position++;
		
		// password
		password = g_new0(gchar,sizeof(gchar)*(valuelength));
		g_memmove(password,&serialized_data[position],valuelength);
		position = position + valuelength;
		
		// salt
		salt = g_new0(guchar,sizeof(guchar)*AES_SALT_LENGTH);
		g_memmove(&salt[0],&serialized_data[position],AES_SALT_LENGTH);
		position = position + AES_SALT_LENGTH;
		
		// length of firstname
		valuelength = *(guint8*)&serialized_data[position];
		position++;
		
		// firstname
		firstname = g_new0(gchar,sizeof(gchar)*(valuelength));
		g_memmove(firstname,&serialized_data[position],valuelength);
		position = position + valuelength;
		
		// length of lastname
		valuelength = *(guint8*)&serialized_data[position];
		position++;
		
		// lastname
		lastname = g_new0(gchar,sizeof(gchar)*(valuelength));
		g_memmove(lastname,&serialized_data[position],valuelength);
		position = position + valuelength;
		
		// length of authlevel
		valuelength = *(guint8*)&serialized_data[position];
		position++;
		
		// authlevel
		authlevel = g_new0(gchar,sizeof(gchar)*(valuelength));
		g_memmove(authlevel,&serialized_data[position],valuelength);
		position = position + valuelength;
		
		// age
		age = *(guint16*)&serialized_data[position];
		position = position + sizeof(guint16);
		
		// points
		points = *(guint32*)&serialized_data[position];
		position = position + sizeof(guint32);
		
		// avatarid
		avatarid = *(guint32*)&serialized_data[position];
		position = position + sizeof(guint32);
		
		// create profile and add to list
		if((profile = gems_teacher_server_setup_profile(type,id,username,password,salt,authlevel,firstname,
			lastname,age,points,avatarid)) != NULL) list = g_list_append(list,profile);
		
		g_free(username);
		username = NULL;
		g_free(password);
		password = NULL;
		g_free(salt);
		salt = NULL;
		g_free(authlevel);
		authlevel = NULL;
		g_free(firstname);
		firstname = NULL;
		g_free(lastname);
		lastname = NULL;
		
	}
	return list;
}

/**
 * gems_teacher_server_clear_profile_list:
 * @list: A #GList containing pointers to #jammo_profile_container structs
 *
 * Clear the #GList containing pointers to #jammo_profile_container structs.
 * Will be called when clearing the #teacher_server_profile_data with 
 * gems_teacher_server_clear_profile_data_container(). Clears and frees
 * all #jammo_profile_container structures in the @list.
 *
 * Returns: void
 */
void gems_teacher_server_clear_profile_list(GList* list)
{
	if(list) g_list_foreach(list,(GFunc)gems_teacher_server_clear_profile_container,NULL);
	g_list_free(list);
	list = NULL;
}

/**
 * gems_teacher_server_print_profile_list:
 * @list: A #GList containing pointers to #jammo_profile_container structs
 *
 * Print profiles in @list. For debugging only.
 *
 * Returns: void
 */
void gems_teacher_server_print_profile_list(GList* list)
{
	if(list) g_list_foreach(list,(GFunc)gems_teacher_server_print_profile,NULL);
}

/**
 * gems_teacher_server_print_profile:
 * @profile: profile to print
 *
 * Print profile data to stdout. For debugging only.
 *
 * Returns: void
 */
void gems_teacher_server_print_profile(jammo_profile_container* profile)
{
	printf("%u:%s:%s:%s:%s:%s:%u:%u:%u\n",profile->userid, profile->username, profile->password,profile->authlevel,
		profile->firstname,profile->lastname,profile->age,profile->points,profile->avatarid);
}

/**
 * gems_teacher_server_encrypt_profiles:
 * @data: Teacher database
 * @passwd: Plain password provided by teacher
 *
 * Serialize all data in database and encrypt the database. Every profile in
 * the database will be put into huge #gchar by calling 
 * gems_teacher_server_serialize_all_profiles() and the returned data is then
 * encrypted. To write the database on to disk call 
 * gems_teacher_server_write_profile_database(). First creates a password for
 * encryption and intializes security context, then encrypts the data.
 *
 * Returns: TRUE when database encryption was success, FALSE otherwise.
 */
gboolean gems_teacher_server_encrypt_profiles(teacher_server_profile_data* data, const gchar* passwd)
{
	gboolean rval = TRUE;
	gchar* serialized = NULL;
	guchar* decrypted = NULL;
	guchar* dechash = NULL;
	guchar* password = NULL;
	guint encrypted_length = 0;
	guint hash_length = 0;
	
	// check params and content
	if(data == NULL || passwd == NULL) return FALSE;
	if(data->profiles == NULL) return FALSE;	
	
	// serialize data
	if((serialized = gems_teacher_server_serialize_all_profiles(data->profiles, &data->datalength)) == NULL) rval = FALSE;
	else
	{
		// init salt and passwd
		if(data->salt == NULL) data->salt = gems_security_create_password_salt();
		password = gems_security_create_password(passwd,data->salt);
	
		// init security
		if(gems_security_init_security(password,data->salt))
		{
			// Password hash
			if(data->pwhash != NULL) g_free(data->pwhash);
			hash_length = PASSLEN;
			data->pwhash = gems_security_calculate_hash(password,&hash_length);
			if(hash_length != SHA_HASH_LENGTH) rval = FALSE; // Invalid size hash, profile might not be valid
		
			// Profile data hash
			if(data->datahash != NULL) g_free(data->datahash);
			hash_length = data->datalength;
			data->datahash = gems_security_calculate_hash((guchar*)serialized,&hash_length);
			if(hash_length != SHA_HASH_LENGTH) rval = FALSE; // Invalid size hash, profile might not be valid
		
			// encrypt
			if(data->data != NULL) g_free(data->data);
			data->data = gems_security_encrypt_data((guchar*)serialized,&(data->datalength));
		
			// decrypt
			encrypted_length = data->datalength;
			decrypted = gems_security_decrypt_data(data->data,&encrypted_length);
		
			// hash decrypted
			hash_length = encrypted_length;
			dechash = gems_security_calculate_hash(decrypted,&hash_length);
			if(hash_length != SHA_HASH_LENGTH) rval = FALSE; // Invalid size hash, profile might not be valid
	
			// verify hashes
			if(!gems_security_verify_hash(data->datahash,dechash,SHA_HASH_LENGTH)) rval = FALSE;
			gems_security_clear_security();
		}
		else rval = FALSE;
	}
	
	if(serialized) g_free(serialized);
	if(decrypted) g_free(decrypted);
	if(dechash) g_free(dechash);
	if(password)
	{
		gems_guaranteed_memset(password,0,PASSLEN);
		g_free(password);
	}
	
	return rval;
}

/**
 * gems_teacher_server_decrypt_profiles:
 * @data: Teacher database
 * @passwd: Plain password provided by teacher
 *
 * Decrypt the profiles in the #teacher_server_profile_data "data"-variable and
 * add pass decrypted profiles to deserialization function. Use this after
 * gems_teacher_server_read_profile_database(). Decrypted profiles will be 
 * deserialized into the profiles list in #teacher_server_profile_data, for
 * deserialization gems_teacher_server_deserialize_profiles() is called.
 *
 * Returns: TRUE when password was correct and data was decrypted, FALSE
 * otherwise.
 */
gboolean gems_teacher_server_decrypt_profiles(teacher_server_profile_data* data, const gchar* passwd)
{
	gboolean rval = TRUE;
	guchar* password = NULL;
	guchar* pwhash = NULL;
	guchar* decrypted = NULL;
	guchar* dechash = NULL;
	guint hashlength = 0;
	
	if(!data || !passwd) return FALSE;
	if((!data->salt) || (!data->pwhash) || (!data->datahash) || (!data->data) || (data->datalength == 0))
		rval = FALSE;
	else
	{
		// form password
		password = gems_security_create_password(passwd,data->salt);
	
		if(gems_security_init_security(password,data->salt))
		{
			// hash pass
			hashlength = PASSLEN;
			pwhash = gems_security_calculate_hash(password,&hashlength);
			if(hashlength != SHA_HASH_LENGTH) rval = FALSE;
		
			// password verified
			if(gems_security_verify_hash(pwhash,data->pwhash,SHA_HASH_LENGTH))
			{
				// decrypt
				decrypted = gems_security_decrypt_data(data->data,&(data->datalength));
			
				// hash
				hashlength = data->datalength;
				dechash = gems_security_calculate_hash(decrypted,&hashlength);
				if(hashlength != SHA_HASH_LENGTH) rval = FALSE;
			
				// verify hash
				if(gems_security_verify_hash(data->datahash,dechash,SHA_HASH_LENGTH))
				{
					if(data->profiles != NULL)
					{
						gems_teacher_server_clear_profile_list(data->profiles);
						data->profiles = NULL;
					}
					// Clear own security contexts BEFORE deserializing profiles
					gems_security_clear_security();
					// deserialize
					data->profiles = gems_teacher_server_deserialize_profiles(data->profiles,(gchar*)decrypted,data->datalength);
				}
				else rval = FALSE;
			}
			else rval = FALSE;
		}
		else rval = FALSE;
	}
	
	if(password)
	{
		gems_guaranteed_memset(password,0,PASSLEN);
		g_free(password);
	}
	if(pwhash) g_free(pwhash);
	if(decrypted) g_free(decrypted);
	if(dechash) g_free(dechash);
	
	return rval;
}

/**
 * gems_teacher_server_write_profile_database:
 * @data: Teacher database
 *
 * Write encrypted data to disk from database, will return 
 * %WRITE_FAIL_CONTENT_NOT_SET if data is not encrypted. Encrypt database 
 * before calling this with gems_teacher_server_encrypt_profiles(). Database
 * will be written to configure_get_jammo_directory()/%TSRV_DATABASE. 
 *
 * Returns: #GemsTUWrite value.
 */
GemsTUWrite gems_teacher_server_write_profile_database(teacher_server_profile_data* data)
{
	if(!data) return WRITE_FAIL_CONTENT_NOT_SET;
	if(!data->salt) return WRITE_FAIL_CONTENT_NOT_SET;
	if(!data->pwhash) return WRITE_FAIL_CONTENT_NOT_SET;
	if(!data->datahash) return WRITE_FAIL_CONTENT_NOT_SET;
	if(!data->data) return WRITE_FAIL_CONTENT_NOT_SET;
	if(data->datalength == 0) return WRITE_FAIL_CONTENT_NOT_SET;
	
	gchar* filename = g_build_filename(configure_get_jammo_directory(),TSRV_DATABASE,NULL);
	GError* error = NULL;
	gchar* content = NULL;
	guint contentlength = 0;
	
	// Check for previous profile file
	if(g_file_test(filename,(G_FILE_TEST_EXISTS)))
	{
		if(g_remove(filename) != 0)
		{
			// Error, change username?
			g_free(filename);
			return WRITE_FAIL_CANNOT_REMOVE_FILE;
		}
	}
	
	contentlength = AES_SALT_LENGTH + SHA_HASH_LENGTH + SHA_HASH_LENGTH + data->datalength;
			
	content = g_new0(gchar,sizeof(gchar) * contentlength);

	// Set salt
	*(guint32*)&content[0] = *(guint32*)&data->salt[0];
	*(guint32*)&content[sizeof(guint32)] = *(guint32*)&data->salt[sizeof(guint32)];

	// Set password hash
	g_memmove(&content[AES_SALT_LENGTH],data->pwhash,SHA_HASH_LENGTH);
	// Set profile hash
	g_memmove(&content[AES_SALT_LENGTH + SHA_HASH_LENGTH],data->datahash,SHA_HASH_LENGTH);
	// Set profile
	g_memmove(&content[AES_SALT_LENGTH + SHA_HASH_LENGTH + SHA_HASH_LENGTH],data->data,data->datalength);
	

	// Set content
  if(!g_file_set_contents(filename,content,contentlength,&error))
  {
  	if(error)
		{
			// print error
			g_error_free(error);
			g_free(content);
			g_free(filename);
			return WRITE_FAIL_CANNOT_SET_FILE_CONTENT;
		}
  }
  g_free(content);
	g_free(filename);
	return WRITE_OK;
}

/**
 * gems_teacher_server_write_profile_database:
 * @data: Teacher database
 *
 * Read the encrypted database from the disk from directory 
 * configure_get_jammo_directory()/%TSRV_DATABASE. When %READ_OK is returned
 * decrypt the database with gems_teacher_server_decrypt_profiles().
 *
 * Returns: GemsTURead value.
 */
GemsTURead gems_teacher_server_read_profile_database(teacher_server_profile_data** data)
{
	gchar* filename = g_build_filename(configure_get_jammo_directory(),TSRV_DATABASE,NULL);
	gchar* contents = NULL;
	guint length = 0;
	GError* error = NULL;
	gint rval = READ_OK;
	
	// Test if profile file exists;
	if(g_file_test(filename,G_FILE_TEST_IS_REGULAR))
	{
		// Get the file content
		if(!g_file_get_contents(filename,&contents,(gsize*)&length,&error))
		{
			// Cannot read, TODO report error.
			if(error) g_error_free(error);
			rval = READ_FAIL_CANNOT_READ;
		}
		else
		{
			// Extract salt
			if((*data)->salt != NULL) g_free((*data)->salt);
			(*data)->salt = g_new0(guchar,sizeof(guchar) * AES_SALT_LENGTH);
			*(guint32*)&(*data)->salt[0] = *(guint32*)&contents[0];
			*(guint32*)&(*data)->salt[sizeof(guint32)] = *(guint32*)&contents[sizeof(guint32)];
			
			// Extract password hash
			if((*data)->pwhash != NULL) g_free((*data)->pwhash);
			(*data)->pwhash = g_new0(guchar,sizeof(guchar) * (SHA_HASH_LENGTH + 1));
			g_memmove((*data)->pwhash,&contents[AES_SALT_LENGTH],SHA_HASH_LENGTH);
			
			// Extract profile hash
			if((*data)->datahash != NULL) g_free((*data)->datahash);
			(*data)->datahash = g_new0(guchar,sizeof(guchar) * (SHA_HASH_LENGTH + 1));
			g_memmove((*data)->datahash,&contents[AES_SALT_LENGTH + SHA_HASH_LENGTH],SHA_HASH_LENGTH);
			
			// Extract encrypted profile
			if((*data)->data != NULL) g_free((*data)->data);
			(*data)->datalength = length - SHA_HASH_LENGTH - SHA_HASH_LENGTH - AES_SALT_LENGTH;
			(*data)->data = g_new0(guchar,sizeof(guchar) * ((*data)->datalength));
			g_memmove((*data)->data,&contents[AES_SALT_LENGTH + SHA_HASH_LENGTH + SHA_HASH_LENGTH],(*data)->datalength);
			
			rval = READ_OK;
		}
	}
	else
	{
		rval = READ_FAIL_FILE_NOT_EXIST;
	}
	
	if(contents != NULL) g_free(contents);
	if(filename != NULL) g_free(filename);

	return rval;
}
