#ifndef __SONGBANK_COMM_H__
#define __SONGBANK_COMM_H__

#include <peerhood/IteratorWrapper.h>	
#include "communication.h"

#include "gems_structures.h"
#include "gems_service_songbank.h"

#include <stdio.h>
#include <string.h>
#include <pthread.h>

#define REQ_BUFFER_SIZE 200
#define MAX_SB_CONNECTIONS 10

/**** parameters for g_timeout_add_full ****/
#define SB_COMM_PRIORITY G_PRIORITY_HIGH
#define SB_COMM_INTERVAL 200
#define SB_COMM_DESTROY_NOTIFY NULL

SongBankComm* sbComm;

int SongBankComm_init( SongBankComm* _sbComm, MPeerHood* _peerHood );//gems_service_songbank _mySBobj,
int SongBankComm_clean();

/*	other public methods
bool RequestFile(const string&, const string& );//TODO maybe this should be divided into request and download
//TODO SaveToServer()
//TODO GetFileList() //from some other device
void Notify (TEventType aEvent, const string &aAddress);
*/

void NewSBConnection( const unsigned short aPort, MAbstractConnection *aConnection, int aConnectionId );

//	private methods
gboolean gems_service_songbank_serveRequest( gpointer* );
int SetupPeerhood(SongBankComm* );//gems_components* data);
int GetFileFromNetwork(const char* filename, const char* author, const char* saveToFile);
int AskFileFromDevice(gint connSocket, const char* filename, const char* author );

//static int RecieveRequest( int connSocket, char* name, char* author );
int RecieveRequest( int connSocket, char* request, int* nameLength, int* authorLength );
void ParseRequest( char* request, char* name, int nameLength, char* author, int authorLength );
int	SendResponse( int connSocket, const char* filePath );
int SendFile( int connSocket, const char* filename );
int RecieveFile( int connSocket, const char* fileSavePath );


#endif // __SONGBANKCOMM_H__
