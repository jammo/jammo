/*
 * gems_utils.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2011 University of Oulu, Lappeenranta University of Technology
 *
 * Authors: Jussi Laakkonen <jussi.laakkonen@lut.fi>
 */
 
#include <string.h>
#include <stdio.h>
#include "gems_utils.h"
#include "gems_definitions.h"
#include "../configure.h"
#include "../cem/cem.h"

gchar** list_of_profiles = NULL;
guint list_of_profiles_size = 0;

/**
 * gems_all_integer:
 * @c: charstring to check
 *
 * Check if all characters in @c are digits.
 *
 * Returns: TRUE, all digits, FALSE, at least one is not digit.
 */
gboolean gems_all_integers(gchar* c)
{
	if(c == NULL) return FALSE;
	
	for(gint i = 0; i < strlen(c); i++) if(!g_ascii_isdigit(c[i])) return FALSE;
	return TRUE;
}

/**
 * gems_erase_newline:
 * @c: charstring where newline is erased
 *
 * Erase newline marker from given charstring @c. Searches the charstring @c
 * for newline marker and replaces it with NULL char.
 *
 * Returns: void
 */
void gems_erase_newline(gchar* c)
{
	if(c == NULL) return;
	
	// Search for '\n'
	gchar* pos = g_strrstr(c,"\n");
	// Set '\n' -> '\0'
	if(pos) pos[0] = '\0'; 
}

/**
 * gems_check_illegal_characters:
 * @data: Charstring to check for illegal characters
 *
 * Checks if the given @data contains any illegal characters defined in 
 * %ILLEGAL_CHARS.
 *
 * Returns: TRUE if at least one illegal character is found, FALSE if the
 * @data is "clean".
 */
gboolean gems_check_illegal_characters(const gchar* data)
{
	if(data == NULL) return FALSE;
	
	// Illegal characters
	//gchar illegal[ILLEGAL_CHAR_COUNT] = ILLEGAL_CHARS;
	
	for(gint i = 0; i < ILLEGAL_CHAR_COUNT; i++)
	{
		for(gint j = 0; j < strlen(data); j++)
		{
			// found
			if(data[j] == ILLEGAL_CHARS[i]) return TRUE;
		}
	}
	// Not found
	return FALSE;
}

/**
 * gems_list_saved_profiles:
 * @count: Amount of profiles in the returned list is stored into this variable
 *
 * Searches all compatible profiles from configure_get_jammo_directory()/
 * %JAMMOPROFILE directory. The profile list is saved into static variable and
 * is retrieved only once unless gems_free_list_saved_profiles() is called.
 *
 * Returns: A pointer array with @count amount of elements, each element is a
 * profile username.
 */
gchar** gems_list_saved_profiles(guint* count)
{
	if(count == NULL) return FALSE;
	
	// Exists, return it
	if(list_of_profiles)
	{
		*count = list_of_profiles_size;
		return list_of_profiles;
	}
	
	// Profile path
	gchar* path = g_build_filename(configure_get_jammo_directory(),JAMMOPROFILE,NULL);
	
	// Get extension, ignore first '.'
	gchar* profile_ext = g_strdup(&PROFILE_FILE_EXT[1]);
	
	GError** error = NULL;
	GDir* dir = NULL;
	
	// Try to open dir
	if((dir = g_dir_open(path,0,error)))
	{
		const gchar* file = NULL;
		GList* profiles = NULL;
		
		for(file = g_dir_read_name(dir); file ; file = g_dir_read_name(dir))
		{
			gchar* full = g_build_filename(path,file,NULL);
			
			// Check that file is regular
			if(g_file_test(full,G_FILE_TEST_IS_REGULAR))
			{
				// Split the file into two
				gchar** splitted = g_strsplit(file,".",2);
				
				if(g_strcmp0(splitted[1],profile_ext) == 0)
				{
					// check if version is ok
					if(gems_check_profile_version(full))
					{
						// Add to list
						profiles = g_list_insert_sorted_with_data(profiles,g_strdup(splitted[0]),(GCompareDataFunc)g_ascii_strcasecmp,NULL);
						list_of_profiles_size++;
					}
				}
				g_strfreev(splitted);
			}
			g_free(full);
		}
		
		if(list_of_profiles_size > 0)
		{
			list_of_profiles = g_new(gchar*, list_of_profiles_size + 1);
			list_of_profiles[list_of_profiles_size - 1] = NULL;
			
			GList* iter = NULL;
			guint i = 0;
			
			for(iter = g_list_first(profiles); iter ; iter = g_list_next(iter))
			{
				list_of_profiles[i] = iter->data;
				i++;
			}
		}
		
		if(profiles) g_list_free(profiles);
		
		g_dir_close(dir);
	}
	// Error, clear it and free allocated list, return NULL
	else
	{
		gchar* logmsg = NULL;
		if(error) logmsg = g_strdup_printf("Error reading profile directory, reason: %s",(*error)->message);
		else logmsg = g_strdup_printf("Error reading profile directory, error unknown");
		cem_add_to_log(logmsg,J_LOG_ERROR);
		
		g_clear_error(error);
		g_free(profile_ext);
		g_free(path);
		gems_free_list_saved_profiles();
		return NULL;
	}
	
	g_free(profile_ext);
	g_free(path);
	*count = list_of_profiles_size;
	return list_of_profiles;
}

/**
 * gems_free_list_saved_profiles:
 *
 * Free the list of saved profiles.
 *
 * Returns: void
 */
void gems_free_list_saved_profiles()
{
	
	if(list_of_profiles)
	{
		for(gint i = 0; i < list_of_profiles_size; i++) if(list_of_profiles[i]) g_free(list_of_profiles[i]);
		g_free(list_of_profiles);
		list_of_profiles = NULL;
		list_of_profiles_size = 0;
	}
}

/**
 * gems_check_profile_version:
 * @filename: Full path to profile file
 *
 * Check the version of the given profile @filename. Reads the first byte from
 * the profile @filename and checks if it can be used with current application
 * version.
 *
 * Returns: TRUE when profile with @filename is compatible, FALSE otherwise.
 */
gboolean gems_check_profile_version(gchar* filename)
{
	gboolean rval = FALSE;
	GError* error = NULL;

	GIOChannel* file = g_io_channel_new_file(filename,"r",&error);
	
	if(file)
	{
		gchar* buf = g_new0(gchar,2);
		guint bytes_read = 0;
		
		if(g_io_channel_read_chars(file,buf,1,(gsize*)&bytes_read,&error) == G_IO_STATUS_NORMAL)
		{
			if(bytes_read == 1)
			{
				guint8 version = *(guint8*)&buf[0];
				if(version == JAMMO_PROFILE_VERSION) rval = TRUE;
			}
		}
		else if(error) g_clear_error(&error);
		
		g_free(buf);
	}
	else if(error) g_clear_error(&error);
	
	g_io_channel_unref(file);
	
	return rval;
}

/**
 * gems_guaranteed_memset:
 * @data: Data to be changed
 * @value: Value to set to @data
 * @length: Amount of @value:s to set to @data
 *
 * A guaranteed memset, sets the @length amount of bytes of @data to @value.  
 * Should guarantee that secret is erased from memory even with compiler
 * optimizations, from: http://www.dwheeler.com/secure-programs/Secure-Programs-
 * HOWTO/protect-secrets.html
 *
 * Returns: void* to @data
 */
void *gems_guaranteed_memset(void *data, gint value, guint length)
{
	if(!data || length == 0) return data;
	volatile gchar *buffer = data;
	while(length--) *buffer++ = value;
	return data;
}

