/*
 * jammo-mentor-activity.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010-2011 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#ifndef __JAMMO_MENTOR_ACTIVITY_H__
#define __JAMMO_MENTOR_ACTIVITY_H__

#include <tangle.h>
#include "jammo-mentor.h"

#define JAMMO_TYPE_MENTOR_ACTIVITY (jammo_mentor_activity_get_type())
#define JAMMO_MENTOR_ACTIVITY(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), JAMMO_TYPE_MENTOR_ACTIVITY, JammoMentorActivity))
#define JAMMO_IS_MENTOR_ACTIVITY(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), JAMMO_TYPE_MENTOR_ACTIVITY))
#define JAMMO_MENTOR_ACTIVITY_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), JAMMO_TYPE_MENTOR_ACTIVITY, JammoMentorActivityClass))
#define JAMMO_IS_MENTOR_ACTIVITY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), JAMMO_TYPE_MENTOR_ACTIVITY))
#define JAMMO_MENTOR_ACTIVITY_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), JAMMO_TYPE_MENTOR_ACTIVITY, JammoMentorActivityClass))

typedef struct _JammoMentorActivityPrivate JammoMentorActivityPrivate;

typedef struct _JammoMentorActivity {
	TangleActivity parent_instance;
	JammoMentorActivityPrivate* priv;
} JammoMentorActivity;

typedef struct _JammoMentorActivityClass {
	TangleActivityClass parent_class;
	
	void (*spoken)(JammoMentorActivity* mentor_activity, const gchar* speech);
	void (*interrupted)(JammoMentorActivity* mentor_activity, const gchar* speech);
} JammoMentorActivityClass;

GType jammo_mentor_activity_get_type(void) G_GNUC_CONST;

TangleActivity* jammo_mentor_activity_new(void);

JammoMentor* jammo_mentor_activity_get_mentor(JammoMentorActivity* mentor_activity);
void jammo_mentor_activity_set_mentor(JammoMentorActivity* mentor_activity, JammoMentor* mentor);
const gchar* jammo_mentor_activity_get_speech(JammoMentorActivity* mentor_activity);
void jammo_mentor_activity_set_speech(JammoMentorActivity* mentor_activity, const gchar* speech);
gboolean jammo_mentor_activity_get_speak_once(JammoMentorActivity* mentor_activity);
void jammo_mentor_activity_set_speak_once(JammoMentorActivity* mentor_activity, gboolean once);
const gchar* jammo_mentor_activity_get_idle_speech(JammoMentorActivity* mentor_activity);
void jammo_mentor_activity_set_idle_speech(JammoMentorActivity* mentor_activity, const gchar* speech);
guint jammo_mentor_activity_get_duration(JammoMentorActivity* mentor_activity);
void jammo_mentor_activity_set_duration(JammoMentorActivity* mentor_activity, guint duration);
void jammo_mentor_activity_get_highlight(JammoMentorActivity* mentor_activity, gfloat* highlight_x_return, gfloat* highlight_y_return);
void jammo_mentor_activity_set_highlight(JammoMentorActivity* mentor_activity, gfloat highlight_x, gfloat highlight_y);
gboolean jammo_mentor_activity_get_highlight_set(JammoMentorActivity* mentor_activity);
void jammo_mentor_activity_set_highlight_set(JammoMentorActivity* mentor_activity, gboolean is_set);

#endif
