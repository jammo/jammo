/*
 * jammo-mentor.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#ifndef __JAMMO_MENTOR_H__
#define __JAMMO_MENTOR_H__

#include <tangle.h>

#define JAMMO_TYPE_MENTOR (jammo_mentor_get_type())
#define JAMMO_MENTOR(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), JAMMO_TYPE_MENTOR, JammoMentor))
#define JAMMO_IS_MENTOR(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), JAMMO_TYPE_MENTOR))
#define JAMMO_MENTOR_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), JAMMO_TYPE_MENTOR, JammoMentorClass))
#define JAMMO_IS_MENTOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), JAMMO_TYPE_MENTOR))
#define JAMMO_MENTOR_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), JAMMO_TYPE_MENTOR, JammoMentorClass))

typedef struct _JammoMentorPrivate JammoMentorPrivate;

typedef struct _JammoMentor {
	TangleButton parent_instance;
	JammoMentorPrivate* priv;
} JammoMentor;

typedef struct _JammoMentorClass {
	TangleButtonClass parent_class;
} JammoMentorClass;

GType jammo_mentor_get_type(void) G_GNUC_CONST;

typedef void (*JammoMentorSpokenCallback)(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer user_data);

ClutterActor* jammo_mentor_new(ClutterActor* actor, gfloat idle_scale, gfloat active_scale);

gfloat jammo_mentor_get_standby_scale(JammoMentor* mentor);
void jammo_mentor_set_standby_scale(JammoMentor* mentor, gfloat scale);
gfloat jammo_mentor_get_active_scale(JammoMentor* mentor);
void jammo_mentor_set_active_scale(JammoMentor* mentor, gfloat scale);
void jammo_mentor_get_hotspot(JammoMentor* mentor, gfloat* hotspot_x_return, gfloat* hotspot_y_return);
void jammo_mentor_set_hotspot(JammoMentor* mentor, gfloat hotspot_x, gfloat hotspot_y);
void jammo_mentor_speak(JammoMentor* mentor, const gchar* speech);
void jammo_mentor_speak_with_callback(JammoMentor* mentor, const gchar* speech, JammoMentorSpokenCallback callback, gpointer user_data);
void jammo_mentor_speak_and_highlight(JammoMentor* mentor, const gchar* speech, gfloat highlight_x, gfloat highlight_y);
void jammo_mentor_speak_and_highlight_with_callback(JammoMentor* mentor, const gchar* speech, gfloat highlight_x, gfloat highlight_y, JammoMentorSpokenCallback callback, gpointer user_data);
void jammo_mentor_highlight(JammoMentor* mentor, gfloat highlight_x, gfloat highlight_y, guint duration);
void jammo_mentor_highlight_with_callback(JammoMentor* mentor, gfloat highlight_x, gfloat highlight_y, guint duration, JammoMentorSpokenCallback callback, gpointer user_data);
void jammo_mentor_speak_once(JammoMentor* mentor, const gchar* speech);
void jammo_mentor_speak_once_with_callback(JammoMentor* mentor, const gchar* speech, JammoMentorSpokenCallback callback, gpointer user_data);
void jammo_mentor_speak_and_highlight_once(JammoMentor* mentor, const gchar* speech, gfloat highlight_x, gfloat highlight_y);
void jammo_mentor_speak_and_highlight_once_with_callback(JammoMentor* mentor, const gchar* speech, gfloat highlight_x, gfloat highlight_y, JammoMentorSpokenCallback callback, gpointer user_data);
void jammo_mentor_shut_up(JammoMentor* mentor);
const gchar* jammo_mentor_get_idle_speech(JammoMentor* mentor);
void jammo_mentor_set_idle_speech(JammoMentor* mentor, const gchar* speech);
void jammo_mentor_set_idle_speech_with_callback(JammoMentor* mentor, const gchar* speech, JammoMentorSpokenCallback callback, gpointer user_data);
gulong jammo_mentor_get_idle_timeout(JammoMentor* mentor);
void jammo_mentor_set_idle_timeout(JammoMentor* mentor, gulong milliseconds);
const gchar* jammo_mentor_get_language(JammoMentor* mentor);
void jammo_mentor_set_language(JammoMentor* mentor, const gchar* language);
gboolean jammo_mentor_get_passive(JammoMentor* mentor);
void jammo_mentor_set_passive(JammoMentor* mentor, gboolean passive);
gboolean jammo_mentor_get_device_locked(JammoMentor* mentor);

JammoMentor* jammo_mentor_get_default(void);

#endif
