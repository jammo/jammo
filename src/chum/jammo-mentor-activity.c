/*
 * jammo-mentor-activity.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010-2011 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "jammo-mentor-activity.h"

G_DEFINE_TYPE(JammoMentorActivity, jammo_mentor_activity, TANGLE_TYPE_ACTIVITY);

enum {
	PROP_0,
	PROP_MENTOR,
	PROP_SPEECH,
	PROP_SPEAK_ONCE,
	PROP_IDLE_SPEECH,
	PROP_DURATION,
	PROP_HIGHLIGHT_X,
	PROP_HIGHLIGHT_Y,
	PROP_HIGHLIGHT_SET
};

enum {
	SPOKEN,
	INTERRUPTED,
	LAST_SIGNAL
};

struct _JammoMentorActivityPrivate {
	JammoMentor* mentor;
	gchar* speech;
	gchar* idle_speech;
	guint duration;
	gfloat highlight_x;
	gfloat highlight_y;
	guint speak_once : 1;
	guint highlight : 1;
};

static void on_mentor_spoken(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer user_data);

static guint signals[LAST_SIGNAL] = { 0 };

TangleActivity* jammo_mentor_activity_new(void) {

	return TANGLE_ACTIVITY(g_object_new(JAMMO_TYPE_MENTOR_ACTIVITY, NULL));
}

JammoMentor* jammo_mentor_activity_get_mentor(JammoMentorActivity* mentor_activity) {
	g_return_val_if_fail(JAMMO_IS_MENTOR_ACTIVITY(mentor_activity), NULL);

	return mentor_activity->priv->mentor;
}

void jammo_mentor_activity_set_mentor(JammoMentorActivity* mentor_activity, JammoMentor* mentor) {
	g_return_if_fail(JAMMO_IS_MENTOR_ACTIVITY(mentor_activity));
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	if (mentor_activity->priv->mentor != mentor) {
		if (mentor_activity->priv->mentor) {
			g_object_unref(mentor_activity->priv->mentor);
		}
		mentor_activity->priv->mentor = mentor;
		g_object_ref(mentor_activity->priv->mentor);
		g_object_notify(G_OBJECT(mentor_activity), "mentor");
	}
}

const gchar* jammo_mentor_activity_get_speech(JammoMentorActivity* mentor_activity) {
	g_return_val_if_fail(JAMMO_IS_MENTOR_ACTIVITY(mentor_activity), NULL);

	return mentor_activity->priv->speech;
}

void jammo_mentor_activity_set_speech(JammoMentorActivity* mentor_activity, const gchar* speech) {
	g_return_if_fail(JAMMO_IS_MENTOR_ACTIVITY(mentor_activity));

	if (g_strcmp0(mentor_activity->priv->speech, speech)) {
		g_object_freeze_notify(G_OBJECT(mentor_activity));

		g_free(mentor_activity->priv->speech);
		mentor_activity->priv->speech = g_strdup(speech);
		g_object_notify(G_OBJECT(mentor_activity), "speech");
		
		if (speech && mentor_activity->priv->duration > 0) {
			mentor_activity->priv->duration = 0;
			g_object_notify(G_OBJECT(mentor_activity), "duration");
		}

		g_object_thaw_notify(G_OBJECT(mentor_activity));
	}
}

gboolean jammo_mentor_activity_get_speak_once(JammoMentorActivity* mentor_activity) {
	g_return_val_if_fail(JAMMO_IS_MENTOR_ACTIVITY(mentor_activity), FALSE);

	return mentor_activity->priv->speak_once;
}

void jammo_mentor_activity_set_speak_once(JammoMentorActivity* mentor_activity, gboolean once) {
	g_return_if_fail(JAMMO_IS_MENTOR_ACTIVITY(mentor_activity));

	if (mentor_activity->priv->speak_once != once) {
		mentor_activity->priv->speak_once = once;
		g_object_notify(G_OBJECT(mentor_activity), "speak-once");	
	}
}

guint jammo_mentor_activity_get_duration(JammoMentorActivity* mentor_activity) {
	g_return_val_if_fail(JAMMO_IS_MENTOR_ACTIVITY(mentor_activity), 0);

	return mentor_activity->priv->duration;
}

void jammo_mentor_activity_set_duration(JammoMentorActivity* mentor_activity, guint duration) {
	g_return_if_fail(JAMMO_IS_MENTOR_ACTIVITY(mentor_activity));

	if (mentor_activity->priv->duration != duration) {
		g_object_freeze_notify(G_OBJECT(mentor_activity));

		mentor_activity->priv->duration = duration;
		g_object_notify(G_OBJECT(mentor_activity), "duration");

		if (duration > 0 && mentor_activity->priv->speech) {
			g_free(mentor_activity->priv->speech);
			mentor_activity->priv->speech = NULL;
			g_object_notify(G_OBJECT(mentor_activity), "speech");
		}

		g_object_thaw_notify(G_OBJECT(mentor_activity));
	}
}

void jammo_mentor_activity_get_highlight(JammoMentorActivity* mentor_activity, gfloat* highlight_x_return, gfloat* highlight_y_return) {
	g_return_if_fail(JAMMO_IS_MENTOR_ACTIVITY(mentor_activity));

	if (highlight_x_return) {
		*highlight_x_return = mentor_activity->priv->highlight_x;
	}
	if (highlight_y_return) {
		*highlight_y_return = mentor_activity->priv->highlight_y;
	}
}

void jammo_mentor_activity_set_highlight(JammoMentorActivity* mentor_activity, gfloat highlight_x, gfloat highlight_y) {
	g_return_if_fail(JAMMO_IS_MENTOR_ACTIVITY(mentor_activity));

	g_object_freeze_notify(G_OBJECT(mentor_activity));

	if (mentor_activity->priv->highlight_x != highlight_x) {
		mentor_activity->priv->highlight_x = highlight_x;
		g_object_notify(G_OBJECT(mentor_activity), "highlight-x");
	}
	if (mentor_activity->priv->highlight_y != highlight_y) {
		mentor_activity->priv->highlight_y = highlight_y;
		g_object_notify(G_OBJECT(mentor_activity), "highlight-y");
	}
	
	jammo_mentor_activity_set_highlight_set(mentor_activity, TRUE);
	
	g_object_thaw_notify(G_OBJECT(mentor_activity));
}

gboolean jammo_mentor_activity_get_highlight_set(JammoMentorActivity* mentor_activity) {
	g_return_val_if_fail(JAMMO_IS_MENTOR_ACTIVITY(mentor_activity), FALSE);

	return mentor_activity->priv->highlight;
}

void jammo_mentor_activity_set_highlight_set(JammoMentorActivity* mentor_activity, gboolean is_set) {
	g_return_if_fail(JAMMO_IS_MENTOR_ACTIVITY(mentor_activity));

	if (mentor_activity->priv->highlight != is_set) {
		mentor_activity->priv->highlight = is_set;
		g_object_notify(G_OBJECT(mentor_activity), "highlight-set");	
	}
}

const gchar* jammo_mentor_activity_get_idle_speech(JammoMentorActivity* mentor_activity) {
	g_return_val_if_fail(JAMMO_IS_MENTOR_ACTIVITY(mentor_activity), NULL);

	return mentor_activity->priv->idle_speech;
}

void jammo_mentor_activity_set_idle_speech(JammoMentorActivity* mentor_activity, const gchar* speech) {
	g_return_if_fail(JAMMO_IS_MENTOR_ACTIVITY(mentor_activity));

	if (g_strcmp0(mentor_activity->priv->idle_speech, speech)) {
		g_free(mentor_activity->priv->idle_speech);
		mentor_activity->priv->idle_speech = g_strdup(speech);
		g_object_notify(G_OBJECT(mentor_activity), "idle-speech");	
	}
}

static void jammo_mentor_activity_activate(TangleActivity* activity) {
	JammoMentorActivity* mentor_activity;
	JammoMentor* mentor;
	
	mentor_activity = JAMMO_MENTOR_ACTIVITY(activity);
	if (!(mentor = mentor_activity->priv->mentor)) {
		mentor = jammo_mentor_get_default();
	}
	
	g_return_if_fail(mentor != NULL);
	g_return_if_fail(mentor_activity->priv->speech != NULL ||
	                 mentor_activity->priv->idle_speech != NULL ||
			 mentor_activity->priv->duration > 0);
	
	jammo_mentor_shut_up(mentor);
	if (mentor_activity->priv->highlight) {
		if (mentor_activity->priv->speech) {
			if (mentor_activity->priv->speak_once) {
				jammo_mentor_speak_and_highlight_once_with_callback(mentor, mentor_activity->priv->speech, mentor_activity->priv->highlight_x, mentor_activity->priv->highlight_y, on_mentor_spoken, mentor_activity);
			} else {
				jammo_mentor_speak_and_highlight_with_callback(mentor, mentor_activity->priv->speech, mentor_activity->priv->highlight_x, mentor_activity->priv->highlight_y, on_mentor_spoken, mentor_activity);
			}
		} else if (mentor_activity->priv->duration > 0) {
			jammo_mentor_highlight_with_callback(mentor, mentor_activity->priv->highlight_x, mentor_activity->priv->highlight_y, mentor_activity->priv->duration, on_mentor_spoken, mentor_activity);
		}
	} else if (mentor_activity->priv->speech) {
		if (mentor_activity->priv->speak_once) {
			jammo_mentor_speak_once_with_callback(mentor, mentor_activity->priv->speech, on_mentor_spoken, mentor_activity);
		} else {
			jammo_mentor_speak_with_callback(mentor, mentor_activity->priv->speech, on_mentor_spoken, mentor_activity);
		}
	}
	
	if (mentor_activity->priv->idle_speech) {
		jammo_mentor_set_idle_speech(mentor, mentor_activity->priv->idle_speech);
	}
}

static void jammo_mentor_activity_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoMentorActivity* mentor_activity;
	
	mentor_activity = JAMMO_MENTOR_ACTIVITY(object);

	switch (prop_id) {
		case PROP_MENTOR:
			jammo_mentor_activity_set_mentor(mentor_activity, JAMMO_MENTOR(g_value_get_object(value)));
			break;
		case PROP_SPEECH:
			jammo_mentor_activity_set_speech(mentor_activity, g_value_get_string(value));
			break;
		case PROP_SPEAK_ONCE:
			jammo_mentor_activity_set_speak_once(mentor_activity, g_value_get_boolean(value));
			break;
		case PROP_IDLE_SPEECH:
			jammo_mentor_activity_set_idle_speech(mentor_activity, g_value_get_string(value));
			break;
		case PROP_DURATION:
			jammo_mentor_activity_set_duration(mentor_activity, g_value_get_uint(value));
			break;
		case PROP_HIGHLIGHT_X:
			jammo_mentor_activity_set_highlight(mentor_activity, g_value_get_float(value), mentor_activity->priv->highlight_y);
			break;
		case PROP_HIGHLIGHT_Y:
			jammo_mentor_activity_set_highlight(mentor_activity, mentor_activity->priv->highlight_x, g_value_get_float(value));
			break;
		case PROP_HIGHLIGHT_SET:
			jammo_mentor_activity_set_highlight_set(mentor_activity, g_value_get_boolean(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_mentor_activity_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoMentorActivity* mentor_activity;

	mentor_activity = JAMMO_MENTOR_ACTIVITY(object);

        switch (prop_id) {
		case PROP_MENTOR:
			g_value_set_object(value, mentor_activity->priv->mentor);
			break;
		case PROP_SPEECH:
			g_value_set_string(value, mentor_activity->priv->speech);
			break;
		case PROP_SPEAK_ONCE:
			g_value_set_boolean(value, mentor_activity->priv->speak_once);
			break;
		case PROP_IDLE_SPEECH:
			g_value_set_string(value, mentor_activity->priv->idle_speech);
			break;
		case PROP_DURATION:
			g_value_set_uint(value, mentor_activity->priv->duration);
			break;
		case PROP_HIGHLIGHT_X:
			g_value_set_float(value, mentor_activity->priv->highlight_x);
			break;
		case PROP_HIGHLIGHT_Y:
			g_value_set_float(value, mentor_activity->priv->highlight_y);
			break;
		case PROP_HIGHLIGHT_SET:
			g_value_set_boolean(value, mentor_activity->priv->highlight);
			break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static void jammo_mentor_activity_finalize(GObject* object) {
	G_OBJECT_CLASS(jammo_mentor_activity_parent_class)->finalize(object);
}

static void jammo_mentor_activity_dispose(GObject* object) {
	G_OBJECT_CLASS(jammo_mentor_activity_parent_class)->dispose(object);
}

static void jammo_mentor_activity_class_init(JammoMentorActivityClass* mentor_activity_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(mentor_activity_class);
	TangleActivityClass* activity_class = TANGLE_ACTIVITY_CLASS(mentor_activity_class);

	gobject_class->finalize = jammo_mentor_activity_finalize;
	gobject_class->dispose = jammo_mentor_activity_dispose;
	gobject_class->set_property = jammo_mentor_activity_set_property;
	gobject_class->get_property = jammo_mentor_activity_get_property;

	activity_class->activate = jammo_mentor_activity_activate;
	
	/**
	 * JammoMentorActivity:mentor:
	 *
	 * If :mentor is NULL, the default mentor (see jammo_mentor_get_default()) is used.
	 */
	g_object_class_install_property(gobject_class, PROP_MENTOR,
	                                g_param_spec_object("mentor",
	                                                    "Mentor",
	                                                    "The mentor",
	                                                    JAMMO_TYPE_MENTOR,
	                                                    G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	/**
	 * JammoMentorActivity:speech:
	 *
	 * The speech that the mentor should speak when the mentor_activity is executed.
	 * Can be NULL, if the purpose of the mentor_activity is only to set idle speech.
	 * As a side effect, setting a non-null speech resets duration back to zero.
	 */
	g_object_class_install_property(gobject_class, PROP_SPEECH,
	                                g_param_spec_string("speech",
	                                                    "Speech",
	                                                    "The speech that the mentor should speak when the mentor_activity is executed",
	                                                    NULL,
	                                                    G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	/**
	 * JammoMentorActivity:speak-once:
	 *
	 * Whether the speech should be spoken only once in a lifetime of a mentor
	 */
	g_object_class_install_property(gobject_class, PROP_SPEAK_ONCE,
	                                g_param_spec_boolean("speak-once",
	                                                     "Speak once",
	                                                     "Whether the speech should be spoken only once in a lifetime of a mentor",
	                                                     FALSE,
	                                                     G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	/**
	 * JammoMentorActivity:idle-speech:
	 *
	 * The speech that the mentor should speak when an user is idling.
	 * Leave this NULL, if you do not want to change idle speech when the mentor_activity is executed.
	 */
	g_object_class_install_property(gobject_class, PROP_IDLE_SPEECH,
	                                g_param_spec_string("idle-speech",
	                                                    "Idle speech",
	                                                    "The speech that the mentor should speak when an user is idling",
	                                                    NULL,
	                                                    G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	/**
	 * JammoMentorActivity:duration:
	 *
	 * The duration of a movement related to highlighting.
	 * Leave this zero, if you do not want to move a mentor when the mentor_activity is executed.
	 * As a side effect, setting a non-zero duration resets speech back to null.
	 */
	g_object_class_install_property(gobject_class, PROP_DURATION,
	                                g_param_spec_uint("duration",
	                                                  "Duration",
	                                                  "The duration of a movement related to highlighting",
	                                                  0, G_MAXUINT, 0,
	                                                  G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	/**
	 * JammoMentorActivity:highlight-x:
	 *
	 * The horizontal coordinate of the highlight position.
	 * Setting this property sets also a :highlight-set property TRUE as a side effect.
	 */
	g_object_class_install_property(gobject_class, PROP_HIGHLIGHT_X,
	                                g_param_spec_float("highlight-x",
	                                                   "Highlight X",
	                                                   "The horizontal coordinate of the highlight position",
	                                                   -G_MAXFLOAT, G_MAXFLOAT, 0.0,
	                                                   G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	/**
	 * JammoMentorActivity:highlight-y:
	 *
	 * The vertical coordinate of the highlight position.
	 * Setting this property sets also a :highlight-set property TRUE as a side effect.
	 */
	g_object_class_install_property(gobject_class, PROP_HIGHLIGHT_Y,
	                                g_param_spec_float("highlight-y",
	                                                   "Highlight Y",
	                                                   "The vertical coordinate of the highlight position",
	                                                   -G_MAXFLOAT, G_MAXFLOAT, 0.0,
	                                                   G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	/**
	 * JammoMentorActivity:highlight-set:
	 *
	 * Whether the mentor highlights the given position.
	 * See :highlight-x and :highlight-y.
	 */
	g_object_class_install_property(gobject_class, PROP_HIGHLIGHT_SET,
	                                g_param_spec_boolean("highlight-set",
	                                                     "Highlight Set",
	                                                     "Whether the mentor highlights the given position",
	                                                     FALSE,
	                                                     G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

	/**
	 * JammoMentorActivity::spoken:
	 * @mentor_activity: the object which received the signal
	 *
	 * The ::spoken signal is emitted when the mentor_activity with :speech property has
	 * been completed (the mentor has spoken).
	 */
	signals[SPOKEN] = g_signal_new("spoken", G_TYPE_FROM_CLASS(gobject_class),
	                               G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(JammoMentorActivityClass, spoken),
				       NULL, NULL,
				       g_cclosure_marshal_VOID__STRING,
				       G_TYPE_NONE, 1,
				       G_TYPE_STRING);
	/**
	 * JammoMentorActivity::interrupted:
	 * @mentor_activity: the object which received the signal
	 *
	 * The ::interrupted signal is emitted when the mentor_activity with :speech property has
	 * been completed (the mentor has been interrupted).
	 */
	signals[INTERRUPTED] = g_signal_new("interrupted", G_TYPE_FROM_CLASS(gobject_class),
	                        	    G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(JammoMentorActivityClass, interrupted),
					    NULL, NULL,
					    g_cclosure_marshal_VOID__STRING,
					    G_TYPE_NONE, 1,
					    G_TYPE_STRING);
						   
	g_type_class_add_private (gobject_class, sizeof (JammoMentorActivityPrivate));
}

static void jammo_mentor_activity_init(JammoMentorActivity* mentor_activity) {
	mentor_activity->priv = G_TYPE_INSTANCE_GET_PRIVATE(mentor_activity, JAMMO_TYPE_MENTOR_ACTIVITY, JammoMentorActivityPrivate);
}

static void on_mentor_spoken(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer user_data) {
	JammoMentorActivity* mentor_activity;
	
	mentor_activity = JAMMO_MENTOR_ACTIVITY(user_data);
	
	if (interrupted) {
		g_signal_emit(mentor_activity, signals[INTERRUPTED], 0, speech);
	} else {
		g_signal_emit(mentor_activity, signals[SPOKEN], 0, speech);
	}
}
