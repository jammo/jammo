

#include <glib.h>
#include <gmodule.h>
#include <stdlib.h>
#include <clutter/clutter.h>
#include <cogl/cogl.h>

#include "jammo-contour-area.h"

/* private declaration
 *--------------------------------------------------*/


G_DEFINE_TYPE (JammoContourArea, jammo_contour_area, CLUTTER_TYPE_ACTOR);

#define JAMMO_CONTOUR_AREA_GET_PRIVATE(obj) \
(G_TYPE_INSTANCE_GET_PRIVATE ((obj), JAMMO_TYPE_CONTOUR_AREA, JammoContourAreaPrivate))

struct _JammoContourAreaPrivate
{
  CoglHandle texture_id;
  CoglHandle offscreen_id;
  gint x;
  gint y;
};

/* implementation
 *--------------------------------------------------*/

#define radius  5.0
static void
jammo_contour_area_paint (ClutterActor *self)
{
  JammoContourAreaPrivate *priv = JAMMO_CONTOUR_AREA_GET_PRIVATE (self);

  cogl_set_source_texture (priv->texture_id);

  cogl_set_source_color4ub ( 0xff, 0xff, 0x00, 0xdd);
 //test
 /*
   cogl_rectangle_with_texture_coords (priv->x, priv->y,
                                      priv->x+radius, priv->y+radius,
                                      0,0,1,1);
*/

  //cogl_path_ellipse (priv->x+5, priv->y+15, radius, radius);

  //It will add new point to existing line.
  cogl_path_line_to (priv->x+5, priv->y+15);

  cogl_path_stroke_preserve();  //this will save path to next round
  //cogl_path_stroke();         //this will trash used path after drawing
  //cogl_path_fill (); Fill doesn't work. Whole screen is freezed.
}

static void
jammo_contour_area_finalize (GObject *object)
{
  G_OBJECT_CLASS (jammo_contour_area_parent_class)->finalize (object);
}

static void
jammo_contour_area_dispose (GObject *object)
{
  JammoContourAreaPrivate *priv;

  priv = JAMMO_CONTOUR_AREA_GET_PRIVATE (object);

  cogl_handle_unref (priv->texture_id);
  cogl_handle_unref (priv->offscreen_id);

  G_OBJECT_CLASS (jammo_contour_area_parent_class)->dispose (object);
}

static void
jammo_contour_area_init (JammoContourArea *self)
{
  JammoContourAreaPrivate *priv;
  self->priv = priv = JAMMO_CONTOUR_AREA_GET_PRIVATE(self);

  printf ("Creating texture with size\n");

  CoglTextureFlags flags = COGL_TEXTURE_NO_SLICING; //NO_SLICING is important.
  priv->texture_id = cogl_texture_new_with_size (800,480,
                    flags,
                    COGL_PIXEL_FORMAT_RGB_888);

  if (priv->texture_id == COGL_INVALID_HANDLE)
    printf ("Failed creating texture with size!\n");


  priv->offscreen_id = cogl_offscreen_new_to_texture (priv->texture_id);
  if (priv->offscreen_id == COGL_INVALID_HANDLE)
    printf ("Failed creating offscreen to texture!\n");
}

static void
jammo_contour_area_class_init (JammoContourAreaClass *klass)
{
  GObjectClass      *gobject_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class   = CLUTTER_ACTOR_CLASS (klass);

  gobject_class->finalize     = jammo_contour_area_finalize;
  gobject_class->dispose      = jammo_contour_area_dispose;
  actor_class->paint          = jammo_contour_area_paint;

  g_type_class_add_private (gobject_class, sizeof (JammoContourAreaPrivate));
}


void jammo_contour_area_trace_update(JammoContourArea* contour_trace_area, gfloat X, gfloat Y) {
  JammoContourAreaPrivate *priv = JAMMO_CONTOUR_AREA_GET_PRIVATE (contour_trace_area);
  priv->x=X;
  priv->y=Y;
}

ClutterActor* jammo_contour_area_new (void)
{
  cogl_path_new(); //This will start new path (if there are previous trace)
  return g_object_new (JAMMO_TYPE_CONTOUR_AREA, NULL);
}
