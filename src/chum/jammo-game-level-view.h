/*
 * jammo-game-level-view.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */


#ifndef __JAMMO_GAME_LEVEL_VIEW_H__
#define __JAMMO_GAME_LEVEL_VIEW_H__

#include "jammo-game-level.h"
#include <clutter/clutter.h>

#define JAMMO_TYPE_GAME_LEVEL_VIEW (jammo_game_level_view_get_type())
#define JAMMO_GAME_LEVEL_VIEW(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), JAMMO_TYPE_GAME_LEVEL_VIEW, JammoGameLevelView))
#define JAMMO_IS_GAME_LEVEL_VIEW(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), JAMMO_TYPE_GAME_LEVEL_VIEW))
#define JAMMO_GAME_LEVEL_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), JAMMO_TYPE_GAME_LEVEL_VIEW, JammoGameLevelViewClass))
#define JAMMO_IS_GAME_LEVEL_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), JAMMO_TYPE_GAME_LEVEL_VIEW))
#define JAMMO_GAME_LEVEL_VIEW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), JAMMO_TYPE_GAME_LEVEL_VIEW, JammoGameLevelViewClass))

typedef struct _JammoGameLevelViewPrivate JammoGameLevelViewPrivate;

typedef struct _JammoGameLevelView {
	TangleWidget parent_instance;
	JammoGameLevelViewPrivate* priv;
} JammoGameLevelView;

typedef struct _JammoGameLevelViewClass {
	TangleWidgetClass parent_class;
} JammoGameLevelViewClass;

GType jammo_game_level_view_get_type(void) G_GNUC_CONST;

ClutterActor* jammo_game_level_view_new(JammoGameLevel* game_level, ClutterActor* indicator_actor);

#endif
