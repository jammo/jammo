/*
 * jammo-track-view.c
 *
 * This file is part of JamMo.
 *
 * This is tangle_widget with one extra property: JammoTrack.
 * Can be used, if properly X-track-view is not implemented
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */


#include "jammo-track-view.h"
#include "../meam/jammo-meam.h"
G_DEFINE_TYPE(JammoTrackView, jammo_track_view, TANGLE_TYPE_WIDGET);

enum {
	PROP_0,
	PROP_TRACK,
};

struct _JammoTrackViewPrivate {
	JammoTrack* track;
};

ClutterActor* jammo_track_view_new(JammoTrack* track) {
	return CLUTTER_ACTOR(g_object_new(JAMMO_TYPE_TRACK_VIEW, "track", track, NULL));
}



static void jammo_track_view_constructed(GObject* object) {
	//JammoTrackView* track_view;
	//track_view = JAMMO_TRACK_VIEW(object);
}

static void jammo_track_view_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoTrackView* track_view;

	track_view = JAMMO_TRACK_VIEW(object);

	switch (prop_id) {
		case PROP_TRACK:
			track_view->priv->track = JAMMO_TRACK(g_value_get_object(value));
			g_object_ref(track_view->priv->track);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_track_view_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
	JammoTrackView* track_view;

	track_view = JAMMO_TRACK_VIEW(object);

	switch (prop_id) {
		case PROP_TRACK:
			g_value_set_object(value, track_view->priv->track);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_track_view_finalize(GObject* object) {
	G_OBJECT_CLASS(jammo_track_view_parent_class)->finalize(object);
}

static void jammo_track_view_dispose(GObject* object) {
	G_OBJECT_CLASS(jammo_track_view_parent_class)->dispose(object);
}

static void jammo_track_view_class_init(JammoTrackViewClass* track_view_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(track_view_class);

	gobject_class->constructed = jammo_track_view_constructed;
	gobject_class->finalize = jammo_track_view_finalize;
	gobject_class->dispose = jammo_track_view_dispose;
	gobject_class->set_property = jammo_track_view_set_property;
	gobject_class->get_property = jammo_track_view_get_property;


	/**
	 * JammoTrackView:track:
	 *
	 * The track of this view.
	 */
	g_object_class_install_property(gobject_class, PROP_TRACK,
	                                g_param_spec_object("track",
	                                "Track",
	                                "The track of this view",
	                                JAMMO_TYPE_TRACK,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	g_type_class_add_private (gobject_class, sizeof (JammoTrackViewPrivate));
}

static void jammo_track_view_init(JammoTrackView* track_view) {
	track_view->priv = G_TYPE_INSTANCE_GET_PRIVATE(track_view, JAMMO_TYPE_TRACK_VIEW, JammoTrackViewPrivate);
}

