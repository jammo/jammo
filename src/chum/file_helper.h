/** file_helper.h is part of JamMo.
License: GPLv2, read more from COPYING
*/
#ifndef _FILE_HELPER_H_
#define _FILE_HELPER_H_

#include "jammo-editing-track-view.h"
void save_composition(const gchar* folder, const gchar* to_filename, ClutterActor* sequencer_widget, gchar* extra_data);
void load_this_sample_array_to_this_track_view(JsonNode *sub_node,JammoEditingTrackView* track_view, gboolean reactive);
void load_this_sample_array_to_this_track_view_legacy2(JsonNode *sub_node,JammoEditingTrackView* track_view, gboolean reactive);

GList *file_helper_get_all_songs();
GList *file_helper_get_all_files(const char* directory);
GList *file_helper_get_all_files_without_path(const char* directory);
#endif /* _FILE_HELPER_H_  */
