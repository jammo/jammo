/*
 * jammo-chum.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#ifndef __JAMMO_CHUM_H__
#define __JAMMO_CHUM_H__

#include <clutter/clutter.h>

void jammo_chum_init(int* argc, char** argv[]);

void jammo_chum_disable_actor(ClutterActor* actor);
void jammo_chum_enable_actor(ClutterActor* actor);
void jammo_chum_disable_all_actors(ClutterActor* actor);
void jammo_chum_enable_all_actors(ClutterActor* actor);

gboolean jammo_chum_is_actor_disabled(ClutterActor* actor);

#endif
