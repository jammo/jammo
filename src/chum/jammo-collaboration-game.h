/*
 * jammo-collaboration-game.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther <mikko.gynther@lut.fi>
 */
 
#ifndef __JAMMO_COLLABORATION_GAME_H__
#define __JAMMO_COLLABORATION_GAME_H__

#include <glib.h>
#include <glib-object.h>
#include <clutter/clutter.h>

#define JAMMO_TYPE_COLLABORATION_GAME (jammo_collaboration_game_get_type ())
#define JAMMO_COLLABORATION_GAME(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_COLLABORATION_GAME, JammoCollaborationGame))
#define JAMMO_IS_COLLABORATION_GAME(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_COLLABORATION_GAME))
#define JAMMO_COLLABORATION_GAME_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_COLLABORATION_GAME, JammoCollaborationGameClass))
#define JAMMO_IS_COLLABORATION_GAME_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_COLLABORATION_GAME))
#define JAMMO_COLLABORATION_GAME_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_COLLABORATION_GAME, JammoCollaborationGameClass))

typedef struct _JammoCollaborationGamePrivate JammoCollaborationGamePrivate;

typedef struct _JammoCollaborationGame {
	GInitiallyUnowned parent_instance;
	JammoCollaborationGamePrivate * priv;
} JammoCollaborationGame;

/**
 * JammoCollaborationGameClass:
 * @teacher_exit: virtual function for stopping the game when teacher orders so
 * @create_song_file: virtual function for creating a song file from the game when it has ended
*/

typedef struct _JammoCollaborationGameClass {
	GInitiallyUnownedClass parent_class;

	void (*teacher_exit)();
	int (*create_song_file)();
	void (*game_callback)(GObject * game, int type, const gchar* song_info);
} JammoCollaborationGameClass;

GType jammo_collaboration_game_get_type(void);

void jammo_collaboration_game_teacher_exit(JammoCollaborationGame * collaboration_game);
int jammo_collaboration_game_create_song_file(JammoCollaborationGame * collaboration_game);

// TODO add common functions

ClutterActor* jammo_collaboration_game_user_id_to_track(JammoCollaborationGame* collaboration_game, guint user_id);

void jammo_collaboration_game_add_view(JammoCollaborationGame* collaboration_game, guint user_id, ClutterActor * view);

void jammo_collaboration_game_remove_view(JammoCollaborationGame* collaboration_game, guint user_id);

void jammo_collaboration_game_dump_views(JammoCollaborationGame* collaboration_game);

int jammo_collaboration_game_viewcount(JammoCollaborationGame* collaboration_game);
#endif
