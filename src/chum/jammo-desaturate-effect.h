/* jammo-desaturate-effect.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2011 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#ifndef __JAMMO_DESATURATE_EFFECT_H__
#define __JAMMO_DESATURATE_EFFECT_H__

#include <clutter/clutter.h>

#define JAMMO_TYPE_DESATURATE_EFFECT (jammo_desaturate_effect_get_type())
#define JAMMO_DESATURATE_EFFECT(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), JAMMO_TYPE_DESATURATE_EFFECT, JammoDesaturateEffect))
#define JAMMO_IS_DESATURATE_EFFECT(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), JAMMO_TYPE_DESATURATE_EFFECT))
#define JAMMO_DESATURATE_EFFECT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), JAMMO_TYPE_DESATURATE_EFFECT, JammoDesaturateEffectClass))
#define JAMMO_IS_DESATURATE_EFFECT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), JAMMO_TYPE_DESATURATE_EFFECT))
#define JAMMO_DESATURATE_EFFECT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), JAMMO_TYPE_DESATURATE_EFFECT, JammoDesaturateEffectClass))

typedef struct _JammoDesaturateEffectPrivate JammoDesaturateEffectPrivate;

typedef struct _JammoDesaturateEffect {
	ClutterShaderEffect parent_instance;
	JammoDesaturateEffectPrivate* priv;
} JammoDesaturateEffect;

typedef struct _JammoDesaturateEffectClass {
	ClutterShaderEffectClass parent_class;
} JammoDesaturateEffectClass;

GType jammo_desaturate_effect_get_type(void) G_GNUC_CONST;

ClutterEffect* jammo_desaturate_effect_new(gdouble factor);

void jammo_desaturate_effect_set_factor(JammoDesaturateEffect* desaturate_effect, gdouble factor);
gdouble jammo_desaturate_effect_get_factor(JammoDesaturateEffect* desaturate_effect);

#endif
