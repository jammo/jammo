/*
 * jammo-game-level.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#ifndef __JAMMO_GAME_LEVEL_H__
#define __JAMMO_GAME_LEVEL_H__

#include <clutter/clutter.h>
#include <tangle.h>
#include "jammo-game-task.h"

#define JAMMO_TYPE_GAME_LEVEL (jammo_game_level_get_type())
#define JAMMO_GAME_LEVEL(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), JAMMO_TYPE_GAME_LEVEL, JammoGameLevel))
#define JAMMO_IS_GAME_LEVEL(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), JAMMO_TYPE_GAME_LEVEL))
#define JAMMO_GAME_LEVEL_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), JAMMO_TYPE_GAME_LEVEL, JammoGameLevelClass))
#define JAMMO_IS_GAME_LEVEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), JAMMO_TYPE_GAME_LEVEL))
#define JAMMO_GAME_LEVEL_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), JAMMO_TYPE_GAME_LEVEL, JammoGameLevelClass))

typedef struct _JammoGameLevelPrivate JammoGameLevelPrivate;

typedef struct _JammoGameLevel {
	TangleObject parent_instance;
	JammoGameLevelPrivate* priv;
} JammoGameLevel;

typedef struct _JammoGameLevelClass {
	TangleObjectClass parent_class;
	
	void (*completed)(JammoGameLevel* game_level);
} JammoGameLevelClass;

GType jammo_game_level_get_type(void) G_GNUC_CONST;

JammoGameLevel* jammo_game_level_new(void);

GList* jammo_game_level_get_task_names(JammoGameLevel* game_level);
void jammo_game_level_add_task_name(JammoGameLevel* game_level, const gchar* task_name);

gboolean jammo_game_level_get_task_completed(JammoGameLevel* game_level, const gchar* game_name);
void jammo_game_level_set_task_completed(JammoGameLevel* game_level, const gchar* game_name, gboolean is_completed);

guint jammo_game_level_get_tasks_to_complete(JammoGameLevel* game_level);
guint jammo_game_level_get_tasks_completed(JammoGameLevel* game_level);

JammoGameTask* jammo_game_level_get_active_task(JammoGameLevel* game_level);

void jammo_game_level_start_task(JammoGameLevel* game_level, const gchar* game_name);
gboolean jammo_game_level_start_next_task(JammoGameLevel* game_level);
void jammo_game_level_stop_task(JammoGameLevel* game_level);

#endif
