/*
 * jammo-chum.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "jammo-chum.h"
#include "jammo-desaturate-effect.h"
#include <tangle.h>

typedef struct {
	ClutterEffect* effect;
	gboolean reactive;
} DisabledActorData;

static GQuark disabled_actor_data_quark;

static void on_reactive_notify(GObject* object, GParamSpec* param_spec, gpointer user_data);
static void on_destroy_notify(gpointer data);

void jammo_chum_init(int* argc, char** argv[]) {
	tangle_init(argc, argv);

	disabled_actor_data_quark = g_quark_from_static_string("jammo-chum-disabled-actor-data-quark");
}

void jammo_chum_disable_actor(ClutterActor* actor) {
	DisabledActorData* disabled_actor_data;
	
	if (!(disabled_actor_data = g_object_get_qdata(G_OBJECT(actor), disabled_actor_data_quark))) {
		disabled_actor_data = g_slice_new(DisabledActorData);
		
		disabled_actor_data->effect = jammo_desaturate_effect_new(1.0);
		g_object_ref(disabled_actor_data->effect);
		clutter_actor_add_effect(actor, disabled_actor_data->effect);
		
		disabled_actor_data->reactive = CLUTTER_ACTOR_IS_REACTIVE(actor);
		clutter_actor_set_reactive(actor, FALSE);
		g_signal_connect(actor, "notify::reactive", G_CALLBACK(on_reactive_notify), disabled_actor_data);
		
		g_object_set_qdata_full(G_OBJECT(actor), disabled_actor_data_quark, disabled_actor_data, on_destroy_notify);
	}
}

void jammo_chum_enable_actor(ClutterActor* actor) {
	DisabledActorData* disabled_actor_data;
	
	if ((disabled_actor_data = g_object_get_qdata(G_OBJECT(actor), disabled_actor_data_quark))) {
		g_signal_handlers_disconnect_by_func(actor, on_reactive_notify, disabled_actor_data);
		clutter_actor_set_reactive(actor, disabled_actor_data->reactive);			

		clutter_actor_remove_effect(actor, disabled_actor_data->effect);

		g_object_set_qdata(G_OBJECT(actor), disabled_actor_data_quark, NULL);
	}
}

void jammo_chum_disable_all_actors(ClutterActor* actor) {
	jammo_chum_disable_actor(actor);
	
	if (CLUTTER_IS_CONTAINER(actor)) {
		clutter_container_foreach(CLUTTER_CONTAINER(actor), CLUTTER_CALLBACK(jammo_chum_disable_all_actors), NULL);
	}
}

void jammo_chum_enable_all_actors(ClutterActor* actor) {
	jammo_chum_enable_actor(actor);
	
	if (CLUTTER_IS_CONTAINER(actor)) {
		clutter_container_foreach(CLUTTER_CONTAINER(actor), CLUTTER_CALLBACK(jammo_chum_enable_all_actors), NULL);
	}
}

gboolean jammo_chum_is_actor_disabled(ClutterActor* actor) {

	return g_object_get_qdata(G_OBJECT(actor), disabled_actor_data_quark) != NULL;
}

static void on_reactive_notify(GObject* object, GParamSpec* param_spec, gpointer user_data) {
	ClutterActor* actor;
	DisabledActorData* disabled_actor_data;
	
	actor = CLUTTER_ACTOR(object);
	disabled_actor_data = (DisabledActorData*)user_data;

	g_signal_handlers_block_by_func(object, G_CALLBACK(on_reactive_notify), user_data);

	disabled_actor_data->reactive = CLUTTER_ACTOR_IS_REACTIVE(actor);
	clutter_actor_set_reactive(actor, FALSE);
	
	g_signal_handlers_unblock_by_func(object, G_CALLBACK(on_reactive_notify), user_data);
}

static void on_destroy_notify(gpointer data) {
	DisabledActorData* disabled_actor_data;
	
	disabled_actor_data = (DisabledActorData*) data;
	
	g_object_unref(disabled_actor_data->effect);
	g_slice_free(DisabledActorData, disabled_actor_data);
}
