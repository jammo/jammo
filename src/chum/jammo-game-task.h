/*
 * jammo-game-task.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#ifndef __JAMMO_GAME_TASK_H__
#define __JAMMO_GAME_TASK_H__

#include <tangle.h>

#define JAMMO_TYPE_GAME_TASK (jammo_game_task_get_type())
#define JAMMO_GAME_TASK(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), JAMMO_TYPE_GAME_TASK, JammoGameTask))
#define JAMMO_IS_GAME_TASK(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), JAMMO_TYPE_GAME_TASK))
#define JAMMO_GAME_TASK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), JAMMO_TYPE_GAME_TASK, JammoGameTaskClass))
#define JAMMO_IS_GAME_TASK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), JAMMO_TYPE_GAME_TASK))
#define JAMMO_GAME_TASK_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), JAMMO_TYPE_GAME_TASK, JammoGameTaskClass))

typedef struct _JammoGameTaskPrivate JammoGameTaskPrivate;

typedef struct _JammoGameTask {
	TangleObject parent_instance;
	JammoGameTaskPrivate* priv;
} JammoGameTask;

typedef struct _JammoGameTaskClass {
	TangleObjectClass parent_class;
	void (*started)(JammoGameTask* game);
	void (*act_completed)(JammoGameTask* game, const gchar* task_name, guint tasks_completed);
	void (*act_redone)(JammoGameTask* game, const gchar* task_name, guint tasks_completed);
	void (*completed)(JammoGameTask* game);
} JammoGameTaskClass;

GType jammo_game_task_get_type(void) G_GNUC_CONST;

JammoGameTask* jammo_game_new(TangleView* view);

void jammo_game_task_start(JammoGameTask* task);
void jammo_game_task_stop(JammoGameTask* task);

GList* jammo_game_tasks_get_acts(JammoGameTask* task);
void jammo_game_task_add_act(JammoGameTask* task, const gchar* act_name);

gboolean jammo_game_task_get_act_completed(JammoGameTask* task, const gchar* act_name);
void jammo_game_task_set_act_completed(JammoGameTask* task, const gchar* act_name, gboolean is_completed);

guint jammo_game_task_get_acts_to_complete(JammoGameTask* task);
void jammo_game_task_set_acts_to_complete(JammoGameTask* task, guint number_of_acts);

guint jammo_game_task_get_acts_completed(JammoGameTask* task);

#endif
