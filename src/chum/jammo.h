ClutterActor* jammo_get_actor_by_id(const gchar* id);
GObject* jammo_get_object_by_id(const gchar* id);
void jammo_clutter_actor_flash(ClutterActor* actor);
