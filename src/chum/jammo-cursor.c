/*
 * jammo-cursor.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "jammo-cursor.h"

G_DEFINE_TYPE(JammoCursor, jammo_cursor, TANGLE_TYPE_WIDGET);

enum {
	PROP_0,
	PROP_SEQUENCER,
	PROP_TEXTURE,
	PROP_SEEKABLE
};

struct _JammoCursorPrivate {
	JammoSequencer* sequencer;
	ClutterActor* texture;
	
	gulong sequencer_started_handler_id;
	gulong sequencer_stopped_handler_id;
	guint timeout_source_id;
	
	gfloat seek_start_stage_x;
	gfloat seek_start_texture_x;
	guint motion_event_handler_id;
	guint button_release_event_handler_id;
	
	guint seekable : 1;
	
	guint seeking : 1;
	guint playing : 1;
};

static void manage_texture(JammoCursor* cursor);
static void on_sequencer_started(JammoSequencer* sequencer, gpointer user_data);
static void on_sequencer_stopped(JammoSequencer* sequencer, gpointer user_data);
static gboolean on_button_press_event(ClutterActor* actor, ClutterEvent* event, gpointer user_data);
static gboolean on_motion_event(ClutterActor* actor, ClutterEvent* event, gpointer user_data);
static gboolean on_button_release_event(ClutterActor* actor, ClutterEvent* event, gpointer user_data);

ClutterActor* jammo_cursor_new(JammoSequencer* sequencer, ClutterActor* texture) {

	return CLUTTER_ACTOR(g_object_new(JAMMO_TYPE_CURSOR, "sequencer", sequencer, "texture", texture, NULL));
}

static void jammo_cursor_dispose(GObject* object) {
	JammoCursor* cursor;

	cursor = JAMMO_CURSOR(object);

	TANGLE_UNREF_AND_NULLIFY_OBJECT(cursor->priv->sequencer);
	TANGLE_UNREF_AND_NULLIFY_OBJECT(cursor->priv->texture);

	if (cursor->priv->sequencer_started_handler_id) {
		g_signal_handler_disconnect(cursor->priv->sequencer, cursor->priv->sequencer_started_handler_id);
		cursor->priv->sequencer_started_handler_id = 0;
	}
	if (cursor->priv->sequencer_stopped_handler_id) {
		g_signal_handler_disconnect(cursor->priv->sequencer, cursor->priv->sequencer_stopped_handler_id);
		cursor->priv->sequencer_stopped_handler_id = 0;
	}
	if (cursor->priv->motion_event_handler_id) {
		g_signal_handler_disconnect(clutter_actor_get_stage(CLUTTER_ACTOR(cursor)), cursor->priv->motion_event_handler_id);
		g_signal_handler_disconnect(clutter_actor_get_stage(CLUTTER_ACTOR(cursor)), cursor->priv->button_release_event_handler_id);
		cursor->priv->motion_event_handler_id = cursor->priv->button_release_event_handler_id = 0;
	}

	G_OBJECT_CLASS(jammo_cursor_parent_class)->dispose(object);
}

static void jammo_cursor_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoCursor* cursor;
	
	cursor = JAMMO_CURSOR(object);

	switch (prop_id) {
		case PROP_SEQUENCER:
			cursor->priv->sequencer = JAMMO_SEQUENCER(g_value_get_object(value));
			g_object_ref(cursor->priv->sequencer);
			g_signal_connect(cursor->priv->sequencer, "started", G_CALLBACK(on_sequencer_started), cursor);
			g_signal_connect(cursor->priv->sequencer, "stopped", G_CALLBACK(on_sequencer_stopped), cursor);
			break;
		case PROP_TEXTURE:
			if (cursor->priv->texture) {
				clutter_container_remove_actor(CLUTTER_CONTAINER(cursor), cursor->priv->texture);
				g_signal_handlers_disconnect_by_func(cursor->priv->texture, G_CALLBACK(on_button_press_event), cursor);
				g_object_unref(cursor->priv->texture);
			}
			cursor->priv->texture = CLUTTER_ACTOR(g_value_get_object(value));
			clutter_container_add_actor(CLUTTER_CONTAINER(cursor), cursor->priv->texture);
			if (cursor->priv->seekable) {
				clutter_actor_set_reactive(CLUTTER_ACTOR(cursor->priv->texture), TRUE);
				g_signal_connect(cursor->priv->texture, "button-press-event", G_CALLBACK(on_button_press_event), cursor);
			}
			g_object_ref(cursor->priv->texture);
			clutter_actor_hide(cursor->priv->texture);
			break;
		case PROP_SEEKABLE:
			if (cursor->priv->seekable && cursor->priv->texture) {
				g_signal_handlers_disconnect_by_func(cursor->priv->texture, G_CALLBACK(on_button_press_event), cursor);
			}
			cursor->priv->seekable = g_value_get_boolean(value);
			if (cursor->priv->seekable && cursor->priv->texture) {
				clutter_actor_set_reactive(CLUTTER_ACTOR(cursor->priv->texture), TRUE);
				g_signal_connect(cursor->priv->texture, "button-press-event", G_CALLBACK(on_button_press_event), cursor);
			}
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_cursor_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoCursor* cursor;

	cursor = JAMMO_CURSOR(object);

        switch (prop_id) {
		case PROP_SEQUENCER:
			g_value_set_object(value, cursor->priv->sequencer);
			break;
		case PROP_TEXTURE:
			g_value_set_object(value, cursor->priv->texture);
			break;
		case PROP_SEEKABLE:
			g_value_set_boolean(value, cursor->priv->seekable);
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static void jammo_cursor_class_init(JammoCursorClass* cursor_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(cursor_class);

	gobject_class->dispose = jammo_cursor_dispose;
	gobject_class->set_property = jammo_cursor_set_property;
	gobject_class->get_property = jammo_cursor_get_property;

	/**
	 * JammoCursor:sequencer:
	 *
	 */
	g_object_class_install_property(gobject_class, PROP_SEQUENCER,
	                                g_param_spec_object("sequencer",
	                                                    "Sequencer",
	                                                    "The sequencer",
	                                                    JAMMO_TYPE_SEQUENCER,
	                                                    G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	/**
	 * JammoCursor:texture:
	 *
	 * The actor that is used as a cursor texture.
	 */
	g_object_class_install_property(gobject_class, PROP_TEXTURE,
	                                g_param_spec_object("texture",
	                                                    "Texture",
	                                                    "The actor that is used as a cursor texture",
	                                                    CLUTTER_TYPE_ACTOR,
	                                                    G_PARAM_READABLE | G_PARAM_WRITABLE |  G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	/**
	 * JammoCursor:seekable:
	 *
	 * Whether the cursor can be dragged in order to set (seek) a new position of the sequencer.
	 */
	g_object_class_install_property(gobject_class, PROP_SEEKABLE,
	                                g_param_spec_boolean("seekable",
	                                                    "Seekable",
	                                                    "Whether the cursor can be dragged in order to set (seek) a new position of the sequencer",
	                                                    TRUE,
	                                                    G_PARAM_READABLE | G_PARAM_WRITABLE |  G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

	g_type_class_add_private(gobject_class, sizeof(JammoCursorPrivate));
}

static void jammo_cursor_init(JammoCursor* cursor) {
	cursor->priv = G_TYPE_INSTANCE_GET_PRIVATE(cursor, JAMMO_TYPE_CURSOR, JammoCursorPrivate);
	cursor->priv->seekable = TRUE;

	g_object_set(G_OBJECT(cursor), "pickable", FALSE, NULL);
	g_signal_connect(cursor, "notify::mapped", G_CALLBACK(manage_texture), NULL);
}

static gboolean on_timeout(gpointer user_data) {
	JammoCursor* cursor;
	guint position;
	guint duration;
	gfloat fraction;
	
	cursor = JAMMO_CURSOR(user_data);
	if (cursor->priv->texture && !cursor->priv->seeking) {
		position = jammo_sequencer_get_position(cursor->priv->sequencer) / 1000000;
		duration = jammo_sequencer_get_duration(cursor->priv->sequencer) / 1000000;
		fraction =  (gfloat)position / duration;
		clutter_actor_set_position(cursor->priv->texture,
		                           fraction * clutter_actor_get_width(CLUTTER_ACTOR(cursor)) - clutter_actor_get_width(cursor->priv->texture) / 2,
					   0.0);
		clutter_actor_queue_relayout(CLUTTER_ACTOR(cursor));
	}

	return TRUE;
}

static void manage_texture(JammoCursor* cursor) {
	if (cursor->priv->playing && CLUTTER_ACTOR_IS_MAPPED(CLUTTER_ACTOR(cursor))) {
		if (cursor->priv->texture) {
			clutter_actor_set_position(cursor->priv->texture, -clutter_actor_get_width(cursor->priv->texture) / 2, 0.0);
			clutter_actor_show(cursor->priv->texture);
			clutter_actor_queue_relayout(CLUTTER_ACTOR(cursor));
		}
		if (!cursor->priv->timeout_source_id) {
			cursor->priv->timeout_source_id = g_timeout_add(80, on_timeout, cursor);
		}
	} else {
		if (cursor->priv->texture) {
			clutter_actor_hide(cursor->priv->texture);
		}
		if (cursor->priv->timeout_source_id) {
			g_source_remove(cursor->priv->timeout_source_id);
			cursor->priv->timeout_source_id = 0;
		}	
	}
}

static void on_sequencer_started(JammoSequencer* sequencer, gpointer user_data) {
	JammoCursor* cursor;
	
	cursor = JAMMO_CURSOR(user_data);

	cursor->priv->playing = TRUE;
	manage_texture(cursor);
}

static void on_sequencer_stopped(JammoSequencer* sequencer, gpointer user_data) {
	JammoCursor* cursor;
	
	cursor = JAMMO_CURSOR(user_data);
	cursor->priv->playing = FALSE;
	manage_texture(cursor);
}

static gboolean on_button_press_event(ClutterActor* actor, ClutterEvent* event, gpointer user_data) {
	JammoCursor* cursor;
	
	cursor = JAMMO_CURSOR(user_data);
	
	cursor->priv->seeking = TRUE;
	clutter_event_get_coords(event, &cursor->priv->seek_start_stage_x, NULL);
	cursor->priv->seek_start_texture_x = clutter_actor_get_x(cursor->priv->texture);

	cursor->priv->motion_event_handler_id = g_signal_connect(clutter_actor_get_stage(actor), "motion-event", G_CALLBACK(on_motion_event), cursor);
	cursor->priv->button_release_event_handler_id = g_signal_connect(clutter_actor_get_stage(actor), "button-release-event", G_CALLBACK(on_button_release_event), cursor);
	
	tangle_actor_set_interacting(TANGLE_ACTOR(cursor), TRUE);
	tangle_actor_set_dragging(TANGLE_ACTOR(cursor), TRUE);

	return FALSE;
}

static gboolean on_motion_event(ClutterActor* actor, ClutterEvent* event, gpointer user_data) {
	JammoCursor* cursor;
	gfloat stage_x, texture_x;
	
	cursor = JAMMO_CURSOR(user_data);
	
	clutter_event_get_coords(event, &stage_x, NULL);
	texture_x = cursor->priv->seek_start_texture_x + stage_x - cursor->priv->seek_start_stage_x;
	clutter_actor_set_x(cursor->priv->texture, texture_x);
	
	return FALSE;
}

static gboolean on_button_release_event(ClutterActor* actor, ClutterEvent* event, gpointer user_data) {
	JammoCursor* cursor;
	gfloat stage_x, texture_x;
	gfloat fraction; 
	
	cursor = JAMMO_CURSOR(user_data);

	cursor->priv->seeking = FALSE;

	clutter_event_get_coords(event, &stage_x, NULL);
	texture_x = cursor->priv->seek_start_texture_x + stage_x - cursor->priv->seek_start_stage_x;
	clutter_actor_set_x(cursor->priv->texture, texture_x);
	
	fraction = texture_x / (clutter_actor_get_width(CLUTTER_ACTOR(cursor)) - clutter_actor_get_width(cursor->priv->texture) / 2);
	if (fraction >= 0.0 && fraction < 1.0) {
		jammo_sequencer_set_position(cursor->priv->sequencer, jammo_sequencer_get_duration(cursor->priv->sequencer) * fraction);
	}
	
	if (cursor->priv->motion_event_handler_id) {
		tangle_actor_set_interacting(TANGLE_ACTOR(cursor), FALSE);
		tangle_actor_set_dragging(TANGLE_ACTOR(cursor), FALSE);

		g_signal_handler_disconnect(actor, cursor->priv->motion_event_handler_id);
		g_signal_handler_disconnect(actor, cursor->priv->button_release_event_handler_id);
		cursor->priv->motion_event_handler_id = cursor->priv->button_release_event_handler_id = 0;
	}
	
	return FALSE;
}
