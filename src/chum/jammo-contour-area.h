#include <glib.h>
#include <gmodule.h>
#include <stdlib.h>
#include <clutter/clutter.h>
#include <cogl/cogl.h>


/* Coglbox declaration
 *--------------------------------------------------*/

G_BEGIN_DECLS

#define JAMMO_TYPE_CONTOUR_AREA jammo_contour_area_get_type()

#define JAMMO_CONTOUR_AREA(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  JAMMO_TYPE_CONTOUR_AREA, JammoContourArea))

#define JAMMO_CONTOUR_AREA_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  JAMMO_TYPE_CONTOUR_AREA, JammoContourAreaClass))

#define JAMMO_IS_CONTOUR_AREA(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  JAMMO_TYPE_CONTOUR_AREA))

#define JAMMO_IS_CONTOUR_AREA_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  JAMMO_TYPE_CONTOUR_AREA))

#define JAMMO_CONTOUR_AREA_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  JAMMO_TYPE_CONTOUR_AREA, JammoContourAreaClass))

typedef struct _JammoContourArea        JammoContourArea;
typedef struct _JammoContourAreaClass   JammoContourAreaClass;
typedef struct _JammoContourAreaPrivate JammoContourAreaPrivate;

struct _JammoContourArea
{
  ClutterActor           parent;

  /*< private >*/
  JammoContourAreaPrivate *priv;
};

struct _JammoContourAreaClass
{
  ClutterActorClass parent_class;
};

GType jammo_contour_area_get_type (void) G_GNUC_CONST;

G_END_DECLS



ClutterActor* jammo_contour_area_new (void);
void jammo_contour_area_trace_update(JammoContourArea* contour_trace_area, gfloat X, gfloat Y);
