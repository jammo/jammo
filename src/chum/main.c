/*
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "../configure.h"
#include "jammo-chum.h"
#include "../meam/jammo-meam.h"
#include "jammo-mentor.h"
#include "jammo-mentor-activity.h"
#include "jammo-track-view.h"
#include <tangle.h>
#include <clutter/clutter.h>
#include <stdlib.h> 
#include <libgen.h>

#include "jammo-mentor.h"

#include <fcntl.h> //we check some files

#include <string.h>

#include "configure.h"

#include "../cem/cem.h"
//used for commandline starting
#include "3-6gui/chum.h"
#include "7-12gui/midi_editor.h"
#include "7-12gui/sequencer.h"
#include "7-12gui/startmenu.h"
#include "7-12gui/communitymenu.h"
#include "7-12gui/gamesmenu.h"
#include "7-12gui/jammo-collaboration-group-composition.h"

#include "jammo-game-task.h"
#include "jammo-game-level.h"
#include "jammo-game-level-view.h"

#ifdef NETWORKING_ENABLED
#include "../gems/gems.h"
#endif

//#include "singinggame.h"

#include <libintl.h> //For gettext
/*If compiled without optimization (-O0), setlocale is not found, because locale.h is not then included
 see more: http://blog.flameeyes.eu/2008/09/02/testing-the-corner-cases
 This might be fixed on newer libintl.h
*/
#include <locale.h>


#if LIBOSSO==1
#include <libosso.h>
#include <clutter/x11/clutter-x11.h>
#endif

//This is defined in welcome.c and will executed if nothing happens after 10 seconds
void welcome_door_clicked(TangleActor* actor, gpointer data);
static gboolean door_timeouts(gpointer data);

static ClutterScript* script;

ClutterActor* jammo_get_actor_by_id(const char* id) {
	ClutterActor* actor = NULL;

	if (!(actor = CLUTTER_ACTOR(clutter_script_get_object(script, id)))) {
		gchar* message = g_strdup_printf("Actor '%s' not found.", id);
		cem_add_to_log(message, J_LOG_WARN);
		g_free(message);
	}
	
	return actor;
}

GObject* jammo_get_object_by_id(const char* id) {
	GObject* object = NULL;

	if (!(object = clutter_script_get_object(script, id))) {
		gchar* message = g_strdup_printf("Object '%s' not found.", id);
		cem_add_to_log(message, J_LOG_WARN);
		g_free(message);
	}
	
	return object;
}

/* A TangleStylesheet directory is tried to load from the following locations:
 * 	DATA_DIR
 * 	<current_working_directory>/data/stylesheet
 */
static TangleStylesheet* get_jammo_stylesheet(void) {
	TangleStylesheet* stylesheet;
	gchar* cwd;
	gchar* filename;
	
	cwd = g_get_current_dir();
	filename = g_build_filename(cwd, "data", "stylesheet", NULL);

	if (g_file_test(filename, G_FILE_TEST_EXISTS)) {
		stylesheet = tangle_stylesheet_new_from_file(filename);
	} else {
		gchar *filename2 = g_build_filename(DATA_DIR, "stylesheet", NULL);
		stylesheet = tangle_stylesheet_new_from_file(filename2);
		g_free(filename2);
	}

	g_free(cwd);
	g_free(filename);

	return stylesheet;
}

/* JSON files and content within those are searched from the following locations:
 * 	DATA_DIR
 * 	<current_working_directory>/data/jammo
 */
static void add_search_paths(ClutterScript* clutter_script) {
	const gchar* default_search_path = DATA_DIR;
	const gchar* default_search_path_audio = AUDIO_DIR;
	gchar* development_search_path3_6;
	gchar* development_search_path7_12;
	gchar* cwd;
	const gchar* search_paths[4];

	cwd = g_get_current_dir();
	development_search_path3_6 = g_build_filename(cwd, "data", "jammo3_6", NULL);
	development_search_path7_12 = g_build_filename(cwd, "data", "jammo7_12", NULL);

	search_paths[0] = development_search_path3_6;
	search_paths[1] = development_search_path7_12;
	search_paths[2] = default_search_path;
	search_paths[3] = default_search_path_audio;
	clutter_script_add_search_paths(clutter_script, search_paths, G_N_ELEMENTS(search_paths));

	/* Remember to keep the paths above and below in sync! */
	
	tangle_add_search_path(development_search_path3_6);
	tangle_add_search_path(development_search_path7_12);
	tangle_add_search_path(default_search_path);
	tangle_add_search_path(default_search_path_audio);

	g_free(cwd);
	g_free(development_search_path3_6);
	g_free(development_search_path7_12);
}

#if LIBOSSO==1

static gboolean pause_display_blanking(gpointer user_data) {
	osso_context_t* osso_context;
	
	osso_context = (osso_context_t*)user_data;
	
	g_return_val_if_fail(osso_display_blanking_pause(osso_context) == OSSO_OK, FALSE);
	
	return TRUE;
}

static gint rpc_callback(const gchar* interface, const gchar* method, GArray* arguments, gpointer user_data, osso_rpc_t* retval) {
	ClutterStage* stage;
	Display* display;
	Window window;
	XEvent event;

	stage = CLUTTER_STAGE(user_data);
	
	if (!strcmp(interface, "org.umsic.jammo") && !strcmp(method, "top_application")) {
		display = clutter_x11_get_default_display();
		window = clutter_x11_get_stage_window(stage);

		XRaiseWindow(display, window);
		/* Fremantle window manager does not obey XRaiseWindow, but reacts on _NET_ACTIVE_WINDOW messages. */
		event.xclient.type = ClientMessage;
		event.xclient.message_type = XInternAtom(display, "_NET_ACTIVE_WINDOW", False);
		event.xclient.display = display;
		event.xclient.window = window;
		event.xclient.format = 32;
		event.xclient.data.l[0] = 1;
		event.xclient.data.l[1] = clutter_x11_get_current_event_time();
		event.xclient.data.l[2] = None;
		event.xclient.data.l[3] = 0;
		event.xclient.data.l[4] = 0;
		XSendEvent(display, clutter_x11_get_root_window(), False, SubstructureNotifyMask | SubstructureRedirectMask, &event);
	}
	
	retval->type = DBUS_TYPE_INVALID;
	
	return OSSO_OK;
}
#endif

/*
Parse settings file
If file is not found (or some other error case) values are not modifed.
 -> Caller should set own defaults before calling,
 (or check against NULL)
*/
static void read_config_file(char* filename, gboolean* log_stdout, int* log_level,gchar** mentor_lang, gboolean* start_passive_mentor, gboolean* start_advanced, gboolean* fullscreen) {
	JsonParser *parser;
	parser = json_parser_new ();
	g_assert (JSON_IS_PARSER (parser));

	if (!json_parser_load_from_file (parser, filename, NULL))
		{
		printf("jammo-main: read_config_file: Can't load settings file '%s'.\n", filename);
		g_object_unref (parser);
		return;
		}

	JsonNode *root;
	JsonObject *object;
	JsonNode *node;

	g_assert (NULL != json_parser_get_root (parser));

	//g_print ("checking root node is an object...\n");
	root = json_parser_get_root (parser);
	g_assert_cmpint (JSON_NODE_TYPE (root), ==, JSON_NODE_OBJECT);

	object = json_node_get_object (root);
	g_assert (object != NULL);


	//Settings for logging
	node = json_object_get_member (object, "WRITE_LOG_ALSO_STDOUT");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
		*log_stdout = json_node_get_boolean (node);

	node = json_object_get_member (object, "LOG_DEBUG");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
		if( json_node_get_boolean (node))
			*log_level+=J_LOG_DEBUG;

	node = json_object_get_member (object, "LOG_INFO");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
		if( json_node_get_boolean (node))
			*log_level+=J_LOG_INFO;

	node = json_object_get_member (object, "LOG_FATAL");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
		if( json_node_get_boolean (node))
			*log_level+=J_LOG_FATAL;

	node = json_object_get_member (object, "LOG_ERROR");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
		if( json_node_get_boolean (node))
			*log_level+=J_LOG_ERROR;

	node = json_object_get_member (object, "LOG_WARN");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
		if( json_node_get_boolean (node))
			*log_level+=J_LOG_WARN;

	node = json_object_get_member (object, "LOG_USER_ACTION");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
		if( json_node_get_boolean (node))
			*log_level+=J_LOG_USER_ACTION;

	node = json_object_get_member (object, "LOG_NETWORK");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
		if( json_node_get_boolean (node))
			*log_level+=J_LOG_NETWORK;

	node = json_object_get_member (object, "LOG_NETWORK_DEBUG");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
		if( json_node_get_boolean (node))
			*log_level+=J_LOG_NETWORK_DEBUG;

	node = json_object_get_member (object, "LOG_MENTOR_SPEECH");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
		if( json_node_get_boolean (node))
			*log_level+=J_LOG_MENTOR_SPEECH;

	//LANGUAGE of Mentor
	const gchar* mlang=NULL;
	node = json_object_get_member (object, "MENTOR_LANGUAGE");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
		mlang = json_node_get_string (node);
		if (mlang) {
			g_free(*mentor_lang); //Free default value.
			*mentor_lang = g_strdup_printf("%s",mlang);
		}

	node = json_object_get_member (object, "START_WITH_PASSIVE_MENTOR");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
		*start_passive_mentor= json_node_get_boolean (node);

	node = json_object_get_member (object, "START_WITH_ADVANCED");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
		*start_advanced= json_node_get_boolean (node);

	node = json_object_get_member (object, "FULLSCREEN");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
		*fullscreen= json_node_get_boolean (node);

	g_object_unref (parser);
}


/*
 Order of initialization:
 * Configure (define directories and creates them if needed)
 * CHUM (graphical)
 * Read settings-files
 * CEM  (Logging)
 * MEAN (audio)
 * GEMS (network)
 * [OSSO: osso-context (+keep-a-live for display) ]
 * gettext (locales)
 * tangle-search-paths (e.g. json folders)
 * Mentor-language
 */
int main(int argc, char** argv) {
	gchar* application_name;
	//Name of binary
	application_name = g_path_get_basename(argv[0]);
	if (!strcmp(application_name, "jammo7_12") || (argc>=2 && strcmp(argv[1],"sequencer")==0)) {
		configure_init_directories(FALSE);  //7-12 game
	} else {
		configure_init_directories(TRUE);  //3-6 game
	}

#if LIBOSSO==1
	osso_context_t* osso_context;
#endif

	jammo_chum_init(&argc, &argv);

	//these can be changed via SETTINGS-file
	gboolean log_to_stdout = FALSE;
	int log_level=J_LOG_NONE;
	gchar* lang_of_mentor = g_strdup_printf("system");
	gboolean start_passive_mentor = FALSE;
	gboolean start_advanced = FALSE; //This affects only for 3-6 years game
	gboolean fullscreen = FALSE;

	gchar* setting_file = g_strdup_printf("%s/jammo.config",configure_get_jammo_directory());
	read_config_file(setting_file,&log_to_stdout,&log_level,&lang_of_mentor,&start_passive_mentor,&start_advanced,&fullscreen);
	g_free(setting_file);

	//LOGGING
	cem_print_also_to_stdout(log_to_stdout);
	cem_set_log_level(log_level);

	gchar* message = g_strdup_printf("JamMo-%s started",VERSION);
	cem_add_to_log(message, J_LOG_INFO);
	g_free(message);
	message = g_strdup_printf("JamMo compiled at %s %s", __DATE__, __TIME__);
	cem_add_to_log(message, J_LOG_DEBUG);
	g_free(message);

	TangleStylesheet* stylesheet;
	gchar* filename;
	ClutterActor* stage;
#ifdef BUFFERER_ENABLED
	ClutterActor* bufferer;
#endif
	GError* error = NULL;
	GObject* object;
	ClutterActor* actor;
	ClutterColor black = { 0, 0, 0, 255 };
	const gchar* duration_cache_filename;

	//Name of binary
	if (!strcmp(application_name, "jammo7_12") || (argc>=2 && strcmp(argv[1],"sequencer")==0)) {
		duration_cache_filename = "jammo7_12.duration_cache";
	} else {
		duration_cache_filename = "jammo3_6.duration_cache";
	}

	jammo_meam_init(&argc, &argv, duration_cache_filename);

	
	/* GEMS initialization and profile manager test */
#ifdef NETWORKING_ENABLED
	//GRand* grand = g_rand_new_with_seed(time(NULL));
	jammo_gems_init(argc, argv);
	//gems_profile_manager_login(g_rand_int(grand),NULL,NULL);
	//printf("Selected random ID = %u\n",gems_profile_manager_get_userid(NULL));
	//g_rand_free(grand);
	
	gems_enable_network_timeout_functions();
	
	/*g_timeout_add_full(G_PRIORITY_DEFAULT_IDLE,200,(GSourceFunc)gems_process_connections, NULL, NULL);
	g_timeout_add_full(G_PRIORITY_DEFAULT_IDLE,200,(GSourceFunc)gems_service_jammo_process_connections, NULL, NULL);
	g_timeout_add_full(G_PRIORITY_LOW,200,(GSourceFunc)gems_communication_search_jammos, NULL, NULL);
	g_timeout_add_full(G_PRIORITY_LOW,300,(GSourceFunc)gems_communication_retrieve_profiles, NULL, NULL);
	g_timeout_add_full(G_PRIORITY_DEFAULT,100,(GSourceFunc)gems_communication_process_connections, NULL, NULL);
	g_timeout_add_full(G_PRIORITY_LOW,200,(GSourceFunc)gems_communication_request_groups, NULL, NULL);
	g_timeout_add_full(G_PRIORITY_LOW,350,(GSourceFunc)gems_communication_sanitize_grouplist, NULL, NULL);*/
#endif

	srand (time (NULL)); //We will use rand()



#if LIBOSSO==1
	if (!strcmp(application_name,"jammo7_12"))
		osso_context = osso_initialize("org.umsic.jammo7_12", VERSION, 0, NULL);
	else
		osso_context = osso_initialize("org.umsic.jammo3_6", VERSION, 0, NULL);

	g_assert(osso_context != NULL);

	osso_display_state_on(osso_context);
	osso_display_blanking_pause(osso_context);
	
	g_timeout_add(55000, pause_display_blanking, osso_context);
#endif

	/* PACKAGE from configure.ac */ \
	setlocale ( LC_ALL, "" );
	bindtextdomain ( PACKAGE, "/opt/jammo/languages" );
	textdomain ( PACKAGE );

	/* TRANSLATORS: This is used on main, when gettext is first time used */
	gchar* localized = g_strdup_printf("main: %s",_("gettext initialized"));
	message = g_strdup_printf("%s",localized);
	g_free(localized);
	cem_add_to_log(message, J_LOG_INFO);
	g_free(message);

	/* TODO: Temporary: make sure Mentor classes exists. */
	/* And orientation games related */
	message = g_strdup_printf("%ld %ld %ld %ld %ld %ld", (long)JAMMO_TYPE_MENTOR, (long)JAMMO_TYPE_MENTOR_ACTIVITY, (long)JAMMO_TYPE_TRACK_VIEW,  (long)JAMMO_TYPE_GAME_TASK,  (long)JAMMO_TYPE_GAME_LEVEL,  (long)JAMMO_TYPE_GAME_LEVEL_VIEW);
	cem_add_to_log(message, J_LOG_NONE); //This doesn't do anything.
	g_free(message);

	if ((stylesheet = get_jammo_stylesheet())) {
		tangle_stylesheet_set_default(stylesheet);
	}

	const gchar* name_of_main_json;
	if (!strcmp(application_name,"jammo7_12") || (argc>=2 && strcmp(argv[1],"sequencer")==0))
			name_of_main_json="jammo7_12.json";
	else
		name_of_main_json = "jammo3_6.json";
	//printf("looking id:main from file '%s'\n",name_of_main_json);

	script = clutter_script_new();
	add_search_paths(script);
	if (!(filename = clutter_script_lookup_filename(script, name_of_main_json))) {
		message = g_strdup_printf("File '%s' not found.",name_of_main_json);
		cem_add_to_log(message, J_LOG_FATAL);
		g_free(message);

		return 1;
	}
	if (!clutter_script_load_from_file(script, filename, &error)) {
		message = g_strdup_printf("Could not load file '%s': %s", filename, error->message);
		cem_add_to_log(message, J_LOG_FATAL);
		g_free(message);

		return 2;
	}
	g_free(filename);
	
	if (!(object = clutter_script_get_object(script, "main"))) {
		message = g_strdup_printf("Object 'main' not found from the file '%s'.",name_of_main_json);
		cem_add_to_log(message, J_LOG_FATAL);
		g_free(message);

		return 3;
	}
	
	if (!CLUTTER_IS_ACTOR(object)) {
		message = g_strdup_printf("Object 'main' is not ClutterActor in the file '%s'.",name_of_main_json);
		cem_add_to_log(message, J_LOG_FATAL);
		g_free(message);

		return 4;
	}
	
	actor = CLUTTER_ACTOR(object);
	
	stage = clutter_stage_get_default();
	clutter_stage_set_title(CLUTTER_STAGE(stage), "JamMo");
	clutter_stage_set_color(CLUTTER_STAGE(stage), &black);
	clutter_stage_set_key_focus(CLUTTER_STAGE(stage), NULL);

#if LIBOSSO==1
	g_assert(osso_rpc_set_default_cb_f(osso_context, rpc_callback, stage) == OSSO_OK);
#endif

#ifdef FORCE_FULLSCREEN
	clutter_stage_set_fullscreen(CLUTTER_STAGE(stage), TRUE);
#else
	clutter_actor_set_size(stage, 800, 480);
	if (fullscreen) //read from config file
		clutter_stage_set_fullscreen(CLUTTER_STAGE(stage), TRUE);
#endif

#ifdef BUFFERER_ENABLED
	bufferer = tangle_bufferer_new();
	clutter_actor_set_size(bufferer, 800, 480);
	clutter_container_add_actor(CLUTTER_CONTAINER(stage), bufferer);	
	clutter_container_add_actor(CLUTTER_CONTAINER(bufferer), actor);
#else
	clutter_container_add_actor(CLUTTER_CONTAINER(stage), actor);
#endif
	
	clutter_actor_show(stage);

	//do not show mentor on startup
	clutter_actor_hide(CLUTTER_ACTOR(jammo_mentor_get_default()));

	if (strncmp(lang_of_mentor,"system",6)==0) {
		gchar* lang = getenv ("LANG");
		message = g_strdup_printf("getenv(LANG)='%s'",lang); 
		cem_add_to_log(message, J_LOG_DEBUG);
		g_free(message);

		g_free(lang_of_mentor); //we overwrite text 'system' 
		lang_of_mentor=g_strndup (lang,5); //omit charset.
	}
	gchar* path;
	if (!strcmp(application_name,"jammo7_12"))
		path = g_strdup_printf("mentor_speech7_12/%s/mentor",lang_of_mentor); //TODO: one 'useless' mentor-folder
	else
		path = g_strdup_printf("mentor_speech/%s/mentor",lang_of_mentor); //TODO: one 'useless' mentor-folder
	g_free(lang_of_mentor);

	message = g_strdup_printf("mentor language path='%s' ",path);
	cem_add_to_log(message, J_LOG_INFO);
	g_free(message);

	jammo_mentor_set_language(jammo_mentor_get_default(),path);
	g_free(path);

	if (start_passive_mentor)
		jammo_mentor_set_passive(jammo_mentor_get_default(),TRUE);


	if (!strcmp(application_name,"jammo7_12")) { //start jammo 7-12 years
		cem_add_to_log("Will start 7-12 years game", J_LOG_DEBUG);
		#ifdef NETWORKING_ENABLED
		gems_set_callback_game_starter(group_composition_remote_game_starter);//callback to start group game over network
		#endif
	}

	//if (!strcmp(application_name,"jammo3_6"))
	else  { //start jammo 3-6 years
		cem_add_to_log("Will start 3-6 years game", J_LOG_DEBUG);
		if(start_advanced){
			TangleProperties* properties;
			properties = TANGLE_PROPERTIES(jammo_get_object_by_id("global_game_properties"));
			tangle_properties_set_boolean(properties, "easy_game_level",FALSE);
		}
		g_timeout_add(10, start_3_6_game,NULL);
		g_timeout_add(10000, door_timeouts,NULL); //10 seconds and door will be opened automatically
	}

	g_free(application_name);
	clutter_main();

	g_object_unref(script);
	if (stylesheet)
		g_object_unref(stylesheet);
	
	jammo_meam_cleanup();
	
	#ifdef NETWORKING_ENABLED
	// Logout before releasing directories, since saving of profile requires these directories
	gems_profile_manager_logout();

	/* GEMS cleanup */
	jammo_gems_cleanup();
	#endif

	configure_release_directories();

	jammo_cem_cleanup();
	return 0;
}
static gboolean door_timeouts(gpointer data){
//printf("door_timeouts\n");
welcome_door_clicked(NULL,NULL);
return FALSE;
}




static void clutter_actor_flashD(ClutterTimeline* t, gpointer data){
	ClutterActor* actor = CLUTTER_ACTOR(data);
	float width = clutter_actor_get_width(actor);
	float height = clutter_actor_get_height(actor);
	clutter_actor_animate(actor, CLUTTER_LINEAR, 250, "width", width*0.90909, "height",height*0.90909, NULL);
}

static void clutter_actor_flashC(ClutterTimeline* t, gpointer data){
	g_signal_handlers_disconnect_by_func(t, clutter_actor_flashC, data);
	ClutterAnimation* animation;
	ClutterTimeline* timeline;
	ClutterActor* actor = CLUTTER_ACTOR(data);
	float width = clutter_actor_get_width(actor);
	float height = clutter_actor_get_height(actor);
	animation = clutter_actor_animate(actor, CLUTTER_LINEAR, 250, "width", width*1.1, "height",height*1.1, NULL);
	timeline = clutter_animation_get_timeline(animation);
	g_signal_connect(timeline, "completed", G_CALLBACK(clutter_actor_flashD), actor);
}

static void clutter_actor_flashB(ClutterTimeline* t, gpointer data){
	g_signal_handlers_disconnect_by_func(t, clutter_actor_flashB, data);
	ClutterAnimation* animation;
	ClutterTimeline* timeline;
	ClutterActor* actor = CLUTTER_ACTOR(data);
	float width = clutter_actor_get_width(actor);
	float height = clutter_actor_get_height(actor);
	animation = clutter_actor_animate(actor, CLUTTER_LINEAR, 250, "width", width*0.90909, "height",height*0.90909, NULL);
	timeline = clutter_animation_get_timeline(animation);
	g_signal_connect(timeline, "completed", G_CALLBACK(clutter_actor_flashC), actor);
}

void jammo_clutter_actor_flash(ClutterActor* actor) {
	ClutterAnimation* animation;
	ClutterTimeline* timeline;
	float width = clutter_actor_get_width(actor);
	float height = clutter_actor_get_height(actor);
	animation = clutter_actor_animate(actor, CLUTTER_LINEAR, 250, "width", width*1.1, "height",height*1.1, NULL);
	timeline = clutter_animation_get_timeline(animation);
	g_signal_connect(timeline, "completed", G_CALLBACK(clutter_actor_flashB), actor);
}

