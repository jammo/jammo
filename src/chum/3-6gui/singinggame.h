#include <tangle.h>
#include <clutter/clutter.h>
#include "../../meam/jammo-sequencer.h"
void singinggame_start_sequencer(JammoSequencer* sequencer);
gboolean singinggame_cross_click(TangleButton* tanglebutton, gpointer sequencer);
void on_sequencer_stopped_recording(JammoSequencer* sequencer_old, gpointer user_data);
ClutterActor* singinggame_add_cursor(ClutterColor* color, JammoSequencer* sequencer, ClutterActor* parent, gboolean seekable);
void singinggame_force_reinit_songs();
void  singinggame_clean_songlist();
