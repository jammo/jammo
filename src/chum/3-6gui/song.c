/*
 * song.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */

#include <clutter/clutter.h>
#include <tangle.h>
#include <stdlib.h> //getenv(home)
#include <unistd.h>  //we check some files

#include "../../cem/cem.h"

#include "../../meam/jammo-backing-track.h"
#include "../../meam/jammo-recording-track.h"
#include "../../configure.h"  /*SONGS_DIR*/
#include "../jammo-mentor.h"
#include "../jammo-cursor.h"
#include "../jammo-contour-area.h"
#include "../jammo.h"

#include "song.h"
#include "singinggame.h"
#include "../file_helper.h"
#include "chum.h"

static ClutterActor* recording_cursor; //we want ask this location

#ifdef MELODIC_CONTOUR
static ClutterActor *contour_cursor;
static ClutterActor *contour_trace_area;
static ClutterActor *singinggame_display_freq;




static void on_pitch_detected(JammoRecordingTrack* track, gfloat frequency, gpointer user_data) {
		g_return_if_fail(contour_cursor != NULL);
		g_return_if_fail(contour_trace_area != NULL);

		ClutterActor* texture_of_cursor;
		g_object_get(recording_cursor, "texture", &texture_of_cursor,NULL);
		gfloat X = clutter_actor_get_x(texture_of_cursor);
		//int Y=(X-400)*(X-400)*0.001+100; //This is good for demo
		int Y=840-(frequency*2); //frequence [220,480] -> place [480,0] //origo is on upper left corner

		//Do not draw line over drawing area (it is very ugly)
		if (Y<10)
			Y=10;
		if (Y>480-clutter_actor_get_height(texture_of_cursor)) //also if frequency is too low
			Y=480-clutter_actor_get_height(texture_of_cursor);

		//if (X%10==0) //If it takes too much effort to draw it in each round.
		jammo_contour_area_trace_update(JAMMO_CONTOUR_AREA(contour_trace_area),X,Y);

		clutter_actor_set_position(contour_cursor,X,Y);


		if (frequency==-1.0) /*This means, we do not have good value*/
			clutter_text_set_text (CLUTTER_TEXT(singinggame_display_freq),"DEBUG: Can't determine frequency");
		else
			{
			gchar* buffer0;
			buffer0 = g_strdup_printf("DEBUG: frequency: [%3.1lf]",frequency);
			clutter_text_set_text (CLUTTER_TEXT(singinggame_display_freq),buffer0);
			g_free(buffer0);
			}
		
}

#endif

static void remove_mask_after_animation(ClutterTimeline* t, gpointer none);
static void start_sequencer(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	clutter_actor_hide(CLUTTER_ACTOR(jammo_mentor_get_default()));
	singinggame_start_sequencer(JAMMO_SEQUENCER(sequencer));
}

static void song_view_showed(ClutterTimeline* t, gpointer sequencer){
	cem_add_to_log("song_view_showed",J_LOG_INFO);

	jammo_song* song = chum_get_current_song();
	jammo_mentor_set_idle_speech(jammo_mentor_get_default(), "");

	gboolean vocal_on_language=TRUE;

	gchar* audio_filename;
	audio_filename = g_strdup_printf("%s/vocal%s.ogg",song->fullpath,chum_get_selected_language());
	if ( access (audio_filename, R_OK) == -1) //File not found => no vocal_track for this language
		vocal_on_language=FALSE;
	g_free(audio_filename);



	gchar* mentor_tells;

	//If song doesn't have "name_XX.spx" look for "intro_XX.spx"
	gchar *filepath1;
	filepath1 =  g_strdup_printf("%s/name%s.spx", song->fullpath, chum_get_selected_language());
	//printf("song.c: looking for name_XX:'%s'\n",filepath1);

	if ( access (filepath1, R_OK) == -1)  { //nameXX File not found
		g_free(filepath1);
		filepath1 =  g_strdup_printf("%s/intro%s.spx", song->fullpath, chum_get_selected_language());
		//printf("song.c: looking for intro_XX:'%s'\n",filepath1);
		if ( access (filepath1, R_OK) == -1)  { //intro_XX File not found
			//No name_XX nor intro_XX, say: "start after prelude"
			//printf("not founded name_XX or intro_XX\n");
			mentor_tells = g_strdup_printf("songstartC.spx %s",
                vocal_on_language?"":"no_vocal_on_this_song.spx"
                );
		}
		else  { //say only intro_XX
			//printf("intro_XX founded (but not name_XX)\n");
			mentor_tells = g_strdup_printf("%s", filepath1);
		}
	}
	else //This is 'normal'-case!
	  {   //say "you have selected" + name_XX (+ no_vocal_for_this_language) +"start after prelude"
		mentor_tells = g_strdup_printf("songstartA.spx %s/name%s.spx %s songstartC.spx",
             song->fullpath, chum_get_selected_language(),
             vocal_on_language?"":"no_vocal_on_this_song.spx"
             );
		}

	g_free(filepath1);
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), mentor_tells, start_sequencer, sequencer);
	g_free(mentor_tells);

}

static void start_sequencer_after_animation(ClutterTimeline* t, gpointer sequencer){
	start_sequencer(NULL, NULL, FALSE, sequencer);
}



//Set_clip can't be animated easily with clutter_animation
static gboolean animate_clipping (gpointer data){
	int STEP=30;
	int STEP_X=60; //raise faster
	gfloat temp_x;
	gfloat temp_y;
	gfloat temp_w;
	gfloat temp_h;
	clutter_actor_get_clip (CLUTTER_ACTOR(data),&temp_x, &temp_y,&temp_w,&temp_h);
	int ready=0; //counter, how many of dimension have reached goal
	if (temp_x>0){
		temp_x-=STEP_X;
		if (temp_x<0) temp_x=0;
	}
	else ready++;
	if (temp_y>0){
		temp_y-=STEP_X;
		if (temp_y<0) temp_y=0;
	}
	else ready++;
	if (temp_w<800){
		temp_w+=STEP;
		if (temp_w>800) temp_w=800;
	}
	else ready++;
	if (temp_h<480){
		temp_h+=STEP;
		if (temp_x>480) temp_x=480;
	}
	else ready++;
	clutter_actor_set_clip  (CLUTTER_ACTOR(data),temp_x, temp_y,temp_w,temp_h);
	//printf("new values: x=%lf, y=%lf, w=%lf, h=%lf\n",temp_x,temp_y, temp_w,temp_h);

	if (ready==4)
		return FALSE;

	return TRUE;

}

static void add_cross_after_animation(ClutterTimeline* t, gpointer view){
	ClutterActor* cross;
	cross=tangle_button_new_with_background_actors(tangle_texture_new("/opt/jammo/cross_icon.png"),
       tangle_texture_new("/opt/jammo/cross_icon_pressed.png"));
	g_object_set(cross, "x", 675.0,  "y", 0.0, NULL);
	g_signal_connect (cross, "clicked", G_CALLBACK (singinggame_cross_click),NULL);
	clutter_container_add(CLUTTER_CONTAINER(view), cross, NULL);
	tangle_actor_show(TANGLE_ACTOR(cross));
}

static void hide_previous_view_after_animation(ClutterTimeline* t, gpointer icon){
	//previous can songselection or cupboard, this is most safe way to do it.
	ClutterActor* actor = jammo_get_actor_by_id("cupboard-view");
	if (actor)
		clutter_actor_hide(actor);
	actor = jammo_get_actor_by_id("songselection-view");
	if (actor)
		clutter_actor_hide(actor);
}


static void remove_mask_after_animation(ClutterTimeline* t, gpointer none){
	cem_add_to_log("remove transparency mask",J_LOG_DEBUG);
	clutter_actor_hide(jammo_get_actor_by_id("mask-for-preventing-double-clicks"));
}

static void hide_unless_cupboard(TangleActor* actor, gpointer data) {
	if (CLUTTER_ACTOR(actor) == jammo_get_actor_by_id("cupboard-view")) {
		clutter_actor_show(jammo_get_actor_by_id("cupboard-view"));
		return;
		}
	tangle_actor_hide_animated(actor);
}


static void on_sequencer_stopped_from_cupboard(JammoSequencer* sequencer_old, gpointer none) {
	cem_add_to_log("Cupboard-Sequencer stopped",J_LOG_INFO);
	//Hide all
	ClutterActor*	mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(hide_unless_cupboard), NULL);

	//And show mentor again
	tangle_actor_show(TANGLE_ACTOR(jammo_mentor_get_default()));

	//printf("Unreffing sequencer '%p'\n", sequencer_old);
	g_object_unref(sequencer_old);
}


/*
Data is jammo_song* struct
This function is used on singing game and cupboard.
*/
static gboolean song_icon_clicked(TangleButton* tanglebutton, gpointer data){
	cem_add_to_log("Song icon clicked",J_LOG_USER_ACTION);
	jammo_mentor_shut_up(jammo_mentor_get_default());

	clutter_actor_show(jammo_get_actor_by_id("mask-for-preventing-double-clicks"));
	cem_add_to_log("Put transparency mask",J_LOG_DEBUG);

	ClutterActor* view;
	if ((view = tangle_view_new())) {
		ClutterActor* mainview;
		if ((mainview = jammo_get_actor_by_id("main-views-widget"))){
			//printf("Will add 'view' to 'mainview'\n");
			clutter_container_add(CLUTTER_CONTAINER(mainview), view, NULL);
			//g_object_set(view, "transition-duration", 0,NULL); //there are no any more "transition-duration"
			clutter_actor_show(view);
			//printf("succesfully made new view and put it to mainview\n");
		}
	}

	jammo_song* song = (jammo_song*)data;
	chum_set_current_song(song);

	gchar* path=song->fullpath;
	//printf("Song '%s' pressed: \n",song->fullpath);

	gchar* image_filename;
	//check if there are language specific image
	image_filename = g_strdup_printf("%s/background%s.png",path,chum_get_selected_language());
	if ( access (image_filename, R_OK) == -1) //File not found -> use without suffix
		{
		g_free(image_filename);
		image_filename = g_strdup_printf("%s/background.png",path);
		}

	//printf("image_filename = '%s'\n",image_filename);

	ClutterActor *texture = tangle_texture_new(image_filename);
	if (!texture) {
		texture = tangle_texture_new("/opt/jammo/nopic.png");
	}

	clutter_container_add(CLUTTER_CONTAINER(view), texture, NULL);
	g_free(image_filename);

	ClutterActor *mini_icon = song->songbutton;
	gfloat temp_x;
	gfloat temp_y;
	gfloat temp_w;
	gfloat temp_h;
	clutter_actor_get_transformed_position(mini_icon,&temp_x,&temp_y); //upper left corner. and icon is sliced and resized!
	clutter_actor_get_transformed_size (mini_icon,&temp_w,&temp_h);
	//printf("x=%lf, y=%lf, w=%lf, h=%lf\n",temp_x,temp_y, temp_w,temp_h);

	float icon_size = song->icon_size;

	//This is used size, clip, plaze
	gfloat x_ratio = icon_size / song->x_size;
	gfloat y_ratio = icon_size / song->y_size;
	//printf("x_ratio=%lf, y_ratio=%lf\n",x_ratio,y_ratio);
	//printf("PLACE_X=%lf ,PLACE_Y=%lf\n",song->xoff*x_ratio+temp_x,song->yoff*y_ratio+temp_y);
	//Visible coordinate of clicked icon (=useless, because we need that hidden/sliced coordinate)

	//Starting values should be same than mini icon has.
	// Size
	g_object_set( texture, "width",800.0 * x_ratio,
								"height", 480.0 * y_ratio,
								NULL);

	// Clip
	clutter_actor_set_clip (CLUTTER_ACTOR( texture),  song->xoff*x_ratio,  song->yoff *y_ratio, icon_size, icon_size);

	// Place
	g_object_set( texture, 
								"x", temp_x,
								"y", temp_y,
								NULL);

	//Animate icon -> fullscreen background
	g_timeout_add (0,animate_clipping,texture); //Can't be done with ClutterAnimation

	ClutterAnimation* animation = clutter_actor_animate( CLUTTER_ACTOR(texture), CLUTTER_EASE_IN_QUART, 3500,
																											 "width", 800.0,
																											 "height", 480.0, 
																											 "x",0.0,"y",0.0, NULL);

	ClutterTimeline* timeline = clutter_animation_get_timeline(animation);

	//When animation is completed: Add cross for closing, and hide view behind
	g_signal_connect(timeline, "completed", G_CALLBACK(add_cross_after_animation), view);
	g_signal_connect(timeline, "completed", G_CALLBACK(remove_mask_after_animation), NULL);
	g_signal_connect(timeline, "completed", G_CALLBACK(hide_previous_view_after_animation), tanglebutton);


	//Make sequencer
	JammoSequencer* sequencer = jammo_sequencer_new();

	if (song->name_of_recorded==NULL) { //This means we are coming from song selection menu
		//printf("song started from game\n");
		g_signal_connect(sequencer, "stopped", G_CALLBACK(on_sequencer_stopped_recording),view);

		//Filename reletes mode and selected language
		gchar *filename;
		if (chum_get_duetto())
			{
			filename = g_strdup_printf("%s/vocal%s.ogg", song->fullpath,chum_get_selected_language());
			if ( access (filename, R_OK) == -1) //File not found
				{  //no vocal track. use only comping.
				filename = g_strdup_printf("%s/comping.ogg", song->fullpath);
				}
			}
		else //solo
		{
		filename = g_strdup_printf("%s/comping%s.ogg", song->fullpath,chum_get_selected_language());
		if ( access (filename, R_OK) == -1) //File not found
				{  //no track for this language,. use  common comping.
				filename = g_strdup_printf("%s/comping.ogg", song->fullpath); //try without language postfix
				}
		}

		JammoBackingTrack* backing_track;
		//printf("looking file '%s' for backing_track \n",filename);
		backing_track = jammo_backing_track_new(filename);   //for comping track
		jammo_sequencer_add_track(sequencer, JAMMO_TRACK(backing_track));
		g_free(filename);

		gchar* recording_filename = g_strdup_printf("%s/last_recorded.wav",configure_get_jammo_directory());
		JammoRecordingTrack* recording_track;


		#ifdef MELODIC_CONTOUR
		const gchar* selected_language=chum_get_selected_language();
		//contour_cursor = moving thing
		contour_cursor = NULL;
		gchar* filepath1;
		filepath1 = g_strdup_printf("%s/cursor%s.png", song->fullpath, selected_language );
		if ( access (filepath1, R_OK) == -1) {
			//g_free(filepath1);
			filepath1 = g_strdup_printf("%s/cursor.png", song->fullpath ); //try without language postfix
			if ( access (filepath1, R_OK) == -1) {
				gchar* message = g_strdup_printf("file '%s' not found, using default cursor",filepath1);
				cem_add_to_log(message,J_LOG_WARN);
				g_free(message);
				//contour_cursor = config_get_configured_actor(NULL,"contour_star");
			} else {
				contour_cursor = tangle_texture_new(filepath1);
			}
		} else {
			contour_cursor = tangle_texture_new(filepath1);
		}
		g_free(filepath1);

		if (contour_cursor)
			clutter_container_add_actor (CLUTTER_CONTAINER (view), contour_cursor);


		//contour_trace_area
		contour_trace_area = jammo_contour_area_new ();
		clutter_container_add_actor (CLUTTER_CONTAINER (view), contour_trace_area);


		//And for debugging, text-field
		singinggame_display_freq=  clutter_text_new_with_text ("Liberty",""); //start with empty text
		clutter_text_set_font_name(CLUTTER_TEXT(singinggame_display_freq),"Luxi Mono 18");
		ClutterColor text_color = { 255, 255, 0, 255 };
		clutter_text_set_color(CLUTTER_TEXT(singinggame_display_freq), &text_color);

		clutter_container_add_actor (CLUTTER_CONTAINER (view), singinggame_display_freq);
		clutter_actor_set_position (singinggame_display_freq, 100, 30);
		clutter_actor_show (singinggame_display_freq);


		recording_track = jammo_recording_track_new_with_pitch_detect(recording_filename);

		//recording_track is from chum.c. on_pitch_detected uses only globals
		g_signal_connect(recording_track, "pitch-detected", G_CALLBACK(on_pitch_detected), NULL);

		#else /*MELODIC_CONTOUR */
		recording_track = jammo_recording_track_new(recording_filename);
		#endif  /*MELODIC_CONTOUR */
		g_free(recording_filename);

		jammo_sequencer_add_track(sequencer, JAMMO_TRACK(recording_track));


		g_signal_connect(timeline, "completed", G_CALLBACK(song_view_showed), sequencer);

		ClutterColor red = { 255, 0, 0, 128 };
		recording_cursor = singinggame_add_cursor(&red, sequencer, view, FALSE);
	}
	else {
		//printf("song started from cupboard\n");

		//Hide mentor
		jammo_mentor_shut_up(jammo_mentor_get_default());
		clutter_actor_hide(CLUTTER_ACTOR(jammo_mentor_get_default()));

		gchar *filename;
		filename = g_strdup_printf("%s/comping%s.ogg", song->fullpath,song->language);
		if ( access (filename, R_OK) == -1) //File not found
			{  //no track for this language,. use  common comping. THIS IS ERROR case. malfuncted savefile!
			g_free(filename);
			filename = g_strdup_printf("%s/comping.ogg", song->fullpath); //try without language postfix
			}
		//printf("looking for file '%s' for comping_track\n",filename);

		JammoBackingTrack* backing_track;
		backing_track = jammo_backing_track_new(filename);   //for comping track
		jammo_sequencer_add_track(sequencer, JAMMO_TRACK(backing_track));
		g_free(filename);

		gchar *loaded_filename;
		loaded_filename = g_strdup_printf("%s",song->name_of_recorded);
		//printf("looking for file '%s' for loaded_track\n",loaded_filename);

		JammoBackingTrack* backing_track2;
		backing_track2 = jammo_backing_track_new(loaded_filename);
		jammo_sequencer_add_track(sequencer, JAMMO_TRACK(backing_track2));
		g_free(loaded_filename);


		g_signal_connect(sequencer, "stopped", G_CALLBACK(on_sequencer_stopped_from_cupboard),view);
		g_signal_connect(timeline, "completed", G_CALLBACK(start_sequencer_after_animation), sequencer);

		ClutterColor color = { 0,255,255, 128 };
		//Use existing recordin_cursor, even this is only playback
		recording_cursor = singinggame_add_cursor(&color, sequencer, view, TRUE); //seekable
		}

	return FALSE;
}


/*
Parsing.
If file is not found, or elements are missing, doesn't touch values.
*/
static void icon_offset_from_json(char* filename, gfloat* x,gfloat* y, gfloat* w, gfloat* h){
  JsonParser *parser;
  parser = json_parser_new ();
  g_assert (JSON_IS_PARSER (parser));

  if (!json_parser_load_from_file (parser, filename, NULL))
        {
          gchar* message = g_strdup_printf("Can't load file '%s' for json-parsing. Using default values",filename);
          cem_add_to_log(message,J_LOG_WARN);
          g_free(message);
          g_object_unref (parser);
          return;
        }

  JsonNode *root;
  JsonObject *object;
  JsonNode *node;

  g_assert (NULL != json_parser_get_root (parser));

  //g_print ("checking root node is an object...\n");
  root = json_parser_get_root (parser);
  g_assert_cmpint (JSON_NODE_TYPE (root), ==, JSON_NODE_OBJECT);

  object = json_node_get_object (root);
  g_assert (object != NULL);


  //Human readable file contains pixels as integers. Clutter is handling pixels as floats.
  node = json_object_get_member (object, "x");
  if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
    *x = json_node_get_int (node) * 1.0;

  node = json_object_get_member (object, "y");
  if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
    *y = json_node_get_int (node) * 1.0;

  node = json_object_get_member (object, "width");
  if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
    *w = json_node_get_int (node) * 1.0;

  node = json_object_get_member (object, "height");
  if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE)
    *h = json_node_get_int (node) * 1.0;

  g_object_unref (parser);
}


/*
Makes songbutton (ClutterActor) and parses offsets for icon.
And add signal handler.
*/
void song_prepare_icon(jammo_song* song){
	gchar* folder=song->fullpath;
	gchar* image_filename;
	image_filename = g_strdup_printf("%s/background%s.png",folder,chum_get_selected_language());

	//check if there are language specific image
	if ( access (image_filename, R_OK) == -1) //File not found -> use without suffix
		{
		g_free(image_filename);
		image_filename = g_strdup_printf("%s/background.png",folder);
		}


	//Default values:
	gfloat xoff    = 0.0;
	gfloat yoff    = 0.0;
	gfloat x_size  = 800.0;
	gfloat y_size  = 480.0;

	//check if there are language specific icon*.json
	gchar* icon_json_filename = g_strdup_printf("%s/icon%s.json",folder,chum_get_selected_language());
	if ( access (icon_json_filename, R_OK) == -1) //File not found -> use without suffix
		{
		g_free(icon_json_filename);
		icon_json_filename = g_strdup_printf("%s/icon.json",folder);
		}
	icon_offset_from_json(icon_json_filename, &xoff, &yoff, &x_size,&y_size);
	//printf("song.c: image_filename = '%s'\n",image_filename);
	//printf("song.c: icon_json_filename = '%s'\n",icon_json_filename);
	g_free(icon_json_filename);

	//printf("Boundaries: %lf,%lf,%lf,%lf\n", xoff,yoff,x_size,y_size);
	song->xoff = xoff;
	song->yoff = yoff;
	song->x_size = x_size;
	song->y_size = y_size;

	ClutterActor* texture;
	ClutterActor* interactive_texture;
	texture = tangle_texture_new(image_filename);
	interactive_texture = tangle_texture_new(image_filename);
	clutter_actor_set_opacity(interactive_texture, 40.0); //This could be fancier effect.
	ClutterActor* songbutton = tangle_button_new_with_background_actors(texture,interactive_texture);
	song->songbutton = songbutton;
	g_free(image_filename);

	song->callback_handler_id = g_signal_connect(songbutton, "clicked", G_CALLBACK (song_icon_clicked),song);
}


/*
This is for song selection menu
*/
GList* song_init_songs_from_disk(){
	cem_add_to_log("song_init: loading songs from disk",J_LOG_DEBUG);
	GList* songs = NULL;

	//On easy mode, load only songs from 'easy' folder
	//On advanced, load 'easy', 'advanced', and ~/.jammo/singins
	int i;
	int max = chum_is_easy_game() ?1:3;
	for (i=0;i<max;i++)
		{
		//There will be more optional folders. TODO
		
		gchar* base_path=NULL; //this is where we try found songs
		const gchar* song_pack; //This is additional information we store in song
		if (i==0)
			{
			song_pack="easy";
			base_path=g_strdup_printf("%s/%s",SONGS_DIR,song_pack);
			}
		else if (i==1)
			{
			song_pack="advanced";
			base_path=g_strdup_printf("%s/%s",SONGS_DIR,song_pack);
			}
		else if (i==2)
			{
			gchar* home = getenv ("HOME");
			song_pack="own";
			base_path=g_strdup_printf("%s/.jammo/songs/",home);
			}
		else song_pack="";
		//printf("base_path='%s'\n",base_path);
		////////////////////

		if (base_path==NULL)
			continue;
		GList* files = file_helper_get_all_files_without_path(base_path); //this is list of songs in this set
		GList* l;
		for (l = files; l; l = l->next)
			{
			gchar* name_of_song = (gchar*)l->data;

			//New song-entry
			jammo_song* song;
			song = g_new( jammo_song, 1 );
			song->name_of_recorded=NULL;
			song->icon_size=150.0;
			song->name_with_pack= g_strdup_printf("%s_%s",song_pack,name_of_song);
			song->fullpath = g_strdup_printf("%s/%s",base_path,name_of_song);
			song_prepare_icon(song);

			g_free(l->data); //Free ghar* inside of this glist-entry
			songs = g_list_append( songs,song);
			}
		g_free(base_path);
		//Next folder
		}
	cem_add_to_log("song_init: loading from disk done",J_LOG_DEBUG);
return songs;
}


void song_free(jammo_song* song, gpointer data) {
	g_free(song->name_with_pack);
	g_free(song->fullpath);
	g_signal_handler_disconnect(song->songbutton, song->callback_handler_id);
	clutter_actor_destroy(song->songbutton);
	g_free(song);
}
