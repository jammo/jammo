/*
 * cupboard.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */
#include <tangle.h>
#include <clutter/clutter.h>
#include <fcntl.h> //we check some files
#include <string.h>
//#include <json-glib/json-glib.h>
#include <stdlib.h>
#include <stdio.h>
#include <glib.h>


#include "../../meam/jammo-sample.h"
#include "../../meam/jammo-backing-track.h"

#include "../jammo-mentor.h"
#include "../jammo-cursor.h"
#include "../../configure.h"  //DATA_DIR
#include "../jammo.h"

#include "singinggame.h"
#include "composition_game.h"
#include "../file_helper.h"
#include "chum.h"

#include "../../cem/cem.h"


/*
When loading composing, game mode is changed to correspond loaded composing.
When we come back from composing, we want set game mode back to origin.

-1 not defined (coming from main-menu)
0=EASY
1=ADVANCED
10=song listened (do not change skill level, but we are not coming for main-menu)
*/
static int previous_game_mode = -1;

void cupboard_door_clicked(TangleActor* actor, gpointer data);

/*
It suppose filenames are formatted in this way (will seg fault if not).
folder/+theme+[123]+[ae]+date.
example:+fantasy+2+e+2010.03.11_00.03.52
theme_name = city / animal / fantasy          (this is checked, affects icon graphics)
variation = 1 / 2 / 3                         (not critical, user will get empty set of samplebuttons, if use non-valid variation)
mode (easy/advanced) = e /a                   (if unclear, interpret advanced, because there can be two tracks)

But this is callback-function for icon and we do not make icon if it doesn't correspond with these rules.
 (We do not want broken icons and error messages)

*/
static gboolean cupboard_composition_icon_clicked(TangleButton *tanglebutton, gpointer data){
	ClutterActor* actor = jammo_get_actor_by_id("cupboard-view");
	clutter_actor_hide(actor);

	//We do not want Mentor say "welcome to composing", when loading saved composing.
	jammo_mentor_speak_once(jammo_mentor_get_default(), "composing_welcome.spx");

	//We do not want Mentor to help "select theme" anymore, when player have went in composing game
	jammo_mentor_speak_once(jammo_mentor_get_default(), "themeselection_welcome.spx");
	
	//We do not want Mentor to help when first samplebutton is clicked or dragged
	jammo_mentor_speak_once(jammo_mentor_get_default(), "composing_first_drag.spx");
	jammo_mentor_speak_once(jammo_mentor_get_default(), "composing_first_click.spx");
	jammo_mentor_shut_up(jammo_mentor_get_default());
	
	gchar *filename; 
	filename = g_strdup_printf("%s",(gchar*)data);
	gchar* message = g_strdup_printf("Filename for loading '%s'",filename);
	cem_add_to_log(message,J_LOG_DEBUG);
	g_free(message);

	strtok(filename, "+"); //this first is directory path. No use in here.

	char* theme_name =  strtok(NULL, "+");
	char* variation_name = strtok(NULL, "+");
	char* easy  = strtok(NULL, "+");
	
	//Take current game-mode.
	if (chum_is_easy_game())
		{
		previous_game_mode=0;
		}
	else 
		{
		previous_game_mode=1;
		}
	//Change game mode to correspond loaded
	if (strncmp(easy,"e",1)==0)
		chum_set_easy_game(TRUE); //e=easy
	else //if (strncmp(easy,"a",1)==0) //it is safer to use advanced if this is not clear
		chum_set_easy_game(FALSE); //a=advanced

	//Data is filename. Currently file stores only samples, not metadata, so they are passed also
	composition_game_start_from_file(data,theme_name,atoi(variation_name));
	g_free(filename);

	return FALSE;
}

/*
Check icon_filename from given json-file
Will return NULL, if
 *file not found
 *file is folder

User should free memory after usage
*/
static gchar* icon_filename_from_json(char* filename){
  JsonParser *parser;
  parser = json_parser_new ();
  g_assert (JSON_IS_PARSER (parser));

  GError *error = NULL;
  if (!json_parser_load_from_file (parser, filename, &error))
        {
          //cem_add_to_log(error->message,J_LOG_ERROR);
          g_error_free (error);
          g_object_unref (parser);
          return NULL;
        }

  JsonNode *root;
  JsonObject *object;

  g_assert (NULL != json_parser_get_root (parser));

  //g_print ("checking root node is an object...\n");
  root = json_parser_get_root (parser);
  g_assert_cmpint (JSON_NODE_TYPE (root), ==, JSON_NODE_OBJECT);

  object = json_node_get_object (root);
  g_assert (object != NULL);


  G_CONST_RETURN gchar *value = json_object_get_string_member(object ,"icon");
  //printf("parser got '%s'\n",value);
  gchar* name =  g_strdup_printf("%s",value);

  g_object_unref (parser);
  return name;
}


/*
Check path, record, language from given json-file
Will return NULL, if
 *file not found
 *file is folder

User should free memory after usage
*/
static void value_from_json(char* filename, char** path, char** record, char** language){
  JsonParser *parser;
  parser = json_parser_new ();
  g_assert (JSON_IS_PARSER (parser));

  GError *error = NULL;
  if (!json_parser_load_from_file (parser, filename, &error))
        {
          //cem_add_to_log(error->message,J_LOG_ERROR);
          g_error_free (error);
          g_object_unref (parser);
          return;
        }

  JsonNode *root;
  JsonObject *object;

  g_assert (NULL != json_parser_get_root (parser));

  //g_print ("checking root node is an object...\n");
  root = json_parser_get_root (parser);
  g_assert_cmpint (JSON_NODE_TYPE (root), ==, JSON_NODE_OBJECT);

  object = json_node_get_object (root);
  g_assert (object != NULL);


  G_CONST_RETURN gchar *value = json_object_get_string_member(object ,"fullpath");
  //printf("parser got '%s'\n",value);
  *path =  g_strdup_printf("%s",value);

  value = json_object_get_string_member(object ,"recorded_filename");
  *record = g_strdup_printf("%s",value);

  value = json_object_get_string_member(object ,"language");
  *language = g_strdup_printf("%s",value);

  g_object_unref (parser);

}

/*
This is additional callback for song-icons.
*/
static gboolean set_previous_game_mode(){
//printf("set_previous_game_mode\n");
previous_game_mode=10;
return FALSE;
}

static void mentor_over(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	cupboard_door_clicked(NULL,NULL);
}

/*
This checks and loads icons from disk.
This is called each time cupboard is shown.
*/
static gboolean load_icons_from_disk (gpointer data){
	cem_add_to_log("cupboard: load_icons_from_disk",J_LOG_DEBUG);
	//When ending composing started from cupboard, we come back to this function.
	//Set easy/advanced state back to what it was before loading compositions.

	if (previous_game_mode!=-1)
		{
		if (previous_game_mode==0)
			chum_set_easy_game(TRUE);
		else if (previous_game_mode==1)
			chum_set_easy_game(FALSE);
		//-1 means we are not coming from composing, but from mainmenu
		//Set it to -1, if we now go to mainmenu
		previous_game_mode=-1;
		//This is also sign that there are no need to load icons again
		clutter_actor_hide(jammo_get_actor_by_id("wait"));
		jammo_mentor_speak_once_with_callback(jammo_mentor_get_default(),"cupboard_welcome.spx",mentor_over,NULL);
		jammo_mentor_set_idle_speech(jammo_mentor_get_default(), "cupboard_welcome.spx");
		return FALSE;;
		}

	//Scrolling area for icons
	ClutterActor* container_for_saved_icons = jammo_get_actor_by_id("cupboard-container-for-icons");
	clutter_container_foreach(CLUTTER_CONTAINER(container_for_saved_icons), CLUTTER_CALLBACK(clutter_actor_hide), NULL);
	clutter_container_foreach(CLUTTER_CONTAINER(container_for_saved_icons), CLUTTER_CALLBACK(clutter_actor_destroy), NULL);

	//Load compositions
	GList* files = file_helper_get_all_files(configure_get_compositions_directory());


	int counter1=0;
	GList* l;
	for (l = files; l; l = l->next)
		{
		//printf("readed '%s'\n",(char*)l->data);
		ClutterActor* icon;

		gchar* icon_filename = icon_filename_from_json((gchar*)l->data);
		//printf("got icon_filename='%s'\n", icon_filename);

		if (icon_filename==NULL)  //probably folder
			continue;
		gchar* icon_filename_with_path = tangle_lookup_filename(icon_filename);
		ClutterActor* image = tangle_texture_new(icon_filename_with_path);
		g_free(icon_filename_with_path);
		if (image) { //If filename is missing, suppose whole file is corrupted and do not add it
			icon = tangle_button_new_with_background_actor(image);
			g_object_set(icon,"depth-position", -1, "width", 80.0, "height", 80.0, "x", 197.0,  "y", 7+counter1*94.0, NULL);

			clutter_container_add(CLUTTER_CONTAINER(container_for_saved_icons), icon, NULL);
			clutter_actor_set_reactive(icon,TRUE);
			g_signal_connect_data(icon, "clicked", G_CALLBACK (cupboard_composition_icon_clicked),l->data,(GClosureNotify) g_free,0);
			counter1++;
		}

		g_free(icon_filename);
		}

	g_list_free(files);



	//And singings
	//There are also folder named 'wavs', but it is not harmfull
	files = file_helper_get_all_files(configure_get_singings_directory());

	int counter2=0;
	for (l = files; l; l = l->next)
		{
		//printf("readed '%s'\n",(char*)l->data);

		char* fullpath=NULL;
		char* recorded_filename=NULL;
		char* language=NULL;
		value_from_json((char*)l->data,&fullpath,&recorded_filename,&language);
		//printf ("got fullpath: '%s'\n",fullpath);

		if (fullpath==NULL){
			//printf("skip this, probably folder (or other error case)\n");
			continue;
		}

		jammo_song* song;
		song = g_new( jammo_song, 1 );
		song->fullpath = g_strdup_printf("%s",fullpath);
		song->name_of_recorded = recorded_filename;
		song->language = language;

		//song_prepare_icon uses chum_current_language to determine background*.png file
		const gchar* old_language = chum_get_selected_language();
		chum_set_selected_language(language);
		song_prepare_icon(song);
		chum_set_selected_language(old_language);

		//song->name_of_recorded = g_strdup_printf("%s/vocal_fi.spx",fullpath); //DEBUG

		//Next section has lots of common with singingame.c:~300 FIXME
		gfloat icon_size = 78.0;    //square
		song->icon_size=icon_size;
		int icon_marginal =  94 - icon_size; //94 is height of one shelf
		int left_marginal =  30;
		int top_marginal  =   7;

		//This is used size, clip, place
		gfloat x_ratio = icon_size / song->x_size;
		gfloat y_ratio = icon_size / song->y_size;

		// Size
		g_object_set( song->songbutton, "width",800.0 * x_ratio,
								 "height", 480.0 * y_ratio,
								 NULL);

		// Clip
		clutter_actor_set_clip (CLUTTER_ACTOR( song->songbutton),
												song->xoff*x_ratio,
												song->yoff *y_ratio,
												icon_size, icon_size);

		// Place
		g_object_set( song->songbutton,
								"x", +left_marginal - song->xoff*x_ratio,
								"y", counter2*(icon_size+icon_marginal)+top_marginal -  song->yoff*y_ratio,
								NULL);

		//Image and frames goes one group
		ClutterActor* group = clutter_group_new();
		clutter_container_add_actor (CLUTTER_CONTAINER (group), song->songbutton);


		//Frames.
		ClutterColor black = { 0,0,0, 255 };

		ClutterActor* top = clutter_rectangle_new_with_color (&black);
		clutter_rectangle_set_border_color(CLUTTER_RECTANGLE(top),&black);
		clutter_rectangle_set_border_width(CLUTTER_RECTANGLE(top),2);
		clutter_actor_set_size (top,icon_size, 2);
		clutter_actor_set_position (top,left_marginal , counter2*(icon_size+icon_marginal)+top_marginal);
		clutter_container_add_actor (CLUTTER_CONTAINER (group), top);

		ClutterActor* bottom = clutter_rectangle_new_with_color (&black);
		clutter_rectangle_set_border_color(CLUTTER_RECTANGLE(bottom),&black);
		clutter_rectangle_set_border_width(CLUTTER_RECTANGLE(bottom),2);
		clutter_actor_set_size (bottom,icon_size, 2);
		clutter_actor_set_position (bottom,left_marginal , counter2*(icon_size+icon_marginal)+top_marginal+icon_size);
		clutter_container_add_actor (CLUTTER_CONTAINER (group), bottom);

		ClutterActor* left = clutter_rectangle_new_with_color (&black);
		clutter_rectangle_set_border_color(CLUTTER_RECTANGLE(left),&black);
		clutter_rectangle_set_border_width(CLUTTER_RECTANGLE(left),2);
		clutter_actor_set_size (left,2, icon_size);
		clutter_actor_set_position (left,left_marginal , counter2*(icon_size+icon_marginal)+top_marginal);
		clutter_container_add_actor (CLUTTER_CONTAINER (group), left);

		ClutterActor* right = clutter_rectangle_new_with_color (&black);
		clutter_rectangle_set_border_color(CLUTTER_RECTANGLE(right),&black);
		clutter_rectangle_set_border_width(CLUTTER_RECTANGLE(right),2);
		clutter_actor_set_size (right,2, icon_size);
		clutter_actor_set_position (right,left_marginal+icon_size , counter2*(icon_size+icon_marginal)+top_marginal);
		clutter_container_add_actor (CLUTTER_CONTAINER (group), right);

		clutter_container_add(CLUTTER_CONTAINER(container_for_saved_icons), group, NULL);

		//Icon has own callback, and this is additional.
		g_signal_connect_data(song->songbutton, "clicked", G_CALLBACK (set_previous_game_mode),NULL,(GClosureNotify) g_free,0);

		counter2++;
		}
	g_list_foreach(files, (GFunc)g_free, NULL);
	g_list_free(files);


		int counter = counter1>counter2?counter1:counter2; //max(counter1,counter2);
		//make movable shelves
		int i;
		for (i=0;i<counter;i++)
			{
			ClutterActor* b_icon = tangle_texture_new("/opt/jammo/cupboard_shelf.png");
			g_object_set(b_icon,"depth-position", -2, "width", 290.0, "height", 94.0, "x", 0.0,  "y", -1+i*94.0, NULL);
			clutter_container_add(CLUTTER_CONTAINER(container_for_saved_icons), b_icon, NULL);
			}



	//Check is there need for scrolling at all:
	TangleLayout* layout = tangle_widget_get_layout(TANGLE_WIDGET(container_for_saved_icons));
	GList* trick_list = tangle_layout_get_tricks(layout);
	ClutterActor* trick = trick_list->data;

	if (counter*94.0>376.0) {
			g_object_set(trick,"axis",TANGLE_SCROLL_Y_AXIS,NULL);
		}
	else {
		g_object_set(trick,"axis",TANGLE_SCROLL_NO_AXIS,NULL);
	}
	g_list_free(trick_list);


	clutter_actor_hide(jammo_get_actor_by_id("wait"));
	jammo_mentor_speak_once_with_callback(jammo_mentor_get_default(),"cupboard_welcome.spx",mentor_over,NULL);
	jammo_mentor_set_idle_speech(jammo_mentor_get_default(), "cupboard_welcome.spx");
	return FALSE;
}



static void doors_opened(ClutterTimeline* timeline, gpointer none)  {
	ClutterActor* actor;
	cem_add_to_log("cupboard: remove reactivity of cupboard doors, they are now opened",J_LOG_USER_ACTION);

	if ((actor = jammo_get_actor_by_id( "cupboard-leftdoor"))) {
		clutter_actor_set_reactive(actor,FALSE);
	}

	if ((actor = jammo_get_actor_by_id( "cupboard-rightdoor"))) {
		clutter_actor_set_reactive(actor,FALSE);
	}

}



//This is callback for json. @show-completed
void cupboard_start_cupboard(TangleActor* actor, gpointer data){
	cem_add_to_log("cupboard_start_cupboard",J_LOG_DEBUG);
	jammo_mentor_set_idle_speech(jammo_mentor_get_default(), ""); //If it takes too long to load
	ClutterActor* hourglass = jammo_get_actor_by_id("wait");
	clutter_actor_show(hourglass);
	g_timeout_add(10, load_icons_from_disk,NULL); //Clutter needs some time to show actor
}

//This is callback for json.
void cupboard_door_clicked(TangleActor* tangle_actor, gpointer data){
	cem_add_to_log("cupboard_door_clicked",J_LOG_USER_ACTION);
	jammo_mentor_shut_up(jammo_mentor_get_default());
	ClutterActor* actor;
	ClutterActor* actor2;
	ClutterAnimation* animation;
	ClutterAnimation* animation2;

	actor = jammo_get_actor_by_id("cupboard-leftdoor");
	g_assert(actor != NULL);

	actor2 = jammo_get_actor_by_id("cupboard-rightdoor");
	g_assert(actor2 != NULL);

	if ((animation = clutter_actor_get_animation(actor))) {
		clutter_animation_completed(animation);
		doors_opened(NULL,NULL);
	} else {
		animation = clutter_actor_animate(actor, CLUTTER_LINEAR, 5000, "rotation-angle-y", -150.0, "x", 217.0, NULL);
		ClutterTimeline* timeline;
		timeline = clutter_animation_get_timeline(animation);
		g_signal_connect(timeline,"completed", G_CALLBACK(doors_opened), NULL);
	}

if ((animation2 = clutter_actor_get_animation(actor2))) {
		clutter_animation_completed(animation2);
	} else {
		animation2 = clutter_actor_animate(actor2, CLUTTER_LINEAR, 5000, "rotation-angle-y", 150.0, "x", 490.0, NULL);
	}

}
