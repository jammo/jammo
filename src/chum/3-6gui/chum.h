/**chum.h is part of JamMo.
License: GPLv2, read more from COPYING

This file is for clutter based gui and it handles config file reading
*/


#ifndef _CHUM_H_
#define _CHUM_H_

#include <clutter/clutter.h>

#include "../../meam/jammo-sample.h" //We want return Sample*
#include "../../meam/jammo-editing-track.h"
#include "../../configure.h"
#include "song.h"
#include "jammo-collaboration-pair-composition.h"

JammoCollaborationPairComposition* chum_get_pair_composition();
void chum_set_pair_composition(JammoCollaborationPairComposition* pc);

jammo_song* chum_get_current_song();
void chum_set_current_song(jammo_song* song);

const char* chum_get_selected_language();
void  chum_set_selected_language(const gchar* language);

void chum_set_duetto(gboolean b);
gboolean chum_get_duetto();

gboolean chum_is_easy_game();
void chum_set_easy_game(gboolean b);

gboolean start_3_6_game(gpointer data);


//Start game over the network (by teacher)
void chum_remote_game_starter(gpointer data, gint16 track_id);

#endif  /*  _CHUM_H_  */
