/*
 * jammo_singinggame.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */

#include <tangle.h>
#include <clutter/clutter.h>
#include <unistd.h>  //we check some files
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


#include "../../meam/jammo-backing-track.h"
#include "../../meam/jammo-recording-track.h"
#include "../jammo-mentor.h"
#include "../jammo-cursor.h"
#include "../jammo.h"

#include "../../cem/cem.h"
#include "chum.h"
#include "../file_helper.h"
#include "song.h"
static JammoSequencer* singinggame_sequencer = NULL; //used for stopping


static GList* songs = NULL;


gboolean singinggame_language_selection_clicked(TangleButton* tanglebutton, gpointer sequencer){
	clutter_actor_show(jammo_get_actor_by_id("language-selection-view"));
	clutter_actor_hide(jammo_get_actor_by_id("songselection-view"));
	return TRUE;
}

gboolean singinggame_duetto_selection_clicked(TangleButton* tanglebutton, gpointer sequencer){
	clutter_actor_show(jammo_get_actor_by_id("duetto-selection-view"));
	clutter_actor_hide(jammo_get_actor_by_id("songselection-view"));
	return TRUE;
}

gboolean singinggame_cross_click(TangleButton* tanglebutton, gpointer sequencer){
/* //Debugging
	const gchar* id_of_actor;
	id_of_actor = clutter_get_script_id(source);

	printf("cross_clicked. get_id: '%s' and parent '%s'\n",id_of_actor,clutter_get_script_id(G_OBJECT(clutter_actor_get_parent(CLUTTER_ACTOR(source)))));
*/

	//This is essential. If user press cross when mentor is talking, this sequencer_stop is handled _before_ sequencer_play
	jammo_mentor_shut_up(jammo_mentor_get_default());

	jammo_sequencer_stop(singinggame_sequencer);
	return FALSE;
}

ClutterActor* singinggame_add_cursor(ClutterColor* color, JammoSequencer* sequencer, ClutterActor* parent, gboolean seekable) {
	ClutterActor *container = tangle_widget_new();
	ClutterActor* texture_color;
	ClutterActor* texture_image;
	ClutterActor* cursor;


	//Priorities:
	//1: cursor relating this language and this song
	//2: cursor relating this song
	//3: ad-hoc box
	gchar* filepath1;
	filepath1 = g_strdup_printf("%s/cursor%s.png",(chum_get_current_song())->fullpath, chum_get_selected_language() );

	if ( access (filepath1, R_OK) == -1) //File not found -> no cursor relating to this language
		{
		gchar* message = g_strdup_printf("no language-specific cursor '%s', checking if any cursor",filepath1);
		cem_add_to_log(message,J_LOG_DEBUG);
		g_free(message);

		g_free(filepath1);
		filepath1=NULL;
		filepath1 = g_strdup_printf("%s/cursor.png",(chum_get_current_song())->fullpath);
		if ( access (filepath1, R_OK) == -1)  //File not found -> no any cursor for this song
			{
			message = g_strdup_printf("no any cursor '%s', using empty box",filepath1);
			cem_add_to_log(message,J_LOG_DEBUG);
			g_free(message);

			g_free(filepath1);
			filepath1=NULL;
			}
		}

	if (filepath1) {
		texture_image = tangle_texture_new(filepath1);
		g_free(filepath1);
	}

	else {
		ClutterColor transparency = { 0, 0, 0, 0 };
		texture_image = clutter_rectangle_new_with_color(&transparency);
		clutter_actor_set_size(texture_image, 60.0,60.0);
	}


	gfloat width,height;
	clutter_actor_get_size(texture_image, &width, &height);
	
	texture_color = clutter_rectangle_new_with_color(color);
	clutter_actor_set_size(texture_color, width+4,height+4); //slightly bigger than image
	
	tangle_widget_add(TANGLE_WIDGET(container), texture_color, NULL);
	tangle_widget_add(TANGLE_WIDGET(container), texture_image, NULL);
	
	
	cursor = jammo_cursor_new(sequencer, container);
	g_object_set(cursor, "seekable", seekable, NULL);
	tangle_actor_set_depth_position(TANGLE_ACTOR(cursor), 1);
	clutter_actor_set_position(cursor, 0.0, 480.0-height-2);
	clutter_actor_set_size(cursor, 800.0, height+4);
	clutter_container_add_actor(CLUTTER_CONTAINER(parent), cursor);


	return cursor;
}


/*
This function is globally visible.
Whenever 'cross-icon' is pressed, this sequencer is stopped.
*/
void singinggame_start_sequencer(JammoSequencer* sequencer) {
	singinggame_sequencer = sequencer;
	jammo_sequencer_play(sequencer);
}


static void start_sequencer(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	clutter_actor_hide(CLUTTER_ACTOR(jammo_mentor_get_default()));
	singinggame_start_sequencer(JAMMO_SEQUENCER(sequencer));
}


static void on_sequencer_stopped_playback(JammoSequencer* sequencer_old, gpointer none) {
	cem_add_to_log("Playback-Sequencer stopped",J_LOG_INFO);

	//Hide all
	ClutterActor*	mainview = jammo_get_actor_by_id("main-views-widget"); //No drawing on stage anymore, use this.
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);
	
	//Go back to songselection
	ClutterActor* view = jammo_get_actor_by_id("songselection-view");
	clutter_actor_show(view);

	//And show mentor again
	tangle_actor_show(TANGLE_ACTOR(jammo_mentor_get_default()));

	//Saving to cupboard.
	gchar* cmd;

	cem_add_to_log("Do saving",J_LOG_DEBUG);
	char timestamp_now [80];
	cem_get_time(timestamp_now);

	char* filename= g_strdup_printf("%s/wavs/%s_%s.wav",configure_get_singings_directory(),(chum_get_current_song())->name_with_pack,timestamp_now);

	cmd = g_strdup_printf("mv \"%s/last_recorded.wav\" \"%s\"", configure_get_jammo_directory(), filename);

	//printf("Executing: '%s'\n",cmd);
	if (system(cmd)) {
		gchar* message = g_strdup_printf("Can't call '%s' ", cmd);
		cem_add_to_log(message, J_LOG_ERROR);
		g_free(message);
	}
	g_free(cmd);

	FILE *ofp;
	gchar *outputFilename;
	outputFilename = g_strdup_printf("%s/%s.json", configure_get_singings_directory(),(chum_get_current_song())->name_with_pack);

	//printf("outputFilename='%s'\n",outputFilename);
	ofp = fopen(outputFilename, "w");
	g_free(outputFilename);
	g_return_if_fail(ofp != NULL);

	fprintf(ofp,"{\n");
	fprintf(ofp,"\"fullpath\" : \"%s\",\n",(chum_get_current_song())->fullpath);
	fprintf(ofp,"\"recorded_filename\" : \"%s\",\n",filename);
	fprintf(ofp,"\"language\" : \"%s\"\n",chum_get_selected_language());
	fprintf(ofp,"}\n");
	fclose(ofp);

	g_free(filename);
}


void on_sequencer_stopped_recording(JammoSequencer* sequencer_old, gpointer user_data) {
	cem_add_to_log("Recording-Sequencer stopped",J_LOG_INFO);
	ClutterActor* view;
	
	view = CLUTTER_ACTOR(user_data);

	/*
	#ifdef MELODIC_CONTOUR
	clutter_actor_hide(contour_cursor);
	clutter_actor_hide(contour_trace);
	clutter_actor_hide(singinggame_display_freq);
	contour_counter=LEFT_MARGINAL;
	#endif
*/


	//make new sequencer and add tracks to it
	JammoSequencer* sequencer = jammo_sequencer_new();
	g_signal_connect(sequencer, "stopped", G_CALLBACK(on_sequencer_stopped_playback), NULL);

	gchar *filename;
	filename = g_strdup_printf("%s/comping%s.ogg", (chum_get_current_song())->fullpath,chum_get_selected_language());
	if ( access (filename, R_OK) == -1) //File not found
		{  //no track for this language,. use  common comping.
		g_free(filename);
		filename = g_strdup_printf("%s/comping.ogg", (chum_get_current_song())->fullpath); //try without language postfix
		}
	//printf("looking for file '%s' for comping_track\n",filename);

	JammoBackingTrack* backing_track;  
	backing_track = jammo_backing_track_new(filename);   //for comping track
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(backing_track));
	jammo_playing_track_set_volume(JAMMO_PLAYING_TRACK(backing_track), 0.5);
	//g_free(filename);


	gchar *loaded_filename;
	loaded_filename =  g_strdup_printf("%s/last_recorded.wav",configure_get_jammo_directory());
	//printf("looking for file '%s' for loaded_track\n",loaded_filename);


	//Recorded file must be there and it must not be zero sized.
	struct stat buf;
	if (stat(loaded_filename, &buf) == -1)
		{
		gchar* message = g_strdup_printf("Recorded file '%s' doesn't found",loaded_filename);
		cem_add_to_log(message, J_LOG_WARN);
		g_free(message);
		}
	else
		{
		if (buf.st_size == 0)
			{
			unlink(loaded_filename);
			}
		else
			{
			JammoBackingTrack* backing_track2;
			backing_track2 = jammo_backing_track_new(loaded_filename);
			jammo_sequencer_add_track(sequencer, JAMMO_TRACK(backing_track2));
			jammo_playing_track_set_volume(JAMMO_PLAYING_TRACK(backing_track2), 4.0);
			}
		}

	//g_free(loaded_filename);

	ClutterColor green = { 0, 255, 0, 128 };
	singinggame_add_cursor(&green, sequencer, view, TRUE);



	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "welldone.spx singinggame_listen.spx", start_sequencer, sequencer);
}


static gboolean initialization_needed = TRUE;

void  singinggame_clean_songlist() {
	if (songs){ //songs is static GList
		cem_add_to_log("Cleaning singing game song list", J_LOG_DEBUG);
		g_list_foreach (songs,(GFunc)song_free,NULL);
		g_list_free(songs);
	}
}

void singinggame_force_reinit_songs(){
	singinggame_clean_songlist();

	songs = song_init_songs_from_disk();
	clutter_actor_hide(jammo_get_actor_by_id("wait"));
	initialization_needed = TRUE;
}


/*
This funtion is called when songselection-view is showed (animation stopped).
If list of loaded songs had changed (initialization_needed==TRUE)
  previous song-container-widget is cleaned and all new icons are made (and frames for them)

Regardless of initialization_needed: current language are checked, and non-supported icons are dimmed (not disabled)

*/

void singinggame_start_songselection(){
	cem_add_to_log("Singinggame_start_songselection", J_LOG_INFO);

	ClutterActor* container = jammo_get_actor_by_id("songselection-all-songs");

	if (initialization_needed) {
		clutter_container_foreach(CLUTTER_CONTAINER(container), CLUTTER_CALLBACK(clutter_actor_hide), NULL); //TODO: this should not be needed
		clutter_container_foreach(CLUTTER_CONTAINER(container), CLUTTER_CALLBACK(clutter_actor_destroy), NULL);
	}

	gfloat height_of_container=0;
	const char* selected_language = chum_get_selected_language();

	//////////////
	//Change flag to corresponding language
	gchar *new_name = g_strdup_printf("%s/language_flag%s.png", DATA_DIR, selected_language); //name contains prefix _
	//printf("looking for '%s'\n",new_name);

	tangle_texture_set_from_file(TANGLE_TEXTURE(jammo_get_actor_by_id("songselection-language-texture")),new_name);
	g_free(new_name);
	////////////

	int column_counter=0;  //placement on x-direction
	int row_counter=0;      //placement on y-direction

	gfloat icon_size = 150.0;    //square
	int icon_marginal = 10;
	int left_marginal = 40;
	int top_marginal = 140;
	int items_in_one_row = 4;

	height_of_container+=1.0*top_marginal;
	height_of_container+=icon_size;
	height_of_container+=30; //just for marginal for bottom


	//songs is GList*, it is static so it is initialized once, not generated every time
	GList* l;
	for (l = songs; l; l = l->next)
		{
		if (column_counter==items_in_one_row)
			{
			column_counter=0;
			row_counter++;
			height_of_container+=1.0*icon_size+icon_marginal;
			//printf("Height of the container is now %f\n",height_of_container);
			}

		jammo_song* song = (jammo_song*)l->data;
		//printf("fullpath '%s', name_with_pack '%s' \n",song->fullpath,song->name_with_pack);
		//printf("xoff '%f', yoff '%f',xsize '%f', ysize '%f' \n", song->xoff,  song->yoff, song->x_size, song->y_size);

		if (initialization_needed)
			{
			//This is used size, clip, plaze
			gfloat x_ratio = icon_size / song->x_size;
			gfloat y_ratio = icon_size / song->y_size;

			// Size
			g_object_set( song->songbutton, "width",800.0 * x_ratio,
									 "height", 480.0 * y_ratio,
									 NULL);

			// Clip
			clutter_actor_set_clip (CLUTTER_ACTOR( song->songbutton),
															song->xoff*x_ratio,
															song->yoff *y_ratio,
															icon_size, icon_size);

			// Place
			g_object_set( song->songbutton,
									"x", column_counter*(icon_size+icon_marginal)+left_marginal - song->xoff*x_ratio,
									"y", row_counter*(icon_size+icon_marginal)+top_marginal -  song->yoff*y_ratio,
									NULL);


			//Image and frames goes one group
			ClutterActor* group = tangle_widget_new();
			tangle_widget_add (TANGLE_WIDGET (group), song->songbutton,NULL);

			//Frames.
			ClutterColor black = { 0,0,0, 255 };

			ClutterActor* top = clutter_rectangle_new_with_color (&black);
			clutter_rectangle_set_border_color(CLUTTER_RECTANGLE(top),&black);
			clutter_rectangle_set_border_width(CLUTTER_RECTANGLE(top),2);
			clutter_actor_set_size (top,icon_size, 2);
			clutter_actor_set_position (top,column_counter*(icon_size+icon_marginal)+left_marginal , row_counter*(icon_size+icon_marginal)+top_marginal); 
			tangle_widget_add (TANGLE_WIDGET (group), top,NULL);

			ClutterActor* bottom = clutter_rectangle_new_with_color (&black);
			clutter_rectangle_set_border_color(CLUTTER_RECTANGLE(bottom),&black);
			clutter_rectangle_set_border_width(CLUTTER_RECTANGLE(bottom),2);
			clutter_actor_set_size (bottom,icon_size, 2);
			clutter_actor_set_position (bottom,column_counter*(icon_size+icon_marginal)+left_marginal , row_counter*(icon_size+icon_marginal)+top_marginal+icon_size); 
			tangle_widget_add (TANGLE_WIDGET (group), bottom,NULL);

			ClutterActor* left = clutter_rectangle_new_with_color (&black);
			clutter_rectangle_set_border_color(CLUTTER_RECTANGLE(left),&black);
			clutter_rectangle_set_border_width(CLUTTER_RECTANGLE(left),2);
			clutter_actor_set_size (left,2, icon_size);
			clutter_actor_set_position (left,column_counter*(icon_size+icon_marginal)+left_marginal , row_counter*(icon_size+icon_marginal)+top_marginal); 
			tangle_widget_add (TANGLE_WIDGET (group), left,NULL);

			ClutterActor* right = clutter_rectangle_new_with_color (&black);
			clutter_rectangle_set_border_color(CLUTTER_RECTANGLE(right),&black);
			clutter_rectangle_set_border_width(CLUTTER_RECTANGLE(right),2);
			clutter_actor_set_size (right,2, icon_size);
			clutter_actor_set_position (right,column_counter*(icon_size+icon_marginal)+left_marginal+icon_size , row_counter*(icon_size+icon_marginal)+top_marginal); 
			tangle_widget_add (TANGLE_WIDGET (group), right,NULL);

			tangle_widget_add(TANGLE_WIDGET(container), group, NULL);
			}

		//This dim/undim checking is done every time
		ClutterActor* texture;
		g_object_get(song->songbutton,"normal-background-actor", &texture, NULL);

		gchar* filepath_vocal;
		filepath_vocal = g_strdup_printf("%s/vocal%s.ogg",song->fullpath,selected_language);
		//printf("filename for vocal: '%s'\n",filepath_vocal);

		 //Checking if there are introXX.spx -> then do not dim icon.
		gchar* filepath_intro;
		filepath_intro =  g_strdup_printf("%s/intro%s.spx", song->fullpath, chum_get_selected_language());

		//If these all are true -> icon will be dimmed
		if ( chum_get_duetto() &&   /* Vocal selected */
           access (filepath_intro, R_OK) == -1 &&  /* No intro (intro means special song)*/
           access (filepath_vocal, R_OK) == -1) /* No vocal_track for this language */
			{
			//printf(" Dim this\n");
			clutter_actor_set_opacity(texture ,64); //0=transparency, 255=opaque
			}
		else
			{
			//printf(" unDim this\n");
			clutter_actor_set_opacity(texture,255); //0=transparency, 255=opaque
			}

		g_free(filepath_vocal);
		g_free(filepath_intro);
		column_counter++;

		}

	const gchar* mentor_speech;

	TangleLayout* layout = tangle_widget_get_layout(TANGLE_WIDGET(container));
	GList* trick_list = tangle_layout_get_tricks(layout);
	ClutterActor* trick = trick_list->data;

	if (height_of_container>480) { //tell more instructions ( about scrolling )
			g_object_set(trick,"axis",TANGLE_SCROLL_Y_AXIS,NULL);
			mentor_speech="songselection_welcome.spx songselection_welcome_advanced.spx";
		}
	else {
		g_object_set(trick,"axis",TANGLE_SCROLL_NO_AXIS,NULL);
		mentor_speech="songselection_welcome.spx";
	}
	g_list_free(trick_list);

	//printf("height_of_container:%f\n",height_of_container);
	jammo_mentor_speak_once(jammo_mentor_get_default(),mentor_speech);


	if (chum_get_duetto())
		jammo_mentor_set_idle_speech(jammo_mentor_get_default(),"songselection_idle.spx songselection_idle_vocal.spx");
	else
		jammo_mentor_set_idle_speech(jammo_mentor_get_default(),"songselection_idle.spx songselection_idle_instrument.spx");

	initialization_needed=FALSE;
}

static gulong handler_for_mentor_say_selected;

static void mentor_say_what_is_selected(TangleActor *actor, ClutterActorBox *arg1, gpointer user_data){
	//printf("got '%s'\n",(gchar*)user_data);
	gchar *filepath1;
	filepath1 =  g_strdup_printf("%s_selected.spx", (gchar*)user_data);

	jammo_mentor_speak(jammo_mentor_get_default(), filepath1);
	g_free(filepath1);
	g_signal_handler_disconnect (actor,handler_for_mentor_say_selected);
}

gboolean singinggame_duetto_clicked (TangleActor* actor, gpointer data) {
	ClutterActor* ct =CLUTTER_ACTOR(actor);
	const gchar* name = clutter_actor_get_name(ct);

	clutter_actor_hide(jammo_get_actor_by_id("duetto-selection-view"));
	clutter_actor_show(jammo_get_actor_by_id("songselection-view"));

	gchar* message = g_strdup_printf("%s clicked",name);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);

	ClutterActor* view = jammo_get_actor_by_id("songselection-view");
	TangleButton* tanglebutton = TANGLE_BUTTON(jammo_get_actor_by_id("songselection-singmode-button"));

	if (strcmp(name,"cancel")==0) {
		tangle_button_set_selected(tanglebutton,!tangle_button_get_selected(tanglebutton));
		return TRUE;
	}

	if (strcmp(name,"duetto")==0)
		{
		tangle_button_set_selected(tanglebutton,FALSE);
		chum_set_duetto(TRUE);
		}
	else if (strcmp(name,"solo")==0)
		{
		tangle_button_set_selected(tanglebutton,TRUE);
		chum_set_duetto(FALSE);
		}
	gchar *name_for_mentor;
	name_for_mentor =  g_strdup_printf("%s", name);
	handler_for_mentor_say_selected=g_signal_connect_data(view, "show-completed", G_CALLBACK(mentor_say_what_is_selected),
                                                                  (gpointer)name_for_mentor, (GClosureNotify) g_free,0);

	return TRUE;
}


gboolean singinggame_language_clicked (TangleActor* actor, gpointer data) {
	ClutterActor* ct =CLUTTER_ACTOR(actor);
	const gchar* name = clutter_actor_get_name(ct);

	gchar* message = g_strdup_printf("Language '%s' clicked",name);
	cem_add_to_log(message,J_LOG_DEBUG);
	g_free(message);



	ClutterActor* view = jammo_get_actor_by_id("songselection-view");
	if (strcmp(name,"cancel")!=0) {

	chum_set_selected_language(name);
	/*
	There aren't substring(begin,end) in g_
	e.g.:
	name="_en"
	reverse it:
	"ne_"
	take only two character
	"ne" (g_strndup adds automatically \0)
	reverse it again
	"en"
	*/
	char* name_for_mentor;
	char* name_for_mentor1 = g_strdup(name);
	name_for_mentor = g_strreverse (g_strndup (g_strreverse(name_for_mentor1),2));
	g_free(name_for_mentor1);
	handler_for_mentor_say_selected = g_signal_connect_data(view, "show-completed", G_CALLBACK(mentor_say_what_is_selected),
                                                                     (gpointer)name_for_mentor, (GClosureNotify) g_free,0);
	}

	clutter_actor_hide(jammo_get_actor_by_id("language-selection-view"));
	clutter_actor_show(view);

	jammo_mentor_set_idle_speech(jammo_mentor_get_default(), ""); //If it takes too long to load
	ClutterActor* hourglass = jammo_get_actor_by_id("wait");
	clutter_actor_show(hourglass);
	singinggame_force_reinit_songs();
	return TRUE;
}



