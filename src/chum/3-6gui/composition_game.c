#include <tangle.h>
#include <clutter/clutter.h>
#include <string.h>

#include "../jammo.h" //e.g. jammo_get_actor_by_id
#include "../jammo-mentor.h"

#include "chum.h"
#include "theme.h"
#include "composition_game_file_helper.h"
#include "themeselection.h"
#include "composition_game.h"
#include "../../cem/cem.h"

static jammo_theme* current_theme; //One theme is handled at a time.

static ClutterActor* view_to_go_when_end; //After start this points themeselection or cupboard
static gboolean only_playing_mode=FALSE; //When editing this is FALSE. When coming from cupboard, only listen is allowed

static gboolean already_quitted = FALSE; //Pressing cross when quitting has already started.

void composition_game_close(){
	cem_add_to_log("composition_game_closed, going to themeselection",J_LOG_INFO);
	if(already_quitted) {
		//printf("double quitting\n");
		return;
	}
	already_quitted = TRUE;

	only_playing_mode=FALSE; //for next round

	//Hide the saving menu
	clutter_actor_hide(jammo_get_actor_by_id("saving-dialog-view"));
	//When user selects icon for saved game, cancel is hided. Make it visible again
	// Because container is hidden, it will not pop on the screen now (but for next round)
	// If user close game with empty track, there are no actor "saving_dialog_cancel".
	ClutterActor* saving_dialog_cancel = jammo_get_actor_by_id("saving_dialog_cancel");
	if (saving_dialog_cancel)
		clutter_actor_show(saving_dialog_cancel);

	ClutterActor* button_layer = jammo_get_actor_by_id("composition_game_buttons");
	tangle_actor_hide_animated(TANGLE_ACTOR(button_layer));

	ClutterActor* view = jammo_get_actor_by_id("compositiongame-view");

	clutter_actor_show(CLUTTER_ACTOR(jammo_mentor_get_default())); /*This is needed when coming from cupboard, and mentor is hided*/
	delete_theme(current_theme);
	if (chum_get_pair_composition()) {
		g_object_unref(chum_get_pair_composition());
		chum_set_pair_composition(NULL);
		//Go back to pair-game menu.
		themeselection_going_to(NULL,NULL);
	}

	else { //single player game: respect 'view_to_go_when_end'
		tangle_actor_show(TANGLE_ACTOR(view_to_go_when_end));
	}

	tangle_actor_hide_animated(TANGLE_ACTOR(view));
}


static void hide_save_menu(){
	clutter_actor_hide(jammo_get_actor_by_id("saving-dialog-view"));
	theme_play_all_icon_animations(current_theme);
}

void composition_game_quit_without_saving_clicked (TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("composition_game_quit_without_saving",J_LOG_USER_ACTION);
	jammo_mentor_shut_up(jammo_mentor_get_default());
	composition_game_close();
}

void composition_game_back_to_game_clicked (TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("composition-game back to game from saving-menu clicked",J_LOG_USER_ACTION);
	hide_save_menu();
	//technically we haven't changed the view, so mentor_idle_speech is not changed automatically
	tangle_activity_activate(TANGLE_ACTIVITY(jammo_get_object_by_id("compositiongame-mentor-activity")));
}


/*
This is called when user click 'cross' / 'back_to_theme_selection'
*/
void composition_game_cross_clicked (TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("composition_game_cross_clicked",J_LOG_USER_ACTION);
	jammo_sample_stop_all();

	//In only_playing_mode stopping sequencer means also game_close -> and after that only_playing_mode=FALSE.
	if (only_playing_mode){
			jammo_sequencer_stop(current_theme->sequencer);
			return;
	}
	else
		jammo_sequencer_stop(current_theme->sequencer);

	/*
	When cross is clicked, saving menu is showed. There are couple of situations when menu is not showed
	-We are coming from cupboard, no edit has happened (This case is handled already)
	-Track1 is empty and it is only track
	-Track1 is empty and there are also empty track2
	*/
	gboolean pop_menu = TRUE;

	if (tangle_widget_get_n_children(TANGLE_WIDGET(current_theme->bottom_track_view)) ==0) { //track1 empty
		if  (current_theme->upper_track_view == NULL)  //only one track
			pop_menu = FALSE;
		else  //There are two tracks
			if (tangle_widget_get_n_children(TANGLE_WIDGET(current_theme->upper_track_view)) ==0) { //track2 empty
				pop_menu = FALSE;
			}
		}

	if (pop_menu){
		theme_stop_all_icon_animations(current_theme);
		clutter_actor_show(jammo_get_actor_by_id("saving-dialog-view"));
	}
	else
		composition_game_close();
}



void composition_game_play_clicked (TangleButton* tanglebutton, gpointer none){
	//If user press play-button when first sample is playing (triggering mentor to give instructions for dragging)
	//Mentor jumps and talks sametime than track.
	//This fixes it:
	jammo_mentor_speak_once(jammo_mentor_get_default(), "composing_first_click.spx");
	jammo_mentor_shut_up(jammo_mentor_get_default());

	//tanglebutton is toggle-button.
	gboolean selected;
	g_object_get(tanglebutton, "selected", &selected,  NULL);

	if (selected)
		{
		theme_stop_all_icon_animations(current_theme);
		cem_add_to_log("Play-button pressed",J_LOG_USER_ACTION);
		tangle_actor_hide_animated(TANGLE_ACTOR(jammo_mentor_get_default()));

		jammo_sample_stop_all();
		jammo_sequencer_play(JAMMO_SEQUENCER(current_theme->sequencer));

		jammo_editing_track_view_set_editing_enabled(current_theme->bottom_track_view, FALSE);
		if (current_theme->upper_track_view) {
			jammo_editing_track_view_set_editing_enabled(current_theme->upper_track_view, FALSE);
		}

		g_object_set(tanglebutton, "x", 720.0,  NULL);
		}

	else
		{
		cem_add_to_log("Stop-button pressed",J_LOG_USER_ACTION);;
		jammo_sequencer_stop(JAMMO_SEQUENCER(current_theme->sequencer)); //This will automatically cause 'on_sequencer_stopped'
		}

}



//This is called from cupboard
static gpointer filename_for_loading;
void composition_game_start_from_file(gpointer data, gchar* theme_name, int variation_name) {
	cem_add_to_log("composition_game_start_from_file",J_LOG_DEBUG);
	only_playing_mode=TRUE;
	filename_for_loading = data;
	composition_game_start_theme(theme_name,variation_name,jammo_get_actor_by_id("cupboard-view"));
}

static void do_actual_loadind_now(){
	//printf("loading now\n");
	composition_game_file_helper_load_from_file(current_theme, filename_for_loading, current_theme->bottom_track_view, current_theme->upper_track_view);
	//printf("loaded\n");

	//printf("hide mentor\n");
	clutter_actor_hide(CLUTTER_ACTOR(jammo_mentor_get_default()));

	jammo_editing_track_view_set_editing_enabled(current_theme->bottom_track_view, FALSE);
	if (current_theme->upper_track_view) {
		jammo_editing_track_view_set_editing_enabled(current_theme->upper_track_view, FALSE);
	}

	//printf("start sequencer\n");
	jammo_sequencer_play(current_theme->sequencer);
	//printf("do_actual_loadind_now ready\n");
}





static gulong handler_for_view_transition;
static void view_transition_completed(TangleActor *actor, ClutterActorBox *arg1, ClutterActorBox *arg2, gpointer sequencer){

		if (only_playing_mode) //this means we have something to load
			do_actual_loadind_now();
		g_signal_handler_disconnect (actor,handler_for_view_transition);
}


jammo_theme* composition_game_start_theme(const gchar* name, int variation, ClutterActor* where_to_go_after){
	view_to_go_when_end = where_to_go_after; //We set this static ClutterActor, so we know where to go when composition game is quitted.

	gchar* message = g_strdup_printf("Theme '%s' starting. Variation is %d",name,variation);
	cem_add_to_log(message,J_LOG_DEBUG);
	g_free(message);

	already_quitted = FALSE;
	//Change Background for this theme
	gchar *background_image_name = g_strdup_printf("%s/themes/%s/background.png",DATA_DIR,name);
	ClutterActor *background;
	background=tangle_texture_new(background_image_name);
	g_free(background_image_name);



	ClutterActor* view;
	if ((view = jammo_get_actor_by_id("compositiongame-view"))) {
		handler_for_view_transition = g_signal_connect(view, "transition-completed", G_CALLBACK(view_transition_completed), NULL);
		g_object_set(view, "background-actor", background, NULL);
		clutter_actor_show(view);
	}

	//play_button is needed, because sequencer will handle changing it to stop-button
	// X is needed bacause it adds
	current_theme=theme_create(name,variation,only_playing_mode,jammo_get_actor_by_id("composition_game_play")); /*static current_theme */

	//Add samplebuttons and track_views
	clutter_container_add(CLUTTER_CONTAINER(view), current_theme->container, NULL);

	//Add save_menu icons to and hide save-menu
	ClutterActor* saving_dialog = jammo_get_actor_by_id("saving-dialog-view");
	clutter_actor_raise_top(saving_dialog);
	clutter_container_add(CLUTTER_CONTAINER(saving_dialog), current_theme->save_menu_container, NULL); //TODO: tangle_widget

	ClutterActor* button_layer = jammo_get_actor_by_id("composition_game_buttons");
	tangle_actor_show(TANGLE_ACTOR(button_layer));

	return current_theme;
}
