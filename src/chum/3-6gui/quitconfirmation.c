#include <tangle.h>
#include <clutter/clutter.h>
#include <stdlib.h> //For exit()

#include "../jammo.h"  //jammo_get_actor_by_id
#include "../jammo-mentor.h"
#include "../../cem/cem.h"
#include "singinggame.h"
#include "main_menu.h"

static void end_jammo(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	cem_add_to_log("Quitted by player", J_LOG_INFO);
	singinggame_clean_songlist();
	clutter_main_quit();
}

gboolean quitconfirmation_quit_confirmed(TangleActor* actor, gpointer data){
	cem_add_to_log("Quit confirmed", J_LOG_USER_ACTION);
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "quit_byebye.spx", end_jammo, NULL);
	return TRUE;
}

//@show-completed
void quitconfirmation_mentor_action(TangleActor* actor, gpointer data){
	//Hide mentor (withoud hiding)
	clutter_actor_set_width(CLUTTER_ACTOR(jammo_mentor_get_default()),0.1);
	clutter_actor_set_height(CLUTTER_ACTOR(jammo_mentor_get_default()),0.1);

	jammo_mentor_speak(jammo_mentor_get_default(), "quit_confirmation.spx");
	jammo_mentor_set_idle_speech(jammo_mentor_get_default(), "quit_confirmation.spx");
}

gboolean quitconfirmation_cancel_action(TangleActor* actor, gpointer data){
	cem_add_to_log("Quit canceled", J_LOG_USER_ACTION);
	jammo_mentor_shut_up(jammo_mentor_get_default());
	clutter_actor_set_width(CLUTTER_ACTOR(jammo_mentor_get_default()),309);
	clutter_actor_set_height(CLUTTER_ACTOR(jammo_mentor_get_default()),480);

	jammo_mentor_set_idle_speech(jammo_mentor_get_default(), "main_menu_idle.spx");
	main_menu_go_back_to_main_menu(NULL,NULL);

	return TRUE;
}
