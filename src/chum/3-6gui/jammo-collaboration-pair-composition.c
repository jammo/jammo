/*
 * jammo-collaboration-pair-composition.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther <mikko.gynther@lut.fi>
 */

#include "../jammo-collaboration-game.h"
#include "jammo-collaboration-pair-composition.h"
#include "theme.h"
#include "composition_game.h"
#include "../jammo.h" // jammo_get_actor_by_id

G_DEFINE_TYPE(JammoCollaborationPairComposition, jammo_collaboration_pair_composition, JAMMO_TYPE_COLLABORATION_GAME);

#include "../../meam/jammo-sequencer.h"
#include "../../meam/jammo-backing-track.h"
#include "../jammo-editing-track-view.h"
#include "../../meam/jammo-loop.h"

#ifdef NETWORKING_ENABLED
#include "../../gems/gems_definitions.h"
#include "../../gems/groupmanager.h"
#include "../../gems/gems.h"
#include "../../gems/collaboration.h"
#endif

enum {
	PROP_0,
	PROP_OWN_TRACK_VIEW,
	PROP_PEER_TRACK_VIEW,
	PROP_JAMMO_THEME
};

struct _JammoCollaborationPairCompositionPrivate {

	JammoEditingTrackView * own_track_view;
	JammoEditingTrackView * peer_track_view;
	guint peer_user_id;
	gboolean host;
	jammo_theme * jammotheme;
};

// function prototypes
void jammo_collaboration_pair_composition_on_sequencer_stopped_recording(JammoSequencer* sequencer, gpointer data);
int create_mix_of_pair_composition(gpointer data);
void exit_pair_composition(JammoCollaborationPairComposition * collaboration_pair_composition);
static void clean_up(JammoCollaborationPairComposition * collaboration_pair_composition);

/*This will make non-HOST player*/
JammoCollaborationPairComposition* jammo_collaboration_pair_composition_new() {
	return JAMMO_COLLABORATION_PAIR_COMPOSITION(g_object_new(JAMMO_TYPE_COLLABORATION_PAIR_COMPOSITION, NULL));
}

/*
call with TRUE, if caller will be host
otherwise with FALSE
*/
JammoCollaborationPairComposition* jammo_collaboration_pair_composition_new_host(gboolean host) {
	JammoCollaborationPairComposition* collaboration_pair_composition = jammo_collaboration_pair_composition_new();
	collaboration_pair_composition->priv->host=host;
	return collaboration_pair_composition;
}

/*
Returns FALSE if parameter is NULL.
*/
gboolean jammo_collaboration_pair_composition_is_host(JammoCollaborationPairComposition * collaboration_pair_composition) {
	if (!collaboration_pair_composition)
		return FALSE;
	return collaboration_pair_composition->priv->host;
}

ClutterActor* user_id_to_track(guint user_id);

static void jammo_collaboration_pair_composition_add_jammo_sample_button_remote(GObject * game, guint user_id, guint loop_id, guint slot) {

	JammoCollaborationPairComposition * collaboration_pair_composition = JAMMO_COLLABORATION_PAIR_COMPOSITION(game);

	//ClutterActor* track_view = user_id_to_track(user_id);
	printf("remote: add_sample, uid %u, loop_id %u, slot %u\n", user_id, loop_id, slot);

	gchar* theme;
	gint variation;
	g_object_get(collaboration_pair_composition,"theme",&theme, NULL);
	g_object_get(collaboration_pair_composition,"variation",&variation, NULL);

	printf("got theme :'%s', variation: '%d'\n",theme,variation);
	//TODO:
	jammo_theme * jammotheme;
	g_object_get(collaboration_pair_composition, "jammotheme", &jammotheme, NULL);

	JammoSampleButton* sample_button = JAMMO_SAMPLE_BUTTON(theme_give_sample_button_for_this_theme_and_variation_for_this_id(jammotheme,theme,variation,loop_id));

	clutter_actor_meta_set_enabled(CLUTTER_ACTOR_META(tangle_actor_get_action_by_type(CLUTTER_ACTOR(sample_button), TANGLE_TYPE_DRAG_ACTION)), FALSE);
	jammo_editing_track_view_add_jammo_sample_button(collaboration_pair_composition->priv->peer_track_view, sample_button, slot);

	printf("peer-track used (pointer:'%p')\n",collaboration_pair_composition->priv->peer_track_view);
}

static void jammo_collaboration_pair_composition_remove_jammo_sample_button_remote(GObject * game, guint user_id, guint slot) {
	JammoCollaborationPairComposition * collaboration_pair_composition = JAMMO_COLLABORATION_PAIR_COMPOSITION(game);

	printf("remote: remove_sample, uid %u, slot %u\n", user_id,  slot);
	jammo_editing_track_view_remove_jammo_sample_button_from_slot(collaboration_pair_composition->priv->peer_track_view, slot);

	printf("peer-track used (pointer:'%p')\n",collaboration_pair_composition->priv->peer_track_view);
}

static void jammo_collaboration_pair_composition_loop_sync_remote(GObject * game, guint32 user_id, GList * list) {
	GList* temp;
	JammoLoop* n;

	// TODO clear existing sample buttons

	//TODO: loop_id --> sample_button

	//TODO: add loops
	printf("jammo-collaboration-pair-composition loop sync stub, user id %u\n", user_id);
  printf("Loop list:\n");
	for (temp = list; temp; temp = temp->next) {
		n = (temp->data);
		printf("  loop_id %u, slot %u\n", n->loop_id,n->slot);
	}
}


static void jammo_collaboration_pair_composition_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoCollaborationPairComposition * collaboration_pair_composition;
	
	collaboration_pair_composition = JAMMO_COLLABORATION_PAIR_COMPOSITION(object);

	
	switch (prop_id) {
		case PROP_OWN_TRACK_VIEW:
			if (collaboration_pair_composition->priv->own_track_view!=NULL) {
				g_object_unref(collaboration_pair_composition->priv->own_track_view);
			}
			if (JAMMO_IS_EDITING_TRACK_VIEW(g_value_get_object(value))) {
				collaboration_pair_composition->priv->own_track_view = JAMMO_EDITING_TRACK_VIEW(g_value_get_object(value));
			}
			else {
				collaboration_pair_composition->priv->own_track_view = NULL;
			}
			break;
		case PROP_PEER_TRACK_VIEW:
			if (collaboration_pair_composition->priv->peer_track_view!=NULL) {
				g_object_unref(collaboration_pair_composition->priv->peer_track_view);
			}
			if (JAMMO_IS_EDITING_TRACK_VIEW(g_value_get_object(value))) {
				collaboration_pair_composition->priv->peer_track_view = JAMMO_EDITING_TRACK_VIEW(g_value_get_object(value));
				//Callback for multiplaying
				printf("peer-track setted (pointer:'%p')\n",collaboration_pair_composition->priv->peer_track_view);
				gems_set_callback_add_loop(jammo_collaboration_pair_composition_add_jammo_sample_button_remote);
				gems_set_callback_remove_loop(jammo_collaboration_pair_composition_remove_jammo_sample_button_remote);
				gems_set_callback_loop_sync(jammo_collaboration_pair_composition_loop_sync_remote);
			}
			else {
				collaboration_pair_composition->priv->peer_track_view = NULL;
			}
			break;
		case PROP_JAMMO_THEME:
			collaboration_pair_composition->priv->jammotheme = g_value_get_pointer(value);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_collaboration_pair_composition_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
	JammoCollaborationPairComposition * collaboration_pair_composition;
	
	collaboration_pair_composition = JAMMO_COLLABORATION_PAIR_COMPOSITION(object);

	switch (prop_id) {
		case PROP_OWN_TRACK_VIEW:
			g_value_set_object(value, collaboration_pair_composition->priv->own_track_view);
			break;
		case PROP_PEER_TRACK_VIEW:
			g_value_set_object(value, collaboration_pair_composition->priv->peer_track_view);
			break;
		case PROP_JAMMO_THEME:
			g_value_set_pointer(value, collaboration_pair_composition->priv->jammotheme);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

// implementations for base class functions

// implementation for teacher exit
void jammo_collaboration_pair_composition_teacher_exit(JammoCollaborationGame * collaboration_game) {
	/*
	// TODO implement teacher exit
	JammoCollaborationPairComposition * pair_composition;
	pair_composition = JAMMO_COLLABORATION_PAIR_COMPOSITION(collaboration_game);
	*/
	printf("TODO: jammo_collaboration_pair_composition_teacher_exit called\n");
}

// create_song_file
// return values 0 success, 1 can not open file
int jammo_collaboration_pair_composition_create_song_file(JammoCollaborationGame * collaboration_game) {
	/*
	// TODO implement
	JammoCollaborationPairComposition * pair_composition;
	pair_composition = JAMMO_COLLABORATION_PAIR_COMPOSITION(collaboration_game);
	*/
	printf("TODO: jammo_collaboration_pair_composition_create_song_file called\n");

	return 0;
}

void jammo_collaboration_pair_composition_game_callback(GObject * game, int type, const gchar* song_info) {
	printf("jammo_collaboration_pair_composition_game_callback\n");
	JammoCollaborationPairComposition * collaboration_pair_composition = JAMMO_COLLABORATION_PAIR_COMPOSITION(game);
	
	// local player is host and gems informes that peer has joined
	if (collaboration_pair_composition->priv->host && type==GEMS_GAME_JOINED_PAIR_COMPOSITION) {
		printf("jammo_collaboration_pair_composition: peer joined pair composition\n");
		//composition_game_start_theme("city",1, jammo_get_actor_by_id("main-views-widget"));
		guint32* plist = gems_group_get_group_members();
		if(plist[0] != FREE_SLOT) {
			guint peer_user_id = plist[0];
			printf("got peer id %u\n", peer_user_id);
			collaboration_pair_composition->priv->peer_user_id=peer_user_id;
		}
	}
	else if (type==GEMS_GAME_JOINED_PAIR_COMPOSITION) {
		printf("jammo_collaboration_pair_composition: joined pair composition\n");
	}

	if (song_info) {
		printf("song_info: %s\n", song_info);

		//Debug. (maybe there are not need to convert it to int and back)
		int theme_number = song_info[0] - '0'; //char to int conversion
		int variation = song_info[1] - '0';

		gchar* theme="";
		if (theme_number==1)
			theme="animal";
		else if (theme_number==2)
			theme="city";
		else if (theme_number==3)
			theme="fantasy";

		//printf("putting theme '%s' and variation %d\n",theme,variation);
		g_object_set(collaboration_pair_composition,"theme",theme, NULL);
		g_object_set(collaboration_pair_composition,"variation",variation, NULL);

		jammo_theme* jammotheme = composition_game_start_theme(theme,variation,jammo_get_actor_by_id("main-menu-view")); //Last parameter = where to go when game ends
		g_object_set(collaboration_pair_composition,"jammotheme",jammotheme, NULL);
	}
}

static GObject* jammo_collaboration_pair_composition_constructor(GType type, guint n_properties, GObjectConstructParam* properties) {
	GObject* object;
	JammoCollaborationPairComposition * collaboration_pair_composition;
	
	object = G_OBJECT_CLASS(jammo_collaboration_pair_composition_parent_class)->constructor(type, n_properties, properties);

	collaboration_pair_composition = JAMMO_COLLABORATION_PAIR_COMPOSITION(object);
	collaboration_pair_composition->priv->own_track_view=NULL;
	collaboration_pair_composition->priv->peer_track_view=NULL;
	collaboration_pair_composition->priv->peer_user_id=0;
	collaboration_pair_composition->priv->host=FALSE;
	return object;
}


static void jammo_collaboration_pair_composition_finalize(GObject* object) {
	/*
	JammoCollaborationPairComposition* collaboration_pair_composition;
	collaboration_pair_composition = JAMMO_COLLABORATION_PAIR_COMPOSITION(object);
	*/
	G_OBJECT_CLASS(jammo_collaboration_pair_composition_parent_class)->finalize(object);
}

static void jammo_collaboration_pair_composition_dispose(GObject* object) {
	JammoCollaborationPairComposition* collaboration_pair_composition = JAMMO_COLLABORATION_PAIR_COMPOSITION(object);
	clean_up(collaboration_pair_composition);
	gems_group_leave_from_group(NULL);
	G_OBJECT_CLASS(jammo_collaboration_pair_composition_parent_class)->dispose(object);
}

static void jammo_collaboration_pair_composition_class_init(JammoCollaborationPairCompositionClass* collaboration_pair_composition_class) {
	printf("class init\n");
	GObjectClass* gobject_class = G_OBJECT_CLASS(collaboration_pair_composition_class);
	JammoCollaborationGameClass* collaboration_game_class = JAMMO_COLLABORATION_GAME_CLASS(collaboration_pair_composition_class);

	gobject_class->constructor = jammo_collaboration_pair_composition_constructor;
	gobject_class->finalize = jammo_collaboration_pair_composition_finalize;
	gobject_class->dispose = jammo_collaboration_pair_composition_dispose;
	gobject_class->set_property = jammo_collaboration_pair_composition_set_property;
	gobject_class->get_property = jammo_collaboration_pair_composition_get_property;

	collaboration_game_class->teacher_exit = jammo_collaboration_pair_composition_teacher_exit;
	collaboration_game_class->create_song_file = jammo_collaboration_pair_composition_create_song_file;
	collaboration_game_class->game_callback = jammo_collaboration_pair_composition_game_callback;

	g_object_class_install_property(gobject_class, PROP_OWN_TRACK_VIEW,
	                                g_param_spec_object("own-track-view",
	                                "Own track view",
	                                "Player's own track view",
	                                JAMMO_TYPE_EDITING_TRACK_VIEW, G_PARAM_READABLE | G_PARAM_WRITABLE));

	g_object_class_install_property(gobject_class, PROP_PEER_TRACK_VIEW,
	                                g_param_spec_object("peer-track-view",
	                                "Peer track view",
	                                "Peer's track view",
	                                JAMMO_TYPE_EDITING_TRACK_VIEW, G_PARAM_READABLE | G_PARAM_WRITABLE));

	g_object_class_install_property(gobject_class, PROP_JAMMO_THEME,
	                                g_param_spec_pointer("jammotheme",
	                                "Theme",
	                                "Theme",
	                                G_PARAM_READABLE | G_PARAM_WRITABLE));

	g_type_class_add_private(gobject_class, sizeof(JammoCollaborationPairCompositionPrivate));
}

static void jammo_collaboration_pair_composition_init(JammoCollaborationPairComposition* collaboration_pair_composition) {
	collaboration_pair_composition->priv = G_TYPE_INSTANCE_GET_PRIVATE(collaboration_pair_composition, JAMMO_TYPE_COLLABORATION_PAIR_COMPOSITION, JammoCollaborationPairCompositionPrivate);
}

void jammo_collaboration_pair_composition_loop_sync (JammoCollaborationPairComposition * group_composition) {
	// TODO get own loops from view and send them
	GList * list =NULL;
	gems_loop_sync(list);
	jammo_loop_free_glist(&list);
}

// functions needed in pair composition

void jammo_collaboration_pair_composition_on_sequencer_stopped_recording(JammoSequencer* sequencer, gpointer data) {

	// uncomment to use object
	/*JammoCollaborationPairComposition * collaboration_pair_composition = (JammoCollaborationPairComposition *)data;*/

	// uncomment adding callback to start generating mix of pair composition
	/*g_timeout_add_full(G_PRIORITY_DEFAULT,100,(GSourceFunc)create_mix_of_pair_composition,collaboration_pair_composition,NULL);*/
	
}

int create_mix_of_pair_composition(gpointer data) {
	static JammoCollaborationPairComposition * collaboration_pair_composition;

	collaboration_pair_composition = (JammoCollaborationPairComposition *)data;

	JammoSequencer * sequencer;
	g_object_get(collaboration_pair_composition, "sequencer", &sequencer, NULL);

	gchar * mix_location;
	g_object_get(collaboration_pair_composition, "mix-location", &mix_location, NULL);

	// TODO check state and do not change mode many times
	if (0) {
		// change sequencer to file mode
		if (jammo_sequencer_set_output_filename(sequencer, mix_location)==0) {
			printf("sequencer changed to file mode successfully\n");
		}
	}

	// playback will now create a file
	jammo_sequencer_play(sequencer);

	if (1 /* stopped */){

		exit_pair_composition(collaboration_pair_composition);
		return 0;
	}

	// still generating mix file
	return 1;
}

// function for exiting pair_composition
// this can be used to call main menu or something else
void exit_pair_composition(JammoCollaborationPairComposition * collaboration_pair_composition) {
	// unref sequencer and tracks
	clean_up(collaboration_pair_composition);
}

int jammo_collaboration_pair_composition_start(JammoCollaborationPairComposition * collaboration_pair_composition, guint32 join_group_id, guint32 group_owner_id) {

	// this is how to add callback functions to main loop
	/*g_timeout_add_full(G_PRIORITY_DEFAULT,100,(GSourceFunc)create_mix_of_pair_composition,collaboration_pair_composition,NULL);*/

	gems_components * gems_data = gems_get_data();

	if (collaboration_pair_composition->priv->host) {
		printf("***HOST\n");

		gchar* theme;
		gint variation;
		g_object_get(collaboration_pair_composition,"theme",&theme, NULL);
		g_object_get(collaboration_pair_composition,"variation",&variation, NULL);

		//printf("pair_composition: Looking THEME from pair composition (%s)\n",theme);
		//Mapping strings to int. Just alphaphetic order.
		int theme_number=0;
		if (strcmp(theme,"animal")==0)
			theme_number=1;
		else if (strcmp(theme,"city")==0)
			theme_number=2;
		else if (strcmp(theme,"fantasy")==0)
			theme_number=3;

		//printf("Putting song file theme number '%d'\n",theme_number);
		//DEBUG: just put theme_number and variation first on the song_info
		gchar * song_info = g_strdup_printf("%d%d testing song info\n",theme_number,variation);
		collaboration_set_song_info(song_info);



		//It is not yet using this at all:
		collaboration_start_pair_game(theme_number,variation,gems_data);
	}

	else {
		printf("***CLIENT\n");
		gems_group_join_to_group(join_group_id);
		collaboration_pair_composition->priv->peer_user_id = group_owner_id;
	}

	// set pair composition to gems
	gems_data->service_collaboration->collaboration_game=G_OBJECT(collaboration_pair_composition);

  return 0;
}

// unrefs sequencer and tracks which should cause deletion of those objects
static void clean_up(JammoCollaborationPairComposition * collaboration_pair_composition) {
	JammoSequencer * sequencer;
	g_object_get(collaboration_pair_composition,"sequencer",&sequencer, NULL);

	// unref sequencer. this unrefs the tracks also and should delete all objects
	g_object_unref(sequencer);
	g_object_set(G_OBJECT(collaboration_pair_composition),"sequencer",NULL, NULL);

	collaboration_clear_song_info();
}


