#include <math.h> //sin
#include <tangle.h>
#include <clutter/clutter.h>
#include <fcntl.h> //we check some files
#include <string.h>
#include <stdlib.h>

#include "../jammo-cursor.h"
#include "../../configure.h" //e.g. DATA_DIR
#include "../jammo-sample-button.h"
#include "../jammo-mentor.h"
#include "../jammo.h"

#ifdef NETWORKING_ENABLED
#include "../../gems/gems.h"
#include "../../gems/groupmanager.h"
#endif

#include "jammo-collaboration-pair-composition.h"

#include "chum.h"
#include "composition_game.h"
#include "theme.h"
#include "main_menu.h"

#include "../../cem/cem.h"

/*
This function is called when user chooses and clicks one theme in theme_selection (defined on themeselection-view.json).
Selected theme is 'name'-property of clicked button.
*/
gboolean themeselection_theme_clicked(ClutterActor* actor,ClutterEvent* event, gpointer data) {
	const gchar* name = clutter_actor_get_name(CLUTTER_ACTOR(actor));
	gfloat screen_x, screen_y,x,y;
	ClutterModifierType state;

	clutter_event_get_coords (event, &screen_x, &screen_y);
	clutter_actor_transform_stage_point(CLUTTER_ACTOR(actor), screen_x, screen_y, &x, &y);

	//Check if CTRL is pressed:
	state = clutter_event_get_state (event);

	//subthemes are 1,2,3.
	int variation;
	if (state & CLUTTER_CONTROL_MASK) { //CTRL pressed
		cem_add_to_log("Theme selected with 'CTRL-cheat' ",J_LOG_USER_ACTION);
		if (y < clutter_actor_get_height(actor)/3) //upper part of the image
			variation = 1;
		else if (y < 2 *clutter_actor_get_height(actor)/3) //middle part of the image
			variation = 2;
		else   //bottom part of the image
			variation = 3;
	}
	else {
		variation = (rand () % 3) + 1;
	}

	gchar* message = g_strdup_printf("Theme-variation is %d",variation);
	cem_add_to_log(message,J_LOG_INFO);
	g_free(message);
	
	#ifdef NETWORKING_ENABLED
	jammo_theme* theme = composition_game_start_theme(name,variation,jammo_get_actor_by_id("themeselection-view"));
	#else
	composition_game_start_theme(name,variation,jammo_get_actor_by_id("themeselection-view"));
	#endif

	//This is a previous view (themeselection-view). Hide it.
	ClutterActor* old_view;
	if ((old_view = jammo_get_actor_by_id("themeselection-view"))) {
		tangle_actor_hide_animated(TANGLE_ACTOR(old_view));
	}

	#ifdef NETWORKING_ENABLED
	//If there are pair game and we are host
	if (jammo_collaboration_pair_composition_is_host(chum_get_pair_composition())) {
		g_object_set(G_OBJECT(chum_get_pair_composition()),"jammotheme",theme, NULL);
		g_object_set(G_OBJECT(chum_get_pair_composition()),"theme",name, NULL);
		g_object_set(G_OBJECT(chum_get_pair_composition()),"variation",variation, NULL);

	//printf("theme set to pair composition (%s)\n",name);
	// pair_composition is game object, 0 parameters are ignored because they are used
	// only when joining
	jammo_collaboration_pair_composition_start(chum_get_pair_composition(), 0, 0);

	}
	#endif

	//This image shows that we are in pair-game theme-selection.
	tangle_button_set_selected(TANGLE_BUTTON(jammo_get_object_by_id("themeselection_pairgame_marker")),FALSE);

	return TRUE;
}

#ifdef NETWORKING_ENABLED
static gboolean join_selected_game(TangleButton* tanglebutton, gpointer data){
	cem_add_to_log("join_selected_game pressed",J_LOG_USER_ACTION);
	//set this to client and join game
	gems_group_info * group = (gems_group_info*)data;
	guint id = group->id;
	guint owner = group->owner;
	
	//Create game object and store it inside of chum
	chum_set_pair_composition(jammo_collaboration_pair_composition_new_host(FALSE));
	//And then start it as non-HOST.
	jammo_collaboration_pair_composition_start(chum_get_pair_composition(), id, owner);

	//Hide pairgame-menu
	ClutterActor* mainview;
	mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);

	//Keep waiting when host send info about theme

	return TRUE;
}
#endif

gboolean themeselection_going_to(TangleActor* actor, gpointer data){
cem_add_to_log("Selected composing game (starting theme-selection)",J_LOG_USER_ACTION);

#ifdef NETWORKING_ENABLED
//If advanced game level, check if there are another jammo-players
if (!chum_is_easy_game()) {
	//check if there are another jammo running (or another peerhood?)
	gboolean is_another_jammo_running = FALSE;
	
	// get list of jammos
	GList* jammo_list =gems_list_jammo_connections();
	if (g_list_length(jammo_list)>0) {
		// there are jammos
		is_another_jammo_running = TRUE;
	}
	else {
		// no jammo
		is_another_jammo_running = FALSE;
	}
	g_list_free(jammo_list);
	if (is_another_jammo_running) {
		ClutterActor* mainview;
		mainview = jammo_get_actor_by_id("main-views-widget");
		clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);


		ClutterActor* menu = jammo_get_actor_by_id("pairgamemenu-view");
		clutter_actor_show (CLUTTER_ACTOR(menu));


		//check if there are games running
		//and add them to scroll-area
		GList* group_list = gems_group_list_other_groups();
		gint number_of_pair_games=0;
		GList* list;
		GList* pair_game_list =NULL;
		for (list=group_list;list;list=list->next) {
		
			if (((gems_group_info*)(list->data))->type == GROUP_TYPE_PAIR) {
				number_of_pair_games++;
				// store pair games in list, so they can be shown to user
				pair_game_list=g_list_append(pair_game_list, ((gems_group_info*)(list->data)));
			}
		}
		if (number_of_pair_games) {
			ClutterActor* container = jammo_get_actor_by_id("pair_game_menu_game_list");
			//Clean list:
			clutter_container_foreach(CLUTTER_CONTAINER(container), CLUTTER_CALLBACK(clutter_actor_destroy), NULL);

			//Check is there need for scroller at all
			TangleLayout* layout = tangle_widget_get_layout(TANGLE_WIDGET(container));
			GList* trick_list = tangle_layout_get_tricks(layout);
			ClutterActor* trick = trick_list->data;

			//Three or less item fits without scrolling (we need they are 100px height and we have some 350px space)
			if (number_of_pair_games<4) {
				g_object_set(trick,"axis",TANGLE_SCROLL_NO_AXIS,NULL);
			}
			else {
				g_object_set(trick,"axis",TANGLE_SCROLL_Y_AXIS,NULL);
			}
			g_list_free(trick_list);


			//Each time start scroller not scrolled.
			ClutterAction* scroll_action = tangle_actor_get_action_by_type(container,TANGLE_TYPE_SCROLL_ACTION);
			g_object_set(scroll_action, "offset-y", 0, NULL);

			ClutterActor* avatar_icon;
			ClutterActor* theme_icon;
			int counter=0;
			while (counter<number_of_pair_games)  {//for-each running game.
				// show pair games
				gint16 theme = gems_group_get_theme((gems_group_info*)(g_list_nth(pair_game_list, counter)->data));
				const gchar* theme_name;
				if (theme==1)
					theme_name="animal";
				else if (theme==2)
					theme_name="city";
				else if (theme==3)
					theme_name="fantasy";
				else theme_name=""; //Error case
				//printf("theme of group is %d and it is called %s\n", theme, theme_name);


				avatar_icon=tangle_button_new_with_background_actor(tangle_texture_new("/opt/jammo/themes/city/110/beatbox.png")); //TODO: use avatar
				g_object_set(avatar_icon, "x", 100.0,  "y", 100.0 *(counter), NULL);
				guint * group_id = &(((gems_group_info*)(g_list_nth(pair_game_list, counter)->data))->id );
				gchar* message = g_strdup_printf("pair game: counter %d, pair games %d, group_id %u", counter, number_of_pair_games, *group_id);
				cem_add_to_log(message,J_LOG_NETWORK);
				g_free(message);

				// give group as parameter for callback
				g_signal_connect (avatar_icon, "clicked", G_CALLBACK (join_selected_game),(gems_group_info*)(g_list_nth(pair_game_list, counter)->data));
				tangle_widget_add(TANGLE_WIDGET(container), avatar_icon, NULL);
				tangle_actor_show(TANGLE_ACTOR(avatar_icon));


				gchar* theme_icon_name = g_strdup_printf("/opt/jammo/%s_icon.png", theme_name);
				message = g_strdup_printf("theme_icon_name: '%s'",theme_icon_name);
				cem_add_to_log(message,J_LOG_NETWORK);
				g_free(message);

				theme_icon=tangle_button_new_with_background_actor(tangle_texture_new(theme_icon_name));
				g_free(theme_icon_name);
				g_object_set(theme_icon, "x", 200.0,  "y", 100.0 *(counter), "width", 400.0, "height", 80.0, NULL);
				tangle_widget_add(TANGLE_WIDGET(container), theme_icon, NULL);
				tangle_actor_show(TANGLE_ACTOR(theme_icon));
				counter++;
			}


		}
		g_list_free(pair_game_list);
		g_list_free(group_list);

	return TRUE;
	}
}
#endif /* NETWORKING_ENABLED */

//easy game-level OR no another player, just start normal themeselection
	main_menu_theme_clicked(NULL,NULL);
	return TRUE;
}

#ifdef NETWORKING_ENABLED
gboolean themeselection_create_game(TangleActor* actor, gpointer data){
	cem_add_to_log("create game",J_LOG_NETWORK);
	main_menu_theme_clicked(NULL,NULL);

	//Now there are marker on theme_selection that pair game is selected
	tangle_button_set_selected(TANGLE_BUTTON(jammo_get_object_by_id("themeselection_pairgame_marker")),TRUE);

	//Make game-object and this player is HOST.
	//theme and variation are set when they are choosed
	 chum_set_pair_composition(jammo_collaboration_pair_composition_new_host(TRUE));

	return TRUE;
}
#endif /* NETWORKING_ENABLED */

gboolean themeselection_cross_clicked(TangleActor* actor, gpointer data){
	tangle_button_set_selected(TANGLE_BUTTON(jammo_get_object_by_id("themeselection_pairgame_marker")),FALSE);

	clutter_actor_hide(jammo_get_actor_by_id("themeselection-view"));
	clutter_actor_show(jammo_get_actor_by_id("main-menu-view"));

	return TRUE;
}
