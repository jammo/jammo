/** main_menu.c is part of JamMo.
License: GPLv2, read more from COPYING

*/

#include <tangle.h>
#include "../jammo.h"
#include "../../cem/cem.h"

static void hide_unless_mainmenu(TangleActor* actor, gpointer data) {
	if (CLUTTER_ACTOR(actor) == jammo_get_actor_by_id("main-menu-view")) {
		clutter_actor_show(jammo_get_actor_by_id("main-menu-view"));
		return;
		}
	tangle_actor_hide_animated(actor);
}


gboolean main_menu_go_back_to_main_menu(TangleActor* actor, gpointer data){
	(jammo_get_actor_by_id("main-menu-view"));

	ClutterActor* mainview;
	mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(hide_unless_mainmenu), NULL);

	return TRUE;
}

gboolean main_menu_easy_selection_clicked(TangleActor* actor, gpointer data){
	clutter_actor_show(jammo_get_actor_by_id("easy-selection-view"));
	clutter_actor_hide(jammo_get_actor_by_id("main-menu-view"));
	return TRUE;
}

gboolean main_menu_singing_clicked(TangleActor* actor, gpointer data){
	clutter_actor_show(jammo_get_actor_by_id("songselection-view"));
	clutter_actor_hide(jammo_get_actor_by_id("main-menu-view"));
	return TRUE;
}

gboolean main_menu_theme_clicked(TangleActor* actor, gpointer data){
	clutter_actor_show(jammo_get_actor_by_id("themeselection-view"));
	clutter_actor_hide(jammo_get_actor_by_id("main-menu-view"));

	ClutterActor* pairgamemenu;
	pairgamemenu=jammo_get_actor_by_id("pairgamemenu-view");
	if (pairgamemenu)
		clutter_actor_hide(pairgamemenu);

	return TRUE;
}

gboolean main_menu_cupboard_clicked(TangleActor* actor, gpointer data){
	clutter_actor_show(jammo_get_actor_by_id("cupboard-view"));
	clutter_actor_hide(jammo_get_actor_by_id("main-menu-view"));
	return TRUE;
}

gboolean main_menu_quit_clicked(TangleActor* actor, gpointer data){
	clutter_actor_show(jammo_get_actor_by_id("quitconfirmation-view"));
	//clutter_actor_hide(jammo_get_actor_by_id("main-menu-view"));
	return TRUE;
}

void main_menu_show_completed(TangleActor* actor, gpointer data){
	cem_add_to_log("startmenu show completed",J_LOG_INFO);
	TangleProperties* properties;
	properties = TANGLE_PROPERTIES(jammo_get_object_by_id("global_game_properties"));

	TangleButton* tanglebutton = TANGLE_BUTTON(jammo_get_actor_by_id("main-menu-skill-level-button"));
	tangle_button_set_selected(tanglebutton,!tangle_properties_get_boolean(properties, "easy_game_level"));
}
