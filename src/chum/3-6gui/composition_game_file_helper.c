#include <tangle.h>
#include <clutter/clutter.h>
#include <string.h>

#include "../../configure.h" //e.g. DATA_DIR
#include "../jammo-sample-button.h"
#include "../jammo-editing-track-view.h"
#include "../../cem/cem.h"

#include "theme.h"
#include "../file_helper.h"

static void load_this_sample_array_to_this_track_view_legacy1(JsonNode *sub_node,JammoEditingTrackView* track_view,jammo_theme* theme, const gchar* theme_name, int variation);

gboolean composition_game_file_helper_load_from_file(jammo_theme* theme, gpointer data,JammoEditingTrackView* bottom_track_view ,JammoEditingTrackView* upper_track_view) {

gchar* filename = (gchar*) data;
gchar* message = g_strdup_printf("Loading composition from file '%s'",filename);
cem_add_to_log(message,J_LOG_DEBUG);
g_free(message);

JsonParser *parser;
parser = json_parser_new ();
g_assert (JSON_IS_PARSER (parser));

GError *error = NULL;
if (!json_parser_load_from_file (parser, filename, &error))
	{
		cem_add_to_log(error->message,J_LOG_ERROR);
		//g_print ("Error: %s\n", error->message);
		g_error_free (error);
		g_object_unref (parser);
		return FALSE;
	}

JsonNode *root;
JsonObject *object;
JsonNode *node;

g_assert (NULL != json_parser_get_root (parser));

root = json_parser_get_root (parser);
g_assert_cmpint (JSON_NODE_TYPE (root), ==, JSON_NODE_OBJECT);

object = json_node_get_object (root);
g_assert (object != NULL);


const gchar* theme_name;
gint variation =-1;
gint format_version = 1; //jammo-0.7.7 and all before that is format_version=1
	//string
	/* //No need to read icon on this phase.
	node = json_object_get_member (object, "icon");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE){
		gchar* icon_name =(gchar*)  json_node_get_string (node);
		//printf("icon: '%s'\n",icon_name);
	}
	*/

	//string
	node = json_object_get_member (object, "theme");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE){
		theme_name = json_node_get_string (node);
		//printf("theme: '%s'\n",theme_name);
	}
	else theme_name="";

	//int
	node = json_object_get_member (object, "variation");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE){
		variation = json_node_get_int (node);
		//printf("variation: '%d'\n",variation);
	}

	//int
	node = json_object_get_member (object, "format-version");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE){
		format_version = json_node_get_int (node);
	}
	message = g_strdup_printf("using format_version: %d",format_version);
	cem_add_to_log(message,J_LOG_DEBUG);
	g_free(message);

	gint track_counter =1;
	//Array
	node = json_object_get_member (object, "tracks");
	JsonArray* track_array;
	guint length_tracks = 0;
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_ARRAY){
		track_array =  json_node_get_array (node);
		length_tracks = json_array_get_length(track_array);
	}
	else {
		cem_add_to_log("Not any tracks for loading",J_LOG_WARN);
	}

	//printf("length %d\n",length_tracks);
	int j;
	for (j=0;j<length_tracks;j++) {

		//Object
		JsonNode* track_node;
		track_node = json_array_get_element(track_array,j);


		if (track_node!=NULL && JSON_NODE_TYPE(track_node) == JSON_NODE_OBJECT){
			//printf("Found another track\n");
			JammoEditingTrackView* track_view = NULL;
			if (track_counter == 1)
				track_view = bottom_track_view;
			else if (track_counter == 2)
				track_view = upper_track_view;
			else {
				cem_add_to_log("problems with loading from file",J_LOG_ERROR);
			}
			JsonObject* sub_object = json_node_get_object(track_node);
			JsonNode *sub_node;
			sub_node = json_object_get_member (sub_object, "track-type");
			/* //Track-type is not used in this phase
			if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
				gchar* track_type =(gchar*)  json_node_get_string (sub_node);
				//printf("track-type: '%s'\n",track_type);
				}
			*/

			sub_node = json_object_get_member (sub_object, "samples");
			if (format_version==1) //Legacy1
				load_this_sample_array_to_this_track_view_legacy1(sub_node,track_view,theme,theme_name,variation); //static
			else if (format_version==2) //Legacy2
				load_this_sample_array_to_this_track_view_legacy2(sub_node,track_view,FALSE); //FALSE=non-reactive
			else
				load_this_sample_array_to_this_track_view(sub_node,track_view,FALSE); //FALSE=non-reactive


		} //Track is ready
		track_counter++;
	} //Next track

g_object_unref(parser);

return TRUE;
}


/*
 * This is used loading format-version=1 compositions.
 */
static void load_this_sample_array_to_this_track_view_legacy1(JsonNode *sub_node,JammoEditingTrackView* track_view,jammo_theme* theme, const gchar* theme_name, int variation) {
	//Array  //sub_node = json_object_get_member (sub_object, "samples");
	if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_ARRAY){
		JsonArray* sample_array =  json_node_get_array (sub_node);

		guint length = json_array_get_length(sample_array);
		//printf("length %d\n",length);
		int i;
		for (i=0;i<length;i++) {
			JsonNode* sample_node;
			sample_node = json_array_get_element(sample_array,i);
			if (sample_node!=NULL && JSON_NODE_TYPE(sample_node) == JSON_NODE_OBJECT){
				JsonObject* sample_object = json_node_get_object(sample_node);
				gint loop_id=-1;
				gint slot=-1;

				//int
				sub_node = json_object_get_member (sample_object, "loop_id");
				if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
					loop_id =  json_node_get_int (sub_node);
					//printf("loop_id: '%d'\n",loop_id);
				}

				//int
				sub_node = json_object_get_member (sample_object, "slot");
				if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
					slot =  json_node_get_int (sub_node);
					//printf("slot: '%d'\n",slot);
				}
				if (track_view) {
					ClutterActor* sample_button=NULL;
						sample_button = theme_give_sample_button_for_this_theme_and_variation_for_this_id(theme, theme_name,variation,loop_id);
					if (sample_button) {
						jammo_editing_track_view_add_jammo_sample_button(track_view,JAMMO_SAMPLE_BUTTON(sample_button),slot);
						clutter_actor_set_reactive(sample_button,FALSE);
					} //sample_button!=NULL check
				} //track!=NULL check

			} //This sample-object is ready

		} //Foreach in sample-array ready

	} //sample-array is ready

}
