#ifndef __THEME_H_
#define __THEME_H_

#include <tangle.h>
#include "../../meam/jammo-sequencer.h"
#include "../jammo-editing-track-view.h"
typedef struct _jammo_theme{
	JammoSequencer* sequencer; /*currently used sequencer*/
	ClutterActor* container;  /* All samples and tracks are in here*/
	ClutterActor* save_menu_container;  /* All samples and tracks are in here*/
	JammoEditingTrackView* bottom_track_view;
	JammoEditingTrackView* upper_track_view; //this is not used in 'easy' mode
	ClutterActor* color_bar; //color_bar under track-views
	GList* animation_timelines; /*This is container for animation timelines of samplebuttons*/
	ClutterActor* play_button; /* This is needed when sequencer stops we want change between stop and play images*/
	gboolean only_playing_mode; //When editing this is FALSE. When coming from cupboard, only listen is allowed
	const gchar *name;
	gint variation;
	GPtrArray* sample_button_array;
} jammo_theme;


jammo_theme* theme_create(const gchar* name, int variation, gboolean only_listening, ClutterActor* play_button);
void theme_stop_all_icon_animations(jammo_theme* theme);
void theme_play_all_icon_animations(jammo_theme* theme);
void delete_theme(jammo_theme* theme);

ClutterActor* theme_give_sample_button_for_this_theme_and_variation_for_this_id(jammo_theme* theme, const gchar* themename, guint variation, guint id);

#endif /* __THEME_H_ */
