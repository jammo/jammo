/*
 * jammo-collaboration-pair-composition.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther <mikko.gynther@lut.fi>
 */
 
#ifndef __JAMMO_COLLABORATION_PAIR_COMPOSITION_H__
#define __JAMMO_COLLABORATION_PAIR_COMPOSITION_H__

#include <glib.h>
#include <glib-object.h>
#include "../jammo-collaboration-game.h"

#define JAMMO_TYPE_COLLABORATION_PAIR_COMPOSITION (jammo_collaboration_pair_composition_get_type ())
#define JAMMO_COLLABORATION_PAIR_COMPOSITION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_COLLABORATION_PAIR_COMPOSITION, JammoCollaborationPairComposition))
#define JAMMO_IS_COLLABORATION_PAIR_COMPOSITION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_COLLABORATION_PAIR_COMPOSITION))
#define JAMMO_COLLABORATION_PAIR_COMPOSITION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_COLLABORATION_PAIR_COMPOSITION, JammoCollaborationPairCompositionClass))
#define JAMMO_IS_COLLABORATION_PAIR_COMPOSITION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_COLLABORATION_PAIR_COMPOSITION))
#define JAMMO_COLLABORATION_PAIR_COMPOSITION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_COLLABORATION_PAIR_COMPOSITION, JammoCollaborationPairCompositionClass))

typedef struct _JammoCollaborationPairCompositionPrivate JammoCollaborationPairCompositionPrivate;

typedef struct _JammoCollaborationPairComposition {
	JammoCollaborationGame parent_instance;
	JammoCollaborationPairCompositionPrivate* priv;
} JammoCollaborationPairComposition;

typedef struct _JammoCollaborationPairCompositionClass {
	JammoCollaborationGameClass parent_class;
} JammoCollaborationPairCompositionClass;

GType jammo_collaboration_pair_composition_get_type(void);

JammoCollaborationPairComposition* jammo_collaboration_pair_composition_new();

JammoCollaborationPairComposition* jammo_collaboration_pair_composition_new_host(gboolean host);
gboolean jammo_collaboration_pair_composition_is_host(JammoCollaborationPairComposition* collaboration_pair_composition);

int jammo_collaboration_pair_composition_start(JammoCollaborationPairComposition * collaboration_pair_composition, guint32 join_group_id, guint32 group_owner_id);

int jammo_collaboration_pair_composition_peer_id_callback(JammoCollaborationPairComposition * collaboration_pair_composition);

void jammo_collaboration_pair_composition_loop_sync (JammoCollaborationPairComposition * group_composition);

#endif
