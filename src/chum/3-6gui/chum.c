/** chum.c is part of JamMo.
License: GPLv2, read more from COPYING

(from D2.4)
"CHUM controls the user interface and will communicate with all other modules. 
It will use MEAM to create music. It will use GEMS to identify and authorize
the user and to send data to other users and teacher's server. It also sends
all the user actions to CEM fo logging/mentor input/game input."
*/

#include <tangle.h>
#include <fcntl.h> //we check some files
#include <string.h> //we use strcmp
#include <unistd.h>

#include "song.h"

#include "chum.h"


#include "../../cem/cem.h"
#include "../../meam/jammo-meam.h"
#include "../../meam/jammo-sample.h"
#include "../../meam/jammo-editing-track.h"
#include "../../meam/jammo-recording-track.h" 
#include "../../meam/jammo-instrument-track.h"
#include "../../meam/jammo-slider-track.h"
#include "../../meam/jammo-playing-track.h"

#ifdef NETWORKING_ENABLED
#include "../../gems/gems.h"
#endif

#include "../jammo-mentor.h"
#include "../jammo.h"

#include "singinggame.h"
#include "jammo-collaboration-pair-composition.h"

#include "main_menu.h"

static jammo_song* current_song;  /*for karaoke game*/

#include "singinggame.h"
gboolean start_3_6_game(gpointer data){
	//printf("start loading songs\n");
	//Load songs
	singinggame_force_reinit_songs();
	//printf("loading songs done\n");
	#ifdef NETWORKING_ENABLED
	gems_set_callback_game_starter(chum_remote_game_starter);  
	#endif
return FALSE;
}


static JammoCollaborationPairComposition* static_pair_composition = NULL;

JammoCollaborationPairComposition* chum_get_pair_composition() {
	return static_pair_composition;
}

void chum_set_pair_composition(JammoCollaborationPairComposition* pc) {
	static_pair_composition=pc;
}

//This is not same than language from profile (which affects speech of mentor)
//This is only when user wants sing a song in different language.
static char* selected_language=NULL;

const char* chum_get_selected_language(){
	return selected_language;
}

void  chum_set_selected_language(const gchar* language){
	if (selected_language)
		g_free(selected_language);
	selected_language = g_strdup(language);
}


static gboolean duetto=TRUE;
void chum_set_duetto(gboolean b){
	duetto=b;
}

gboolean chum_get_duetto(){
	return duetto;
}


//FALSE means advanced_game
gboolean chum_is_easy_game(){
	TangleProperties* properties;
	properties = TANGLE_PROPERTIES(jammo_get_object_by_id("global_game_properties"));
	return tangle_properties_get_boolean(properties, "easy_game_level");
}

void chum_set_easy_game(gboolean b){
	TangleProperties* properties;
	properties = TANGLE_PROPERTIES(jammo_get_object_by_id("global_game_properties"));
	tangle_properties_set_boolean(properties, "easy_game_level",b);

	gchar* message = g_strdup_printf("Game level set to %s", b ? "easy":"advanced");
	cem_add_to_log(message,J_LOG_DEBUG);
	g_free(message);
}


jammo_song* chum_get_current_song(){
	return current_song;
}

void chum_set_current_song(jammo_song* song){
	current_song = song;
}


//for communication interface
JammoSample* chum_add_new_sample_to_track_remote(int a, const gchar* id, guint64 startTime){
return NULL;
}

void chum_remove_sample_from_slot_remote(int a, int slot){
return;
}



//FIXME: this is copy-pasted from singing-game (solo/duetto and language-selection uses same kind of mechanism)
static gulong handler_for_mentor_say_selected;

static void mentor_say_what_is_selected(TangleActor *actor, ClutterActorBox *arg1, gpointer user_data){
	//printf("got '%s'\n",(gchar*)user_data);
	gchar *filepath1;
	filepath1 =  g_strdup_printf("%s_selected.spx", (gchar*)user_data);

	jammo_mentor_speak(jammo_mentor_get_default(), filepath1);
	g_free(filepath1);
	g_signal_handler_disconnect (actor,handler_for_mentor_say_selected);
}


static gboolean reload_songs(gpointer data) {
	singinggame_force_reinit_songs();
	main_menu_go_back_to_main_menu(NULL,NULL);
return FALSE;
}


gboolean jammo_easy_changed(TangleActor* actor, gpointer data){
	ClutterActor* ct =CLUTTER_ACTOR(actor);
	const gchar* name = clutter_actor_get_name(ct);
	gchar* message = g_strdup_printf("%s clicked", name);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);

	ClutterActor* view = jammo_get_actor_by_id("main-menu-view");

		if (strcmp(name,"cancel")==0) {
			main_menu_go_back_to_main_menu(NULL,NULL);
			return TRUE; //Do not do any loading
		}

	if (strcmp(name,"easy")==0)
		{
		if (chum_is_easy_game()){
			main_menu_go_back_to_main_menu(NULL,NULL);
			return TRUE; //Do not do any loading
		}
		chum_set_easy_game(TRUE);
		handler_for_mentor_say_selected=g_signal_connect(view, "show-completed", G_CALLBACK(mentor_say_what_is_selected), (gchar*)"skill_easy");
		}
	else if (strcmp(name,"advanced")==0)
		{
		if (!chum_is_easy_game()){
			main_menu_go_back_to_main_menu(NULL,NULL);
			return TRUE; //Do not do any loading
		}
		chum_set_easy_game(FALSE);
		handler_for_mentor_say_selected=g_signal_connect(view, "show-completed", G_CALLBACK(mentor_say_what_is_selected), (gchar*)"skill_advanced");
		}

	jammo_mentor_set_idle_speech(jammo_mentor_get_default(), ""); //If it takes too long to load
	ClutterActor* hourglass = jammo_get_actor_by_id("wait");
	clutter_actor_show(hourglass);
	g_timeout_add(10, reload_songs,NULL); //Clutter needs some time to show actor

	return TRUE;
}
//Teacher forced a new game
void chum_remote_game_starter(gpointer data, gint16 track_id)
{
	#ifdef NETWORKING_ENABLED
	cem_add_to_log("Creating pair game at CHUM",J_LOG_NETWORK_DEBUG);
	gems_group_info * group = (gems_group_info*)data;
	guint id = group->id;
	guint owner = group->owner;

	chum_set_easy_game(FALSE);//Force advanced game
	chum_set_pair_composition(jammo_collaboration_pair_composition_new_host(FALSE));
	//And then start it as non-HOST.
	jammo_collaboration_pair_composition_start(chum_get_pair_composition(), id, owner);

	//Hide pairgame-menu
	ClutterActor* mainview;
	mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);
	#else
	cem_add_to_log(" No network-support: trying to create pair game at CHUM",J_LOG_DEBUG);
	#endif
}




