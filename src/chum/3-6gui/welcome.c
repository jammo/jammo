/*
 * welcome.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */
#include <stdlib.h>
#include <tangle.h>
#include <clutter/clutter.h>

#include "../../configure.h"
#include "../jammo.h"
#include "../../meam/jammo-sample.h"
#include "chum.h"

#ifdef NETWORKING_ENABLED
#include "../../cem/cem.h"
#include "../../gems/gems_utils.h"
#include "../../gems/gems.h"
#endif


static void finalize_welcome(ClutterTimeline* timeline, gpointer none)  {
	ClutterActor* actor;

	if ((actor = jammo_get_actor_by_id("welcome-view"))) {
		tangle_actor_hide_animated(TANGLE_ACTOR(actor));
	}

	#ifdef NETWORKING_ENABLED
	// There are no GUI-login. Use default profile - a temporary solution
	// Check version 1.0.0 from git for one idea of GUI-login
	if(gems_profile_manager_create_default_user_profile("12345678",6))
	{
		gchar* logmsg = NULL;
		if(gems_profile_manager_authenticate_default_user("12345678") == LOGIN_OK)
			logmsg = g_strdup_printf("login_screen(): Created default user profile and logged in as default user.");
		// Should not happen
		else
			logmsg = g_strdup_printf("login_screen(): Cannot login to created default user profile.");

		cem_add_to_log(logmsg,J_LOG_DEBUG);
		g_free(logmsg);
	}
	#endif  /* NETWORKING_ENABLED */

	//login_screen();
	if ((actor = jammo_get_actor_by_id("main-menu-view"))) {
		clutter_actor_show(actor);
		clutter_actor_queue_relayout(clutter_actor_get_parent(actor));
	}

	//Set default language for singing game
	// It also affects first flag showed
	gchar* lang = getenv ("LANG");
	//printf("LANG='%s'\n",lang);

	gchar* lang2 = g_strndup (lang,2); //two characters are enough
	gchar* lang3 = g_strdup_printf("_%s",lang2);
	chum_set_selected_language(lang3);
	g_free(lang2);
	g_free(lang3);
}


/*
If user doesn't click door, there are timeout. Timeout calls welcome_door_clicked, but tangle_actor==NULL.
All this hazzling is needed because user can click (or double click) very same time than timeout triggers.
*/
static gboolean door_is_opening = FALSE; //Or opened already.
void welcome_door_clicked(TangleActor* tangle_actor, gpointer data){
	if (door_is_opening && tangle_actor == NULL)
		return;

	door_is_opening = TRUE;
	ClutterActor* actor;
	ClutterAnimation* animation;
	ClutterTimeline* timeline;
	gchar* filename;
	JammoSample* sample;

	actor = jammo_get_actor_by_id("welcome-door");
	g_assert(actor != NULL);
	
	if ((animation = clutter_actor_get_animation(actor))) {
		clutter_animation_completed(animation);
		jammo_sample_stop_all();
		
		finalize_welcome(NULL,NULL);
	} else {
		animation = clutter_actor_animate(actor, CLUTTER_LINEAR, 2750, "rotation-angle-y", -110.0, NULL);

		timeline = clutter_animation_get_timeline(animation);
		clutter_timeline_add_marker_at_time(timeline, "show-main-menu-view", 1750);
		g_signal_connect(timeline, "marker-reached::show-main-menu-view", G_CALLBACK(finalize_welcome), NULL);
		
		filename = g_strdup_printf("%s/door_opening.wav",DATA_DIR);
		sample = jammo_sample_new_from_file(filename);
		g_free(filename);
		jammo_sample_play(sample);
		g_object_unref(sample);
	}

	//Show version number: defined in configure.ac

	ClutterActor* version_label = jammo_get_actor_by_id("welcome-version-info");
	if (version_label) {
		gchar* version = g_strdup_printf("JamMo version:%s",VERSION);
		clutter_text_set_text (CLUTTER_TEXT(version_label),version);
		g_free(version);
		clutter_text_set_font_name (CLUTTER_TEXT(version_label),"Luxi Mono 38");
		ClutterColor text_color = { 255, 255, 0, 255 };
		clutter_text_set_color(CLUTTER_TEXT(version_label), &text_color);
		clutter_actor_raise_top(version_label);
		clutter_actor_show(version_label);
	}

}
