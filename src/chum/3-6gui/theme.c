#include <tangle.h>
#include <clutter/clutter.h>
#include <fcntl.h> //we check some files
#include <math.h> //sin
#include <string.h>
#include <stdlib.h>

#include "../../configure.h" //e.g. DATA_DIR
#include "../jammo-editing-track-view.h"
#include "../jammo-cursor.h"
#include "../jammo-mentor.h"
#include "../jammo.h"
#include "../file_helper.h"

#include "chum.h" //chum_is_easy_game
#include "theme.h"
#include "composition_game.h"
#include "composition_game_file_helper.h"
#include "../../meam/jammo-backing-track.h"

#include "../../cem/cem.h"
static int x_place_for_icon = 0; //Used when making icons for save-menu

//slot=beat. bar=4*beat
#define WIDTH_OF_BEAT 10.0


static gdouble phasing(gdouble start_value, int phase) {
	gdouble value = start_value;
	value += phase/100.0;
	if (value>=1) value -= 1;
	return value;
}
/*
Draw cupboard with two doors. It is placed far right.
Right-door will open little.
Clicked button will move inside cupboard. (over left door, but under right).
*/
static void animate_icon_to_cupboard(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer button){
	if (!button)
		return;
	int x_for_cupboard = 510;
	//Cupboard itself
	ClutterActor* cupboard = tangle_texture_new("/opt/jammo/cupboard.png");
	clutter_actor_set_x(cupboard,x_for_cupboard);
	clutter_actor_set_y(cupboard,0);
	TangleWidget* view=TANGLE_WIDGET(clutter_actor_get_parent(CLUTTER_ACTOR(button)));
	if (!view)
		return;
	tangle_widget_add(view, cupboard, NULL);

	//Left door. this will be closed
	ClutterActor* cupboard_left = tangle_texture_new("/opt/jammo/cupboard_leftdoor.png");
	clutter_actor_set_x(cupboard_left,x_for_cupboard);
	clutter_actor_set_y(cupboard_left,60);
	tangle_widget_add(TANGLE_WIDGET(clutter_actor_get_parent(CLUTTER_ACTOR(button))), cupboard_left, NULL);

	//Raise clicked button (also over all other buttons)
	clutter_actor_raise_top(CLUTTER_ACTOR(button));

	//Right door. This will be animated. This will be topmost actor
	ClutterActor* cupboard_right = tangle_texture_new("/opt/jammo/cupboard_rightdoor.png");
	clutter_actor_set_anchor_point(cupboard_right, clutter_actor_get_width(cupboard_right), 0.0);  //anchor is on right side
	clutter_actor_set_x(cupboard_right,x_for_cupboard+clutter_actor_get_width(cupboard_left)+clutter_actor_get_width(cupboard_right));
	clutter_actor_set_y(cupboard_right,60);
	tangle_widget_add(TANGLE_WIDGET(clutter_actor_get_parent(CLUTTER_ACTOR(button))), cupboard_right, NULL);
	clutter_actor_animate(cupboard_right, CLUTTER_LINEAR, 3000, "rotation-angle-y", 10.0,  NULL);

	//Animate (=move) clicked button inside cupboard
	ClutterAnimation* animation;
	ClutterTimeline* timeline;
	animation = clutter_actor_animate(button, CLUTTER_LINEAR, 4000, "x", 700.0, NULL);
	timeline = clutter_animation_get_timeline(animation);
	g_signal_connect(timeline, "completed", G_CALLBACK(composition_game_close), NULL);
}


static gboolean save_image_pressed(TangleButton *tanglebutton, gpointer data){
	jammo_theme* theme = (jammo_theme*)data;
	clutter_actor_hide(jammo_get_actor_by_id("saving_dialog_cancel")); //No canceling after image is selected.

	const gchar* name_of_icon = clutter_actor_get_name(CLUTTER_ACTOR(tanglebutton));
	gchar* message = g_strdup_printf("Selected '%s' for icon",name_of_icon);
	cem_add_to_log(message,J_LOG_DEBUG);
	g_free(message);

	gchar* filename_for_saving = g_strdup_printf("+%s+%d+%s-",theme->name,theme->variation,chum_is_easy_game()?"e":"a");
	//e.g.: +fantasy+2+e-

	gchar* additional_metadata = g_strdup_printf("\"icon\" : \"%s\",\n\
\"theme\" : \"%s\",\n\
\"variation\" : %d,\n"\
  ,name_of_icon,theme->name,theme->variation);

	save_composition(configure_get_compositions_directory(),filename_for_saving, clutter_actor_get_parent(CLUTTER_ACTOR(theme->bottom_track_view)) ,additional_metadata);

	g_free(filename_for_saving);
	g_free(additional_metadata);
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(),"save_confirmed.spx",animate_icon_to_cupboard,tanglebutton);
	return FALSE;
}


/*
clutter_timeline_get_progress returns value between [0,1]
with phasing we can shift it always same amount
*/
static gdouble sine_wave (ClutterAlpha *alpha,gpointer user_data) {
return sin (phasing(clutter_timeline_get_progress (clutter_alpha_get_timeline (alpha)),GPOINTER_TO_INT(user_data)) * G_PI);
}


static int tempo_table[] = {0,140,110,90}; //variation1 is 140bpm, variation2 is 110bpm, variation3 is 90pbm

//On advanced game, variation is not used, they are all using same set of sample_buttons
ClutterActor* theme_give_sample_button_for_this_theme_and_variation_for_this_id(jammo_theme* theme, const gchar* themename, guint variation, guint id){
	if (theme->sample_button_array == NULL)
		return NULL;
	JammoSampleButton* sb = JAMMO_SAMPLE_BUTTON(g_ptr_array_index(theme->sample_button_array, id)); //clone this!
	//It is unique, we can't return it.  We make clone of it.

	ClutterActor* sample_button;
	sample_button = jammo_sample_button_new_from_files(jammo_sample_button_get_image_filename(sb),jammo_sample_button_get_sample_filename(sb));
	jammo_sample_button_set_loop_id(JAMMO_SAMPLE_BUTTON(sample_button),jammo_sample_button_get_loop_id(sb));
	return sample_button;
}

/* Actor for parameter is soundfamily-container.*/
static void pause_actors_animation(ClutterActor *actor, gpointer user_data) {
	//printf("pause\n");
	TangleActorIterator actor_iterator;
	ClutterActor* child;
	
	for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(actor), &actor_iterator); (child = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
		ClutterAnimation* animation = clutter_actor_get_animation(child);
		if (animation) {
			ClutterTimeline* timeline = clutter_animation_get_timeline(animation);
			clutter_timeline_pause(CLUTTER_TIMELINE(timeline));
		}
	}
}

/* Actor for parameter is soundfamily-container.
*/
static void unpause_actors_animation(ClutterActor *actor, gpointer sequencer) {
	TangleActorIterator actor_iterator;
	ClutterActor* child;

	//printf("unpause\n");
	if (jammo_sequencer_get_position(JAMMO_SEQUENCER(sequencer))) //position==0 => 'not playing'-state
		return; //If sequencer is playing, we do not want start any animation
	
	for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(actor), &actor_iterator); (child = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
		ClutterAnimation* animation = clutter_actor_get_animation(child);
		if (animation) {
			ClutterTimeline* timeline = clutter_animation_get_timeline(animation);
			clutter_timeline_start(CLUTTER_TIMELINE(timeline));
		}
	}
}


static gboolean sample_button_listened(JammoSampleButton* sample_button, gpointer data){
	gchar* name;
	g_object_get(sample_button,"sample-filename", &name,NULL);
	gchar *message = g_strdup_printf("Sample '%s' listened on stage",name);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);
	return TRUE;
}

static int static_counter_for_id = 0;
/*
This function creates new jammo_sample_button from given parameters and put it to given parent ClutterActor.
*/
static void create_samplebutton(const gchar* themename, const gchar* variation, const gchar* imagefile, const gchar* soundfile, gfloat x, gfloat y,jammo_theme* theme,TangleWidget* container_for_buttons,TangleWidget* parent){
	gchar *full_image = g_strdup_printf("themes/%s/%s/%s",themename,variation,imagefile);
	//printf("image file: '%s'\n",full_image);

	gchar *full_sound = g_strdup_printf("%s/%s/%s/%s",THEMES_DIR,themename,variation,soundfile);
	//printf("sound file: '%s'\n",full_sound);

	ClutterActor* sample_button;
	//sample_button = jammo_sample_button_new_from_files(full_image, full_sound);
	sample_button = jammo_sample_button_new();
	jammo_sample_button_set_image_filename(JAMMO_SAMPLE_BUTTON(sample_button),full_image);
	jammo_sample_button_set_sample_filename(JAMMO_SAMPLE_BUTTON(sample_button),full_sound);

	g_signal_connect (sample_button, "listened", G_CALLBACK (sample_button_listened),NULL);
	jammo_sample_button_set_loop_id(JAMMO_SAMPLE_BUTTON(sample_button), theme->sample_button_array->len);
	g_ptr_array_add(theme->sample_button_array, sample_button);
	static_counter_for_id++;

	/* If we are in listening mode, we do not need:
	*pushable
	*draggable
	*animated
	   sample_buttons
	*/

	if (theme->only_playing_mode) {
		clutter_actor_set_reactive(sample_button,FALSE);
		}

	else {
	//This is good place to do menu for selecting icon for saved composition
	if (x_place_for_icon<500) { //if 500px is used do not add anymore icons
		gchar* filename_for_icon_texture;
		filename_for_icon_texture = tangle_lookup_filename(full_image);

		ClutterActor* icon_button = tangle_button_new_with_background_actor(tangle_texture_new(filename_for_icon_texture));
		g_free(filename_for_icon_texture);

		gchar* name_for_callback = g_strdup_printf("%s",full_image);
		g_signal_connect (icon_button, "clicked", G_CALLBACK (save_image_pressed),theme);
		clutter_actor_set_name(icon_button, name_for_callback);
		g_free(name_for_callback);
		clutter_actor_set_x(icon_button,x_place_for_icon);
		clutter_actor_set_y(icon_button,200);
		clutter_actor_set_width(icon_button,80); //This is the size/ratio when it is in cupboard
		tangle_widget_add(TANGLE_WIDGET(container_for_buttons), icon_button, NULL);
		x_place_for_icon+=clutter_actor_get_width(icon_button)+5;
	}




	//Each animation has own phase
	int phase = (rand () % 100) + 1;
	//printf("phase %d\n",phase);

	//Each animation has own wobbling 'mode'-function
	gulong sine_function;
	sine_function = clutter_alpha_register_func (sine_wave, GINT_TO_POINTER(phase));

	ClutterTimeline *timeline1 = tangle_timeline_new(3000+phase*20);  //each has own rhythm (use same random number than phase)

	theme->animation_timelines = g_list_append(theme->animation_timelines,timeline1);
	ClutterAnimation* animation;
	animation = clutter_actor_animate_with_timeline (sample_button,
            sine_function,
            timeline1,
            "height", 50.0,  //add some vertical scaling, it doesn't disturb movement
            NULL);
	clutter_animation_set_loop (animation,TRUE);
	g_object_unref(timeline1);
	}

	g_free(full_image);
	g_free(full_sound);
	clutter_actor_set_position(sample_button, x, y);
	tangle_widget_add(parent, sample_button, NULL);
}


//soundfamily is button containing samplebuttons.
static ClutterActor* soundfamily[4]; //This can be GList, but now we know that we use always four.

/*
actor is button, not interested.
event is not interested.
user_data is container
*/
static void menu_clicked (TangleButton *tanglebutton, gpointer user_data){
	//check if this menu is clicked already (is it visible)

	//printf("family pressed\n");
	if (CLUTTER_ACTOR_IS_VISIBLE(CLUTTER_ACTOR(user_data))) {
		//printf("hiding\n");
		tangle_actor_hide_animated(TANGLE_ACTOR(user_data));
	}
	else {
		int i;
		for (i=0;i<4;i++)
			{
			tangle_actor_hide_animated(TANGLE_ACTOR(soundfamily[i]));
			}
		//printf("showing\n");
		tangle_actor_show(TANGLE_ACTOR(user_data));
		//clutter_actor_show(CLUTTER_ACTOR(user_data));
	}
}

static ClutterActor* create_soundfamily_button(jammo_theme* theme, const gchar* button_name, gfloat x, gfloat y,TangleWidget* parent){
	ClutterActor* widget = tangle_widget_new();
	tangle_widget_add(parent, widget,NULL);
	clutter_actor_hide(widget);

	//For pausing and unpausing hidden animations
	g_signal_connect(widget, "hide", G_CALLBACK(pause_actors_animation), NULL);

	//Do not ever start animations on pair-game:
	if (!chum_get_pair_composition()) {
		g_signal_connect(widget, "show", G_CALLBACK(unpause_actors_animation), theme->sequencer);
	}

	gchar* filename = g_strdup_printf("%s/%s/advanced/%s", THEMES_DIR, theme->name,button_name);
	ClutterActor *menu_button =  tangle_button_new_with_background_actor(tangle_texture_new(filename));
	g_free(filename);

	g_object_set(menu_button, "x",x, "y", y, NULL);
	tangle_widget_add(parent, menu_button,NULL);
	g_signal_connect (menu_button, "clicked", G_CALLBACK (menu_clicked),widget);

	return widget;
}







/*
Make colored rectangles and add them to given parent.
Returns number of used bars (length of color_coding).
white='#', because it has special meaning. Samples can't be dropped on this bar.
*/
static ClutterActor* create_backing_track_color_bar(const gchar* color_coding, gfloat bar_size, gfloat y_offset) {
	ClutterActor* actor;
	ClutterColor white  = { 255,255,255,   255 };
	ClutterColor yellow = { 255, 255, 0,   255 };
	ClutterColor green  = { 0, 255, 0,     255 };
	ClutterColor orange = { 255, 165, 0,   255 };
	ClutterColor blue   = { 25, 25, 112,   255 };
	ClutterColor red    = { 255, 0, 0,     255 };
	ClutterColor lblue  = { 100, 149, 237, 255 };
	ClutterColor violet = { 148,0,211,     255 };
	ClutterColor pink   = { 255,105,180,   255 };

	ClutterActor* parent = tangle_widget_new();
	int max=strlen(color_coding);
	int i;
	for (i=0;i<max;i++)
		{
		actor = tangle_widget_new();
		ClutterColor* color;
		switch (color_coding[i])
			{
			case '#': color =  &white         ;break;
			case 'y': color =  &yellow        ;break;
			case 'g': color =  &green         ;break;
			case 'o': color =  &orange        ;break;
			case 'b': color =  &blue          ;break;
			case 'r': color =  &red           ;break;
			case 'l': color =  &lblue         ;break;
			case 'v': color =  &violet        ;break;
			case 'p': color =  &pink          ;break;
			default: color = &white;
			}

		tangle_widget_set_background_color(TANGLE_WIDGET(actor), color);
		g_object_set(actor, "width", bar_size, "height", 40.0, "x", i* bar_size, "y", 80.0+y_offset, NULL);
		clutter_container_add_actor(CLUTTER_CONTAINER(parent), actor);
		}
	return parent;
}


static void on_cursor_notify_x(GObject* object, GParamSpec* param_spec, gpointer user_data) {
	TangleScrollAction* scroll_action;
	gfloat x, visible, offset, max;

	scroll_action = user_data;
	g_object_get(object, "x", &x, NULL);
	g_object_get(scroll_action, "page-length-x", &visible, "offset-x", &offset, "max-offset-x", &max, NULL);
	gfloat old_offset = offset;

	if (x < offset) { //Cursor is out of view somewhere too left
		offset = x - visible + 80.0;
	} else if (x > offset + visible - 80.0) { //Cursor is reaching right edge and there are need for scroll container
		offset += 360.0;
	}

	//Check Min and Max boundaries:
	if (offset < 0.0) offset = 0.0;
	if (offset > max) offset = max;

	//If there are no reason, do not do tangle_object_animate
	if (offset != old_offset)
		tangle_object_animate(G_OBJECT(scroll_action),  CLUTTER_EASE_IN_OUT_QUAD, 250, "offset-x", offset, NULL);
}


static void on_sequencer_stopped(JammoSequencer* sequencer, gpointer data) {
	jammo_theme* theme = (jammo_theme*)data;
	if (theme->only_playing_mode)
		{
		composition_game_close();
		return;
		}

	cem_add_to_log("Sequencer stopped",J_LOG_USER_ACTION);
	tangle_actor_show(TANGLE_ACTOR(jammo_mentor_get_default()));

	//Check is any soundfamily visible and start it's animations
	if (soundfamily[0]) {
	int i;
		for (i=0;i<4;i++) {
			if (CLUTTER_ACTOR_IS_VISIBLE(soundfamily[i])){
				clutter_actor_hide(soundfamily[i]);
				clutter_actor_show(soundfamily[i]); //this triggers animations to play
			}
		}
	}
	else //easy-game
		theme_play_all_icon_animations(theme);

	jammo_editing_track_view_set_editing_enabled(theme->bottom_track_view, TRUE);
	if (theme->upper_track_view && !(chum_get_pair_composition())) {
		jammo_editing_track_view_set_editing_enabled(theme->upper_track_view, TRUE);
	}


	g_object_set(theme->play_button, "x", 0.0,  NULL);
	//If user press stop, this is not needed (it happens automatically)
	//But if track ends and sequencer stops, we must call this
	//g_object_set(theme->play_button, "selected", TRUE,  NULL);
	tangle_button_set_selected(TANGLE_BUTTON(theme->play_button),FALSE);
}


void theme_stop_all_icon_animations(jammo_theme* theme){
	//printf("theme_stop_all_icon_animations \n");
	GList *timeline;
	for (timeline = theme->animation_timelines;timeline;timeline = timeline->next)
		{
		clutter_timeline_pause(CLUTTER_TIMELINE(timeline->data));
		}
}
void theme_play_all_icon_animations(jammo_theme* theme) {
	GList *timeline;
	for (timeline = theme->animation_timelines;timeline;timeline = timeline->next)
		{
		clutter_timeline_start(CLUTTER_TIMELINE(timeline->data));
		}
}
static void remove_all_icon_animations(jammo_theme* theme) {
	GList *timeline;
	for (timeline = theme->animation_timelines;timeline;timeline = timeline->next)
		{
		clutter_timeline_set_loop (CLUTTER_TIMELINE(timeline->data),FALSE); //Animations will stop and send completed, and then they are free'ed
		}
}

/*
Use when samplebutton is dropped to track or removed from track.
*/
static void log_track_activity (const gchar* event, int slot, ClutterActor* track, ClutterActor* actor) {
	gchar* name;
	g_object_get(actor,"sample-filename", &name,NULL); //It is sample_button
	
	gchar* name_of_track;
	g_object_get(track,"name", &name_of_track,NULL);

	gchar *message = g_strdup_printf("Sample '%s' %s track:%s at slot = %d",name, event, name_of_track, slot);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);
}


static void on_actor_added(ClutterActor* track, gint slot, ClutterActor* actor, gpointer none) {
	log_track_activity("added to", slot, track, actor);
}


static void on_actor_removed(ClutterActor* track, gint slot, ClutterActor* actor, gpointer none) {
	log_track_activity("removed from", slot, track, actor);
}


jammo_theme* theme_create(const gchar* name,int variation, gboolean only_listening, ClutterActor* play_button) {
	x_place_for_icon = 0; //static (used only when initializing new theme)
	jammo_theme* theme;
	theme = g_new( jammo_theme, 1 );

	theme->sample_button_array = g_ptr_array_new();
	theme->only_playing_mode=only_listening;
	theme->animation_timelines=NULL;
	theme->play_button = play_button;
	theme->name = name;
	theme->variation = variation;

	ClutterActor* view = tangle_widget_new();
	JammoSequencer* sequencer = jammo_sequencer_new();
	g_signal_connect(sequencer, "stopped", G_CALLBACK(on_sequencer_stopped), theme);

	if (only_listening)
		clutter_actor_hide(play_button);
	else
		clutter_actor_show(play_button); //it could be hided if we have been in cupboard

	ClutterActor* save_menu_container = tangle_widget_new();

	//clutter_container_add(CLUTTER_CONTAINER(container_for_buttons),save_menu_container,NULL);
	//clutter_actor_hide(save_menu_container);
	theme->container=view;
	theme->sequencer=sequencer;
	theme->save_menu_container=save_menu_container;

	//Advanced game sets this to TRUE
	gboolean use_two_tracks=FALSE;


	const gchar* background_color_code = "some_default";
	const gchar* backing_track_filepath = "no_any_backing_track";

	int disabled_bars_for_begin = 0;
	int disabled_bars_for_end = 0;
	int tempo_factory = 4;    //4 is default
	int beats_per_minute=90; //This value should be overwritten.


	if (chum_is_easy_game()) {
	backing_track_filepath = g_strdup_printf("%s/%s/%d/backing_track.wav", THEMES_DIR, name, tempo_table[variation]);
	beats_per_minute = tempo_table[variation];

	//Add samplebuttons. There are three themes, and each has three variations.
	if (strncmp(name,"fantasy",7)==0 && variation==1){
	create_samplebutton("fantasy","140","donkey.png",  "castle_140_drums_2.wav",        296.0, 103.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","140","faun.png",    "castle_140_synth_melody_2.wav", 574.0, 233.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","140","kangaroo.png","castle_140_synth_riff_2.wav",   626.0, 155.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","140","potion.png",  "castle_140_glockenspiel_1.wav", 129.0, 252.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","140","snake.png",   "castle_140_synth_glide_4.wav",  332.0, 197.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","140","witch.png",   "castle_140_melody_2.wav",       545.0,  31.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));

	background_color_code = "#yyyyyyyyppppppppyyyyyyyypppppppp###";
	disabled_bars_for_begin = 1;
	disabled_bars_for_end = 3;
	}
	else if (strncmp(name,"fantasy",7)==0 && variation==2){
	create_samplebutton("fantasy","110","dragon.png","castle_110_clarinet_4.wav",      221.0,  39.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","110","faerie.png","castle_110_glockenspiel_2.wav",  506.0, 201.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","110","flower.png","castle_110_piano_2.wav",         631.0, 243.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","110","ghost.png", "castle_110_flute_2.wav",         548.0,  26.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","110","owl.png",   "castle_110_owl_2.wav",           627.0, 158.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","110","wolf.png",  "castle_110_wolf_4.wav",           81.0, 274.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));

	background_color_code = "bbbbbbbbbbbbbbbbrrrrrrrrvvvvvvvvbbbbbbbb##";
	disabled_bars_for_end = 2;
	tempo_factory = 3;
	}
	else if (strncmp(name,"fantasy",7)==0 && variation==3){
	create_samplebutton("fantasy","90", "bird.png",    "castle_90_whistle_4.wav",    511.0, 107.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","90", "frog.png",    "castle_90_vibraphone_2.wav",  91.0, 245.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","90", "specter.png", "castle_90_triangle_4.wav",   217.0,  21.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","90", "mermaid.png", "castle_90_chimes_4.wav",     434.0, 274.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","90", "prince.png",  "castle_90_brass_2.wav",      628.0, 188.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("fantasy","90", "unicorn.png", "castle_90_oboe_2.wav",       318.0, 111.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));

	background_color_code = "llllvvvvvvvvllllllllvvvvvvvvrrrrrrrrvvvv";
	tempo_factory = 3;
	}

	else if (strncmp(name,"city",4)==0 && variation==1){
	create_samplebutton("city","140","bicycle.png",    "city_140_guitar1_2.wav",     574.0, 136.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","140","bucket.png",     "city_140_drum_fill_1.wav",   504.0, 235.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","140","bus.png",        "city_140_guitar_solo_4.wav",  84.0, 157.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","140","guitarhero.png", "city_140_guitar_riff_2.wav", 377.0, 102.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","140","kids.png",       "city_140_yell_claps_2.wav",  629.0, 215.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","140","motorcycle.png", "city_140_guitar2_2.wav",     306.0, 205.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));

	background_color_code = "yyyyyyyybbyyyyyyyybbgggggggbyyyyyyyy##";
	disabled_bars_for_end = 2;
	}
	else if (strncmp(name,"city",4)==0 && variation==2){
	create_samplebutton("city","110",  "beatbox.png", "city_110_beatbox_1.wav",       594.0, 169.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","110",  "crane.png",   "city_110_synth_melody_2.wav",  449.0,  23.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","110",  "robot.png",   "city_110_fx_2.wav",            292.0, 204.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","110",  "sign.png",    "city_110_trumpet_2.wav",       479.0, 144.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","110",  "skate.png",   "city_110_gliss_bass_2.wav",    161.0, 121.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","110",  "zeppelin.png","city_110_bass_strings_2.wav",  299.0,   0.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));

	background_color_code = "bbbbbbbbrrrrbbbbrrrrbbbbrrrr##";
	disabled_bars_for_end = 2;
	}
	else if (strncmp(name,"city",4)==0 && variation==3){
	create_samplebutton("city","90",  "car.png",         "city_90_bass_shaker_2.wav",  286.0, 183.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","90",  "chopper.png",     "city_90_melody1_2.wav",      387.0,   0.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","90",  "flower.png",      "city_90_synth_chords_2.wav", 471.0, 237.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","90",  "icecreamvan.png", "city_90_bell_melody_2.wav",  114.0, 155.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","90",  "jumpingrope.png", "city_90_melody2_2.wav",      621.0, 193.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("city","90",  "ufo.png",         "city_90_fx_2.wav",           264.0,  11.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));

	background_color_code = "ggggggggyyyyllllgggggggg##";
	disabled_bars_for_end = 2;
	}

	else if (strncmp(name,"animal",6)==0 && variation==1){
	create_samplebutton("animal","140",  "flamingo.png", "animalw_140_marimba_2.wav",   600.0, 160.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","140",  "gnome.png",    "animalw_140_vocal1_2.wav",    136.0, 220.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","140",  "gnomes.png",   "animalw_140_singing_2.wav",   297.0, 165.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","140",  "mouse.png",    "animalw_140_ud_2.wav",        310.0, 269.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","140",  "raven.png",    "animalw_140_vocal2_2.wav",    530.0,  71.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","140",  "snake.png",    "animalw_140_rainstick_1.wav", 500.0, 240.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));

	background_color_code = "yyyyyyyyoooooooorrrrrrrryyyyyyyy";
	}
	else if (strncmp(name,"animal",6)==0 && variation==2){
	create_samplebutton("animal","110",  "crocodile.png",   "animalw_110_percussion2_2.wav", 186.0, 141.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","110",  "dragonfly.png",   "animalw_110_guitar_2.wav",      320.0,  47.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","110",  "elephant.png",    "animalw_110_bass_2.wav",        315.0, 226.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","110",  "salamander.png",  "animalw_110_percussion1_2.wav", 133.0, 251.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","110",  "snake.png",       "animalw_110_vibraslap_2.wav",   448.0, 117.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","110",  "spider.png",      "animalw_110_pluck_2.wav",       617.0, 222.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));

	background_color_code = "ggggggggbbbbbbbbgggggggg#";
	disabled_bars_for_end = 1;
	}
	else if (strncmp(name,"animal",6)==0 && variation==3){
	create_samplebutton("animal","90",  "bluebird.png",  "animalw_90_accordion2_2.wav",      533.0,  68.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","90",  "butterfly.png", "animalw_90_guitar1_2.wav",         122.0,  29.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","90",  "flower.png",    "animalw_90_guitar2_2.wav",         337.0, 281.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","90",  "goril.png"  ,   "animalw_90_percussion_bass_2.wav", 329.0, 173.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","90",  "greenbird.png", "animalw_90_accordion1_2.wav",       94.0, 150.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));
	create_samplebutton("animal","90",  "monkey.png"    ,"animalw_90_percussion_2.wav",      328.0,  30.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(view));

	background_color_code = "yyyyyyyyggggggggoooooooo##";
	disabled_bars_for_end = 2;
	}

	} //easy game





else { //advanced
	//Make buttons and containers for samplebuttons.
	soundfamily[0] = create_soundfamily_button(theme,"rhythmic_icon.png", 296.0, 103.0,TANGLE_WIDGET(view));
	soundfamily[1] = create_soundfamily_button(theme,"melodic_icon.png",  101.0, 164.0,TANGLE_WIDGET(view));
	soundfamily[2] = create_soundfamily_button(theme,"harmonic_icon.png", 631.0, 199.0,TANGLE_WIDGET(view));
	soundfamily[3] = create_soundfamily_button(theme,"effect_icon.png",   498.0,  26.0,TANGLE_WIDGET(view));

	if (strncmp(name,"fantasy",7)==0){
	//Rhythmic loops
	create_samplebutton( "fantasy","advanced","donkey_1.png",  "Rh_90__34_1_shaker2.wav",   216.0,  20.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0]));
	create_samplebutton( "fantasy","advanced","donkey_2.png",  "Rh_90__34_2_bassdrum.wav",  296.0,  20.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0])); 
	create_samplebutton( "fantasy","advanced","donkey_3.png",  "Rh_90__34_1_triangle.wav",  216.0, 103.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0]));
	create_samplebutton( "fantasy","advanced","donkey_4.png",  "Rh_90__34_1_cajon.wav",     396.0,  20.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0]));
	create_samplebutton( "fantasy","advanced","donkey_5.png",  "Rh_90__34_1_shaker.wav",    396.0, 103.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0]));

	//Melodic loops
	create_samplebutton("fantasy","advanced","potion_1.png",  "Me_90_F_34_1_piano.wav",        120.0, 80.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));
	create_samplebutton("fantasy","advanced","potion_2.png",  "Me_90_D_34_1_synth.wav",        51.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));
	create_samplebutton("fantasy","advanced","potion_3.png",  "Me_90_A_34_2_vibraphone.wav",   190.0, 204.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));
	create_samplebutton("fantasy","advanced","potion_4.png",  "Me_90_A_34_2_whistle.wav",      190.0, 120.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));
	create_samplebutton("fantasy","advanced","potion_5.png",  "Me_90_C_34_2_flute.wav",        190.0,  35.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));

	//Harmonic loops
	create_samplebutton( "fantasy","advanced", "unicorn_1.png", "Ha_90_Dm_34_2_synth.wav",     721.0, 203.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));
	create_samplebutton( "fantasy","advanced", "unicorn_2.png", "Ha_90_Dm_34_2_guitar.wav",    721.0, 115.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));
	create_samplebutton( "fantasy","advanced", "unicorn_3.png", "Ha_90_D_34_2_accordion.wav",  621.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));
	create_samplebutton( "fantasy","advanced", "unicorn_4.png", "Ha_90_Am_34_2_elpiano.wav",   545.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));
	create_samplebutton( "fantasy","advanced", "unicorn_5.png", "Ha_90_Am_34_2_cello.wav",     465.0, 95.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));

	//Effect loops
	create_samplebutton( "fantasy","advanced", "ghost_1.png",  "Fx_90__34_1_vibraslap.wav",   598.0, 26.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));
	create_samplebutton( "fantasy","advanced", "ghost_2.png",  "Fx_90__34_2_chimes.wav",      400.0,  26.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));
	create_samplebutton( "fantasy","advanced", "ghost_3.png",  "Fx_90__34_2_gong.wav",        400.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));
	create_samplebutton( "fantasy","advanced", "ghost_4.png",  "Fx_90_Dm_34_2_crystal.wav",    498.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));
	create_samplebutton( "fantasy","advanced", "ghost_5.png",  "Fx_90_Dn_34_2_synth.wav",      598.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));

	if (variation==1)
		{
		background_color_code = "rrrrrrrrbbbbbbbbrrrrrrrrbbbbbbbbrrrrrrrr"; //'rhythmic'
		tempo_factory=3;
		}
	if (variation==2)
		{
		background_color_code = "bbbbvvvvbbbbvvvvrrrrbbbbvvvvbbbbvvvvrrrr";  //'strings'
		tempo_factory=3;
		}
	if (variation==3)
		{
		background_color_code = "vvvvbbbbvvvvbbbbvvvvbbbbllllvvvvvvvv####";  //'synth'
		tempo_factory=3;
		disabled_bars_for_end = 3;
		}
	beats_per_minute = 90;  //All advanced fantasy variations use same set of samples, so they have same bpm
	}

	else if (strncmp(name,"animal",6)==0){
	//Rhythmic loops
	create_samplebutton( "animal","advanced","monkey_1.png",  "Rh_110__54_1_bongo.wav",      216.0,  20.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0]));
	create_samplebutton( "animal","advanced","monkey_2.png",  "Rh_110__54_1_cajon.wav",      296.0,  20.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0])); 
	create_samplebutton( "animal","advanced","monkey_3.png",  "Rh_110__54_1_darabouka.wav",  216.0, 103.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0]));
	create_samplebutton( "animal","advanced","monkey_4.png",  "Rh_110__54_1_percussion.wav", 396.0,  20.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0]));
	create_samplebutton( "animal","advanced","monkey_5.png",  "Rh_110__54_1_tabla.wav",      396.0, 103.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0]));

	//Melodic loops
	create_samplebutton("animal","advanced","salamander_1.png",  "Me_110_A_54_1_clarinet.wav",  120.0,  80.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));
	create_samplebutton("animal","advanced","salamander_2.png",  "Me_110_C_54_1_flute.wav",      51.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));
	create_samplebutton("animal","advanced","salamander_3.png",  "Me_110_D_54_1_marimba.wav",   190.0, 204.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));
	create_samplebutton("animal","advanced","salamander_4.png",  "Me_110_D_54_1_oud.wav",       190.0, 120.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));
	create_samplebutton("animal","advanced","salamander_5.png",  "Me_110_F_54_1_pluck.wav",     190.0,  35.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));

	//Harmonic loops
	create_samplebutton( "animal","advanced", "dragonfly_1.png", "Ha_110_A_54_1_accordion.wav",  721.0, 203.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));
	create_samplebutton( "animal","advanced", "dragonfly_3.png", "Ha_110_D_54_1_guitar.wav",     721.0, 115.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));
	create_samplebutton( "animal","advanced", "dragonfly_2.png", "Ha_110_D_54_1_zither.wav",     651.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));
	create_samplebutton( "animal","advanced", "dragonfly_4.png", "Ha_110_G_54_1_guitar.wav",     605.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));
	create_samplebutton( "animal","advanced", "dragonfly_5.png", "Ha_110_G_54_1_marimba.wav",    575.0, 203.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));

	//Effect loops
	create_samplebutton( "animal","advanced", "bird_1.png",  "Fx_110__54_1_birdshout.wav",     598.0,  26.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));
	create_samplebutton( "animal","advanced", "bird_2.png",  "Fx_110__54_1_monkeyshout.wav",   430.0,  26.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));
	create_samplebutton( "animal","advanced", "bird_3.png",  "Fx_110__54_1_parrot.wav",        410.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));
	create_samplebutton( "animal","advanced", "bird_4.png",  "Fx_110__54_1_shaker.wav",        468.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));
	create_samplebutton( "animal","advanced", "bird_5.png",  "Fx_110__54_4_rainstick.wav",     518.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));

	if (variation==1)
		{
		//this is one bar shorter than another animal advanced tracks!
		background_color_code = "ggggyyyyvvvvyyyygggggggg#"; //'drumming/percussion'
		tempo_factory=5;
		}
	if (variation==2)
		{
		background_color_code = "rrrrrrrroooooooorrrrrrrr##";  //'flute'
		tempo_factory=5;
		}
	if (variation==3)
		{
		background_color_code = "yyyyggggyyyyggggyyyygggg##";  //'marimba'
		tempo_factory=5;
		}

	disabled_bars_for_end = 2; //each variations has same
	beats_per_minute = 110;  //All advanced animal variations use same set of samples, so they have same bpm
	}


	else if (strncmp(name,"city",4)==0){
	//Rhythmic loops
	create_samplebutton( "city","advanced","bucket_1.png",  "Rh_140__44_2_cowbell.wav",      216.0,  20.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0]));
	create_samplebutton( "city","advanced","bucket_2.png",  "Rh_140__44_2_eldrums1.wav",     296.0,  20.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0])); 
	create_samplebutton( "city","advanced","bucket_3.png",  "Rh_140__44_2_eldrums2.wav",     216.0, 103.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0]));
	create_samplebutton( "city","advanced","bucket_4.png",  "Rh_140__44_2_rockdrums1.wav",   396.0,  20.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0]));
	create_samplebutton( "city","advanced","bucket_5.png",  "Rh_140__44_2_rockdrums2.wav",   396.0, 103.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[0]));

	//Melodic loops
	create_samplebutton("city","advanced","guitar_1.png",  "Me_140_D_44_2_bass2.wav",     140.0,  80.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));
	create_samplebutton("city","advanced","guitar_2.png",  "Me_140_D_44_2_guitar2.wav",    71.0,  80.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));
	create_samplebutton("city","advanced","guitar_3.png",  "Me_140_F_44_2_guitar1.wav",   210.0, 204.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));
	create_samplebutton("city","advanced","guitar_4.png",  "Me_140_F_44_2_synth.wav",     210.0, 120.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));
	create_samplebutton("city","advanced","guitar_5.png",  "Me_140_G_44_2_bass1.wav",     210.0,  35.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[1]));

	//Harmonic loops
	create_samplebutton( "city","advanced", "cycle_5.png", "Ha_140_C_44_2_synth2.wav",      716.0, 203.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));
	create_samplebutton( "city","advanced", "cycle_3.png", "Ha_140_D_44_2_guitar2.wav",     721.0, 115.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));
	create_samplebutton( "city","advanced", "cycle_2.png", "Ha_140_D_44_2_organ.wav",       611.0, 120.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));
	create_samplebutton( "city","advanced", "cycle_4.png", "Ha_140_G_44_2_guitar1.wav",     525.0, 125.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));
	create_samplebutton( "city","advanced", "cycle_1.png", "Ha_140_G_44_2_synth1.wav",      535.0, 203.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[2]));

	//Effect loops
	create_samplebutton( "city","advanced", "chopper_1.png",  "Fx_140__44_2_claps.wav",       578.0,  26.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));
	create_samplebutton( "city","advanced", "chopper_2.png",  "Fx_140_G_44_2_highsynth.wav",  390.0,  26.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));
	create_samplebutton( "city","advanced", "chopper_3.png",  "Fx_140_G_44_2_robot.wav",      410.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));
	create_samplebutton( "city","advanced", "chopper_4.png",  "Fx_140_G_44_2_synth.wav",      498.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));
	create_samplebutton( "city","advanced", "chopper_5.png",  "Fx_140_G_44_2_trumpet.wav",    588.0, 110.0,theme, TANGLE_WIDGET(save_menu_container),TANGLE_WIDGET(soundfamily[3]));

	if (variation==1)
		{
		background_color_code = "yyyyrrrryyyyyyyyrrrrrrrryyyyyyyyrrrrrrrr##"; //'drums'
		tempo_factory=4;
		}
	if (variation==2)
		{
		background_color_code = "rrrrrrrrooooyyyyyyyyrrrrrrrryyyyyyyyoooo##";  //'guitar'
		tempo_factory=4;
		}
	if (variation==3)
		{
		background_color_code = "rrrroooorrrroooorrrroooorrrroooorrrroooo##";  //'synth'
		tempo_factory=4;
		}

	disabled_bars_for_end = 2; //each variations has same
	beats_per_minute = 140;  //All advanced city variations use same set of samples, so they have same bpm
	}



	backing_track_filepath = g_strdup_printf("%s/%s/advanced/backing_track_variation_%d.wav", THEMES_DIR, name,variation);
	use_two_tracks=TRUE;
} //advanced game







//printf("making track_views\n");
int y_offset =0;
if (use_two_tracks) y_offset=80;


	//Container for tracks (one or two) and backing_track
	ClutterActor* container_for_tracks = jammo_get_actor_by_id("composition_game_container_for_tracks");

	theme->color_bar = create_backing_track_color_bar(background_color_code,WIDTH_OF_BEAT*4,y_offset);
	clutter_actor_set_position(theme->color_bar, 0.0, 0.0);

	int number_of_bars = strlen(background_color_code);
	tangle_widget_add(TANGLE_WIDGET(container_for_tracks), theme->color_bar, NULL);

	//Tune width of containers
	clutter_actor_set_size(theme->color_bar,number_of_bars*WIDTH_OF_BEAT*4, 38.0);
	clutter_actor_set_size(container_for_tracks,number_of_bars*WIDTH_OF_BEAT*4, 120.0+y_offset); //height of track is 80 and backingtrack is 40

	gchar* message = g_strdup_printf("tempo_factory (time_sig): %d, beats_per_minute: %d",tempo_factory,beats_per_minute);
	cem_add_to_log(message,J_LOG_DEBUG);
	g_free(message);

	//We want operate whole time with guint64.
	guint64 temp=60*tempo_factory;
	guint64 one_second = 1000000000L;
	guint64 big = temp * one_second;
	guint64 bar_duration = big / beats_per_minute;
	guint64 beat_duration = bar_duration / 4;

	// sequencer needs to know time signature and tempo so they can be passed on to tracks
	jammo_sequencer_set_time_signature(sequencer, tempo_factory, 4);
	jammo_sequencer_set_tempo(sequencer, beats_per_minute);

	//Backing track
	gchar* filename_with_path;
	filename_with_path = tangle_lookup_filename(backing_track_filepath);

	message = g_strdup_printf("backingtrack filename (with fullpath): '%s'",filename_with_path);
	cem_add_to_log(message,J_LOG_DEBUG);
	g_free(message);
	int fd = open(filename_with_path,O_RDONLY);
	if (fd != -1) { //Found it.
		JammoBackingTrack* backing_track = jammo_backing_track_new(filename_with_path);
		jammo_sequencer_add_track(sequencer, JAMMO_TRACK(backing_track));
	}
	g_free(filename_with_path);


	//Bottom track. Below of this there will be place for scrollbar.
	JammoEditingTrack* editing_track_theme = jammo_editing_track_new_fixed_duration(4*number_of_bars*beat_duration);
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(editing_track_theme));
	ClutterActor* track_view = jammo_editing_track_view_new(editing_track_theme, 4*number_of_bars,beat_duration, WIDTH_OF_BEAT, 80.0);
	g_object_set(track_view, "disabled-slots-begin", disabled_bars_for_begin*4, "disabled-slots-end", disabled_bars_for_end*4,NULL);
	clutter_actor_set_reactive(track_view, TRUE);
	g_object_set(track_view, "line-every-nth-slot", 8, NULL); //every other bar
	clutter_actor_set_position(track_view, 0.0, 2.0+y_offset);
	tangle_widget_add(TANGLE_WIDGET(container_for_tracks), track_view, NULL);

	theme->bottom_track_view = JAMMO_EDITING_TRACK_VIEW(track_view); //This is needed when loading/saving
	theme->upper_track_view = NULL; //This will be NULL, if use_two_tracks is not TRUE.

	if (use_two_tracks){
	//Upper track.
	JammoEditingTrack* editing_track_theme2 = jammo_editing_track_new_fixed_duration(4*number_of_bars*beat_duration);
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(editing_track_theme2));
	ClutterActor* track_view2 = jammo_editing_track_view_new(editing_track_theme2, 4*number_of_bars,beat_duration, WIDTH_OF_BEAT, 80.0);


	//pair-game:
	if (chum_get_pair_composition()) {
		cem_add_to_log("pairgame",J_LOG_DEBUG);
		jammo_editing_track_view_set_editing_enabled(JAMMO_EDITING_TRACK_VIEW(track_view2),FALSE);
		g_object_set(chum_get_pair_composition(),"peer-track-view", track_view2,NULL); //this will handle all other things
	}
	else {
		cem_add_to_log("single player game", J_LOG_DEBUG);
		g_object_set(track_view2, "disabled-slots-begin", disabled_bars_for_begin*4, "disabled-slots-end", disabled_bars_for_end*4,NULL);
		clutter_actor_set_reactive(track_view2, TRUE);
	}

	g_object_set(track_view2, "line-every-nth-slot", 8, NULL);  //every other bar
	clutter_actor_set_position(track_view2, 0.0, 2.0);
	tangle_widget_add(TANGLE_WIDGET(container_for_tracks), track_view2, NULL);

	theme->upper_track_view = JAMMO_EDITING_TRACK_VIEW(track_view2); //This is needed when loading/saving
	}

	//tune size and position
	clutter_actor_set_position(container_for_tracks, 0.0, 360.0-y_offset);
	clutter_actor_set_size(container_for_tracks, 800.0, 120.0+y_offset);


	//Logging activity:
	clutter_actor_set_name(CLUTTER_ACTOR(theme->bottom_track_view),"Bottom");
	g_signal_connect(theme->bottom_track_view, "sample-added", G_CALLBACK(on_actor_added), NULL);
	g_signal_connect(theme->bottom_track_view, "sample-removed", G_CALLBACK(on_actor_removed), NULL);

	if (theme->upper_track_view) {
		clutter_actor_set_name(CLUTTER_ACTOR(theme->upper_track_view),"Upper");
		g_signal_connect(theme->upper_track_view, "sample-added", G_CALLBACK(on_actor_added), NULL);
		g_signal_connect(theme->upper_track_view, "sample-removed", G_CALLBACK(on_actor_removed), NULL);
	}


	ClutterColor red = { 255, 0, 0, 128 };
	ClutterActor* texture;
	ClutterActor* cursor;

	texture = clutter_rectangle_new_with_color(&red);
	clutter_actor_set_size(texture, 10.0, 80.0 +y_offset);
	cursor = jammo_cursor_new(sequencer, texture);
	tangle_actor_set_depth_position(TANGLE_ACTOR(cursor), 1);
	clutter_actor_set_position(cursor, 0.0, 0.0);
	clutter_actor_set_size(cursor, 4*number_of_bars*WIDTH_OF_BEAT, 80.0);
	clutter_container_add_actor(CLUTTER_CONTAINER(container_for_tracks), cursor);

	ClutterAction* scroll_action = tangle_actor_get_action_by_type(container_for_tracks,TANGLE_TYPE_SCROLL_ACTION);
	g_signal_connect(texture,  "notify::x", G_CALLBACK(on_cursor_notify_x), scroll_action);

	//Each time start track scrolled begin.
	g_object_set(scroll_action, "offset-x", 0, NULL);

	//Advanced-game: there are containers for samplebuttons, do not start hidden animations.
	if (!chum_is_easy_game())
		theme_stop_all_icon_animations(theme);
	return theme;
}


void delete_theme(jammo_theme* theme){
	cem_add_to_log("Freeing theme",J_LOG_DEBUG);

	if (theme->animation_timelines != NULL) {
		remove_all_icon_animations(theme);
		g_list_free(theme->animation_timelines);
	}

	g_ptr_array_free(theme->sample_button_array,TRUE);

	if (theme->save_menu_container != NULL){
		clutter_actor_destroy(theme->save_menu_container);
	}

	//Free contents of tracks
	ClutterActor* container_for_tracks = jammo_get_actor_by_id("composition_game_container_for_tracks");
	if (theme->bottom_track_view){
		clutter_container_foreach(CLUTTER_CONTAINER(theme->bottom_track_view), CLUTTER_CALLBACK(g_object_unref), NULL);
		clutter_container_remove_actor(CLUTTER_CONTAINER(container_for_tracks),CLUTTER_ACTOR(theme->bottom_track_view));
	}
	if (theme->upper_track_view){
		clutter_container_foreach(CLUTTER_CONTAINER(theme->upper_track_view), CLUTTER_CALLBACK(g_object_unref), NULL);
		clutter_container_remove_actor(CLUTTER_CONTAINER(container_for_tracks),CLUTTER_ACTOR(theme->upper_track_view));
	}

	clutter_actor_destroy(theme->color_bar);


	//Remove set of samplebuttons and all track_views and scrolling-actors etc.
	clutter_actor_destroy(theme->container);

	g_object_unref(theme->sequencer);
	g_free(theme);
}
