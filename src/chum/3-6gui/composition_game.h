#include "theme.h"
jammo_theme* composition_game_start_theme(const gchar* name, int variation, ClutterActor* where_to_go_after);
void composition_game_start_from_file(gpointer inputFilename, gchar* theme_name, int variation_name);
void composition_game_close();
