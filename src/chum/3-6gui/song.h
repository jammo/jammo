/*
 * song.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */
#ifndef _SONG_H_
#define _SONG_H_

#include <clutter/clutter.h>
typedef struct _jammo_song{
	gchar* fullpath; /* e.g /opt/jammo/songs/easy/spider */
	gchar* name_with_pack;  /* e.g    easy_spider   */   /*used for identifying savings*/
	gchar* name_of_recorded; /* keep this NULL, if not from cupboard*/
	gchar* language;
	gfloat icon_size;
	gfloat xoff;
	gfloat yoff;
	gfloat x_size;
	gfloat y_size;
	ClutterActor* songbutton;  /* This has callback and texture,  no slicing/plazing/zooming/framing*/
	gulong callback_handler_id;
} jammo_song;


GList* song_init_songs_from_disk();
void song_prepare_icon(jammo_song* song);
void song_free(jammo_song* song, gpointer data);
#endif /* __SONG_H_ */
