/*
 * jammo-note-button.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */

#ifndef __JAMMO_NOTE_BUTTON_H__
#define __JAMMO_NOTE_BUTTON_H__

#include <tangle.h>
#include "../meam/jammo-midi.h"

#define JAMMO_TYPE_NOTE_BUTTON (jammo_note_button_get_type())
#define JAMMO_NOTE_BUTTON(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_NOTE_BUTTON, JammoNoteButton))
#define JAMMO_IS_NOTE_BUTTON(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_NOTE_BUTTON))
#define JAMMO_NOTE_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_NOTE_BUTTON, JammoNoteButtonClass))
#define JAMMO_IS_NOTE_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_NOTE_BUTTON))
#define JAMMO_NOTE_BUTTON_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_NOTE_BUTTON, JammoNoteButtonClass))

typedef struct _JammoNoteButtonPrivate JammoNoteButtonPrivate;

typedef struct _JammoNoteButton {
	TangleButton parent_instance;
	JammoNoteButtonPrivate* priv;
} JammoNoteButton;

typedef struct _JammoNoteButtonClass {
	TangleButtonClass parent_class;
} JammoNoteButtonClass;

GType jammo_note_button_get_type(void) G_GNUC_CONST;


ClutterActor* jammo_note_button_new_without_events();
ClutterActor* jammo_note_button_new(gfloat width, gfloat height, JammoMidiEvent* start, JammoMidiEvent* stop);

#endif
