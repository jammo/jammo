/*
 * jammo-track-view.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */

#ifndef __JAMMO_TRACK_VIEW_H__
#define __JAMMO_TRACK_VIEW_H__

#include <tangle.h>
#include "../meam/jammo-track.h"


#define JAMMO_TYPE_TRACK_VIEW (jammo_track_view_get_type())
#define JAMMO_TRACK_VIEW(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), JAMMO_TYPE_TRACK_VIEW, JammoTrackView))
#define JAMMO_IS_TRACK_VIEW(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), JAMMO_TYPE_TRACK_VIEW))
#define JAMMO_TRACK_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), JAMMO_TYPE_TRACK_VIEW, JammoTrackViewClass))
#define JAMMO_IS_TRACK_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), JAMMO_TYPE_TRACK_VIEW))
#define JAMMO_TRACK_VIEW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), JAMMO_TYPE_TRACK_VIEW, JammoTrackViewClass))

typedef struct _JammoTrackViewPrivate JammoTrackViewPrivate;

typedef struct _JammoTrackView {
	TangleWidget parent_instance;
	JammoTrackViewPrivate* priv;
} JammoTrackView;

typedef struct _JammoTrackViewClass {
	TangleWidgetClass parent_class;
} JammoTrackViewClass;

GType jammo_track_view_get_type(void) G_GNUC_CONST;

ClutterActor* jammo_track_view_new(JammoTrack* track);
#endif
