/*
 * jammo-miditrack-view.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */

#include <stdlib.h>
#include "jammo-miditrack-view.h"
#include "jammo-note-button.h"
#include "../meam/jammo-midi.h"
#include <tangle.h>

G_DEFINE_TYPE(JammoMiditrackView, jammo_miditrack_view, TANGLE_TYPE_WIDGET);

enum {
	PROP_0,
	PROP_TRACK,
	PROP_N_SLOTS,       //number of timeslots
	PROP_HIGHEST_NOTE,  //
	PROP_LOWEST_NOTE,   //these defines also height of the track
	PROP_SLOT_DURATION,
	PROP_SLOT_HEIGHT,
	PROP_SLOT_WIDTH,
	PROP_DISABLED_SLOTS_BEGIN,
	PROP_DISABLED_SLOTS_END,
	PROP_PEN_MODE,      //clicking to track will make new notes
	PROP_ERASER_MODE,      //clicking to note on track will remove note
	PROP_GRID,             //Is grid visible or hidden
	PROP_EDITING_ENABLED
};

struct _JammoMiditrackViewPrivate {
	JammoInstrumentTrack* miditrack;
	guint n_slots;
	guint64 slot_duration;
	gfloat slot_width;
	gfloat slot_height;
	guint disabled_slots_begin;
	guint disabled_slots_end;
	ClutterColor hilight_color;
	
	gfloat real_slot_width;
	gfloat hilighted_start;
	gfloat hilighted_end;
	gfloat hilighted_horizontal_line;
	TangleGrid* grid;
	
	gint last_pressed_slot_x;   //Mouse click is accepted only if press and
	gint last_pressed_slot_y;   //    release happen in same slot
	gboolean discard_next_click;  //We want enable moving notes also in pen_mode

	guint show_grid : 1;
	guint pen_mode : 1;
	guint eraser_mode : 1;
	guint editing_enabled : 1;
	guint highest_note;
	guint lowest_note;
};


static void on_actor_added(TangleWidget* widget, ClutterActor* actor, gpointer user_data);
static void on_actor_removed(TangleWidget* widget, ClutterActor* actor, gpointer user_data);

ClutterActor* jammo_miditrack_view_new(JammoInstrumentTrack* miditrack, guint n_slots, guint64 slot_duration, gfloat slot_width, gfloat slot_height, guint h_note, guint l_note) {

	//printf ("JAMMO_TYPE_MIDITRACK_VIEW, n-slots:%d, slot-duration:%llu,  slot-width:%f, slot-height:%f, highest_note=%d, lowest_note=%d\n", n_slots,(unsigned long long)slot_duration, slot_width, slot_height, h_note,l_note);
	 ClutterActor* actor = CLUTTER_ACTOR(g_object_new(JAMMO_TYPE_MIDITRACK_VIEW, "track", miditrack, "n-slots", n_slots, "slot-duration", slot_duration, "slot-width", slot_width, "slot-height", slot_height, "highest-note", h_note, "lowest-note", l_note, NULL));
	 clutter_actor_set_reactive(actor,TRUE);
	 return actor;
}

/*
EventLists can come from files or straigt from the instrument_tracks.
Only start-stop pairs are added.
Single start or single stop is just omitted.
*/
void jammo_miditrack_view_add_event_list(JammoMiditrackView* miditrack_view, GList* events) {
	//Make table for each possible notes
	int size=96;
	JammoMidiEvent* table[size];
	int i;

	g_return_if_fail(JAMMO_IS_MIDITRACK_VIEW(miditrack_view));

	for (i=0;i<size;i++)
		table[i]=NULL;

	//Loop over list
	GList* list =NULL;
	JammoMidiEvent* n;

	for (list=events; list; list = list->next) {
		n = (list->data);
		if (n->type==JAMMOMIDI_NOTE_ON) {
			//printf("start found\n");
			if (table[n->note]) {
				//printf("second start without end\n");
				//do nothing (or do we want put some default ending?)
			}
			else {
				//printf("put to table\n");
				table[n->note]=n;
			}
		}
		if (n->type==JAMMOMIDI_NOTE_OFF) {
			if (table[n->note]==NULL){
				//ending without start
				//do nothing
			}
			else {
			//this is ending
			jammo_miditrack_view_add_note(miditrack_view, table[n->note], n );
			table[n->note]=NULL;
			}
		}
	}

	//If there was 'starts' without 'stops', they are now left in table[]
	// (each non-NULL)
}

/*
This is for loading from file (or using over network)

Note is star+stop jammo_midi_events.
 They must have same note-value.
 Start must come before end.

Returns newly created jammo-note-button
 There are no need to do anything about it (it is on midi-track).
*/
ClutterActor* jammo_miditrack_view_add_note(JammoMiditrackView* miditrack_view, JammoMidiEvent* start, JammoMidiEvent* stop ) {
	TangleWidget* widget;
	widget = TANGLE_WIDGET(miditrack_view);

	g_return_val_if_fail(JAMMO_IS_MIDITRACK_VIEW(miditrack_view), NULL);

	if (start->note != stop->note) {
		return NULL;
	}
	if (stop->timestamp < start->timestamp) {
		return NULL;
	}
	//width is in pixels and can be anything (i.e bigger or smaller than slot_width)
	gfloat width = (gfloat)(((gfloat)(stop->timestamp - start->timestamp)) / miditrack_view->priv->slot_duration) * miditrack_view->priv->slot_width;
	//printf("stop: %llu. start %llu, duration: %llu. width_px %f\n",stop->timestamp, start->timestamp, miditrack_view->priv->slot_duration,width);
	gfloat height = miditrack_view->priv->slot_height;

	ClutterActor* new_note_button = jammo_note_button_new(width,height,start,stop);
	jammo_instrument_track_add_jammo_midi_event(miditrack_view->priv->miditrack, start);
	jammo_instrument_track_add_jammo_midi_event(miditrack_view->priv->miditrack, stop);


	clutter_actor_meta_set_enabled(CLUTTER_ACTOR_META(tangle_actor_get_action_by_type(new_note_button, TANGLE_TYPE_DRAG_ACTION)),
          miditrack_view->priv->editing_enabled);

	//x_position is in pixels and can be anything (i.e. not dividable by slot_width)
	gfloat x_position = ((gfloat)(start->timestamp) / miditrack_view->priv->slot_duration) * miditrack_view->priv->slot_width;
	int y_slot = miditrack_view->priv->highest_note -start->note; //y_slot is always discrete integer.

	//printf("x: %f, y: %f\n", x_position,y_slot * miditrack_view->priv->slot_height);
	clutter_actor_set_x(new_note_button, x_position );
	clutter_actor_set_y(new_note_button, y_slot * miditrack_view->priv->slot_height);
	tangle_widget_add(widget, new_note_button, NULL);

	return new_note_button;
}


/*
Give new sizes in pixels for this miditrack_view.
Number of slots (x) and number of rows (y) are const.
So width of slot (x) and height of slot (y) will change.
*/
gboolean jammo_miditrack_view_zoom_to_fit(JammoMiditrackView* miditrack_view, gfloat target_x, gfloat target_y) {
	gfloat orig_width,orig_heigth;
	guint h_note, l_note, number_of_slots;

	g_return_val_if_fail(JAMMO_IS_MIDITRACK_VIEW(miditrack_view), FALSE);

	g_object_get(miditrack_view, "n-slots", &number_of_slots,
                               "highest-note", &h_note,
                               "lowest-note", &l_note,
                               "slot-height", &orig_heigth,
                               "slot-width", &orig_width,
                               NULL);

	gfloat new_width = target_x / number_of_slots; //new width for each slots
	gfloat ratio_x = new_width / orig_width;      //x-coordinate will be multiplied with this

	int number_of_notes = h_note-l_note+1;
	gfloat new_height = target_y / number_of_notes;  //new height for each slots
	gfloat ratio_y= new_height / orig_heigth;        //y-coordinate will be multiplied with this

	//New values for track
	g_object_set(miditrack_view,"slot-height", new_height,"slot-width", new_width,NULL);

	//New values for each existing note
	TangleWidget* widget = TANGLE_WIDGET(miditrack_view);
	TangleActorIterator actor_iterator;
	ClutterActor* actor;
	
	for (tangle_widget_initialize_actor_iterator(widget, &actor_iterator); (actor = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
		clutter_actor_set_width(actor,ratio_x*clutter_actor_get_width(actor)); //old value * ratio_x
		clutter_actor_set_x(actor,ratio_x*clutter_actor_get_x(actor));  //old value * ratio_x
		clutter_actor_set_height(actor,new_height); //Height of note is always same than height of slot.
		clutter_actor_set_y(actor,ratio_y*clutter_actor_get_y(actor));  //old value * ratio_y
		}
	return FALSE;
}

void jammo_miditrack_view_set_show_grid(JammoMiditrackView* miditrack_view, gboolean state) {
	g_return_if_fail(JAMMO_IS_MIDITRACK_VIEW(miditrack_view));

	miditrack_view->priv->show_grid = state;
}

gboolean jammo_miditrack_view_get_pen_mode(JammoMiditrackView* miditrack_view) {
	g_return_val_if_fail(JAMMO_IS_MIDITRACK_VIEW(miditrack_view), FALSE);

	return miditrack_view->priv->pen_mode;
}

void jammo_miditrack_view_set_pen_mode(JammoMiditrackView* miditrack_view, gboolean mode) {
	g_return_if_fail(JAMMO_IS_MIDITRACK_VIEW(miditrack_view));

	miditrack_view->priv->pen_mode = mode;
	g_object_notify(G_OBJECT(miditrack_view), "pen-mode");
}

gboolean jammo_miditrack_view_get_eraser_mode(JammoMiditrackView* miditrack_view) {
	g_return_val_if_fail(JAMMO_IS_MIDITRACK_VIEW(miditrack_view), FALSE);

	return miditrack_view->priv->eraser_mode;
}

void jammo_miditrack_view_set_eraser_mode(JammoMiditrackView* miditrack_view, gboolean mode) {
	g_return_if_fail(JAMMO_IS_MIDITRACK_VIEW(miditrack_view));

	miditrack_view->priv->eraser_mode = mode;
	g_object_notify(G_OBJECT(miditrack_view), "eraser-mode");
}

gboolean jammo_miditrack_view_get_editing_enabled(JammoMiditrackView* miditrack_view) {
	g_return_val_if_fail(JAMMO_IS_MIDITRACK_VIEW(miditrack_view), FALSE);

	return miditrack_view->priv->editing_enabled;
}

void jammo_miditrack_view_set_editing_enabled(JammoMiditrackView* miditrack_view, gboolean editing_enabled) {
	TangleActorIterator actor_iterator;
	ClutterActor* child;

	g_return_if_fail(JAMMO_IS_MIDITRACK_VIEW(miditrack_view));

	if (miditrack_view->priv->editing_enabled != editing_enabled) {
		miditrack_view->priv->editing_enabled = editing_enabled;
		
		clutter_actor_meta_set_enabled(CLUTTER_ACTOR_META(tangle_actor_get_action_by_type(CLUTTER_ACTOR(miditrack_view), TANGLE_TYPE_DROP_ACTION)), editing_enabled);
	
		for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(miditrack_view), &actor_iterator); (child = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
			clutter_actor_meta_set_enabled(CLUTTER_ACTOR_META(tangle_actor_get_action_by_type(child, TANGLE_TYPE_DRAG_ACTION)), editing_enabled);
		}

		g_object_notify(G_OBJECT(miditrack_view), "editing-enabled");
	}
}

static gboolean on_drag_begin_or_motion(TangleDropAction* drop_action, TangleDragAction* drag_action, gpointer user_data) {
	gboolean droppable = FALSE;
	JammoMiditrackView* miditrack_view;
	ClutterActor* meta_actor;
	ClutterActor* drag_actor;
	gfloat x, y, w, h;
	gfloat center_x, center_y;
	guint slot;
	guint n_slots;
	gfloat start;
	gfloat vertical_place;
	
	miditrack_view = JAMMO_MIDITRACK_VIEW(user_data);
	
	meta_actor = clutter_actor_meta_get_actor(CLUTTER_ACTOR_META(drag_action));
	drag_actor = tangle_drag_action_get_drag_actor(drag_action);;
	if (JAMMO_IS_NOTE_BUTTON(meta_actor)) {
		clutter_actor_get_transformed_position(drag_actor, &x, &y);
		clutter_actor_get_size(drag_actor, &w, &h);
		if (clutter_actor_transform_stage_point(CLUTTER_ACTOR(miditrack_view), x + w / 2, y + h / 2, &center_x, &center_y)) {
			n_slots = (guint)(clutter_actor_get_width(drag_actor) / miditrack_view->priv->real_slot_width + 0.5);
			center_x -= (n_slots - 1) * miditrack_view->priv->real_slot_width / 2;
			slot = center_x / miditrack_view->priv->real_slot_width;


			if (slot >= miditrack_view->priv->disabled_slots_begin &&
			    slot + n_slots - 1 < miditrack_view->priv->n_slots - miditrack_view->priv->disabled_slots_end) {
				droppable = TRUE;
				start = slot * miditrack_view->priv->real_slot_width;
				if (start != miditrack_view->priv->hilighted_start) {
					miditrack_view->priv->hilighted_start = start;
					miditrack_view->priv->hilighted_end = start + n_slots * miditrack_view->priv->real_slot_width;
					clutter_actor_queue_redraw(CLUTTER_ACTOR(miditrack_view));
				}
				vertical_place = (gint)(center_y / miditrack_view->priv->slot_height) * miditrack_view->priv->slot_height;
				if (vertical_place != miditrack_view->priv->hilighted_horizontal_line){
					miditrack_view->priv->hilighted_horizontal_line = vertical_place;
					clutter_actor_queue_redraw(CLUTTER_ACTOR(miditrack_view));
				}

			}
		}
	}
	
	if (!droppable && miditrack_view->priv->hilighted_start != miditrack_view->priv->hilighted_end) {
		miditrack_view->priv->hilighted_start = miditrack_view->priv->hilighted_end = 0.0;
		clutter_actor_queue_redraw(CLUTTER_ACTOR(miditrack_view));
	}
	    		
	return !droppable;
}

static void jammo_miditrack_view_get_preferred_width(TangleActor* actor, gfloat for_height, gboolean interacting, gfloat* min_width_p, gfloat* natural_width_p, gfloat* max_width_p) {
	JammoMiditrackView* miditrack_view;
	
	miditrack_view = JAMMO_MIDITRACK_VIEW(actor);
	
	if (min_width_p) {
		*min_width_p = miditrack_view->priv->n_slots * miditrack_view->priv->slot_width +1;
	}
	if (natural_width_p) {
		*natural_width_p = miditrack_view->priv->n_slots * miditrack_view->priv->slot_width +1;
	}
	if (max_width_p) {
		*max_width_p = 0.0;
	}
}

static void jammo_miditrack_view_get_preferred_height(TangleActor* actor, gfloat for_width, gboolean interacting, gfloat* min_height_p, gfloat* natural_height_p, gfloat* max_height_p) {
	JammoMiditrackView* miditrack_view;

	miditrack_view = JAMMO_MIDITRACK_VIEW(actor);

	gfloat height = (miditrack_view->priv->highest_note - miditrack_view->priv->lowest_note +1) * miditrack_view->priv->slot_height +1;
	if (min_height_p) {
		*min_height_p = height;
	}
	if (natural_height_p) {
		*natural_height_p = height;
	}
	if (max_height_p) {
		*max_height_p = height;
	}
	//printf("height of miditrack: '%f'. (notes %d...%d. slot_height=%f)\n",height, miditrack_view->priv->highest_note, miditrack_view->priv->lowest_note,  miditrack_view->priv->slot_height);
}

static void jammo_miditrack_view_allocate(ClutterActor* actor, const ClutterActorBox* box, ClutterAllocationFlags flags) {
	JammoMiditrackView* miditrack_view;

	miditrack_view = JAMMO_MIDITRACK_VIEW(actor);

	CLUTTER_ACTOR_CLASS(jammo_miditrack_view_parent_class)->allocate(actor, box, flags);

	miditrack_view->priv->real_slot_width = (box->x2 - box->x1) / miditrack_view->priv->n_slots;

	if (miditrack_view->priv->show_grid){
		tangle_grid_set_grid_spacing_x(miditrack_view->priv->grid, miditrack_view->priv->slot_width );
		tangle_grid_set_grid_spacing_y(miditrack_view->priv->grid, miditrack_view->priv->slot_height);
	}
	else { //only make frames
		tangle_grid_set_grid_spacing_x(miditrack_view->priv->grid, miditrack_view->priv->slot_width * miditrack_view->priv->n_slots -1);
		tangle_grid_set_grid_spacing_y(miditrack_view->priv->grid, miditrack_view->priv->slot_height * (miditrack_view->priv->highest_note
		- miditrack_view->priv->lowest_note +1) -1);
	}
}




/*

width: 1.0 = one slot-width
x: pixels
y: pixels

*/
static void internal_add_note(JammoMiditrackView* miditrack_view, gfloat width,gfloat x, gfloat y, gfloat center_x, gfloat center_y)
{
	printf("internal_add_note:  %f ; %f ; %f ;%f\n",x, y, center_x, center_y);
	ClutterActor* new_note_button;
	guint slot_x, slot_y;
	//TangleWidget* widget;
	//widget = TANGLE_WIDGET(miditrack_view);

	center_x -= (width - 1) * miditrack_view->priv->real_slot_width / 2;
	slot_x = center_x / miditrack_view->priv->real_slot_width;

	slot_y = center_y/miditrack_view->priv->slot_height;
	printf("slot_y = '%d', highest_note='%d', lowest_note='%d'\n",slot_y,miditrack_view->priv->highest_note,miditrack_view->priv->lowest_note);

	/* y-placement = pitch of note
	top row is slot_y=0, and under it there are 1,2,3,4...
	uppermost is priv->highest_note

	x-placement = time
	slot_duration * slot
	*/

	//We make timestamp already snapped to our current grid.
	JammoMidiEvent* start_event = malloc(sizeof(JammoMidiEvent));
	start_event->note = miditrack_view->priv->highest_note - slot_y;
	start_event->timestamp = miditrack_view->priv->slot_duration * slot_x;     //Starting time
	start_event->type = JAMMOMIDI_NOTE_ON;

	JammoMidiEvent* stop_event = malloc(sizeof(JammoMidiEvent));
	stop_event->note = miditrack_view->priv->highest_note - slot_y;
	stop_event->timestamp = start_event->timestamp + miditrack_view->priv->slot_duration * width;  //Ending time
	stop_event->type = JAMMOMIDI_NOTE_OFF;

	new_note_button=jammo_miditrack_view_add_note(miditrack_view, start_event, stop_event);
	//new_note_button is correct place automatically, but we want animate snapping.

	//Move it back (e.g. dragging ends here)
	clutter_actor_set_x(new_note_button, x);
	clutter_actor_set_y(new_note_button, y);

	//And put it correct place with animation
	clutter_actor_animate(new_note_button, CLUTTER_EASE_OUT_CUBIC, 300, "x", slot_x * miditrack_view->priv->real_slot_width,
                         "y",  slot_y * miditrack_view->priv->slot_height, NULL);
}

static gboolean on_drag_end(TangleDropAction* drop_action, TangleDragAction* drag_action, gpointer user_data) {
	JammoMiditrackView* miditrack_view;

	miditrack_view = JAMMO_MIDITRACK_VIEW(user_data);

	miditrack_view->priv->hilighted_start = miditrack_view->priv->hilighted_end = -1.0; //do not show hilighting anymore

	return FALSE;
}

static void on_dropped(TangleDropAction* drop_action, TangleDragAction* drag_action, gpointer user_data) {
	JammoMiditrackView* miditrack_view;
	//ClutterActor* meta_actor;
	ClutterActor* drag_actor;
	gfloat actor_x, actor_y, w, h;
	gfloat x, y, center_x, center_y;

	miditrack_view = JAMMO_MIDITRACK_VIEW(user_data);
		if (miditrack_view->priv->pen_mode) {
		//printf("dropping: in pen-mode\n");
		JAMMO_MIDITRACK_VIEW(miditrack_view)->priv->discard_next_click = TRUE;
	}
	
	//meta_actor = clutter_actor_meta_get_actor(CLUTTER_ACTOR_META(drag_action));
	drag_actor = tangle_drag_action_get_drag_actor(drag_action);
	clutter_actor_get_transformed_position(drag_actor, &actor_x, &actor_y);
	clutter_actor_get_size(drag_actor, &w, &h);
	if (clutter_actor_transform_stage_point(CLUTTER_ACTOR(miditrack_view), actor_x, actor_y, &x, &y) &&
	    clutter_actor_transform_stage_point(CLUTTER_ACTOR(miditrack_view), actor_x + w / 2, actor_y + h / 2, &center_x, &center_y)) {

	gfloat width = (clutter_actor_get_width(clutter_actor_meta_get_actor(CLUTTER_ACTOR_META(drag_action))) / miditrack_view->priv->real_slot_width );
	printf("handle_dropping:  %f ; %f ; %f ;%f\n",x, y, center_x, center_y);
	internal_add_note(miditrack_view, width,x - actor_x + w / 2,y - actor_y + h / 2,center_x,center_y);
	}
	else {
	//printf("dropping out of bounds -> this will be deleted!\n");
	miditrack_view->priv->discard_next_click = FALSE;
	}
}

static gboolean jammo_miditrack_view_handle_press(ClutterActor* clutter_actor, ClutterButtonEvent* event) {
	JammoMiditrackView* miditrack_view;
	miditrack_view = JAMMO_MIDITRACK_VIEW(clutter_actor);
	gfloat x, y;
	clutter_actor_transform_stage_point(CLUTTER_ACTOR(clutter_actor), event->x, event->y, &x, &y);

	gint slot_x = x / miditrack_view->priv->slot_width;
	gint slot_y = y /miditrack_view->priv->slot_height;
	//printf("press: slot_y = '%d', slot_x = '%d'\n",slot_y,slot_x);
	miditrack_view->priv->last_pressed_slot_x = slot_x;
	miditrack_view->priv->last_pressed_slot_y = slot_y;
	return FALSE;
}


static gboolean jammo_miditrack_view_handle_release(ClutterActor* clutter_actor, ClutterButtonEvent* event) {
	TangleActor* actor;
	actor = TANGLE_ACTOR(clutter_actor);

	if (JAMMO_MIDITRACK_VIEW(actor)->priv->pen_mode) {
		if (JAMMO_MIDITRACK_VIEW(actor)->priv->discard_next_click) {
			JAMMO_MIDITRACK_VIEW(actor)->priv->discard_next_click = FALSE;
			return FALSE;
		}

		gfloat x, y;
		clutter_actor_transform_stage_point(CLUTTER_ACTOR(clutter_actor), event->x, event->y, &x, &y);
		//printf("handle release in %f px ; %f px\n",x, y);

		JammoMiditrackView* miditrack_view;
		miditrack_view = JAMMO_MIDITRACK_VIEW(clutter_actor);
		gint slot_x = x / miditrack_view->priv->slot_width;
		gint slot_y = y /miditrack_view->priv->slot_height;
		//printf("release: slot_y = '%d', slot_x = '%d'\n",slot_y,slot_x);

		if (!(miditrack_view->priv->last_pressed_slot_x == slot_x &&
				miditrack_view->priv->last_pressed_slot_y == slot_y)) {
			//printf("not released on same grid than pressed\n");
			return FALSE;
		}

	gfloat default_note_width = 1.0;
	internal_add_note(JAMMO_MIDITRACK_VIEW(actor),default_note_width,x,y,x,y);
	}

	return FALSE;
}


static void jammo_miditrack_view_paint(ClutterActor* actor) {
	JammoMiditrackView* miditrack_view;
	gdouble scale_x;
	gdouble scale_y;
	guint8 alpha;
	ClutterActorBox box;

	miditrack_view = JAMMO_MIDITRACK_VIEW(actor);

	if (CLUTTER_ACTOR_IS_MAPPED(actor)) {
		CLUTTER_ACTOR_CLASS(jammo_miditrack_view_parent_class)->paint(actor);

		if (miditrack_view->priv->hilighted_start != miditrack_view->priv->hilighted_end) {
			cogl_push_matrix();

			g_object_get(actor, "transition-scale-x", &scale_x, "transition-scale-y", &scale_y, NULL);
			cogl_scale(scale_x, scale_y, 0);

			alpha = clutter_actor_get_paint_opacity(actor) * miditrack_view->priv->hilight_color.alpha / 255;
			cogl_set_source_color4ub(miditrack_view->priv->hilight_color.red,
			 			 miditrack_view->priv->hilight_color.green,
			 			 miditrack_view->priv->hilight_color.blue,
						 alpha);
			tangle_actor_get_aligned_allocation(TANGLE_ACTOR(actor), &box);
			cogl_rectangle(box.x1 + miditrack_view->priv->hilighted_start, box.y1, box.x1 + miditrack_view->priv->hilighted_end, box.y2);
			cogl_rectangle(box.x1 , box.y1 + miditrack_view->priv->hilighted_horizontal_line, box.x2 , box.y1 + miditrack_view->priv->hilighted_horizontal_line + miditrack_view->priv->slot_height);

			cogl_pop_matrix();
		}
	}
}

static void jammo_miditrack_view_constructed(GObject* object) {
	JammoMiditrackView* miditrack_view;
	ClutterAction* drop_action;

	miditrack_view = JAMMO_MIDITRACK_VIEW(object);

	g_signal_connect(miditrack_view, "actor-added", G_CALLBACK(on_actor_added), NULL);
	g_signal_connect(miditrack_view, "actor-removed", G_CALLBACK(on_actor_removed), NULL);

	JAMMO_MIDITRACK_VIEW(object)->priv->grid = TANGLE_GRID(tangle_grid_new(0.0, 0.0));
	tangle_widget_set_background_actor(TANGLE_WIDGET(miditrack_view), CLUTTER_ACTOR(JAMMO_MIDITRACK_VIEW(object)->priv->grid));

	drop_action = tangle_drop_action_new();
	g_signal_connect(drop_action, "drag-begin", G_CALLBACK(on_drag_begin_or_motion), miditrack_view);
	g_signal_connect(drop_action, "drag-motion", G_CALLBACK(on_drag_begin_or_motion), miditrack_view);
	g_signal_connect(drop_action, "drag-end", G_CALLBACK(on_drag_end), miditrack_view);
	g_signal_connect(drop_action, "dropped", G_CALLBACK(on_dropped), miditrack_view);
	clutter_actor_add_action(CLUTTER_ACTOR(miditrack_view), drop_action);
}

static void jammo_miditrack_view_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoMiditrackView* miditrack_view;

	miditrack_view = JAMMO_MIDITRACK_VIEW(object);

	switch (prop_id) {
		case PROP_TRACK:
			miditrack_view->priv->miditrack = JAMMO_INSTRUMENT_TRACK(g_value_get_object(value));
			g_object_ref(miditrack_view->priv->miditrack);
			break;
		case PROP_N_SLOTS:
			miditrack_view->priv->n_slots = g_value_get_uint(value);
			break;
		case PROP_HIGHEST_NOTE:
			miditrack_view->priv->highest_note = g_value_get_uint(value);
			break;
		case PROP_LOWEST_NOTE:
			miditrack_view->priv->lowest_note = g_value_get_uint(value);
			break;
		case PROP_SLOT_DURATION:
			miditrack_view->priv->slot_duration = g_value_get_uint64(value);
			break;
		case PROP_SLOT_HEIGHT:
			miditrack_view->priv->slot_height = g_value_get_float(value);
			clutter_actor_queue_relayout(CLUTTER_ACTOR(miditrack_view));
			break;
		case PROP_SLOT_WIDTH:
			miditrack_view->priv->slot_width = g_value_get_float(value);
			clutter_actor_queue_relayout(CLUTTER_ACTOR(miditrack_view));
			break;
		case PROP_DISABLED_SLOTS_BEGIN:
			miditrack_view->priv->disabled_slots_begin = g_value_get_uint(value);
			break;
		case PROP_DISABLED_SLOTS_END:
			miditrack_view->priv->disabled_slots_end = g_value_get_uint(value);
			break;
		case PROP_PEN_MODE:
			jammo_miditrack_view_set_pen_mode(miditrack_view, g_value_get_boolean(value));
			break;
		case PROP_ERASER_MODE:
			jammo_miditrack_view_set_eraser_mode(miditrack_view, g_value_get_boolean(value));
			break;
		case PROP_GRID:
			jammo_miditrack_view_set_show_grid(miditrack_view, g_value_get_boolean(value));
			break;
		case PROP_EDITING_ENABLED:
			jammo_miditrack_view_set_editing_enabled(miditrack_view, g_value_get_boolean(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_miditrack_view_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
	JammoMiditrackView* miditrack_view;

	miditrack_view = JAMMO_MIDITRACK_VIEW(object);

	switch (prop_id) {
		case PROP_TRACK:
			g_value_set_object(value, miditrack_view->priv->miditrack);
			break;
		case PROP_N_SLOTS:
			g_value_set_uint(value, miditrack_view->priv->n_slots);
			break;
		case PROP_HIGHEST_NOTE:
			g_value_set_uint(value, miditrack_view->priv->highest_note);
			break;
		case PROP_LOWEST_NOTE:
			g_value_set_uint(value, miditrack_view->priv->lowest_note);
			break;
		case PROP_SLOT_DURATION:
			g_value_set_uint64(value, miditrack_view->priv->slot_duration);
			break;
		case PROP_SLOT_HEIGHT:
			g_value_set_float(value, miditrack_view->priv->slot_height);
			break;
		case PROP_SLOT_WIDTH:
			g_value_set_float(value, (miditrack_view->priv->real_slot_width ? miditrack_view->priv->real_slot_width : miditrack_view->priv->slot_width));
			break;
		case PROP_DISABLED_SLOTS_BEGIN:
			g_value_set_uint(value, miditrack_view->priv->disabled_slots_begin);
			break;
		case PROP_DISABLED_SLOTS_END:
			g_value_set_uint(value, miditrack_view->priv->disabled_slots_end);
			break;
		case PROP_PEN_MODE:
			g_value_set_boolean(value, miditrack_view->priv->pen_mode);
			break;
		case PROP_ERASER_MODE:
			g_value_set_boolean(value, miditrack_view->priv->eraser_mode);
			break;
		case PROP_GRID:
			g_value_set_boolean(value, miditrack_view->priv->show_grid);
			break;
		case PROP_EDITING_ENABLED:
			g_value_set_boolean(value, miditrack_view->priv->editing_enabled);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
		}
}

static void jammo_miditrack_view_finalize(GObject* object) {
	G_OBJECT_CLASS(jammo_miditrack_view_parent_class)->finalize(object);
}

static void jammo_miditrack_view_dispose(GObject* object) {
	G_OBJECT_CLASS(jammo_miditrack_view_parent_class)->dispose(object);
}




static void jammo_miditrack_view_class_init(JammoMiditrackViewClass* miditrack_view_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(miditrack_view_class);
	ClutterActorClass* clutter_actor_class = CLUTTER_ACTOR_CLASS(miditrack_view_class);
	TangleActorClass* actor_class = TANGLE_ACTOR_CLASS(miditrack_view_class);

	gobject_class->constructed = jammo_miditrack_view_constructed;
	gobject_class->finalize = jammo_miditrack_view_finalize;
	gobject_class->dispose = jammo_miditrack_view_dispose;
	gobject_class->set_property = jammo_miditrack_view_set_property;
	gobject_class->get_property = jammo_miditrack_view_get_property;

	clutter_actor_class->paint = jammo_miditrack_view_paint;
	clutter_actor_class->allocate = jammo_miditrack_view_allocate;

	actor_class->get_preferred_width = jammo_miditrack_view_get_preferred_width;
	actor_class->get_preferred_height = jammo_miditrack_view_get_preferred_height;

	clutter_actor_class->button_press_event = jammo_miditrack_view_handle_press;
	clutter_actor_class->button_release_event = jammo_miditrack_view_handle_release;
	/**
	 * JammoMiditrackView:miditrack:
	 *
	 * The miditrack of this view.
	 */
	g_object_class_install_property(gobject_class, PROP_TRACK,
	                                g_param_spec_object("track",
	                                "Track",
	                                "The miditrack of this view",
	                                JAMMO_TYPE_INSTRUMENT_TRACK,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoMiditrackView:n-slots:
	 *
	 * The number of slots.
	 */
	g_object_class_install_property(gobject_class, PROP_N_SLOTS,
	                                g_param_spec_uint("n-slots",
	                                "N slots",
	                                "The number of slots",
	                                0, G_MAXUINT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoMiditrackView:highest-note:
	 *
	 * The highest note. C on octave0 is 0. C#0 is 1 ... a# on octave 7 is 95, b7 is 96.
	 */
	g_object_class_install_property(gobject_class, PROP_HIGHEST_NOTE,
	                                g_param_spec_uint("highest-note",
	                                "Highest note",
	                                "The highest note",
	                                0, 96, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE  | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoMiditrackView:lowest-note:
	 *
	 * The lowest note. C on octave0 is 0. C#0 is 1 ... a# on octave 7 is 95, b7 is 96.
	 */
	g_object_class_install_property(gobject_class, PROP_LOWEST_NOTE,
	                                g_param_spec_uint("lowest-note",
	                                "Lowest note",
	                                "The lowest note",
	                                0, 96, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoMiditrackView:slot-duration:
	 *
	 * The duration of a slot in nanoseconds.
	 */
	g_object_class_install_property(gobject_class, PROP_SLOT_DURATION,
	                                g_param_spec_uint64("slot-duration",
	                                "Slot duration",
	                                "The duration of a slot in nanoseconds",
	                                0, G_MAXUINT64, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoMiditrackView:slot-height:
	 *
	 * The height of the slot in the screen (pixels, Clutter units).
	 */
	g_object_class_install_property(gobject_class, PROP_SLOT_HEIGHT,
	                                g_param_spec_float("slot-height",
	                                "Slot height",
	                                "The height of the slot in the screen (pixels, Clutter units)",
	                                0, G_MAXFLOAT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoMiditrackView:slot-width:
	 *
	 * The width of the slot in the screen (pixels, Clutter units).
	 */
	g_object_class_install_property(gobject_class, PROP_SLOT_WIDTH,
	                                g_param_spec_float("slot-width",
	                                "Slot width",
	                                "The width of the slot in the screen (pixels, Clutter units)",
	                                0, G_MAXFLOAT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	/**
	 * JammoMiditrackView:disabled-slots-begin:
	 *
	 * The number of slots that are disabled (in terms of dropping) from the beginning of the miditrack.
	 */
	g_object_class_install_property(gobject_class, PROP_DISABLED_SLOTS_BEGIN,
	                                g_param_spec_uint("disabled-slots-begin",
	                                "Disabled slots begin",
	                                "The number of slots that are disabled (in terms of dropping) from the beginning of the miditrack",
	                                0, G_MAXUINT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoMiditrackView:disabled-slots-end:
	 *
	 * The number of slots that are disabled (in terms of dropping) from the end of the miditrack.
	 */
	g_object_class_install_property(gobject_class, PROP_DISABLED_SLOTS_END,
	                                g_param_spec_uint("disabled-slots-end",
	                                "Disabled slots end",
	                                "The number of slots that are disabled (in terms of dropping) from the end of the miditrack",
	                                0, G_MAXUINT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoMiditrackView:editing enabled:
	 *
	 * Whether the miditrack can be edited or not.
	 */
	g_object_class_install_property(gobject_class, PROP_EDITING_ENABLED,
	                                g_param_spec_boolean("editing-enabled",
	                                "Editing enabled",
	                                "Whether the miditrack can be edited or not",
	                                TRUE,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoMiditrackView:pen mode::
	 *
	 * When pen_mode is TRUE, clicking track will make new notes.
	 */
	g_object_class_install_property(gobject_class, PROP_PEN_MODE,
	                                g_param_spec_boolean("pen-mode",
	                                "Pen mode",
	                                "Clicking will make new notes",
	                                FALSE,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

/**
	 * JammoMiditrackView:pen mode::
	 *
	 * When eraser_mode is TRUE, clicking note (on this trac) will delete that note.
	 */
	g_object_class_install_property(gobject_class, PROP_ERASER_MODE,
	                                g_param_spec_boolean("eraser-mode",
	                                "Eraser mode",
	                                "Clicking will delete note",
	                                FALSE,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
/**
	 * JammoMiditrackView:grid:
	 *
	 * Is grid visible or hidden.
	 */
	g_object_class_install_property(gobject_class, PROP_GRID,
	                                g_param_spec_boolean("grid",
	                                "Visible grid",
	                                "Whether grid is visible or hidden",
	                                FALSE,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	g_type_class_add_private (gobject_class, sizeof (JammoMiditrackViewPrivate));
}

static void jammo_miditrack_view_init(JammoMiditrackView* miditrack_view) {
	//printf("INIT\n");
	miditrack_view->priv = G_TYPE_INSTANCE_GET_PRIVATE(miditrack_view, JAMMO_TYPE_MIDITRACK_VIEW, JammoMiditrackViewPrivate);

	miditrack_view->priv->editing_enabled = TRUE;

	miditrack_view->priv->hilight_color.red = 0;
	miditrack_view->priv->hilight_color.green = 255;
	miditrack_view->priv->hilight_color.blue = 255;
	miditrack_view->priv->hilight_color.alpha = 100;

	miditrack_view->priv->hilighted_start = miditrack_view->priv->hilighted_end = -1.0;
}

static void on_actor_added(TangleWidget* widget, ClutterActor* actor, gpointer user_data) {
/*
	JammoMiditrackView* miditrack_view;
	JammoNoteButton* note_button;

	g_return_if_fail(JAMMO_IS_NOTE_BUTTON(actor));
	
	miditrack_view = JAMMO_MIDITRACK_VIEW(widget);
	note_button = JAMMO_NOTE_BUTTON(actor);
*/
}

static void on_actor_removed(TangleWidget* widget, ClutterActor* actor, gpointer user_data) {
	//printf("on_actor_removed triggered\n");
	JammoMiditrackView* miditrack_view;
	JammoNoteButton* note_button;

	g_return_if_fail(JAMMO_IS_NOTE_BUTTON(actor));
	note_button=JAMMO_NOTE_BUTTON(actor);

	g_return_if_fail(JAMMO_IS_MIDITRACK_VIEW(widget));
	miditrack_view=JAMMO_MIDITRACK_VIEW(widget);

	JammoMidiEvent* event;

	g_object_get(note_button, "start-event", &event, NULL);
	//printf("got for removing: %d, %llu \n",event->note, (unsigned long long)event->timestamp);

	jammo_instrument_track_remove_event(miditrack_view->priv->miditrack, event->note,  event->timestamp);
}

void jammo_miditrack_view_remove_all(JammoMiditrackView* miditrack_view) {
	g_return_if_fail(JAMMO_IS_MIDITRACK_VIEW(miditrack_view));

	//printf("jammo_miditrack_view_remove_all\n");
	TangleWidget* widget = TANGLE_WIDGET(miditrack_view);
	GList* children = clutter_container_get_children(CLUTTER_CONTAINER(widget));
	if (children) {
		g_list_foreach(children, (GFunc)clutter_actor_destroy, NULL);
		g_list_free(children);
	}
}
