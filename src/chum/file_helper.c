
/** file_helper.c is part of JamMo.
License: GPLv2, read more from COPYING

This file contain helper functions, which are meant to be used via chum.
*/
#include <glib-object.h>
#include <string.h>
#include <stdlib.h>

#include "jammo-chum.h"
#include <tangle.h>

#include "../configure.h" //e.g. DATA_DIR
#include "jammo-track-view.h"
#include "jammo-editing-track-view.h"
#include "jammo-miditrack-view.h"
#include "../meam/jammo-slider-track.h"
#include "../meam/jammo-backing-track.h"

#include "../cem/cem.h"

//These macros are there because there could be something extra in sequencer_widget which is passed as parameter.
//We do not do anything unless we are sure we have correct real JAMMO_TRACK.

//With first track, start only with linebreak, another add comma and then linebreak
#define FIRST_TRACK_STARTS_WITHOUT_COMMA \
	fprintf(ofp,first_track?"":",\n"); \
	first_track=FALSE;

#define WRITE_VOLUME_AND_MUTED \
	gboolean something_writted=FALSE; \
	if (jammo_playing_track_get_volume(JAMMO_PLAYING_TRACK(track))!= 1.0) {\
		gint buf_len = 10; \
		gchar buffer[buf_len]; \
		gchar* volume = g_ascii_dtostr (buffer, buf_len,jammo_playing_track_get_volume(JAMMO_PLAYING_TRACK(track))); \
		fprintf(ofp,",\n\t\"volume\" : %s\n",volume); \
		something_writted=TRUE; \
		}\
	if (jammo_playing_track_get_muted(JAMMO_PLAYING_TRACK(track))) {\
			fprintf(ofp,",\n\t\"muted\" : true\n"); \
			something_writted=TRUE; \
			} \
	if (!something_writted) {\
		fprintf(ofp,"\n"); \
	}

/*
-Final filename starts with JAMMO_DIRECTORY,COMPOSITIONS_DIRECTORY
-sequencer_widget must be TangleWdiget and contain track_views and miditrack_view, etc.

-user_metadata will be something differencies between 3-6 game and 7-12 game.
 3-6 game contains info about theme_name and variation
 7-12 game contains info about pitch and tempo

*/

void save_composition(const gchar* folder, const gchar* to_filename, ClutterActor* sequencer_widget, gchar* user_metadata) {

	//Generate filename based on timestamp
	char timestamp_now [80];
	cem_get_time(timestamp_now);

	guint miditrack_count=0;
	guint slidertrack_count=0;

	FILE* ofp;
	gchar* outputFilename;

	//This is temporarily
	outputFilename = g_strdup_printf("%s/last_composition.json", configure_get_jammo_directory());

	ofp = fopen(outputFilename, "w");
	g_free(outputFilename);
	g_return_if_fail(ofp != NULL);

	//'Header'
	fprintf(ofp,"{\n");

	//Metadata
	fprintf(ofp,"%s\n",user_metadata);

	fprintf(ofp,"\"format-version\" : 3,\n");
	fprintf(ofp,"\"Author\" : \"%s\",\n","author");   //TODO: add author
	fprintf(ofp,"\n");


	//Tracks
	fprintf(ofp,"\"tracks\" : [");
	JammoTrack* track;

	gboolean first_track = TRUE;
	TangleActorIterator actor_iterator;
	ClutterActor* track_view;
	
	for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(sequencer_widget), &actor_iterator); (track_view = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {

		/* //There are something non-track_views on container. FIXME
		if (TANGLE_IS_SCROLLING_ACTOR(track_view)){
			fprintf(ofp,"(scroller)\n");
			continue;
		}
		*/


		if (JAMMO_IS_EDITING_TRACK_VIEW(track_view)){
			cem_add_to_log(" Saving EditingTrack",J_LOG_DEBUG);
			FIRST_TRACK_STARTS_WITHOUT_COMMA

			fprintf(ofp,"\n\t{\n");
			fprintf(ofp,"\t\"track-type\" : \"EditingTrack\",\n");

			g_object_get(track_view,"track",&track,NULL);

			const gchar* name_of_track = clutter_actor_get_name(track_view);
			fprintf(ofp,"\t\"name\" : \"%s\",\n",name_of_track);

			JammoSampleType type;
			g_object_get(track_view,"sample-type", &type,NULL);

			GEnumClass* enum_class = G_ENUM_CLASS(g_type_class_ref(JAMMO_TYPE_SAMPLE_TYPE));
			GEnumValue* enum_value = g_enum_get_value(enum_class, type);
			g_type_class_unref(enum_class);
			if (enum_value) {
				const gchar* nick = enum_value->value_nick;
				fprintf(ofp,"\t\"editing-track-type\" : \"%s\",\n",nick);
			}

			gfloat slot_size;
			g_object_get(track_view,"slot-width",&slot_size,NULL);
			//printf("slot_size '%lf'\n",slot_size);
			gint slot_width = slot_size;
			fprintf(ofp,"\t\"slot-width\" : %d,\n",slot_width);

			fprintf(ofp,"\t\"samples\" : [");

			TangleActorIterator actor_iterator2;
			ClutterActor* child;
			JammoSampleButton* jsb;
			gboolean first = TRUE;
	
			for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(track_view), &actor_iterator2); (child = tangle_actor_iterator_get_actor(&actor_iterator2)); tangle_actor_iterator_next(&actor_iterator2))
				{
				if (first)
					fprintf(ofp,"\n");
				else
					fprintf(ofp,",\n");
				first=FALSE;
				jsb = JAMMO_SAMPLE_BUTTON(child);
				//printf("x: '%lf' \n",clutter_actor_get_x(CLUTTER_ACTOR(jsb)));
				//printf("slot: '%lf' \n",clutter_actor_get_x(CLUTTER_ACTOR(jsb))/slot_size);
				fprintf(ofp,"\t\t{\n");

				//Not saving anymore loop_id. composition_game_file_helper can read old 'format' still
				//gint loop_id;
				//g_object_get(jsb, "loop_id", &loop_id, NULL);
				//fprintf(ofp,"\t\t\"loop_id\" : %d,\n",loop_id);

				gchar* image_filename;
				g_object_get(jsb, "image-filename", &image_filename, NULL);
				fprintf(ofp,"\t\t\"image\" : \"%s\",\n",image_filename);

				gchar* audio_filename;
				g_object_get(jsb, "sample-filename", &audio_filename, NULL);
				fprintf(ofp,"\t\t\"audio\" : \"%s\",\n",audio_filename);

				gint slot = clutter_actor_get_x(CLUTTER_ACTOR(jsb))/slot_size;
				fprintf(ofp,"\t\t\"slot\" : %d\n",slot);
				fprintf(ofp,"\t\t}");
				}
		fprintf(ofp,"\n\t]");
		WRITE_VOLUME_AND_MUTED
		fprintf(ofp,"\t}");
		} //Jammo-Track-View ends


		else if (JAMMO_IS_MIDITRACK_VIEW(track_view)){
			 cem_add_to_log(" Saving MidiTrack",J_LOG_DEBUG);
			 FIRST_TRACK_STARTS_WITHOUT_COMMA

			g_object_get(track_view,"track",&track,NULL);
			int type;
			g_object_get(track,"instrument-type",&type,NULL);

			fprintf(ofp,"\n\t{\n");
			fprintf(ofp,"\t\"track-type\" : \"VirtualInstrumentTrack\",\n");
			const gchar* name_of_track = clutter_actor_get_name(track_view);
			fprintf(ofp,"\t\"name\" : \"%s\",\n",name_of_track);
			fprintf(ofp,"\t\"instrument\" : %d",type);

			GList* list= jammo_instrument_track_get_event_list(JAMMO_INSTRUMENT_TRACK(track));
			if (list) {
				//fullpath!
				gchar* name_for_notes = g_strdup_printf("%s/%s%s-midi%d.txt",folder,to_filename,timestamp_now,miditrack_count);
				miditrack_count++;
				jammomidi_glist_to_file(list, name_for_notes);

				fprintf(ofp,",\n\t\"note-file\" : \"%s\"",name_for_notes);
				g_free(name_for_notes);
			}
			WRITE_VOLUME_AND_MUTED
			fprintf(ofp,"\t}");
		}//Jammo-Midtrack-View ends

		else if (JAMMO_IS_TRACK_VIEW(track_view)){
			cem_add_to_log(" Saving common TRACK_VIEW...",J_LOG_DEBUG);
			g_object_get(track_view,"track",&track,NULL);

			if (JAMMO_IS_SLIDER_TRACK(track)) {
			cem_add_to_log("  Saving SliderTrack",J_LOG_DEBUG);
			FIRST_TRACK_STARTS_WITHOUT_COMMA
			int type;
			g_object_get(track,"slider-type",&type,NULL);


			fprintf(ofp,"\n\t{\n");
			fprintf(ofp,"\t\"track-type\" : \"SliderTrack\",\n");
			const gchar* name_of_track = clutter_actor_get_name(track_view);
			fprintf(ofp,"\t\"name\" : \"%s\",\n",name_of_track);
			fprintf(ofp,"\t\"instrument\" : \"%d\"",type);

			GList* list= jammo_slider_track_get_event_list(JAMMO_SLIDER_TRACK(track));
			if (list) {
				//fullpath!
				gchar* name_for_events = g_strdup_printf("%s/%s%s-slider%d.txt",folder,to_filename,timestamp_now,slidertrack_count);
				slidertrack_count++;
				jammo_slider_event_glist_to_file(list, name_for_events);

				fprintf(ofp,",\n\t\"note-file\" : \"%s\"",name_for_events);
				g_free(name_for_events);
			}

			WRITE_VOLUME_AND_MUTED
			fprintf(ofp,"\t}");
			}//Jammo-Slider-Track ends

			else if (JAMMO_IS_BACKING_TRACK(track)) {
			cem_add_to_log("  Saving BackingTrack",J_LOG_DEBUG);
			FIRST_TRACK_STARTS_WITHOUT_COMMA
			gchar *filename;
			g_object_get(track,"filename",&filename,NULL);

			ClutterActor* back;
			g_object_get(track_view,"background-actor",&back,NULL);

			gchar* image_filename;
			g_object_get(back,"filename",&image_filename,NULL);

			fprintf(ofp,"\n\t{\n");
			fprintf(ofp,"\t\"track-type\" : \"BackingTrack\",\n");
			const gchar* name_of_track = clutter_actor_get_name(track_view);
			fprintf(ofp,"\t\"name\" : \"%s\",\n",name_of_track);
			fprintf(ofp,"\t\"image-file\" : \"%s\",\n",image_filename);
			fprintf(ofp,"\t\"audio-file\" : \"%s\"",filename);
			WRITE_VOLUME_AND_MUTED
			fprintf(ofp,"\t}");
			}//Jammo-Backing-Track ends


		}


	else {
		//There can be some scrollers or something
	}

}//Next track

	//'Footer'
	fprintf(ofp,"\n\n]\n}\n");
	fclose(ofp);

	//Saving to cupboard
	gchar* cmd;
	gchar* final_filename;
	final_filename = g_strdup_printf("%s/%s%s.json",folder,to_filename,timestamp_now);

	cmd = g_strdup_printf("mv %s/last_composition.json %s",configure_get_jammo_directory(),final_filename);

	//printf("Executing: '%s'\n",cmd);
	if (system(cmd))
		printf("Error, can't call '%s'\n",cmd);

	g_free(cmd);

	gchar* message = g_strdup_printf("Saved file '%s'", final_filename);
	cem_add_to_log(message,J_LOG_DEBUG);
	g_free(message);

	g_free(final_filename);
}



/*
 * JsonNode *sub_node is 'samples' -array
 * JammoEditingTrackView* track_view is target JammoEditingTrackView
 * gboolean reactive : reactivity of each new jammo_sample_button
 */
void load_this_sample_array_to_this_track_view(JsonNode *sub_node,JammoEditingTrackView* track_view, gboolean reactive) {
	//Array
	//sub_node = json_object_get_member (sub_object, "samples");
	if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_ARRAY){
		JsonArray* sample_array =  json_node_get_array (sub_node);

		guint length = json_array_get_length(sample_array);
		//printf("length %d\n",length);
		int i;
		for (i=0;i<length;i++) {
			JsonNode* sample_node;
			sample_node = json_array_get_element(sample_array,i);
			if (sample_node!=NULL && JSON_NODE_TYPE(sample_node) == JSON_NODE_OBJECT){
				JsonObject* sample_object = json_node_get_object(sample_node);
				gint slot=-1;
				const gchar* image_file=NULL;
				const gchar* audio_file=NULL;

				//string
				sub_node = json_object_get_member (sample_object, "image");
				if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
					image_file = json_node_get_string (sub_node);
					//printf("image: '%s'\n",image_file);
				}
				//string
				sub_node = json_object_get_member (sample_object, "audio");
				if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
					audio_file = json_node_get_string (sub_node);
					//printf("audio: '%s'\n",audio_file);
				}

				//int
				sub_node = json_object_get_member (sample_object, "slot");
				if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
					slot =  json_node_get_int (sub_node);
					//printf("slot: '%d'\n",slot);
				}
				if (track_view) {
					ClutterActor* sample_button=NULL;
					if (image_file && audio_file)
						sample_button = jammo_sample_button_new_from_files(image_file, audio_file);
					if (sample_button) {
						jammo_editing_track_view_add_jammo_sample_button(track_view,JAMMO_SAMPLE_BUTTON(sample_button),slot);
						clutter_actor_set_reactive(sample_button,reactive);
						//name contains _34_ (means 3/4) which means waltz-sample
						if (g_strrstr(audio_file,"_34_")!=NULL) {
							printf("ladattiin waltz\n");
							jammo_sample_button_set_waltz(JAMMO_SAMPLE_BUTTON(sample_button),TRUE);
							jammo_sample_button_set_text(JAMMO_SAMPLE_BUTTON(sample_button),"3");
						}
					} //sample_button!=NULL check
				} //track!=NULL check

			} //This sample-object is ready

		} //Foreach in sample-array ready

	} //sample-array is ready

}

/*
 * JsonNode *sub_node is 'samples' -array
 * JammoEditingTrackView* track_view is target JammoEditingTrackView
 * gboolean reactive : reactivity of each new jammo_sample_button
 *
 ** This is used loading format-version=2 compositions.
 */
void load_this_sample_array_to_this_track_view_legacy2(JsonNode *sub_node,JammoEditingTrackView* track_view, gboolean reactive) {
	//Array
	//sub_node = json_object_get_member (sub_object, "samples");
	if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_ARRAY){
		JsonArray* sample_array =  json_node_get_array (sub_node);

		guint length = json_array_get_length(sample_array);
		//printf("length %d\n",length);
		int i;
		for (i=0;i<length;i++) {
			JsonNode* sample_node;
			sample_node = json_array_get_element(sample_array,i);
			if (sample_node!=NULL && JSON_NODE_TYPE(sample_node) == JSON_NODE_OBJECT){
				JsonObject* sample_object = json_node_get_object(sample_node);
				gint slot=-1;
				const gchar* image_file=NULL;
				const gchar* audio_file=NULL;

				//string
				sub_node = json_object_get_member (sample_object, "image");
				if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
					image_file = json_node_get_string (sub_node);
					//printf("image: '%s'\n",image_file);
				}
				//string
				sub_node = json_object_get_member (sample_object, "audio");
				if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
					audio_file = json_node_get_string (sub_node);
					//printf("audio: '%s'\n",audio_file);
				}

				//int
				sub_node = json_object_get_member (sample_object, "slot");
				if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
					slot =  json_node_get_int (sub_node);
					slot = slot*4; //THIS IS LEGACY2: slot was earlier bar, now it is beat!
					//printf("slot: '%d'\n",slot);
				}
				if (track_view) {
					ClutterActor* sample_button=NULL;
					if (image_file && audio_file)
						sample_button = jammo_sample_button_new_from_files(image_file, audio_file);
					if (sample_button) {
						jammo_editing_track_view_add_jammo_sample_button(track_view,JAMMO_SAMPLE_BUTTON(sample_button),slot);
						clutter_actor_set_reactive(sample_button,reactive);
						//name contains _34_ (means 3/4) which means waltz-sample
						if (g_strrstr(audio_file,"_34_")!=NULL) {
							printf("ladattiin waltz\n");
							jammo_sample_button_set_waltz(JAMMO_SAMPLE_BUTTON(sample_button),TRUE);
							jammo_sample_button_set_text(JAMMO_SAMPLE_BUTTON(sample_button),"3");
						}
					} //sample_button!=NULL check
				} //track!=NULL check

			} //This sample-object is ready

		} //Foreach in sample-array ready

	} //sample-array is ready

}


/**
compares 2 filenames.
This is function is needed because we want return directory listing sorted.
If another (or both) is NULL, will return 0.
**/
static int string_compare( gconstpointer a, gconstpointer b ){
   if (a==NULL)
      return 0;
   if (b==NULL)
      return 0;

   guint length = -1;
   gint rv = -1;

   length =  strlen( (gchar *) a );
   rv = memcmp( a, b, length);

   return rv;
}


static GList *scan_and_sort_directory( const gchar *directory_name ) {
   GError *error = NULL;
   guint flags = 0;

   GDir *directory = g_dir_open( directory_name, flags, &error );

   if( directory == NULL )
      {
      gchar* message = g_strdup_printf("Couldn't not open the directory %s", directory_name);
      cem_add_to_log(message,J_LOG_ERROR);
      g_free(message);
      cem_add_to_log(error->message,J_LOG_ERROR);
      g_error_free( error );
      return NULL;
      }


   GList *list = NULL;
   while (TRUE)
      {
      const gchar *temp = NULL;
      /* the return value of g_dir_read_name is own by glib
         Do not modify or free */
      temp = g_dir_read_name( directory );
      if( temp == NULL )
        {
        break;
        }
      gchar *copy = g_strndup( temp, strlen(temp) );
      list = g_list_append( list, (gchar *) copy );
      }

   list = g_list_sort( list, string_compare );
   g_dir_close( directory );
   return list;
}


/*
One song is in one folder, which contain songfile and picture (maybe lyrics and so on).
'name of song' is name of this folder.

This function returns list of all those folders (they all starts with SONGS_FOLDER macro)
*/
GList *file_helper_get_all_songs() {
  GList *song_list = NULL;
  gint index_counter = 0;
  gchar *song_name = NULL;

  GList *song_folders = scan_and_sort_directory( SONGS_DIR );
  if( song_folders == NULL )
      {
      cem_add_to_log("List of songs is NULL",J_LOG_WARN);
      return NULL;
      }

  for( index_counter=0 ; index_counter < g_list_length(song_folders) ; index_counter++ )
      {
      gchar *element = (gchar *) g_list_nth_data( song_folders, index_counter );
      song_name = g_strdup_printf("%s/%s", SONGS_DIR,element);
      song_list = g_list_append( song_list,song_name);
      //printf("added to list: '%s'\n",song_name);
      }
  return song_list;
}

GList *file_helper_get_all_files(const char* directory) {
  GList *song_list = NULL;
  gint index_counter = 0;
  gchar *song_name = NULL;

  GList *song_folders = scan_and_sort_directory( directory );
  if( song_folders == NULL )
      {
      gchar* message = g_strdup_printf("Directory '%s' is empty, will return NULL",directory);
      cem_add_to_log(message,J_LOG_WARN);
      g_free(message);
      return NULL;
      }

  for( index_counter=0 ; index_counter < g_list_length(song_folders) ; index_counter++ )
      {
      gchar *element = (gchar *) g_list_nth_data( song_folders, index_counter );
      song_name = g_strdup_printf("%s/%s", directory,element);
      song_list = g_list_append( song_list,song_name);
      //printf("added to list: '%s'\n",song_name);
      }
  g_list_foreach(song_folders,(GFunc)g_free,NULL);
  return song_list;
}

GList *file_helper_get_all_files_without_path(const char* directory) {
  GList *song_list = NULL;
  gint index_counter = 0;
  gchar *song_name = NULL;

  GList *song_folders = scan_and_sort_directory( directory );
  if( song_folders == NULL )
      {
      gchar* message = g_strdup_printf("Directory '%s' is empty, will return NULL",directory);
      cem_add_to_log(message,J_LOG_WARN);
      g_free(message);
      return NULL;
      }

  for( index_counter=0 ; index_counter < g_list_length(song_folders) ; index_counter++ )
      {
      gchar *element = (gchar *) g_list_nth_data( song_folders, index_counter );
      song_name = g_strdup_printf("%s",element);
      song_list = g_list_append( song_list,song_name);
      //printf("added to list: '%s'\n",song_name);
      }
  g_list_foreach(song_folders,(GFunc)g_free,NULL);
  return song_list;
}
