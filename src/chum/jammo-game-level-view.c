/*
 * jammo-game-level-view.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "jammo-game-level-view.h"

G_DEFINE_TYPE(JammoGameLevelView, jammo_game_level_view, TANGLE_TYPE_WIDGET);

enum {
	PROP_0,
	PROP_GAME_LEVEL,
	PROP_INDICATOR_ACTOR
};

struct _JammoGameLevelViewPrivate {
	JammoGameLevel* game_level;
	ClutterActor* indicator_actor;

	guint games_completed;
	guint games_to_complete;
};

#define MARGIN_WIDTH 2

ClutterActor* jammo_game_level_view_new(JammoGameLevel* game_level, ClutterActor* indicator_actor) {

	return CLUTTER_ACTOR(g_object_new(JAMMO_TYPE_GAME_LEVEL_VIEW, "game-level", game_level, "indicator-actor", indicator_actor, NULL));
}

static void jammo_game_level_view_map(ClutterActor* actor) {
	JammoGameLevelView* game_level_view;
	
	game_level_view = JAMMO_GAME_LEVEL_VIEW(actor);

	CLUTTER_ACTOR_CLASS(jammo_game_level_view_parent_class)->map(actor);

	if (game_level_view->priv->indicator_actor) {
		clutter_actor_map(game_level_view->priv->indicator_actor);
	}
}

static void jammo_game_level_view_allocate(ClutterActor* actor, const ClutterActorBox* box, ClutterAllocationFlags flags) {
	JammoGameLevelView* game_level_view;
	guint games_completed;
	guint games_to_complete;
	gdouble fraction;
	gfloat height;
		
	game_level_view = JAMMO_GAME_LEVEL_VIEW(actor);
	
	CLUTTER_ACTOR_CLASS(jammo_game_level_view_parent_class)->allocate(actor, box, flags);

	games_completed = jammo_game_level_get_tasks_completed(game_level_view->priv->game_level);
	games_to_complete = jammo_game_level_get_tasks_to_complete(game_level_view->priv->game_level);
	
	if (games_completed != game_level_view->priv->games_completed ||
	    games_to_complete != game_level_view->priv->games_to_complete) {
		fraction = (gdouble)games_completed / games_to_complete;
		height = box->y2 - box->y1 - 2 * MARGIN_WIDTH;

		clutter_actor_set_x(game_level_view->priv->indicator_actor, MARGIN_WIDTH);
		clutter_actor_set_width(game_level_view->priv->indicator_actor, box->x2 - box->x1 - 2 * MARGIN_WIDTH);
		clutter_actor_animate(game_level_view->priv->indicator_actor, CLUTTER_EASE_OUT_BOUNCE, 1000,
				      "y", MARGIN_WIDTH + height - (height * fraction),
				      "height", height * fraction,
		                      NULL);

		game_level_view->priv->games_completed = games_completed;
		game_level_view->priv->games_to_complete = games_to_complete;
	}

}

static void jammo_game_level_view_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoGameLevelView* game_level_view;
	
	game_level_view = JAMMO_GAME_LEVEL_VIEW(object);

	switch (prop_id) {
		case PROP_GAME_LEVEL:
			if (g_value_get_object(value)) {
				game_level_view->priv->game_level = JAMMO_GAME_LEVEL(g_value_get_object(value));
				g_object_ref(game_level_view->priv->game_level);
				g_signal_connect_swapped(game_level_view->priv->game_level, "notify::tasks-completed", G_CALLBACK(clutter_actor_queue_relayout), game_level_view);
				g_signal_connect_swapped(game_level_view->priv->game_level, "notify::tasks-to-complete", G_CALLBACK(clutter_actor_queue_relayout), game_level_view);
			}
			break;
		case PROP_INDICATOR_ACTOR:
			if (g_value_get_object(value)) {
				game_level_view->priv->indicator_actor = CLUTTER_ACTOR(g_value_get_object(value));
				g_object_ref(game_level_view->priv->indicator_actor);
				clutter_container_add_actor(CLUTTER_CONTAINER(game_level_view), game_level_view->priv->indicator_actor);
			}
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_game_level_view_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoGameLevelView* game_level_view;

	game_level_view = JAMMO_GAME_LEVEL_VIEW(object);

        switch (prop_id) {
		case PROP_GAME_LEVEL:
			g_value_set_object(value, game_level_view->priv->game_level);
			break;
		case PROP_INDICATOR_ACTOR:
			g_value_set_object(value, game_level_view->priv->indicator_actor);
			break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static void jammo_game_level_view_finalize(GObject* object) {
	G_OBJECT_CLASS(jammo_game_level_view_parent_class)->finalize(object);
}

static void jammo_game_level_view_dispose(GObject* object) {
        JammoGameLevelView* game_level_view;

	game_level_view = JAMMO_GAME_LEVEL_VIEW(object);

	TANGLE_UNREF_AND_NULLIFY_OBJECT(game_level_view->priv->game_level);
	if (game_level_view->priv->indicator_actor) {
		clutter_container_remove_actor(CLUTTER_CONTAINER(game_level_view), game_level_view->priv->indicator_actor);
		g_object_unref(game_level_view->priv->indicator_actor);
		game_level_view->priv->indicator_actor = NULL;
	}

	G_OBJECT_CLASS(jammo_game_level_view_parent_class)->dispose(object);
}

static void jammo_game_level_view_class_init(JammoGameLevelViewClass* game_level_view_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(game_level_view_class);
	ClutterActorClass* actor_class = CLUTTER_ACTOR_CLASS(game_level_view_class);

	gobject_class->finalize = jammo_game_level_view_finalize;
	gobject_class->dispose = jammo_game_level_view_dispose;
	gobject_class->set_property = jammo_game_level_view_set_property;
	gobject_class->get_property = jammo_game_level_view_get_property;

	actor_class->map = jammo_game_level_view_map;
	actor_class->allocate = jammo_game_level_view_allocate;

	/**
	 * JammoGameLevelView:game-level:
	 *
	 * The #JammoGameLevel that is tracked.
	 */
	g_object_class_install_property(gobject_class, PROP_GAME_LEVEL,
	                                g_param_spec_object("game-level",
	                                "Game level",
	                                "The JammoGameLevel that is tracked",
	                                JAMMO_TYPE_GAME_LEVEL,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoGameLevelView:indicator-actor:
	 *
	 * The #ClutterActor that is used to show the current state.
	 */
	g_object_class_install_property(gobject_class, PROP_INDICATOR_ACTOR,
	                                g_param_spec_object("indicator-actor",
	                                "Indicator actor",
	                                "The ClutterActor that is used to show the current state",
	                                CLUTTER_TYPE_ACTOR,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	g_type_class_add_private(gobject_class, sizeof(JammoGameLevelViewPrivate));
}

static void jammo_game_level_view_init(JammoGameLevelView* game_level_view) {
	game_level_view->priv = G_TYPE_INSTANCE_GET_PRIVATE(game_level_view, JAMMO_TYPE_GAME_LEVEL_VIEW, JammoGameLevelViewPrivate);
}
