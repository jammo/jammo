/*
 * jammo-track-view.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "jammo-editing-track-view.h"
#include "jammo-sample-button.h"
#include "../meam/jammo-sample.h"
#include "../meam/jammo-meam.h"

#ifdef NETWORKING_ENABLED
#include "../gems/gems.h"
#endif

G_DEFINE_TYPE(JammoEditingTrackView, jammo_editing_track_view, TANGLE_TYPE_WIDGET);

enum {
	PROP_0,
	PROP_TRACK,
	PROP_N_SLOTS,
	PROP_SLOT_DURATION,
	PROP_SLOT_HEIGHT,
	PROP_SLOT_WIDTH,
	PROP_LINE_EVERY_NTH_SLOT,
	PROP_DISABLED_SLOTS_BEGIN,
	PROP_DISABLED_SLOTS_END,
	PROP_EDITING_ENABLED,
	PROP_SAMPLE_COLOR,
	PROP_HILIGHT_COLOR,
	PROP_SAMPLE_TYPE
};

enum {
	DROPPING_INCOMPATIBLE_SAMPLE,
	SAMPLE_ADDED,
	SAMPLE_REMOVED,
	LAST_SIGNAL
};

struct _JammoEditingTrackViewPrivate {
	JammoEditingTrack* track;
	guint n_slots;
	guint64 slot_duration;
	gfloat slot_width;
	gfloat slot_height;
	guint line_every_nth_slot;
	guint disabled_slots_begin;
	guint disabled_slots_end;
	ClutterColor sample_color;
	ClutterColor hilight_color;
	JammoSampleType sample_type;
	
	gfloat real_slot_width;
	gfloat hilighted_start;
	gfloat hilighted_end;
	TangleGrid* grid;
	
	guint editing_enabled : 1;
};

static guint signals[LAST_SIGNAL];

static const ClutterColor default_sample_color = { 0, 0, 0, 0 };
static const ClutterColor default_hilight_color = { 255, 255, 255, 100 };

static void on_actor_added(TangleWidget* widget, ClutterActor* actor, gpointer user_data);
static void on_actor_removed(TangleWidget* widget, ClutterActor* actor, gpointer user_data);
static gboolean check_if_overlapping(JammoEditingTrackView* track_view, ClutterActor* sample_button, guint slot_index);
static gboolean on_drag_begin_or_motion(TangleDropAction* drop_action, TangleDragAction* drag_action, gpointer user_data);
static gboolean on_drag_end(TangleDropAction* drop_action, TangleDragAction* drag_action, gpointer user_data);
static void on_dropped(TangleDropAction* drop_action, TangleDragAction* drag_action, gpointer user_data);
static void marshal_BOOLEAN__OBJECT (GClosure     *closure,
                                GValue       *return_value G_GNUC_UNUSED,
                                guint         n_param_values,
                                const GValue *param_values,
                                gpointer      invocation_hint G_GNUC_UNUSED,
                                gpointer      marshal_data);
static void marshal_VOID__INT_OBJECT (GClosure     *closure,
                          GValue       *return_value G_GNUC_UNUSED,
                          guint         n_param_values,
                          const GValue *param_values,
                          gpointer      invocation_hint G_GNUC_UNUSED,
                          gpointer      marshal_data);

ClutterActor* jammo_editing_track_view_new(JammoEditingTrack* track, guint n_slots, guint64 slot_duration, gfloat slot_width, gfloat slot_height) {

	//printf ("JAMMO_TYPE_EDITING_TRACK_VIEW, n-slots:%d, slot-duration:%Lu,  slot-width:%f, slot-height:%f\n", n_slots,slot_duration, slot_width, slot_height);
	return CLUTTER_ACTOR(g_object_new(JAMMO_TYPE_EDITING_TRACK_VIEW, "track", track, "n-slots", n_slots, "slot-duration", slot_duration, "slot-width", slot_width, "slot-height", slot_height, NULL));
}


static void on_sample_duration_notify(GObject* object, GParamSpec* pspec, gpointer user_data) {
	//printf("on_sample_duration_notify\n");
	TangleVault* vault;
	JammoSampleButton* sample_button;
	JammoEditingTrackView* track_view;
	guint slot;

	vault = TANGLE_VAULT(user_data);
	tangle_vault_get(vault, 3, JAMMO_TYPE_SAMPLE_BUTTON, &sample_button, JAMMO_TYPE_EDITING_TRACK_VIEW, &track_view, G_TYPE_UINT, &slot);

	clutter_actor_set_x(CLUTTER_ACTOR(sample_button),
          slot * (track_view->priv->real_slot_width ? track_view->priv->real_slot_width : track_view->priv->slot_width));
	clutter_actor_set_y(CLUTTER_ACTOR(sample_button), 0.0);
	clutter_actor_set_height(CLUTTER_ACTOR(sample_button), track_view->priv->slot_height);


	guint64 duration_of_sample = jammo_sample_get_duration(jammo_sample_button_get_sample(JAMMO_SAMPLE_BUTTON(sample_button)));
	guint n_slots = (guint) ((double)duration_of_sample / (double) track_view->priv->slot_duration + 0.5);
	clutter_actor_set_width(CLUTTER_ACTOR(sample_button), n_slots * track_view->priv->slot_width);

	//printf("duration_of_one_slot: %llu, duration_of_sample: %llu, n_slots: %d\n", duration_of_one_slot, duration_of_sample, n_slots);
	tangle_widget_set_background_color(TANGLE_WIDGET(sample_button), &track_view->priv->sample_color);

	tangle_widget_add(TANGLE_WIDGET(track_view), CLUTTER_ACTOR(sample_button), NULL);
	if (object)
		g_signal_handlers_disconnect_by_func(object, G_CALLBACK(on_sample_duration_notify), user_data);
}

// get_duration_of_slot returns the current duration of one slot. tempo is asked from jammo-editing-track
static guint64 get_duration_of_slot(JammoEditingTrack * editing_track) {
	guint64 duration_of_beat=0;
	gint beats_per_bar = jammo_editing_track_get_time_signature_beats(editing_track);
	//We want operate whole time with guint64.
	guint64 temp=60*beats_per_bar;
	guint64 one_second = 1000000000L;
	guint64 big = temp * one_second * jammo_editing_track_get_time_signature_note_value(editing_track);
	// 4.0 comes from bpm (x quarter notes in a minute)
	// 4 because there are four beat per bar
	duration_of_beat = ((gdouble)big / jammo_editing_track_get_tempo(editing_track) / 4.0 /4);
	return duration_of_beat;
}

/* Note! This function does not check if the given sample will be overlapping some other sample. */
void jammo_editing_track_view_add_jammo_sample_button(JammoEditingTrackView* track_view, JammoSampleButton* sample_button, guint slot) {
	g_return_if_fail(JAMMO_IS_EDITING_TRACK_VIEW(track_view));

	jammo_editing_track_add_sample(track_view->priv->track, jammo_sample_button_get_sample(sample_button), slot * get_duration_of_slot(track_view->priv->track));

	//We need to wait until duration of sample is calculated (we need it to adjust width)
	TangleVault* vault;

	vault = tangle_vault_new(3, JAMMO_TYPE_SAMPLE_BUTTON, sample_button, JAMMO_TYPE_EDITING_TRACK_VIEW, track_view, G_TYPE_UINT, slot);
	if (jammo_sample_get_duration(jammo_sample_button_get_sample(sample_button))==JAMMO_DURATION_INVALID) {
		tangle_signal_connect_vault(jammo_sample_button_get_sample(JAMMO_SAMPLE_BUTTON(sample_button)),
           "notify::duration", G_CALLBACK(on_sample_duration_notify), vault);
	}
	else {
		on_sample_duration_notify(NULL,NULL,vault);
	}
}

/*
Used when removing over network,
Or when backingtrack changing causes clipping.
*/
void jammo_editing_track_view_remove_jammo_sample_button_from_slot(JammoEditingTrackView* track_view, guint asked_slot) {
	gfloat asked_x = asked_slot * track_view->priv->slot_width;
	TangleActorIterator actor_iterator;
	ClutterActor* actor;

	gfloat x;  //left side of actor
	gfloat x2; //x2=x+width, right side of actor

	g_return_if_fail(JAMMO_IS_EDITING_TRACK_VIEW(track_view));
	
	for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(track_view), &actor_iterator); (actor = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
		x = clutter_actor_get_x(actor);
		x2 = x + clutter_actor_get_width(actor);
		if (x<= asked_x && asked_x < x2) {
			clutter_container_remove_actor(CLUTTER_CONTAINER(clutter_actor_get_parent(actor)), actor);
			break;
		}
	}
}

guint jammo_editing_track_view_get_sample_button_slot(JammoEditingTrackView* track_view, JammoSampleButton* sample_button) {
	g_return_val_if_fail(JAMMO_IS_EDITING_TRACK_VIEW(track_view), (guint)-1);
	g_return_val_if_fail(clutter_actor_get_parent(CLUTTER_ACTOR(sample_button)) == CLUTTER_ACTOR(track_view), (guint)-1);

	return (guint)(clutter_actor_get_x(CLUTTER_ACTOR(sample_button)) / track_view->priv->real_slot_width);
}

gboolean jammo_editing_track_view_get_editing_enabled(JammoEditingTrackView* track_view) {
	g_return_val_if_fail(JAMMO_IS_EDITING_TRACK_VIEW(track_view), FALSE);

	return track_view->priv->editing_enabled;
}

void jammo_editing_track_view_set_editing_enabled(JammoEditingTrackView* track_view, gboolean editing_enabled) {
	TangleActorIterator actor_iterator;
	ClutterActor* child;

	g_return_if_fail(JAMMO_IS_EDITING_TRACK_VIEW(track_view));

	if (track_view->priv->editing_enabled != editing_enabled) {
		track_view->priv->editing_enabled = editing_enabled;
		
		clutter_actor_meta_set_enabled(CLUTTER_ACTOR_META(tangle_actor_get_action_by_type(CLUTTER_ACTOR(track_view), TANGLE_TYPE_DROP_ACTION)), editing_enabled);
	
		for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(track_view), &actor_iterator); (child = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
			clutter_actor_meta_set_enabled(CLUTTER_ACTOR_META(tangle_actor_get_action_by_type(child, TANGLE_TYPE_DRAG_ACTION)), editing_enabled);
		}
		
		g_object_notify(G_OBJECT(track_view), "editing-enabled");
	}
}

JammoSampleType jammo_editing_track_view_get_sample_type(JammoEditingTrackView* track_view) {
	g_return_val_if_fail(JAMMO_IS_EDITING_TRACK_VIEW(track_view), JAMMO_SAMPLE_UNTYPED);

	return track_view->priv->sample_type;
}

void jammo_editing_track_view_set_sample_type(JammoEditingTrackView* track_view, JammoSampleType sample_type) {
	g_return_if_fail(JAMMO_IS_EDITING_TRACK_VIEW(track_view));

	if (track_view->priv->sample_type != sample_type) {
		track_view->priv->sample_type = sample_type;
		g_object_notify(G_OBJECT(track_view), "sample-type");
	}
}

static void jammo_editing_track_view_get_preferred_width(TangleActor* actor, gfloat for_height, gboolean interacting, gfloat* min_width_p, gfloat* natural_width_p, gfloat* max_width_p) {
	JammoEditingTrackView* track_view;
	
	track_view = JAMMO_EDITING_TRACK_VIEW(actor);
	
	if (min_width_p) {
		*min_width_p = track_view->priv->n_slots * track_view->priv->slot_width;
	}
	if (natural_width_p) {
		*natural_width_p = track_view->priv->n_slots * track_view->priv->slot_width;
	}
	if (max_width_p) {
		*max_width_p = 0.0;
	}
}

static void jammo_editing_track_view_get_preferred_height(TangleActor* actor, gfloat for_width, gboolean interacting, gfloat* min_height_p, gfloat* natural_height_p, gfloat* max_height_p) {
	JammoEditingTrackView* track_view;

	track_view = JAMMO_EDITING_TRACK_VIEW(actor);

	if (min_height_p) {
		*min_height_p = track_view->priv->slot_height;
	}
	if (natural_height_p) {
		*natural_height_p = track_view->priv->slot_height;
	}
	if (max_height_p) {
		*max_height_p = track_view->priv->slot_height;
	}
}

static void jammo_editing_track_view_allocate(ClutterActor* actor, const ClutterActorBox* box, ClutterAllocationFlags flags) {
	JammoEditingTrackView* track_view;

	track_view = JAMMO_EDITING_TRACK_VIEW(actor);

	CLUTTER_ACTOR_CLASS(jammo_editing_track_view_parent_class)->allocate(actor, box, flags);
	
	track_view->priv->real_slot_width = (box->x2 - box->x1) / track_view->priv->n_slots;

	if (track_view->priv->line_every_nth_slot) {
		tangle_grid_set_grid_spacing_x(track_view->priv->grid, track_view->priv->real_slot_width * track_view->priv->line_every_nth_slot - (1.0 / track_view->priv->n_slots));
		tangle_grid_set_grid_spacing_y(track_view->priv->grid, box->y2 - box->y1 - 1.0);
	} else {
		tangle_grid_set_grid_spacing_x(track_view->priv->grid, 0.0);
		tangle_grid_set_grid_spacing_y(track_view->priv->grid, 0.0);
	}
}

static void jammo_editing_track_view_paint(ClutterActor* actor) {
	JammoEditingTrackView* track_view;
	gdouble scale_x;
	gdouble scale_y;
	guint8 alpha;
	ClutterActorBox box;

	track_view = JAMMO_EDITING_TRACK_VIEW(actor);

	if (CLUTTER_ACTOR_IS_MAPPED(actor)) {
		CLUTTER_ACTOR_CLASS(jammo_editing_track_view_parent_class)->paint(actor);

		if (track_view->priv->hilighted_start != track_view->priv->hilighted_end) {
			cogl_push_matrix();

			g_object_get(actor, "transition-scale-x", &scale_x, "transition-scale-y", &scale_y, NULL);
			cogl_scale(scale_x, scale_y, 0);

			alpha = clutter_actor_get_paint_opacity(actor) * track_view->priv->hilight_color.alpha / 255;
			cogl_set_source_color4ub(track_view->priv->hilight_color.red,
			 			 track_view->priv->hilight_color.green,
			 			 track_view->priv->hilight_color.blue,
						 alpha);
			tangle_actor_get_aligned_allocation(TANGLE_ACTOR(actor), &box);
			cogl_rectangle(box.x1 + track_view->priv->hilighted_start, box.y1, box.x1 + track_view->priv->hilighted_end, box.y2);

			cogl_pop_matrix();
		}
	}
}

static void jammo_editing_track_view_constructed(GObject* object) {
	JammoEditingTrackView* track_view;
	ClutterAction* drop_action;

	track_view = JAMMO_EDITING_TRACK_VIEW(object);
	
	g_signal_connect(track_view, "actor-added", G_CALLBACK(on_actor_added), NULL);
	g_signal_connect(track_view, "actor-removed", G_CALLBACK(on_actor_removed), NULL);
	
	track_view->priv->grid = TANGLE_GRID(tangle_grid_new(0.0, 0.0));
	tangle_widget_set_background_actor(TANGLE_WIDGET(object), CLUTTER_ACTOR(track_view->priv->grid));

	drop_action = tangle_drop_action_new();
	g_signal_connect(drop_action, "drag-begin", G_CALLBACK(on_drag_begin_or_motion), track_view);
	g_signal_connect(drop_action, "drag-motion", G_CALLBACK(on_drag_begin_or_motion), track_view);
	g_signal_connect(drop_action, "drag-end", G_CALLBACK(on_drag_end), track_view);
	g_signal_connect(drop_action, "dropped", G_CALLBACK(on_dropped), track_view);
	clutter_actor_add_action(CLUTTER_ACTOR(track_view), drop_action);

	/* //Checking number of slots and duration of slots on underlying editing-track
	JammoEditingTrack* track=track_view->priv->track;
	if (track) {
		guint n_slots = track_view->priv->n_slots;
		guint64 duration = jammo_editing_track_get_fixed_duration(track);
		printf("n_slots %d, duration %llu\n",n_slots,duration);
	}
	*/
}

static void jammo_editing_track_view_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoEditingTrackView* track_view;
	
	track_view = JAMMO_EDITING_TRACK_VIEW(object);

	switch (prop_id) {
		case PROP_TRACK:
			track_view->priv->track = JAMMO_EDITING_TRACK(g_value_get_object(value));
			g_object_ref(track_view->priv->track);
			break;
		case PROP_N_SLOTS:
			track_view->priv->n_slots = g_value_get_uint(value);
			break;
		case PROP_SLOT_DURATION:
			track_view->priv->slot_duration = g_value_get_uint64(value);
			break;
		case PROP_SLOT_HEIGHT:
			track_view->priv->slot_height = g_value_get_float(value);
			clutter_actor_queue_relayout(CLUTTER_ACTOR(track_view));
			break;
		case PROP_SLOT_WIDTH:
			track_view->priv->slot_width = g_value_get_float(value);
			clutter_actor_queue_relayout(CLUTTER_ACTOR(track_view));
			break;
		case PROP_LINE_EVERY_NTH_SLOT:
			track_view->priv->line_every_nth_slot = g_value_get_uint(value);
			clutter_actor_queue_relayout(CLUTTER_ACTOR(track_view));
			break;
		case PROP_DISABLED_SLOTS_BEGIN:
			track_view->priv->disabled_slots_begin = g_value_get_uint(value);
			break;
		case PROP_DISABLED_SLOTS_END:
			track_view->priv->disabled_slots_end = g_value_get_uint(value);
			break;
		case PROP_EDITING_ENABLED:
			jammo_editing_track_view_set_editing_enabled(track_view, g_value_get_boolean(value));
			break;
		case PROP_SAMPLE_COLOR:
			track_view->priv->sample_color = *clutter_value_get_color(value);
			break;
		case PROP_HILIGHT_COLOR:
			track_view->priv->hilight_color = *clutter_value_get_color(value);
			break;
		case PROP_SAMPLE_TYPE:
			track_view->priv->sample_type = g_value_get_enum(value);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_editing_track_view_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoEditingTrackView* track_view;

	track_view = JAMMO_EDITING_TRACK_VIEW(object);

        switch (prop_id) {
		case PROP_TRACK:
			g_value_set_object(value, track_view->priv->track);
			break;
		case PROP_N_SLOTS:
			g_value_set_uint(value, track_view->priv->n_slots);
			break;
		case PROP_SLOT_DURATION:
			g_value_set_uint64(value, track_view->priv->slot_duration);
			break;
		case PROP_SLOT_HEIGHT:
			g_value_set_float(value, track_view->priv->slot_height);
			break;
		case PROP_SLOT_WIDTH:
			g_value_set_float(value, (track_view->priv->real_slot_width ? track_view->priv->real_slot_width : track_view->priv->slot_width));
			break;
		case PROP_LINE_EVERY_NTH_SLOT:
			g_value_set_uint(value, track_view->priv->line_every_nth_slot);
			break;
		case PROP_DISABLED_SLOTS_BEGIN:
			g_value_set_uint(value, track_view->priv->disabled_slots_begin);
			break;
		case PROP_DISABLED_SLOTS_END:
			g_value_set_uint(value, track_view->priv->disabled_slots_end);
			break;
		case PROP_EDITING_ENABLED:
			g_value_set_boolean(value, track_view->priv->editing_enabled);
			break;
		case PROP_SAMPLE_COLOR:
			clutter_value_set_color(value, &track_view->priv->sample_color);
			break;
		case PROP_HILIGHT_COLOR:
			clutter_value_set_color(value, &track_view->priv->hilight_color);
			break;
		case PROP_SAMPLE_TYPE:
			g_value_set_enum(value, track_view->priv->sample_type);
			break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static void jammo_editing_track_view_finalize(GObject* object) {
	G_OBJECT_CLASS(jammo_editing_track_view_parent_class)->finalize(object);
}

static void jammo_editing_track_view_dispose(GObject* object) {
	JammoEditingTrackView* editing_track_view;
	
	editing_track_view = JAMMO_EDITING_TRACK_VIEW(object);
	
	if (editing_track_view->priv->track) {
		g_object_unref(editing_track_view->priv->track);
		editing_track_view->priv->track = NULL;
	}
	//When editing-track-view-widget is removed, all it's children is also removed
	//And they are sending 'on_actor_removed'-signal. (but priv->track is already deleted)
	g_signal_handlers_disconnect_by_func(editing_track_view,on_actor_removed,NULL);
	G_OBJECT_CLASS(jammo_editing_track_view_parent_class)->dispose(object);
}

static void jammo_editing_track_view_class_init(JammoEditingTrackViewClass* track_view_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(track_view_class);
	ClutterActorClass* clutter_actor_class = CLUTTER_ACTOR_CLASS(track_view_class);
	TangleActorClass* actor_class = TANGLE_ACTOR_CLASS(track_view_class);

	gobject_class->constructed = jammo_editing_track_view_constructed;
	gobject_class->finalize = jammo_editing_track_view_finalize;
	gobject_class->dispose = jammo_editing_track_view_dispose;
	gobject_class->set_property = jammo_editing_track_view_set_property;
	gobject_class->get_property = jammo_editing_track_view_get_property;
	
	clutter_actor_class->paint = jammo_editing_track_view_paint;
	clutter_actor_class->allocate = jammo_editing_track_view_allocate;
	
	actor_class->get_preferred_width = jammo_editing_track_view_get_preferred_width;
	actor_class->get_preferred_height = jammo_editing_track_view_get_preferred_height;
	
	/**
	 * JammoEditingTrackView:track:
	 *
	 * The track of this view.
	 */
	g_object_class_install_property(gobject_class, PROP_TRACK,
	                                g_param_spec_object("track",
	                                "Track",
	                                "The track of this view",
	                                JAMMO_TYPE_EDITING_TRACK,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoEditingTrackView:n-slots:
	 *
	 * The number of slots.
	 */
	g_object_class_install_property(gobject_class, PROP_N_SLOTS,
	                                g_param_spec_uint("n-slots",
	                                "N slots",
	                                "The number of slots",
	                                0, G_MAXUINT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoEditingTrackView:slot-duration:
	 *
	 * The duration of a slot in nanoseconds.
	 */
	g_object_class_install_property(gobject_class, PROP_SLOT_DURATION,
	                                g_param_spec_uint64("slot-duration",
	                                "Slot duration",
	                                "The duration of a slot in nanoseconds",
	                                0, G_MAXUINT64, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoEditingTrackView:slot-height:
	 *
	 * The height of the slot in the screen (pixels, Clutter units).
	 */
	g_object_class_install_property(gobject_class, PROP_SLOT_HEIGHT,
	                                g_param_spec_float("slot-height",
	                                "Slot height",
	                                "The height of the slot in the screen (pixels, Clutter units)",
	                                0, G_MAXFLOAT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoEditingTrackView:slot-width:
	 *
	 * The width of the slot in the screen (pixels, Clutter units).
	 */
	g_object_class_install_property(gobject_class, PROP_SLOT_WIDTH,
	                                g_param_spec_float("slot-width",
	                                "Slot width",
	                                "The width of the slot in the screen (pixels, Clutter units)",
	                                0, G_MAXFLOAT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoEditingTrackView:line-every-nth-slot:
	 *
	 * The number of slots after a separator line is drawn (0 to disable).
	 */
	g_object_class_install_property(gobject_class, PROP_LINE_EVERY_NTH_SLOT,
	                                g_param_spec_uint("line-every-nth-slot",
	                                "Line every nth slot",
	                                "The number of slots after a separator line is drawn (0 to disable)",
	                                0, G_MAXUINT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoEditingTrackView:disabled-slots-begin:
	 *
	 * The number of slots that are disabled (in terms of dropping) from the beginning of the track.
	 */
	g_object_class_install_property(gobject_class, PROP_DISABLED_SLOTS_BEGIN,
	                                g_param_spec_uint("disabled-slots-begin",
	                                "Disabled slots begin",
	                                "The number of slots that are disabled (in terms of dropping) from the beginning of the track",
	                                0, G_MAXUINT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoEditingTrackView:disabled-slots-end:
	 *
	 * The number of slots that are disabled (in terms of dropping) from the end of the track.
	 */
	g_object_class_install_property(gobject_class, PROP_DISABLED_SLOTS_END,
	                                g_param_spec_uint("disabled-slots-end",
	                                "Disabled slots end",
	                                "The number of slots that are disabled (in terms of dropping) from the end of the track",
	                                0, G_MAXUINT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoEditingTrackView:editing enabled:
	 *
	 * Whether the track can be edited or not.
	 */
	g_object_class_install_property(gobject_class, PROP_EDITING_ENABLED,
	                                g_param_spec_boolean("editing-enabled",
	                                "Editing enabled",
	                                "Whether the track can be edited or not",
	                                TRUE,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoEditingTrackView:sample-color:
	 *
	 * A color that is used as a background color of dropped samples.
	 */
	g_object_class_install_property(gobject_class, PROP_SAMPLE_COLOR,
	                                clutter_param_spec_color("sample-color",
	                                "Sample color",
	                                "A color that is used as a background color of dropped samples",
	                                &default_sample_color,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoEditingTrackView:hilight-color:
	 *
	 * A color that is used to hilight selected slots when dragging samples.
	 */
	g_object_class_install_property(gobject_class, PROP_HILIGHT_COLOR,
	                                clutter_param_spec_color("hilight-color",
	                                "Hilight color",
	                                "A color that is used to hilight selected slots when dragging samples",
	                                &default_hilight_color,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoEditingTrackView:sample-type:
	 *
	 * The type of samples that are allowed to be dropped into the track. If a #JammoEditingTrackView or a #JammoSampleButton has :sample-type
	 * set to JAMMO_SAMPLE_UNTYPE dropping is always allowed.
	 */
	g_object_class_install_property(gobject_class, PROP_SAMPLE_TYPE,
	                                g_param_spec_enum("sample-type",
	                                "Sample type",
	                                "The type of samples that are allowed to be dropped into the track",
	                                JAMMO_TYPE_SAMPLE_TYPE, JAMMO_SAMPLE_UNTYPED,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	/**
	 * JammoEditingTrackView:dropping-incompatible-sample:
	 * @sample_button: the #JammoSampleButton that is being dropped and is incompatible in terms of #JammoSampleType.
	 *
	 * The :dropping-incompatible-sample signal is being emitted when an user tries to drag (and drop) a sample button
	 * that has a different #JammoSampleType than the track (see :sample-type). If either has JAMMO_SAMPLE_UNTYPED,
	 * the compatability check is not being performed.
	 *
	 * The signal handler should return FALSE, if the dropping should fail (in that case, also other handlers are
	 * being called), or TRUE, if the dropping should be possible (no other handlers are being called after that).
	 */
	signals[DROPPING_INCOMPATIBLE_SAMPLE] = g_signal_new("dropping-incompatible-sample", G_TYPE_FROM_CLASS(gobject_class),
	                                		     G_SIGNAL_RUN_LAST, 0,
							     g_signal_accumulator_true_handled, NULL,
							     marshal_BOOLEAN__OBJECT,
							     G_TYPE_BOOLEAN, 1,
							     JAMMO_TYPE_SAMPLE_BUTTON);

		signals[SAMPLE_ADDED] = g_signal_new("sample-added", G_TYPE_FROM_CLASS(gobject_class),
                                G_SIGNAL_RUN_LAST, 0,
                                NULL, NULL,
                                marshal_VOID__INT_OBJECT,
                                G_TYPE_NONE, //return type of handler-function
                                2, //Number of parameters
                                G_TYPE_INT, //type of 1st parameter (slot)
                                JAMMO_TYPE_SAMPLE_BUTTON);  //type of 2nd parameter


		signals[SAMPLE_REMOVED] = g_signal_new("sample-removed", G_TYPE_FROM_CLASS(gobject_class),
                                G_SIGNAL_RUN_LAST, 0,
                                NULL, NULL,
                                marshal_VOID__INT_OBJECT,
                                G_TYPE_NONE, //return type of handler-function
                                2, //Number of parameters
                                G_TYPE_INT, //type of 1st parameter (slot)
                                JAMMO_TYPE_SAMPLE_BUTTON);  //type of 2nd parameter


	g_type_class_add_private (gobject_class, sizeof (JammoEditingTrackViewPrivate));
}

static void jammo_editing_track_view_init(JammoEditingTrackView* track_view) {
	track_view->priv = G_TYPE_INSTANCE_GET_PRIVATE(track_view, JAMMO_TYPE_EDITING_TRACK_VIEW, JammoEditingTrackViewPrivate);
	track_view->priv->editing_enabled = TRUE;

	track_view->priv->sample_color = default_sample_color;
	track_view->priv->hilight_color = default_hilight_color;

	track_view->priv->hilighted_start = track_view->priv->hilighted_end = -1.0;
}

static void on_actor_added(TangleWidget* widget, ClutterActor* actor, gpointer user_data) {
//	JammoEditingTrackView* track_view;
	JammoSampleButton* sample_button;
	JammoSample* sample;

	g_return_if_fail(JAMMO_IS_SAMPLE_BUTTON(actor));
	
//	track_view = JAMMO_EDITING_TRACK_VIEW(widget);
	sample_button = JAMMO_SAMPLE_BUTTON(actor);
	sample = jammo_sample_button_get_sample(sample_button);
	
	g_assert(jammo_sample_get_editing_track(sample));
}

static void on_actor_removed(TangleWidget* widget, ClutterActor* actor, gpointer user_data) {
	JammoEditingTrackView* track_view;
	JammoSampleButton* sample_button;

	g_return_if_fail(JAMMO_IS_SAMPLE_BUTTON(actor));
	
	track_view = JAMMO_EDITING_TRACK_VIEW(widget);
	sample_button = JAMMO_SAMPLE_BUTTON(actor);

	guint slot = clutter_actor_get_x(actor) / track_view->priv->slot_width;
	g_signal_emit(track_view, signals[SAMPLE_REMOVED], 0, slot,sample_button);

	jammo_editing_track_remove_sample(track_view->priv->track, jammo_sample_button_get_sample(sample_button));

	#ifdef NETWORKING_ENABLED
	//If editing is not allowed, this actor was removed by network and should not been informed to gems
	if (track_view->priv->editing_enabled) {
		gems_remove_sample_from_slot(slot);
	}
	#endif /* NETWORKING_ENABLED */
}

static gboolean check_if_overlapping(JammoEditingTrackView* track_view, ClutterActor* sample_button, guint slot_index) {
	guint n_slots;
	TangleActorIterator actor_iterator;
	ClutterActor* actor;
	gfloat x;
	
	guint64 duration_of_one_slot= track_view->priv->slot_duration;
	guint64 duration_of_sample = jammo_sample_get_duration(jammo_sample_button_get_sample(JAMMO_SAMPLE_BUTTON(sample_button)));
	n_slots = (guint) ((double)duration_of_sample / (double) duration_of_one_slot + 0.5);

	
	for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(track_view), &actor_iterator); (actor = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
		x = clutter_actor_get_x(actor);
		if (actor != sample_button &&
		    x < slot_index * track_view->priv->real_slot_width &&
		    x + clutter_actor_get_width(actor) > slot_index * track_view->priv->real_slot_width) {
		    n_slots += slot_index - (guint)(x / track_view->priv->real_slot_width);
			slot_index = (guint)(x / track_view->priv->real_slot_width);
			break;
		}
		actor = NULL;
	}
	if (!actor) {
		for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(track_view), &actor_iterator); (actor = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
			x = clutter_actor_get_x(actor);
			if (actor != sample_button &&
			    x >= slot_index * track_view->priv->real_slot_width &&
			    x <= (slot_index + n_slots - 1) * track_view->priv->real_slot_width) {
				n_slots -= (guint)(x / track_view->priv->real_slot_width) - slot_index;
				break;
			}
			actor = NULL;
		}
	}
	
	return actor != NULL;
}

static gboolean is_droppable(JammoEditingTrackView* track_view, TangleDragAction* drag_action, guint* slot_return, guint* n_slots_return) {
	gboolean droppable = FALSE;
	ClutterActor* meta_actor;
	ClutterActor* drag_actor;
	gfloat x, y, w, h;
	gfloat center_x, center_y;
	
	meta_actor = clutter_actor_meta_get_actor(CLUTTER_ACTOR_META(drag_action));
	drag_actor = tangle_drag_action_get_drag_actor(drag_action);;
	if (JAMMO_IS_SAMPLE_BUTTON(meta_actor)) {
		clutter_actor_get_transformed_position(drag_actor, &x, &y);
		clutter_actor_get_size(drag_actor, &w, &h);
		if (clutter_actor_transform_stage_point(CLUTTER_ACTOR(track_view), x + w / 2, y + h / 2, &center_x, &center_y)) {

			guint64 duration_of_one_slot= track_view->priv->slot_duration;
			guint64 duration_of_sample = jammo_sample_get_duration(jammo_sample_button_get_sample(JAMMO_SAMPLE_BUTTON(meta_actor)));
			*n_slots_return = (guint) ((double)duration_of_sample / (double) duration_of_one_slot + 0.5);

			center_x -= (*n_slots_return - 1) * track_view->priv->real_slot_width / 2;
			*slot_return = center_x / track_view->priv->real_slot_width;

			//slot_return is guint, it is possible it is 'negative', which then means 'something big'
			if (*slot_return > 100000) {
				return FALSE;
			}

			//Check 'rounding' to the first beat of the bar
			//non-waltz-samples is always rounded
			//waltz-samples is not rounded if previous slot is occupied
			if (!(jammo_sample_button_is_waltz(JAMMO_SAMPLE_BUTTON(meta_actor))  &&
			      check_if_overlapping(track_view, CLUTTER_ACTOR(meta_actor), *slot_return-1) ))
				{
				if (*slot_return%4 == 1)
					*slot_return-=1;
				else if (*slot_return%4 == 2)
					*slot_return+=2;
				else if (*slot_return%4 == 3)
					*slot_return+=1;
				}

			if (*slot_return >= track_view->priv->disabled_slots_begin &&
			    *slot_return + *n_slots_return - 1 < track_view->priv->n_slots - track_view->priv->disabled_slots_end &&
			    !check_if_overlapping(track_view, meta_actor, *slot_return)) {
				droppable = TRUE;
			}

		}
	}
	
	return droppable;	
}

static gboolean on_drag_begin_or_motion(TangleDropAction* drop_action, TangleDragAction* drag_action, gpointer user_data) {
	JammoEditingTrackView* track_view;
	guint slot, n_slots;
	gfloat start;

	track_view = JAMMO_EDITING_TRACK_VIEW(user_data);
	
	
	if (is_droppable(track_view, drag_action, &slot, &n_slots)) {
		start = slot * track_view->priv->real_slot_width;
		if (start != track_view->priv->hilighted_start) {
			track_view->priv->hilighted_start = start;
			track_view->priv->hilighted_end = start + n_slots * track_view->priv->real_slot_width;
			clutter_actor_queue_redraw(CLUTTER_ACTOR(track_view));
		}
	} else if (track_view->priv->hilighted_start != track_view->priv->hilighted_end) {
		track_view->priv->hilighted_start = track_view->priv->hilighted_end = 0.0;
		clutter_actor_queue_redraw(CLUTTER_ACTOR(track_view));
	}
	    		
	return FALSE;
}

static gboolean on_drag_end(TangleDropAction* drop_action, TangleDragAction* drag_action, gpointer user_data) {	
	JammoEditingTrackView* track_view;
	guint slot, n_slots;

	track_view = JAMMO_EDITING_TRACK_VIEW(user_data);

	track_view->priv->hilighted_start = track_view->priv->hilighted_end = -1.0;

	return !is_droppable(track_view, drag_action, &slot, &n_slots);
}

static void on_dropped(TangleDropAction* drop_action, TangleDragAction* drag_action, gpointer user_data) {
	JammoEditingTrackView* track_view;
	gboolean accepted;
	ClutterActor* meta_actor;
	ClutterActor* drag_actor;
	gfloat actor_x, actor_y, w, h;
	gfloat x, y, center_x, center_y;
	JammoSampleButton* sample_button;
	ClutterActor* copy_of_sample_button;
	guint slot;
	guint n_slots;
	guint64 duration_of_one_slot;
	guint64 duration_of_sample;

	track_view = JAMMO_EDITING_TRACK_VIEW(user_data);
	
	accepted = TRUE;
	meta_actor = clutter_actor_meta_get_actor(CLUTTER_ACTOR_META(drag_action));
	drag_actor = tangle_drag_action_get_drag_actor(drag_action);
	clutter_actor_get_transformed_position(drag_actor, &actor_x, &actor_y);
	clutter_actor_get_size(drag_actor, &w, &h);
	if (clutter_actor_transform_stage_point(CLUTTER_ACTOR(track_view), actor_x, actor_y, &x, &y) &&
	    clutter_actor_transform_stage_point(CLUTTER_ACTOR(track_view), actor_x + w / 2, actor_y + h / 2, &center_x, &center_y)) {

		//Default=types are not strict.
		//Signal-listener can return FALSE -> then dropping incompatible is prohibited
		if (track_view->priv->sample_type != JAMMO_SAMPLE_UNTYPED &&
		     jammo_sample_button_get_sample_type(JAMMO_SAMPLE_BUTTON(meta_actor)) != track_view->priv->sample_type &&
		     jammo_sample_button_get_sample_type(JAMMO_SAMPLE_BUTTON(meta_actor)) != JAMMO_SAMPLE_UNTYPED) {
			accepted = TRUE;
			g_signal_emit(track_view, signals[DROPPING_INCOMPATIBLE_SAMPLE], 0, meta_actor, &accepted);
		}
	}

	if (accepted) {
		sample_button = JAMMO_SAMPLE_BUTTON(meta_actor);
		copy_of_sample_button = jammo_sample_button_new_from_existing(sample_button);

		tangle_widget_set_background_color(TANGLE_WIDGET(copy_of_sample_button), &track_view->priv->sample_color);

		duration_of_one_slot = track_view->priv->slot_duration;
		duration_of_sample = jammo_sample_get_duration(jammo_sample_button_get_sample(JAMMO_SAMPLE_BUTTON(meta_actor)));
		n_slots = (guint) ((double)duration_of_sample / (double) duration_of_one_slot + 0.5);

		center_x -= (n_slots - 1) * track_view->priv->real_slot_width / 2;
		slot = center_x / track_view->priv->real_slot_width;


		//Check 'rounding' to the first beat of the bar
		//non-waltz-samples is always rounded
		//waltz-samples is not rounded if previous slot is occupied
		if (!(jammo_sample_button_is_waltz(JAMMO_SAMPLE_BUTTON(sample_button))  &&
		    check_if_overlapping(track_view, CLUTTER_ACTOR(meta_actor), slot-1) ))
			{
				if (slot%4 == 1)
					slot-=1;
				else if (slot%4 == 2)
					slot+=2;
				else if (slot%4 == 3)
					slot+=1;
			}


		jammo_editing_track_add_sample(track_view->priv->track, jammo_sample_button_get_sample(JAMMO_SAMPLE_BUTTON(copy_of_sample_button)), slot * get_duration_of_slot(track_view->priv->track));

		/* tangle_draggable_actor_set_floating_mode_dragged(TANGLE_DRAGGABLE_ACTOR(copy_of_sample_button), TANGLE_FLOATING_MODE_COLLAPSE); */
		clutter_actor_set_x(copy_of_sample_button, actor_x);
		clutter_actor_set_y(copy_of_sample_button, actor_y);
		clutter_actor_set_height(copy_of_sample_button, track_view->priv->slot_height);
		clutter_actor_set_width(copy_of_sample_button, n_slots * track_view->priv->slot_width);
		tangle_widget_add(TANGLE_WIDGET(track_view), copy_of_sample_button, NULL);

		clutter_actor_animate(copy_of_sample_button, CLUTTER_EASE_OUT_CUBIC, 300, "x", slot * track_view->priv->real_slot_width, "y", 0.0, NULL);

		g_signal_emit(track_view, signals[SAMPLE_ADDED], 0, slot,copy_of_sample_button);
		jammo_sample_button_on_dropped(sample_button);
		#ifdef NETWORKING_ENABLED
		gems_add_new_sample_to_track(jammo_sample_button_get_loop_id(sample_button), slot); //FIXME: remove this: signal emitting is enough
		#endif /* NETWORKING_ENABLED */
	}
}


/* Generated marshaler */

#ifdef G_ENABLE_DEBUG
#define g_marshal_value_peek_object(v)   g_value_get_object (v)
#define g_marshal_value_peek_int(v)      g_value_get_int (v)
#else /* !G_ENABLE_DEBUG */
/* WARNING: This code accesses GValues directly, which is UNSUPPORTED API.
 *          Do not access GValues directly in your code. Instead, use the
 *          g_value_get_*() functions
 */
#define g_marshal_value_peek_object(v)   (v)->data[0].v_pointer
#define g_marshal_value_peek_int(v)      (v)->data[0].v_int
#endif /* !G_ENABLE_DEBUG */

void marshal_BOOLEAN__OBJECT (GClosure     *closure,
                              GValue       *return_value G_GNUC_UNUSED,
                              guint         n_param_values,
                              const GValue *param_values,
                              gpointer      invocation_hint G_GNUC_UNUSED,
                              gpointer      marshal_data)
{
  typedef gboolean (*GMarshalFunc_BOOLEAN__OBJECT) (gpointer     data1,
                                                    gpointer     arg_1,
                                                    gpointer     data2);
  register GMarshalFunc_BOOLEAN__OBJECT callback;
  register GCClosure *cc = (GCClosure*) closure;
  register gpointer data1, data2;
  gboolean v_return;

  g_return_if_fail (return_value != NULL);
  g_return_if_fail (n_param_values == 2);

  if (G_CCLOSURE_SWAP_DATA (closure))
    {
      data1 = closure->data;
      data2 = g_value_peek_pointer (param_values + 0);
    }
  else
    {
      data1 = g_value_peek_pointer (param_values + 0);
      data2 = closure->data;
    }
  callback = (GMarshalFunc_BOOLEAN__OBJECT) (marshal_data ? marshal_data : cc->callback);

  v_return = callback (data1,
                       g_marshal_value_peek_object (param_values + 1),
                       data2);

  g_value_set_boolean (return_value, v_return);
}

//Generated with: echo "VOID:INT,OBJECT" | glib-genmarshal --body --prefix=marshal
void marshal_VOID__INT_OBJECT (GClosure     *closure,
                          GValue       *return_value G_GNUC_UNUSED,
                          guint         n_param_values,
                          const GValue *param_values,
                          gpointer      invocation_hint G_GNUC_UNUSED,
                          gpointer      marshal_data)
{
  typedef void (*GMarshalFunc_VOID__INT_OBJECT) (gpointer     data1,
                                                 gint         arg_1,
                                                 gpointer     arg_2,
                                                 gpointer     data2);
  register GMarshalFunc_VOID__INT_OBJECT callback;
  register GCClosure *cc = (GCClosure*) closure;
  register gpointer data1, data2;

  g_return_if_fail (n_param_values == 3);

  if (G_CCLOSURE_SWAP_DATA (closure))
    {
      data1 = closure->data;
      data2 = g_value_peek_pointer (param_values + 0);
    }
  else
    {
      data1 = g_value_peek_pointer (param_values + 0);
      data2 = closure->data;
    }
  callback = (GMarshalFunc_VOID__INT_OBJECT) (marshal_data ? marshal_data : cc->callback);

  callback (data1,
            g_marshal_value_peek_int (param_values + 1),
            g_marshal_value_peek_object (param_values + 2),
            data2);
}
