/*
 * jammo-game-level.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "jammo-game-level.h"
#include "jammo-game-task.h"
#include "../cem/cem.h"
#include <string.h>

static void clutter_scriptable_iface_init(ClutterScriptableIface* iface);

G_DEFINE_TYPE_WITH_CODE(JammoGameLevel, jammo_game_level, TANGLE_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(CLUTTER_TYPE_SCRIPTABLE, clutter_scriptable_iface_init););

enum {
	PROP_0,
	PROP_TASKS_COMPLETED,
	PROP_TASKS_TO_COMPLETE
};

enum {
	COMPLETED,
	LAST_SIGNAL
};
typedef struct _TaskData {
	gchar* name;
	
	guint completed : 1;
} TaskData;

struct _JammoGameLevelPrivate {
	GList* task_datas;
	
	TaskData* active_task_data;
	JammoGameTask* active_task;
	ClutterScript* script;
	guint script_id;
};

static guint signals[LAST_SIGNAL] = { 0 };
static ClutterScriptableIface* parent_scriptable_iface = NULL;

static gint task_data_compare_name(TaskData* task_data, const gchar* name);
static void free_active_task(JammoGameLevel* game_level);
static void on_task_completed(JammoGameTask* task, gpointer user_data);

JammoGameLevel* jammo_game_level_new() {
	
	return JAMMO_GAME_LEVEL(g_object_new(JAMMO_TYPE_GAME_LEVEL, NULL));
}

GList* jammo_game_level_get_task_names(JammoGameLevel* game_level) {
	GList* task_names = NULL;
	GList* task_data_in_list;
	TaskData* task_data;
	
	g_return_val_if_fail(JAMMO_IS_GAME_LEVEL(game_level), NULL);

	for (task_data_in_list = game_level->priv->task_datas; task_data_in_list; task_data_in_list = task_data_in_list->next) {
		task_data = (TaskData*)task_data_in_list->data;
		task_names = g_list_prepend(task_names, task_data->name);
	}
	
	return task_names;
}

void jammo_game_level_add_task_name(JammoGameLevel* game_level, const gchar* task_name) {
	TaskData* task_data;
	
	g_return_if_fail(JAMMO_IS_GAME_LEVEL(game_level));

	if (g_list_find_custom(game_level->priv->task_datas, task_name, (GCompareFunc)task_data_compare_name)) {
		g_warning("A task name '%s' already exists in the game level.\n", task_name);
	} else {
		task_data = g_new0(TaskData, 1);
		task_data->name = g_strdup(task_name);
		game_level->priv->task_datas = g_list_prepend(game_level->priv->task_datas, task_data);
		
		g_object_notify(G_OBJECT(game_level), "tasks-to-complete");
	}
}

gboolean jammo_game_level_get_task_completed(JammoGameLevel* game_level, const gchar* task_name) {
	gboolean completed = FALSE;
	GList* task_data_in_list;
	TaskData* task_data;
	
	g_return_val_if_fail(JAMMO_IS_GAME_LEVEL(game_level), FALSE);

	if (!(task_data_in_list = g_list_find_custom(game_level->priv->task_datas, task_name, (GCompareFunc)task_data_compare_name))) {
		g_warning("A task name '%s' does not exist in the game level.\n", task_name);
	} else {
		task_data = (TaskData*)task_data_in_list->data;
		completed = task_data->completed;
	}
	
	return completed;
}

void jammo_game_level_set_task_completed(JammoGameLevel* game_level, const gchar* task_name, gboolean is_completed) {
	GList* task_data_in_list;
	TaskData* task_data;
	
	g_return_if_fail(JAMMO_IS_GAME_LEVEL(game_level));

	if (!(task_data_in_list = g_list_find_custom(game_level->priv->task_datas, task_name, (GCompareFunc)task_data_compare_name))) {
		g_warning("A task name '%s' does not exists in the game level.\n", task_name);
	} else {
		task_data = (TaskData*)task_data_in_list->data;
		if (task_data->completed != is_completed) {
			task_data->completed = is_completed;
			g_object_notify(G_OBJECT(game_level), "tasks-completed");

			if (task_data->completed && jammo_game_level_get_tasks_completed(game_level) >= g_list_length(game_level->priv->task_datas)) {
				g_signal_emit(game_level, signals[COMPLETED], 0);
			}
		}
	}
}

guint jammo_game_level_get_tasks_to_complete(JammoGameLevel* game_level) {
	g_return_val_if_fail(JAMMO_IS_GAME_LEVEL(game_level), 0);

	return g_list_length(game_level->priv->task_datas);
}

guint jammo_game_level_get_tasks_completed(JammoGameLevel* game_level) {
	guint number_of_completed_tasks = 0;
	GList* task_data_in_list;
	TaskData* task_data;

	g_return_val_if_fail(JAMMO_IS_GAME_LEVEL(game_level), 0);
	
	for (task_data_in_list = game_level->priv->task_datas; task_data_in_list; task_data_in_list = task_data_in_list->next) {
		task_data = (TaskData*)task_data_in_list->data;
		if (task_data->completed) {
			number_of_completed_tasks++;
		}
	}
	
	
	return number_of_completed_tasks;
}

JammoGameTask* jammo_game_level_get_active_task(JammoGameLevel* game_level) {
	g_return_val_if_fail(JAMMO_IS_GAME_LEVEL(game_level), NULL);

	return game_level->priv->active_task;
}

void jammo_game_level_start_task(JammoGameLevel* game_level, const gchar* task_name) {
	gchar* message = g_strdup_printf("jammo_game_level_start_task '%s'",task_name);
	cem_add_to_log(message,J_LOG_DEBUG);
	g_free(message);

	GList* task_data_in_list;
	TaskData* task_data;
	gchar* filename;
	GError* error = NULL;

	g_return_if_fail(JAMMO_IS_GAME_LEVEL(game_level));

	//g_return_if_fail(!game_level->priv->active_task);
	if (game_level->priv->active_task)
		cem_add_to_log("active_task should be NULL... still continuing",J_LOG_FATAL);

	if (!(task_data_in_list = g_list_find_custom(game_level->priv->task_datas, task_name, (GCompareFunc)task_data_compare_name))) {
		message = g_strdup_printf("A task name '%s' does not exist in the game level.", task_name);
		cem_add_to_log(message,J_LOG_FATAL);
		g_free(message);
	} else {
		task_data = (TaskData*)task_data_in_list->data;
		
		if (!game_level->priv->script) {
			game_level->priv->script = clutter_script_new();
		}

		if (!(filename = tangle_lookup_filename(task_name))) {
			g_critical("Cannot find a JSON file '%s'.\n", task_name);
		} else {
			if (!(game_level->priv->script_id = clutter_script_load_from_file(game_level->priv->script, filename, &error))) {
				g_critical("Cannot load a JSON file '%s': %s\n", filename, error->message);
				g_error_free(error);
			} else {
				if (!(game_level->priv->active_task = JAMMO_GAME_TASK(clutter_script_get_object(game_level->priv->script, task_name)))) {
					g_critical("Cannot load a JSON file '%s': %s\n", filename, error->message);
					
					free_active_task(game_level);
				} else {
					clutter_script_connect_signals(game_level->priv->script, game_level);
					g_signal_connect(game_level->priv->active_task, "completed", G_CALLBACK(on_task_completed), game_level);
					jammo_game_task_start(game_level->priv->active_task);

					game_level->priv->active_task_data = task_data;
				}
			}
		
			g_free(filename);
		}
		
	}
}

gboolean jammo_game_level_start_next_task(JammoGameLevel* game_level) {
	gboolean retvalue = FALSE;
	GList* task_data_in_list;
	TaskData* task_data;
	
	g_return_val_if_fail(JAMMO_IS_GAME_LEVEL(game_level), FALSE);

	//Search next task which is not completed yet.
	for (task_data_in_list = game_level->priv->task_datas; task_data_in_list; task_data_in_list = task_data_in_list->next) {
		task_data = (TaskData*)task_data_in_list->data;
		if (!task_data->completed) {
			jammo_game_level_start_task(game_level, task_data->name);
			retvalue = TRUE;
			break;
		}
	}
		
	return retvalue;
}

void jammo_game_level_stop_task(JammoGameLevel* game_level) {
	g_return_if_fail(JAMMO_IS_GAME_LEVEL(game_level));
	g_return_if_fail(game_level->priv->active_task);
	
	jammo_game_task_stop(game_level->priv->active_task);
	free_active_task(game_level);
}

static void jammo_game_level_completed(JammoGameLevel* game_level) {
}

static void jammo_game_level_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	//JammoGameLevel* game_level;
	//game_level = JAMMO_GAME_LEVEL(object);

	switch (prop_id) {
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_game_level_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoGameLevel* game_level;

	game_level = JAMMO_GAME_LEVEL(object);

        switch (prop_id) {
		case PROP_TASKS_TO_COMPLETE:
			g_value_set_uint(value, jammo_game_level_get_tasks_to_complete(game_level));
			break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static void jammo_game_level_finalize(GObject* object) {
	G_OBJECT_CLASS(jammo_game_level_parent_class)->finalize(object);
}

static void jammo_game_level_dispose(GObject* object) {
	JammoGameLevel* game_level;
	
	game_level = JAMMO_GAME_LEVEL(object);

	game_level->priv->script = NULL;

	TANGLE_UNREF_AND_NULLIFY_OBJECT(game_level->priv->script);

	G_OBJECT_CLASS(jammo_game_level_parent_class)->dispose(object);
}

static void jammo_game_level_class_init(JammoGameLevelClass* game_level_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(game_level_class);

	gobject_class->finalize = jammo_game_level_finalize;
	gobject_class->dispose = jammo_game_level_dispose;
	gobject_class->set_property = jammo_game_level_set_property;
	gobject_class->get_property = jammo_game_level_get_property;

	game_level_class->completed = jammo_game_level_completed;
	
	/**
	 * JammoGameLevel:tasks-to-complete:
	 *
	 * The number of tasks that has to be completed.
	 */
	g_object_class_install_property(gobject_class, PROP_TASKS_TO_COMPLETE,
	                                g_param_spec_uint("tasks-to-complete",
	                                "Tasks to complete",
	                                "The number of tasks that has to be completed",
	                                0, G_MAXUINT, 0,
	                                G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoGameLevel:tasks-completed:
	 *
	 * The number of completed tasks.
	 */
	g_object_class_install_property(gobject_class, PROP_TASKS_COMPLETED,
	                                g_param_spec_uint("tasks-completed",
	                                "Completed tasks",
	                                "The number of completed tasks",
	                                0, G_MAXUINT, 0,
	                                G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	/**
	 * JammoGameLevel::completed:
	 * @game: the object which received the signal
	 *
	 * The ::completed signal is emitted when the game-level has been completed by setting all tasks completed with jammo_game_level_set_task_completed().
	 */
	signals[COMPLETED] = g_signal_new("completed", G_TYPE_FROM_CLASS(gobject_class),
	                                  G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(JammoGameLevelClass, completed),
					  NULL, NULL,
				  	  g_cclosure_marshal_VOID__VOID,
				 	  G_TYPE_NONE, 0);

	g_type_class_add_private (gobject_class, sizeof (JammoGameLevelPrivate));
}

static void jammo_game_level_init(JammoGameLevel* game_level) {
	game_level->priv = G_TYPE_INSTANCE_GET_PRIVATE(game_level, JAMMO_TYPE_GAME_LEVEL, JammoGameLevelPrivate);
}

static gboolean jammo_game_level_parse_custom_node(ClutterScriptable* scriptable, ClutterScript* script, GValue* value, const gchar* name, JsonNode* node) {
	gboolean retvalue = FALSE;
	JammoGameLevel* game_level;
	GSList* slist = NULL;
	JsonArray* array;
	gint i;
	JsonNode* n;
	
	game_level = JAMMO_GAME_LEVEL(scriptable);
	
	if (!game_level->priv->script) {
		game_level->priv->script = script;
		g_object_ref(game_level->priv->script);
	}

	if (!strcmp(name, "tasks")) {
		if (JSON_NODE_TYPE(node) != JSON_NODE_ARRAY) {
			g_warning("Expected JSON array for '%s', skipped.", name);
		} else {
			array = json_node_get_array(node);
			for (i = json_array_get_length(array) - 1; i >= 0; i--) {
				n = json_array_get_element(array, i);
				if (JSON_NODE_TYPE(n) != JSON_NODE_VALUE || json_node_get_value_type(n) != G_TYPE_STRING) {
					g_warning("Expected a string value in '%s' array, skipped.", name);
				} else {
					slist = g_slist_prepend(slist, json_node_dup_string(n));
				}
			}
					
			g_value_init(value, G_TYPE_POINTER);
			g_value_set_pointer(value, g_slist_reverse(slist));
			
			retvalue = TRUE;
		}	
	} else if (parent_scriptable_iface->parse_custom_node) {
		retvalue = parent_scriptable_iface->parse_custom_node(scriptable, script, value, name, node);
	}
	
	return retvalue;
}

static void jammo_game_level_set_custom_property(ClutterScriptable* scriptable, ClutterScript* script, const gchar* name, const GValue* value) {
	JammoGameLevel* game_level;
	GSList* slist;
	GSList* slist_item;
	
	game_level = JAMMO_GAME_LEVEL(scriptable);
	
	if (!strcmp(name, "tasks")) {
		g_return_if_fail(G_VALUE_HOLDS(value, G_TYPE_POINTER));

		slist = (GSList*)g_value_get_pointer(value);
		for (slist_item = slist; slist_item; slist_item = slist_item->next) {
			jammo_game_level_add_task_name(game_level, (const gchar*)slist_item->data);
			g_free(slist_item->data);
		}
		g_slist_free(slist);	
	} else if (parent_scriptable_iface->set_custom_property) {
		parent_scriptable_iface->set_custom_property(scriptable, script, name, value);
	} else {
		g_object_set_property(G_OBJECT(scriptable), name, value);
	}
}

static void clutter_scriptable_iface_init(ClutterScriptableIface* iface) {
	if (!(parent_scriptable_iface = g_type_interface_peek_parent (iface))) {
		parent_scriptable_iface = g_type_default_interface_peek(CLUTTER_TYPE_SCRIPTABLE);
	}
	
	iface->parse_custom_node = jammo_game_level_parse_custom_node;
	iface->set_custom_property = jammo_game_level_set_custom_property;
}

static gint task_data_compare_name(TaskData* task_data, const gchar* name) {

	return strcmp(task_data->name, name);
}

static void free_active_task(JammoGameLevel* game_level) {
	clutter_script_unmerge_objects(game_level->priv->script, game_level->priv->script_id);

	g_object_unref(game_level->priv->script);
	game_level->priv->script = NULL;

	game_level->priv->active_task = NULL;
	game_level->priv->active_task_data = NULL;
}

static void on_task_completed(JammoGameTask* task, gpointer user_data) {
	JammoGameLevel* game_level;
	
	game_level = JAMMO_GAME_LEVEL(user_data);
	
	g_return_if_fail(game_level->priv->active_task == task);
	jammo_game_level_set_task_completed(game_level, game_level->priv->active_task_data->name, TRUE);
	
}
