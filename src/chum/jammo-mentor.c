/*
 * jammo-mentor.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "jammo-mentor.h"
#include "../meam/jammo-sample.h"
#include "../cem/cem.h"
#include <time.h>
#include <string.h>

#if DBUS_GLIB==1
#include <dbus/dbus-glib.h>
#endif

G_DEFINE_TYPE(JammoMentor, jammo_mentor, TANGLE_TYPE_BUTTON)

#if DBUS_GLIB==1
/*These values are for Nokia N900*/
#define MCE_TKLOCK_MODE_SIG "tklock_mode_ind"
#define MCE_SERVICE "com.nokia.mce"
#define MCE_SIGNAL_PATH "/com/nokia/mce/signal"
#define MCE_SIGNAL_IF "com.nokia.mce.signal"
#define MCE_DEVICE_LOCKED "locked"
#endif

#define DEFAULT_DURATION 1000

enum {
	PROP_0,
	PROP_STANDBY_SCALE,
	PROP_ACTIVE_SCALE,
	PROP_HOTSPOT_X,
	PROP_HOTSPOT_Y,
	PROP_IDLE_SPEECH,
	PROP_IDLE_TIMEOUT,
	PROP_PASSIVE,
	PROP_DEVICE_LOCKED
};

struct _JammoMentorPrivate {
	gfloat standby_scale;
	gfloat active_scale;
	gfloat hotspot_x;
	gfloat hotspot_y;
	gchar* idle_speech;
	JammoMentorSpokenCallback idle_speech_spoken_callback;
	gpointer idle_speech_spoken_user_data;
	gulong idle_timeout;
	gchar* language;
	
	GList* spoken_speeches;
	JammoSample* active_sample;
	ClutterAnimation* active_animation;

	ClutterActor* stage;
	gulong stage_captured_event_handler_id;
	guint timeout_event_source_id;
	time_t last_activity_time;

	#if DBUS_GLIB==1
	DBusGProxy* mce_proxy;
	#endif

	guint idle_speech_spoken : 1;
	guint interrupted : 1;
	guint device_locked : 1;
	guint passive : 1;
};

static void speak(JammoMentor* mentor, const gchar* speech, gboolean highlight, gfloat highlight_x, gfloat highlight_y, guint duration, JammoMentorSpokenCallback callback, gpointer user_data);
static void set_idle_timeout(JammoMentor* mentor, gboolean activate);
#if DBUS_GLIB==1
static void on_mce_tklock_mode_sig(DBusGProxy* proxy, const gchar* mode, gpointer user_data);
#endif

static GList* mentors;

ClutterActor* jammo_mentor_new(ClutterActor* actor, gfloat standby_scale, gfloat active_scale) {

	return CLUTTER_ACTOR(g_object_new(JAMMO_TYPE_MENTOR, "normal-background-actor", actor, "standby-scale", standby_scale, "active-scale", active_scale, "prefer-background-size", TRUE, NULL));
}

gfloat jammo_mentor_get_standby_scale(JammoMentor* mentor) {
	g_return_val_if_fail(JAMMO_IS_MENTOR(mentor), 0.0);

	return mentor->priv->standby_scale;
}

void jammo_mentor_set_standby_scale(JammoMentor* mentor, gfloat scale) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	if (mentor->priv->standby_scale != scale) {
		mentor->priv->standby_scale = scale;
		if (!mentor->priv->active_sample) {
			tangle_actor_animate(TANGLE_ACTOR(mentor), CLUTTER_EASE_IN_QUAD, 1500, "scale-x", mentor->priv->standby_scale, "scale-y", mentor->priv->standby_scale, NULL);
		}
		g_object_notify(G_OBJECT(mentor), "standby-scale");
	}
}

gfloat jammo_mentor_get_active_scale(JammoMentor* mentor) {
	g_return_val_if_fail(JAMMO_IS_MENTOR(mentor), 0.0);

	return mentor->priv->active_scale;
}

void jammo_mentor_set_active_scale(JammoMentor* mentor, gfloat scale) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	if (mentor->priv->active_scale != scale) {
		mentor->priv->active_scale = scale;
		if (mentor->priv->active_sample) {
			tangle_actor_animate(TANGLE_ACTOR(mentor), CLUTTER_EASE_IN_QUAD, 1500, "scale-x", mentor->priv->active_scale, "scale-y", mentor->priv->active_scale, NULL);
		}
		g_object_notify(G_OBJECT(mentor), "active-scale");
	}
}

void jammo_mentor_get_hotspot(JammoMentor* mentor, gfloat* hotspot_x_return, gfloat* hotspot_y_return) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	if (hotspot_x_return) {
		*hotspot_x_return = mentor->priv->hotspot_x;
	}
	if (hotspot_y_return) {
		*hotspot_y_return = mentor->priv->hotspot_y;
	}
}

void jammo_mentor_set_hotspot(JammoMentor* mentor, gfloat hotspot_x, gfloat hotspot_y) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	g_object_freeze_notify(G_OBJECT(mentor));

	if (mentor->priv->hotspot_x != hotspot_x) {
		mentor->priv->hotspot_x = hotspot_x;
		g_object_notify(G_OBJECT(mentor), "hotspot-x");
	}
	if (mentor->priv->hotspot_y != hotspot_y) {
		mentor->priv->hotspot_y = hotspot_y;
		g_object_notify(G_OBJECT(mentor), "hotspot-y");
	}
	
	g_object_thaw_notify(G_OBJECT(mentor));
}

void jammo_mentor_speak(JammoMentor* mentor, const gchar* speech) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	speak(mentor, speech, FALSE, 0.0, 0.0, DEFAULT_DURATION, NULL, NULL);
}

void jammo_mentor_speak_with_callback(JammoMentor* mentor, const gchar* speech, JammoMentorSpokenCallback callback, gpointer user_data) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	speak(mentor, speech, FALSE, 0.0, 0.0, DEFAULT_DURATION, callback, user_data);
}

void jammo_mentor_speak_and_highlight(JammoMentor* mentor, const gchar* speech, gfloat highlight_x, gfloat highlight_y) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	speak(mentor, speech, TRUE, highlight_x, highlight_y, DEFAULT_DURATION, NULL, NULL);
}

void jammo_mentor_speak_and_highlight_with_callback(JammoMentor* mentor, const gchar* speech, gfloat highlight_x, gfloat highlight_y, JammoMentorSpokenCallback callback, gpointer user_data) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	speak(mentor, speech, TRUE, highlight_x, highlight_y, DEFAULT_DURATION, callback, user_data);
}

void jammo_mentor_highlight(JammoMentor* mentor, gfloat highlight_x, gfloat highlight_y, guint duration) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	speak(mentor, NULL, TRUE, highlight_x, highlight_y, duration, NULL, NULL);
}

void jammo_mentor_highlight_with_callback(JammoMentor* mentor, gfloat highlight_x, gfloat highlight_y, guint duration, JammoMentorSpokenCallback callback, gpointer user_data) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	speak(mentor, NULL, TRUE, highlight_x, highlight_y, duration, callback, user_data);
}

void jammo_mentor_speak_once(JammoMentor* mentor, const gchar* speech) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	if (!g_list_find_custom(mentor->priv->spoken_speeches, speech, (GCompareFunc)g_strcmp0)) {
		mentor->priv->spoken_speeches = g_list_prepend(mentor->priv->spoken_speeches, g_strdup(speech));
		speak(mentor, speech, FALSE, 0.0, 0.0, DEFAULT_DURATION, NULL, NULL);
	}
}

void jammo_mentor_speak_once_with_callback(JammoMentor* mentor, const gchar* speech, JammoMentorSpokenCallback callback, gpointer user_data) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	if (!g_list_find_custom(mentor->priv->spoken_speeches, speech, (GCompareFunc)g_strcmp0)) {
		mentor->priv->spoken_speeches = g_list_prepend(mentor->priv->spoken_speeches, g_strdup(speech));
		speak(mentor, speech, FALSE, 0.0, 0.0, DEFAULT_DURATION, callback, user_data);
	}
}

void jammo_mentor_speak_and_highlight_once(JammoMentor* mentor, const gchar* speech, gfloat highlight_x, gfloat highlight_y) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	if (!g_list_find_custom(mentor->priv->spoken_speeches, speech, (GCompareFunc)g_strcmp0)) {
		mentor->priv->spoken_speeches = g_list_prepend(mentor->priv->spoken_speeches, g_strdup(speech));
		speak(mentor, speech, TRUE, highlight_x, highlight_y, DEFAULT_DURATION, NULL, NULL);
	}
}

void jammo_mentor_speak_and_highlight_once_with_callback(JammoMentor* mentor, const gchar* speech, gfloat highlight_x, gfloat highlight_y, JammoMentorSpokenCallback callback, gpointer user_data) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	if (!g_list_find_custom(mentor->priv->spoken_speeches, speech, (GCompareFunc)g_strcmp0)) {
		mentor->priv->spoken_speeches = g_list_prepend(mentor->priv->spoken_speeches, g_strdup(speech));
		speak(mentor, speech, TRUE, highlight_x, highlight_y, DEFAULT_DURATION, callback, user_data);
	}
}

void jammo_mentor_shut_up(JammoMentor* mentor) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	if (mentor->priv->active_sample) {
		mentor->priv->interrupted = TRUE;
		jammo_sample_stop(mentor->priv->active_sample);
	}
}

const gchar* jammo_mentor_get_idle_speech(JammoMentor* mentor) {
	g_return_val_if_fail(JAMMO_IS_MENTOR(mentor), NULL);

	return mentor->priv->idle_speech;
}

void jammo_mentor_set_idle_speech(JammoMentor* mentor, const gchar* speech) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	jammo_mentor_set_idle_speech_with_callback(mentor, speech, NULL, NULL);
}

void jammo_mentor_set_idle_speech_with_callback(JammoMentor* mentor, const gchar* speech, JammoMentorSpokenCallback callback, gpointer user_data) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	if (g_strcmp0(mentor->priv->idle_speech, speech)) {
		g_free(mentor->priv->idle_speech);
		mentor->priv->idle_speech = g_strdup(speech);
		mentor->priv->idle_speech_spoken = FALSE;
		set_idle_timeout(mentor, TRUE);
		g_object_notify(G_OBJECT(mentor), "idle-speech");
	}
	mentor->priv->idle_speech_spoken_callback = callback;
	mentor->priv->idle_speech_spoken_user_data = user_data;
}

gulong jammo_mentor_get_idle_timeout(JammoMentor* mentor) {
	g_return_val_if_fail(JAMMO_IS_MENTOR(mentor), 0);

	return mentor->priv->idle_timeout;
}

void jammo_mentor_set_idle_timeout(JammoMentor* mentor, gulong milliseconds) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	if (mentor->priv->idle_timeout != milliseconds) {
		mentor->priv->idle_timeout = milliseconds;
		set_idle_timeout(mentor, TRUE);
		g_object_notify(G_OBJECT(mentor), "idle-timeout");
	}
}

const gchar* jammo_mentor_get_language(JammoMentor* mentor) {
	g_return_val_if_fail(JAMMO_IS_MENTOR(mentor), NULL);

	return mentor->priv->language;
}

void jammo_mentor_set_language(JammoMentor* mentor, const gchar* language) {
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	if (mentor->priv->language)
		g_free(mentor->priv->language);
	mentor->priv->language = g_strdup(language);
}

gboolean jammo_mentor_get_passive(JammoMentor* mentor){
	g_return_val_if_fail(JAMMO_IS_MENTOR(mentor), FALSE);

	return mentor->priv->passive;
}

void jammo_mentor_set_passive(JammoMentor* mentor, gboolean passive){
	g_return_if_fail(JAMMO_IS_MENTOR(mentor));

	if (mentor->priv->passive != passive) {
		mentor->priv->passive = passive;
		
		if (passive) {
			cem_add_to_log("Mentor goes to passive mode", J_LOG_DEBUG);

			clutter_actor_set_opacity(CLUTTER_ACTOR(mentor), 60);
		} else {
			cem_add_to_log("Mentor goes to active mode", J_LOG_DEBUG);

			clutter_actor_set_opacity(CLUTTER_ACTOR(mentor), 255);
		}
	}
}

gboolean jammo_mentor_get_device_locked(JammoMentor* mentor) {
	g_return_val_if_fail(JAMMO_IS_MENTOR(mentor), FALSE);

	return mentor->priv->device_locked;
}

JammoMentor* jammo_mentor_get_default(void) {

	return (mentors ? JAMMO_MENTOR(mentors->data) : NULL);
}

static gboolean jammo_mentor_clicked(TangleButton* button) {
	JammoMentor* mentor;
	
	mentor = JAMMO_MENTOR(button);
	
	if (mentor->priv->active_sample) {
		mentor->priv->interrupted = TRUE;
		jammo_sample_stop(mentor->priv->active_sample);
	} else if (mentor->priv->active_animation) {
		mentor->priv->interrupted = TRUE;
		clutter_animation_completed(mentor->priv->active_animation);
	} else if (mentor->priv->idle_speech) {
		speak(mentor, mentor->priv->idle_speech, FALSE, 0.0, 0.0, DEFAULT_DURATION, mentor->priv->idle_speech_spoken_callback, mentor->priv->idle_speech_spoken_user_data);
	}

	return FALSE;
}

static gboolean jammo_mentor_held(TangleButton* button) {
	JammoMentor* mentor;
	
	mentor = JAMMO_MENTOR(button);

	cem_add_to_log("Mentor tap-and-hold",J_LOG_DEBUG);
	jammo_mentor_shut_up(mentor);

	jammo_mentor_set_passive(mentor, !mentor->priv->passive);

	return FALSE;
}

static void jammo_mentor_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoMentor* mentor;
	
	mentor = JAMMO_MENTOR(object);

	switch (prop_id) {
		case PROP_STANDBY_SCALE:
			jammo_mentor_set_standby_scale(mentor, g_value_get_float(value));
			break;
		case PROP_ACTIVE_SCALE:
			jammo_mentor_set_active_scale(mentor, g_value_get_float(value));
			break;
		case PROP_HOTSPOT_X:
			jammo_mentor_set_hotspot(mentor, g_value_get_float(value), mentor->priv->hotspot_y);
			break;
		case PROP_HOTSPOT_Y:
			jammo_mentor_set_hotspot(mentor, mentor->priv->hotspot_x, g_value_get_float(value));
			break;
		case PROP_IDLE_SPEECH:
			jammo_mentor_set_idle_speech(mentor, g_value_get_string(value));
			break;
		case PROP_IDLE_TIMEOUT:
			jammo_mentor_set_idle_timeout(mentor, g_value_get_ulong(value));
			break;
		case PROP_PASSIVE:
			jammo_mentor_set_passive(mentor, g_value_get_boolean(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_mentor_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoMentor* mentor;

	mentor = JAMMO_MENTOR(object);

        switch (prop_id) {
		case PROP_STANDBY_SCALE:
			g_value_set_float(value, mentor->priv->standby_scale);
			break;
		case PROP_ACTIVE_SCALE:
			g_value_set_float(value, mentor->priv->active_scale);
			break;
		case PROP_HOTSPOT_X:
			g_value_set_float(value, mentor->priv->hotspot_x);
			break;
		case PROP_HOTSPOT_Y:
			g_value_set_float(value, mentor->priv->hotspot_y);
			break;
		case PROP_IDLE_SPEECH:
			g_value_set_string(value, mentor->priv->idle_speech);
			break;
		case PROP_IDLE_TIMEOUT:
			g_value_set_ulong(value, mentor->priv->idle_timeout);
			break;
		case PROP_PASSIVE:
			g_value_set_boolean(value, mentor->priv->passive);
			break;
		case PROP_DEVICE_LOCKED:
			g_value_set_boolean(value, mentor->priv->device_locked);
			break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static void jammo_mentor_show(ClutterActor* actor) {
	set_idle_timeout(JAMMO_MENTOR(actor), TRUE);
	
	CLUTTER_ACTOR_CLASS(jammo_mentor_parent_class)->show(actor);
}

static void jammo_mentor_hide(ClutterActor* actor) {
	set_idle_timeout(JAMMO_MENTOR(actor), FALSE);
	
	CLUTTER_ACTOR_CLASS(jammo_mentor_parent_class)->hide(actor);
}

static void jammo_mentor_finalize(GObject* object) {
	G_OBJECT_CLASS(jammo_mentor_parent_class)->finalize(object);
}

static void jammo_mentor_dispose(GObject* object) {
	JammoMentor* mentor;
	
	mentor = JAMMO_MENTOR(object);
	
	set_idle_timeout(mentor, FALSE);

	#if DBUS_GLIB==1
	if (mentor->priv->mce_proxy) {
		g_object_unref(mentor->priv->mce_proxy);
		mentor->priv->mce_proxy = NULL;
	}
	#endif

	G_OBJECT_CLASS(jammo_mentor_parent_class)->dispose(object);
}

static void jammo_mentor_class_init(JammoMentorClass* mentor_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(mentor_class);
	ClutterActorClass* clutter_actor_class = CLUTTER_ACTOR_CLASS(mentor_class);
	TangleButtonClass* button_class = TANGLE_BUTTON_CLASS(mentor_class);

	gobject_class->finalize = jammo_mentor_finalize;
	gobject_class->dispose = jammo_mentor_dispose;
	gobject_class->set_property = jammo_mentor_set_property;
	gobject_class->get_property = jammo_mentor_get_property;

	clutter_actor_class->show = jammo_mentor_show;
	clutter_actor_class->hide = jammo_mentor_hide;

	button_class->clicked = jammo_mentor_clicked;
	button_class->held = jammo_mentor_held;

	/**
	 * JammoMentor:standby-scale:
	 *
	 * The scaling that is efective when the mentor is in standby.
	 */
	g_object_class_install_property(gobject_class, PROP_STANDBY_SCALE,
	                                g_param_spec_float("standby-scale",
	                                                   "Standby scale",
	                                                   "The scaling that is efective when the mentor is in standby",
	                                                   0.0, G_MAXFLOAT, 0.0,
	                                                   G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_CONSTRUCT));
	/**
	 * JammoMentor:active-scale:
	 *
	 * The scaling that is efective when the mentor is active.
	 */
	g_object_class_install_property(gobject_class, PROP_ACTIVE_SCALE,
	                                g_param_spec_float("active-scale",
	                                                   "Active scale",
	                                                   "The scaling that is efective when the mentor is active",
	                                                   0.0, G_MAXFLOAT, 0.0,
	                                                   G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_CONSTRUCT));
	/**
	 * JammoMentor:hotspot-x:
	 *
	 * The horizontal offset of the point within the mentor to be used when highlighting.
	 */
	g_object_class_install_property(gobject_class, PROP_HOTSPOT_X,
	                                g_param_spec_float("hotspot-x",
	                                                   "Hotspot X",
	                                                   "The horizontal offset of the point within the mentor to be used when highlighting",
	                                                   -G_MAXFLOAT, G_MAXFLOAT, 0.0,
	                                                   G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	/**
	 * JammoMentor:hotspoty:
	 *
	 * The vertical offset of the point within the mentor to be used when highlighting.
	 */
	g_object_class_install_property(gobject_class, PROP_HOTSPOT_Y,
	                                g_param_spec_float("hotspot-y",
	                                                   "Hotspot Y",
	                                                   "The vertical offset of the point within the mentor to be used when highlighting",
	                                                   -G_MAXFLOAT, G_MAXFLOAT, 0.0,
	                                                   G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	/**
	 * JammoMentor:idle-speech:
	 *
	 * The speech that the mentor will speak after a specified time (see :idle-time).
	 */
	g_object_class_install_property(gobject_class, PROP_IDLE_SPEECH,
	                                g_param_spec_string("idle-speech",
	                                                    "Idle speech",
	                                                    "The speech that the mentor will speak after a specified time",
	                                                    NULL,
	                                                    G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_CONSTRUCT));
	/**
	 * JammoMentor:idle-time:
	 *
	 * The time in seconds after the mentor will speak (see :idle-speech) if an user has been idle.
	 * Value 0 means disabled.
	 */
	g_object_class_install_property(gobject_class, PROP_IDLE_TIMEOUT,
	                                g_param_spec_ulong("idle-timeout",
	                                                   "Idle timeout",
	                                                   "The time in seconds after the mentor will speak if an user has been idle.",
	                                                   0, G_MAXULONG, 0,
	                                                   G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_CONSTRUCT));
	/**
	 * JammoMentor:passive:
	 *
	 * Whether the mentor is passive.
	 *
	 * Passive mentor does not respond or speak after idle timeout.
	 */
	g_object_class_install_property(gobject_class, PROP_PASSIVE,
	                                g_param_spec_boolean("passive",
	                                                     "Passive",
	                                                     "Whether the mentor is passive",
	                                                     FALSE,
	                                                     G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));
	/**
	 * JammoMentor:device-locked:
	 *
	 * Whether the device is locked.
	 *
	 * Despite being conceptually unrelated to a mentor, this property keeps track of the device locking state.
	 * The mentor itself will only speak if an user has been idle and the device is unlocked. Thus, it monitors
	 * the device locking state and exposes the result for other uses also using this property.
	 */
	g_object_class_install_property(gobject_class, PROP_DEVICE_LOCKED,
	                                g_param_spec_boolean("device-locked",
	                                                     "Device locked",
	                                                     "Whether the device is locked.",
	                                                     FALSE,
	                                                     G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB));

	g_type_class_add_private (gobject_class, sizeof (JammoMentorPrivate));
}

static void jammo_mentor_init(JammoMentor* mentor) {

	mentor->priv = G_TYPE_INSTANCE_GET_PRIVATE(mentor, JAMMO_TYPE_MENTOR, JammoMentorPrivate);
	mentors = g_list_append(mentors, mentor);
	
	g_object_set(mentor, "hold-timeout", 1500, NULL);

	#if DBUS_GLIB==1
	DBusGConnection* connection;

	if ((connection = dbus_g_bus_get(DBUS_BUS_SYSTEM, NULL))) {
		mentor->priv->mce_proxy = dbus_g_proxy_new_for_name(connection, MCE_SERVICE, MCE_SIGNAL_PATH, MCE_SIGNAL_IF);
		dbus_g_proxy_add_signal(mentor->priv->mce_proxy, MCE_TKLOCK_MODE_SIG, G_TYPE_STRING, G_TYPE_INVALID);
		dbus_g_proxy_connect_signal(mentor->priv->mce_proxy, MCE_TKLOCK_MODE_SIG, G_CALLBACK(on_mce_tklock_mode_sig), mentor, NULL);
	}
	#endif
}

static const gchar* find_characters(const gchar* string, const gchar* characters_parameter) {
	const gchar* found = NULL;
	const gchar* c;
	
	while (!found && *string) {
		for (c = characters_parameter; *c; c++) {
			if (*string == *c) {
				found = string;
				break;
			}
		}
		string++;
	}
	
	return found;
}

static const gchar* find_characters_inverse(const gchar* string, const gchar* characters_parameter) {
	const gchar* found = NULL;
	const gchar* c;
	
	while (*string) {
		for (c = characters_parameter; *c; c++) {
			if (*string == *c) {
				break;
			}
		}
		if (!*c) {
			found = string;
			break;
		}
		string++;
	}
	
	return found;
}

static void real_speak(JammoMentor* mentor, const gchar* next_speech, JammoMentorSpokenCallback callback, const gchar* speech, gpointer user_data);

static void on_sample_stopped(JammoSample* sample, gpointer user_data) {
	TangleVault* vault;
	JammoMentor* mentor;
	const gchar* next_speech;
	JammoMentorSpokenCallback callback;
	const gchar* speech;
	gpointer data;
	
	vault = TANGLE_VAULT(user_data);
	tangle_vault_get(vault, 5, JAMMO_TYPE_MENTOR, &mentor, G_TYPE_STRING, &next_speech, G_TYPE_POINTER, &callback, G_TYPE_STRING, &speech, G_TYPE_POINTER, &data);
	
	g_object_unref(mentor->priv->active_sample);
	mentor->priv->active_sample = NULL;

	if (next_speech && !mentor->priv->interrupted) {
		real_speak(mentor, next_speech, callback, speech, data);
	} else {
		tangle_actor_animate(TANGLE_ACTOR(mentor), CLUTTER_EASE_IN_QUAD, DEFAULT_DURATION,
	                	     "squint-x", 0.0,
				     "squint-y", 0.0,
	                	     "rotation-angle-x", 0.0,
	                	     "rotation-angle-y", 0.0,
				     "scale-x", mentor->priv->standby_scale,
				     "scale-y", mentor->priv->standby_scale,
				     NULL);

		set_idle_timeout(mentor, TRUE);

		if (callback) {
			callback(mentor, speech, mentor->priv->interrupted, data);
		}

		mentor->priv->interrupted = FALSE;

		/* TODO: g_free(speech); ? */
	}
}

static void on_animation_completed(ClutterAnimation* animation, gpointer user_data) {
	TangleVault* vault;
	JammoMentor* mentor;
	JammoMentorSpokenCallback callback;
	gpointer data;

	vault = TANGLE_VAULT(user_data);
	tangle_vault_get(vault, 3, JAMMO_TYPE_MENTOR, &mentor, G_TYPE_POINTER, &callback, G_TYPE_POINTER, &data);

	tangle_actor_animate(TANGLE_ACTOR(mentor), CLUTTER_EASE_IN_QUAD, DEFAULT_DURATION,
	                     "squint-x", 0.0,
			     "squint-y", 0.0,
	                     "rotation-angle-x", 0.0,
	                     "rotation-angle-y", 0.0,
			     "scale-x", mentor->priv->standby_scale,
			     "scale-y", mentor->priv->standby_scale,
			     NULL);

	set_idle_timeout(mentor, TRUE);

	if (callback) {
		callback(mentor, NULL, mentor->priv->interrupted, data);
	}

	mentor->priv->interrupted = FALSE;

	/* TODO: g_free(speech); ? */
}

#define WHITE_SPACES " \t\n\r"

static void real_speak(JammoMentor* mentor, const gchar* next_speech, JammoMentorSpokenCallback callback, const gchar* speech, gpointer user_data) {
	const gchar* space;
	gchar* current_speech;
	gchar* localized_speech;
	gchar* filename;
	TangleVault* vault;
	
	if ((space = find_characters(next_speech, WHITE_SPACES))) {
		current_speech = g_strndup(next_speech, space - next_speech);
		next_speech = find_characters_inverse(space, WHITE_SPACES);
	} else {
		current_speech = g_strdup(next_speech);
		next_speech = NULL;
	}
	
	if (current_speech[0] == '/' || !mentor->priv->language) {
		localized_speech = g_strdup(current_speech);
	} else {
		localized_speech = g_strconcat(mentor->priv->language, "/", current_speech, NULL);
	}
	if ((filename = tangle_lookup_filename(localized_speech))) {
		mentor->priv->active_sample = jammo_sample_new_from_file(filename);
		g_free(filename);
	} else {
		mentor->priv->active_sample = jammo_sample_new_from_file(localized_speech);
	}

	cem_add_to_log(localized_speech,J_LOG_MENTOR_SPEECH);
	//printf("%s * %s * %s * %s\n", current_speech, localized_speech, speech, next_speech);

	g_free(localized_speech);
	g_free(current_speech);
	
	vault = tangle_vault_new(5, JAMMO_TYPE_MENTOR, mentor, G_TYPE_STRING, next_speech, G_TYPE_POINTER, callback, G_TYPE_STRING, speech, G_TYPE_POINTER, user_data);
	tangle_signal_connect_vault(mentor->priv->active_sample, "stopped", G_CALLBACK(on_sample_stopped), vault);

	jammo_sample_play(mentor->priv->active_sample);
}

static void speak(JammoMentor* mentor, const gchar* speech, gboolean highlight, gfloat highlight_x, gfloat highlight_y, guint duration, JammoMentorSpokenCallback callback, gpointer user_data) {
	ClutterAnimation* animation = NULL;
	gfloat x, y, width, height, squint_x, squint_y;
	ClutterVertex rotation_center_x = { 0 };
	ClutterVertex rotation_center_y = { 0 };
	gdouble rotation_angle_x, rotation_angle_y;
	TangleVault* vault;

	if (mentor->priv->passive) {
		cem_add_to_log(" Mentor in passive mode, skip", J_LOG_DEBUG);
		
		if (callback) {
			callback(mentor, speech, TRUE, user_data);
		}
	} else {
		set_idle_timeout(mentor, FALSE);
		tangle_actor_show(TANGLE_ACTOR(mentor));

		if (mentor->priv->active_sample) {
			jammo_sample_stop(mentor->priv->active_sample);
		}

		if (highlight) {
			clutter_actor_get_position(CLUTTER_ACTOR(mentor), &x, &y);
			clutter_actor_get_size(CLUTTER_ACTOR(mentor), &width, &height);
			
			squint_x = highlight_x - x - (mentor->priv->hotspot_x * mentor->priv->active_scale);
			squint_y = highlight_y - y - (mentor->priv->hotspot_y * mentor->priv->active_scale);
			
			rotation_center_x.y = height / 2.0;
			rotation_center_y.x = width / 2.0;
			g_object_set(CLUTTER_ACTOR(mentor), "rotation-center-x", &rotation_center_x, "rotation-center-y", &rotation_center_y, NULL);

			if (squint_x < 0.0) {
				rotation_angle_y = 180.0;
				squint_x = -squint_x - width * mentor->priv->active_scale;
			} else {
				rotation_angle_y = 0.0;
			}
			if (squint_y < 0.0) {
				rotation_angle_x = 180.0;
				squint_y = -squint_y - height * mentor->priv->active_scale;
			} else {
				rotation_angle_x = 0.0;
			}

			animation = tangle_actor_animate(TANGLE_ACTOR(mentor), CLUTTER_EASE_IN_QUAD, duration,
			                                 "squint-x", squint_x,
							 "squint-y", squint_y,
							 "rotation-angle-x", rotation_angle_x,
							 "rotation-angle-y", rotation_angle_y,
							 "scale-x", mentor->priv->active_scale,
							 "scale-y", mentor->priv->active_scale,
							 NULL);
		} else {
			animation = tangle_actor_animate(TANGLE_ACTOR(mentor), CLUTTER_EASE_IN_QUAD, duration,
			                                 "scale-x", mentor->priv->active_scale,
							 "scale-y", mentor->priv->active_scale,
							 NULL);		
		}

		if (speech) {
			real_speak(mentor, speech, callback, speech, user_data);
		} else if (animation) {
			vault = tangle_vault_new(3, JAMMO_TYPE_MENTOR, mentor, G_TYPE_POINTER, callback, G_TYPE_POINTER, user_data);
			tangle_signal_connect_vault(animation, "completed", G_CALLBACK(on_animation_completed), vault);

			mentor->priv->active_animation = animation;
			g_object_add_weak_pointer(G_OBJECT(mentor->priv->active_animation), (gpointer*)&mentor->priv->active_animation);
		}
	}
}

static gboolean on_stage_captured_event(ClutterActor* actor, ClutterEvent* event, gpointer user_data) {
	JammoMentor* mentor;
	
	mentor = JAMMO_MENTOR(user_data);
	
	mentor->priv->last_activity_time = time(NULL);
	
	return FALSE;
}

static gboolean on_timeout(gpointer user_data) {
	JammoMentor* mentor;
	time_t elapsed_time;
	
	mentor = JAMMO_MENTOR(user_data);
	
	elapsed_time = time(NULL) - mentor->priv->last_activity_time;
	if (!mentor->priv->passive && elapsed_time >= mentor->priv->idle_timeout) {
		mentor->priv->timeout_event_source_id = 0;
		mentor->priv->idle_speech_spoken = TRUE;
		speak(mentor, mentor->priv->idle_speech, FALSE, 0.0, 0.0, DEFAULT_DURATION, mentor->priv->idle_speech_spoken_callback, mentor->priv->idle_speech_spoken_user_data);
	} else {
		mentor->priv->timeout_event_source_id = g_timeout_add((mentor->priv->idle_timeout - elapsed_time) * 1000, on_timeout, mentor);
	}
	
	return FALSE;
}

static void set_idle_timeout(JammoMentor* mentor, gboolean activate) {
	if (mentor->priv->timeout_event_source_id) {
		g_source_remove(mentor->priv->timeout_event_source_id);
		mentor->priv->timeout_event_source_id = 0;
	}
	
	if (activate && mentor->priv->idle_timeout && !mentor->priv->device_locked && mentor->priv->idle_speech && !mentor->priv->idle_speech_spoken && !mentor->priv->active_sample && CLUTTER_ACTOR_IS_VISIBLE(mentor)) {
		if (!mentor->priv->stage && (mentor->priv->stage = clutter_actor_get_stage(CLUTTER_ACTOR(mentor)))) {
			mentor->priv->stage_captured_event_handler_id = g_signal_connect(mentor->priv->stage, "captured-event", G_CALLBACK(on_stage_captured_event), mentor);
		}

		mentor->priv->last_activity_time = time(NULL);

		mentor->priv->timeout_event_source_id = g_timeout_add(mentor->priv->idle_timeout * 1000, on_timeout, mentor);
	} else if (!activate && mentor->priv->stage) {
		g_signal_handler_disconnect(mentor->priv->stage, mentor->priv->stage_captured_event_handler_id);
		mentor->priv->stage = NULL;
	}
}

#if DBUS_GLIB==1
static void on_mce_tklock_mode_sig(DBusGProxy* proxy, const gchar* mode, gpointer user_data) {
	JammoMentor* mentor;
	gboolean old_device_locked;
	
	mentor = JAMMO_MENTOR(user_data);

	old_device_locked = mentor->priv->device_locked;
	if (!strcmp(mode, MCE_DEVICE_LOCKED)) {
		mentor->priv->device_locked = TRUE;
		jammo_mentor_shut_up(mentor);
	} else {
		mentor->priv->device_locked = FALSE;
		mentor->priv->idle_speech_spoken = FALSE;
	}
	if (old_device_locked != mentor->priv->device_locked) {
		set_idle_timeout(mentor, !mentor->priv->device_locked);
		g_object_notify(G_OBJECT(mentor), "device-locked");
	}
}
#endif
