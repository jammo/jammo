/*
 * jammo-cursor.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#ifndef __JAMMO_CURSOR_H__
#define __JAMMO_CURSOR_H__

#include <tangle.h>
#include "../meam/jammo-sequencer.h"

#define JAMMO_TYPE_CURSOR (jammo_cursor_get_type())
#define JAMMO_CURSOR(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), JAMMO_TYPE_CURSOR, JammoCursor))
#define JAMMO_IS_CURSOR(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), JAMMO_TYPE_CURSOR))
#define JAMMO_CURSOR_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), JAMMO_TYPE_CURSOR, JammoCursorClass))
#define JAMMO_IS_CURSOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), JAMMO_TYPE_CURSOR))
#define JAMMO_CURSOR_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), JAMMO_TYPE_CURSOR, JammoCursorClass))

typedef struct _JammoCursorPrivate JammoCursorPrivate;

typedef struct _JammoCursor {
	TangleWidget parent_instance;
	JammoCursorPrivate* priv;
} JammoCursor;

typedef struct _JammoCursorClass {
	TangleWidgetClass parent_class;
} JammoCursorClass;

GType jammo_cursor_get_type(void) G_GNUC_CONST;

ClutterActor* jammo_cursor_new(JammoSequencer* sequencer, ClutterActor* cursor_texture);

#endif
