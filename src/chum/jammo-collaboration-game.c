/*
 * jammo-collaboration-game.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther <mikko.gynther@lut.fi>
 */

#include "jammo-collaboration-game.h"
#include "jammo-editing-track-view.h"
#include "jammo-miditrack-view.h"
#include "../meam/jammo-backing-track.h"
#include "../meam/jammo-sequencer.h"
#include "../gems/gems.h"

G_DEFINE_TYPE(JammoCollaborationGame, jammo_collaboration_game, G_TYPE_INITIALLY_UNOWNED);

enum {
	PROP_0,
	PROP_BACKING_TRACK_LOCATION,
	PROP_BACKING_TRACK,
	PROP_SEQUENCER,
	PROP_MIX_LOCATION,
	PROP_SONG_FILE_OUT_LOCATION,
	PROP_THEME,
	PROP_VARIATION
};

struct _JammoCollaborationGamePrivate {
	// "public"
	gchar * backing_track_location;
	gchar * mix_location;
	gchar * song_file_out_location;
	gchar * theme;
	gint variation;

	// "protected", can be used if wanted but will be created by inherited games if
	// not already created
	JammoBackingTrack * backing_track;
	JammoSequencer * sequencer;
	
	// private
	// list for user_id, view pairs
	GList * user_data_list;
};

struct jammo_collaboration_game_user_data {
	guint user_id;
	ClutterActor * view;
};

static void jammo_collaboration_game_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
	JammoCollaborationGame * collaboration_game;
	
	collaboration_game = JAMMO_COLLABORATION_GAME(object);

	switch (prop_id) {
		case PROP_BACKING_TRACK_LOCATION:
			g_value_set_string(value, collaboration_game->priv->backing_track_location);
			break;
		case PROP_BACKING_TRACK:
			g_value_set_object(value, collaboration_game->priv->backing_track);
			break;
		case PROP_SEQUENCER:
			g_value_set_object(value, collaboration_game->priv->sequencer);
			break;
		case PROP_MIX_LOCATION:
			g_value_set_string(value, collaboration_game->priv->mix_location);
			break;
		case PROP_SONG_FILE_OUT_LOCATION:
			g_value_set_string(value, collaboration_game->priv->song_file_out_location);
			break;
		case PROP_THEME:
			g_value_set_string(value, collaboration_game->priv->theme);
			break;
		case PROP_VARIATION:
			g_value_set_int(value, collaboration_game->priv->variation);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_collaboration_game_set_property(GObject* object, guint prop_id,const GValue* value, GParamSpec* pspec) {
	JammoCollaborationGame * collaboration_game;
	
	collaboration_game = JAMMO_COLLABORATION_GAME(object);

	switch (prop_id) {
		case PROP_BACKING_TRACK_LOCATION:
			if (collaboration_game->priv->backing_track_location!=NULL) {
				g_free(collaboration_game->priv->backing_track_location);
				collaboration_game->priv->backing_track_location=NULL;
			}
			g_assert(!collaboration_game->priv->backing_track_location);
			collaboration_game->priv->backing_track_location = g_strdup(g_value_get_string(value));
			break;
		case PROP_BACKING_TRACK:
			if (collaboration_game->priv->backing_track!=NULL) {
				g_object_unref(collaboration_game->priv->backing_track);
			}
			if (JAMMO_IS_BACKING_TRACK(g_value_get_object(value))) {
				collaboration_game->priv->backing_track = JAMMO_BACKING_TRACK(g_value_get_object(value));
			}
			else {
				collaboration_game->priv->backing_track = NULL;				
			}
			break;
		case PROP_SEQUENCER:
			if (collaboration_game->priv->sequencer!=NULL) {
				g_object_unref(collaboration_game->priv->sequencer);
			}
			if (JAMMO_IS_SEQUENCER(g_value_get_object(value))) {
				collaboration_game->priv->sequencer = JAMMO_SEQUENCER(g_value_get_object(value));
			}
			else {
				collaboration_game->priv->sequencer = NULL;
			}
			break;
		case PROP_MIX_LOCATION:
			if (collaboration_game->priv->mix_location!=NULL) {
				g_free(collaboration_game->priv->mix_location);
				collaboration_game->priv->mix_location=NULL;
			}
			g_assert(!collaboration_game->priv->mix_location);
			collaboration_game->priv->mix_location = g_strdup(g_value_get_string(value));
			break;
		case PROP_SONG_FILE_OUT_LOCATION:
			if (collaboration_game->priv->song_file_out_location!=NULL) {
				g_free(collaboration_game->priv->song_file_out_location);
				collaboration_game->priv->song_file_out_location=NULL;
			}
			g_assert(!collaboration_game->priv->song_file_out_location);
			collaboration_game->priv->song_file_out_location = g_strdup(g_value_get_string(value));
			break;
		case PROP_THEME:
			if (collaboration_game->priv->theme!=NULL) {
				g_free(collaboration_game->priv->theme);
				collaboration_game->priv->theme=NULL;
			}
			g_assert(!collaboration_game->priv->theme);
			collaboration_game->priv->theme = g_strdup(g_value_get_string(value));
			break;
		case PROP_VARIATION:
			collaboration_game->priv->variation = g_value_get_int(value);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

// implementation for teacher exit
void jammo_collaboration_game_teacher_exit(JammoCollaborationGame * collaboration_game) {
	// TODO implement teacher exit
	g_return_if_fail(JAMMO_IS_COLLABORATION_GAME(collaboration_game));

	// call function that was set in constructor
	// this can be different on inherited classes
	JAMMO_COLLABORATION_GAME_GET_CLASS (collaboration_game)->teacher_exit(collaboration_game);
}

// return values 0 success, 1 can not open file, 2 other error
int jammo_collaboration_game_create_song_file(JammoCollaborationGame * collaboration_game) {
	if (JAMMO_IS_COLLABORATION_GAME(collaboration_game)==FALSE) {
		return 2;
	}

	// call function that was set in constructor
	// this can be different on inherited classes
	return (JAMMO_COLLABORATION_GAME_GET_CLASS (collaboration_game)->create_song_file(collaboration_game));
}

void jammo_collaboration_game_game_callback(GObject * game, int type, const gchar* song_info) {
	JammoCollaborationGame * collaboration_game = JAMMO_COLLABORATION_GAME(game);
	
	// TODO common operations for all games

	JAMMO_COLLABORATION_GAME_GET_CLASS (collaboration_game)->game_callback(game, type, song_info);
}

static GObject* jammo_collaboration_game_constructor(GType type, guint n_properties, GObjectConstructParam* properties) {
	GObject* object;
	JammoCollaborationGame * collaboration_game;

	object = G_OBJECT_CLASS(jammo_collaboration_game_parent_class)->constructor(type, n_properties, properties);

	collaboration_game = JAMMO_COLLABORATION_GAME(object);
	collaboration_game->priv->backing_track_location=NULL;
	collaboration_game->priv->backing_track=NULL;
	collaboration_game->priv->sequencer=NULL;
	collaboration_game->priv->mix_location=NULL;
	collaboration_game->priv->song_file_out_location=NULL;
	collaboration_game->priv->user_data_list= NULL;
	collaboration_game->priv->theme= NULL;
	
	collaboration_game->priv->variation = 0;

	return object;
}


static void jammo_collaboration_game_finalize(GObject* object) {
	JammoCollaborationGame* collaboration_game;
	
	collaboration_game = JAMMO_COLLABORATION_GAME(object);
	if (collaboration_game->priv->backing_track_location) {
		g_free(collaboration_game->priv->backing_track_location);
		collaboration_game->priv->backing_track_location=NULL;
	}
	if (collaboration_game->priv->sequencer) {
		g_object_unref(collaboration_game->priv->sequencer);
		collaboration_game->priv->sequencer=NULL;
		// there is no need to unref backing track because sequencer will do it
		collaboration_game->priv->backing_track=NULL;
	}
	if (collaboration_game->priv->mix_location) {
		g_free(collaboration_game->priv->mix_location);
		collaboration_game->priv->mix_location=NULL;
	}
	
	if (collaboration_game->priv->theme) {
		g_free(collaboration_game->priv->theme);
		collaboration_game->priv->theme=NULL;
	}
	
	GList * list;
	struct jammo_collaboration_game_user_data * user_data;
	for (list=collaboration_game->priv->user_data_list;list;list=list->next) {
		user_data=list->data;
		free(user_data);
		user_data=NULL;
	}
	g_list_free(collaboration_game->priv->user_data_list);

	G_OBJECT_CLASS(jammo_collaboration_game_parent_class)->finalize(object);
}

static void jammo_collaboration_game_dispose(GObject* object) {
	G_OBJECT_CLASS(jammo_collaboration_game_parent_class)->dispose(object);
}

static void jammo_collaboration_game_class_init(JammoCollaborationGameClass* collaboration_game_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(collaboration_game_class);

	printf("game class init\n");

	gobject_class->constructor = jammo_collaboration_game_constructor;
	gobject_class->finalize = jammo_collaboration_game_finalize;
	gobject_class->dispose = jammo_collaboration_game_dispose;
	gobject_class->set_property = jammo_collaboration_game_set_property;
	gobject_class->get_property = jammo_collaboration_game_get_property;

	// group callback for gems
	// this is used in passing group related information to game, e.g. player joined game
	gems_set_callback_game(jammo_collaboration_game_game_callback);

	g_object_class_install_property(gobject_class, PROP_BACKING_TRACK_LOCATION,
	                                g_param_spec_string("backing-track-location",
	                                "Backing track location",
	                                "Path to backing track that will be used",
	                                "", G_PARAM_READABLE | G_PARAM_WRITABLE));


	g_object_class_install_property(gobject_class, PROP_BACKING_TRACK,
	                                g_param_spec_object("backing-track",
	                                "Backing track",
	                                "Backing track in current game",
	                                JAMMO_TYPE_BACKING_TRACK, G_PARAM_READABLE | G_PARAM_WRITABLE));

	g_object_class_install_property(gobject_class, PROP_SEQUENCER,
	                                g_param_spec_object("sequencer",
	                                "Sequencer",
	                                "Sequencer in current game",
	                                JAMMO_TYPE_SEQUENCER, G_PARAM_READABLE | G_PARAM_WRITABLE));

	g_object_class_install_property(gobject_class, PROP_MIX_LOCATION,
	                                g_param_spec_string("mix-location",
	                                "Mix location",
	                                "Path to mix file that will be produced",
	                                "", G_PARAM_READABLE | G_PARAM_WRITABLE));

	g_object_class_install_property(gobject_class, PROP_SONG_FILE_OUT_LOCATION,
	                                g_param_spec_string("song-file-out-location",
	                                "Song file out location",
	                                "Path to song file that will be produced",
	                                "", G_PARAM_READABLE | G_PARAM_WRITABLE));
	                                
	g_object_class_install_property(gobject_class, PROP_THEME,
	                                g_param_spec_string("theme",
	                                "Theme",
	                                "Theme used in game",
	                                "", G_PARAM_READABLE | G_PARAM_WRITABLE));

	g_object_class_install_property(gobject_class, PROP_VARIATION,
	                                g_param_spec_int("variation",
	                                "Variation",
	                                "Variation of theme",
	                                G_MININT,G_MAXINT,0, G_PARAM_READABLE | G_PARAM_WRITABLE));

	g_type_class_add_private(gobject_class, sizeof(JammoCollaborationGamePrivate));

}

static void jammo_collaboration_game_init(JammoCollaborationGame* collaboration_game) {
	printf("game init\n");
	collaboration_game->priv = G_TYPE_INSTANCE_GET_PRIVATE(collaboration_game, JAMMO_TYPE_COLLABORATION_GAME, JammoCollaborationGamePrivate);
}

ClutterActor* jammo_collaboration_game_user_id_to_track(JammoCollaborationGame* collaboration_game, guint user_id) {
	GList * list;
	struct jammo_collaboration_game_user_data * user_data;
	for (list=collaboration_game->priv->user_data_list ; list ; list=list->next) {
		user_data=list->data;
		if ((user_data->user_id) == user_id) {
			return user_data->view;
		}
	}
	return NULL;
}

void jammo_collaboration_game_add_view(JammoCollaborationGame* collaboration_game, guint user_id, ClutterActor * view) {
	struct jammo_collaboration_game_user_data * user_data = malloc(sizeof(struct jammo_collaboration_game_user_data));
	user_data->user_id=user_id;
	user_data->view=view;
	collaboration_game->priv->user_data_list = g_list_append(collaboration_game->priv->user_data_list, user_data);
}

void jammo_collaboration_game_remove_view(JammoCollaborationGame* collaboration_game, guint user_id) {
	GList * list;
	struct jammo_collaboration_game_user_data * user_data;
	for (list=collaboration_game->priv->user_data_list;list;list=list->next) {
		user_data=list->data;
		if (user_data->user_id==user_id) {
			collaboration_game->priv->user_data_list = g_list_remove(collaboration_game->priv->user_data_list, user_data);
			free(user_data);
			user_data=NULL;
			break;
		}
	}
}

void jammo_collaboration_game_dump_views(JammoCollaborationGame* collaboration_game) {
	printf("JammoCollaborationGame user_data_list length: %u, contents:\n", g_list_length(collaboration_game->priv->user_data_list));
	GList * list;
	struct jammo_collaboration_game_user_data * user_data;
	for (list=collaboration_game->priv->user_data_list; list; list=list->next) {
		user_data=list->data;
		printf("  user_id: %u, view: ", user_data->user_id);
		if (JAMMO_IS_EDITING_TRACK_VIEW(user_data->view)) {
			printf("track view\n");
		}
		else if (JAMMO_IS_MIDITRACK_VIEW(user_data->view)) {
			printf("miditrack view\n");
		}
		else {
			printf("unknown\n");
		}
	}
	printf("\n");
}
int jammo_collaboration_game_viewcount(JammoCollaborationGame* collaboration_game)
{
	return g_list_length(collaboration_game->priv->user_data_list);
}
