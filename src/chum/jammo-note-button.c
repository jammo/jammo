/*
 * jammo-note-button.c
 *
 * This file is part of JamMo.
 *
 * This is box visualizing note. It always need start and stop events (jammo-midi-event).
 * It is draggable and can be dropped to miditrack-view.
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */

#include "jammo-note-button.h"
#include "jammo-miditrack-view.h"
#include <string.h>
#include "../meam/jammo-midi.h"

G_DEFINE_TYPE(JammoNoteButton, jammo_note_button, TANGLE_TYPE_BUTTON);

enum {
	PROP_0,
	PROP_START_EVENT,
	PROP_STOP_EVENT,
};

struct _JammoNoteButtonPrivate {
	JammoMidiEvent* start_event;
	JammoMidiEvent* stop_event;
	ClutterAction* drag_action;
};

static gboolean on_clicked(TangleButton* button, gpointer user_data);

static const ClutterColor default_note_color = { 255, 0, 0, 128 };
static const ClutterColor default_handle_color = { 255, 255, 0, 128 };

//This is only for debugging
ClutterActor* jammo_note_button_new_without_events() {
	ClutterActor* note =  CLUTTER_ACTOR(g_object_new(JAMMO_TYPE_NOTE_BUTTON, NULL));
	ClutterActor* box =  clutter_rectangle_new_with_color(&default_note_color);

	tangle_button_set_normal_background_actor(TANGLE_BUTTON(note), box);
	return note;
}

static void release_handle(GObject* object, GParamSpec* param_spec, gpointer user_data) {
	//printf("note_button: release_handle\n");
	GstClockTime new_stop_time;
	gfloat new_width;
	gfloat x;
	guint64 slot_duration;
	gfloat slot_width;

	JammoNoteButton* note_button = JAMMO_NOTE_BUTTON(clutter_actor_get_parent(CLUTTER_ACTOR(object)));
	ClutterActor* parent = clutter_actor_get_parent(CLUTTER_ACTOR(note_button));

	new_width=clutter_actor_get_width(CLUTTER_ACTOR(note_button));
	x=clutter_actor_get_x(CLUTTER_ACTOR(note_button));
	g_object_get(parent,"slot-duration", &slot_duration, "slot-width", &slot_width, NULL);

	new_stop_time = (x+new_width) / slot_width * slot_duration;
	//printf("new_width=%f   , x=%f   , slot_duration %llu  , slot_width=%f \n",new_width,x,slot_duration,slot_width);
	//printf("new_stop_time =%llu \n",new_stop_time);
	note_button->priv->stop_event->timestamp = new_stop_time;
}


/*
width,height
and start+stop events
*/
ClutterActor* jammo_note_button_new(gfloat width, gfloat height, JammoMidiEvent* start, JammoMidiEvent* stop) {
	//printf("making note with width=%f and height=%f\n",width,height);
	ClutterActor* note_itself =  CLUTTER_ACTOR(g_object_new(JAMMO_TYPE_NOTE_BUTTON, "start-event",start,"stop-event",stop,NULL));

	ClutterActor* box =  clutter_rectangle_new_with_color(&default_note_color);
	ClutterActor* handle =  clutter_rectangle_new_with_color(&default_handle_color);

	ClutterAction* action;
	TangleBinding* binding;
	GValue value = { 0 };

	gfloat handle_width = height/5;  //TODO. We can't use constant because width can be (let say) 1.0px when zoomed-out.
	clutter_actor_set_size(box,(gfloat)width,(gfloat)height);
	clutter_actor_set_size(handle,handle_width,(gfloat)height);

	tangle_widget_add(TANGLE_WIDGET(note_itself), handle,NULL);

	tangle_button_set_normal_background_actor(TANGLE_BUTTON(note_itself), box);

	binding = tangle_binding_new(G_OBJECT(box), "width", G_OBJECT(handle), "x");
	g_value_init(&value, G_TYPE_FLOAT);
	g_value_set_float(&value, -handle_width);
	tangle_binding_set_constant_term(binding, &value);
	g_value_unset(&value);

	binding = tangle_binding_new(G_OBJECT(box), "height", G_OBJECT(handle), "width");
	g_value_init(&value, G_TYPE_FLOAT);
	g_value_set_float(&value, 0.2);
	tangle_binding_set_multiplier(binding, &value);
	g_value_unset(&value);

	action = tangle_stretch_action_new(box);
	tangle_stretch_action_set_min_size(TANGLE_STRETCH_ACTION(action), 10.0, height);
	tangle_stretch_action_set_max_size(TANGLE_STRETCH_ACTION(action), 700.0, height);
	clutter_actor_add_action(handle, action);
	clutter_actor_set_reactive(handle, TRUE);

	g_signal_connect (handle, "button-release-event", G_CALLBACK (release_handle),NULL);
	return note_itself;
}



static void on_drag_end(ClutterDragAction* action, ClutterActor* actor, gfloat event_x, gfloat event_y, ClutterModifierType modifiers, gpointer user_data) {
	//printf("note_button: on_drag_end\n");
	JammoNoteButton* note_button;
	ClutterActor* parent;
	
	note_button = JAMMO_NOTE_BUTTON(user_data);
	
	parent = clutter_actor_get_parent(CLUTTER_ACTOR(note_button));
	if (JAMMO_IS_MIDITRACK_VIEW(parent)) {
		clutter_container_remove_actor(CLUTTER_CONTAINER(parent), CLUTTER_ACTOR(note_button));
	}
}

static void jammo_note_button_constructed(GObject* object) {
	JammoNoteButton* note_button;
	
	note_button = JAMMO_NOTE_BUTTON(object);

	note_button->priv->drag_action = tangle_drag_action_new();
	g_signal_connect(note_button->priv->drag_action, "drag-end", G_CALLBACK(on_drag_end), note_button);
	clutter_actor_add_action(CLUTTER_ACTOR(object), note_button->priv->drag_action);

	tangle_widget_set_prefer_background_size(TANGLE_WIDGET(note_button), TRUE);

	g_signal_connect(note_button, "button-press-event", G_CALLBACK(on_clicked), NULL);
}

static void jammo_note_button_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoNoteButton* note_button;
	
	note_button = JAMMO_NOTE_BUTTON(object);

	switch (prop_id) {
		case PROP_START_EVENT:
			note_button->priv->start_event = (JammoMidiEvent*)(g_value_get_pointer(value));
			break;
		case PROP_STOP_EVENT:
			note_button->priv->stop_event = (JammoMidiEvent*)(g_value_get_pointer(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_note_button_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
	JammoNoteButton* note_button;
	note_button = JAMMO_NOTE_BUTTON(object);

	switch (prop_id) {
		case PROP_START_EVENT:
			g_value_set_pointer (value, note_button->priv->start_event);
			break;
		case PROP_STOP_EVENT:
			g_value_set_pointer (value, note_button->priv->stop_event);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
        }
}

static void jammo_note_button_finalize(GObject* object) {
	G_OBJECT_CLASS(jammo_note_button_parent_class)->finalize(object);
}

static void jammo_note_button_dispose(GObject* object) {
	/*
	JammoNoteButton* note_button = JAMMO_NOTE_BUTTON(object);
	printf("dispose note_button\n");
	if (note_button->priv->start_event) {
		g_free(note_button->priv->start_event);
		note_button->priv->start_event = NULL;
	}
	if (note_button->priv->stop_event) {
		g_free(note_button->priv->stop_event);
		note_button->priv->stop_event = NULL;
	}
*/
	G_OBJECT_CLASS(jammo_note_button_parent_class)->dispose(object);
}

static void jammo_note_button_class_init(JammoNoteButtonClass* note_button_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(note_button_class);

	gobject_class->constructed = jammo_note_button_constructed;
	gobject_class->finalize = jammo_note_button_finalize;
	gobject_class->dispose = jammo_note_button_dispose;
	gobject_class->set_property = jammo_note_button_set_property;
	gobject_class->get_property = jammo_note_button_get_property;

	/**
	 * JammoNoteButton:start-event:
	 */
	g_object_class_install_property(gobject_class, PROP_START_EVENT,
	                                g_param_spec_pointer ("start-event",
	                                "Start Event",
	                                "",
	                                //NULL,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoNoteButton:stop-event:
	 */
	g_object_class_install_property(gobject_class, PROP_STOP_EVENT,
	                                g_param_spec_pointer ("stop-event",
	                                "Stop Event",
	                                "",
	                                //NULL,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	g_type_class_add_private(gobject_class, sizeof(JammoNoteButtonPrivate));
}


static void jammo_note_button_init(JammoNoteButton* note_button) {
	note_button->priv = G_TYPE_INSTANCE_GET_PRIVATE(note_button, JAMMO_TYPE_NOTE_BUTTON, JammoNoteButtonPrivate);
	//printf("jammo-note-button-init\n");
}


static gboolean on_clicked(TangleButton* button, gpointer user_data) {
	//printf("note_button: on_clicked\n");
	JammoNoteButton* note_button;
	note_button = JAMMO_NOTE_BUTTON(button);

	GstClockTime start=0;
	GstClockTime stop=0;
	if (note_button->priv->start_event) {
		jammomidi_print_event(note_button->priv->start_event);
		start = note_button->priv->start_event->timestamp;
	}
	if (note_button->priv->stop_event) {
		jammomidi_print_event(note_button->priv->stop_event);
		stop = note_button->priv->stop_event->timestamp;
	}
	printf("duration of this note-button: %f s\n",((long long unsigned)(stop-start)*1.0)/1000000000LLU);

	//If track is in eraser-mode, remove clicked note.
	ClutterActor* parent = clutter_actor_get_parent(CLUTTER_ACTOR(note_button));
	if (JAMMO_IS_MIDITRACK_VIEW(parent)) {
		if (jammo_miditrack_view_get_eraser_mode(JAMMO_MIDITRACK_VIEW(parent))) {
				clutter_container_remove_actor(CLUTTER_CONTAINER(parent), CLUTTER_ACTOR(note_button));
				return TRUE;
		}
	}
	else {
		printf("parent of note-button was not miditrack-view\n");
	}

	return FALSE;
}

