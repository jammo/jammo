/* 
 * File:   jammer-emmafile.c
 * Author: alex
 *
 * Created on February 21, 2010, 12:02 AM
 */

#include <stdlib.h>
#include <stdio.h>
#include <glib-object.h>
#include "../../configure.h"
#include "jammer-emmafile.h"

#define BUFFER_SIZE 6 

int create_emma(int motionBuffer[BUFFER_SIZE][3]) {
   FILE *emma_F;
   char *xml_filename;
   xml_filename =  g_strdup_printf("%s/emma.xml",configure_get_jammo_directory());
   emma_F = fopen (xml_filename, "w");
   int i;
   fprintf(emma_F, "<emma:emma version=\"1.0\" xmlns:emma=\"http://www.w3.org/2003/04/emma\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"xsi:schemaLocation=\"http://www.w3.org/2003/04/emma http://www.w3.org/TR/2009/REC-emma-20090210/emma.xsd\" xmlns=\"http://www.example.com/example\">\n<emma:sequence id=\"N900face\">\n ");
   for (i=0; i<BUFFER_SIZE; i++)
       {
               fprintf(emma_F,"<emma:interpretation id=\"coord%d\" emma:medium= \"tactile\" emma:mode=\"accelerometer\">\n<x>%d</x>\n<y>%d</y>\n<z>%d</z>\n<sampling_rate>1</sampling_rate>\n<sensitivity>20</sensitivity>\n</emma:interpretation>\n",i,motionBuffer[i][0],motionBuffer[i][1],motionBuffer[i][2]);
       }
   fprintf(emma_F, "</emma:sequence>\n</emma:emma>\n" );
   fclose(emma_F);
   return 0;
}

int create_emma_int(int interpretation) {
   FILE *emma_F;
   char *xml_filename;
   xml_filename =  g_strdup_printf("%s/emma-int.xml",configure_get_jammo_directory());
   emma_F = fopen (xml_filename, "w");
   fprintf(emma_F, "<emma:emma version=\"1.0\" xmlns:emma=\"http://www.w3.org/2003/04/emma\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"xsi:schemaLocation=\"http://www.w3.org/2003/04/emma http://www.w3.org/TR/2009/REC-emma-20090210/emma.xsd\" xmlns=\"http://www.example.com/example\">\n<emma:sequence id=\"N900face\">\n ");
   if (interpretation == 0){
        fprintf(emma_F,"<emma:interpretation id=\"coord\" emma:medium= \"tactile\" emma:mode=\"accelerometer\">\n<position>Face Down</position>\n<sampling_rate>1</sampling_rate>\n<sensitivity>20</sensitivity>\n</emma:interpretation>\n");
   }else if (interpretation ==1){
        fprintf(emma_F,"<emma:interpretation id=\"coord\" emma:medium= \"tactile\" emma:mode=\"accelerometer\">\n<position>Side Up</position>\n<sampling_rate>1</sampling_rate>\n<sensitivity>20</sensitivity>\n</emma:interpretation>\n");
   }else if (interpretation ==2){
        fprintf(emma_F,"<emma:interpretation id=\"coord\" emma:medium= \"tactile\" emma:mode=\"accelerometer\">\n<position>Face Up</position>\n<sampling_rate>1</sampling_rate>\n<sensitivity>20</sensitivity>\n</emma:interpretation>\n");
   }
   fprintf(emma_F, "</emma:sequence>\n</emma:emma>\n" );
   fclose(emma_F);
   return 0;
}
