/*
There are list of songs.
When pressing song-icon it will start playing alone ( on ad-hoc sequencer).
 Pressing another song-icon will stop previous song.

If vocal-button is toggled, vocal-version of track is used.


Last selected/listened will be used when user press 'OK'-button.
Cancel-button returns without modification.

*/
#include <glib-object.h>
#include <tangle.h>


#include "../../meam/jammo-meam.h"
#include "../../meam/jammo-backing-track.h"


#include "../jammo.h"
#include "../../configure.h"
#include "../file_helper.h"
#include "sequencer.h"
#include "sequencer_track_view.h"
#include <string.h>
#include "../../cem/cem.h"

//Only one of these can be toggled down at a time.
static gboolean vocal_selected = FALSE;
static gboolean melodic_selected = FALSE;

static gchar* last_played_filename=NULL;
static gchar* image_name_for_last_played=NULL;
static GList* visible_borders = NULL; //Actors for frames surrounding selected image
static ClutterActor* static_backingtrack = NULL; // When coming to backingtrack_selection this should be set.

static JammoSequencer* adhoc_sequencer=NULL;
static void on_sequencer_stopped(JammoSequencer* sequencer, gpointer user_data) {
	cem_add_to_log("Backintrack-selection: preview song listened to end",J_LOG_INFO);
	/*Nothing to really do.
	If user selects next song this adhoc_sequencer is freed.
	If user closes backingtrack_selection adhoc_sequencer is freed.
	*/
}



static void show_and_add_to_list(ClutterActor* actor, gpointer none) {
	//printf("show borders and add them to list\n");
	clutter_actor_show(actor);
	visible_borders=g_list_append(visible_borders,actor);
}

static void clear_list_and_make_new(){
	if (visible_borders){
		g_list_foreach(visible_borders, (GFunc)clutter_actor_hide,NULL);
		g_list_free(visible_borders);
	}

	visible_borders = NULL; //make new
}

void backingtrack_selection_set_track_view(ClutterActor* actor){
	static_backingtrack = actor;
}

gboolean backingtrack_song_clicked (TangleButton* tanglebutton, gpointer none){
	const gchar* name = clutter_actor_get_name(CLUTTER_ACTOR(tanglebutton));
	gchar* message = g_strdup_printf("Backintrack-selection: Song '%s' clicked",name);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);
	//Remove and add frames for selected.
	clear_list_and_make_new(); //for visible_borders
	ClutterActor* actor = tangle_button_get_normal_background_actor(tanglebutton);
	clutter_container_foreach(CLUTTER_CONTAINER(actor), CLUTTER_CALLBACK(show_and_add_to_list), NULL);

	JammoSequencer* global_sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));

	//Vocal or accompaniment only
	gchar* prefix;
	gchar* postfix=""; //e.g. language
	if (vocal_selected) {
		prefix = "SaV";
		//TODO: use menu for flags to determine this
		postfix="_fi";


		//If vocal is used, set tempo to 110. (As designed!)
		if (strcmp(name,"pendolino")==0) //This is exception
			jammo_sequencer_set_tempo(global_sequencer,130);
		else if (strcmp(name,"LaBamba")==0)   //This is exception
			jammo_sequencer_set_tempo(global_sequencer,130); //TODO: currently start 130_fi.
			//postfix=""; //It has all tempo-variations. It doesn't have postfix for language
		else if (strcmp(name,"SiyaHamba")==0)   //This is exception
			postfix=""; //It has all tempo-variations. It doesn't have postfix for language
		else if (strcmp(name,"banana")==0)   //This is exception
			postfix=""; //It has all tempo-variations. It doesn't have postfix for language
		else if (strcmp(name,"brotherjohn")==0)   //This is exception
				jammo_sequencer_set_tempo(global_sequencer,90);
		else if ( (strcmp(name,"ScarboroughFairGrime")==0) ||     //This is exception
              (strcmp(name,"ScarboroughFairFolkMetal")==0) ||
              (strcmp(name,"ScarFairSouthAsian")==0) )
			postfix="_en";  //These have all tempo-variations. Language is only for en.


		else
			jammo_sequencer_set_tempo(global_sequencer,110);
	}
	else if (melodic_selected)
		prefix ="SaM";
	else
		prefix ="Sa";

	gchar* backing_track_filename = g_strdup_printf("backingtracks/%s_%s_$t_$p%s.ogg",prefix,name,postfix);
	message=g_strdup_printf("Backintrack-selection: looking file '%s' ",backing_track_filename);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);
	if (adhoc_sequencer){
		g_object_set(adhoc_sequencer,"play",FALSE,NULL);
		g_object_unref(adhoc_sequencer);
		adhoc_sequencer=NULL;
	}

	adhoc_sequencer = jammo_sequencer_new();
	jammo_sequencer_set_tempo(adhoc_sequencer,jammo_sequencer_get_tempo(global_sequencer));
	jammo_sequencer_set_pitch(adhoc_sequencer,jammo_sequencer_get_pitch(global_sequencer));
	//printf("tempo and pitch set\n");

	JammoTrack* track = JAMMO_TRACK(jammo_backing_track_new(backing_track_filename));
	jammo_sequencer_add_track(adhoc_sequencer, JAMMO_TRACK(track));
	if (last_played_filename)
		g_free(last_played_filename);
	last_played_filename = g_strdup_printf("%s",backing_track_filename);
	image_name_for_last_played = g_strdup_printf("icon_%s.png",name);

	g_signal_connect(adhoc_sequencer, "stopped", G_CALLBACK(on_sequencer_stopped), NULL);
	jammo_sequencer_play(adhoc_sequencer);

	return TRUE;
}


gboolean backingtrack_selection_vocal_clicked(TangleButton* tanglebutton, gpointer none){
	vocal_selected = tangle_button_get_selected(tanglebutton);

	if (vocal_selected) {
	 cem_add_to_log("Backintrack-selection: vocal selected",J_LOG_USER_ACTION);
	 tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("backingtrack-selection-melodical")),FALSE);
	 melodic_selected=FALSE;
	}
	else {
		 cem_add_to_log("Backintrack-selection: vocal deselected",J_LOG_USER_ACTION);
	}

	return TRUE;
}

gboolean backingtrack_selection_melodical_clicked(TangleButton* tanglebutton, gpointer none){
	melodic_selected = tangle_button_get_selected(tanglebutton);

	if (melodic_selected) {
	 cem_add_to_log("Backintrack-selection: melodic selected",J_LOG_USER_ACTION);
	 tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("backingtrack-selection-vocal")),FALSE);
	 vocal_selected=FALSE;
	}
	else {
		 cem_add_to_log("Backintrack-selection: melodic deselected",J_LOG_USER_ACTION);
	}

	return TRUE;
}

static void end_backingtrack_selection() {
	if (adhoc_sequencer) {
		g_object_set(adhoc_sequencer,"play",FALSE,NULL);
		g_object_unref(adhoc_sequencer);
		adhoc_sequencer=NULL;
	}

	if (last_played_filename) {
		g_free(last_played_filename);
		last_played_filename=NULL;
	}

	if (image_name_for_last_played) {
		g_free(image_name_for_last_played);
		image_name_for_last_played=NULL;
	}
	static_backingtrack = NULL;
	tangle_actor_hide_animated(TANGLE_ACTOR(jammo_get_actor_by_id("backingtrack-selection-view")));
	return_to_fullsequencer_gui();
}


gboolean backingtrack_selection_ok_clicked(TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("Backintrack-selection: OK clicked",J_LOG_USER_ACTION);

	if (last_played_filename) {
		ClutterActor* track_view = static_backingtrack;
		if (track_view) {
			//audio
			JammoTrack* track = NULL;
			g_object_get(track_view,"track",&track,NULL);
			if (!track) {
				cem_add_to_log("Backintrack-selection: No track found.",J_LOG_FATAL);
				return TRUE;
			}

			g_object_set(track,"filename",last_played_filename,NULL);
			//Number of slots is changed.
			JammoSequencer* sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));
			guint64 duration_of_sequencer = jammo_sequencer_get_duration(sequencer);
			guint64 duration_of_track = jammo_track_get_duration(track);


			gchar* message = g_strdup_printf("Backintrack changed: old sequencer duration was: %" G_GUINT64_FORMAT "", duration_of_sequencer);
			cem_add_to_log(message,J_LOG_DEBUG);
			g_free(message);

			message = g_strdup_printf("Backintrack changed: new sequencer duration will be: %" G_GUINT64_FORMAT "", duration_of_track);
			cem_add_to_log(message,J_LOG_DEBUG);
			g_free(message);

			sequencer_track_view_fix_cursor();

			ClutterActor* container = jammo_get_actor_by_id("fullsequencer-container-for-tracks");
			TangleActorIterator actor_iterator;
			ClutterActor* child;

			for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(container), &actor_iterator); (child = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
				if (JAMMO_IS_EDITING_TRACK_VIEW(child)) {
					guint64 duration_of_one_slot;
					g_object_get(child,"slot-duration",&duration_of_one_slot,NULL);
					gint new_number_of_slots = duration_of_track/duration_of_one_slot;
					g_object_set(child,"n-slots",new_number_of_slots,NULL);

					message = g_strdup_printf("New number of slots for %s: %d",clutter_actor_get_name(child),new_number_of_slots);
					cem_add_to_log(message,J_LOG_DEBUG);
					g_free(message);

					int old_number_of_slots = duration_of_sequencer/duration_of_one_slot;
					int i;
					for (i=new_number_of_slots;i<old_number_of_slots;i++){
						jammo_editing_track_view_remove_jammo_sample_button_from_slot(JAMMO_EDITING_TRACK_VIEW(child),i);
					}
				}
			}

			//image
			gchar* image_file = g_build_filename("backingtracks",image_name_for_last_played,NULL);
			//printf ("image_file '%s'\n",image_file);
			ClutterActor* new_back = tangle_texture_new(image_file);
			if (new_back==NULL) {
				cem_add_to_log("Backintrack-selection: No image found, using gray texture",J_LOG_FATAL);
				new_back = clutter_rectangle_new_with_color (clutter_color_new(100,100,100,100));
				clutter_actor_set_size(new_back, clutter_actor_get_width(track_view), clutter_actor_get_height(track_view));
			}
			else {
				g_object_set(new_back,"repeat-x", TRUE,NULL);
			}
			tangle_widget_set_background_actor(TANGLE_WIDGET(track_view),new_back);

		}
	}

	end_backingtrack_selection();
	return TRUE;
}


gboolean backingtrack_selection_cancel_clicked(TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("Backintrack-selection: cancel clicked",J_LOG_USER_ACTION);
	end_backingtrack_selection();

	return TRUE;
}

#define step_height 130
static void roll_song_list(int direction) {
	ClutterActor* scrolling_view = jammo_get_actor_by_id("backingtrack-selection-container");
	ClutterAction* action = tangle_actor_get_action_by_type(scrolling_view,TANGLE_TYPE_SCROLL_ACTION);
	float old_value;
	g_object_get(action, "offset-y", &old_value, NULL);
	 //Rounding
	int steps = old_value / step_height;
	old_value = steps * step_height;
	tangle_object_animate(G_OBJECT(action), CLUTTER_EASE_IN_OUT_QUAD, 250, "offset-y", old_value + (direction*step_height), NULL);
}

gboolean backingtrack_up_arrow_clicked(TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("Backintrack-selection: up arrow clicked",J_LOG_USER_ACTION);
	roll_song_list(-1); //up

	return TRUE;
}


gboolean backingtrack_down_arrow_clicked(TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("Backintrack-selection: down arrow clicked",J_LOG_USER_ACTION);
	roll_song_list(1); //down

	return TRUE;
}

//Set clamping after view is visible. It does not harm when called second time.
void backingtrack_selection_show_completed(TangleActor* tangle_actor, gpointer data){
	clear_list_and_make_new(); //for visible_borders
	ClutterActor* actor = jammo_get_actor_by_id("backingtrack-selection-container");
	if (actor) {
		ClutterAction* action = tangle_actor_get_action_by_type(actor, TANGLE_TYPE_SCROLL_ACTION);
		if (action)
			g_signal_connect_swapped(action, "clamp-offset-y", G_CALLBACK(tangle_widget_clamp_child_boundaries), actor);
	}

}

