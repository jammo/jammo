/**sequencer_track_view.h is part of JamMo.
License: GPLv2, read more from COPYING

SequencerTrackView is struct which contains jammo-track-view (or jammo-miditrack-view)
and buttons for volume, muting
and label
and some metadata.

*/

#ifndef SEQUENCER_SEQUENCER_H_
#define SEQUENCER_SEQUENCER_H_

#include <stdbool.h>

/*
 * Wraps TrackView and buttons & labels into one clean package
 * to add and remove TrackViews easily to sequencer
 */
typedef struct {
	ClutterActor *control_panel;
	ClutterActor *track_view;
	gint track_type;
} SequencerTrackView;

/*
 * Contains all possible SequencerTrackView types
 */
enum {
	INSTRUMENT_TRACK_VIEW = 1,
	SLIDER_TRACK_VIEW,
	AUDIO_TRACK_VIEW,
	EDITING_TRACK_VIEW,
	BACKING_TRACK_VIEW
};


void sequencer_track_view_set_duration(guint64 duration);

ClutterActor* sequencer_track_view_create_editing_track(gboolean muted, gfloat volume, gint e_type, gint slot_width, const gchar* name_of_track_view);
ClutterActor* sequencer_track_view_create_instrument_track(gboolean muted, gfloat volume, gint instrument_type, const gchar*
name_of_track_view);
ClutterActor* sequencer_track_view_create_backing_track(gboolean muted, gfloat volume, const gchar* audio, const gchar* image, const gchar* name_of_track_view);

void sequencer_track_view_fix_cursor();
bool save_sequencer_state();

#endif /* SEQUENCER_SEQUENCER_H_ */
