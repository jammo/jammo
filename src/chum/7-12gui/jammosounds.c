/*
License: GPLv2, read more from COPYING

This file contains the functionality of JamMo Sounds menu.
 */

#include "startmenu.h"
#include <string.h>
#include "jammosounds.h"
#include "community_utilities.h"
#include "../../cem/cem.h"
#include <stdlib.h>

static void autoscroll_to_correct_pos(ClutterActor *listToScroll);

static ClutterContainer *list = 0;
static gint listItemHeight = 0;
GList *loops = 0;


void start_jammosounds(){

	GList* l = NULL; //Iterator
	int numberOfLoops = 0; //this should contain the number of loops in the list
	loops = 0;

	cem_add_to_log("Starting JamMoSounds", J_LOG_DEBUG); 

	ClutterActor* mainview;
	
	mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);
	
	ClutterActor* jammosounds_view = jammo_get_actor_by_id("jammosounds-view");

	if(jammosounds_view){

		load_loops_from_server(); //Get loops
		
		numberOfLoops = g_list_length(loops);

		clutter_actor_show(CLUTTER_ACTOR(jammosounds_view));
		
		if(numberOfLoops > 5){
			list = CLUTTER_CONTAINER(jammo_get_actor_by_id("jammosounds-songs-list-scrollable"));	
		}else{
			list = CLUTTER_CONTAINER(jammo_get_actor_by_id("jammosounds-songs-list-unscrollable"));	
		}

		for(l = loops; l; l = l->next){
			add_loop_to_list(l->data, numberOfLoops);	
		}

		ClutterAction *listAction = tangle_actor_get_action_by_type(CLUTTER_ACTOR(list), TANGLE_TYPE_SCROLL_ACTION);
		g_signal_connect_swapped(listAction, "clamp-offset-y", G_CALLBACK(autoscroll_to_correct_pos), list);
	
	}else{
		cem_add_to_log("can't find 'jammosounds-view' ", J_LOG_ERROR);
	}
}


gboolean add_loop_to_list(gpointer data, int numberOfLoops){

	loop_type *loop = data;
	
	gchar* listImage_filename = g_strdup_printf("%s/communitymenu/community_jammosounds_bar.png", DATA_DIR);
	gchar* importButton_filename = g_strdup_printf("%s/communitymenu/community_icon_toloopgame.png", DATA_DIR);
	gchar* arrowImage_filename = g_strdup_printf("%s/communitymenu/community_rightarrow.png", DATA_DIR);
	
	ClutterActor* listImage = tangle_texture_new(listImage_filename);
	ClutterContainer *listContainer = CLUTTER_CONTAINER(clutter_group_new());
	ClutterActor *importButton = tangle_button_new_with_background_actor(tangle_texture_new(importButton_filename));			
	ClutterActor *arrowImage = tangle_texture_new(arrowImage_filename);
	ClutterActor *instrument;
	ClutterActor *text;
	ClutterActor *loopName;
	
	if(listImage == NULL || arrowImage == NULL)
		return FALSE;	

	if(listItemHeight == 0)
		listItemHeight = clutter_actor_get_height(listImage);

	clutter_container_add_actor(listContainer, listImage);
	//clutter_container_add_actor(listContainer, listImage);

	g_free(listImage_filename);
	g_free(importButton_filename);
	g_free(arrowImage_filename);
	
	//TODO: add instrument icon depending on the instrument
	gchar* instrument_filename = g_strdup_printf("%s/wheel_game/piano.png", DATA_DIR);
	instrument = tangle_texture_new(instrument_filename);  
	clutter_actor_set_position(instrument, 130, 0);
	clutter_actor_set_height(instrument, listItemHeight);
	clutter_container_add_actor(listContainer, instrument);
	g_free(instrument_filename);
	
	text = clutter_text_new_with_text(TEXT_NORMAL, loop->loopname);	
	loopName = tangle_button_new_with_background_actor(text);
	clutter_actor_set_name(loopName, loop->filename);

	clutter_actor_set_position(loopName, 230, listItemHeight/5*2);
	clutter_container_add_actor(listContainer, loopName);	
		
	clutter_actor_set_position(arrowImage, 655, 15);
	
	clutter_container_add_actor(listContainer, arrowImage);

	clutter_actor_set_position(CLUTTER_ACTOR(importButton), 690, 15);

	clutter_container_add_actor(listContainer, CLUTTER_ACTOR(importButton));
	
	g_signal_connect_swapped(importButton, "clicked", G_CALLBACK(import_selected_loop), 
				clutter_actor_get_parent(importButton));

	g_signal_connect(loopName, "clicked", G_CALLBACK(song_on_clicked_function), NULL);
	g_object_set(loopName, "hold-timeout", 1500, NULL);
	g_signal_connect(loopName, "held", G_CALLBACK(song_on_held_function), NULL);

	
	clutter_container_add_actor(list, CLUTTER_ACTOR(listContainer));
	
	if(numberOfLoops <= 6)
		clutter_actor_set_height(CLUTTER_ACTOR(list), listItemHeight * numberOfLoops+1);	
	else
		clutter_actor_set_height(CLUTTER_ACTOR(list), listItemHeight * 6);		

	return TRUE;

}

void clear_loops(GList* loops_){

	loops_ = g_list_first(loops_);
	loop_type *temp = 0;

	if(loops_ != NULL)
	{
		temp = loops_->data;

		if(temp->loopid != NULL)
			free(temp->loopid);
			
		if(temp->filename != NULL)
			free(temp->filename);

		if(loops_->data != NULL)
			free(loops_->data);
	
		while(loops_->next != NULL)
		{
			loops_ = loops_->next;			
			temp = loops_->data;

			if(temp->loopid != NULL)
			free(temp->loopid);
			
			if(temp->filename != NULL)
				free(temp->filename);

			if(loops_->data != NULL)
				free(loops_->data);				
		}
	g_list_free(loops_);
	loops_ = NULL;
	
	}//if
}

void import_selected_loop(ClutterActor *loop){

	const char* filepath = clutter_actor_get_name(loop);

	printf("jammosounds: %s\n", filepath);
	cem_add_to_log("Importing loop...", J_LOG_DEBUG);

	clear_loops(loops);
	community_utilities_clear_container_recursively(list);

	//TODO: check if loop already exists in hd
	//TODO: download loop.ogg from server
}

gboolean load_loops_from_server() 
{	
	gboolean returnvalue = FALSE;
	
	//TODO: load all_loops.json from server and check that the file was received
	
	char * all_loops_filepath = g_strdup_printf("%s/all_loops.json", configure_get_temp_directory()); //Only for testing!
	int type = 0; //loops
	
	if(community_utilities_parse_songs_from_file(all_loops_filepath, &loops, type))
		returnvalue = TRUE;
	else
		returnvalue = FALSE;

	g_free(all_loops_filepath);
	
	//TODO: download user profiles etc.?
		
	return returnvalue;
}

gboolean end_jammosounds(TangleButton *tanglebutton, gpointer none){

	community_utilities_clear_container_recursively(list);

	clear_loops(loops);
   
	if(strcmp(clutter_actor_get_name(CLUTTER_ACTOR(tanglebutton)), "gotocommunity") == 0)
		startmenu_goto_communitymenu(tanglebutton, none);
		
	else
	   startmenu_goto_startmenu(tanglebutton, none);

	return TRUE;
}

void autoscroll_to_correct_pos(ClutterActor *listToScroll){

	gint slot;
	gfloat not_used, offset;
	ClutterAction *action = tangle_actor_get_action_by_type(listToScroll, TANGLE_TYPE_SCROLL_ACTION);
	
	tangle_scroll_action_get_offsets(TANGLE_SCROLL_ACTION(action), &not_used, &offset);
	slot = (gint)offset % (gint)listItemHeight < listItemHeight/2  	? (gint)(offset) / listItemHeight  
							  		: (gint)(offset) / listItemHeight + 1;

	tangle_object_animate(G_OBJECT(action), CLUTTER_EASE_IN_OUT_QUAD, 300, "offset-y", 
				(gfloat)(slot*listItemHeight), NULL);

}

