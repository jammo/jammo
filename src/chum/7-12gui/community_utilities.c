#include <tangle.h>
#include <glib-object.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "mysongs.h"
#include "community_utilities.h"
#include "../jammo.h"
#include "../../meam/jammo-sample.h"
#include "../../cem/cem.h"
#include "../../configure.h"
#include "../file_helper.h"
#include "sequencer.h"


static gboolean renaming_loop = FALSE;

static void rename_loop(ClutterActor* loopName, int type);
static void hide_song_label();

//Long click:  rename the item
void song_on_held_function(TangleButton* button, gpointer user_data) {
	ClutterActor* actor=CLUTTER_ACTOR(button);

	g_object_set_data(G_OBJECT(actor),"handler_for_longclick",GUINT_TO_POINTER(0));

	const char* filename = clutter_actor_get_name(actor);
	const char* projects = configure_get_projects_directory();
	const char* finalized = configure_get_finalized_directory();
	int type = -1;

	if(strncmp(filename, projects, strlen(projects)) == 0){ // project
		type = 0;
	}else if(strncmp(finalized, filename, strlen(finalized)) == 0){ // finished song
		type = 1;
	}

	rename_loop(actor, type);
}


//Short click: play
void song_on_clicked_function(TangleButton* button, gpointer user_data) {

	ClutterActor* actor = CLUTTER_ACTOR(button);
	const char* filename = clutter_actor_get_name(actor);
	const char* projects = configure_get_projects_directory();
	const char* finalized = configure_get_finalized_directory();
	int type = -1;

	if(strncmp(filename, projects, strlen(projects)) == 0){ // project
		type = 0;
	}else if(strncmp(finalized, filename, strlen(finalized)) == 0){ // finished song
		type = 1;
	}

	if(renaming_loop){
		rename_loop(actor, type);
	}else{

		if(type == 0){
			start_sequencer_with_this_file(actor, NULL);
		}else if(type == 1){
			community_utilities_play_selected_loop(actor);
		}else{
			cem_add_to_log("Invalid loop name\n", J_LOG_ERROR);
		}
	}
}


/**
 * Renames song/project and song's labelfile in list and in hard-drive
 * 
 * parameters:
 * -loopName is clutter actor holding the filename
 * -type: 0=finished song, 1=project
**/
static void rename_loop(ClutterActor *loopName, int type){

	static gboolean firstCall = TRUE;

	ClutterActor *text = tangle_button_get_normal_background_actor(TANGLE_BUTTON(loopName));
	const char* givenname = clutter_text_get_text(CLUTTER_TEXT(text));
	const char* filename = clutter_actor_get_name(loopName);
	gchar * newpath = NULL;
	int i = 0, j = 0, type_length = 0, path_length = 0, userid_length = 10;
	int length = strlen(filename);
	char oldpath[length];

	gchar* temp = g_strdup_printf("%s", filename);

	if(type == 0){ //projects
		path_length = strlen(configure_get_projects_directory());
		type_length = 5; //.json
	}else if(type == 1 || type == 3){ //songs
		path_length = strlen(configure_get_finalized_directory());
		type_length = 4; //.ogg
	}
		
	for(i = path_length + 1; i < (length-(type_length + userid_length)); i++){
		oldpath[j] = temp[i];
		j++;
	}
	oldpath[j] = 0;
	g_free(temp);

	gchar * path = NULL;

	path = g_strdup_printf("%s/label_%d_%s.csv", configure_get_jammo_directory(), 
			/*gems_profile_manager_get_userid(NULL)*/TEST_USERID, oldpath);

	if(firstCall == TRUE){
		firstCall = FALSE;
		renaming_loop = TRUE;
		cem_add_to_log("Renaming text...", J_LOG_DEBUG);

		clutter_text_set_editable (CLUTTER_TEXT(text), TRUE);
		clutter_actor_set_reactive(text, TRUE);
		clutter_actor_grab_key_focus(text);
	}else{
		firstCall = TRUE;
		cem_add_to_log("Done.", J_LOG_DEBUG);

		clutter_text_set_editable (CLUTTER_TEXT(text), FALSE);
		clutter_actor_set_reactive(text, FALSE);
	
		if(type == 0){

			const char* projects = configure_get_projects_directory();
			gchar* new_projects_name = g_strdup_printf("%s/%s_%d.json",projects,givenname,/*gems_profile_manager_get_userid(NULL)*/TEST_USERID);
			
			if(!community_utilities_file_exist(new_projects_name)){ //if file with same name exists we don't rename

				if(community_utilities_file_exist(path)){ //labelfile exists
					newpath = g_strdup_printf("%s/label_%d_%s.csv", configure_get_jammo_directory(), 
								/*gems_profile_manager_get_userid(NULL)*/TEST_USERID, givenname);
					rename(path, newpath);
				}
				rename(filename, new_projects_name);
				clutter_actor_set_name(loopName, new_projects_name);			
			}
			g_free(new_projects_name);

		}else if(type == 1){

			const char *finalized = configure_get_finalized_directory();
			gchar* newname = g_strdup_printf("%s/%s_%d.ogg",finalized,givenname,/*gems_profile_manager_get_userid(NULL)*/TEST_USERID);

			if(!community_utilities_file_exist(newname)){ //if file with same name exists we don't rename

				if(community_utilities_file_exist(path)){ //labelfile exists
					newpath = g_strdup_printf("%s/label_%d_%s.csv", configure_get_jammo_directory(), 
								/*gems_profile_manager_get_userid(NULL)*/TEST_USERID, givenname);
					rename(path, newpath);
				}
				rename(filename, newname);
				clutter_actor_set_name(loopName, newname);
			}
			g_free(newname);
		}
		renaming_loop = FALSE;
	}
	g_free(path);
	g_free(newpath);
}

/**
 * Plays selected song. 
 * 
 * parameters:
 * -actor is clutter actor holding the filename
**/
void community_utilities_play_selected_loop(ClutterActor *actor){
	static JammoSample *sample = NULL;
	const char *filename = clutter_actor_get_name(actor);
	printf("play loop %s\n", filename);

	if(sample == NULL){

		cem_add_to_log("Starting loop", J_LOG_DEBUG);

		if(actor == NULL){
			cem_add_to_log("Cannot play file because selected actor is invalid", J_LOG_DEBUG);
		}else{
			gchar * message = g_strdup_printf("Playing file: %s", filename);
			cem_add_to_log(message, J_LOG_DEBUG);
			g_free(message);

			sample = jammo_sample_new_from_file(filename);
		}
		jammo_sample_play(sample);
	}else{
		cem_add_to_log("loop stop", J_LOG_DEBUG);
		jammo_sample_stop(sample);
		g_object_unref(sample);
		sample = NULL;
	}
}

/**
 * Reads label information from labelfile given as parameter, and shows the label viewer
**/
void community_utilities_start_label_viewer(show_label_params* params){
	cem_add_to_log("Starting record label view.", J_LOG_DEBUG);

	FILE *file;
	gchar data[200];	
	gchar *recordName;
	gchar *composer;
	gchar *image;
	if((file = fopen(params->path, "r")) != 0){
	
		if(fread(data, sizeof(gchar), 200, file) != 0){
		
			recordName = strtok(data, ",");
			composer = strtok(NULL, ",");
			image = strtok(NULL, ",");
				
			if(strcmp(recordName, "0") != 0 || strcmp(composer, "0") != 0 || strcmp(image, "0") != 0){
				 
				if(strcmp(params->parentview, "jammosongs") == 0) 				 
					tangle_actor_hide_animated(TANGLE_ACTOR(jammo_get_actor_by_id("jammosongs-view")));
					
				if(strcmp(params->parentview, "mysongs") == 0) 				 
					tangle_actor_hide_animated(TANGLE_ACTOR(jammo_get_actor_by_id("mysongs-view")));
					
				clutter_actor_show(jammo_get_actor_by_id("record-label-viewer"));
				
				g_signal_connect_swapped(jammo_get_actor_by_id("record-label-back-button"), "clicked", G_CALLBACK(hide_song_label), params->parentview);
								
				if(strcmp(recordName, "0") != 0)
					clutter_text_set_text(CLUTTER_TEXT(jammo_get_actor_by_id("record-label-name")), recordName);
				else clutter_text_set_text(CLUTTER_TEXT(jammo_get_actor_by_id("record-label-name")), "");
					
				if(strcmp(composer, "0") != 0)
					clutter_text_set_text(CLUTTER_TEXT(jammo_get_actor_by_id("record-label-composer")), composer);
				else clutter_text_set_text(CLUTTER_TEXT(jammo_get_actor_by_id("record-label-composer")), "");
				
				if(strcmp(image, "0") != 0){
				
					tangle_texture_set_from_file(TANGLE_TEXTURE(jammo_get_actor_by_id("record-label-image")), image);
					clutter_actor_show(jammo_get_actor_by_id("record-label-image"));
					
				}else clutter_actor_hide(jammo_get_actor_by_id("record-label-image"));
				
			
			}
			
		}
		fclose(file);
	}
		
}

/**
 * Changes view from label viewer to parent view
**/
static void hide_song_label(gchar* parentview){
	tangle_actor_hide_animated(TANGLE_ACTOR(jammo_get_actor_by_id("record-label-viewer")));
	
	if(strcmp(parentview, "jammosongs") == 0) 				 
		clutter_actor_show(jammo_get_actor_by_id("jammosongs-view"));	
		
	if(strcmp(parentview, "mysongs") == 0) 				 
		clutter_actor_show(jammo_get_actor_by_id("mysongs-view"));			
		
}


/*
 * Function for checking whether file exists or not.
 * 
 * parameters:
 * -filename is the fullpath of the file
 */
gboolean community_utilities_file_exist(const gchar* filename) {
	struct stat buffer;

	if(stat(filename, &buffer) == 0){
		return TRUE;
	}
	else {
		gchar * message = g_strdup_printf("Cannot find file: %s", filename);
		cem_add_to_log(message, J_LOG_ERROR);
		g_free(message);
		return FALSE;
	}
}


#ifdef this_is_not_yet_used
/**
 * Deletes all files inside jammo temp directory. This should be called before jammo is terminated.  
**/
static gboolean clear_community_temp_directory(){
	//NOTE: This should be called before jammo is terminated so that temp files will be removed from hard-drive.
	
	gchar* command = g_strdup_printf("rm -f %s/*.*",configure_get_temp_directory());
	gboolean returnvalue = FALSE;

	if(system(command) != 0){
    		cem_add_to_log("Error clearing jammo temp director", J_LOG_ERROR);
		returnvalue = FALSE;
		
	}else{
  		cem_add_to_log("Jammo temp directory was sucessfully cleared", J_LOG_ERROR);
		returnvalue = TRUE;
	}
	g_free(command);
	return returnvalue;
}
#endif

/**
 * Function clears container that may contain other containers.
 *
 * parameters:
 * -listToClear is the container to be cleared
**/
void community_utilities_clear_container_recursively(ClutterContainer *listToClear){

	if (CLUTTER_IS_ACTOR(listToClear)){
		//Revert back it's scrolling parameters
		ClutterAction* scroll_action = tangle_actor_get_action_by_type(CLUTTER_ACTOR(listToClear),TANGLE_TYPE_SCROLL_ACTION);
		if (scroll_action) {
			tangle_scroll_action_set_thresholds(TANGLE_SCROLL_ACTION(scroll_action), 30.0,30.0);
		}
	}
	int i = 0;
	GList *listItems;
	
	if(listToClear != NULL){
	 	
		listItems = clutter_container_get_children(listToClear); 
		
		if(listItems != NULL){
		
			do{			
				if(i > 0)
					listItems = listItems->next;
								
				clutter_container_remove_actor(CLUTTER_CONTAINER(listToClear), CLUTTER_ACTOR(listItems->data));
				i++;
	
			}while(listItems->next != NULL);
		
		g_list_free(listItems);

		}

	}
	listToClear = 0;	

}

/**
 * Function creates buttons from each .png type file in the directory.
 * 
 * parameters:
 * -path is the path of the directory
 * -list is a container where the buttons are added
 * -funtion is a pointer to a function which handles button press events
 * -width is the width of the button
 * -height is the height of the button
**/
gboolean community_utilities_make_buttons_from_directory(const char* path, ClutterContainer* scrolling_container, GCallback function, int width, int height){
	ClutterActor *button = 0;
	char result[MAX_FILES_IN_DIR][MAX_FILE_NAME_LEN];
	int numberOfImages;
	
	numberOfImages = community_utilities_open_and_arrange_dir(path, ".png", result);
	
	for(int i = 0; i < numberOfImages; i++){
		gchar* image_path = g_strdup_printf("%s%s", path,result[i]);
		community_utilities_add_single_button(button, scrolling_container, image_path, width, height, function);
		g_free(image_path);
	}

	if (numberOfImages*width <= 800){
		//Do not scroll it, they fit.
		ClutterAction* scroll_action = tangle_actor_get_action_by_type(CLUTTER_ACTOR(scrolling_container),TANGLE_TYPE_SCROLL_ACTION);
		tangle_scroll_action_set_thresholds(TANGLE_SCROLL_ACTION(scroll_action), 30000.0,3000.0);

		//Adjust position
		g_object_set(G_OBJECT(scroll_action), "offset-x", 0.0, NULL);

		//If scroller is very this moment scrolling, this is needed to stop it
		tangle_object_animate(G_OBJECT(scroll_action),  CLUTTER_EASE_IN_OUT_QUAD, 50, "offset-x", 0.0, NULL);

	clutter_actor_set_width(CLUTTER_ACTOR(scrolling_container),numberOfImages*width);
	}
	else
		clutter_actor_set_width(CLUTTER_ACTOR(scrolling_container),800.0);

	return TRUE;
}	

/**
 * Function creates and adds a single button into container.
 *
 * parameters:
 * -button is null pointer to a button to be created
 * -list is a container where button is added
 * -path is a path to image of the button
 * -width is the width of the button
 * -height is the height of the button
 * -funtion is a pointer to a function which handles button press events
**/
gboolean community_utilities_add_single_button(ClutterActor* button, ClutterContainer* list, char* path, int width, int height, GCallback function){
	//printf("community_utilities_add_single_button '%s'\n",path);
	button = tangle_button_new_with_background_actor(tangle_texture_new(path));
	clutter_actor_set_name(button, path);
	clutter_container_add_actor(list, button);
	clutter_actor_set_size(button, height, width);
	g_signal_connect_swapped(button, "clicked", G_CALLBACK(function), button);

	return TRUE;
}

/**
 * A callback funtion that limits the line count of editable textfield.
 *
 * parameters:
 * -pointer to a textfield structure that contains info about how to limit line count etc.
**/
void community_utilities_limit_line_count(TextField *text){

	gchar *textField = malloc((text->max_characters+1) * sizeof(gchar));
	PangoLayout *layout;
	int line_index;
	
	strncpy(textField, clutter_text_get_text(CLUTTER_TEXT(text->text)), text->max_characters);
	textField[text->max_characters+1] = 0;

	layout = clutter_text_get_layout(CLUTTER_TEXT(text->text));

	while(pango_layout_get_line_count(layout) > text->lineCount){
			line_index = clutter_text_get_cursor_position(CLUTTER_TEXT(text->text));

			if(line_index == -1){
				textField[strlen(textField)-1] = 0;
			}else{
				textField[line_index] = 0;
				strcat(textField, textField + line_index + 1);
				
				clutter_text_set_cursor_position(CLUTTER_TEXT(text->text), line_index == 0 ? 0 : line_index-1);
			}
			
			g_signal_handler_disconnect(text->text, text->handler_id);
			clutter_text_set_text(CLUTTER_TEXT(text->text), textField); 
			text->handler_id = g_signal_connect_swapped(text->text, "text-changed", (GCallback)community_utilities_limit_line_count, text);
			layout = clutter_text_get_layout(CLUTTER_TEXT(text->text));
	}
	
	g_free(textField);
		
}

/**
 * Frees dynamically allocated memory from GList that contains pointers to message structures.
**/
void community_utilities_clear_messagelist(GList* messages_){

	messages_ = g_list_first(messages_);

	message_type *temp = 0;

	if(messages_ != NULL){
		
		temp = messages_->data;

		if(temp->msg != NULL)
			free(temp->msg);
			
		if(temp->filepath != NULL)
			free(temp->filepath);

		if(messages_->data != NULL)
			free(messages_->data);
	
		while(messages_->next != NULL)
		{
			messages_ = messages_->next;
				
			temp = messages_->data;

			if(temp->msg != NULL)
				free(temp->msg);				
			
			if(temp->filepath != NULL)
				free(temp->filepath);				
				
			if(messages_->data != NULL)
				free(messages_->data);				
		}
	g_list_free(messages_);
	
	messages_ = NULL;
	
	}
}


/**
 * Frees dynamically allocated memory from GList that contains pointers to song_type structures.
**/
void community_utilities_clear_songlist(GList* songs){
	songs = g_list_first(songs);

	song_type *temp = 0;

	if(songs != NULL)
	{
		temp = songs->data;

		if(temp->songid != NULL)
			free(temp->songid);
			
		if(temp->filename != NULL)
			free(temp->filename);

		if(temp->cover != NULL)
			free(temp->cover);
			
		if(temp->comments_path != NULL)
			free(temp->comments_path);

		if(songs->data != NULL)
			free(songs->data);
	
		while(songs->next != NULL)
		{
			songs = songs->next;
				
			temp = songs->data;

			if(temp->songid != NULL)
			free(temp->songid);
			
			if(temp->filename != NULL)
				free(temp->filename);

			if(temp->cover != NULL)
				free(temp->cover);
			
			if(temp->comments_path != NULL)
				free(temp->comments_path);

			if(songs->data != NULL)
				free(songs->data);				
		}
		g_list_free(songs);
		songs = NULL;
	}
}



/**
 * This function opens directory, and sorts files in alphabetical order.
 * Files must be sorted because file order from opendir() may be different in each device.
 *
 * parameters: 
 * -path is full path of directory to be opened
 * -suffix defines which type of files will be sorted, other files are ignored
 *   (if suffix is "DIR", only directories are listed)
 * -result is an array of strings that contains filenames in order when this funtion returns
 *   Caller must free it after use.
 *
 * returns number of files found
**/
int community_utilities_open_and_arrange_dir(const char* path, const char* suffix, char result[MAX_FILES_IN_DIR][MAX_FILE_NAME_LEN]){
	DIR *dir;
	struct dirent *dir_ent;
	int numberOfFiles = 0;
	dir = opendir(path);

	if(dir != NULL){  //Error checking
		for(numberOfFiles = 0; (dir_ent = readdir(dir)) != 0; numberOfFiles++){
			if(strcmp(suffix, "DIR") == 0){  //Asked subdirectories
				if(strstr(dir_ent->d_name, ".") != 0 || strstr(dir_ent->d_name, "..") != 0) {
					numberOfFiles--;
					continue;
				}
			}else{   //Asked files with suffix
				if(strstr(dir_ent->d_name, suffix) == 0) {
					numberOfFiles--;
					continue;
				}
			}
			//take this
			strcpy(result[numberOfFiles], dir_ent->d_name);
		}

		qsort(result, numberOfFiles, MAX_FILE_NAME_LEN, (void*)strcoll);
		closedir(dir);
	}

	return numberOfFiles;
}

/**
 * Funtion frees GList that contains pointers to dynamically allocated profile_view_params structures
**/
void community_utilities_clear_paramslist(GList* paramsList) {
	paramsList = g_list_first(paramsList);
	
	if(paramsList != NULL){
	
		if(paramsList->data != 0)
		   free(paramsList->data);
		   
		while(paramsList->next != 0){
			paramsList = paramsList->next;
			
			if(paramsList->data != 0)
				free(paramsList->data);
		
		}   
	}
	
	g_list_free(paramsList);

}

/**
 * Funtion frees GList that contains pointers to dynamically allocated show_label_params structures
**/
void community_utilities_clear_labelparamslist(GList* paramslist){

	paramslist = g_list_first(paramslist);
	
	if(paramslist != NULL){
	
		g_free(((show_label_params*)paramslist->data)->path);
		g_free(paramslist->data);   
		   
		while(paramslist->next != 0){
			paramslist = paramslist->next;
			g_free(((show_label_params*)paramslist->data)->path);
			g_free(paramslist->data);
		}   
	}
	g_list_free(paramslist);
}

/**
 * Scrolls the list to correct position automatically
**/
void community_utilities_autoscroll_list(list_type* list) {
	gint slot;
	gfloat not_used, offset;

	ClutterAction *action = tangle_actor_get_action_by_type(CLUTTER_ACTOR(list->list), TANGLE_TYPE_SCROLL_ACTION);
	
	tangle_scroll_action_get_offsets(TANGLE_SCROLL_ACTION(action), &not_used, &offset);

	slot = (gint)offset % list->listItemHeight < list->listItemHeight / 2 ? (gint)(offset) / list->listItemHeight  
							  		: (gint)(offset) / list->listItemHeight + 1;

	tangle_object_animate(G_OBJECT(action), CLUTTER_EASE_IN_OUT_QUAD, 300, "offset-y", (gfloat)(slot*list->listItemHeight), NULL);
}


/**
 * Downloads a specific file from server
**/
gboolean community_utilities_load_file_from_server(const gchar* filename) {
	gboolean returnvalue = TRUE;
	
	//TODO: download json from server and check that the file was received
		
	return returnvalue;
}

/**
 * Loads songs/loops from json file. Note that this function allocates memory which should be freed later. 
 *
 * parameters:
 * -filename is name of the json file
 * -list is a pointer pointing to GList where songs/loops will be saved to
 * -type: 0=loop, 1=song
**/
gboolean community_utilities_parse_songs_from_file(const gchar* filename, GList** list, int type) {
	int j;
	JsonParser *parser;
	JsonNode *root;
	JsonObject *object;
	JsonNode *node;

	song_type *song = 0;
	loop_type *loop = 0;
	
	//Init JSON Parser
	parser = json_parser_new();
	g_assert (JSON_IS_PARSER(parser));
	GError *error = NULL;
	
	//Load file
	if (!json_parser_load_from_file (parser, filename, &error)){ 
		g_print ("Error: %s\n", error->message);
		g_error_free (error);
		g_object_unref (parser);
		return FALSE;
	}

	if(json_parser_get_root(parser) != NULL){
		root = json_parser_get_root (parser);
	}else{
		return FALSE;
	}
	if(JSON_NODE_TYPE(root) != JSON_NODE_OBJECT)
		return FALSE;

	object = json_node_get_object (root);
	if(object == NULL)
		return FALSE;
	
	//Fill array with tracks
	JsonArray* track_array;
	guint length_tracks = 0;
	node = json_object_get_member (object, "tracks");
	
	if (node != NULL && JSON_NODE_TYPE (node) == JSON_NODE_ARRAY){
		track_array =  json_node_get_array (node);
		length_tracks = json_array_get_length(track_array);
		printf("Found %d tracks\n", length_tracks);
	}else {
		printf("No tracks found\n"); 
		return TRUE;
	}
	
	//Check track properties
	for (j = 0; j < length_tracks; j++) {

		JsonNode* track_node;
		track_node = json_array_get_element(track_array, j);

		if (track_node != NULL && (JSON_NODE_TYPE(track_node) == JSON_NODE_OBJECT)){
			
			JsonObject* sub_object = json_node_get_object(track_node);
			JsonNode *sub_node;
			
			const gchar* id = "";
			const gchar* name = "";
			guint32 userid = 0; 
			const gchar* track_filename =  "";
			//gchar* info_filename = "";
			const gchar* label =  "";
			const gchar* comments =  "";
			const gchar* date =  "";

			printf("\nFound track!\n");

			if(type == 1){ //songs
			
				sub_node = json_object_get_member(sub_object, "songid"); 
				if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
					id = json_node_get_string(sub_node);
					printf("songid: '%s'\n", id);
				}		
				sub_node = json_object_get_member(sub_object, "songname");
			
				if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
					name = json_node_get_string(sub_node);
					printf("songname: '%s'\n", name);
				}

			}else if (type == 0){ //loops

				sub_node = json_object_get_member(sub_object, "loopid"); 
				if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
					id = json_node_get_string(sub_node);
					printf("loopid: '%s'\n", id);
				}		
				sub_node = json_object_get_member(sub_object, "loopname");
			
				if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
					name = json_node_get_string(sub_node);
					printf("loopname: '%s'\n", name);
				}

			}
			sub_node = json_object_get_member(sub_object, "userid"); 
			
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				userid = (guint32)atoi(json_node_get_string(sub_node));			
				printf("userid: '%d'\n", userid);
			}		
			sub_node = json_object_get_member(sub_object, "filename");
			
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				track_filename = json_node_get_string(sub_node);
				printf("filename: '%s'\n", track_filename);
			}		
			/*sub_node = json_object_get_member(sub_object, "info"); 
			
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				info_filename = (gchar*)json_node_get_string(sub_node);
				printf("info-file: '%s'\n", info_filename);
			}*/

			if(type == 1){ //songs	
	
				sub_node = json_object_get_member(sub_object, "cover");
			
				if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
					label = json_node_get_string(sub_node);
					printf("cover path: '%s'\n", label);
				}
				sub_node = json_object_get_member(sub_object, "comments"); 
			
				if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
					comments = json_node_get_string(sub_node);
					printf("comments path: '%s'\n", comments);
				}
			}
			
			sub_node = json_object_get_member(sub_object, "date");
			
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				date = json_node_get_string(sub_node);
				printf("date: '%s'\n", date);
			}

			//Save information into struct

			if(type == 1){ //songs

				song = malloc(sizeof (song_type));

				strcpy(song->songname, name);
				song->user_id = userid;	
				strcpy(song->date, date);
			
				song->songid = malloc((strlen(id)+1)*sizeof(gchar));
				strcpy(song->songid, id);	
				song->filename = malloc((strlen(track_filename)+1)*sizeof(gchar));
				strcpy(song->filename, track_filename);
				//song->json = malloc(strlen(info_filename)+sizeof(gchar));
				//strcpy(song->json, info_filename);
				song->cover = malloc((strlen(label)+1)*sizeof(gchar));
				strcpy(song->cover, label);
				song->comments_path = malloc((strlen(comments)+1)*sizeof(gchar));
				strcpy(song->comments_path, comments);	

			}else if (type == 0){ //loops

				loop = malloc(sizeof(loop_type));

				strcpy(loop->loopname, name);
				loop->user_id = userid;	
				strcpy(loop->date, date);
			
				loop->loopid = malloc((strlen(id)+1)*sizeof(gchar));
				strcpy(loop->loopid, id);	
				loop->filename = malloc((strlen(track_filename)+1)+sizeof(gchar));
				strcpy(loop->filename, track_filename);

			}
			
		} //Track is ready
		
		//Save struct into global array
		
		if(type == 1){
		 	*list = g_list_append((GList*)*list, (gpointer)song);
		}else if(type == 0){
			*list = g_list_append((GList*)*list, (gpointer)loop);
		}

	} //Next track

	printf("\n");
	g_object_unref(parser);
	
	return TRUE;
}
