/*
License: GPLv2, read more from COPYING

This file contains the functionality of JamMo Sounds menu.
 */
#ifndef JAMMOSOUNDS_H_
#define JAMMOSOUNDS_H_

void start_jammosounds();
gboolean end_jammosounds(TangleButton *tanglebutton, gpointer none);
gboolean add_loop_to_list(gpointer data,  int number);
void import_selected_loop(ClutterActor*);
gboolean load_loops_from_server();
void clear_loops(GList* loops_);

#endif 
