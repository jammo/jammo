/*
 * jammo-collaboration-group-composition.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther <mikko.gynther@lut.fi>
 *	    Tommi Kallonen <tommi.kallonen@lut.fi>
 */
 
#ifndef __JAMMO_COLLABORATION_GROUP_COMPOSITION_H__
#define __JAMMO_COLLABORATION_GROUP_COMPOSITION_H__

#include <glib.h>
#include <glib-object.h>
#include "../jammo-collaboration-game.h"
#include "../jammo-editing-track-view.h"
#include "../jammo-miditrack-view.h"

#define JAMMO_TYPE_COLLABORATION_GROUP_COMPOSITION (jammo_collaboration_group_composition_get_type ())
#define JAMMO_COLLABORATION_GROUP_COMPOSITION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_COLLABORATION_GROUP_COMPOSITION, JammoCollaborationGroupComposition))
#define JAMMO_IS_COLLABORATION_GROUP_COMPOSITION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_COLLABORATION_GROUP_COMPOSITION))
#define JAMMO_COLLABORATION_GROUP_COMPOSITION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_COLLABORATION_GROUP_COMPOSITION, JammoCollaborationGroupCompositionClass))
#define JAMMO_IS_COLLABORATION_GROUP_COMPOSITION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_COLLABORATION_GROUP_COMPOSITION))
#define JAMMO_COLLABORATION_GROUP_COMPOSITION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_COLLABORATION_GROUP_COMPOSITION, JammoCollaborationGroupCompositionClass))

typedef struct _JammoCollaborationGroupCompositionPrivate JammoCollaborationGroupCompositionPrivate;

typedef struct _JammoCollaborationGroupComposition {
	JammoCollaborationGame parent_instance;
	JammoCollaborationGroupCompositionPrivate* priv;
} JammoCollaborationGroupComposition;

typedef struct _JammoCollaborationGroupCompositionClass {
	JammoCollaborationGameClass parent_class;
} JammoCollaborationGroupCompositionClass;

GType jammo_collaboration_group_composition_get_type(void);

JammoCollaborationGroupComposition* jammo_collaboration_group_composition_new();

int jammo_collaboration_group_composition_get_number_of_players(JammoCollaborationGroupComposition * collaboration_group_composition);

void jammo_collaboration_group_composition_set_number_of_players(JammoCollaborationGroupComposition * collaboration_group_composition, int number_of_players);

int jammo_collaboration_group_composition_start(JammoCollaborationGroupComposition * collaboration_group_composition);

void jammo_collaboration_group_composition_send_midi_to_group (JammoCollaborationGroupComposition * group_composition);
void jammo_collaboration_group_composition_send_slider_to_group (JammoCollaborationGroupComposition * group_composition);
void jammo_collaboration_group_composition_loop_sync (JammoCollaborationGroupComposition * group_composition);

void jammo_collaboration_group_composition_new_view(GObject * game, ClutterActor * view);

void group_composition_remote_game_starter(gpointer data, gint16 track_id);
#endif
