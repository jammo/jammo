/**sequencer is part of JamMo.
License: GPLv2, read more from COPYING

This file is for clutter based gui.
*/

#ifndef GAMESMENU_H_
#define GAMESMENU_H_

void gamesmenu_show();
void gamesmenu_start_first_task_on_this_level(const char* levelname);
void gamesmenu_start_instructions_for_level1();
#endif /* GAMESMENU_H_ */
