/**instrumentmenu.c is part of JamMo.
License: GPLv2, read more from COPYING

instrumentmenu_view.json is calling this file
*/
#include <stdlib.h>
#include <tangle.h>

#include "sequencer.h"
#include "../jammo.h"

#include "../../meam/jammo-instrument-track.h"
#include "../../meam/jammo-slider-track.h"
#include "../jammo-track-view.h"
#include "../jammo-miditrack-view.h"
#include "instrument_gui.h"
#include "jammer_view.h"

#include "../../cem/cem.h"
#include "../jammo-mentor.h"

static void start_selected_instrument(gboolean slider, gint instrument_type){
	if (slider){
		JammoTrack* track = JAMMO_TRACK(jammo_slider_track_new(instrument_type));
		ClutterActor* track_view_actor = jammo_track_view_new(JAMMO_TRACK(track));
		instrument_gui_start(NULL, TANGLE_WIDGET(track_view_actor)); //NULL means no keyboard, but slider
	}
	else {
	JammoTrack* track = JAMMO_TRACK(jammo_instrument_track_new(instrument_type));
	//These values are almost useless:
	ClutterActor* track_view_actor = jammo_miditrack_view_new(JAMMO_INSTRUMENT_TRACK(track),
				80*4,       //Miditrack-view uses more narrow slots
				80/4,
				80, 80, //height can be anything, we zoom it
				59,36);  //Highest-note and lowest-note. TODO: should be asked from instrumet
	instrument_gui_start(JAMMO_MIDITRACK_VIEW(track_view_actor), NULL); //NULL means no slider
	}
}

gboolean instrumentmenu_slider_clicked(ClutterActor* button, gpointer data) {
	cem_add_to_log("Slider selected", J_LOG_USER_ACTION);
	tangle_actor_hide_animated(TANGLE_ACTOR(jammo_mentor_get_default()));
	const gchar* name = clutter_actor_get_name(button);
	int instrument_type= atoi(name);
	start_selected_instrument(TRUE,instrument_type);

	return TRUE;
}


gboolean instrumentmenu_instrument_clicked(ClutterActor* button, gpointer data) {
	cem_add_to_log("Instrument selected", J_LOG_USER_ACTION);
	tangle_actor_hide_animated(TANGLE_ACTOR(jammo_mentor_get_default()));
	const gchar* name = clutter_actor_get_name(button);
	int instrument_type= atoi(name);
	start_selected_instrument(FALSE,instrument_type);

	return TRUE;
}

gboolean instrumentmenu_jammer_clicked(ClutterActor* button, gpointer data) {
	cem_add_to_log("Jammer selected",J_LOG_USER_ACTION);
	tangle_actor_hide_animated(TANGLE_ACTOR(jammo_mentor_get_default()));
	clutter_actor_show(jammo_get_actor_by_id("jammer-view"));
	clutter_actor_hide(jammo_get_actor_by_id("instrumentmenu-view"));
	start_jammer();

	return TRUE;
}

gboolean instrumentmenu_goto_gamesmenu (TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("From  instrumentmenu to gamesmenu clicked",J_LOG_USER_ACTION);

	tangle_actor_hide_animated(TANGLE_ACTOR(jammo_get_actor_by_id("instrumentmenu-view")));
	clutter_actor_show (jammo_get_actor_by_id("gamesmenu-view"));
	return TRUE;
}
