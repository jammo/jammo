/* 
 * File:   jammer-emmafile.h
 * Author: eleni
 *
 * Created on February 21, 2010, 12:02 AM
 */
#define BUFFER_SIZE 6 

int create_emma(int motionBuffer[BUFFER_SIZE][3]);

int create_emma_int(int interpretation);
