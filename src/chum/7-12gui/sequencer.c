/**sequencer.c is part of JamMo.
License: GPLv2, read more from COPYING

This file is center of 7-12 years full sequencer view.
View contains three part:
 *wheel-game (loop-view)
 *track-view
 *general-view (settings)

*/

#include <glib-object.h>
//#include <clutter/clutter.h>
#include <math.h>
#include <string.h>

#include <tangle.h>

#include "../../meam/jammo-meam.h"
#include "../../meam/jammo-slider-event.h"
#include "../../meam/jammo-slider-track.h"
#include "../../meam/jammo-backing-track.h"
#include "../../meam/jammo-metronome-track.h"

#include "../jammo.h"
#include "../jammo-mentor.h"
#include "../jammo-editing-track-view.h"
#include "../jammo-miditrack-view.h"
#include "../../cem/cem.h"

#include "sequencer.h"
#include "sequencer_loop.h"
#include "sequencer_track_view.h"
#include "startmenu.h"
#include "gamesmenu.h"

#include "../jammo-game-task.h"
#include "../jammo-game-level.h"
#include "../jammo-game-level-view.h"

#include "../../configure.h"
#include "../file_helper.h"

#include "mysongs.h"
#include "jammo-collaboration-group-composition.h"

/*
Sequencer and sequencer-track-view-area are defined in json.

When sequencer starts: it calls load_state_from_file(load_only_backingtrack=TRUE)
 It starts loading backing-track.
 When backing_track is loaded (on_duration_notify)
  Tell sequencer_track_view_area that now duration is known (used e.g. with cursor)
  Then load_state_from_file(load_only_backingtrack=FALSE) loads another tracks.
*/

//This is The sequencer of the game. All 'play'-buttons should play this.
//All new tracks are added to this
//If metronome is enabled it is added to this
JammoSequencer* static_sequencer;
JammoMetronomeTrack* static_metronome_track;

static JammoCollaborationGroupComposition* static_group_composition = NULL;

JammoCollaborationGroupComposition* sequencer_get_group_composition() {
	return static_group_composition;
}

void sequencer_set_group_composition(JammoCollaborationGroupComposition* pc) {
	static_group_composition=pc;
}

static void load_state_from_file(const gchar* filename, gboolean load_only_backingtrack) {

JsonParser *parser;
parser = json_parser_new ();
g_assert (JSON_IS_PARSER (parser));

GError *error = NULL;
if (!json_parser_load_from_file (parser, filename, &error))
	{
		g_print ("Error: %s\n", error->message);
		g_error_free (error);
		g_object_unref (parser);
		return;
	}

JsonNode *root;
JsonObject *object;
JsonNode *node;

g_assert (NULL != json_parser_get_root (parser));

root = json_parser_get_root (parser);
g_assert_cmpint (JSON_NODE_TYPE (root), ==, JSON_NODE_OBJECT);

object = json_node_get_object (root);
g_assert (object != NULL);

	gint tempo = 110; //'normal' tempo
	const gchar* pitch;

	guint64 duration  = 0; //Duration is mandatory

	//META data: Load these only once
if (load_only_backingtrack){
	//int
	node = json_object_get_member (object, "tempo");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE){
		tempo = json_node_get_int (node);
		//printf("tempo: '%d'\n",tempo);
	}
	//string
	node = json_object_get_member (object, "pitch");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE){
		pitch = json_node_get_string (node);
	}
	pitch ="D";  //'normal' pitch
	//printf("pitch: '%s'\n",pitch);

	//guint64
	node = json_object_get_member (object, "duration");
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE){
		duration =  json_node_get_int (node);
		//printf("duration: '%" G_GUINT64_FORMAT "'\n",duration);
	}

	jammo_sequencer_set_tempo(static_sequencer,tempo);
	jammo_sequencer_set_pitch(static_sequencer,pitch);
	//These will be synced after loading

	sequencer_track_view_set_duration(duration);
}


	ClutterActor* track_view = NULL; //Will be JammoTrackView or one of it's subclass e.g. JammoEditingTrackView

	//Array
	node = json_object_get_member (object, "tracks");
	JsonArray* track_array;
	guint length_tracks = 0;
	if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_ARRAY){
		track_array =  json_node_get_array (node);
		length_tracks = json_array_get_length(track_array);
	}
	else {printf("Not any tracks\n"); }

	//printf("length %d\n",length_tracks);
	int j;
	for (j=0;j<length_tracks;j++) {
		track_view = NULL;

		//Object
		JsonNode* track_node;
		track_node = json_array_get_element(track_array,j);

		if (track_node!=NULL && JSON_NODE_TYPE(track_node) == JSON_NODE_OBJECT){
			//printf("Found track!\n");
			const gchar* track_type;
			const gchar* name_of_track;

			JsonObject* sub_object = json_node_get_object(track_node);
			JsonNode *sub_node;
			sub_node = json_object_get_member (sub_object, "track-type");
			if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
				track_type = json_node_get_string (sub_node);
				}
			else track_type="";
			//printf(" track-type: '%s'\n",track_type);

			//boolean. We need this to construct track_view
			gboolean muted=FALSE; //default
			sub_node = json_object_get_member (sub_object, "muted");
			if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
				muted = json_node_get_boolean (sub_node);
				//printf(" muted: '%d'\n",muted);
			}

			// double/float
			gfloat volume=1.0; //default volume is 100%
			sub_node = json_object_get_member (sub_object, "volume");
			if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
				volume = (gfloat) json_node_get_double (sub_node);
				//printf(" volume: '%f'\n",volume);
			}
			//string
			sub_node = json_object_get_member (sub_object, "name");
			if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
				name_of_track = json_node_get_string (sub_node);
			}
			else name_of_track="";
			//printf(" name of track: '%s'\n",name_of_track);


			//BACKING-TRACK
			if (strncmp(track_type, "BackingTrack", 12) == 0) {
				//printf("Found BackingTrack\n");
				const gchar* backing_track_filename;
				//string
				sub_node = json_object_get_member (sub_object, "audio-file");
				if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
					backing_track_filename = json_node_get_string (sub_node);
					//printf("backing_track_filename: '%s'\n",backing_track_filename);

				const gchar* image_filename;
				//string
				sub_node = json_object_get_member (sub_object, "image-file");
				if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
					image_filename = json_node_get_string (sub_node);
				}
				else image_filename="";
				//printf("image_filename: '%s'\n",image_filename);

				track_view=sequencer_track_view_create_backing_track(muted,volume,backing_track_filename,image_filename,name_of_track);
				}
				else backing_track_filename="";
			} //Backing-track over


			//EDITING-TRACK
			if(strncmp(track_type, "EditingTrack", 12) == 0) {
				gint e_track_type=JAMMO_SAMPLE_UNTYPED;
				const gchar* editing_track_type;
				gint slot_width=-1; //-1 means use default value.

				//string
				sub_node = json_object_get_member (sub_object, "editing-track-type");
				if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
					editing_track_type = json_node_get_string (sub_node);
				}
				else editing_track_type="";
				//printf("editing-track-type: '%s'\n",editing_track_type);

				GEnumClass* enum_class = G_ENUM_CLASS(g_type_class_ref(JAMMO_TYPE_SAMPLE_TYPE));
				GEnumValue* enum_value = g_enum_get_value_by_nick(enum_class, editing_track_type);
				g_type_class_unref(enum_class);
				if (enum_value)
					e_track_type = enum_value->value;

				//int
				sub_node = json_object_get_member (sub_object, "slot-width");
				if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
					slot_width = json_node_get_int (sub_node);
				}

				int format_version=2; //Default.
				//int
				node = json_object_get_member (object, "format-version");
				if (node!=NULL && JSON_NODE_TYPE (node) == JSON_NODE_VALUE){
					format_version = json_node_get_int (node);
				}

				//slot_width differs from format-version==2
				if (format_version==2) { //Legacy2
					if (slot_width==40) slot_width=10; //sandbox-level
					if (slot_width==80) slot_width=22; //orientation-level
				}

				track_view = sequencer_track_view_create_editing_track(muted,volume,e_track_type,slot_width,name_of_track);


				//Array
				sub_node = json_object_get_member (sub_object, "samples");
				if (format_version==2) //Legacy2
					load_this_sample_array_to_this_track_view_legacy2(sub_node,JAMMO_EDITING_TRACK_VIEW(track_view),TRUE); //TRUE=reactive
				else
					load_this_sample_array_to_this_track_view(sub_node,JAMMO_EDITING_TRACK_VIEW(track_view),TRUE); //TRUE=reactive
				#ifdef NETWORKING_ENABLED
				//Group game
				if (sequencer_get_group_composition()) {
					jammo_collaboration_group_composition_new_view(G_OBJECT(sequencer_get_group_composition()), CLUTTER_ACTOR(track_view));
				}
				#endif /* NETWORKING_ENABLED */
			} //Editing-track over

			//VirtualInstrumentTrack
			else if(strncmp(track_type, "VirtualInstrumentTrack", 22) == 0) {
				//int
				gint instrument=0; //default instrument
				sub_node = json_object_get_member (sub_object, "instrument");
					if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
						instrument = json_node_get_int (sub_node);
						//printf(" instrument: '%d'\n",instrument);
					}

				track_view=sequencer_track_view_create_instrument_track(muted,volume,instrument,name_of_track);

				//string
				sub_node = json_object_get_member (sub_object, "note-file");
				if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
					const gchar* note_file = json_node_get_string (sub_node);
					//printf("loading notes from file '%s'\n",note_file);
					GList* events = jammomidi_file_to_glist(note_file);
					jammo_miditrack_view_add_event_list(JAMMO_MIDITRACK_VIEW(track_view),events);
				}
			} //VirtualInstrumentTrack over

#ifdef not_implemented_on_sequencer_track_view
			//SliderTrack
			else if(strncmp(track_type, "SliderTrack", 11) == 0) {
				//int
				gint instrument=0; //default instrument
				sub_node = json_object_get_member (sub_object, "instrument");
					if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
						instrument = json_node_get_int (sub_node);
						printf("instrument: '%d'\n",instrument);
					}

				//track_view=sequencer_track_view_create_instrument_track(muted,volume,instrument,name_of_track);  //TODO
				//In slider-case: track_view is TangleWidget.
				track = g_object_get_data(G_OBJECT(track_view), "track");

				//string
				sub_node = json_object_get_member (sub_object, "note-file");
				if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_VALUE){
					const gchar* note_file =  json_node_get_string (sub_node);
					printf("loading notes from file '%s'\n",note_file);
					GList* events = jammo_slider_event_file_to_glist(note_file);
					jammo_slider_track_set_event_list(JAMMO_SLIDER_TRACK(track), events); //We need access to track here!
				}
			} //SliderTrack over
#endif

		} //Track is ready

	} //Next track

g_object_unref(parser);

}




gboolean sequencer_change_to_loop_view(TangleActor *actor, gpointer data) {
	cem_add_to_log("changing view to loop",J_LOG_INFO);
	ClutterActor* scrolling_view = jammo_get_actor_by_id("fullsequencer-view-container");
	if (scrolling_view==NULL){
		cem_add_to_log("no actor 'fullsequencer-view-container'",J_LOG_ERROR);
		return TRUE;
	}
	ClutterAction* action = tangle_actor_get_action_by_type(scrolling_view,TANGLE_TYPE_SCROLL_ACTION);
	tangle_object_animate(G_OBJECT(action), CLUTTER_EASE_IN_CIRC, 400, "offset-y", 0.0, NULL);
	return TRUE;
}

gboolean sequencer_change_to_sequencer_view(TangleActor *actor, gpointer data) {
	cem_add_to_log("changing view to sequencer",J_LOG_INFO);
	ClutterActor* scrolling_view = jammo_get_actor_by_id("fullsequencer-view-container");
	if (scrolling_view==NULL){
		cem_add_to_log("no actor 'fullsequencer-view-container'",J_LOG_ERROR);
		return TRUE;
	}
	ClutterAction* action = tangle_actor_get_action_by_type(scrolling_view,TANGLE_TYPE_SCROLL_ACTION);
	tangle_object_animate(G_OBJECT(action), CLUTTER_EASE_IN_CIRC, 400, "offset-y", 480.0, NULL);
	return TRUE;
}

gboolean sequencer_change_to_bottom_view (TangleActor *actor, gpointer data) {
	cem_add_to_log("changing view to general menu",J_LOG_INFO);
	ClutterActor* scrolling_view = jammo_get_actor_by_id("fullsequencer-view-container");
	if (scrolling_view==NULL){
		cem_add_to_log("no actor 'fullsequencer-view-container'",J_LOG_ERROR);
		return TRUE;
	}
	ClutterAction* action = tangle_actor_get_action_by_type(scrolling_view,TANGLE_TYPE_SCROLL_ACTION);
	tangle_object_animate(G_OBJECT(action), CLUTTER_EASE_IN_CIRC, 400, "offset-y", 960.0, NULL);
	return TRUE;
}

void secuencer_sync_buttons_to_pitch_and_tempo() {
	int starting_tempo = jammo_sequencer_get_tempo(static_sequencer);
	//printf("starting_tempo %d\n",starting_tempo);
	ClutterActor* button;
	const gchar* name_button;
	if(starting_tempo== 130)       name_button="tempo-button-fast";
	else if(starting_tempo== 110)  name_button="tempo-button-normal";
	else if(starting_tempo==90)    name_button="tempo-button-slow";
	else name_button="";

	button =jammo_get_actor_by_id(name_button);
	if (button)
		tangle_button_set_selected(TANGLE_BUTTON(button),TRUE);

	const gchar* starting_pitch = jammo_sequencer_get_pitch(static_sequencer);
	if(strcmp(starting_pitch,"G")==0)       name_button="pitch-button-high";
	else if(strcmp(starting_pitch,"D")==0)  name_button="pitch-button-normal";
	else if(strcmp(starting_pitch,"A")==0)  name_button="pitch-button-low";

	button =jammo_get_actor_by_id(name_button);
	if (button)
		tangle_button_set_selected(TANGLE_BUTTON(button),TRUE);
}

//show-completed callback (=animation is ready)
static void levelview_loaded(ClutterActor *levelview, gpointer none) {
	g_signal_handlers_disconnect_by_func(levelview, levelview_loaded, NULL);
	const gchar* levelname = clutter_actor_get_name(levelview);

	//Volumebar must be showed, unless first time using it, it shows volume=0.
	//Now we can hide it.
	ClutterActor* volume_container = jammo_get_actor_by_id("fullsequencer-volumebar-container");
	clutter_actor_hide(volume_container);

	//Make sample-buttons on wheel-game
	//This can take time (thus we have hourglass on screen)
	if (strcmp(levelname,"level1")==0)  //optimization: Level1 doesn't need them all.
		sequencer_loop_tune_wheels(1);    //load only sample-buttons for level1
	else
		sequencer_loop_tune_wheels(0);    //load all sample-buttons


	//Hide hourglass
	ClutterActor* wait = jammo_get_actor_by_id("wait");
	if (wait)
		clutter_actor_hide(wait);

	//Start task1 on this level
	JammoGameLevel* game_level = JAMMO_GAME_LEVEL(jammo_get_object_by_id("game-level"));
	if (game_level) {
		gchar* task_name = g_strdup_printf("%s-task1.json",levelname);
		jammo_game_level_start_task(game_level,task_name);
		g_free(task_name);

		secuencer_sync_buttons_to_pitch_and_tempo();
	}

}

void sequencer_start_with_file(const gchar* filename) {
	gamesmenu_show(); //When level starts, it suppose that gamesmenu is also visible
	gamesmenu_start_first_task_on_this_level("empty-level");

	gchar *message = g_strdup_printf("Starting sequencer from file '%s'",filename);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);

	load_state_from_file(filename, true);
	secuencer_sync_buttons_to_pitch_and_tempo();
}


/*
Use when samplebutton is dropped to track or removed from track.
*/
static void log_track_activity (const gchar* event, int slot, ClutterActor* track, ClutterActor* actor) {
	gchar* name;
	g_object_get(actor,"sample-filename", &name,NULL); //It is sample_button

	gchar* name_of_track;
	g_object_get(track,"name", &name_of_track,NULL);

	gchar *message = g_strdup_printf("Sample '%s' %s track:%s at slot = %d",name, event, name_of_track, slot);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);
}


static void on_actor_added(ClutterActor* track, gint slot, ClutterActor* actor, gpointer none) {
	log_track_activity("added to", slot, track, actor);
}


static void on_actor_removed(ClutterActor* track, gint slot, ClutterActor* actor, gpointer none) {
	log_track_activity("removed from", slot, track, actor);
}
/*
When editing track is registered (=loaded) add signals to it
*/
void fullsequencer_trackview_loaded (TangleActor* trackview, gpointer none){
	g_signal_handlers_disconnect_by_func(trackview, fullsequencer_trackview_loaded, NULL);

	g_signal_connect(trackview, "sample-added", G_CALLBACK(on_actor_added), NULL);
	g_signal_connect(trackview, "sample-removed", G_CALLBACK(on_actor_removed), NULL);
}


static ClutterActor* current_levelview =NULL;
void start_fullsequencer_gui(ClutterActor* levelview)
{
	tangle_actor_hide_animated(TANGLE_ACTOR(jammo_mentor_get_default()));

	current_levelview=levelview;
	cem_add_to_log("Sequencer GUI starts",J_LOG_INFO);

	g_signal_connect(current_levelview, "show-completed", G_CALLBACK(levelview_loaded), NULL); //Continue after animated show
	clutter_actor_show(current_levelview);

	//Volumebar must be showed, unless first time using it, it shows volume=0.
	ClutterActor* volume_container = jammo_get_actor_by_id("fullsequencer-volumebar-container");
	clutter_actor_show(volume_container);

	ClutterAction* action = tangle_actor_get_action_by_type(jammo_get_actor_by_id("fullsequencer-view-container"), TANGLE_TYPE_SCROLL_ACTION);
	g_signal_connect_swapped(action, "clamp-offset-y", G_CALLBACK(tangle_widget_clamp_child_boundaries), jammo_get_actor_by_id("fullsequencer-view-container"));

	static_sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));
	static_metronome_track = NULL;

	JammoSequencer* sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));
	guint64 duration = jammo_sequencer_get_duration(sequencer);
	sequencer_track_view_set_duration(duration);

	sequencer_change_to_sequencer_view(NULL,NULL); //Start to middle view. //TODO:do inside JSON

	ClutterActor* wait = jammo_get_actor_by_id("wait");
	if (wait)
		clutter_actor_show(wait);

}

void return_to_fullsequencer_gui(){
	//At least backing-track-selection could change pitch/tempo
	secuencer_sync_buttons_to_pitch_and_tempo();

	clutter_actor_show(CLUTTER_ACTOR(jammo_mentor_get_default()));
	clutter_actor_show(current_levelview);
}

/*
 * CallBack for JSON
 */
static void leave_fullsequencer() {
	JammoSequencer* sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));

	jammo_sequencer_stop(sequencer);
	//Autosaving?
	//Confirmation?
	if (sequencer_get_group_composition()) {
		g_object_unref(sequencer_get_group_composition());
		sequencer_set_group_composition(NULL);
	}

	tangle_view_unload(TANGLE_VIEW(current_levelview));
	current_levelview=NULL;
}

gboolean fullsequencer_home_clicked (TangleButton* tanglebutton, gpointer none){
	jammo_mentor_set_idle_speech(jammo_mentor_get_default(), "");
	jammo_mentor_shut_up(jammo_mentor_get_default());
	leave_fullsequencer();
	startmenu_goto_startmenu(NULL,NULL);
	return TRUE;
}

gboolean fullsequencer_general_tempo_clicked(TangleButton *tanglebutton, gpointer user_data) {
	const gchar* name=clutter_actor_get_name(CLUTTER_ACTOR(tanglebutton));
	if (name==NULL)
		return TRUE;

	int new_tempo = 110;
	if (strncmp(name,"fast",4)==0)
			new_tempo=130;
	else if (strncmp(name,"normal",6)==0)
			new_tempo=110;
	else if (strncmp(name,"slow",4)==0)
			new_tempo=90;

	gchar* message = g_strdup_printf("tempo '%s'='%d' clicked",name,new_tempo);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);

	jammo_sequencer_set_tempo(static_sequencer, new_tempo);
	return TRUE;
}


gboolean fullsequencer_general_pitch_clicked(TangleButton *tanglebutton, gpointer user_data) {
	const gchar* name=clutter_actor_get_name(CLUTTER_ACTOR(tanglebutton));
	if (name==NULL)
		return TRUE;

	const gchar* new_pitch;
	if (strncmp(name,"high",4)==0)
			new_pitch = "G";
	else if (strncmp(name,"normal",6)==0)
			new_pitch = "D";
	else if (strncmp(name,"low",3)==0)
			new_pitch = "A";
	else new_pitch = "G";

	gchar* message = g_strdup_printf("pitch '%s'='%s' clicked",name,new_pitch);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);

	jammo_sequencer_set_pitch(static_sequencer, new_pitch);
	return TRUE;
}


static void on_rendering_stopped(JammoSequencer* sequencer, gpointer none) {
	cem_add_to_log("Rendering ready",J_LOG_INFO);
	ClutterActor* wait = jammo_get_actor_by_id("wait");
	if (wait)
		clutter_actor_hide(wait);

	g_signal_handlers_disconnect_by_func(sequencer, on_rendering_stopped, NULL);
	if (!(jammo_sequencer_set_output_filename(sequencer, NULL))) {
		cem_add_to_log("Problem going back to playback-mode after render",J_LOG_ERROR);
	}

}

gboolean sequencer_render_button_pressed (TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("Render-button pressed",J_LOG_USER_ACTION);

	//If sequencer is playing, use render button as stop-button.
	gboolean play;
	g_object_get(static_sequencer,"play",&play,NULL);

	if (play) {
		jammo_sequencer_stop(static_sequencer);
		cem_add_to_log(" Sequencer was playing/rendering -> stop",J_LOG_DEBUG);
		return TRUE;
	}

	char timestamp [80];
	cem_get_time(timestamp);

	//gchar* outputFilename = g_strdup_printf("%s/%s.ogg",configure_get_finalized_directory(),timestamp);
	gchar* outputFilename = g_strdup_printf("%s/%s_%d.ogg",configure_get_finalized_directory(), timestamp, 
												/*gems_profile_manager_get_userid(NULL)*/ 202179860);

	gchar* message = g_strdup_printf("Rendering file named '%s'",outputFilename);
	cem_add_to_log(message,J_LOG_INFO);
	g_free(message);

	if (jammo_sequencer_set_output_filename(static_sequencer, outputFilename)) {
		ClutterActor* wait = jammo_get_actor_by_id("wait");
		if (wait)
			clutter_actor_show(wait);

		g_signal_connect(static_sequencer, "stopped", G_CALLBACK(on_rendering_stopped), NULL);
		jammo_sequencer_play(static_sequencer);
	}
	else
		cem_add_to_log("Can't render audiofile",J_LOG_ERROR);

	g_free(outputFilename);
	return TRUE;
}

void sequencer_save_project(){
	gchar* meta_data = g_strdup_printf
      ("\"pitch\" : \"%s\",\n\"tempo\" : %d,\n\"duration\" : %" G_GUINT64_FORMAT ",\n",
      jammo_sequencer_get_pitch(static_sequencer),
      jammo_sequencer_get_tempo(static_sequencer),
      jammo_sequencer_get_duration(static_sequencer));

	save_composition(configure_get_projects_directory(), "", jammo_get_actor_by_id("fullsequencer-container-for-tracks"),meta_data);
	g_free(meta_data);
	cem_add_to_log("  Saving ready",J_LOG_INFO);
}

gboolean sequencer_save_project_button_pressed (TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("Saving project file",J_LOG_INFO);
	sequencer_save_project();
	return TRUE;
}


gboolean sequencer_mysongs_button_pressed (TangleButton* tanglebutton, gpointer none){
	leave_fullsequencer();
	start_mysongs();
	return TRUE;
}


gboolean fullsequencer_metronome_clicked (TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("Metronome pressed",J_LOG_USER_ACTION);

	if (static_metronome_track==NULL) {
		static_metronome_track = jammo_metronome_track_new();
		jammo_sequencer_add_track(static_sequencer, JAMMO_TRACK(static_metronome_track));
		jammo_metronome_track_set_time_signature_beats(static_metronome_track,4);
		jammo_metronome_track_set_time_signature_note_value(static_metronome_track,4);
		jammo_metronome_track_set_accent(static_metronome_track,FALSE);
		jammo_playing_track_set_muted(JAMMO_PLAYING_TRACK(static_metronome_track), FALSE);
	}
	else
		jammo_playing_track_set_muted(JAMMO_PLAYING_TRACK(static_metronome_track), !tangle_button_get_selected(tanglebutton));

	return TRUE;
}
