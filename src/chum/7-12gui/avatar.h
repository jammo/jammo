#ifndef AVATAR_H
#define AVATAR_H

#define MAX_FILES_IN_DIR 50
#define MAX_FILE_NAME_LEN 60

/*
* Avatar structure contains parts needed to build avatar.
*/
typedef struct{
	ClutterActor* body;
	ClutterActor* hair;
	ClutterActor* head;
	ClutterActor* eyes;
	ClutterActor* instrument;
}Avatar;


void avatar_parse_json(gchar* filename,gchar** name, gchar** hobbies, gchar** gender, guint32* serialized_image, guint32* age);
guint32 avatar_give_serialized_image_for_this_id(guint32 id);
gchar* avatar_give_username_for_this_id(guint32 avatar_id);


gboolean avatar_unserialize_image(Avatar* avatar, guint32 serialized_image);
guint32 avatar_serialize_image(Avatar* avatar);
void avatar_shrink(Avatar* avatar);
#endif /* AVATAR_H */
