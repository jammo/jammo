/**sequencer_loop.c is part of JamMo.
 License: GPLv2, read more from COPYING

 This file is for clutter based gui.
 This is part of the sequencer.
 */
#include <glib-object.h>
#include <clutter/clutter.h>
#include <string.h>
#include <stdlib.h>
#include <tangle.h>

#include "sequencer_loop.h"
#include "sequencer.h"
#include "../jammo-sample-button.h"
#include "../jammo.h" //get_id

#include "../../cem/cem.h"

#include "../../configure.h"
//Static pointers for loop rolls
static ClutterActor* rhytmical_sample_looper;
static ClutterActor* melodical_sample_looper;
static ClutterActor* harmonical_sample_looper;
static ClutterActor* effect_sample_looper;

static GList* rhythms = NULL;
static GList* melodics = NULL;
static GList* harmonics = NULL;
static GList* effects = NULL;

typedef struct {
	uint r;
	uint m;
	uint h;
	uint e;
} quad;

static int number_of_cannot_fail_combos = 11; //TODO: calculate dynamically
static quad cannot_fail_lists[] = {
	{ 1,0,0,-1    },
	{ 0,1,1,0     },
	{ 2,2,2,-1    },
	{ 3,3,3,-1    },
	{ 6,8,-1,1    },
	{ 7,4,4,-1    },
	{ 3,5,5,2     },
	{ 5,7,6,-1    },
	{ 8,8,7,-1    },
	{ 4,6,9,4     },
	{ 9,9,8,-1    }
};

static JammoSampleType type_by_id(int id) {
JammoSampleType type=JAMMO_SAMPLE_UNTYPED;
 if (id>=1000 && id <2000){
		type=JAMMO_SAMPLE_RHYTMICAL;
	} else if (id>=2000 && id <3000) {
		type=JAMMO_SAMPLE_MELODICAL;
	} else if (id>=3000 && id <4000) {
		type=JAMMO_SAMPLE_HARMONICAL;
	}	else if (id>=4000 && id <5000) {
		type=JAMMO_SAMPLE_EFFECT;
	}
	return type;
}

gboolean sequencer_lock_pressed (TangleButton* tanglebutton, gpointer data){

	const gchar* name = clutter_actor_get_name(CLUTTER_ACTOR(tanglebutton));
	gchar* message = g_strdup_printf("Wheel_game: Lock '%s' pressed",name);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);

	ClutterActor* scroller = NULL;
	if (strncmp(name,"rhytm",5)==0)
		scroller = rhytmical_sample_looper;
	else if (strncmp(name,"melod",5)==0)
		scroller = melodical_sample_looper;
	else if (strncmp(name,"harmo",5)==0)
		scroller = harmonical_sample_looper;
	else if (strncmp(name,"effec",5)==0)
		scroller = effect_sample_looper;

	else {
		cem_add_to_log("can't detect which lock pressed",J_LOG_WARN);
		return FALSE;
	}

	ClutterAction* scroll_action = tangle_actor_get_action_by_type(scroller,TANGLE_TYPE_SCROLL_ACTION);
	gfloat threshold_x, threshold_y;

	if (scroller)  {
		tangle_scroll_action_get_thresholds(TANGLE_SCROLL_ACTION(scroll_action), &threshold_x,&threshold_y);
		if (threshold_y==10000.0)
			tangle_scroll_action_set_thresholds(TANGLE_SCROLL_ACTION(scroll_action), threshold_x,30.0);//TODO: use original default value
		else
			tangle_scroll_action_set_thresholds(TANGLE_SCROLL_ACTION(scroll_action), threshold_x,10000.0);
	}
	return TRUE;
}


/*
ScrollingActor doesn't have clampping. This will set it.
*/
static void fine_tune_wheel (ClutterActor* sample_looper) {
	if (sample_looper==NULL)
		return;

	ClutterAction* action;
	
	action = tangle_actor_get_action_by_type(sample_looper, TANGLE_TYPE_SCROLL_ACTION);
	g_signal_connect_swapped(action, "clamp-offset-y", G_CALLBACK(tangle_widget_clamp_child_boundaries), sample_looper);
}


#define ABS_F(x) ((x) < 0 ? -(x) : (x))
/*
Returns JammoSample closest to center of wheel. If wheel is rolling and two sample are equal
some of them are returned.
(This function can be used when wheel is running but there are no reason to do that).
Will return NULL in error cases.
Returns new just created sample, not actual sample from wheel.
*/
static JammoSample* get_centermost_sample_from_loop_roll(ClutterActor* wheel) {
	TangleActorIterator actor_iterator;
	ClutterActor* child;
	JammoSample* sample = NULL;
	gfloat offset;
	ClutterActorBox box;

	offset = clutter_actor_get_height(wheel) / 2.0;
	//printf("offset = %f\n",offset);

	for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(wheel), &actor_iterator); (child = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
		if (JAMMO_IS_SAMPLE_BUTTON(child)){
			clutter_actor_get_allocation_box(child, &box);
			if (box.y1 < offset && box.y2 > offset) {
				sample = jammo_sample_new_from_existing(jammo_sample_button_get_sample(JAMMO_SAMPLE_BUTTON(child)));
				break;
			}
		}
	}

	return sample;
}


/***************************
Functions for json
**************************/


//Removing ad-hoc -sequncer when stopped
static void on_sequencer_stopped(JammoSequencer* sequencer, gpointer data) {
	//printf("stopped\n");
	g_object_unref(sequencer);
}

/*
 * plays all samples from centerline of loop rolls
 */
gboolean fullsequencer_loop_play_all_clicked (TangleActor *actor, gpointer data) {
	cem_add_to_log("fullsequencer_play_all_loops-clicked",J_LOG_USER_ACTION);
	jammo_sample_stop_all();

	//get_centermost_sample_from_loop_roll can return NULL (in error cases)
	JammoSample* r = get_centermost_sample_from_loop_roll(rhytmical_sample_looper);
	JammoSample* m = get_centermost_sample_from_loop_roll(melodical_sample_looper);
	JammoSample* h = get_centermost_sample_from_loop_roll(harmonical_sample_looper);
	JammoSample* e = get_centermost_sample_from_loop_roll(effect_sample_looper);

	//Make ad-hoc-sequencer for samples
	JammoSequencer* ad_hoc_sequencer = jammo_sequencer_new();
	jammo_sequencer_set_tempo(ad_hoc_sequencer,jammo_sequencer_get_tempo(JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"))));
	jammo_sequencer_set_pitch(ad_hoc_sequencer,jammo_sequencer_get_pitch(JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"))));

	if (r!=NULL){
		JammoEditingTrack* track1 = jammo_editing_track_new();
		jammo_sequencer_add_track(ad_hoc_sequencer,JAMMO_TRACK(track1));
		jammo_editing_track_add_sample(track1,r,0);
	}
	if (m!=NULL) {
		JammoEditingTrack* track2 = jammo_editing_track_new();
		jammo_sequencer_add_track(ad_hoc_sequencer,JAMMO_TRACK(track2));
		jammo_editing_track_add_sample(track2,m,0);
	}
	if (h!=NULL) {
		JammoEditingTrack* track3 = jammo_editing_track_new();
		jammo_sequencer_add_track(ad_hoc_sequencer,JAMMO_TRACK(track3));
		jammo_editing_track_add_sample(track3,h,0);
	}
	if (e!=NULL) {
		JammoEditingTrack* track4 = jammo_editing_track_new();
		jammo_sequencer_add_track(ad_hoc_sequencer,JAMMO_TRACK(track4));
		jammo_editing_track_add_sample(track4,e,0);
	}

	g_signal_connect(ad_hoc_sequencer, "stopped", G_CALLBACK(on_sequencer_stopped), NULL);
	jammo_sequencer_play(ad_hoc_sequencer);

	return TRUE;
}


/*
amount=how many slots.
If loop is already rolling, this will abort it and will make own.
Loop will still snap to grid always.
*/
static void rotate_loop_for_amount(ClutterActor* wheel, gint amount) {
	ClutterAction* scroll_action = tangle_actor_get_action_by_type(wheel,TANGLE_TYPE_SCROLL_ACTION);

	//Check if lock is pressed
	gfloat not_used,threshold_y;
	tangle_scroll_action_get_thresholds(TANGLE_SCROLL_ACTION(scroll_action), &not_used,&threshold_y);

	//printf("threshold_y '%f'\n",threshold_y);
	if (threshold_y==10000.0) //means rolling is disabled for this wheel
		return;
	gfloat offset;
	tangle_scroll_action_get_offsets(TANGLE_SCROLL_ACTION(scroll_action), &not_used,&offset);
	//printf("offset '%f'\n",offset);

	int current_slot = (int)(offset)/92; //round to lower slot, if rolling already
	gfloat target = (current_slot+amount) * 92.0;
	//printf("current_slot = %d, target pixel='%f'\n",current_slot,target);
	tangle_object_animate(G_OBJECT(scroll_action),  CLUTTER_EASE_IN_OUT_QUAD, 550, "offset-y", target, NULL);
}


/*
Will rotate wheel so target_slot is center of wheel.
slot means index on list.
*/
static void rotate_to_place(ClutterActor* wheel, gint target_slot) {
	ClutterAction* scroll_action = tangle_actor_get_action_by_type(wheel,TANGLE_TYPE_SCROLL_ACTION);

	target_slot-=2; //unless target will be in most upper slot
	//printf("current_slot = %d, target pixel='%f'\n",current_slot,target);
	tangle_object_animate(G_OBJECT(scroll_action),  CLUTTER_EASE_IN_OUT_QUAD, 750, "offset-y", target_slot*92.0, NULL);
}


/*
cannot_fail_lists is index for corresponding lists.
*/
gboolean fullsequencer_loop_cannot_fail_clicked (TangleActor* actor, gpointer data) {
	cem_add_to_log("fullsequencer_loop_cannot_fail_clicked-clicked",J_LOG_USER_ACTION);

	GRand *random_gen = g_rand_new();

	gint index_for_combo = g_rand_int_range(random_gen, 0, number_of_cannot_fail_combos-1);
	quad combo = cannot_fail_lists[index_for_combo];
	gchar* message = g_strdup_printf("CannotFailCombo[%d]: %d,%d,%d,%d", index_for_combo,combo.r, combo.m, combo.h, combo.e);
	cem_add_to_log(message,J_LOG_DEBUG);
	g_free(message);

	//-1 means last element of corresponding list
	if (combo.r==-1) combo.r= g_list_length(rhythms)-1;
	if (combo.m==-1) combo.m= g_list_length(melodics)-1;
	if (combo.h==-1) combo.h= g_list_length(harmonics)-1;
	if (combo.e==-1) combo.e= g_list_length(effects)-1;

	rotate_to_place(rhytmical_sample_looper, combo.r);
	rotate_to_place(melodical_sample_looper, combo.m);
	rotate_to_place(harmonical_sample_looper, combo.h);
	rotate_to_place(effect_sample_looper, combo.e);

	return TRUE;
}

void sequencer_loop_randomize_wheels(){
	GRand *random_gen = g_rand_new();

	gint rot_rhyt = g_rand_int_range(random_gen, -30, 30);
	gint rot_melo = g_rand_int_range(random_gen, -30, 30);
	gint rot_harm = g_rand_int_range(random_gen, -30, 30);
	gint rot_effe = g_rand_int_range(random_gen, -30, 30);

	rotate_loop_for_amount(rhytmical_sample_looper, rot_rhyt);
	rotate_loop_for_amount(melodical_sample_looper, rot_melo);
	rotate_loop_for_amount(harmonical_sample_looper, rot_harm);
	rotate_loop_for_amount(effect_sample_looper, rot_effe);

	g_rand_free(random_gen);
}

//This is just random.
gboolean fullsequencer_loop_crazy_clicked (TangleActor *actor, gpointer data){
	cem_add_to_log("fullsequencer_loop_crazy_clicked-clicked",J_LOG_USER_ACTION);
	sequencer_loop_randomize_wheels();
	return TRUE;
}


//////////////////////////////////////


static void sample_button_drag_begin(JammoSampleButton* sample_button, gpointer data) {
	cem_add_to_log("jammo_sample_button from wheel drag-begin",J_LOG_USER_ACTION);
	sequencer_change_to_sequencer_view(NULL,NULL);
}


typedef struct {
	const gchar* image;
	const gchar* sound;
} samplebutton_tuple;




static GList* load_samples_from_json (const char* filename_p) {
	GList* result = NULL;
	JsonParser *parser;
	parser = json_parser_new ();
	g_assert (JSON_IS_PARSER (parser));

	gchar* filename = g_strdup_printf("%s/%s",DATA_DIR,filename_p);

	if (!json_parser_load_from_file (parser, filename, NULL))
		{
		printf(" Can't load settings file '%s'.\n", filename);
		g_object_unref (parser);
		}
	else{

	JsonNode *root;
	JsonObject *object;

	g_assert (NULL != json_parser_get_root (parser));

	//g_print ("checking root node is an object...\n");
	root = json_parser_get_root (parser);
	g_assert_cmpint (JSON_NODE_TYPE (root), ==, JSON_NODE_OBJECT);

	object = json_node_get_object (root);
	g_assert (object != NULL);


	//Array
	JsonNode* sub_node = json_object_get_member (object, "loop-samples");
	if (sub_node!=NULL && JSON_NODE_TYPE (sub_node) == JSON_NODE_ARRAY){
		JsonArray* sample_array =  json_node_get_array (sub_node);

		guint length = json_array_get_length(sample_array);
		//printf("length %d\n",length);
		int i;
		for (i=0;i<length;i+=2) {
			const gchar* image =  json_array_get_string_element(sample_array,i);
			const gchar* sound =  json_array_get_string_element(sample_array,i+1);

			//printf("loaded from json: image: '%s' . sound '%s'\n",image,sound);

			//New tuple
			samplebutton_tuple* tuple;
			tuple = g_new( samplebutton_tuple, 1 );
			tuple->image= g_strdup_printf("wheel_game/%s",image);
			tuple->sound = g_strdup_printf("wheel_game/%s",sound);

			result=g_list_append(result, tuple);
		}

	}

	}
	//Add last empty-sample-button (used with 'cannot-fail')
	samplebutton_tuple* null_tuple;
	null_tuple = g_new( samplebutton_tuple, 1 );
	null_tuple->image= g_strdup_printf("wheel_game/empty.png");
	null_tuple->sound = NULL;
	result=g_list_append(result, null_tuple);

	g_free(filename);
	return result;
}



static gboolean sample_button_listened(JammoSampleButton* sample_button, gpointer data){
	gchar* name;
	g_object_get(sample_button,"sample-filename", &name,NULL);
	gchar *message = g_strdup_printf("Sample '%s' listened on stage",name);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);
	return TRUE;
}

/*
loop_type:
1000=rhythms
2000=melodics
3000=harmonics
4000=effects

returns number of this type of sample_buttons
*/
static void add_sample_buttons(ClutterActor* looper, GList* table, int loop_type) {
	if (looper==NULL)
		return;
	int counter=0;
	gint max = g_list_length( table );

	for (counter=0;counter<max;counter++) {
		samplebutton_tuple* new_button=(samplebutton_tuple*)g_list_nth_data(table, counter);
		ClutterActor* sample_button;
		int loop_id = counter+loop_type;
		gchar* full_image = g_strdup(new_button->image);
		gchar* full_sound = g_strdup_printf("%s", new_button->sound);
		//printf("new_sample: '%s', '%s'\n",full_image,full_sound);
		sample_button = jammo_sample_button_new_with_type(type_by_id(loop_id));
		g_object_set(sample_button,"sequencer", jammo_get_object_by_id("fullsequencer-the-sequencer"),NULL);

		//With clutter-1.4 samplebutton should be in container before image_filename is set
		// (runtime Warning: Spurious clutter_actor_allocate called for actor X which isn't a descendent of the stage!)
		tangle_widget_add(TANGLE_WIDGET(looper),sample_button,NULL);

		//printf("DEBUG: looking image: '%s' and audio: '%s'\n",full_image,full_sound);
		jammo_sample_button_set_image_filename(JAMMO_SAMPLE_BUTTON(sample_button),full_image);
		jammo_sample_button_set_sample_filename(JAMMO_SAMPLE_BUTTON(sample_button), full_sound);

		//name contains _34_ (means 3/4) which means waltz-sample
		if (new_button->sound!=NULL && g_strrstr(new_button->sound,"_34_")!=NULL) {
			jammo_sample_button_set_waltz(JAMMO_SAMPLE_BUTTON(sample_button),TRUE);
			jammo_sample_button_set_text(JAMMO_SAMPLE_BUTTON(sample_button),"3");
		}


		//Logging
		g_signal_connect (sample_button, "listened", G_CALLBACK (sample_button_listened),NULL);

		g_object_set(sample_button,"drag-threshold-y",10000,NULL); //No dragging by vertical-axis (it is for scrolling wheel)
		g_signal_connect(sample_button,"drag-begin", G_CALLBACK(sample_button_drag_begin),NULL);

		//sound==NULL -> this is not really samplebutton: it can't be dragged at all.
		if (new_button->sound==NULL)
			//No dragging nor high-lighting
			clutter_actor_set_reactive(sample_button,FALSE);
		else
			jammo_sample_button_set_loop_id(JAMMO_SAMPLE_BUTTON(sample_button), loop_id);

		g_free(full_image);
		g_free(full_sound);

	}
	return;
}

ClutterActor* sequencer_loop_get_nth_sample_looper(int wheel_index) {
	switch (wheel_index) {
		case 1: return rhytmical_sample_looper;
		case 2: return melodical_sample_looper;
		case 3: return harmonical_sample_looper;
		case 4: return effect_sample_looper;
		default: return NULL;
	}
}

void sequencer_loop_tune_wheels(int level_number) {
	//Make them empty. TODO: handle proper free'ing when sequencer is shutdown.
	rhythms = NULL;
	melodics = NULL;
	harmonics = NULL;
	effects = NULL;

	//Static controls-for-edit1$mute-button
	ClutterActor* container = jammo_get_actor_by_id("fullsequencer_four_wheels");
	if (container==NULL)
		return;
	//It is: jammo_get_actor_by_id("fullsequencer_four_wheels$0"), but can't find!
	rhytmical_sample_looper  = tangle_widget_get_nth_child(TANGLE_WIDGET(container),0);
	melodical_sample_looper  = tangle_widget_get_nth_child(TANGLE_WIDGET(container),1);
	harmonical_sample_looper = tangle_widget_get_nth_child(TANGLE_WIDGET(container),2);
	effect_sample_looper     = tangle_widget_get_nth_child(TANGLE_WIDGET(container),3);

	if (level_number==1) {
		rhythms = load_samples_from_json ("Rh_samples_for_level1.json");
		melodics = load_samples_from_json ("Me_samples_for_level1.json");

		//Last empty-sample-button is not used on level1
		rhythms = g_list_remove(rhythms,g_list_last(rhythms)->data);
		melodics = g_list_remove(melodics,g_list_last(melodics)->data);
	}
	else {
		rhythms = load_samples_from_json ("Rh_samples.json");
		melodics = load_samples_from_json ("Me_samples.json");
		harmonics = load_samples_from_json ("Ha_samples.json");
		effects = load_samples_from_json ("Fx_samples.json");
	}

	add_sample_buttons(rhytmical_sample_looper,rhythms,1000);
	add_sample_buttons(melodical_sample_looper,melodics,2000);
	add_sample_buttons(harmonical_sample_looper,harmonics,3000);
	add_sample_buttons(effect_sample_looper,effects,4000);

	fine_tune_wheel(rhytmical_sample_looper);
	fine_tune_wheel(melodical_sample_looper);
	fine_tune_wheel(harmonical_sample_looper);
	fine_tune_wheel(effect_sample_looper);
}



/*
Each jammo-sample-button has loop-id.
First digit tells type of loop.

Will return NULL, if anything goes wrong.

*/
ClutterActor* sequencer_loop_give_sample_button_for_this_id(guint id){
	GList* asked_table;
	int length_of_asked;
	guint id_; //Id without first digit
	//gchar* type;  //TODO: use this
	if (id>=1000 && id <2000){
		asked_table=rhythms;
		id_=id-1000;
	} else if (id>=2000 && id <3000) {
		asked_table=melodics;
		id_=id-2000;
	} else if (id>=3000 && id <4000) {
		asked_table=harmonics;
		id_=id-3000;
	}	else if (id>=4000 && id <5000) {
		asked_table=effects;
		id_=id-4000;
	} else {
		return NULL;
	}

	length_of_asked=g_list_length(asked_table);
	if (id_>length_of_asked)
		return NULL;

	ClutterActor* sample_button;
	samplebutton_tuple* new_button=g_list_nth_data(asked_table,id_);
	gchar* full_image = tangle_lookup_filename(new_button->image);
	gchar* full_sound = g_strdup_printf("%s", new_button->sound);
	//printf("new_sample: '%s', '%s'\n",full_image,full_sound);
	sample_button = jammo_sample_button_new_with_type(type_by_id(id));

	//g_object_set(sample_button,"sequencer", jammo_get_object_by_id("fullsequencer-the-sequencer"),NULL);
	jammo_sample_button_set_image_filename(JAMMO_SAMPLE_BUTTON(sample_button),full_image);
	jammo_sample_button_set_sample_filename(JAMMO_SAMPLE_BUTTON(sample_button), full_sound);

	jammo_sample_button_set_loop_id(JAMMO_SAMPLE_BUTTON(sample_button), id);
	g_free(full_image);
	g_free(full_sound);

	return sample_button;
}
