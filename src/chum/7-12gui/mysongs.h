/*
License: GPLv2, read more from COPYING

This file is for clutter based gui.
*/

#ifndef MYSONGS_H_
#define MYSONGS_H_

#include <glib-object.h>

void start_mysongs();

gboolean mysongs_goto_communitymenu(TangleButton *tanglebutton, gpointer none);
gboolean mysongs_goto_startmenu(TangleButton *tanglebutton, gpointer none);
void mysongs_end_mysongs();
//Functions for creating and moving loops
gboolean mysongs_add_loop_to_list(char* loopName, int numberOfLoops, gchar* json_param);
void mysongs_delete_song(ClutterActor *actor);
void mysongs_remove_song_view(ClutterActor *actor);
void mysongs_cancel_song_delete(ClutterActor *actor);
void mysongs_get_comments(ClutterActor * json_);
void mysongs_send_song(ClutterActor *actor);
void mysongs_refresh_list();
void mysongs_update_label_button(const gchar* path);
void mysongs_label_button_clicked(ClutterActor* label);
gboolean find_song_by_filename(char* songName, GList* songlist);
void start_sequencer_with_this_file(ClutterActor *actor, gpointer none);
gboolean sendJammoSong(const char *song_filename, const char *song_cover, const char *comments_file, const char *all_songs_json); 
gboolean mysongs_copy_label(gchar* label_param, guint32 userid_param, const gchar* songname_param);

#endif 
