#include <tangle.h>
#include <string.h>
#include "../../cem/cem.h"
#include "../../configure.h"
#include "avatar.h"
#include "community_utilities.h"

/**
 * Loads profile information from json.
**/
void avatar_parse_json(gchar* filename,gchar** username, gchar** hobbies, gchar** gender, guint32* serialized_image, guint32* age) {

	JsonParser *parser;
	parser = json_parser_new ();
	g_assert (JSON_IS_PARSER (parser));
	GError *error = NULL;

	if (!json_parser_load_from_file (parser, filename, &error))
		{
		gchar* message = g_strdup_printf("avatar_parse_json: '%s' with file '%s'",error->message,filename);
		cem_add_to_log(message,J_LOG_ERROR);
		g_free(message);
		g_error_free (error);
		g_object_unref (parser);
		*serialized_image = 269488404; //Default-value
		return;
		}

	JsonNode *root;
	JsonObject *object;
	JsonNode *node;
	const gchar* temp_string;

	g_assert (NULL != json_parser_get_root (parser));

	root = json_parser_get_root (parser);
	g_assert_cmpint (JSON_NODE_TYPE (root), ==, JSON_NODE_OBJECT);

	object = json_node_get_object (root);
	g_assert (object != NULL);


	cem_add_to_log("Found avatar file",J_LOG_INFO);

	node = json_object_get_member(object, "username");
	if (node != NULL && JSON_NODE_TYPE(node) == JSON_NODE_VALUE){
		temp_string = json_node_get_string (node);
		if (temp_string) {
			g_free(*username); //Free default value.
			*username = g_strdup_printf("%s",temp_string);
			}
		//printf("  username: '%s'\n",*username);
	}

	node = json_object_get_member(object, "hobbies");
	if (node != NULL && JSON_NODE_TYPE(node) == JSON_NODE_VALUE){
			temp_string = json_node_get_string (node);
		if (temp_string) {
			g_free(*hobbies); //Free default value.
			*hobbies = g_strdup_printf("%s",temp_string);
			}
		//printf("  hobbies: '%s'\n",*hobbies);
	}

	node = json_object_get_member(object, "gender");
	if (node != NULL && JSON_NODE_TYPE(node) == JSON_NODE_VALUE){
			temp_string = json_node_get_string (node);
		if (temp_string) {
			g_free(*gender); //Free default value.
			*gender = g_strdup_printf("%s",temp_string);
			}
		//printf("  gender: '%s'\n",*gender);
	}

	node = json_object_get_member(object, "serialized_image");
	if (node != NULL && JSON_NODE_TYPE(node) == JSON_NODE_VALUE){
		*serialized_image = json_node_get_int(node);
		//printf("  serialized_image: '%d'\n",*serialized_image);
	}


	node = json_object_get_member(object, "age");
	if (node != NULL && JSON_NODE_TYPE(node) == JSON_NODE_VALUE){
		*age = json_node_get_int(node);
		//printf("  age: '%d'\n",*age);
	}


	g_object_unref(parser);
	return;
}

/*
 This is now lazy function (using avatar_parse_json).
 It will each time perform new file read!
 */
/* usefull? gems_profile_manager_get_avatar_id(gems_profile_manager_get_profile_of_user(user_id));*/
guint32 avatar_give_serialized_image_for_this_id(guint32 avatar_id){
	guint32 serialized_image;

	//These are not used
	gchar* username = g_strdup_printf(" ");
	gchar* hobbies = g_strdup_printf(" ");
	gchar* gender = g_strdup_printf(" ");
	guint32 age=0;

	gchar* profile_path = g_strdup_printf("%s/user_%d.json", configure_get_temp_directory(), avatar_id);
	avatar_parse_json(profile_path,&username,&hobbies,&gender,&serialized_image,&age);

 return serialized_image;
}

/*
 This is now lazy function (using avatar_parse_json).
 It will each time perform new file read!

 Caller must free returned gchar*
 */
gchar* avatar_give_username_for_this_id(guint32 avatar_id){

	gchar* username = g_strdup_printf("Anonymous"); //Default name
	//These are not used
	gchar* hobbies = g_strdup_printf(" ");
	gchar* gender = g_strdup_printf(" ");
	guint32 serialized_image;
	guint32 age=0;

	gchar* profile_path = g_strdup_printf("%s/user_%d.json", configure_get_temp_directory(), avatar_id);
	avatar_parse_json(profile_path,&username,&hobbies,&gender,&serialized_image,&age);

	return username;
}


/**
 * Computes the avatar from guint32 type number.
 *
 * Avatar is serialized in 32 bits.
 *
 * Note: ids are index numbers of avatar parts. Parts are arranged in alphabetical order.
 *
 * parameters:
 * -avatar is pointer to a Avatar struct, where the avatar will be created.
**/
gboolean avatar_unserialize_image(Avatar* avatar, guint32 serialized_image){
	//printf("avatar_unserialize_image '%d'\n",serialized_image);

	int head, hair_color, hair_type, body, instrument, eyes;
	char path[150];
	char result[MAX_FILES_IN_DIR][MAX_FILE_NAME_LEN];

	head =       (serialized_image & 0x0000000F);           //bits 0-3
	hair_color = (serialized_image & 0x000000F0) >> 4;      //bits 4-7
	body =       (serialized_image & 0x00000F00) >> 8;      //bits 8-11
	hair_type =  (serialized_image & 0x000FF000) >> 12;     //bits 12-19
	eyes  =      (serialized_image & 0x00F00000) >> 20;     //bits 20-23
	instrument = (serialized_image & 0xFF000000) >> 24;     //bits 24-31

	//printf("unserialize: head=%d, hair_color=%d, body=%d, hair_type=%d, eyes=%d, instrument=%d\n",head,hair_color,body,hair_type,eyes,instrument);

	gchar* heads_path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/heads/", DATA_DIR);
	gchar* bodys_path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/bodys/", DATA_DIR);
	gchar* hairs_path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/hairs/", DATA_DIR);
	gchar* eyes_path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/eyes/", DATA_DIR);
	gchar* instruments_path = g_strdup_printf("%s/communitymenu/INSTRUMENTS/", DATA_DIR);

	if(head != 0){
		strcpy(path, heads_path);
		community_utilities_open_and_arrange_dir(path, ".png", result);
		avatar->head = tangle_texture_new(strcat(path, result[head-1]));
		clutter_actor_set_name(avatar->head, path);
	}

	if(body != 0){
		strcpy(path, bodys_path);
		community_utilities_open_and_arrange_dir(path, ".png", result);
		avatar->body = tangle_texture_new(strcat(path, result[body-1]));
		clutter_actor_set_name(avatar->body, path);
	}

	if(eyes != 0){
		strcpy(path, eyes_path);
		community_utilities_open_and_arrange_dir(path, ".png", result);
		avatar->eyes = tangle_texture_new(strcat(path, result[eyes-1]));
		clutter_actor_set_name(avatar->eyes, path);
	}

	if(hair_color != 0 && hair_type != 0){
		strcpy(path, hairs_path);
		community_utilities_open_and_arrange_dir(path, "DIR", result);

		strcat(path, result[hair_color-1]);
		strcat(path, "/");

		community_utilities_open_and_arrange_dir(path, ".png", result);
		strcat(path, result[hair_type-1]);

		avatar->hair = tangle_texture_new(path);
		clutter_actor_set_name(avatar->hair, path);
	}

	if(instrument != 0){
		strcpy(path, instruments_path);
		community_utilities_open_and_arrange_dir(path, ".png", result);
		avatar->instrument = tangle_button_new_with_background_actor(tangle_texture_new(strcat(path, result[instrument-1])));
		clutter_actor_set_name(avatar->instrument, path);
	}

	g_free(heads_path);
	g_free(bodys_path);
	g_free(eyes_path);
	g_free(hairs_path);
	g_free(instruments_path);

	return TRUE;
}

/**
 * Computes guint32 number from avatar.
 *
 * Parts are arranged in alphabetical order.
 *
**/
guint32 avatar_serialize_image(Avatar* avatar){

	guint32 head = 0, hair_color = 0, hair_type = 0, body = 0, instrument = 0, eyes=0;
	char path[150];
	char result[MAX_FILES_IN_DIR][MAX_FILE_NAME_LEN];
	int i;
	int numberOfImages = 0;

	gchar* heads_path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/heads/", DATA_DIR);
	gchar* bodys_path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/bodys/", DATA_DIR);
	gchar* eyes_path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/eyes/", DATA_DIR);
	gchar* hairs_path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/hairs/", DATA_DIR);
	gchar* instruments_path = g_strdup_printf("%s/communitymenu/INSTRUMENTS/", DATA_DIR);

	if(avatar->head != 0){
		strcpy(path, heads_path);
		numberOfImages = community_utilities_open_and_arrange_dir(path, ".png", result);

		for(i = 0; i < numberOfImages; i++)
			if(strstr(clutter_actor_get_name(avatar->head), result[i]) != 0)
				break;

		head = i+1;
	}

	if(avatar->body != 0){
		strcpy(path, bodys_path);
		numberOfImages = community_utilities_open_and_arrange_dir(path, ".png", result);

		for(i = 0; i < numberOfImages; i++)
			if(strstr(clutter_actor_get_name(avatar->body), result[i]) != 0)
				break;

		body = i+1;
	}

	if(avatar->eyes != 0){
		strcpy(path, eyes_path);
		numberOfImages = community_utilities_open_and_arrange_dir(path, ".png", result);

		for(i = 0; i < numberOfImages; i++)
			if(strstr(clutter_actor_get_name(avatar->eyes), result[i]) != 0)
				break;

		eyes = i+1;
	}


	if(avatar->hair != 0){
		strcpy(path, hairs_path);
		numberOfImages = community_utilities_open_and_arrange_dir(path, "DIR", result);

		for(i = 0; i < numberOfImages; i++)
			if(strstr(clutter_actor_get_name(avatar->hair), result[i]) != 0)
				break;

		hair_color = i+1;

		strcpy(path, clutter_actor_get_name(avatar->hair));

		char *temp = strrchr(path, '/');
		temp++;
		*temp = 0;
		numberOfImages = community_utilities_open_and_arrange_dir(path, ".png", result);

		for(i = 0; i < numberOfImages; i++)
			if(strstr(clutter_actor_get_name(avatar->hair), result[i]) != 0)
				break;

		hair_type = i+1;
	}

	if(avatar->instrument != 0){
		strcpy(path, instruments_path);
		numberOfImages = community_utilities_open_and_arrange_dir(path, ".png", result);

		for(i = 0; i < numberOfImages; i++)
			if(strstr(clutter_actor_get_name(avatar->instrument), result[i]) != 0)
				break;

		instrument = i+1;
	}

	guint32 id = 0;

	//printf("serialize: head=%d, hair_color=%d, body=%d, hair_type=%d, eyes=%d, instrument=%d\n",head,hair_color,body,hair_type,eyes,instrument);

	id = head;
	id += (hair_color << 4);
	id += (body << 8);
	id += (hair_type << 12);
	id += (eyes << 20);
	id += (instrument << 24);

	g_free(heads_path);
	g_free(bodys_path);
	g_free(hairs_path);
	g_free(eyes_path);
	g_free(instruments_path);

	#ifdef NETWORKING_ENABLED
	gems_profile_manager_set_avatar_id(id);
	#endif
	return id;
}


/**
 * Scales avatar to 30%,  so it can be used in thread-views.
 * It must be already in container.
 *
*/
void avatar_shrink(Avatar* avatar){
	gfloat width, height;

	if(avatar->body != 0){
		clutter_actor_get_size(avatar->body, &width, &height);
		clutter_actor_set_size(avatar->body, width*0.3, height*0.3);
		clutter_actor_set_position(avatar->body, 7, 35);
	}

	if(avatar->head != 0){
		clutter_actor_get_size(avatar->head, &width, &height);
		clutter_actor_set_size(avatar->head, width*0.3, height*0.3);
		clutter_actor_set_position(avatar->head, 14, 10);
	}

	if(avatar->hair != 0){
		clutter_actor_get_size(avatar->hair, &width, &height);
		clutter_actor_set_size(avatar->hair, width*0.3, height*0.3);
		clutter_actor_set_position(avatar->hair, 2, -5);
	}

	if(avatar->eyes != 0){
		clutter_actor_get_size(avatar->eyes, &width, &height);
		clutter_actor_set_size(avatar->eyes, width*0.3, height*0.3);
		clutter_actor_set_position(avatar->eyes, 15, 9);
	}

	// hide/remove instrument
	if(avatar->instrument != 0){
		clutter_container_remove_actor(CLUTTER_CONTAINER(clutter_actor_get_parent(avatar->instrument)),avatar->instrument);
	}

}
