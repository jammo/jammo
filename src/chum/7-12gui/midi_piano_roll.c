#include <tangle.h>
#include <glib-object.h>
#include <ctype.h>
#include <string.h>
#include <clutter/clutter.h>

#include "midi_helper.h"

#include "../../meam/jammo-midi.h"


/**
 * This method will draw piano roll to the left, including black/white keys & letters of the current notes.
 */
ClutterActor*  create_piano_roll(gfloat height_of_key, int highest_note, int lowest_note, gboolean with_text) {

	//Placeholder for piano key group
	ClutterActor* pianoLeft=tangle_widget_new();
	clutter_actor_show(pianoLeft);

	//Placeholder for note names
	ClutterActor* pianoTexts=clutter_group_new();
	clutter_actor_show(pianoTexts);
	tangle_widget_add(TANGLE_WIDGET(pianoLeft), pianoTexts, NULL);

	int t;
	gfloat yPosition = 0;
	// this the loop for pianokeys and letters
	for (t = highest_note; t > lowest_note-1; t--) { //highest_note will be topmost (it has lowest y-coordinate)
		//printf("note %d, position %f\n",t,yPosition);
		const char* current_note = jammomidi_note_to_char(t);
		ClutterActor* key;

		if (current_note[1]=='#'){ //Sharp = Black piano key
			if (with_text)
				draw_text(current_note, 70.0, yPosition+20.0, pianoTexts, midi_editor_get_color("white"));
			key = tangle_texture_new("/opt/jammo/midieditor/pianoblack.png");
		}
		else { // White piano key
			if (with_text)
				draw_text(current_note, 70.0, yPosition+20.0, pianoTexts, midi_editor_get_color("black"));
			key = tangle_texture_new("/opt/jammo/midieditor/pianowhite.png");
		}

		clutter_actor_set_position(key, 0, yPosition);
		clutter_actor_show(key);
		tangle_widget_add(TANGLE_WIDGET(pianoLeft), key, NULL);

		yPosition += height_of_key;
	}

	clutter_actor_raise_top(pianoTexts);
	clutter_actor_set_size(pianoLeft,100.0,yPosition); //For some reason, this doesn't handle own sizes correctly (last note is halved)
	//printf("foot of most bottom notes: %f\n", yPosition);
	return pianoLeft;
}

