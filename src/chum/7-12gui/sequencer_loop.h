/**sequencer_loop.h is part of JamMo.
License: GPLv2, read more from COPYING

This file is for clutter based gui.
This is part of the sequencer.
 */

#ifndef SEQUENCER_LOOP_H_
#define SEQUENCER_LOOP_H_

void sequencer_loop_tune_wheels(int level_number);
ClutterActor* sequencer_loop_give_sample_button_for_this_id(guint id);
ClutterActor* sequencer_loop_get_nth_sample_looper(int index);
void sequencer_loop_randomize_wheels();
#endif /* SEQUENCER_LOOP_H_ */
