/*
License: GPLv2, read more from COPYING

This file contains the functionality of Workshops menu.
 */
#include <tangle.h>
#include <string.h>
#include "communitymenu.h"
#include "startmenu.h"
#include "../jammo.h"
#include "workshops.h"
#include "community_utilities.h"
#include "../../cem/cem.h"
#include "../../configure.h"
#include "../file_helper.h"

#include "sequencer.h" //From workshops to sequencer

static list_type songlist = {0, 0};
ClutterColor workshop_text_color = { 255, 255, 255, 255 };

void start_workshops(){

	cem_add_to_log("Starting Workshops", J_LOG_DEBUG);

	int numberOfLoops = 0;

	GList *l = NULL; //Iterator
	GList *projects = NULL;
	
	tangle_actor_hide_animated(TANGLE_ACTOR(jammo_get_actor_by_id("communitymenu-view")));

	ClutterActor* mainview;
	mainview = jammo_get_actor_by_id("main-views-widget");

	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);
	
	ClutterActor* workshop_view = jammo_get_actor_by_id("workshop-view");

	if(workshop_view)
	{
		clutter_actor_show(CLUTTER_ACTOR(workshop_view));

		projects = file_helper_get_all_files(configure_get_projects_directory());

		numberOfLoops += g_list_length (projects);

		if(numberOfLoops > 5)
			songlist.list = CLUTTER_CONTAINER(jammo_get_actor_by_id("workshop-songs-list-scrollable"));	

		else	songlist.list = CLUTTER_CONTAINER(jammo_get_actor_by_id("workshop-songs-list-unscrollable"));	

		for (l = projects; l; l = l->next)
			workshop_add_loop_to_list(l->data);

		if(numberOfLoops <= 6)
			clutter_actor_set_height(CLUTTER_ACTOR(songlist.list), songlist.listItemHeight * numberOfLoops);

		else	clutter_actor_set_height(CLUTTER_ACTOR(songlist.list), songlist.listItemHeight * 6);


		ClutterAction *listAction = tangle_actor_get_action_by_type(CLUTTER_ACTOR(songlist.list), TANGLE_TYPE_SCROLL_ACTION);

		g_signal_connect_swapped(listAction, "clamp-offset-y", G_CALLBACK(community_utilities_autoscroll_list), &songlist);
	}

	else 
		cem_add_to_log("can't find 'workshop-view' ", J_LOG_ERROR);
	
}
gboolean workshop_goto_communitymenu(TangleButton *tanglebutton, gpointer none)
{
	community_utilities_clear_container_recursively(songlist.list);
	start_communitymenu();
	return TRUE;
}

gboolean workshop_goto_startmenu(TangleButton *tanglebutton, gpointer none)
{
	community_utilities_clear_container_recursively(songlist.list);
	startmenu_goto_startmenu(tanglebutton, none);
	return TRUE;
}

gboolean workshop_mentor_clicked (TangleButton *tanglebutton, gpointer data)
{
	cem_add_to_log("mentor clicked", J_LOG_DEBUG);
	return TRUE;
}

gboolean workshop_add_loop_to_list(char* filename)
{
	gchar* colorbar_filename = g_strdup_printf("%s/communitymenu/colorbar1.png", DATA_DIR);
	gchar* sequencerButton_filename = g_strdup_printf("%s/communitymenu/community_backtosequencer.png", DATA_DIR);

	ClutterActor *colorbar = tangle_texture_new(colorbar_filename);
	ClutterActor *textButton = NULL;
	ClutterActor *text = NULL;
	ClutterActor *sequencerButton =tangle_button_new_with_background_actor(tangle_texture_new(sequencerButton_filename));
	ClutterContainer *listContainer = CLUTTER_CONTAINER(clutter_group_new());

	int i 		= 0;
	int j 		= 0;
	int type_length = 5; //.json
	int path_length = strlen(configure_get_projects_directory());
	int length	= strlen(filename);

	char temp[length];
	char filename_for_textbutton[length];

	strcpy(temp, filename);

	for(i = path_length + 1; i < length - type_length; i++)
	{
		filename_for_textbutton[j] = temp[i];
		j++;
	}

	if(songlist.listItemHeight == 0) songlist.listItemHeight = clutter_actor_get_height(colorbar);

	filename_for_textbutton[j] = 0;

	text = clutter_text_new_full(TEXT_NORMAL, filename_for_textbutton, &workshop_text_color);

	textButton = tangle_button_new_with_background_actor(text);

	clutter_actor_set_name(textButton,filename);

	clutter_text_set_editable (CLUTTER_TEXT(text), FALSE);
	clutter_text_set_max_length (CLUTTER_TEXT(text), 99);
	clutter_text_set_single_line_mode (CLUTTER_TEXT(text), TRUE);

	clutter_actor_set_name(sequencerButton,filename);

	if(colorbar == NULL || sequencerButton == NULL)
	{
		cem_add_to_log("Failed to load Colorbar or sequencerButtons", J_LOG_ERROR);
		return FALSE;	
	}	

	clutter_container_add_actor(listContainer, colorbar);	//Add components inside the container

	clutter_actor_set_position(textButton, 60, 23);			
	clutter_container_add_actor(listContainer, textButton);

	clutter_actor_set_position(sequencerButton, 690, 5);
	clutter_container_add_actor(listContainer, sequencerButton);

	g_free(colorbar_filename);	
	g_free(sequencerButton_filename);
	
	//FIXME: use sequencerButton as this used to be:
	//g_signal_connect_swapped(textButton, "clicked", G_CALLBACK(play_selected_loop), textButton);
	//g_signal_connect (sequencerButton, "clicked", G_CALLBACK (start_sequencer_with_this_file),NULL);

	g_signal_connect(textButton, "clicked", G_CALLBACK(song_on_clicked_function), NULL);
	g_object_set(textButton, "hold-timeout", 1500, NULL);
	g_signal_connect(textButton, "held", G_CALLBACK(song_on_held_function), NULL);

	clutter_container_add_actor(songlist.list, CLUTTER_ACTOR(listContainer));
	
	tangle_actor_set_depth_position(TANGLE_ACTOR(sequencerButton), 8);

	return TRUE;
}
