/*
License: GPLv2, read more from COPYING

This file contains the functionality of discuss menu.
 */
#include <tangle.h>
#include <glib-object.h>
#include <math.h>
#include <string.h>
#include <clutter/clutter.h>
#include "discuss.h"
#include "communitymenu.h"
#include "startmenu.h"
#include "single_thread.h"
#include "community_utilities.h"
#include "profile_viewer.h"
#include "../../cem/cem.h"
#include "avatar.h"
#include "avatar_editor.h"
#include <stdlib.h>

//File containing topic names (etc)
#define FORUM_TOPICS_FILE "forum_topics.json"

static void autoscroll_to_correct_pos(ClutterActor *list_);

static ClutterColor line_color = { 144, 148, 140, 255 };
static ClutterColor text_color = { 237, 28, 36, 255 };
static ClutterColor text_color2 = { 0, 0, 0, 255 };
static ClutterContainer *list = 0;
static ClutterContainer *threadslist = 0;
static GList* static_discussionthreads = 0;
static GList* threads = 0;
extern GList* paramsList;
static gint item_height = 0;
static gchar * currentThread = NULL;
static TextField titleTextField;
static TextField descriptionTextField;

static gchar* getAllDiscussionThreads();

static void start_addthread_view();
static gboolean addDiscussion(gpointer data, int num);
static gboolean addThread(gpointer data, int number);
static void end_discuss();
static void clear_discussions(GList* list_);
static void getThread(ClutterActor *element);
static gboolean load_threads_from_server(gchar* currentThread);
static gboolean sendJammoThread(gchar* thread_path, gchar* messages_path);
static gboolean discuss_change_to_threads_view(ClutterActor * element);
static gboolean parse_discussions_from_file(gchar* filename, int type);
static gboolean discuss_send_thread(gint index_, guint32 userid, const gchar* sender, const gchar* title, const gchar* msg, const gchar* filepath, const gchar* time_, int msgtype);
static ClutterActor *create_line(int y, int length, int size, ClutterContainer *cont);

/**
 * Starts discuss-view
**/
void start_discuss(){
	cem_add_to_log("Starting Discussusion Forum", J_LOG_DEBUG);

	GList* l = NULL; //Iterator
	int entries_size = 0;
	int number = 0;
	static_discussionthreads = 0;

	ClutterActor* mainview;
	mainview = jammo_get_actor_by_id("main-views-widget");

	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);

	ClutterActor* discuss_view = jammo_get_actor_by_id("discuss-view");

	if(!discuss_view){
		cem_add_to_log("can't find 'discuss-view' ", J_LOG_FATAL);
	}

	//NOTE: FORUM_TOPICS_FILE must exist or list will be empty. See end of this file for example.
	gchar* all_discussionthreads_filepath = getAllDiscussionThreads();
	//printf("filepath is '%s'\n",all_discussionthreads_filepath);

	if (all_discussionthreads_filepath) {
		int type = 0; //List of topics
		if(parse_discussions_from_file(all_discussionthreads_filepath, type))
			cem_add_to_log("parsing all_discussionthreads success", J_LOG_DEBUG);
		else
			cem_add_to_log("parsing all_discussionthreads fails", J_LOG_ERROR);
		g_free(all_discussionthreads_filepath);
	}

	entries_size = g_list_length(static_discussionthreads);

	clutter_actor_show(CLUTTER_ACTOR(discuss_view));

	if(entries_size > 5){
		list = CLUTTER_CONTAINER(jammo_get_actor_by_id("discuss-view-container"));
	}else{
		list = CLUTTER_CONTAINER(jammo_get_actor_by_id("discuss-view-container-unscrollable"));
	}
	for(l = static_discussionthreads; l; l = l->next){
		addDiscussion(l->data, number);
		number++;
	}

	ClutterAction* action = tangle_actor_get_action_by_type(CLUTTER_ACTOR(list), TANGLE_TYPE_SCROLL_ACTION);
	if (action)
		g_signal_connect_swapped(action, "clamp-offset-y", G_CALLBACK(autoscroll_to_correct_pos), list);
}

/**
 * Starts thread view
**/
void start_discussionthreads()
{	
	GList* l = NULL; //Iterator
	int numberOfThreads = 0, i = 0;
	threads = 0;

	ClutterActor* mainview;
	mainview = jammo_get_actor_by_id("main-views-widget");

	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL); 

	ClutterActor* discussion_threads_view = jammo_get_actor_by_id("threads-view");

	if(discussion_threads_view)
	{
		if(currentThread != NULL){
			load_threads_from_server(currentThread); //Get the threads
			numberOfThreads = g_list_length(threads);
		}		
		clutter_actor_show(CLUTTER_ACTOR(discussion_threads_view));

		if(numberOfThreads > 5)
			threadslist = CLUTTER_CONTAINER(jammo_get_actor_by_id("threads-view-list-scrollable"));	
		else
			threadslist = CLUTTER_CONTAINER(jammo_get_actor_by_id("threads-view-list-unscrollable"));

		for(l = threads; l; l = l->next){
			addThread(l->data, i);  //TODO: get proper user_id (starter of thread)
			i++;
		}

		ClutterAction* action = tangle_actor_get_action_by_type(CLUTTER_ACTOR(threadslist), TANGLE_TYPE_SCROLL_ACTION);
		if (action)
			g_signal_connect_swapped(action, "clamp-offset-y", G_CALLBACK(autoscroll_to_correct_pos), CLUTTER_ACTOR(threadslist));

	}else{
		cem_add_to_log("can't find 'discuss-container-for-discussion-threads' ", J_LOG_ERROR);
	}

}

/**
 * Shows add thread view used for adding new threads
**/
static void start_addthread_view() {
	ClutterActor* mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);
	
	ClutterActor* view = jammo_get_actor_by_id("addthread-view");	

	if(view){
		clutter_actor_show(CLUTTER_ACTOR(view));

		titleTextField.text = jammo_get_actor_by_id("title");
		titleTextField.lineCount = 1;
		titleTextField.max_characters = 50;
		titleTextField.handler_id = g_signal_connect_swapped(titleTextField.text,
                "text-changed", (GCallback)community_utilities_limit_line_count, &titleTextField);

		descriptionTextField.text = jammo_get_actor_by_id("description");
		descriptionTextField.lineCount = 5;
		descriptionTextField.max_characters = 300;
		descriptionTextField.handler_id = g_signal_connect_swapped(descriptionTextField.text,
                "text-changed", (GCallback)community_utilities_limit_line_count, &descriptionTextField);
	}else {	
		cem_add_to_log("can't find 'addthread-view' ", J_LOG_ERROR);
	}

}


/***************************
Functions to fill lists
**************************/

/**
 * Adds discussionthread to list
**/
static gboolean addDiscussion(gpointer data, int number){

	discussion_type *dthread = data;

	gchar* arrow_image_filename = g_strdup_printf("%s/communitymenu/community_rightarrow.png", DATA_DIR);
	const gchar * json = dthread->json;
	
	ClutterActor *line;
	ClutterActor *cont = CLUTTER_ACTOR(clutter_group_new());

	ClutterActor *arrow = tangle_button_new_with_background_actor(tangle_texture_new(
				arrow_image_filename));

	line = create_line(73, 800, 2, CLUTTER_CONTAINER(cont));

	g_free(arrow_image_filename);
        
	ClutterActor *title = clutter_text_new_full(TEXT_LARGE, dthread->title, &text_color);
	clutter_text_set_max_length(CLUTTER_TEXT(title), 15);
	clutter_actor_set_size(title, ((15*10)+30), 51); 

	ClutterActor *discussion = tangle_button_new_with_background_actor(title);
	clutter_actor_set_name(discussion, json);	

	if(line == NULL || arrow == NULL || dthread->title == NULL)
		return FALSE;

	if(item_height == 0)
		item_height = 75;

    	clutter_actor_set_position(arrow, 550, 12);
	clutter_container_add_actor(CLUTTER_CONTAINER(cont), arrow);
        
	clutter_container_add_actor(CLUTTER_CONTAINER(cont), discussion);
	clutter_actor_set_position(discussion, 320, 20);

	g_signal_connect_swapped(arrow, "clicked", G_CALLBACK(discuss_change_to_threads_view), CLUTTER_ACTOR(discussion));
	g_signal_connect_swapped(CLUTTER_ACTOR(discussion), "clicked", G_CALLBACK(discuss_change_to_threads_view), CLUTTER_ACTOR(discussion));
	
	clutter_container_add_actor(list, cont);
	
	if(number <= 6){
		clutter_actor_set_height(CLUTTER_ACTOR(list), item_height * number);
	}else{
		clutter_actor_set_height(CLUTTER_ACTOR(list), item_height * 6);
	}

	return TRUE;
}

/**
 * Adds thread to list
**/
static gboolean addThread(gpointer data, int number){
	
	discussion_type *thread = data;
	
	profile_view_params *params = malloc(sizeof(profile_view_params));
	paramsList = g_list_append(paramsList, params);
	params->user_id = thread->user_id;
	strcpy(params->parent_view, "discuss");
	
	Avatar avatar = {0,0,0,0};
	guint32 serialized_avatar_image = avatar_give_serialized_image_for_this_id(thread->user_id);
	avatar_unserialize_image(&avatar, serialized_avatar_image);


	gchar* avatarbg_image_filename = g_strdup_printf("%s/communitymenu/community_avatarbackground_small.png", DATA_DIR);
	gchar* comment_image_filename = g_strdup_printf("%s/communitymenu/community_comment_icon.png", DATA_DIR);
	
	ClutterActor *line;
	ClutterActor *cont = CLUTTER_ACTOR(clutter_group_new());

	ClutterActor *avatarbg = clutter_group_new();

	clutter_container_add_actor(CLUTTER_CONTAINER(avatarbg), tangle_texture_new(avatarbg_image_filename));

	ClutterActor *starter = tangle_button_new_with_background_actor(avatarbg);

	ClutterActor* comment_ = tangle_button_new_with_background_actor(tangle_texture_new(comment_image_filename)); 

	line = create_line(74, 650, 1, CLUTTER_CONTAINER(cont));

	ClutterActor *text = clutter_text_new_full(TEXT_LARGE, thread->title, &text_color2);
    	clutter_actor_set_size(text, ((35*12)+30), 51);
	clutter_text_set_max_length(CLUTTER_TEXT(text), 35);

	set_avatar(&avatar, CLUTTER_ACTOR(avatarbg));
	avatar_shrink(&avatar);

	ClutterActor *title = tangle_button_new_with_background_actor(text);
	clutter_actor_set_name(title, thread->json);

	if(line == NULL || comment_ == NULL || avatarbg == NULL|| thread->title == NULL)
		return FALSE;

	if(item_height == 0)
		item_height = 75;

    	clutter_actor_set_position(comment_, 690, 15);
	clutter_container_add_actor(CLUTTER_CONTAINER(cont), comment_);

    	clutter_actor_set_position(starter, 10, 1);
	clutter_container_add_actor(CLUTTER_CONTAINER(cont), starter);

	clutter_container_add_actor(CLUTTER_CONTAINER(cont), title);
	clutter_actor_set_position(title, 80, 20);

	g_signal_connect_swapped(CLUTTER_ACTOR(title), "clicked", G_CALLBACK(getThread), CLUTTER_ACTOR(title));
	g_signal_connect_swapped(comment_, "clicked", G_CALLBACK(getThread), CLUTTER_ACTOR(title));
	g_signal_connect_swapped(starter, "clicked", G_CALLBACK(start_profile_view), (gpointer)params);
	
	clutter_container_add_actor(threadslist, cont);
	
	if(number <= 6){
		clutter_actor_set_height(CLUTTER_ACTOR(threadslist), item_height * number);
	}else{
		clutter_actor_set_height(CLUTTER_ACTOR(threadslist), item_height * 6); 
	}
	g_free(avatarbg_image_filename);
	g_free(comment_image_filename);

	return TRUE;
}


static void clear_discussions(GList* list_){

	list_ = g_list_first(list_);

	discussion_type *temp = 0;

	if(list_ != NULL){
		
		temp = list_->data;

		if(temp->id != NULL)
			free(temp->id);
			
		if(temp->json != NULL)
			free(temp->json);

		if(list_->data != NULL)
			free(list_->data);
	
		while(list_->next != NULL)
		{
			list_ = list_->next;			
			temp = list_->data;

			if(temp->id != NULL)
				free(temp->id);
			
			if(temp->json != NULL)
				free(temp->json);

			if(list_->data != NULL)
				free(list_->data);				
		}//while

	g_list_free(list_);
	list_ = NULL;

	} //if
}

/**
 * Used to create line that separates list elements
**/
static ClutterActor *create_line(int y, int length, int size, ClutterContainer *cont) {
	ClutterActor *line = clutter_rectangle_new_with_color(&line_color);
	clutter_actor_set_size(line, length, size);
	clutter_actor_set_position(line, 0, y);

	clutter_container_add_actor(CLUTTER_CONTAINER(cont), line);
	clutter_actor_show(line);

	return line;
}


/***************************
Functions for json
**************************/
gboolean discuss_community_clicked (TangleButton* tanglebutton, gpointer none){
	end_discuss();

	startmenu_goto_communitymenu(NULL,NULL);
	return TRUE;
}

gboolean discuss_home_clicked (TangleButton* tanglebutton, gpointer none){
	end_discuss();

	startmenu_goto_startmenu(NULL,NULL);
	return TRUE;
}


gboolean discuss_mentor_clicked (TangleButton *tanglebutton, gpointer data)   {
	cem_add_to_log("Mentor clicked", J_LOG_DEBUG);

	return TRUE;
}

/* //Maybe never used by user?
gboolean discuss_create_new_topic (TangleButton *tanglebutton, gpointer data)   {
	cem_add_to_log("Making new thread...", J_LOG_DEBUG);

	return TRUE;
}
*/
/**
 * Ends discussview and clears all containers
**/
static void end_discuss(){
	community_utilities_clear_paramslist(paramsList);
	paramsList = 0;

	if(g_list_length(static_discussionthreads) != 0)
		clear_discussions(static_discussionthreads);
	if(g_list_length(threads) != 0)
		clear_discussions(threads);

	g_free(currentThread);

	community_utilities_clear_container_recursively(CLUTTER_CONTAINER(threadslist));
	community_utilities_clear_container_recursively(CLUTTER_CONTAINER(list));
	cem_add_to_log("Clear_containered containers", J_LOG_DEBUG);
}


/***************************
Callback functions
**************************/

/**
 * Checks that all fields of add thread view have enough text and then calls discuss_send_thread to send the thread to server
 *
 * JSON callback
**/
gboolean add_new_thread(TangleButton* tanglebutton, gpointer none){
	static gboolean is_title_ok = FALSE;
	static gboolean is_msg_ok = FALSE;

	cem_add_to_log("Adding thread...", J_LOG_DEBUG);

	ClutterActor *error_msg = jammo_get_actor_by_id("error_message");

	if(strlen(clutter_text_get_text(CLUTTER_TEXT(titleTextField.text))) < 3){
		
		cem_add_to_log("Title must be at least 3 characters long!", J_LOG_DEBUG);
		clutter_text_set_text(CLUTTER_TEXT(error_msg), "Title must be at least 3 characters long!");

		is_title_ok = FALSE;
	}else{
		is_title_ok = TRUE;
		cem_add_to_log("Title ok.", J_LOG_DEBUG);
	}

	if(strlen(clutter_text_get_text(CLUTTER_TEXT(descriptionTextField.text))) < 10){
		
		cem_add_to_log("Message must be longer than 10 characters!", J_LOG_DEBUG);
		clutter_text_set_text(CLUTTER_TEXT(error_msg), "Message must be longer than 10 characters!");

		is_msg_ok = FALSE;
	}else{
		is_msg_ok = TRUE;
		cem_add_to_log("Msg ok", J_LOG_DEBUG);	
	}

	if(is_title_ok == TRUE && is_msg_ok == TRUE){	
		guint32 user_id = TEST_USERID; //TODO: user_id =gems_profile_manager_get_userid(NULL)
		gchar* sender = avatar_give_username_for_this_id(user_id);


		gint thread_index = 0;
		const gchar* title = clutter_text_get_text(CLUTTER_TEXT(titleTextField.text));
		const gchar* msg = clutter_text_get_text(CLUTTER_TEXT(descriptionTextField.text));
		int type = TEXT_COMMENT;
		char date [80];
		cem_get_time(date);

		if(!discuss_send_thread(thread_index, user_id, sender, title, msg, "", date, type)){
			cem_add_to_log("Failed to add new thread: error happened while trying to send file.", J_LOG_DEBUG);
			clutter_text_set_text(CLUTTER_TEXT(error_msg), "Failed to add new thread. Please try again.");
		}else{
			cem_add_to_log("Added thread.", J_LOG_DEBUG);
		}
		g_free(sender);
		//clutter_text_set_text (CLUTTER_TEXT(jammo_get_actor_by_id("title")),"");
		//clutter_text_set_text (CLUTTER_TEXT(jammo_get_actor_by_id("description")),"");
		//clutter_text_set_text(CLUTTER_TEXT(error_msg), "");
		
		start_discussionthreads();
	}else{
		cem_add_to_log("Cannot add thread.", J_LOG_DEBUG);
	}
	return TRUE;
}

/**
 * Shows the messages of selected thread
**/
static void getThread(ClutterActor *element){
	const char* json_path = clutter_actor_get_name(element);
	
	cem_add_to_log("getThread", J_LOG_DEBUG);

	clear_discussions(threads); // <---ok?
	//threads = 0;
	community_utilities_clear_container_recursively(threadslist);

	ClutterActor* mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);

	start_thread_view( "discuss", json_path, 0);
	
}


/**
 * Changes the view to add thread view used for adding a new thread
 *
 * JSON callback
**/
gboolean discuss_change_to_add_thread_view(TangleButton* tangle_button, gpointer data) {
	cem_add_to_log("changing view to add thread view", J_LOG_DEBUG);

	community_utilities_clear_container_recursively(threadslist);
	clear_discussions(threads);
	//threads = 0;
	
	start_addthread_view();
	
	return TRUE;
}

/**
 * Changes the view from discussionthread view to thread view
**/
static gboolean discuss_change_to_threads_view(ClutterActor *element){
	const char* json_path = clutter_actor_get_name(element);
	
	cem_add_to_log("discuss_change_to_threads_view", J_LOG_DEBUG);

	clear_discussions(static_discussionthreads);
	static_discussionthreads = 0;
	community_utilities_clear_container_recursively(list);
	
	//currentThread = malloc((strlen(json_path)+1)*sizeof(gchar));
	//strcpy(currentThread, json_path);	
	currentThread = g_strdup_printf("%s",json_path);
	
	ClutterActor* mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);
	
	start_discussionthreads(); 

	return TRUE;
}

/**
 * Changes the view from thread view to discussionthread view
 *
 * JSON callback
**/
gboolean discuss_change_to_discussion_view(TangleButton* tangle_button, gpointer data) {
	cem_add_to_log("changing view to discussion view", J_LOG_DEBUG);

	community_utilities_clear_container_recursively(threadslist);
	clear_discussions(threads);
	//threads = 0;
	
	/*if(currentThread != NULL){
		free(currentThread);
		currentThread = NULL;
	}*/
	g_free(currentThread);
	start_discuss(); 

	return TRUE;
}

/**
 * Scrolls the list to correct position automatically
**/
static void autoscroll_to_correct_pos(ClutterActor *list_){
	gint slot;
	gfloat not_used, offset;
	
	ClutterAction *action = tangle_actor_get_action_by_type(list_, TANGLE_TYPE_SCROLL_ACTION);
	
	tangle_scroll_action_get_offsets(TANGLE_SCROLL_ACTION(action), &not_used, &offset);
	slot = (gint)offset % (gint)item_height < item_height/2  	? (gint)(offset) / item_height  
							  		: (gint)(offset) / item_height + 1;

	tangle_object_animate(G_OBJECT(action), CLUTTER_EASE_IN_OUT_QUAD, 300, "offset-y", 
				(gfloat)(slot*item_height), NULL);
}


/***************************
Server functions
**************************/

/**
 * Downloads discussionthread json files from server
 *
 * In error cases returns NULL.
 * If success, returns filepath to the just downloaded file
**/
static gchar* getAllDiscussionThreads(){
	//TODO: get alldiscussionthreads.json from server and check that the file was received
	if(!community_utilities_load_file_from_server(FORUM_TOPICS_FILE)){
		gchar* msg = g_strdup_printf("Couldn't download topics ('%s') from server",FORUM_TOPICS_FILE);
		cem_add_to_log(msg, J_LOG_ERROR);
		g_free(msg);
		return NULL;
	}
	//Now file is on local disk and we can use it.

	//Only for testing! Use file from installation directory
	char* all_discussionthreads_filepath = g_strdup_printf("%s/%s",DATA_DIR,FORUM_TOPICS_FILE);
	//TODO: use this folder: configure_get_temp_directory());

	return all_discussionthreads_filepath;
}

/**
 * Downloads all json files needed by threads from server
**/
static gboolean load_threads_from_server(gchar* thread){
	gboolean returnvalue = FALSE;

	//TODO: download json file from server and check that file was received
	if(!community_utilities_load_file_from_server(thread)){
		gchar* msg = g_strdup_printf("Load threads from server error: Couldn't download %s from server!", thread);
		cem_add_to_log(msg, J_LOG_ERROR);
		g_free(msg);
		return FALSE;
	}	
	int type = 1; //thread
	GList* l = NULL; //Iterator
	int numberOfFound = 0;
	
	if(parse_discussions_from_file(thread, type)){
		returnvalue = TRUE;
		numberOfFound = g_list_length(threads);
	}else{
		returnvalue = FALSE;
	}
		
	//Download user profiles
	if(returnvalue == TRUE){
		//TODO: download all user json files and check that files were received
		
		if(numberOfFound > 0){
	
			for (l = threads; l; l = l->next){
				discussion_type* temp = threads->data;
					
				//TODO: build user profile filename!
				gchar* user_profile = g_strdup_printf("user_%d.json", temp->user_id);
				
				if(!community_utilities_load_file_from_server(user_profile)){
					cem_add_to_log("Load threads from server error: Couldn't download user profiles from server!", J_LOG_ERROR);
					returnvalue = FALSE;
				}
				g_free(user_profile);
				
			}//for
		}
	}	
	return returnvalue;
}


/**
 * Sends a thread to server
**/
static gboolean sendJammoThread(gchar* thread_path, gchar* messages_path) {
	gboolean returnvalue = TRUE;
		
	//TODO: Send messages.json and threads.json to server 
	
	//TODO: Check that files where successfully sended
		
	return returnvalue;
}


/***************************
JSON Parsers & Builders
**************************/

/**
 * This parser can read
 *    list of topics  (type=0)
 *    list of threads (type=1)
**/
static gboolean parse_discussions_from_file(gchar* filename, int type) {
	int j;
	JsonParser *parser;
	JsonNode *root;
	JsonObject *object;
	JsonNode *node;
	discussion_type *discussion = 0;
	
	//Init JSON Parser
	parser = json_parser_new();
	g_assert (JSON_IS_PARSER(parser));
	GError *error = NULL;
	
	//Load file
	if (!json_parser_load_from_file (parser, filename, &error)){
		g_print ("Error: %s\n", error->message);
		g_error_free (error);
		g_object_unref (parser);
		return FALSE;
	}

	if(json_parser_get_root(parser) != NULL){
		root = json_parser_get_root (parser);
	}else{
		return FALSE;
	}
	if(JSON_NODE_TYPE(root) != JSON_NODE_OBJECT)
		return FALSE;

	object = json_node_get_object (root);
	if(object == NULL)
		return FALSE;
	
	//Fill array
	JsonArray* discussion_array;
	guint length = 0;
	node = json_object_get_member (object, "discussions");
	
	if (node != NULL && JSON_NODE_TYPE (node) == JSON_NODE_ARRAY){
		discussion_array =  json_node_get_array(node);
		length = json_array_get_length(discussion_array);
		gchar* msg = g_strdup_printf("Found %d %s", length, type==0?"topics":"threads");
		cem_add_to_log(msg, J_LOG_DEBUG);
		g_free(msg);
	}else {
		gchar* msg = g_strdup_printf("No %s found",type==0?"topics":"threads");
		cem_add_to_log(msg, J_LOG_DEBUG);
		g_free(msg);
		return TRUE;
	}
	
	//Check properties
	for (j = 0; j < length; j++) {

		JsonNode* discussion_node;
		discussion_node = json_array_get_element(discussion_array, j);

		if (discussion_node != NULL && (JSON_NODE_TYPE(discussion_node) == JSON_NODE_OBJECT)){
			
			JsonObject* sub_object = json_node_get_object(discussion_node);
			JsonNode *sub_node;
			
			const gchar* id = "";
			gint  thread_index = 0;
			guint32 user_id = 0;
			const gchar* title = "";
			const gchar* date = "";
			gchar* json = g_strdup_printf(" ");

			gchar* msg = g_strdup_printf("%s found", type==0?"Topic":"Thread");
			cem_add_to_log(msg, J_LOG_DEBUG);
			g_free(msg);

			sub_node = json_object_get_member(sub_object, "id"); 
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				id = json_node_get_string(sub_node);
				printf("id: '%s'\n", id);
			}	
			sub_node = json_object_get_member(sub_object, "userid"); 

			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				user_id = (guint32)atoi(json_node_get_string(sub_node));			
				printf("userid: '%d'\n", user_id);
			}
			sub_node = json_object_get_member(sub_object, "index"); 

			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				thread_index = (gint)atoi(json_node_get_string(sub_node));			
				printf("index: '%d'\n", thread_index);
			}
			sub_node = json_object_get_member(sub_object, "title");
			
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				title = json_node_get_string(sub_node);
				printf("title: '%s'\n", title);
			}
			sub_node = json_object_get_member(sub_object, "json"); 
			
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				const gchar* temp_string;
				temp_string = json_node_get_string(sub_node);
				json = g_strdup_printf("%s/%s", configure_get_temp_directory(), temp_string);

				if (type==0) { //In case of list of TOPICs, we want that file exists
					FILE* file;
					if ((file = fopen(json, "a")) == NULL) {
						cem_add_to_log("Error happened while trying to touch file", J_LOG_ERROR);
						return FALSE;
					} else {
						fclose(file);
					}
				}

				printf("json: '%s'\n", json);
			}
			sub_node = json_object_get_member(sub_object, "date");
			
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				date = json_node_get_string(sub_node);
				printf("date: '%s'\n", date);
			}

			//Save information into struct
			discussion = malloc(sizeof(discussion_type));

			strcpy(discussion->title, title);
			discussion->user_id = user_id;	
			discussion->index = thread_index;	
			strcpy(discussion->date, date);
			
			discussion->json = malloc((strlen(json)+1)*sizeof(gchar));
			strcpy(discussion->json, json);	
			discussion->id = malloc((strlen(id)+1)*sizeof(gchar));
			strcpy(discussion->id, id);
			
			g_free(json);
		} //Discussion is ready
		
		//Save struct into global array

		if(type == 0){ //Topics
			static_discussionthreads = g_list_append(static_discussionthreads, (gpointer)discussion);
		}else if(type == 1){ //threads
			threads = g_list_append(threads, (gpointer)discussion);
		}

	} //Next discussion

	g_object_unref(parser);
	
	return TRUE;
}

/**
 * Builds json files needed to send a new thread to server
**/
static gboolean discuss_send_thread(gint index_, guint32 userid, const gchar* sender, const gchar* title, const gchar* msg, const gchar* filepath, const gchar* time_, int msgtype) {
	cem_add_to_log("discuss_send_thread",J_LOG_DEBUG);
	
	//TODO: re-download current thread file because it might have changed meanwhile
	if(!community_utilities_load_file_from_server(currentThread)){
		cem_add_to_log("Couldn't download current thread -file from server for writing!",J_LOG_DEBUG);
		return FALSE;
	}

	FILE* messageFile;
	gchar* messagesFilepath = g_strdup_printf("%s/messages_%s_%d.json", configure_get_temp_directory(), title, userid);
	gchar* messagesFileWithoutPath = g_strdup_printf("messages_%s_%d.json", title, userid);
	gboolean returnvalue = FALSE;
	
	int length	= strlen(currentThread);
	char temp[length];
	strcpy(temp, currentThread);

	/*
	//TODO: Get discussionthread name
	int i = 0;
	int j = 0;
	int type_length = 5;
	int path_length = strlen(configure_get_temp_directory());
	int userid_length = 10;

	char discussionthread_name[length];
	for(i = (path_length + 1 + strlen("threads_")); i < (length - (1 + type_length + userid_length)); i++)
	{
		discussionthread_name[j] = temp[i];
		j++;
	}
	discussionthread_name[j] = 0;

	gchar *threads_file = g_strdup_printf("%s/threads_%s_%d.json", configure_get_temp_directory(), discussionthread_name, userid);
	*/

	gchar *threadid = g_strdup_printf("thread_%s_%d.json", title, userid); 	
	
	//write comments file
	if ((messageFile = fopen(messagesFilepath, "w")) == NULL) {
			cem_add_to_log("Error happened while trying to write messages file", J_LOG_ERROR);
			return FALSE;
	}else{
		fprintf(messageFile, "{\n"); 
		fprintf(messageFile, "\"messages\": [\n"); 	
		fprintf(messageFile, "{\n"); 
		fprintf(messageFile, "\"index\": \"%d\",\n", index_);
		fprintf(messageFile, "\"userid\": \"%d\",\n", userid);
		fprintf(messageFile, "\"sender\": \"%s\",\n", sender);
		fprintf(messageFile, "\"title\": \"%s\",\n", title);
		fprintf(messageFile, "\"message\": \"%s\",\n", msg);
		fprintf(messageFile, "\"filepath\": \"%s\",\n", filepath);
		fprintf(messageFile, "\"type\": \"%d\",\n", msgtype);
		fprintf(messageFile, "\"date\": \"%s\"\n", time_);
		fprintf(messageFile, "}\n"); 
		fprintf(messageFile, "]\n"); 	
		fprintf(messageFile, "}"); 
	}
	fclose(messageFile);

	FILE * pFile;
	gboolean fileIsEmpty = FALSE;
	
	//check whether json file is empty
	if ((pFile = fopen(currentThread, "r")) == NULL) {
		gchar * error_msg = g_strdup_printf("Error happened while trying to read %s", currentThread);
		cem_add_to_log(error_msg, J_LOG_ERROR);
		g_free(error_msg);
		
		return FALSE;	
	}else{
		if(fgetc(pFile) == EOF)
			fileIsEmpty = TRUE;		
	}
	fclose(pFile);

	char * dest_filepath = g_strdup_printf("%s/temp.json", configure_get_temp_directory());

	if(fileIsEmpty){

		//write a new json file
		if ((pFile = fopen(currentThread, "w")) == NULL) {
			gchar * error_msg = g_strdup_printf("Error happened while trying to write %s", currentThread);
			cem_add_to_log(error_msg, J_LOG_ERROR);
			g_free(error_msg);
			
			return FALSE;
		}else{
			fprintf(pFile, "{\n"); 
			fprintf(pFile, "\"discussions\": [\n"); 	
			fprintf(pFile, "{\n"); 
			fprintf(pFile, "\"id\": \"%s\",\n", threadid);
			fprintf(pFile, "\"userid\": \"%d\",\n", userid);
			fprintf(pFile, "\"index\": \"%d\",\n", 0);
			fprintf(pFile, "\"title\": \"%s\",\n", title);
			fprintf(pFile, "\"json\": \"%s\",\n", messagesFileWithoutPath);
			fprintf(pFile, "\"date\": \"%s\"\n", time_);
			fprintf(pFile, "}\n"); 
			fprintf(pFile, "]\n"); 	
			fprintf(pFile, "}"); 
		}
		fclose(pFile);	
	}else{
		
		//update json file
		FILE *destFile;
		int c, position_found = 0;
		
		//open source file
		if((pFile = fopen(currentThread, "r")) == NULL){
			cem_add_to_log("Error happened while trying to open source file", J_LOG_ERROR);
			return FALSE;
		}
		//open destination file
		if((destFile = fopen(dest_filepath, "w")) == NULL){
			cem_add_to_log("Error happened while trying to open destination file", J_LOG_ERROR);
			return FALSE;
		}	
		//copy the file
		fprintf(destFile, "{\n"); 
		fprintf(destFile, "\"discussions\": [");
		
		do{
			c = fgetc(pFile);
			if(ferror(pFile)) {
			cem_add_to_log("Error reading source file", J_LOG_ERROR);
			return FALSE;
			}
			if(c == '[')
				position_found = 1;
			
			if(position_found == 1 && c != '[' && c != ']'){
				if(c != ']') fputc(c, destFile);
				if(ferror(destFile)) {
					cem_add_to_log("Error writing destination file", J_LOG_ERROR);
					return FALSE;
				}
			}
		}while(c != ']');	
	
		fclose(pFile);
		
		fprintf(destFile, ",\n");
		fprintf(destFile, "{\n"); 
		fprintf(destFile, "\"id\": \"%s\",\n", threadid);
		fprintf(destFile, "\"userid\": \"%d\",\n", userid);
		fprintf(destFile, "\"index\": \"%d\",\n", 0);
		fprintf(destFile, "\"title\": \"%s\",\n", title);
		fprintf(destFile, "\"json\": \"%s\",\n", messagesFileWithoutPath);
		fprintf(destFile, "\"date\": \"%s\"\n", time_);
       		fprintf(destFile, "}\n");	      	 	
		fprintf(destFile, "]\n"); 	
		fprintf(destFile, "}"); 	 	
		fclose(destFile);
		
		if(remove(currentThread) != 0){ //We remove the original json
			cem_add_to_log("Error removing original thread json", J_LOG_ERROR);
			return FALSE;
		}			
		rename(dest_filepath, currentThread); //And rename temp file with the name of the original
		
	}//Json file is ready

	gchar * message = g_strdup_printf("Sending new thread '\"%s\"' to server...", title);
	cem_add_to_log(message, J_LOG_DEBUG);
	g_free(message);
	
	if(!sendJammoThread(dest_filepath, messagesFilepath)){
		returnvalue = FALSE;
		cem_add_to_log("Error happened when trying to send thread to server", J_LOG_ERROR);
	}else{
		returnvalue = TRUE;
		cem_add_to_log("Thread was successfully sended", J_LOG_ERROR);
	}
	g_free(dest_filepath);
	g_free(messagesFilepath);
	g_free(messagesFileWithoutPath);
	g_free(threadid);

	return returnvalue;
}

#ifdef not_used_yed
/**
 * Builds json files needed to send discussionthread to server
 *
 * not used yet
**/
static gboolean send_discussthread(gint dthread_index, guint32 userid, gchar* dthread_title, gchar* json_path, gchar* date){
	//NOTE: Only admins should have access to this function!
	
	//TODO: Get FORUM_TOPICS_FILE from the server
	if(!community_utilities_load_file_from_server(FORUM_TOPICS_FILE)){
		gchar* msg = g_strdup_printf("Couldn't download topics ('%s') from server for writing",FORUM_TOPICS_FILE);
		cem_add_to_log(msg, J_LOG_ERROR);
		g_free(msg);
		return FALSE;
	}	
	char *all_discussionthreads_filepath = g_strdup_printf("%s/%s", configure_get_temp_directory(), FORUM_TOPICS_FILE); //Only for testing!

	FILE *jsonFile;
	gchar *jsonFilepath = g_strdup_printf("%s/threads_%s_%d.json", configure_get_temp_directory(), dthread_title, userid);
	gboolean returnvalue = FALSE;
	gchar* dthread_id = g_strdup_printf("thread_%s_%d.json", dthread_title, userid); 	
	
	//write threads file
	if ((jsonFile = fopen(jsonFilepath, "w")) == NULL) {
			cem_add_to_log("Error happened while trying to write threads file", J_LOG_ERROR);
			return FALSE;
	}
	fclose(jsonFile);

	FILE * pFile;
	gboolean fileIsEmpty = FALSE;


	
	//check whether json file is empty
	if ((pFile = fopen(all_discussionthreads_filepath, "r")) == NULL) {
		gchar * error_msg = g_strdup_printf("Error happened while trying to read %s", all_discussionthreads_filepath);
		cem_add_to_log(error_msg, J_LOG_ERROR);
		g_free(error_msg);
		
		return FALSE;	
	}else{
		if(fgetc(pFile) == EOF)
			fileIsEmpty = TRUE;		
	}
	fclose(pFile);

	char * dest_filepath = g_strdup_printf("%s/temp.json", configure_get_temp_directory());

	if(fileIsEmpty){

		//write a new json file
		if ((pFile = fopen(all_discussionthreads_filepath, "w")) == NULL) {
			gchar * error_msg = g_strdup_printf("Error happened while trying to write %s", all_discussionthreads_filepath);
			cem_add_to_log(error_msg, J_LOG_ERROR);
			g_free(error_msg);
			
			return FALSE;
		}else{
			fprintf(pFile, "{\n"); 
			fprintf(pFile, "\"discussions\": [\n"); 	
			fprintf(pFile, "{\n"); 
			fprintf(pFile, "\"id\": \"%s\",\n", dthread_id);
			fprintf(pFile, "\"index\": \"%d\",\n", dthread_index);
			fprintf(pFile, "\"userid\": \"%d\",\n", userid);
			fprintf(pFile, "\"title\": \"%s\",\n", dthread_title);
			fprintf(pFile, "\"json\": \"%s\",\n", jsonFilepath);
			fprintf(pFile, "\"date\": \"%s\"\n", date);
			fprintf(pFile, "}\n"); 
			fprintf(pFile, "]\n"); 	
			fprintf(pFile, "}"); 
		}
		fclose(pFile);	
	}else{
		
		//update json file
		FILE *destFile;
		int c, position_found = 0;
		
		//open source file
		if((pFile = fopen(all_discussionthreads_filepath, "r")) == NULL){
			printf("Error happened while trying to open source file '%s'\n", all_discussionthreads_filepath);
			return FALSE;
		}
		//open destination file
		if((destFile = fopen(dest_filepath, "w")) == NULL){
			printf("Error happened while trying to open destination file '%s'\n", dest_filepath);
			return FALSE;
		}	
		//copy the file
		fprintf(destFile, "{\n"); 
		fprintf(destFile, "\"discussions\": [");
		
		do{
			c = fgetc(pFile);
			if(ferror(pFile)) {
      				printf("Error reading source file\n");
      				return FALSE;
    			}
			if(c == '[')
				position_found = 1;
			
			if(position_found == 1 && c != '[' && c != ']'){
				if(c != ']') fputc(c, destFile);
				if(ferror(destFile)) {
      					printf("Error writing destination file\n");
      					return FALSE;
				}
			}
		}while(c != ']');	
		fclose(pFile);
		
		fprintf(destFile, ",\n");
		fprintf(destFile, "{\n"); 
		fprintf(destFile, "\"id\": \"%s\",\n", dthread_id);
		fprintf(destFile, "\"index\": \"%d\",\n", dthread_index);
		fprintf(destFile, "\"userid\": \"%d\",\n", userid);
		fprintf(destFile, "\"title\": \"%s\",\n", dthread_title);
		fprintf(destFile, "\"json\": \"%s\",\n", jsonFilepath);
		fprintf(destFile, "\"date\": \"%s\"\n", date);
       		fprintf(destFile, "}\n");	      	 	
		fprintf(destFile, "]\n"); 	
		fprintf(destFile, "}"); 	 	
		fclose(destFile);
		
		if(remove(all_discussionthreads_filepath) != 0){ //We remove the original json
			printf("Error removing original thread json\n");
			return FALSE;
		}			
		rename(dest_filepath, all_discussionthreads_filepath); //And rename temp file with the name of the original
		
	}//Json file is ready

	gchar * message = g_strdup_printf("Adding new discussionthread '\"%s\"'...", dthread_title);
	cem_add_to_log(message, J_LOG_DEBUG);
	g_free(message);
	
	if(!sendJammoThread(dest_filepath, jsonFilepath)){
		returnvalue = FALSE;
		printf("Error happened when trying to send discussionthread to server\n");
	}else{
		returnvalue = TRUE;
		printf("Discussionthread was successfully sended\n");
	}
	g_free(dest_filepath);
	g_free(jsonFilepath);
	g_free(dthread_id);

	return returnvalue;

	return TRUE;
}
#endif

/**
Example of FORUM_TOPICS_FILE
 (threads uses same fields, so there are some useless)
Fields:
id: just for internal use, must be unique
index: not used with topics
userid: not visible. creator of topic (Note: it is not meant that normal users create topics)
title: shown on menu
json: without path.  if file doesn't exist, it is created
date: not visible for user.

{
"discussions": [
{
"id": "discussionthread_18039856",
"index": "0",
"userid": "18039856",
"title": "MUSIC MAKING",
"json": "MUSIC MAKING.json",
"date": "01.04.2011 15:55"
}
,
{
"id": "discussionthread_18039852",
"index": "1",
"userid": "18039856",
"title": "HOBBIES",
"json": "HOBBIES.json",
"date": "01.04.2011 15:55"
}
]
}
**/
