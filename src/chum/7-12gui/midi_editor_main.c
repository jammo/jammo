
/*
 * This file is part of JamMo.
 *
 * This is center of midi-editor.
 * It contains piano_roll on left, menu on bottom and grid/note-area on center.
 *
 * It is meant to be called from fullsequencer-view or instrument_gui.
 * It needs JammoMiditrackView and JammoSequencer as parameter.
 * Editor unparents+parents given JammoMiditrackView.
 *
 *
 * On debug purpose midi-editor can be started with filename pointing notelist.
 *   Then ending editor, will terminate program.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */


#include <tangle.h>
#include <glib-object.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <clutter/clutter.h>
#include "midi_editor.h"
#include "../../meam/jammo-meam.h"
#include "../../meam/jammo-editing-track.h"
#include "../../meam/jammo-instrument-track.h"
#include "../../meam/jammo-midi.h"

#include "../jammo.h"
#include "../jammo-chum.h"
#include "../jammo-mentor.h"
#include "../jammo-cursor.h"
#include "../jammo-miditrack-view.h"
#include "midi_helper.h"

#include "midi_piano_roll.h"

#include "sequencer.h"
#include "../../cem/cem.h"
//STATICS
static gfloat height_of_grid_slot = 64.0;  //non-zoomed grid-size (700px/64 = 11 slots.  400px/64= 6 slots)
static gfloat width_of_grid_slot = 64.0;

static ClutterActor* static_piano_roll_in;   //zoomed_in version of piano_roll
static ClutterActor* static_piano_roll_out;  //zoomed_out version of piano_roll

static JammoSequencer* static_sequencer;
static TangleBinding* binding_for_track_and_piano;

//we got these as parameters and are returning them after use
static JammoMiditrackView* static_jammo_miditrack_view; //Editor doesn't make own miditrack-view, but uses existing!
static ClutterActor* parent;
static guint highest_note; //this comes inside static_jammo_miditrack_view
static guint lowest_note;  //this comes inside static_jammo_miditrack_view
static gfloat orig_slot_width, orig_slot_height, orig_x, orig_y;



static void start_midi_editor(void);

//This is for debugging only
void midi_editor_start_with_filename(gchar* note_file){
	//Make some default MiditracView

	static_sequencer = jammo_sequencer_new();
	JammoInstrumentTrack* track;
	track = jammo_instrument_track_new(2); //2=UD
	jammo_instrument_track_set_realtime(track, FALSE);
	jammo_sequencer_add_track(static_sequencer, JAMMO_TRACK(track));

	//limits:
	guint high = 60;
	guint low = 40;
	guint slots = 25;
	// duration_of_slot = 0,25s,
	ClutterActor* track_view = jammo_miditrack_view_new(track, slots, 250000000LL, width_of_grid_slot, height_of_grid_slot,high,low);
	parent=NULL; //This will trigger when we are quitting (we want program to end then)


	GList* events = jammomidi_file_to_glist(note_file);
	jammo_miditrack_view_add_event_list(JAMMO_MIDITRACK_VIEW(track_view),events);

	midi_editor_start_with_miditrack_view(JAMMO_MIDITRACK_VIEW(track_view),static_sequencer);
}



/*
	Zoom track.
	*hide grid?
	*disable editing?
*/
gboolean midieditor_zoom_clicked  (TangleButton *tanglebutton, gpointer user_data)  {

	gfloat width=0;
	gfloat height=0;

	if (tangle_button_get_selected(tanglebutton)) { //Zoom Out;
		printf("zoom out\n");
		//if scrolled very bottom and zoomed out, track is ouside of view
		ClutterAction* scroll_action = tangle_actor_get_action_by_type(jammo_get_actor_by_id("midieditor-container-for-track"),TANGLE_TYPE_SCROLL_ACTION);
		tangle_scroll_action_set_offsets(TANGLE_SCROLL_ACTION(scroll_action), 0.0,0.0);

		width = 700.0;
		height = 400.0;
		jammo_miditrack_view_set_show_grid(static_jammo_miditrack_view,FALSE);
		jammo_miditrack_view_set_editing_enabled(static_jammo_miditrack_view,FALSE);
		jammo_miditrack_view_set_pen_mode(static_jammo_miditrack_view,FALSE);
		jammo_miditrack_view_set_eraser_mode(static_jammo_miditrack_view,FALSE);

		jammo_chum_disable_actor(jammo_get_actor_by_id("button_pencil"));
		jammo_chum_disable_actor(jammo_get_actor_by_id("button_eraser"));
		//Piano-roll
		clutter_actor_hide(static_piano_roll_in);
		clutter_actor_show(static_piano_roll_out);
	}
	else {  //Zoom In:
		printf("zoom in\n");
		guint slots;
		g_object_get(static_jammo_miditrack_view,"n-slots", &slots, NULL);

		width = slots*width_of_grid_slot;
		height = (highest_note-lowest_note+1) *height_of_grid_slot;

		jammo_miditrack_view_set_show_grid(static_jammo_miditrack_view,TRUE);
		jammo_miditrack_view_set_editing_enabled(static_jammo_miditrack_view,TRUE);

		jammo_chum_enable_actor(jammo_get_actor_by_id("button_pencil"));
		jammo_chum_enable_actor(jammo_get_actor_by_id("button_eraser"));

		//Piano-roll
		clutter_actor_hide(static_piano_roll_out);
		clutter_actor_show(static_piano_roll_in);
	}

	//Track
	jammo_miditrack_view_zoom_to_fit(static_jammo_miditrack_view,width, height);

	//Cursor
	ClutterActor* cursor = jammo_get_actor_by_id( "midieditor_cursor");
	if (cursor)
		clutter_actor_set_size(cursor, width, 180.0); //Area where cursor is moving.

	return TRUE;
}

/**
 * JSON callback
 */
gboolean midieditor_playstop_clicked  (TangleButton *tanglebutton, gpointer user_data)  {
	//Always stop sequencer
	jammo_sequencer_stop(JAMMO_SEQUENCER(static_sequencer));

	if (tangle_button_get_selected(tanglebutton)) {  //Play pressed
		jammo_sequencer_play(JAMMO_SEQUENCER(static_sequencer));
	}

	return TRUE;
}



/**
 * This function set/unset pencil_mode.
 * JSON callback
 */
gboolean midieditor_pencil_clicked  (TangleButton *tanglebutton, gpointer user_data)  {
	if (jammo_miditrack_view_get_pen_mode(static_jammo_miditrack_view)) {
		cem_add_to_log("MidiEditor: Pencil toggled OFF",J_LOG_USER_ACTION);
		jammo_miditrack_view_set_editing_enabled(static_jammo_miditrack_view, TRUE);
		jammo_miditrack_view_set_pen_mode(static_jammo_miditrack_view, FALSE);
		jammo_miditrack_view_set_eraser_mode(static_jammo_miditrack_view, FALSE);
	}
	else {
		cem_add_to_log("MidiEditor: Pencil toggled ON",J_LOG_USER_ACTION);
		jammo_miditrack_view_set_editing_enabled(static_jammo_miditrack_view, FALSE);
		jammo_miditrack_view_set_pen_mode(static_jammo_miditrack_view, TRUE);
		jammo_miditrack_view_set_eraser_mode(static_jammo_miditrack_view, FALSE);
	}
 tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("button_eraser")),FALSE);

	return TRUE;
}

/**
 * This function set/unset eraser_mode.
 * JSON callback
 */
gboolean midieditor_eraser_clicked  (TangleButton *tanglebutton, gpointer user_data)  {
	if (jammo_miditrack_view_get_eraser_mode(static_jammo_miditrack_view)) {
		cem_add_to_log("MidiEditor: Eraser toggled OFF",J_LOG_USER_ACTION);
		jammo_miditrack_view_set_editing_enabled(static_jammo_miditrack_view, TRUE);
		jammo_miditrack_view_set_pen_mode(static_jammo_miditrack_view, FALSE);
		jammo_miditrack_view_set_eraser_mode(static_jammo_miditrack_view, FALSE);
	}
	else {
		cem_add_to_log("MidiEditor: Eraser toggled ON",J_LOG_USER_ACTION);
		jammo_miditrack_view_set_editing_enabled(static_jammo_miditrack_view, FALSE);
		jammo_miditrack_view_set_pen_mode(static_jammo_miditrack_view, FALSE);
		jammo_miditrack_view_set_eraser_mode(static_jammo_miditrack_view, TRUE);
	}
 tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("button_pencil")),FALSE);

	return TRUE;
}

/**
TODO
*/
gboolean midieditor_finder_clicked (TangleButton *tanglebutton, gpointer user_data)  {
printf("finder clicked\n");
	return FALSE;
}

/**
TODO
*/
gboolean midieditor_quantanize_clicked (TangleButton *tanglebutton, gpointer user_data)  {
printf("midieditor-quantanize clicked\n");
	return FALSE;
}


gboolean midieditor_exit_clicked  (TangleButton *tanglebutton, gpointer user_data)  {
	printf("midieditor_exit_clicked\n");
	if (parent==NULL) { //This happens when started debug-mode
		printf("parent is NULL, quitting\n\n");
		exit(0);
	}

	g_object_unref(binding_for_track_and_piano);

	//jammo_sequencer_stop(static_sequencer); //Do not automatically stop playing
	//return miditrack-view to original parent and original attributes
	ClutterActor* actor = CLUTTER_ACTOR(static_jammo_miditrack_view);
	g_object_ref(actor);
	clutter_container_remove_actor(CLUTTER_CONTAINER(clutter_actor_get_parent(actor)), actor);
	clutter_container_add(CLUTTER_CONTAINER(parent),actor,NULL);   //add
	clutter_actor_set_position(actor, orig_x, orig_y);
	jammo_miditrack_view_set_show_grid(static_jammo_miditrack_view,FALSE);  //hide grid
	jammo_miditrack_view_set_editing_enabled(static_jammo_miditrack_view,FALSE); //disable editing
	jammo_chum_enable_actor(jammo_get_actor_by_id("button_pencil"));
	jammo_chum_enable_actor(jammo_get_actor_by_id("button_eraser"));
	guint slots;
	g_object_get(static_jammo_miditrack_view,"n-slots", &slots,NULL);
	jammo_miditrack_view_zoom_to_fit(static_jammo_miditrack_view,slots*orig_slot_width, (highest_note-lowest_note+1)* orig_slot_height);
	jammo_miditrack_view_set_pen_mode(static_jammo_miditrack_view, FALSE); //pen_mode=FALSE
	g_object_unref(actor);


	//hide editor-view:
	ClutterActor* mainview;
	mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);

	//show sequencer
	return_to_fullsequencer_gui();

	//Delete piano_roll (both dynamically created, they will be created again)
	clutter_actor_destroy(static_piano_roll_in);
	clutter_actor_destroy(static_piano_roll_out);

	return TRUE;
}


/*
Load original position and size so they can be returned.
*/
void midi_editor_start_with_miditrack_view(JammoMiditrackView* miditrack_view, JammoSequencer* sequencer) {
	// These are static
	static_jammo_miditrack_view=miditrack_view;
	parent = clutter_actor_get_parent(CLUTTER_ACTOR(static_jammo_miditrack_view));
	static_sequencer=sequencer;

	orig_x = clutter_actor_get_x(CLUTTER_ACTOR(miditrack_view));
	orig_y = clutter_actor_get_y(CLUTTER_ACTOR(miditrack_view));

	g_object_get(static_jammo_miditrack_view, "slot-width", &orig_slot_width, "slot-height", &orig_slot_height ,
              "highest-note", &highest_note, "lowest-note", &lowest_note,
              NULL);

	//Start with zoomed IN
	g_object_set(static_jammo_miditrack_view, "slot-width", width_of_grid_slot, "slot-height", height_of_grid_slot ,
              NULL);

	//Make editor
	start_midi_editor();
}


/**
 * Handle to start midi editor
 */
static void start_midi_editor(void) {
	printf("Starting midi editor\n");

	ClutterActor* mainview;
	mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);
	tangle_actor_hide_animated(TANGLE_ACTOR(jammo_mentor_get_default()));

	ClutterActor* view = jammo_get_actor_by_id("midieditor-view");
	clutter_actor_show(view);

	//CHUM (graphical)
	ClutterActor* container_for_piano_roll;
	ClutterActor* texture_for_cursor;

	ClutterActor* container_for_track =  jammo_get_actor_by_id("midieditor-container-for-track");


	//Miditrack.
	//We have got ready-miditrack, use it.
	ClutterActor* actor = CLUTTER_ACTOR(static_jammo_miditrack_view); //make things easier
	g_object_ref(actor);

	if (clutter_actor_get_parent(actor)) //e.g. In debug mode and some error cases
		clutter_container_remove_actor(CLUTTER_CONTAINER(parent), actor);
	tangle_widget_add(TANGLE_WIDGET(container_for_track),actor,NULL);  //add
	clutter_actor_set_position(actor, 1.0, 1.0); //1.0,1.0 unless leftmost and topmost borders are not visible
	jammo_miditrack_view_set_show_grid(static_jammo_miditrack_view,TRUE);  //show grid
	guint slots;
	g_object_get(static_jammo_miditrack_view,"n-slots", &slots, NULL);
	jammo_miditrack_view_zoom_to_fit(static_jammo_miditrack_view,slots*width_of_grid_slot, (highest_note-lowest_note+1) *height_of_grid_slot);

	g_object_unref(actor);

	//PIANO-ROLL on left
	//We make two different size piano_roll (zoomed-in and zoomed-out)

	//We need container for piano roll because we want change content of piano_roll (e.g. when zooming)
	container_for_piano_roll = jammo_get_actor_by_id("midieditor-container-for-pianokeys");

	static_piano_roll_in = create_piano_roll(height_of_grid_slot,highest_note,lowest_note,FALSE);
	static_piano_roll_out = create_piano_roll(400.0/(highest_note-lowest_note+1),highest_note,lowest_note,FALSE);
	tangle_widget_add(TANGLE_WIDGET(container_for_piano_roll), static_piano_roll_in, NULL);
	tangle_widget_add(TANGLE_WIDGET(container_for_piano_roll), static_piano_roll_out, NULL);
	clutter_actor_hide(static_piano_roll_out); //Hide zoomed-out version


	//Bind piano_roll and grid together.
	ClutterAction* action = tangle_actor_get_action_by_type(jammo_get_actor_by_id("midieditor-container-for-track"),TANGLE_TYPE_SCROLL_ACTION);
	ClutterAction* action_piano = tangle_actor_get_action_by_type(jammo_get_actor_by_id("midieditor-container-for-pianokeys"),TANGLE_TYPE_SCROLL_ACTION);
	binding_for_track_and_piano = tangle_binding_new(G_OBJECT(action_piano), "offset-y", G_OBJECT(action), "offset-y");


	//Cursor
	texture_for_cursor = jammo_get_actor_by_id( "midieditor_cursor_texture");
	if (texture_for_cursor) {
		clutter_actor_set_size(texture_for_cursor, 10.0, height_of_grid_slot *(highest_note-lowest_note+1)); //Texture size
		ClutterActor* cursor = jammo_get_actor_by_id( "midieditor_cursor");
		clutter_actor_set_size(cursor, width_of_grid_slot*slots, 180.0); //Area where cursor is moving.
	}
}

