/**sequencer is part of JamMo.
License: GPLv2, read more from COPYING

This file is for clutter based gui.
*/
#ifndef _SEQUENCER_H_
#define _SEQUENCER_H_
#include "jammo-collaboration-group-composition.h"

void start_fullsequencer_gui(ClutterActor* levelview);

JammoCollaborationGroupComposition* sequencer_get_group_composition();
void sequencer_set_group_composition(JammoCollaborationGroupComposition* pc);

gboolean sequencer_change_to_loop_view(TangleActor *actor, gpointer data);
gboolean sequencer_change_to_sequencer_view(TangleActor *actor, gpointer data);
gboolean sequencer_change_to_bottom_view(TangleActor *actor, gpointer data);
void return_to_fullsequencer_gui();
void fullsequencer_trackview_loaded (TangleActor* trackview, gpointer none);
void sequencer_start_with_file(const gchar* filename);
void sequencer_save_project();
#endif /* _SEQUENCER_H_ */

