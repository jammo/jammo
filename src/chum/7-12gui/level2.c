#include <tangle.h>
#include <glib-object.h>
#include "gamesmenu.h"
#include "startmenu.h"
#include "../jammo.h"

#include "../jammo-game-task.h"
#include "../jammo-game-level.h"
#include "../jammo-game-level-view.h"


#include "../jammo-mentor.h"
#include "../jammo-editing-track-view.h"
#include "../jammo-miditrack-view.h"
#include "../../cem/cem.h"

#include "sequencer.h"


//Functions for whole level
//This triggers very same time than last task-completed
void level2_completed(JammoGameLevel* game_level) {
	g_print("MENTOR: All task on level2 completed -> Level1 completed\n");
}



//Task1: listening four rhythmic loops
void level2_task1_started(JammoGameTask* task) {
	clutter_actor_show(CLUTTER_ACTOR(jammo_mentor_get_default()));
}

void level2_task1_on_act_completed(JammoGameTask* task, const gchar* act_name, guint acts_completed) {
	gint act_left;
	act_left = (gint)jammo_game_task_get_acts_to_complete(task) - (gint)acts_completed;
	g_print("MENTOR: level2: some task done. %d left\n", act_left);
}

void level2_task1_on_act_redone(JammoGameTask* task, const gchar* act_name, guint acts_completed) {
	g_print("MENTOR: Act completed\n");
}

void level2_task1_completed(JammoGameTask* task, const gchar* act_name, guint acts_completed) {
	g_print("MENTOR: Task1 completed\n");
}



