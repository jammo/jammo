#include <tangle.h>
#include <glib-object.h>
#include "gamesmenu.h"
#include "startmenu.h"
#include "../jammo.h"

#include "../jammo-game-task.h"
#include "../jammo-game-level.h"
#include "../jammo-game-level-view.h"


#include "../jammo-mentor.h"
#include "../jammo-editing-track-view.h"
#include "../jammo-miditrack-view.h"
#include "../../cem/cem.h"

#include "sequencer.h"
#include "sequencer_loop.h"
#include "../jammo-chum.h" //jammo_chum_disable_actor and enable

static void demo_for_task1(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer);
static gboolean on_dropping_incompatible_sample(JammoEditingTrackView* track_view, JammoSampleButton* sample_button, gpointer user_data);

//Functions for whole level
//This triggers very same time than last task-completed
void level1_completed(JammoGameLevel* game_level) {
	g_print("MENTOR: All task on level1 completed -> Level1 completed\n");
}

//Used on task1 and task5
static void sample_button_on_wheel_listened(JammoSampleButton* sample_button, JammoGameTask* task) {
	gchar* name = g_strdup_printf("%d",jammo_sample_button_get_loop_id(sample_button));
	jammo_game_task_set_act_completed(task,name, TRUE);
	g_free(name);
}

static void add_listening_act_to_this_task(ClutterActor* sample_button, gpointer task){
	int id=jammo_sample_button_get_loop_id(JAMMO_SAMPLE_BUTTON(sample_button));

	gchar* act_name = g_strdup_printf("%d",id);
	//printf("act_name ='%s' \n",act_name);
	jammo_game_task_add_act(task, act_name);
	g_signal_connect(sample_button,"listened", G_CALLBACK(sample_button_on_wheel_listened),task);
	g_free(act_name);
}

static void remove_listened_handler(ClutterActor* sample_button, gpointer task){
	g_signal_handlers_disconnect_by_func(sample_button, sample_button_on_wheel_listened, task);
}
////////



static void sample_button_dragged_to_track_for_task(TangleDragAction* drag_action, TangleDropAction* drop_action, gpointer task) {
	if (drop_action) {
		JammoSampleButton* sample_button = JAMMO_SAMPLE_BUTTON (clutter_actor_meta_get_actor(CLUTTER_ACTOR_META(drag_action)));
		gchar* name = g_strdup_printf("%d",jammo_sample_button_get_loop_id(sample_button));
		jammo_game_task_set_act_completed(task,name, TRUE); //If task was done already this triggers task_redone
		g_free(name);
	}
	else {
	}
}

static void add_dropping_act_to_this_task(ClutterActor* sample_button, gpointer task){
	int id=jammo_sample_button_get_loop_id(JAMMO_SAMPLE_BUTTON(sample_button));

	gchar* act_name = g_strdup_printf("%d",id);
	jammo_game_task_add_act(task, act_name);
	g_free(act_name);

	ClutterAction* action;
	action = tangle_actor_get_action_by_type(sample_button, CLUTTER_TYPE_DRAG_ACTION);
	g_signal_connect(action,"dropped", G_CALLBACK(sample_button_dragged_to_track_for_task),task);
}

static void remove_dropped_handler_for_this_task(ClutterActor* sample_button, gpointer task){
	ClutterAction* action;
	action = tangle_actor_get_action_by_type(sample_button, CLUTTER_TYPE_DRAG_ACTION);
	g_signal_handlers_disconnect_by_func(action, sample_button_dragged_to_track_for_task, task);
}

///////////////////////////


static void sample_button_dropped(TangleDragAction* drag_action, TangleDropAction* drop_action, gpointer task) {
	//clutter_actor_meta_get_actor(drop_action) == target widget of drag
	// NULL <=> no-target (=dropped area which can't take it)
	if (drop_action==NULL) {
		//Mentor: Try to hit the track
		jammo_mentor_speak(jammo_mentor_get_default(), "t1_3_help.spx");
	}
	sequencer_change_to_loop_view(NULL,NULL);
}

static void add_dropped_listener(ClutterActor* sample_button, gpointer task){
	ClutterAction* action;
	action = tangle_actor_get_action_by_type(sample_button, CLUTTER_TYPE_DRAG_ACTION);
	g_signal_connect(action,"dropped", G_CALLBACK(sample_button_dropped),task);
}


///////////////
//Task specific
///////////////

//Task1: listening four rhythmic loops
void level1_task1_started(JammoGameTask* task) {

	//disable melodical_sample_looper
	jammo_chum_disable_all_actors(sequencer_loop_get_nth_sample_looper(2));

	//disable/hide track2 entirely (it will used on task6)
	ClutterActor* track_view2 = jammo_get_actor_by_id("track-view2");
	jammo_chum_disable_all_actors(track_view2);
	ClutterActor* track2_controls = jammo_get_actor_by_id("controls-for-track2-level1");
	jammo_chum_disable_all_actors(track2_controls);
	g_object_set(track_view2,"disabled-slots-begin",8*4,NULL);

	//assign sample-buttons to task
	ClutterActor* rhytmical_sample_looper  = sequencer_loop_get_nth_sample_looper(1);
	clutter_container_foreach(CLUTTER_CONTAINER(rhytmical_sample_looper), add_listening_act_to_this_task, task);

	//Disable dropping for editing track1
	ClutterActor* track_view = jammo_get_actor_by_id("track-view1");
	g_object_set(track_view,"disabled-slots-begin",8*4,NULL);

	//When dragging ends, screen is restored to loop-view
	clutter_container_foreach(CLUTTER_CONTAINER(sequencer_loop_get_nth_sample_looper(1)), add_dropped_listener, task);
	clutter_container_foreach(CLUTTER_CONTAINER(sequencer_loop_get_nth_sample_looper(2)), add_dropped_listener, task);

	//If sample is dropped to wrong track, mentor helps
	g_signal_connect(track_view, "dropping-incompatible-sample", G_CALLBACK(on_dropping_incompatible_sample), NULL);
	g_signal_connect(track_view2, "dropping-incompatible-sample", G_CALLBACK(on_dropping_incompatible_sample), NULL);

	//mentor: Lets_rock.
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "t1_1_start0.spx t1_1_help.spx",demo_for_task1,NULL);
}

void level1_task1_on_act_completed(JammoGameTask* task, const gchar* act_name, guint acts_completed) {
}

void level1_task1_on_act_redone(JammoGameTask* task, const gchar* task_name, guint tasks_completed) {
	//Do we want this speak or not?
	//Mentor: you have already listened this one.
	//jammo_mentor_speak(jammo_mentor_get_default(), "listen_to_different.spx");
}

static void task1_finalize(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	jammo_game_level_start_next_task(JAMMO_GAME_LEVEL(jammo_get_object_by_id("game-level")));
}

void level1_task1_completed(JammoGameTask* task) {
	clutter_container_foreach(CLUTTER_CONTAINER(sequencer_loop_get_nth_sample_looper(1)), remove_listened_handler, task);
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "great.spx", task1_finalize, NULL);
	jammo_mentor_set_idle_speech(jammo_mentor_get_default(), "");
}

static void to_task1B(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	sequencer_loop_randomize_wheels();
}

static void to_task1(JammoSample* sample, gpointer sample_texture) {
	//Demo continues: mentor shrinks
	clutter_actor_destroy(CLUTTER_ACTOR(sample_texture));
	g_signal_handlers_disconnect_by_func(sample,G_CALLBACK(to_task1), sample_texture);

	tangle_actor_animate(TANGLE_ACTOR(jammo_mentor_get_default()),CLUTTER_EASE_IN_QUAD, 1000,"scale-x", 0.25, "scale-y", 0.25,"x", 0.0, "rotation-angle-y", 0.0, NULL);
	//Your turn to listen four loops
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "t1_1_start.spx",to_task1B,NULL);
	//mentor-idle: you haven't yet listened enough
	jammo_mentor_set_idle_speech(jammo_mentor_get_default(), "t1_1_idle.spx");
}

static void demo_for_task1C(ClutterTimeline* timeline, gpointer none) {
	//Demo continues: play-sound
	ClutterActor* wheel = sequencer_loop_get_nth_sample_looper(1); //1=rhythm-wheel
	gfloat offset;
	ClutterAction* scroll_action = tangle_actor_get_action_by_type(wheel,TANGLE_TYPE_SCROLL_ACTION);
	g_object_get(scroll_action, "offset-y", &offset, NULL);

	int slot = offset/92;  //92 =fixed height of elements in wheels
	ClutterActor* sample_button = CLUTTER_ACTOR(tangle_widget_get_nth_child(TANGLE_WIDGET(wheel),slot)); //currently most upper sample

	gchar* filename;
	filename = tangle_lookup_filename(jammo_sample_button_get_image_filename(JAMMO_SAMPLE_BUTTON(sample_button)));

	ClutterActor* sample_texture;
	sample_texture = tangle_texture_new(filename);
	g_free(filename);
	clutter_container_add(CLUTTER_CONTAINER(clutter_actor_get_parent(CLUTTER_ACTOR(jammo_mentor_get_default()))),
       sample_texture,NULL);
	clutter_actor_set_x(sample_texture,clutter_actor_get_x(wheel));
	clutter_actor_set_y(sample_texture,clutter_actor_get_y(wheel));
	clutter_actor_show(sample_texture);
	clutter_actor_raise_top(sample_texture);
	jammo_clutter_actor_flash(CLUTTER_ACTOR(sample_texture));

	JammoSample* sample = jammo_sample_button_get_sample(JAMMO_SAMPLE_BUTTON(sample_button));

	g_signal_connect(sample, "stopped", G_CALLBACK(to_task1), sample_texture);
	jammo_sample_play(sample);
}

static void demo_for_task1B(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	//Demo continues: mentor flips, moves, grow
	g_object_set(jammo_mentor_get_default(),"x", 0.0, "y",0.0,NULL);
	ClutterAnimation* animation;
	ClutterTimeline* timeline;

	animation = clutter_actor_animate(CLUTTER_ACTOR(mentor), CLUTTER_EASE_IN_QUAD, 3000, "scale-x", 1.0, "scale-y", 1.0,"x", 550.0, "rotation-angle-y", 180.0, NULL);

	timeline = clutter_animation_get_timeline(animation);
	g_signal_connect(timeline, "completed", G_CALLBACK(demo_for_task1C), NULL);
}

static void demo_for_task1(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	sequencer_change_to_loop_view(NULL,NULL);

	//mentor: you can listen loops by touching
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "t1_1_instruction.spx",demo_for_task1B,NULL);
}



//Task1 over


//Task2 - listen backing track 
static void task2_on_sequencer_stopped(JammoSequencer* sequencer, gpointer user_data){
	g_signal_handlers_disconnect_by_func(sequencer, task2_on_sequencer_stopped, NULL);
	JammoGameTask* task = JAMMO_GAME_TASK(jammo_get_object_by_id("level1-task2.json"));
	jammo_game_task_set_act_completed(task,"backing_track_listened", TRUE);
}

static void to_task2(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer data){
	JammoSequencer* sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));
	g_signal_connect(sequencer, "stopped", G_CALLBACK(task2_on_sequencer_stopped),NULL);
	jammo_sequencer_play(sequencer);
}


void level1_task2_started(JammoGameTask* task) {
	sequencer_change_to_sequencer_view(NULL,NULL);

	//mentor: listen backing-track
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "t1_2_instruction.spx", to_task2, NULL);
}

void level1_task2_on_act_completed(JammoGameTask* task, const gchar* act_name, guint acts_completed) {
	//g_print("MENTOR: Act completed\n");
	//jammo_mentor_speak(jammo_mentor_get_default(), "greatB.spx");
}

void level1_task2_finalize(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer data){
	jammo_game_level_start_next_task(JAMMO_GAME_LEVEL(jammo_get_object_by_id("game-level")));
}

void level1_task2_completed(JammoGameTask* task) {
	g_print("MENTOR: Excellent! Now you have accomplished level1-task2. Please, proceed.\n");
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "", level1_task2_finalize, NULL);
}
//Task2 over

//Task3 -drop four rhythm loops to track
static void demo_for_task3E(ClutterTimeline* timeline, gpointer sample_texture) {
	//Demo continues: restore mentor. delete sample_texture
	sequencer_change_to_loop_view(NULL,NULL);
	clutter_actor_destroy(CLUTTER_ACTOR(sample_texture));
	tangle_actor_animate(TANGLE_ACTOR(jammo_mentor_get_default()),CLUTTER_EASE_IN_QUAD, 1000,"scale-x", 0.25, "scale-y", 0.25,"x", 0.0, "y", 0.0, "rotation-angle-y", 0.0, NULL);

	jammo_mentor_speak(jammo_mentor_get_default(), "t1_3_instruction.spx");

	//Enable editing track1
	ClutterActor* track_view = jammo_get_actor_by_id("track-view1");
	g_object_set(track_view,"disabled-slots-begin",0,NULL);
}

static void demo_for_task3D(ClutterTimeline* t, gpointer sample_texture) {

	//Demo continues: change view and move mento and sample
	sequencer_change_to_sequencer_view(NULL,NULL);
	g_object_set(sample_texture,"y",-50.0,NULL);
	g_object_set(jammo_mentor_get_default(),"y",-70.0,NULL);

	ClutterAnimation* animation;
	ClutterTimeline* timeline;
	//This values are not same, because
	animation = clutter_actor_animate(CLUTTER_ACTOR(jammo_mentor_get_default()), CLUTTER_EASE_IN_QUAD, 3000, "x", 700.0, "y", 100.0, NULL);
	clutter_actor_animate(sample_texture, CLUTTER_EASE_IN_QUAD, 2900,"x", 310.0, "y", 120.0, NULL);

	timeline = clutter_animation_get_timeline(animation);
	g_signal_connect(timeline, "completed", G_CALLBACK(demo_for_task3E), sample_texture);
}


static void demo_for_task3C(ClutterTimeline* t, gpointer sample_texture) {
	//Demo continues: move mentor and sample down to the sequencer-view
	clutter_actor_set_x(sample_texture, 246.0);
	ClutterAnimation* animation;
	ClutterTimeline* timeline;
	animation = clutter_actor_animate(CLUTTER_ACTOR(jammo_mentor_get_default()), CLUTTER_EASE_IN_QUAD, 2000, "y", 346.0, NULL);
	clutter_actor_animate(sample_texture, CLUTTER_EASE_IN_QUAD, 1900, "y", 346.0, NULL); //we want this is ending before mentor

	timeline = clutter_animation_get_timeline(animation);
	g_signal_connect(timeline, "completed", G_CALLBACK(demo_for_task3D), sample_texture);
}
static void demo_for_task3B(ClutterTimeline* t, gpointer data) {
	//Demo continues: mentor drag texture out of wheel and moves it to the right
	g_signal_handlers_disconnect_by_func(t, G_CALLBACK(demo_for_task3B),NULL);
	ClutterAnimation* animation;
	ClutterTimeline* timeline;


	ClutterActor* wheel = sequencer_loop_get_nth_sample_looper(1); //1=rhythm-wheel
	gfloat offset;
	ClutterAction* scroll_action = tangle_actor_get_action_by_type(wheel,TANGLE_TYPE_SCROLL_ACTION);
	g_object_get(scroll_action, "offset-y", &offset, NULL);

	int slot = offset/92;  //92 =fixed height of elements in wheels
	ClutterActor* sample_button = CLUTTER_ACTOR(tangle_widget_get_nth_child(TANGLE_WIDGET(wheel),slot)); //currently most upper sample
	gchar* filename;
	filename = tangle_lookup_filename(jammo_sample_button_get_image_filename(JAMMO_SAMPLE_BUTTON(sample_button)));

	ClutterActor* sample_texture;
	sample_texture = tangle_texture_new(filename);
	g_free(filename);
	clutter_container_add(CLUTTER_CONTAINER(clutter_actor_get_parent(CLUTTER_ACTOR(jammo_mentor_get_default()))),
       sample_texture,NULL);
	clutter_actor_set_x(sample_texture,clutter_actor_get_x(wheel));
	clutter_actor_set_y(sample_texture,clutter_actor_get_y(wheel));
	clutter_actor_show(sample_texture);
	clutter_actor_raise_top(sample_texture);
	jammo_clutter_actor_flash(CLUTTER_ACTOR(sample_texture));


	animation = clutter_actor_animate(CLUTTER_ACTOR(jammo_mentor_get_default()), CLUTTER_EASE_IN_QUAD, 2000, "x", 650.0, NULL);
	clutter_actor_animate(sample_texture, CLUTTER_EASE_IN_QUAD, 1900, "x", 246.0, NULL); //we want this is ending before mentor

	timeline = clutter_animation_get_timeline(animation);
	g_signal_connect(timeline, "completed", G_CALLBACK(demo_for_task3C), sample_texture);
}


static void demo_for_task3(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer data){
	//Demo continues: mentor flips, moves, grow
	g_object_set(jammo_mentor_get_default(),"x", 0.0, "y",0.0,NULL);
	ClutterAnimation* animation;
	ClutterTimeline* timeline;

	animation = clutter_actor_animate(CLUTTER_ACTOR(mentor), CLUTTER_EASE_IN_QUAD, 3000, "scale-x", 1.0, "scale-y", 1.0,"x", 550.0, "rotation-angle-y", 180.0, NULL);

	timeline = clutter_animation_get_timeline(animation);
	g_signal_connect(timeline, "completed", G_CALLBACK(demo_for_task3B), NULL);


}


void level1_task3_started(JammoGameTask* task) {
	sequencer_change_to_loop_view(NULL,NULL);
	clutter_container_foreach(CLUTTER_CONTAINER(sequencer_loop_get_nth_sample_looper(1)), add_dropping_act_to_this_task, task);

	//mentor: look how you can move loops to the track
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "t1_3_demo.spx", demo_for_task3, NULL);
}

void level1_task3_on_act_completed(JammoGameTask* task, const gchar* act_name, guint acts_completed) {
	gint act_left;
	act_left = (gint)jammo_game_task_get_acts_to_complete(task) - (gint)acts_completed;
	g_print("Good! Listen %u more loops.\n", act_left);
}

void level1_task3_on_act_redone(JammoGameTask* task, const gchar* task_name, guint tasks_completed) {
	//In this task we want accept same act multiple times.
	//we generate new act-name and add it to the list
	// (using timestamp, so them will not collide)
	char current_time[80];
	cem_get_time(current_time);
	gchar* name = g_strdup_printf("%s_%s",task_name,current_time);

	jammo_game_task_add_act(task, name);
	jammo_game_task_set_act_completed(task,name, TRUE);
	g_free(name);
}

static void level1_task3_finalize(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer data){
	jammo_game_level_start_next_task(JAMMO_GAME_LEVEL(jammo_get_object_by_id("game-level")));
}

void level1_task3_completed(JammoGameTask* task) {
	clutter_container_foreach(CLUTTER_CONTAINER(sequencer_loop_get_nth_sample_looper(1)), remove_dropped_handler_for_this_task, task);
	//mentor: great.
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "great.spx", level1_task3_finalize, NULL);
}


//Task3 over


//Task4 -use play-button
static void task4_on_sequencer_stopped(JammoSequencer* sequencer, gpointer task){
	cem_add_to_log("level1-task4: sequencer stopped",J_LOG_DEBUG);
	g_signal_handlers_disconnect_by_func(sequencer, task4_on_sequencer_stopped, task);
	jammo_game_task_set_act_completed(task,"play_button_used", TRUE);
}

void level1_task4_started(JammoGameTask* task) {
	printf("level1_task4_started\n");
	JammoSequencer* sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));
	g_signal_connect(sequencer, "stopped", G_CALLBACK(task4_on_sequencer_stopped),task);

	sequencer_change_to_sequencer_view(NULL,NULL);
	jammo_mentor_speak(jammo_mentor_get_default(), "t1_4_instruction.spx");
}

static void level1_task4_finalize(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer data){
	cem_add_to_log("level1-task4: finalize",J_LOG_DEBUG);
	jammo_game_level_start_next_task(JAMMO_GAME_LEVEL(jammo_get_object_by_id("game-level")));
}

void level1_task4_completed(JammoGameTask* task) {
	cem_add_to_log("level1-task4: completed",J_LOG_DEBUG);
	//Show (and flash) melodic-track
	ClutterActor* track_view2 = jammo_get_actor_by_id("track-view2");
	jammo_chum_enable_all_actors(track_view2);
	ClutterActor* track2_controls = jammo_get_actor_by_id("controls-for-track2-level1");
	jammo_chum_enable_all_actors(track2_controls);

	//Changing size of elements inside container, affects whole container. Maybe there should be some other effect.
	jammo_clutter_actor_flash(track_view2);
	jammo_clutter_actor_flash(track2_controls);

	//mentor: great, you will get melodic-track for prize
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "t1_4_completed.spx", level1_task4_finalize, NULL);
}

//Task4 over


//Task5 - listen four melodic loops
void level1_task5_started(JammoGameTask* task) {
	cem_add_to_log("level1-task5: started",J_LOG_DEBUG);
	sequencer_change_to_loop_view(NULL,NULL);

	//enable melodical_sample_looper
	jammo_chum_enable_all_actors(sequencer_loop_get_nth_sample_looper(2));

	//assign sample-buttons on melodical_sample_looper to this task
	clutter_container_foreach(CLUTTER_CONTAINER( sequencer_loop_get_nth_sample_looper(2)), add_listening_act_to_this_task, task);

	//mentor-idle: listen more melodic loops
	jammo_mentor_set_idle_speech(jammo_mentor_get_default(), "t1_5_idle.spx");
	//mentor: listen melodic loops
	jammo_mentor_speak(jammo_mentor_get_default(), "t1_5_instruction.spx");
}



void level1_task5_on_act_completed(JammoGameTask* task, const gchar* act_name, guint acts_completed) {

}

void level1_task5_on_act_redone(JammoGameTask* task, const gchar* task_name, guint tasks_completed) {
	//Do we want this speak or not?
	//Mentor: you have already listened this one.
	//jammo_mentor_speak(jammo_mentor_get_default(), "listen_to_different.spx");
}

static void level1_task5_finalize(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer data){
	jammo_game_level_start_next_task(JAMMO_GAME_LEVEL(jammo_get_object_by_id("game-level")));
}


void level1_task5_completed(JammoGameTask* task) {
	clutter_container_foreach(CLUTTER_CONTAINER(sequencer_loop_get_nth_sample_looper(2)), remove_listened_handler, task);

	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "great.spx", level1_task5_finalize, NULL);
	jammo_mentor_set_idle_speech(jammo_mentor_get_default(), "");
}

//Task5 over


//Task6 - listen backing track and thne drop four melodic loops to melodic track
//rhythmic loops do not count
//This should be separated two different tasks. TODO

/* Sample must be dropped correct track*/
static gboolean on_dropping_incompatible_sample(JammoEditingTrackView* track_view, JammoSampleButton* sample_button, gpointer user_data) {
	JammoSampleType type= jammo_sample_button_get_sample_type(sample_button);
	if (type==JAMMO_SAMPLE_MELODICAL){
		//mentor: this is rhythm-track, drop melodic-loop to melodic-track
		jammo_mentor_speak(jammo_mentor_get_default(), "t1_6_helpB.spx");
	}
	else if (type==JAMMO_SAMPLE_RHYTMICAL){
		//mentor: this is  melodic-track, drop rhytmic-loop to rhytmic-track
		jammo_mentor_speak(jammo_mentor_get_default(), "t1_6_helpC.spx");
	}
	else {
		//It is better to place different instruments on their own tracks
		jammo_mentor_speak(jammo_mentor_get_default(), "incompatible_track_type.spx");
	}
	return FALSE;
}




static void task6_on_sequencer_stopped(JammoSequencer* sequencer, gpointer task){
	g_signal_handlers_disconnect_by_func(sequencer, task6_on_sequencer_stopped, task);
		//enable track2
	ClutterActor* track_view2 = jammo_get_actor_by_id("track-view2");
	g_object_set(track_view2,"disabled-slots-begin",0,NULL);

	//And assign acts for this task
	clutter_container_foreach(CLUTTER_CONTAINER(sequencer_loop_get_nth_sample_looper(2)), add_dropping_act_to_this_task, task);

	sequencer_change_to_loop_view(NULL,NULL);
}


void level1_task6_started(JammoGameTask* task) {
	JammoSequencer* sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));
	g_signal_connect(sequencer, "stopped", G_CALLBACK(task6_on_sequencer_stopped),task);

	sequencer_change_to_sequencer_view(NULL,NULL);
	//First listen backing track and then drag four melody loops to the track
	jammo_mentor_speak(jammo_mentor_get_default(), "t1_6_instruction.spx");
}

void level1_task6_on_act_completed(JammoGameTask* task, const gchar* act_name, guint acts_completed) {
	gint act_left;
	act_left = (gint)jammo_game_task_get_acts_to_complete(task) - (gint)acts_completed;
	g_print("MENTOR: Good! Drag %u more loops.\n", act_left);
}


void level1_task6_on_act_redone(JammoGameTask* task, const gchar* task_name, guint tasks_completed) {
	//In this task we want accept same act multiple times.
	//we generate new act-name and add it to the list
	// (using timestamp, so them will not collide)
	char current_time[80];
	cem_get_time(current_time);
	gchar* name = g_strdup_printf("%s_%s",task_name,current_time);

	jammo_game_task_add_act(task, name);
	jammo_game_task_set_act_completed(task,name, TRUE);
	g_free(name);
}

static void level1_task6_finalize(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer data){
	jammo_game_level_start_next_task(JAMMO_GAME_LEVEL(jammo_get_object_by_id("game-level")));
}

void level1_task6_completed(JammoGameTask* task) {
	clutter_container_foreach(CLUTTER_CONTAINER(sequencer_loop_get_nth_sample_looper(2)), remove_dropped_handler_for_this_task, task);

	//Mentor: great
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "great.spx", level1_task6_finalize, NULL);
}

//Task6 over



//Task7 - listen funny loop and remove it.


static void level1_task7_funny_moved(TangleDragAction* drag_action, TangleDropAction* drop_action, gpointer task) {
	if (drop_action) {// funny loop not deleted, but only moved
		//Technically moving slot will create another actor, and that new actor doesn't have signal handler for level1_task7_funny_moved
		//-> we must accet this as well
		printf("not actually removed, but moved to another slot\n");
	}

	jammo_game_task_set_act_completed(task,"remove-funny-loop", TRUE);
}


static void level1_task7_funny_listened(JammoSampleButton* sample_button, JammoGameTask* task) {
	g_signal_handlers_disconnect_by_func(sample_button,G_CALLBACK(level1_task7_funny_listened),task);
	jammo_game_task_set_act_completed(task,"listen-funny-loop", TRUE);

	//TODO: now task is passed, even funny-loop is only moved to another slot
	g_object_set(sample_button,"drag-threshold-x", 30, "drag-threshold-y", 30, NULL);

	ClutterAction* action;
	action = tangle_actor_get_action_by_type(CLUTTER_ACTOR(sample_button), CLUTTER_TYPE_DRAG_ACTION);
	g_signal_connect(action,"dropped", G_CALLBACK(level1_task7_funny_moved),task);

	jammo_mentor_speak(jammo_mentor_get_default(), "t1_7_help.spx");
}

static void level1_task7_demo(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer funny){
	//Demo: flash funny sample_button on the track
	jammo_clutter_actor_flash(CLUTTER_ACTOR(funny));
}

void level1_task7_started(JammoGameTask* task) {
	sequencer_change_to_sequencer_view(NULL,NULL);
	//Find empty slot on track1
	ClutterActor* track_view = jammo_get_actor_by_id("track-view1");

	JammoSampleButton* funny = JAMMO_SAMPLE_BUTTON(jammo_sample_button_new());

	jammo_sample_button_set_image_filename(JAMMO_SAMPLE_BUTTON(funny),"wheel_game/banana_vocal.png");
	jammo_sample_button_set_sample_filename(JAMMO_SAMPLE_BUTTON(funny), "wheel_game/Fx_electric_$t_44_1.ogg");

	//This sample-button must be listened and only after that it can be moved
	g_object_set(funny,"drag-threshold-x", 10000, "drag-threshold-y", 10000, NULL);

	//Find empty space place for funny sample
	//FIXME: this contains lot's of ad-hoc things!!!
	//guint n_slots;
	//g_object_get(track_view,"n-slots",&n_slots,NULL);
	//printf("n_slots: %d \n",n_slots);

	gboolean free_bar[8]={TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE}; //FIXME: use n_bar
	TangleActorIterator actor_iterator;
	ClutterActor* child;
	
	for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(track_view), &actor_iterator); (child = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {

		guint slot = jammo_editing_track_view_get_sample_button_slot(JAMMO_EDITING_TRACK_VIEW(track_view),JAMMO_SAMPLE_BUTTON(child));
		guint bar = slot / 4 ;
		float width = clutter_actor_get_width(child);
		//printf("reserved slots %d, %f\n",slot, width); //width is 75=slot
		free_bar[bar]=FALSE;
		if (width>80) //FIXME: check width of bar
			free_bar[bar+1]=FALSE;
		if (width>160)
			free_bar[bar+2]=FALSE;
		if (width>310)
			free_bar[bar+3]=FALSE;
	}

	int index_counter=-1; //on bars.
	int i;
	for(i=0;i<8;i++) { //FIXME:use n_bars as max
		if (free_bar[i]){
			index_counter=i;
			break;
			}
	}
	if (index_counter==-1){ //means no free space
		//remove sample from beats 0 and 3 (it is possible there are on waltz-sample on beats 0,1,2, and funny is four beat wide)
		jammo_editing_track_view_remove_jammo_sample_button_from_slot(JAMMO_EDITING_TRACK_VIEW(track_view), 0);
		jammo_editing_track_view_remove_jammo_sample_button_from_slot(JAMMO_EDITING_TRACK_VIEW(track_view), 3);
		index_counter=0;
	}

	//Now we have guaranteed empty bar.
	jammo_editing_track_view_add_jammo_sample_button(JAMMO_EDITING_TRACK_VIEW(track_view), funny, index_counter*4);

	g_signal_connect(funny,"listened", G_CALLBACK(level1_task7_funny_listened),task);

	//mentor blocks sight
	float mentor_place=-240.0;
	mentor_place+=50*index_counter*4; //mentor_place= slot_index*width_of_slot * mentor_scale (75 * 1.5 = 50) //TODO: use correct values

	//mentor: oops, I put funny sample to the track, listen it.
	jammo_mentor_speak_and_highlight_with_callback(jammo_mentor_get_default(),
          "t1_7_start.spx",mentor_place,70.0,level1_task7_demo,funny);
}

void level1_task7_on_act_completed(JammoGameTask* task, const gchar* act_name, guint acts_completed) {
}

static void level1_task7_finalize(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer data){
	//TODO: tell user that it is saved.
	sequencer_save_project();


	jammo_game_level_start_next_task(JAMMO_GAME_LEVEL(jammo_get_object_by_id("game-level")));
}

void level1_task7_completed(JammoGameTask* task) {
	//mentor: great your new composition is now ready, it contains rhythm and melody
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "t1_7_completed.spx", level1_task7_finalize, NULL);
}
