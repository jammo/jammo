/**sequencer is part of JamMo.
License: GPLv2, read more from COPYING

This file is for clutter based gui.
*/

#ifndef COMMUNITYMENU_H_
#define COMMUNITYMENU_H_

#include <tangle.h>

gboolean start_communitymenu();
gboolean communitymenu_goto_mysongs(TangleButton* tanglebutton, gpointer none);
gboolean communitymenu_goto_jammosongs(TangleButton* tanglebutton, gpointer none);
gboolean communitymenu_goto_workshops(TangleButton* tanglebutton, gpointer none);
gboolean communitymenu_goto_jammosounds(TangleButton* tanglebutton, gpointer none);
gboolean communitymenu_goto_help(TangleButton* tanglebutton, gpointer none);
gboolean communitymenu_goto_discuss(TangleButton* tanglebutton, gpointer none);
gboolean communitymenu_goto_profile(TangleButton* tanglebutton, gpointer none);


#endif /* GAMESMENU_H_ */
