/*
License: GPLv2, read more from COPYING

This file contains the functionality of discuss menu.
 */

#ifndef DISCUSS_H_
#define DISCUSS_H_

#include <glib.h>
#include <glib-object.h>

typedef struct {
	gchar *id;	
	gint  index;
	guint32 user_id;
	gchar title[50];	
	gchar *json;
	gchar date[17];
}discussion_type;


void start_discussionthreads();
void start_discuss();

#endif 
