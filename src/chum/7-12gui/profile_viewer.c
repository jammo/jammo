#include "profile_viewer.h"
#include "community_utilities.h"
#include "jammosongs.h"
#include <string.h>
#include "avatar_editor.h"
#include "../../cem/cem.h"
#include "avatar.h"
#include "communitymenu.h"

profile_view_params *params;

/**
 * Builds profile viewer
**/
void start_profile_view(profile_view_params *pvp){

	params = pvp;
	//gems_peer_profile *profile = gems_profile_manager_get_profile_of_user(params->user_id);

	cem_add_to_log("Starting profileviewer", J_LOG_DEBUG);
	show_roster(FALSE); //TODO, pass asked profile
	return;
}

/**
 * Ends profile viewer and frees all allocated memory
**/
gboolean end_profile_view(TangleActor* actor, gpointer data){
	cem_add_to_log("Ending profileviewer", J_LOG_DEBUG);
	end_roster(FALSE);
	start_communitymenu(); //TODO: go back previous view, not communitymenu
	return TRUE;
}

