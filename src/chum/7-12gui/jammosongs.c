/*
License: GPLv2, read more from COPYING

This file contains the functionality of JamMo Songs menu.
 */
#include <tangle.h>
#include <glib-object.h>
#include "startmenu.h"
#include <string.h>
#include "community_utilities.h"
#include "jammosongs.h"
#include "single_thread.h"
#include "src/configure.h"
#include "profile_viewer.h"
#include "../../cem/cem.h"
#include "avatar_editor.h"
#include <stdlib.h>

static list_type list = {0, 0};
GList *paramsList = 0;
GList *labelParamsList = 0;
GList *songs = 0;


void start_jammosongs(){
	
	GList* l = NULL; //Iterator
	int numberOfSongs = 0;
	songs = 0;

	cem_add_to_log("Starting JamMoSongs", J_LOG_DEBUG);

	printf("Starting JamMoSongs");
	
	//clutter_actor_hide(jammo_get_actor_by_id("communitymenu-view"));

	ClutterActor* mainview;
	
	mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);
	
	ClutterActor* jammosongs_view = jammo_get_actor_by_id("jammosongs-view");

	if(jammosongs_view){
				
		load_songs_from_server(); //Get songs
		
		numberOfSongs = g_list_length(songs);

		clutter_actor_show(CLUTTER_ACTOR(jammosongs_view));

		if(numberOfSongs > 5)
			list.list = CLUTTER_CONTAINER(jammo_get_actor_by_id("jammosongs-songs-list-scrollable"));	
		else
			list.list = CLUTTER_CONTAINER(jammo_get_actor_by_id("jammosongs-songs-list-unscrollable"));	

		for(l = songs; l; l = l->next){
			add_song_to_list(l->data, /*gems_profile_manager_get_userid(NULL)*/TEST_USERID);	
			//TODO: third parameter must be user id of the composer, now we are using default
		}
	
		if(numberOfSongs <= 6)
			clutter_actor_set_height(CLUTTER_ACTOR(list.list), list.listItemHeight * numberOfSongs);	
		else
			clutter_actor_set_height(CLUTTER_ACTOR(list.list), list.listItemHeight * 6);	

		ClutterAction *listAction = tangle_actor_get_action_by_type(CLUTTER_ACTOR(list.list), TANGLE_TYPE_SCROLL_ACTION);
	
		g_signal_connect_swapped(listAction, "clamp-offset-y", G_CALLBACK(community_utilities_autoscroll_list), &list);

	}else
		cem_add_to_log("can't find 'jammosongs-view' ", J_LOG_ERROR);
}


gboolean add_song_to_list(gpointer data, guint32 user_id){

	ClutterActor *listImage;
	ClutterContainer *listContainer;
	ClutterActor *reviewButton;
	ClutterActor *avatarBackground;
	ClutterActor *composer;
	ClutterActor *songName;
	ClutterActor *label = 0;
	ClutterActor *labelButton = 0;
	ClutterActor *labelBg = 0;
	guint32 avatar_id = 0;
	Avatar avatar = {0,0,0,0};
	profile_view_params *params = malloc(sizeof(profile_view_params));
	show_label_params *labelParams = malloc(sizeof(show_label_params));

	song_type *song = data;

	paramsList = g_list_append(paramsList, params);
	strcpy(params->parent_view, "jammosongs");
	params->user_id = song->user_id;

	gchar* listImage_filename = g_strdup_printf("%s/communitymenu/colorbar4.png", DATA_DIR);

	listImage = tangle_texture_new(listImage_filename); //TODO: image color depends on the song

	listContainer = CLUTTER_CONTAINER(clutter_group_new());

	gchar* reviewButton_filename = g_strdup_printf("%s/communitymenu/community_comment_icon.png", DATA_DIR);
	reviewButton = tangle_button_new_with_background_actor(tangle_texture_new(reviewButton_filename));
	clutter_actor_set_name(reviewButton, song->comments_path);  //Filepath ok?
	
	gchar* avatarBackground_filename = g_strdup_printf("%s/communitymenu/community_avatarbackground_small.png", DATA_DIR);
	avatarBackground = clutter_group_new();	
	clutter_container_add_actor(CLUTTER_CONTAINER(avatarBackground), tangle_texture_new(avatarBackground_filename));
	composer = tangle_button_new_with_background_actor(avatarBackground);	
			
	gchar * filename = song->filename; //Filepath ok?

	songName = tangle_button_new_with_background_actor(clutter_text_new_with_text(TEXT_NORMAL, song->songname));
	clutter_actor_set_name(songName, filename);

	if(listImage == NULL)
		return FALSE;	

	if(list.listItemHeight == 0)
		list.listItemHeight = clutter_actor_get_height(listImage);

	clutter_container_add_actor(listContainer, listImage);
	
	clutter_actor_set_position(songName, 200, list.listItemHeight/5*2);
	
	clutter_actor_set_position(composer, 20, 0);
	
	avatar_id = song->user_id;
	if (avatar_id == 0)
		avatar_id = 260; //default avatar
		
	avatar_unserialize_image(&avatar, avatar_id);

	set_avatar(&avatar, CLUTTER_ACTOR(avatarBackground));
	avatar_shrink(&avatar);

	clutter_container_add_actor(listContainer, composer);
	
	clutter_container_add_actor(listContainer, songName);

	labelParams->path = g_strdup_printf("%s/label_%d_%s.csv", configure_get_jammo_directory(), user_id, song->songname); //<--- folder?
	
	gchar* label_filename = g_strdup_printf("%s/communitymenu/community_cdlabel_small.png", DATA_DIR);
	
	if(community_utilities_file_exist(labelParams->path)){
		
		label =  clutter_group_new();
		
		labelBg = clutter_rectangle_new_with_color(clutter_color_new(255,255,255,255));
		clutter_actor_set_size(labelBg, 50, 50);
		clutter_container_add_actor(CLUTTER_CONTAINER(label), labelBg);
				
		FILE *file = fopen(labelParams->path, "r");
		char buffer[200];
		if(fread(buffer, sizeof(char), 200, file) != 0){
		
			strtok(buffer, ",");
			strtok(NULL, ",");
			char *instrument;
			instrument = strtok(NULL, ",");
		
			ClutterActor *image = tangle_texture_new(instrument);
			clutter_actor_set_size(image, 40, 40);
			clutter_actor_set_position(image, 5, 5);
		
			clutter_container_add_actor(CLUTTER_CONTAINER(label), image);
		}
		fclose(file);

		labelButton = tangle_button_new_with_background_actor(label);
		
		clutter_actor_set_size(labelButton, 50, 50);
		clutter_actor_set_position(labelButton, 500, 15);

		labelParamsList = g_list_append(labelParamsList, labelParams);	
	}else{
		labelButton = tangle_button_new_with_background_actor(tangle_texture_new(label_filename));
		clutter_actor_set_position(labelButton, 500, list.listItemHeight/6);
		clutter_container_add_actor(listContainer, labelButton);
	}

	g_free(label_filename);

	clutter_actor_set_position(CLUTTER_ACTOR(reviewButton), 700, 15);

	clutter_container_add_actor(listContainer, CLUTTER_ACTOR(reviewButton));
	
	g_signal_connect_swapped(reviewButton, "clicked", G_CALLBACK(review_selected_song), CLUTTER_ACTOR(reviewButton)); //ok?


	g_signal_connect(songName, "clicked", G_CALLBACK(song_on_clicked_function), NULL);
	g_object_set(songName, "hold-timeout", 1500, NULL);
	g_signal_connect(songName, "held", G_CALLBACK(song_on_held_function), NULL);

		
	if(label != 0){	
		strcpy(labelParams->parentview, "jammosongs");
		g_signal_connect_swapped(labelButton, "clicked", G_CALLBACK(community_utilities_start_label_viewer), (gpointer)labelParams);

		clutter_container_add_actor(listContainer, CLUTTER_ACTOR(labelButton));
		//clutter_actor_raise(labelButton, colorbar);
	}	
	g_signal_connect_swapped(composer, "clicked", G_CALLBACK(start_profile_view), (gpointer)params); 

	clutter_container_add_actor(list.list, CLUTTER_ACTOR(listContainer));
	
	g_free(listImage_filename);	
	g_free(reviewButton_filename);	
	g_free(avatarBackground_filename);		

	return TRUE;

}


void review_selected_song(ClutterActor *song){
	
	//GList *list = NULL;
	const char* comments_filepath = clutter_actor_get_name(song);

	community_utilities_clear_labelparamslist(labelParamsList);
	labelParamsList = 0;

	community_utilities_clear_paramslist(paramsList);
	paramsList = 0;

	community_utilities_clear_songlist(songs);
	community_utilities_clear_container_recursively(list.list);

	ClutterActor* mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);

	start_thread_view("jammosongs", comments_filepath, 0);
}


gboolean load_songs_from_server() 
{	
	gboolean returnvalue = FALSE;
	
	//TODO: load all_songs.json from server and check that the file was received
	if(!community_utilities_load_file_from_server("all_songs.json")){
		printf("Load songs from server error: Couldn't download all_songs.json from server!\n");
		return FALSE;
	}	
	
	char * all_songs_filepath = g_strdup_printf("%s/all_songs.json", configure_get_temp_directory()); //Only for testing!
	int type = 1; //songs
	GList* l = NULL; //Iterator
	int numberOfFoundSongs = 0;
	
	if(community_utilities_parse_songs_from_file(all_songs_filepath, &songs, type)){
		returnvalue = TRUE;
		numberOfFoundSongs = g_list_length(songs);
	}else{
		returnvalue = FALSE;
	}
	g_free(all_songs_filepath);
	
	//Download label files
	if(returnvalue == TRUE){
		//TODO: download all song labels and check that files were received
			
		if(numberOfFoundSongs > 0){
	
			for (l = songs; l; l = l->next){
				song_type* temp = songs->data;
					
				//TODO: build cover filename!
				if(!community_utilities_load_file_from_server(temp->cover)){
					printf("Load songs from server error: Couldn't download label files from server!\n");
					returnvalue = FALSE;
				}
			}//for
		}		
	}
	
	//Download user profiles
	if(returnvalue == TRUE){
		//TODO: download all user json files and check that files were received
		
		if(numberOfFoundSongs > 0){
	
			for (l = songs; l; l = l->next){
				song_type* temp = songs->data;
					
				//TODO: build user profile filename!
				gchar* user_profile = g_strdup_printf("user_%d.json", temp->user_id);
				
				if(!community_utilities_load_file_from_server(user_profile)){
					printf("Load songs from server error: Couldn't download user profiles from server!\n");
					returnvalue = FALSE;
				}
				g_free(user_profile);
				
			}//for
		}
	}
	return returnvalue;
}


gboolean end_jammosongs(TangleButton *tanglebutton, gpointer none){

	community_utilities_clear_labelparamslist(labelParamsList);
	labelParamsList = 0;

	community_utilities_clear_paramslist(paramsList);
	paramsList = 0;

	community_utilities_clear_songlist(songs);
	community_utilities_clear_container_recursively(list.list);
   
	if(strcmp(clutter_actor_get_name(CLUTTER_ACTOR(tanglebutton)), "gotocommunity") == 0)
		startmenu_goto_communitymenu(tanglebutton, none);
		
	else
	   startmenu_goto_startmenu(tanglebutton, none);

	return TRUE;
}

