/**sequencer.c is part of JamMo.
License: GPLv2, read more from COPYING

This file is for clutter based gui.
This is the sequencer.
 */
#include <tangle.h>
#include <glib-object.h>
#include "communitymenu.h"
#include "startmenu.h"
#include "../jammo.h"
#include "../../cem/cem.h"
#include "mysongs.h"
#include "jammosongs.h"
#include "workshops.h"
#include "jammosounds.h"
#include "discuss.h"
#include "avatar_editor.h"

gboolean start_communitymenu()
{
	cem_add_to_log("Starting communitymenu", J_LOG_DEBUG);

	ClutterActor* mainview;

	mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);


	ClutterActor* communitymenu_view = jammo_get_actor_by_id("communitymenu-view");
	if (communitymenu_view){
		clutter_actor_show (CLUTTER_ACTOR(communitymenu_view));
		
		
		//show available buttons depending on the game level
		tangle_actor_show(TANGLE_ACTOR(jammo_get_actor_by_id("communitymenu_goto_profile")));
		tangle_actor_show(TANGLE_ACTOR(jammo_get_actor_by_id("communitymenu_goto_mysongs")));
		tangle_actor_show(TANGLE_ACTOR(jammo_get_actor_by_id("communitymenu_goto_jammosongs")));		
		tangle_actor_show(TANGLE_ACTOR(jammo_get_actor_by_id("communitymenu_goto_workshops")));
		tangle_actor_show(TANGLE_ACTOR(jammo_get_actor_by_id("communitymenu_goto_jammosounds")));
		tangle_actor_show(TANGLE_ACTOR(jammo_get_actor_by_id("communitymenu_goto_discuss")));
		
		
	}else
		cem_add_to_log("can't find 'communitymenu-view' \n", J_LOG_ERROR);
             
		
	return FALSE;
}

gboolean communitymenu_goto_mysongs(TangleButton* tanglebutton, gpointer none){

	cem_add_to_log("communitymenu_goto_mysongs", J_LOG_DEBUG);
	
	start_mysongs();

	return TRUE;
}

gboolean communitymenu_goto_jammosongs(TangleButton* tanglebutton, gpointer none){

	cem_add_to_log("communitymenu_goto_jammosongs", J_LOG_DEBUG);
	
	start_jammosongs();

	return TRUE;
}

gboolean communitymenu_goto_workshops(TangleButton* tanglebutton, gpointer none){
	
	cem_add_to_log("communitymenu_goto_workshops", J_LOG_DEBUG);
	
	start_workshops(); 

	return TRUE;
}


gboolean communitymenu_goto_jammosounds(TangleButton* tanglebutton, gpointer none){
	
	cem_add_to_log("communitymenu_goto_jammosounds", J_LOG_DEBUG);
	
	start_jammosounds(); 

	return TRUE;
}


gboolean communitymenu_goto_discuss(TangleButton* tanglebutton, gpointer none){
	
	cem_add_to_log("communitymenu_goto_discuss", J_LOG_DEBUG);
	
	start_discuss(); 

	return TRUE;
}

gboolean communitymenu_goto_profile(TangleButton* tanglebutton, gpointer none){
	
	cem_add_to_log("communitymenu_goto_profile", J_LOG_DEBUG);
	
	start_avatar_editor(); 

	return TRUE;
}



