void instrument_gui_start (JammoMiditrackView* miditrack_view, TangleWidget* slider_container);
void instrument_gui_set_transpose(char key);
char instrument_gui_get_transpose();
