/*
License: GPLv2, read more from COPYING

This file contains the functionality of Workshops menu.
 */

#ifndef WORKSHOPS_H_
#define WORKSHOPS_H_

void start_workshops();
gboolean workshop_goto_communitymenu(TangleButton *tanglebutton, gpointer none);
gboolean workshop_goto_startmenu(TangleButton *tanglebutton, gpointer none);

gboolean workshop_mentor_clicked (TangleButton *tanglebutton, gpointer data);
gboolean workshop_add_loop_to_list(char* filename);

#endif 
