/*
 * jammer_view
 *
 * This file is part of JamMo.
 *
 * This is for Jammer.
 * 	
 */
#include <meam/jammo-meam.h>
#include <chum/jammo-chum.h>
#include <meam/jammo-sample.h>
#include <meam/jammo-editing-track.h>
#include <glib-object.h>
#include <ctype.h>
#include <math.h>
#include <stdlib.h>
#include <tangle.h>

#include "../jammo.h"
#include "jammer-accelerometer.h"
#include "jammer-emmafile.h"

#include "jammer_view.h"
#include "../../configure.h"
#include "startmenu.h"

#include "sequencer.h"
//All global vars Jammer uses TODO: remember to sync with g_free after exit (!important).

#define BUFFER_SIZE 6


static int selectedKitNum; //0-> elec, 1->eth1, 2->eth2, 3->popk  etc

static gboolean isJammerPlaying; //do we read accelerometer values? (TRUE/FALSE)

static gboolean isJammerRecording;

static gboolean isJammerSeqPlaying;

static ClutterActor *btnSet3 = NULL;	///roller container

static ClutterActor *btnSet4 = NULL; ///piano container

static double sliderposx; ///keeps track with roller x pos

static int jammerCounter; ///counting how many loops jammer is running

static int motionBuffer[BUFFER_SIZE][3]; //buffer for calculating movement: 0 is the most recent event

static guint loop_id; ///accel read loop
///JammoSample for playing the sounds (need 3 of them)
static JammoSample* jammerSample0; 
static JammoSample* jammerSample1;
static JammoSample* jammerSample2;

typedef struct 
{
	int id;		//numeric id (0,1,2 ... )
	ClutterActor *instrumentImg;	
	gchar avatarfile[25];	//png filename 
	gchar folder[4];	//folder for sounds
	gboolean isMelodic;
} JammerInstruments;

static JammerInstruments instr[8];	//using 8 instruments in this build

static const gchar* melodicSounds[] = {NULL, NULL, NULL}; //assigned notes of melodic instruments

static int melodicSoundsIndex = 0; ///keeps track that melodic sounds are assigned //3 for full 

static int nextmelodicSoundsIndex = 0; ///next free melodic sound

static JammoEditingTrack* jammer_editing_track;
static JammoSequencer* jammer_sequencer;

///end of global vars TODO::free them all in exit 

//////JAMMER

static void playSound(int i){
	if (!instr[selectedKitNum].isMelodic){	
		if (i == 0){	
			jammo_sample_play(jammerSample0);
			if (isJammerRecording){
				jammo_editing_track_add_sample(jammer_editing_track, jammerSample0, jammo_sequencer_get_position(jammer_sequencer));
			}
		}else if (i == 1){
			jammo_sample_play(jammerSample1);
			if (isJammerRecording){
				jammo_editing_track_add_sample(jammer_editing_track, jammerSample1, jammo_sequencer_get_position(jammer_sequencer));
			}
		}else{
			jammo_sample_play(jammerSample2);
			if (isJammerRecording){
				jammo_editing_track_add_sample(jammer_editing_track, jammerSample2, jammo_sequencer_get_position(jammer_sequencer));
			}
		}
	}else{
		if (melodicSoundsIndex == 3){
			if (i == 0){	
				jammo_sample_play(jammerSample0);
				if (isJammerRecording){
					jammo_editing_track_add_sample(jammer_editing_track, jammerSample0, jammo_sequencer_get_position(jammer_sequencer));
				}
			}else if (i == 1){
				jammo_sample_play(jammerSample1);
				if (isJammerRecording){
					jammo_editing_track_add_sample(jammer_editing_track, jammerSample1, jammo_sequencer_get_position(jammer_sequencer));
				}
			}else{
				jammo_sample_play(jammerSample2);
				if (isJammerRecording){
					jammo_editing_track_add_sample(jammer_editing_track, jammerSample2, jammo_sequencer_get_position(jammer_sequencer));
				}
			}	
		}
	}
}

static void setSoundKit(){
	char *file0;
	char *file1;
	char *file2;
	isJammerPlaying = TRUE;
	if (!instr[selectedKitNum].isMelodic){	
		file0 = g_build_filename(JAMMERSOUNDPATH, instr[selectedKitNum].folder, "/0.wav", NULL);
		file1 = g_build_filename(JAMMERSOUNDPATH, instr[selectedKitNum].folder, "/1.wav", NULL);
		file2 = g_build_filename(JAMMERSOUNDPATH, instr[selectedKitNum].folder, "/2.wav", NULL);
		jammerSample0 = jammo_sample_new_from_file_with_sequencer(file0, jammer_sequencer);
		jammerSample1 = jammo_sample_new_from_file_with_sequencer(file1, jammer_sequencer);
		jammerSample2 = jammo_sample_new_from_file_with_sequencer(file2, jammer_sequencer);
	}else{
		if (melodicSoundsIndex == 3){
			file0 = g_strdup_printf("%s/%s/%s_%s.ogg", JAMMERSOUNDPATH,instr[selectedKitNum].folder, instr[selectedKitNum].folder,melodicSounds[0]);
			file1 = g_strdup_printf("%s/%s/%s_%s.ogg", JAMMERSOUNDPATH,instr[selectedKitNum].folder, instr[selectedKitNum].folder,melodicSounds[1]);
			file2 = g_strdup_printf("%s/%s/%s_%s.ogg", JAMMERSOUNDPATH,instr[selectedKitNum].folder, instr[selectedKitNum].folder,melodicSounds[2]);
			jammerSample0 = jammo_sample_new_from_file_with_sequencer(file0, jammer_sequencer);
			jammerSample1 = jammo_sample_new_from_file_with_sequencer(file1, jammer_sequencer);
			jammerSample2 = jammo_sample_new_from_file_with_sequencer(file2, jammer_sequencer);
		}
	}
}



//TRUE for up(-), FALSE for down (+)
static void moveSlider(gboolean direction){
	///move the slider	
	if (direction){	
		sliderposx = sliderposx - 120.0;
	}else{
		sliderposx = sliderposx + 120.0;
	}
	clutter_actor_animate(CLUTTER_ACTOR(btnSet3), CLUTTER_LINEAR, 500, 
				"x", sliderposx,
				NULL);
}


static void setSoundKitNum(int s){
	isJammerPlaying = FALSE;	
	gboolean direction = FALSE;
	if (selectedKitNum > s){
		direction = FALSE;
	}else{
		direction = TRUE;
	}
	selectedKitNum = s;
	setSoundKit();
	moveSlider(direction);
	if (instr[s].isMelodic && !CLUTTER_ACTOR_IS_VISIBLE(btnSet4) ){
		clutter_actor_show_all(btnSet4);
	}
	if (!instr[s].isMelodic && CLUTTER_ACTOR_IS_VISIBLE(btnSet4) ){
		clutter_actor_hide_all(btnSet4);
	}
	isJammerPlaying = TRUE;
}

static int sort1(const void* x1_parameter, const void* y1_parameter) {
    return (*(int*)x1_parameter - *(int*)y1_parameter);
}

static int distance(int pointA[3],int pointB[3] ){
    double temp;
    temp = sqrt(pow((pointA[0]-pointB[0]),2)+pow((pointA[1]-pointB[1]),2)+pow((pointA[2]-pointB[2]),2));
    return (int)temp;
}


static int metro(int pointA[BUFFER_SIZE] ){
    int i;
    double temp=0;
    for (i=0;i<BUFFER_SIZE;i++){
        temp =+ pow(pointA[i],2);
    }
    int r = sqrt(temp);
    return r;
}

static int motion_detect(int e, int firsttime){
	int sample_size = 10;
	int k_nearest = 5; //k nearest

///Training data
    //MovemementA training data (face down)
    //int fd[10][BUFFER_SIZE][3];
    //par a: set number
    //par b: movement number
    //par c: position
    int fd[sample_size][sample_size][3];
	fd[0][0][0] = 162 ; fd[0][0][1] = -612 ; fd[0][0][2] =  -774 ;
	fd[0][1][0] = 158 ; fd[0][1][1] = -612 ; fd[0][1][2] =  -775 ;
	fd[0][2][0] = 163 ; fd[0][2][1] = -617 ; fd[0][2][2] =  -773 ;
	fd[0][3][0] = 198 ; fd[0][3][1] = -603 ; fd[0][3][2] =  -776 ;
	fd[0][4][0] = 226 ; fd[0][4][1] = -580 ; fd[0][4][2] =  -770 ;
	fd[0][5][0] = 277 ; fd[0][5][1] = -549 ; fd[0][5][2] =  -756 ;
	fd[0][6][0] = 348 ; fd[0][6][1] = -499 ; fd[0][6][2] =  -730 ;
	fd[0][7][0] = 397 ; fd[0][7][1] = -495 ; fd[0][7][2] =  -687 ;
	fd[0][8][0] = 468 ; fd[0][8][1] = -490 ; fd[0][8][2] =  -663 ;
	fd[0][9][0] = 558 ; fd[0][9][1] = -520 ; fd[0][9][2] =  -611 ;
	fd[1][0][0] = -43 ; fd[1][0][1] = -27 ; fd[1][0][2] =  819 ;
	fd[1][1][0] = -92 ; fd[1][1][1] = 87 ; fd[1][1][2] =  623 ;
	fd[1][2][0] = -156 ; fd[1][2][1] = 306 ; fd[1][2][2] =  533 ;
	fd[1][3][0] = -160 ; fd[1][3][1] = 504 ; fd[1][3][2] =  603 ;
	fd[1][4][0] = -169 ; fd[1][4][1] = 595 ; fd[1][4][2] =  569 ;
	fd[1][5][0] = -144 ; fd[1][5][1] = 675 ; fd[1][5][2] =  494 ;
	fd[1][6][0] = -117 ; fd[1][6][1] = 738 ; fd[1][6][2] =  430 ;
	fd[1][7][0] = -83 ; fd[1][7][1] = 756 ; fd[1][7][2] =  394 ;
	fd[1][8][0] = -11 ; fd[1][8][1] = 736 ; fd[1][8][2] =  358 ;
	fd[1][9][0] = 67 ; fd[1][9][1] = 687 ; fd[1][9][2] =  309 ;
	fd[2][0][0] = -88 ; fd[2][0][1] = -68 ; fd[2][0][2] =  925 ;
	fd[2][1][0] = -91 ; fd[2][1][1] = -82 ; fd[2][1][2] =  861 ;
	fd[2][2][0] = -89 ; fd[2][2][1] = -34 ; fd[2][2][2] =  758 ;
	fd[2][3][0] = -108 ; fd[2][3][1] = 136 ; fd[2][3][2] =  655 ;
	fd[2][4][0] = -136 ; fd[2][4][1] = 351 ; fd[2][4][2] =  621 ;
	fd[2][5][0] = -144 ; fd[2][5][1] = 544 ; fd[2][5][2] =  609 ;
	fd[2][6][0] = -158 ; fd[2][6][1] = 612 ; fd[2][6][2] =  569 ;
	fd[2][7][0] = -169 ; fd[2][7][1] = 630 ; fd[2][7][2] =  490 ;
	fd[2][8][0] = -159 ; fd[2][8][1] = 648 ; fd[2][8][2] =  387 ;
	fd[2][9][0] = -155 ; fd[2][9][1] = 676 ; fd[2][9][2] =  299 ;
	fd[3][0][0] = 55 ; fd[3][0][1] = -101 ; fd[3][0][2] =  834 ;
	fd[3][1][0] = 29 ; fd[3][1][1] = 18 ; fd[3][1][2] =  657 ;
	fd[3][2][0] = 9 ; fd[3][2][1] = 244 ; fd[3][2][2] =  557 ;
	fd[3][3][0] = 26 ; fd[3][3][1] = 448 ; fd[3][3][2] =  603 ;
	fd[3][4][0] = 27 ; fd[3][4][1] = 541 ; fd[3][4][2] =  549 ;
	fd[3][5][0] = 15 ; fd[3][5][1] = 589 ; fd[3][5][2] =  481 ;
	fd[3][6][0] = -6 ; fd[3][6][1] = 632 ; fd[3][6][2] =  402 ;
	fd[3][7][0] = -12 ; fd[3][7][1] = 673 ; fd[3][7][2] =  320 ;
	fd[3][8][0] = -7 ; fd[3][8][1] = 713 ; fd[3][8][2] =  266 ;
	fd[3][9][0] = -2 ; fd[3][9][1] = 769 ; fd[3][9][2] =  221 ;
	fd[4][0][0] = -28 ; fd[4][0][1] = -62 ; fd[4][0][2] =  887 ;
	fd[4][1][0] = -52 ; fd[4][1][1] = 14 ; fd[4][1][2] =  771 ;
	fd[4][2][0] = -93 ; fd[4][2][1] = 241 ; fd[4][2][2] =  618 ;
	fd[4][3][0] = -117 ; fd[4][3][1] = 445 ; fd[4][3][2] =  608 ;
	fd[4][4][0] = -126 ; fd[4][4][1] = 557 ; fd[4][4][2] =  568 ;
	fd[4][5][0] = -145 ; fd[4][5][1] = 589 ; fd[4][5][2] =  495 ;
	fd[4][6][0] = -159 ; fd[4][6][1] = 612 ; fd[4][6][2] =  391 ;
	fd[4][7][0] = -173 ; fd[4][7][1] = 646 ; fd[4][7][2] =  299 ;
	fd[4][8][0] = -175 ; fd[4][8][1] = 680 ; fd[4][8][2] =  227 ;
	fd[4][9][0] = -164 ; fd[4][9][1] = 721 ; fd[4][9][2] =  161 ;
	fd[5][0][0] = 21 ; fd[5][0][1] = -121 ; fd[5][0][2] =  958 ;
	fd[5][1][0] = 9 ; fd[5][1][1] = -134 ; fd[5][1][2] =  946 ;
	fd[5][2][0] = -2 ; fd[5][2][1] = -144 ; fd[5][2][2] =  941 ;
	fd[5][3][0] = 1 ; fd[5][3][1] = -153 ; fd[5][3][2] =  899 ;
	fd[5][4][0] = -9 ; fd[5][4][1] = -125 ; fd[5][4][2] =  792 ;
	fd[5][5][0] = -45 ; fd[5][5][1] = 49 ; fd[5][5][2] =  639 ;
	fd[5][6][0] = -65 ; fd[5][6][1] = 272 ; fd[5][6][2] =  564 ;
	fd[5][7][0] = -81 ; fd[5][7][1] = 473 ; fd[5][7][2] =  563 ;
	fd[5][8][0] = -99 ; fd[5][8][1] = 557 ; fd[5][8][2] =  510 ;
	fd[5][9][0] = -117 ; fd[5][9][1] = 600 ; fd[5][9][2] =  435 ;
	fd[6][0][0] = -108 ; fd[6][0][1] = -72 ; fd[6][0][2] =  1008 ;
	fd[6][1][0] = -106 ; fd[6][1][1] = -73 ; fd[6][1][2] =  1002 ;
	fd[6][2][0] = -99 ; fd[6][2][1] = -72 ; fd[6][2][2] =  1002 ;
	fd[6][3][0] = -94 ; fd[6][3][1] = -73 ; fd[6][3][2] =  1002 ;
	fd[6][4][0] = -88 ; fd[6][4][1] = -78 ; fd[6][4][2] =  1000 ;
	fd[6][5][0] = -81 ; fd[6][5][1] = -79 ; fd[6][5][2] =  993 ;
	fd[6][6][0] = -80 ; fd[6][6][1] = -90 ; fd[6][6][2] =  945 ;
	fd[6][7][0] = -77 ; fd[6][7][1] = -57 ; fd[6][7][2] =  809 ;
	fd[6][8][0] = -74 ; fd[6][8][1] = 117 ; fd[6][8][2] =  623 ;
	fd[6][9][0] = -34 ; fd[6][9][1] = 333 ; fd[6][9][2] =  535 ;
	fd[7][0][0] = -106 ; fd[7][0][1] = 75 ; fd[7][0][2] =  648 ;
	fd[7][1][0] = -172 ; fd[7][1][1] = 296 ; fd[7][1][2] =  603 ;
	fd[7][2][0] = -174 ; fd[7][2][1] = 482 ; fd[7][2][2] =  575 ;
	fd[7][3][0] = -171 ; fd[7][3][1] = 550 ; fd[7][3][2] =  526 ;
	fd[7][4][0] = -179 ; fd[7][4][1] = 594 ; fd[7][4][2] =  451 ;
	fd[7][5][0] = -184 ; fd[7][5][1] = 628 ; fd[7][5][2] =  353 ;
	fd[7][6][0] = -180 ; fd[7][6][1] = 658 ; fd[7][6][2] =  272 ;
	fd[7][7][0] = -171 ; fd[7][7][1] = 702 ; fd[7][7][2] =  208 ;
	fd[7][8][0] = -146 ; fd[7][8][1] = 741 ; fd[7][8][2] =  160 ;
	fd[7][9][0] = -100 ; fd[7][9][1] = 805 ; fd[7][9][2] =  113 ;
	fd[8][0][0] = -28 ; fd[8][0][1] = 63 ; fd[8][0][2] =  659 ;
	fd[8][1][0] = -66 ; fd[8][1][1] = 285 ; fd[8][1][2] =  593 ;
	fd[8][2][0] = -95 ; fd[8][2][1] = 470 ; fd[8][2][2] =  576 ;
	fd[8][3][0] = -98 ; fd[8][3][1] = 577 ; fd[8][3][2] =  523 ;
	fd[8][4][0] = -113 ; fd[8][4][1] = 638 ; fd[8][4][2] =  476 ;
	fd[8][5][0] = -108 ; fd[8][5][1] = 658 ; fd[8][5][2] =  392 ;
	fd[8][6][0] = -100 ; fd[8][6][1] = 682 ; fd[8][6][2] =  322 ;
	fd[8][7][0] = -79 ; fd[8][7][1] = 720 ; fd[8][7][2] =  255 ;
	fd[8][8][0] = -74 ; fd[8][8][1] = 779 ; fd[8][8][2] =  233 ;
	fd[8][9][0] = -36 ; fd[8][9][1] = 872 ; fd[8][9][2] =  189 ;
	fd[9][0][0] = 108 ; fd[9][0][1] = 126 ; fd[9][0][2] =  1008 ;
	fd[9][1][0] = 104 ; fd[9][1][1] = 120 ; fd[9][1][2] =  1009 ;
	fd[9][2][0] = 99 ; fd[9][2][1] = 115 ; fd[9][2][2] =  1030 ;
	fd[9][3][0] = 83 ; fd[9][3][1] = 94 ; fd[9][3][2] =  1033 ;
	fd[9][4][0] = 69 ; fd[9][4][1] = 66 ; fd[9][4][2] =  1010 ;
	fd[9][5][0] = 53 ; fd[9][5][1] = 43 ; fd[9][5][2] =  995 ;
	fd[9][6][0] = 40 ; fd[9][6][1] = 20 ; fd[9][6][2] =  981 ;
	fd[9][7][0] = 28 ; fd[9][7][1] = -5 ; fd[9][7][2] =  967 ;
	fd[9][8][0] = 27 ; fd[9][8][1] = -31 ; fd[9][8][2] =  931 ;
	fd[9][9][0] = 24 ; fd[9][9][1] = -36 ; fd[9][9][2] =  859 ;
    ///end of MovementA training data
    ///MovemementB training data (side up)
    int su[sample_size][sample_size][3];
	su[0][0][0] = -1008 ; su[0][0][1] = 144 ; su[0][0][2] =  72 ;
	su[0][1][0] = -1006 ; su[0][1][1] = 144 ; su[0][1][2] =  63 ;
	su[0][2][0] = -1009 ; su[0][2][1] = 144 ; su[0][2][2] =  69 ;
	su[0][3][0] = -1005 ; su[0][3][1] = 142 ; su[0][3][2] =  58 ;
	su[0][4][0] = -996 ; su[0][4][1] = 145 ; su[0][4][2] =  50 ;
	su[0][5][0] = -970 ; su[0][5][1] = 152 ; su[0][5][2] =  50 ;
	su[0][6][0] = -910 ; su[0][6][1] = 144 ; su[0][6][2] =  63 ;
	su[0][7][0] = -813 ; su[0][7][1] = 104 ; su[0][7][2] =  65 ;
	su[0][8][0] = -692 ; su[0][8][1] = 0 ; su[0][8][2] =  58 ;
	su[0][9][0] = -552 ; su[0][9][1] = -190 ; su[0][9][2] =  45 ;
	su[1][0][0] = -1008 ; su[1][0][1] = 72 ; su[1][0][2] =  -126 ;
	su[1][1][0] = -1002 ; su[1][1][1] = 77 ; su[1][1][2] =  -131 ;
	su[1][2][0] = -1015 ; su[1][2][1] = 78 ; su[1][2][2] =  -125 ;
	su[1][3][0] = -1017 ; su[1][3][1] = 77 ; su[1][3][2] =  -123 ;
	su[1][4][0] = -1008 ; su[1][4][1] = 80 ; su[1][4][2] =  -119 ;
	su[1][5][0] = -1004 ; su[1][5][1] = 84 ; su[1][5][2] =  -107 ;
	su[1][6][0] = -995 ; su[1][6][1] = 97 ; su[1][6][2] =  -92 ;
	su[1][7][0] = -969 ; su[1][7][1] = 114 ; su[1][7][2] =  -79 ;
	su[1][8][0] = -917 ; su[1][8][1] = 127 ; su[1][8][2] =  -63 ;
	su[1][9][0] = -818 ; su[1][9][1] = 108 ; su[1][9][2] =  -44 ;
	su[2][0][0] = -1008 ; su[2][0][1] = 81 ; su[2][0][2] =  16 ;
	su[2][1][0] = -1002 ; su[2][1][1] = 98 ; su[2][1][2] =  9 ;
	su[2][2][0] = -990 ; su[2][2][1] = 115 ; su[2][2][2] =  9 ;
	su[2][3][0] = -959 ; su[2][3][1] = 125 ; su[2][3][2] =  11 ;
	su[2][4][0] = -873 ; su[2][4][1] = 123 ; su[2][4][2] =  15 ;
	su[2][5][0] = -769 ; su[2][5][1] = 69 ; su[2][5][2] =  6 ;
	su[2][6][0] = -623 ; su[2][6][1] = -54 ; su[2][6][2] =  -27 ;
	su[2][7][0] = -468 ; su[2][7][1] = -279 ; su[2][7][2] =  -126 ;
	su[2][8][0] = -356 ; su[2][8][1] = -443 ; su[2][8][2] =  -239 ;
	su[2][9][0] = -295 ; su[2][9][1] = -548 ; su[2][9][2] =  -260 ;
	su[3][0][0] = -954 ; su[3][0][1] = -108 ; su[3][0][2] =  -108 ;
	su[3][1][0] = -959 ; su[3][1][1] = -104 ; su[3][1][2] =  -113 ;
	su[3][2][0] = -965 ; su[3][2][1] = -104 ; su[3][2][2] =  -96 ;
	su[3][3][0] = -965 ; su[3][3][1] = -88 ; su[3][3][2] =  -77 ;
	su[3][4][0] = -965 ; su[3][4][1] = -72 ; su[3][4][2] =  -94 ;
	su[3][5][0] = -954 ; su[3][5][1] = -68 ; su[3][5][2] =  -73 ;
	su[3][6][0] = -982 ; su[3][6][1] = 0 ; su[3][6][2] =  -94 ;
	su[3][7][0] = -977 ; su[3][7][1] = 32 ; su[3][7][2] =  -82 ;
	su[3][8][0] = -989 ; su[3][8][1] = 59 ; su[3][8][2] =  -68 ;
	su[3][9][0] = -987 ; su[3][9][1] = 74 ; su[3][9][2] =  -64 ;
	su[4][0][0] = -954 ; su[4][0][1] = 54 ; su[4][0][2] =  -72 ;
	su[4][1][0] = -955 ; su[4][1][1] = 54 ; su[4][1][2] =  -75 ;
	su[4][2][0] = -969 ; su[4][2][1] = 59 ; su[4][2][2] =  -71 ;
	su[4][3][0] = -989 ; su[4][3][1] = 44 ; su[4][3][2] =  -83 ;
	su[4][4][0] = -989 ; su[4][4][1] = 52 ; su[4][4][2] =  -80 ;
	su[4][5][0] = -980 ; su[4][5][1] = 52 ; su[4][5][2] =  -72 ;
	su[4][6][0] = -968 ; su[4][6][1] = 52 ; su[4][6][2] =  -57 ;
	su[4][7][0] = -954 ; su[4][7][1] = 61 ; su[4][7][2] =  -36 ;
	su[4][8][0] = -930 ; su[4][8][1] = 65 ; su[4][8][2] =  -23 ;
	su[4][9][0] = -856 ; su[4][9][1] = 65 ; su[4][9][2] =  -6 ;
	su[5][0][0] = -972 ; su[5][0][1] = -72 ; su[5][0][2] =  -108 ;
	su[5][1][0] = -972 ; su[5][1][1] = -73 ; su[5][1][2] =  -102 ;
	su[5][2][0] = -970 ; su[5][2][1] = -72 ; su[5][2][2] =  -82 ;
	su[5][3][0] = -963 ; su[5][3][1] = -64 ; su[5][3][2] =  -48 ;
	su[5][4][0] = -960 ; su[5][4][1] = -28 ; su[5][4][2] =  -55 ;
	su[5][5][0] = -973 ; su[5][5][1] = -5 ; su[5][5][2] =  -74 ;
	su[5][6][0] = -962 ; su[5][6][1] = 11 ; su[5][6][2] =  -82 ;
	su[5][7][0] = -966 ; su[5][7][1] = 44 ; su[5][7][2] =  -70 ;
	su[5][8][0] = -959 ; su[5][8][1] = 70 ; su[5][8][2] =  -54 ;
	su[5][9][0] = -956 ; su[5][9][1] = 91 ; su[5][9][2] =  -37 ;
	su[6][0][0] = -1008 ; su[6][0][1] = 54 ; su[6][0][2] =  18 ;
	su[6][1][0] = -1004 ; su[6][1][1] = 55 ; su[6][1][2] =  9 ;
	su[6][2][0] = -1008 ; su[6][2][1] = 53 ; su[6][2][2] =  9 ;
	su[6][3][0] = -1018 ; su[6][3][1] = 44 ; su[6][3][2] =  8 ;
	su[6][4][0] = -1008 ; su[6][4][1] = 50 ; su[6][4][2] =  9 ;
	su[6][5][0] = -1002 ; su[6][5][1] = 55 ; su[6][5][2] =  6 ;
	su[6][6][0] = -1011 ; su[6][6][1] = 53 ; su[6][6][2] =  7 ;
	su[6][7][0] = -1010 ; su[6][7][1] = 54 ; su[6][7][2] =  13 ;
	su[6][8][0] = -993 ; su[6][8][1] = 63 ; su[6][8][2] =  24 ;
	su[6][9][0] = -931 ; su[6][9][1] = 63 ; su[6][9][2] =  39 ;
	su[7][0][0] = -899 ; su[7][0][1] = 71 ; su[7][0][2] =  -27 ;
	su[7][1][0] = -778 ; su[7][1][1] = 45 ; su[7][1][2] =  -18 ;
	su[7][2][0] = -626 ; su[7][2][1] = -119 ; su[7][2][2] =  -3 ;
	su[7][3][0] = -534 ; su[7][3][1] = -337 ; su[7][3][2] =  -22 ;
	su[7][4][0] = -455 ; su[7][4][1] = -512 ; su[7][4][2] =  -127 ;
	su[7][5][0] = -378 ; su[7][5][1] = -590 ; su[7][5][2] =  -153 ;
	su[7][6][0] = -291 ; su[7][6][1] = -617 ; su[7][6][2] =  -146 ;
	su[7][7][0] = -197 ; su[7][7][1] = -620 ; su[7][7][2] =  -149 ;
	su[7][8][0] = -99 ; su[7][8][1] = -608 ; su[7][8][2] =  -152 ;
	su[7][9][0] = -11 ; su[7][9][1] = -594 ; su[7][9][2] =  -158 ;
	su[8][0][0] = -972 ; su[8][0][1] = 36 ; su[8][0][2] =  -90 ;
	su[8][1][0] = -975 ; su[8][1][1] = 37 ; su[8][1][2] =  -91 ;
	su[8][2][0] = -981 ; su[8][2][1] = 36 ; su[8][2][2] =  -87 ;
	su[8][3][0] = -987 ; su[8][3][1] = 34 ; su[8][3][2] =  -85 ;
	su[8][4][0] = -994 ; su[8][4][1] = 27 ; su[8][4][2] =  -81 ;
	su[8][5][0] = -997 ; su[8][5][1] = 27 ; su[8][5][2] =  -72 ;
	su[8][6][0] = -994 ; su[8][6][1] = 33 ; su[8][6][2] =  -66 ;
	su[8][7][0] = -991 ; su[8][7][1] = 42 ; su[8][7][2] =  -55 ;
	su[8][8][0] = -990 ; su[8][8][1] = 48 ; su[8][8][2] =  -45 ;
	su[8][9][0] = -988 ; su[8][9][1] = 59 ; su[8][9][2] =  -35 ;
	su[9][0][0] = -1008 ; su[9][0][1] = 108 ; su[9][0][2] =  36 ;
	su[9][1][0] = -1002 ; su[9][1][1] = 111 ; su[9][1][2] =  23 ;
	su[9][2][0] = -1020 ; su[9][2][1] = 94 ; su[9][2][2] =  20 ;
	su[9][3][0] = -1033 ; su[9][3][1] = 77 ; su[9][3][2] =  10 ;
	su[9][4][0] = -989 ; su[9][4][1] = 89 ; su[9][4][2] =  16 ;
	su[9][5][0] = -823 ; su[9][5][1] = 108 ; su[9][5][2] =  10 ;
	su[9][6][0] = -580 ; su[9][6][1] = 3 ; su[9][6][2] =  32 ;
	su[9][7][0] = -381 ; su[9][7][1] = -227 ; su[9][7][2] =  -82 ;
	su[9][8][0] = -418 ; su[9][8][1] = -434 ; su[9][8][2] =  -230 ;
	su[9][9][0] = -457 ; su[9][9][1] = -547 ; su[9][9][2] =  -198 ;
    ///end of MovementB training data
    ///MovemementC training data (face up)
    int fu[sample_size][sample_size][3];
	fu[0][0][0] = 72 ; fu[0][0][1] = -234 ; fu[0][0][2] =  -972 ;
	fu[0][1][0] = 70 ; fu[0][1][1] = -232 ; fu[0][1][2] =  -972 ;
	fu[0][2][0] = 66 ; fu[0][2][1] = -228 ; fu[0][2][2] =  -970 ;
	fu[0][3][0] = 66 ; fu[0][3][1] = -216 ; fu[0][3][2] =  -973 ;
	fu[0][4][0] = 61 ; fu[0][4][1] = -205 ; fu[0][4][2] =  -983 ;
	fu[0][5][0] = 45 ; fu[0][5][1] = -218 ; fu[0][5][2] =  -992 ;
	fu[0][6][0] = 42 ; fu[0][6][1] = -203 ; fu[0][6][2] =  -999 ;
	fu[0][7][0] = 32 ; fu[0][7][1] = -200 ; fu[0][7][2] =  -1001 ;
	fu[0][8][0] = 23 ; fu[0][8][1] = -190 ; fu[0][8][2] =  -992 ;
	fu[0][9][0] = 15 ; fu[0][9][1] = -171 ; fu[0][9][2] =  -970 ;
	fu[1][0][0] = 42 ; fu[1][0][1] = -223 ; fu[1][0][2] =  -999 ;
	fu[1][1][0] = 50 ; fu[1][1][1] = -224 ; fu[1][1][2] =  -990 ;
	fu[1][2][0] = 48 ; fu[1][2][1] = -234 ; fu[1][2][2] =  -961 ;
	fu[1][3][0] = 39 ; fu[1][3][1] = -241 ; fu[1][3][2] =  -870 ;
	fu[1][4][0] = 38 ; fu[1][4][1] = -175 ; fu[1][4][2] =  -703 ;
	fu[1][5][0] = 72 ; fu[1][5][1] = 71 ; fu[1][5][2] =  -526 ;
	fu[1][6][0] = 91 ; fu[1][6][1] = 292 ; fu[1][6][2] =  -500 ;
	fu[1][7][0] = 101 ; fu[1][7][1] = 430 ; fu[1][7][2] =  -435 ;
	fu[1][8][0] = 117 ; fu[1][8][1] = 480 ; fu[1][8][2] =  -341 ;
	fu[1][9][0] = 116 ; fu[1][9][1] = 493 ; fu[1][9][2] =  -224 ;
	fu[2][0][0] = 28 ; fu[2][0][1] = -230 ; fu[2][0][2] =  -929 ;
	fu[2][1][0] = 25 ; fu[2][1][1] = -225 ; fu[2][1][2] =  -904 ;
	fu[2][2][0] = 9 ; fu[2][2][1] = -213 ; fu[2][2][2] =  -788 ;
	fu[2][3][0] = 27 ; fu[2][3][1] = -58 ; fu[2][3][2] =  -585 ;
	fu[2][4][0] = 72 ; fu[2][4][1] = 176 ; fu[2][4][2] =  -465 ;
	fu[2][5][0] = 75 ; fu[2][5][1] = 379 ; fu[2][5][2] =  -355 ;
	fu[2][6][0] = 72 ; fu[2][6][1] = 474 ; fu[2][6][2] =  -278 ;
	fu[2][7][0] = 88 ; fu[2][7][1] = 536 ; fu[2][7][2] =  -225 ;
	fu[2][8][0] = 93 ; fu[2][8][1] = 558 ; fu[2][8][2] =  -130 ;
	fu[2][9][0] = 87 ; fu[2][9][1] = 577 ; fu[2][9][2] =  -30 ;
	fu[3][0][0] = 7 ; fu[3][0][1] = -243 ; fu[3][0][2] =  -947 ;
	fu[3][1][0] = 15 ; fu[3][1][1] = -245 ; fu[3][1][2] =  -949 ;
	fu[3][2][0] = 22 ; fu[3][2][1] = -252 ; fu[3][2][2] =  -954 ;
	fu[3][3][0] = 27 ; fu[3][3][1] = -257 ; fu[3][3][2] =  -961 ;
	fu[3][4][0] = 51 ; fu[3][4][1] = -272 ; fu[3][4][2] =  -969 ;
	fu[3][5][0] = 58 ; fu[3][5][1] = -286 ; fu[3][5][2] =  -974 ;
	fu[3][6][0] = 59 ; fu[3][6][1] = -298 ; fu[3][6][2] =  -968 ;
	fu[3][7][0] = 56 ; fu[3][7][1] = -304 ; fu[3][7][2] =  -948 ;
	fu[3][8][0] = 28 ; fu[3][8][1] = -297 ; fu[3][8][2] =  -912 ;
	fu[3][9][0] = 10 ; fu[3][9][1] = -276 ; fu[3][9][2] =  -838 ;
	fu[4][0][0] = 18 ; fu[4][0][1] = -216 ; fu[4][0][2] =  -972 ;
	fu[4][1][0] = 18 ; fu[4][1][1] = -221 ; fu[4][1][2] =  -973 ;
	fu[4][2][0] = 14 ; fu[4][2][1] = -227 ; fu[4][2][2] =  -972 ;
	fu[4][3][0] = 18 ; fu[4][3][1] = -224 ; fu[4][3][2] =  -975 ;
	fu[4][4][0] = 18 ; fu[4][4][1] = -199 ; fu[4][4][2] =  -971 ;
	fu[4][5][0] = 16 ; fu[4][5][1] = -171 ; fu[4][5][2] =  -976 ;
	fu[4][6][0] = 10 ; fu[4][6][1] = -135 ; fu[4][6][2] =  -964 ;
	fu[4][7][0] = 7 ; fu[4][7][1] = -107 ; fu[4][7][2] =  -954 ;
	fu[4][8][0] = 31 ; fu[4][8][1] = -81 ; fu[4][8][2] =  -873 ;
	fu[4][9][0] = 38 ; fu[4][9][1] = -20 ; fu[4][9][2] =  -697 ;
	fu[5][0][0] = 18 ; fu[5][0][1] = -180 ; fu[5][0][2] =  -972 ;
	fu[5][1][0] = 23 ; fu[5][1][1] = -183 ; fu[5][1][2] =  -973 ;
	fu[5][2][0] = 27 ; fu[5][2][1] = -193 ; fu[5][2][2] =  -971 ;
	fu[5][3][0] = 26 ; fu[5][3][1] = -193 ; fu[5][3][2] =  -978 ;
	fu[5][4][0] = 9 ; fu[5][4][1] = -216 ; fu[5][4][2] =  -981 ;
	fu[5][5][0] = 6 ; fu[5][5][1] = -210 ; fu[5][5][2] =  -972 ;
	fu[5][6][0] = 12 ; fu[5][6][1] = -208 ; fu[5][6][2] =  -973 ;
	fu[5][7][0] = 14 ; fu[5][7][1] = -210 ; fu[5][7][2] =  -972 ;
	fu[5][8][0] = 32 ; fu[5][8][1] = -217 ; fu[5][8][2] =  -982 ;
	fu[5][9][0] = 32 ; fu[5][9][1] = -216 ; fu[5][9][2] =  -1000 ;
	fu[6][0][0] = 18 ; fu[6][0][1] = -432 ; fu[6][0][2] =  -918 ;
	fu[6][1][0] = 19 ; fu[6][1][1] = -433 ; fu[6][1][2] =  -914 ;
	fu[6][2][0] = 22 ; fu[6][2][1] = -434 ; fu[6][2][2] =  -916 ;
	fu[6][3][0] = 23 ; fu[6][3][1] = -432 ; fu[6][3][2] =  -914 ;
	fu[6][4][0] = 22 ; fu[6][4][1] = -426 ; fu[6][4][2] =  -910 ;
	fu[6][5][0] = 23 ; fu[6][5][1] = -417 ; fu[6][5][2] =  -907 ;
	fu[6][6][0] = 8 ; fu[6][6][1] = -418 ; fu[6][6][2] =  -920 ;
	fu[6][7][0] = 7 ; fu[6][7][1] = -394 ; fu[6][7][2] =  -932 ;
	fu[6][8][0] = 9 ; fu[6][8][1] = -388 ; fu[6][8][2] =  -941 ;
	fu[6][9][0] = 15 ; fu[6][9][1] = -381 ; fu[6][9][2] =  -951 ;
	fu[7][0][0] = 0 ; fu[7][0][1] = -234 ; fu[7][0][2] =  -1008 ;
	fu[7][1][0] = 1 ; fu[7][1][1] = -230 ; fu[7][1][2] =  -997 ;
	fu[7][2][0] = 0 ; fu[7][2][1] = -226 ; fu[7][2][2] =  -989 ;
	fu[7][3][0] = 3 ; fu[7][3][1] = -216 ; fu[7][3][2] =  -971 ;
	fu[7][4][0] = -9 ; fu[7][4][1] = -207 ; fu[7][4][2] =  -996 ;
	fu[7][5][0] = -6 ; fu[7][5][1] = -197 ; fu[7][5][2] =  -997 ;
	fu[7][6][0] = -10 ; fu[7][6][1] = -188 ; fu[7][6][2] =  -994 ;
	fu[7][7][0] = -12 ; fu[7][7][1] = -174 ; fu[7][7][2] =  -988 ;
	fu[7][8][0] = -9 ; fu[7][8][1] = -190 ; fu[7][8][2] =  -981 ;
	fu[7][9][0] = -9 ; fu[7][9][1] = -181 ; fu[7][9][2] =  -969 ;
	fu[8][0][0] = -18 ; fu[8][0][1] = -162 ; fu[8][0][2] =  -1008 ;
	fu[8][1][0] = -5 ; fu[8][1][1] = -165 ; fu[8][1][2] =  -995 ;
	fu[8][2][0] = -8 ; fu[8][2][1] = -146 ; fu[8][2][2] =  -994 ;
	fu[8][3][0] = -16 ; fu[8][3][1] = -113 ; fu[8][3][2] =  -995 ;
	fu[8][4][0] = -12 ; fu[8][4][1] = -179 ; fu[8][4][2] =  -1008 ;
	fu[8][5][0] = -16 ; fu[8][5][1] = -179 ; fu[8][5][2] =  -1004 ;
	fu[8][6][0] = -10 ; fu[8][6][1] = -166 ; fu[8][6][2] =  -999 ;
	fu[8][7][0] = -5 ; fu[8][7][1] = -167 ; fu[8][7][2] =  -996 ;
	fu[8][8][0] = -11 ; fu[8][8][1] = -166 ; fu[8][8][2] =  -1002 ;
	fu[8][9][0] = 17 ; fu[8][9][1] = -178 ; fu[8][9][2] =  -1011 ;
	fu[9][0][0] = 54 ; fu[9][0][1] = -324 ; fu[9][0][2] =  -918 ;
	fu[9][1][0] = 52 ; fu[9][1][1] = -324 ; fu[9][1][2] =  -916 ;
	fu[9][2][0] = 43 ; fu[9][2][1] = -316 ; fu[9][2][2] =  -918 ;
	fu[9][3][0] = 29 ; fu[9][3][1] = -316 ; fu[9][3][2] =  -921 ;
	fu[9][4][0] = 18 ; fu[9][4][1] = -316 ; fu[9][4][2] =  -918 ;
	fu[9][5][0] = 18 ; fu[9][5][1] = -311 ; fu[9][5][2] =  -912 ;
	fu[9][6][0] = 10 ; fu[9][6][1] = -310 ; fu[9][6][2] =  -918 ;
	fu[9][7][0] = 14 ; fu[9][7][1] = -306 ; fu[9][7][2] =  -919 ;
	fu[9][8][0] = 5 ; fu[9][8][1] = -295 ; fu[9][8][2] =  -938 ;
	fu[9][9][0] = 0 ; fu[9][9][1] = -283 ; fu[9][9][2] =  -950 ;
    ///end of MovementC training data
    int tempA[BUFFER_SIZE];
    int tempB[BUFFER_SIZE];
    int tempC[BUFFER_SIZE];
    int diffArray[3][BUFFER_SIZE];
    int i,j, temp;
    for (i=0;i<3;i++){
    	for (j=0;j<BUFFER_SIZE;j++){
    		diffArray[i][j] = 0;
    		tempA[j] = 0;
    		tempB[j] = 0;
    		tempC[j] = 0;
    	}
    }

 ///K nearest neighbour 
    ///calculating the distance between current buffer and training data
    for (i=0;i<sample_size;i++){
        //comparing training data
        for (j=0;j<BUFFER_SIZE;j++){
            ///calculating distance (training sample i)
            tempA[j] = distance(motionBuffer[j],fd[i][j]); //vs MovementA
            tempB[j] = distance(motionBuffer[j],su[i][j]);  //vs MovementB
            tempC[j] = distance(motionBuffer[j],fu[i][j]);  //vs MovementC
        }
        temp = metro(tempA);
        diffArray[0][i] = temp;
        temp = metro(tempB);
        diffArray[1][i] = temp;
        temp = metro(tempC);
        diffArray[2][i] = temp;
    }
    //diffArray contains all data that we need to sort out
    //lets check our best candidates
    qsort(diffArray[0],sample_size,sizeof(int),sort1);
    qsort(diffArray[1],sample_size,sizeof(int),sort1);
    qsort(diffArray[2],sample_size,sizeof(int),sort1);
    int finalSort[k_nearest];
    //lets find the lowest values
    int kk = 0;
    int i1 = 0;
    int j1_ = 0;
    int k1 = 0;
    while (kk<k_nearest){
        if ((diffArray[0][i1] <= diffArray[1][j1_]) && (diffArray[0][i1] <= diffArray[2][k1])){
            finalSort[kk] = 0;
            i1++;
        }else if ((diffArray[1][j1_] <= diffArray[0][i1]) && (diffArray[1][j1_] < diffArray[2][k1])){
            finalSort[kk] = 1;
            j1_++;
        }else if ((diffArray[2][k1] < diffArray[0][i1]) && (diffArray[2][k1] < diffArray[1][j1_])){
            finalSort[kk] = 2;
            k1++;
        }
        kk++;
    }
    ///final voting
    g_print("Voting Array: %i %i %i %i %i\n",finalSort[0],finalSort[1],finalSort[2],finalSort[3],finalSort[4]);
    g_print("Smallest Values: ( %i %i )( %i %i )( %i %i )\n",diffArray[0][0],diffArray[0][1],diffArray[1][0],diffArray[1][1],diffArray[2][0],diffArray[2][1]);
    int voteA = 0;
    int voteB = 0;
    int voteC = 0;
    for (i=0;i<k_nearest;i++){
        if (finalSort[i] == 0){
            voteA++;
        }else if (finalSort[i] == 1){
            voteB++;
        }else if (finalSort[i] == 2){
            voteC++;
        }
    }
    int result = -1;
    if (voteA >= voteB && voteA >= voteC){
        result = 0;
        g_print("Possible Movement A (face down)\n");
    }else if (voteB > voteA && voteB > voteC){
        result = 1;
        g_print("Possible Movement B (side up)\n");
    }else if (voteC > voteA && voteC > voteB){
        result = 2;
        g_print("Possible Movement C (face up)\n");
    }
    if (firsttime == 1){
        create_emma_int(result);
    }
    if (diffArray[result][0] < e){
        g_print("Movement %i\n", result);
  	//  printf("soundKit:%s\n", soundKit);
        playSound(result);
    }
   return 0;
}

static gboolean on_roller_button_press_up (ClutterActor *rect, ClutterEvent *event, gpointer data){
	if (selectedKitNum < 7){
		setSoundKitNum (selectedKitNum+1);
		g_print(">>>>>>>>>>selectedKit: %s, selectedKitNum, %i\n",instr[selectedKitNum].folder, selectedKitNum );
	}
	return TRUE;
}

static gboolean on_roller_button_press_down (ClutterActor *rect, ClutterEvent *event, gpointer data){
	if (selectedKitNum > 0){
		setSoundKitNum (selectedKitNum - 1);
		g_print(">>>>>>>>>>selectedKit: %s, selectedKitNum, %i\n",instr[selectedKitNum].folder, selectedKitNum );
	}
	return TRUE;
}

///destroy function
gboolean jammer_gohome_button (TangleActor* actor, gpointer data){
	g_print("Jammer go home!\n");
	g_source_remove(loop_id);
	g_object_unref(G_OBJECT(jammerSample0));
	g_object_unref(G_OBJECT(jammerSample1));
	g_object_unref(G_OBJECT(jammerSample2));
	jammo_sequencer_stop(jammer_sequencer); //has to stop playing before going back
	g_object_unref(btnSet3);
	clutter_actor_hide(btnSet4);
	//g_free(instr);
	clutter_actor_hide(jammo_get_actor_by_id("jammer-view"));
	tangle_view_unload(TANGLE_VIEW(jammo_get_actor_by_id("jammer-view")));
	startmenu_goto_startmenu(NULL,NULL);
	//return_to_fullsequencer_gui();
	return TRUE;
}

///mentor function
gboolean jammer_press_mentor (TangleActor* actor, gpointer data){
	g_print("Jammer Mentor!\n");
	return TRUE;
}

gboolean jammer_press_play (TangleActor* actor, gpointer data){
	g_print("Jammer play!\n");
	if (!isJammerSeqPlaying){
		//start playing
		isJammerRecording = FALSE;
		isJammerPlaying = FALSE;
		isJammerSeqPlaying = TRUE;
		jammo_sequencer_stop(jammer_sequencer);
		jammo_sequencer_play(jammer_sequencer);
		tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("jammer_rec_button")),FALSE);
		tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("jammer_play_button")),TRUE);
	}else{
		//stop playing
		isJammerRecording = FALSE;
		isJammerPlaying = TRUE;
		isJammerSeqPlaying = FALSE;
		jammo_sequencer_stop(jammer_sequencer);
		tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("jammer_play_button")),FALSE);
	}	
	return TRUE;
}

/*RECORD*/
gboolean jammer_press_rec (TangleActor* actor, gpointer data){
	if (!isJammerRecording){
	//start recording
		//first stop the sequencer if it plays
		if (isJammerSeqPlaying){
			isJammerSeqPlaying = FALSE;
			isJammerPlaying = TRUE;
			tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("jammer_play_button")),FALSE);
		}
		//	
		isJammerRecording = TRUE;
		tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("jammer_rec_button")),TRUE);
	
	}else{
	//stop recording
		isJammerRecording = FALSE;
		tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("jammer_rec_button")),FALSE);
	}

	return TRUE;
}

static gboolean timeout_func(gpointer data){ ///this runs at jammerFreq (in millisecs)
	jammerCounter++;
	if (isJammerPlaying){	
		int j = 0;
		int x = 0;
		int y = 0;
		int z = 0;
		int SENSITIVITY = 200; ///how sensitive jammer is TODO: adjust it to current sound latency
		int accel_test;	
		accel_test = accel_read(&x, &y, &z);	
		if (accel_test != 0){
			//g_print ("No accelerometer present!\n");
		}
		if (jammerCounter<=BUFFER_SIZE){
			motionBuffer[BUFFER_SIZE-jammerCounter][0]  =  x;
			motionBuffer[BUFFER_SIZE-jammerCounter][1]  =  y;
			motionBuffer[BUFFER_SIZE-jammerCounter][2]  =  z;
		}else{
			for (j=BUFFER_SIZE-1;j>0;j--){
				motionBuffer[j][0] = motionBuffer[j-1][0];
				motionBuffer[j][1] = motionBuffer[j-1][1];
				motionBuffer[j][2] = motionBuffer[j-1][2];
			}
			motionBuffer[0][0]  =  x;
			motionBuffer[0][1]  =  y;
			motionBuffer[0][2]  =  z;
		}
		///build EMMA file (alex #1)
		if (jammerCounter == BUFFER_SIZE){
			 create_emma(motionBuffer);
		}
		///calculate results every 1.5 seconds //todo: adjust this
		if (jammerCounter%6 == 0){	
			if (jammerCounter == 6){
				motion_detect(SENSITIVITY, 1);
			}else{
				motion_detect(SENSITIVITY, 0);
			}
		}
	}		      
	return TRUE;
}
////INITIALISE JAMMER INSTRUMENTS
static void init_jammerinstr(ClutterActor *btnSet){
	//instruments init
	instr[0].id = 0;
	g_strlcpy(instr[0].avatarfile, "jammer_electric.png", 25);
	g_strlcpy(instr[0].folder, "elec", 5);	
	instr[0].isMelodic = FALSE;	
	instr[1].id = 1;
	g_strlcpy(instr[1].avatarfile, "jammer_ethnic.png", 25);
	g_strlcpy(instr[1].folder, "eth1", 5);
	instr[1].isMelodic = FALSE;	
	instr[2].id = 2;
	g_strlcpy(instr[2].avatarfile, "jammer_ethnic2.png", 25);
	g_strlcpy(instr[2].folder, "eth2", 5);
	instr[2].isMelodic = FALSE;	
	instr[3].id = 3;
	g_strlcpy(instr[3].avatarfile, "jammer_acoustic.png", 25);
	g_strlcpy(instr[3].folder, "popk", 5);
	instr[3].isMelodic = FALSE;
	instr[4].id = 4;
	g_strlcpy(instr[4].avatarfile, "jammer_glockenspiel.png", 25);
	g_strlcpy(instr[4].folder, "gloc", 5);	
	instr[4].isMelodic = TRUE;
	instr[5].id = 5;
	g_strlcpy(instr[5].avatarfile, "jammer_marimba.png", 25);
	g_strlcpy(instr[5].folder, "mari", 5);	
	instr[5].isMelodic = TRUE;
	instr[6].id = 6;
	g_strlcpy(instr[6].avatarfile, "jammer_oud.png", 25);
	g_strlcpy(instr[6].folder, "oudi", 5);	
	instr[6].isMelodic = TRUE;
	instr[7].id = 7;
	g_strlcpy(instr[7].avatarfile, "jammer_xylophone.png", 25);
	g_strlcpy(instr[7].folder, "xylo", 5);	
	instr[7].isMelodic = TRUE;
	//init images
	int posx = 0;
	int i;
	for (i=0;i<8;i++){
		instr[i].instrumentImg = tangle_texture_new(g_build_filename(JAMMERIMGPATH, instr[i].avatarfile, NULL));
		clutter_actor_set_size (instr[i].instrumentImg, 90, 90);
		clutter_actor_set_position (instr[i].instrumentImg, posx,25);
		clutter_container_add_actor (CLUTTER_CONTAINER (btnSet),instr[i].instrumentImg);
		clutter_actor_set_reactive (instr[i].instrumentImg, TRUE);
		posx = posx + 120;
	}
	
}

static int findNextSlot(){
	if (melodicSounds[0] == NULL){
		return 0;
	}else if (melodicSounds[1] == NULL){
		return 1;
	}else if (melodicSounds[2] == NULL){
		return 2;
	}
	return -1;
}

static int getKeyPos(ClutterActor *key, gboolean isBlack){
	gfloat x = 0.0;
	gfloat y = 0.0;
	clutter_actor_get_position(key, &x, &y);
//initial 0: rest 1,2,3
	if (isBlack){
		if (y == -150.0){
			return 0;
		}else{
			return -y/50+1;
		}
	}else{
		if (y == -204.0){
			return 0;
		}else{
			return -y/68+1;
		}
	}
}


static void setKeyPos(ClutterActor *key, int pos, gboolean isBlack){
	gfloat x = 0.0;
	gfloat y = 0.0;	
	clutter_actor_get_position(key, &x, &y);
	if (isBlack){
		if (pos == 0){
			y = -150;
		}else{
			y = -150+(pos)*50;
		}
	}else{
		if (pos == 0){
			y = -204;
		}else{
			y = -204+(pos)*68;
		}
	}
	clutter_actor_set_position(key, 0, y);
}

/*sets a melodic sound*/

static int setMelodicSound(ClutterActor *key){
	int i;	
	const gchar *keyName = clutter_actor_get_name(key);
	nextmelodicSoundsIndex = findNextSlot();
	i = melodicSoundsIndex;		
	melodicSounds[nextmelodicSoundsIndex] = keyName;		
	melodicSoundsIndex++;
	nextmelodicSoundsIndex++;
	if (melodicSoundsIndex == 3){
		setSoundKit(); //init all instruments	
	}
	//g_print("set:%s -- %s -- %s -- %i\n",melodicSounds[0],melodicSounds[1],melodicSounds[2], i );
	return i;
}

/*unsets a melodic sound*/
/*inputs: key ClutterActor and sound index position*/
static void unsetMelodicSound(ClutterActor *key, int i){
	melodicSounds[i-1] = NULL;
	nextmelodicSoundsIndex = i-1;
	melodicSoundsIndex--;
	//g_print("unset:%s -- %s -- %s -- %i\n",melodicSounds[0],melodicSounds[1],melodicSounds[2], i );
}

/*note preview*/

static void preview_note(ClutterActor *key){
	JammoSample* previewSample;
	const gchar *keyName = clutter_actor_get_name(key);
	char *file;
	file = g_strdup_printf("%s/%s/%s_%s.ogg", JAMMERSOUNDPATH,instr[selectedKitNum].folder, instr[selectedKitNum].folder,keyName);
	g_print("preview %s/%s/%s_%s.ogg\n", JAMMERSOUNDPATH,instr[selectedKitNum].folder, instr[selectedKitNum].folder,keyName);
	previewSample = jammo_sample_new_from_file(file);
	jammo_sample_play(previewSample);
}

/*play a white key*/

static gboolean white_key_pressed (ClutterActor *rect, ClutterEvent *event, gpointer data){
	int pos;
	int i;
	ClutterActor *parentActor;
	parentActor = clutter_actor_get_parent(rect);
	pos = getKeyPos(rect, FALSE);	

	if (pos == 0 && melodicSoundsIndex != 3){
		preview_note(parentActor);
		i = setMelodicSound(parentActor);
		i = i +1;
		//key is white and we can set a sound
		setKeyPos(rect, i, FALSE);
	}else if (pos != 0){
		//key has already a sound on and we need to unset it
		unsetMelodicSound(parentActor, pos);
		setKeyPos(rect, 0, FALSE);
	}
	return FALSE;
}


/*play a black key*/

static gboolean black_key_pressed (ClutterActor *rect, ClutterEvent *event, gpointer data){
	int i;
	int pos;
	pos = getKeyPos(rect, TRUE);	
	ClutterActor *parentActor;
	parentActor = clutter_actor_get_parent(rect);
	if (pos == 0 && melodicSoundsIndex != 3){
		preview_note(parentActor);
		i = setMelodicSound(parentActor);
		i = i + 1;
		//key is white and we can set a sound
		setKeyPos(rect, i, TRUE);
	}else if (pos != 0){
		//key has already a sound on and we need to unset it
		unsetMelodicSound(parentActor, pos);
		setKeyPos(rect, 0, TRUE);
	}
	return FALSE;	
}


/* builds octaves*/
static void create_octave(ClutterActor *octSet, gchar *octavenum){
	///notes order (builds from top)
	///b, a, g, f, e, d, c
	int i;
	int posy = 0;
	//white keys	
	for (i=0;i<7;i++){
		ClutterActor *whiteKey_container[i];
		whiteKey_container[i] = clutter_group_new();
		clutter_actor_set_size (whiteKey_container[i], 77, 68);
		clutter_actor_set_position (whiteKey_container[i], 0, posy);
		clutter_container_add_actor (CLUTTER_CONTAINER (octSet), whiteKey_container[i]);
		clutter_actor_set_reactive (whiteKey_container[i], TRUE);
		clutter_actor_set_clip_to_allocation (whiteKey_container[i], TRUE);
		///TODO: Doing it fast instead of elegant ... fix this
		if (i == 0){	
			clutter_actor_set_name(whiteKey_container[i], g_strconcat( "b",octavenum, NULL));
		}else if (i == 1){
			clutter_actor_set_name(whiteKey_container[i], g_strconcat( "a",octavenum, NULL));
		}else if (i == 2){
			clutter_actor_set_name(whiteKey_container[i], g_strconcat( "g",octavenum, NULL));
		}else if (i == 3){
			clutter_actor_set_name(whiteKey_container[i], g_strconcat( "f",octavenum, NULL));
		}else if (i == 4){
			clutter_actor_set_name(whiteKey_container[i], g_strconcat( "e",octavenum, NULL));
		}else if (i == 5){
			clutter_actor_set_name(whiteKey_container[i], g_strconcat( "d",octavenum, NULL));
		}else if (i == 6){
			clutter_actor_set_name(whiteKey_container[i], g_strconcat( "c",octavenum, NULL));
		}
		//////TEXTURE FILE
		ClutterActor *octSet1_whiteKeysTexture;
		octSet1_whiteKeysTexture = tangle_texture_new(g_build_filename(JAMMERIMGPATH, "jammer_key_white-withbg.png", NULL));
		clutter_actor_set_size (octSet1_whiteKeysTexture,77, 272);
		clutter_actor_set_position (octSet1_whiteKeysTexture, 0, -204);
		clutter_container_add_actor (CLUTTER_CONTAINER (whiteKey_container[i]), octSet1_whiteKeysTexture);
		clutter_actor_set_reactive (octSet1_whiteKeysTexture, TRUE);
		g_signal_connect (octSet1_whiteKeysTexture, "button-press-event", G_CALLBACK (white_key_pressed), NULL);
		posy = posy + 68;
	}
	//black keys -- skips iteration #3
	posy = 40;
	for (i=0;i<6;i++){
		ClutterActor *blackKey_container[i];
		if (i != 3){
			blackKey_container[i] = clutter_group_new();
			clutter_actor_set_size (blackKey_container[i], 46, 50);
			clutter_actor_set_position (blackKey_container[i], 0, posy);
			clutter_container_add_actor (CLUTTER_CONTAINER (octSet), blackKey_container[i]);
			clutter_actor_set_reactive (blackKey_container[i], TRUE);
			clutter_actor_set_clip_to_allocation (blackKey_container[i], TRUE);
			///TODO: Doing it fast instead of elegant ... fix this
			if (i == 0){	
				clutter_actor_set_name(blackKey_container[i], g_strconcat( "a#",octavenum, NULL));
			}else if (i == 1){
				clutter_actor_set_name(blackKey_container[i], g_strconcat( "g#",octavenum, NULL));
			}else if (i == 2){
				clutter_actor_set_name(blackKey_container[i], g_strconcat( "f#",octavenum, NULL));
			}else if (i == 4){
				clutter_actor_set_name(blackKey_container[i], g_strconcat( "d#",octavenum, NULL));
			}else if (i == 5){
				clutter_actor_set_name(blackKey_container[i], g_strconcat( "c#",octavenum, NULL));
			}
					
			//////TEXTURE FILE
			ClutterActor *octSet1_blackKeysTexture;
			octSet1_blackKeysTexture = tangle_texture_new(g_build_filename(JAMMERIMGPATH, "jammer_key_black-withbg.png", NULL));
			clutter_actor_set_size (octSet1_blackKeysTexture,43, 200);
			clutter_actor_set_position (octSet1_blackKeysTexture, 0, -150);
			clutter_container_add_actor (CLUTTER_CONTAINER (blackKey_container[i]), octSet1_blackKeysTexture);
			clutter_actor_set_reactive (octSet1_blackKeysTexture, TRUE);
			g_signal_connect (octSet1_blackKeysTexture, "button-press-event", G_CALLBACK (black_key_pressed), NULL);
		}
		posy = posy + 70;
	}
}

void 
start_jammer ()
{
	//init clutter
	ClutterActor *stage;
	stage = tangle_widget_new(); //TODO: define this and all other (tangle_texture_new) on jammer-view.json! --moved some to json

	//init jammer
	g_print("starting jammer\n");
	isJammerPlaying = TRUE;
	isJammerRecording = FALSE;
	isJammerSeqPlaying = FALSE;
	jammer_sequencer = jammo_sequencer_new();// JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));
	jammer_editing_track = jammo_editing_track_new(); 
	jammo_sequencer_add_track (jammer_sequencer, JAMMO_TRACK (jammer_editing_track));
	guint jammerFreq = 100; ///how often jammer reads values and calcs TODO: adjust it to current sound latency
	sliderposx = 15.0;
	jammerCounter = 0;
	selectedKitNum = 1;
	/*Roller group (btnSet2)*/
	ClutterActor *btnSet2 = clutter_group_new();
	clutter_actor_set_size (btnSet2, 200, 240);
	clutter_actor_set_position (btnSet2, 130, 100);
	clutter_container_add_actor (CLUTTER_CONTAINER (stage), btnSet2);
	clutter_actor_set_reactive (btnSet2, TRUE);
	/*Up button right*/
	ClutterActor *upBtn1 = tangle_texture_new(g_build_filename(JAMMERIMGPATH, "jammer_arrows_up.png", NULL));
	clutter_actor_set_size (upBtn1, 84, 23);
	clutter_actor_set_position (upBtn1, 0,0);
	clutter_container_add_actor (CLUTTER_CONTAINER (btnSet2),upBtn1);
	g_signal_connect (upBtn1, "button-press-event", G_CALLBACK (on_roller_button_press_up), NULL);
	clutter_actor_set_reactive (upBtn1, TRUE);
	/*Up button left*/
	ClutterActor *upBtn2 = tangle_texture_new(g_build_filename(JAMMERIMGPATH, "jammer_arrows_up.png", NULL));
	clutter_actor_set_size (upBtn2, 84, 23);
	clutter_actor_set_position (upBtn2, 0,260);
	clutter_container_add_actor (CLUTTER_CONTAINER (btnSet2),upBtn2);
	g_signal_connect (upBtn2, "button-press-event", G_CALLBACK (on_roller_button_press_up), NULL);
	clutter_actor_set_reactive (upBtn2, TRUE);
	/*Down button right*/
	ClutterActor *downBtn1 = tangle_texture_new(g_build_filename(JAMMERIMGPATH, "jammer_arrows_down.png", NULL));
	clutter_actor_set_size (downBtn1, 84, 23);
	clutter_actor_set_position (downBtn1, 120,0);
	clutter_container_add_actor (CLUTTER_CONTAINER (btnSet2),downBtn1);
	g_signal_connect (downBtn1, "button-press-event", G_CALLBACK (on_roller_button_press_down), NULL);
	clutter_actor_set_reactive (downBtn1, TRUE);
	/*Down button left*/
	ClutterActor *downBtn2 = tangle_texture_new(g_build_filename(JAMMERIMGPATH, "jammer_arrows_down.png", NULL));
	clutter_actor_set_size (downBtn2, 84, 23);
	clutter_actor_set_position (downBtn2, 120,260);
	clutter_container_add_actor (CLUTTER_CONTAINER (btnSet2),downBtn2);
	g_signal_connect (downBtn2, "button-press-event", G_CALLBACK (on_roller_button_press_down), NULL);
	clutter_actor_set_reactive (downBtn2, TRUE);
	/*End of button group btnSet2*/
	/*Start of button group for instruments  btnSet3*/
	ClutterActor *viewpoint3 = clutter_group_new();
	clutter_actor_set_size (viewpoint3, 350, 140);
	clutter_actor_set_position (viewpoint3, 60, 170);
	clutter_container_add_actor (CLUTTER_CONTAINER (stage), viewpoint3);
	clutter_actor_set_reactive (viewpoint3, FALSE);
	clutter_actor_set_clip_to_allocation (viewpoint3, TRUE);	
	btnSet3 = clutter_group_new();
	clutter_actor_set_size (btnSet3, 1000, 140);
	clutter_actor_set_position (btnSet3,sliderposx, 0);
	clutter_container_add_actor (CLUTTER_CONTAINER (viewpoint3), btnSet3);
	clutter_actor_set_reactive (btnSet3, FALSE);
	/*instrument positions*/
	g_object_ref(btnSet3);
	init_jammerinstr(btnSet3);
	//init kits
	setSoundKit();
	/*gui element*/
	ClutterActor *chosenBtn = tangle_texture_new(g_build_filename(JAMMERIMGPATH, "chosen.png", NULL));
	clutter_actor_set_size (chosenBtn, 130, 130);
	clutter_actor_set_position (chosenBtn, 115,5);
	clutter_container_add_actor (CLUTTER_CONTAINER (viewpoint3),chosenBtn);
	clutter_actor_set_reactive (downBtn2, FALSE);
	/*End of button group btnSet3*/
	/*Build octaves for melodic instruments (btnSet4)*/
	btnSet4 = clutter_group_new();
	clutter_actor_set_size (btnSet4, 240, 480);
	clutter_actor_set_position (btnSet4, 570, 3);
	clutter_container_add_actor (CLUTTER_CONTAINER (stage), btnSet4);
	clutter_actor_set_reactive (btnSet4, FALSE);
	/* Top octave */
	ClutterActor *octSet1 = clutter_group_new();
	clutter_actor_set_size (octSet1,77, 460);
	clutter_actor_set_position (octSet1, 0, 0);
	clutter_container_add_actor (CLUTTER_CONTAINER (btnSet4), octSet1);
	clutter_actor_set_reactive (octSet1, FALSE);
	create_octave(octSet1, "1");
	/*middle octave*/
	ClutterActor *octSet2 = clutter_group_new();
	clutter_actor_set_size (octSet2,77, 480);
	clutter_actor_set_position (octSet2, 77, 0);
	clutter_container_add_actor (CLUTTER_CONTAINER (btnSet4), octSet2);
	clutter_actor_set_reactive (octSet2, FALSE);
	create_octave(octSet2, "2");		
	/*bottom octave*/
	ClutterActor *octSet3 = clutter_group_new();
	clutter_actor_set_size (octSet3,77, 480);
	clutter_actor_set_position (octSet3, 154, 0);
	clutter_container_add_actor (CLUTTER_CONTAINER (btnSet4), octSet3);
	clutter_actor_set_reactive (octSet3, FALSE);
	create_octave(octSet3, "3");
	/*hide piano*/
	clutter_actor_hide_all(btnSet4);
	/*End of button group btnSet4*/
	//////setting up jammer vars
	g_print("motionBuffer starting\n");
	int i, j;	
	for (i=0;i < BUFFER_SIZE; i++){
		for (j=0;j<3;j++){
			motionBuffer[i][j] = 0;
		}
	}
	///
	g_print("loop starting\n");
 	loop_id = g_timeout_add(jammerFreq,timeout_func, NULL); 
	g_print("show jammer\n");
	clutter_actor_show(stage);
	tangle_widget_add(TANGLE_WIDGET(jammo_get_actor_by_id("jammer-view")),stage,NULL);
}
