/*
 * This file is part of JamMo.
 *
 * Instrument_gui is realtime 'jamming' UI.
 * This is used with midi-instrument and slider.
 *
 *
 * Midi and slider are not symmetric.
 * Midi-track comes inside of miditrack-view and it should handle visualized notes
 * Slider-track comes inside of TangeWidget and doesn't need any visualization (at current)
 *
 *Three cases:
 * A) not yet recording, just testing jamming
 *    there are ad-hoc sequencer containing ad-hoc-instrument-track
 *B) recording.
 *    this plays real-sequencer + real-instrument-track on recording mode
 * c) playback.
 *    this plays real_sequencer
 *
 *
 * When playbacking static_sequencer, keyboard and slider is not working
 * i.e. no visual feedback, no audible feedback, no storing events.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */

#include <tangle.h>
#include <glib-object.h>
#include <ctype.h>
#include <string.h>
#include <clutter/clutter.h>


#include "../../meam/jammo-meam.h"
#include "../../meam/jammo-instrument-track.h"
#include "../../meam/jammo-slider-track.h"
#include "../../meam/jammo-midi.h"
#include "../jammo-chum.h"
#include "../jammo.h"
#include "../jammo-miditrack-view.h"
#include "../../cem/cem.h"

#include "midi_editor.h"

#include "slider_helper.h" //There are ranges for slider

#include "sequencer.h"
//We want disconnect this when leaving
static gulong handler_for_sequencer_stop = 0;

//We need to know, are we playing with piano keys or drumset
static gboolean using_drumkit=FALSE;

//For keyboard
static int UPPER_OCTAVE=3;
// TRANSPOSE is used to transpose keyboard to a different key
// e.g. value 2 transposes keyboard from c to d
// meaning that keys produce sounds d,d#,e... although the keyboard layout is normal c,c#,d...
static int TRANSPOSE=0;
void instrument_gui_set_transpose(char key) {
	switch (key) {
		case 'c':
			TRANSPOSE=0;
			break;
		case 'a':
			TRANSPOSE=+9;
			break;
		case 'd':
			TRANSPOSE=+2;
			break;
		case 'g':
			TRANSPOSE=+7;
			break;
		default:
			// discard
			break;
	}
}
char instrument_gui_get_transpose() {
	switch (TRANSPOSE) {
		case 0:
			return 'c';
			break;
		case 2:
			return 'd';
			break;
		case 9:
			return 'a';
			break;
		case 7:
			return 'g';
			break;
		default:
			// should never come here
			break;
	}
	return 'c';
}

//These we get as parameter and will return after use
static JammoSequencer* static_sequencer;
static JammoMiditrackView* static_jammo_miditrack_view;
static JammoSliderTrack* static_slider_track;


//These are made only for this session and should be free'ed
static JammoInstrumentTrack *ad_hoc_instrument_track;
static JammoSliderTrack* ad_hoc_slider_track;
static JammoSequencer* ad_hoc_sequencer;


//State of recording
static gboolean recording;

static gboolean playing;

//This is pointer to marker of slider (we need to change which slider-track is moving it)
static ClutterActor* slider_marker;

static void end_jamming();

//Keyboard specific
//-----------------

/*
We got notevalue 0...96
if it is negative it means event_off.
keyboad name can be:
	instrument_key_c
	instrument_key_cs
	instrument_key_c2
	instrument_key_cs2
	s= sharp
	2= second row
*/
static void visualize_playback(JammoInstrumentTrack* track, gint note, gpointer none) {
	//printf("visualize_playback: note_value: %d\n",note);
	gboolean sharp = FALSE;
	gboolean bottom = FALSE;
	gboolean event_off = FALSE;
	if (note<0) //OFF event
		{
		event_off=TRUE;
		note *=-1;
		}

	note -= TRANSPOSE;

	const char* notename = jammomidi_note_to_char(note);
	if (notename[1]=='#')
		sharp=TRUE;

	if (note<12*(UPPER_OCTAVE+1))
		bottom=TRUE;

	const char* id  = g_strdup_printf("instrument_%s_%c%s%s",using_drumkit?"drum":"key",notename[0],sharp?"s":"",bottom?"2":"");
	//printf("looking for id '%s'\n",id);
	ClutterActor* key = jammo_get_actor_by_id(id);
	//printf("key id '%s'\n",id);

	//Swap normal and interactive textures when note-ON.
	//And change back when note-OFF.
	ClutterActor* old_normal;
	ClutterActor* old_interactive;

	//This works even there are two ONs or two OFFs in a row (even there should not be)
	//first time for each key:
	if (g_object_get_data(G_OBJECT(key), "orig_normal")==NULL){
		//printf("first time\n");
		old_normal = tangle_button_get_normal_background_actor(TANGLE_BUTTON(key));
		old_interactive = tangle_button_get_interactive_background_actor(TANGLE_BUTTON(key));
		g_object_set_data(G_OBJECT(key), "orig_normal", old_normal);
		g_object_set_data(G_OBJECT(key), "orig_interactive", old_interactive);
	}

	if (event_off) {
		//printf(" OFF\n");
		old_normal = g_object_get_data(G_OBJECT(key), "orig_interactive");
		old_interactive = g_object_get_data(G_OBJECT(key), "orig_normal");
	}
	else  {
		//printf(" ON\n");
		old_normal = g_object_get_data(G_OBJECT(key), "orig_normal");
		old_interactive = g_object_get_data(G_OBJECT(key), "orig_interactive");
	}

	g_object_ref(old_interactive);
	g_object_ref(old_normal);
	tangle_button_set_interactive_background_actor(TANGLE_BUTTON(key), NULL);
	tangle_button_set_normal_background_actor(TANGLE_BUTTON(key), NULL);

	tangle_button_set_normal_background_actor(TANGLE_BUTTON(key),old_interactive);
	tangle_button_set_interactive_background_actor(TANGLE_BUTTON(key),old_normal);
	g_object_unref(old_normal);
	g_object_unref(old_interactive);
}


/*
Name of the pressed tanglebutton is three char lenght
first is notename c,d,e,f...
second is 's' = sharp, 't' = not sharp
third is '1'=upper octave, '2'= bottom octave
eg:
ct1
cs1
ct2
cs2

returns 0..96 notevalue
*/
static char notename_to_numericvalue(const gchar* name) {
	int octave=UPPER_OCTAVE; //TODO. use some good octave from sequencer or midi-track
	char note[2];  //We want pass given char to function which wants char* 
	note[0]= name[0];
	note[1] = '\0';

	if (name[2]!='2') //bottom row
		octave++;

  //this returns notevalue 0...96
	char playable_note = jammomidi_char_to_note(note,octave); 
	if (name[1]=='s') //sharp
		playable_note++;  //use next note-value

	//printf(" which means note-value: %d\n",playable_note);
	return playable_note;
}

/*
This is JSON-callback for 'notify::interaction'
i.e pressing and releasing
*/
void instrument_key_interaction_notify (GObject *object, GParamSpec *pspec, gpointer user_data)  {
		const gchar* name = clutter_actor_get_name(CLUTTER_ACTOR(object));
		//printf("Keyboard '%s' pressed\n",name);
		TangleButton* button;

		JammoInstrumentTrack* instrument_track;
		if (recording){
			g_object_get(static_jammo_miditrack_view,"track",&instrument_track,NULL);
		}
		else {
			instrument_track = ad_hoc_instrument_track;
		}

		button = TANGLE_BUTTON(object);
		char notevalue = notename_to_numericvalue(name);
		notevalue += TRANSPOSE;
		if (tangle_actor_get_interacting(TANGLE_ACTOR(button))) {
			jammo_instrument_track_note_on_realtime (instrument_track, notevalue);
		} else {
			jammo_instrument_track_note_off_realtime (instrument_track, notevalue);
		}

}

gboolean instrument_goeditor_clicked (TangleActor* actor, gpointer data){
	cem_add_to_log("GoTo midi-editor pressed",J_LOG_USER_ACTION);
	end_jamming();

	//start editor
	midi_editor_start_with_miditrack_view(static_jammo_miditrack_view, static_sequencer);

	return TRUE;
}


gboolean instrumentview_goto_startmenu (TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("From  instrumentview to startmenu clicked",J_LOG_USER_ACTION);
	end_jamming();

	clutter_actor_show (jammo_get_actor_by_id("startmenu-view"));
	return TRUE;
}


gboolean instrumentview_goto_instrumentmenu (TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("From  instrument-gui to instrumentmenu clicked",J_LOG_USER_ACTION);
	end_jamming();

	clutter_actor_show (jammo_get_actor_by_id("instrumentmenu-view"));
	return TRUE;
}

//////////////////////////
static gulong handler_for_update_marker = 0;

//slider-specific
//---------------
static void update_marker(JammoSliderTrack* track, gfloat frequency, gpointer none) {
		//printf("update_marker: '%p' Frequency %f\n",track,frequency);
		gfloat x = freq_to_coordinate(frequency);
		if (frequency==0) //-> x=-inf
			x=0.0;
		//printf("X: %f\n",x);
		clutter_actor_set_x(CLUTTER_ACTOR(slider_marker),x);
}

gboolean instrument_gui_slider_handler (ClutterActor *actor, ClutterEvent *event, gpointer user_data) {
	gfloat x_release, y_release;
	gfloat freq;
	clutter_event_get_coords (event, &x_release, &y_release);
	//printf("slider_handler: X=%d\n",(gint)x_release);

	freq = coordinate_to_freq((gint)x_release);
	//printf("Freq: %f\n",freq);

	JammoSliderTrack* slider_track;
	if (recording){
		slider_track = static_slider_track;
	}
	else {
		slider_track = ad_hoc_slider_track;
	}


	if (event->type == CLUTTER_BUTTON_PRESS) {
		//printf("pressed\n");
		jammo_slider_track_set_on_realtime(slider_track, freq);
	} else if (event->type == CLUTTER_BUTTON_RELEASE) {
		//printf("released\n");
		jammo_slider_track_set_off_realtime(slider_track, freq);
	}	else if (event->type == CLUTTER_MOTION) {
		//printf("motion\n");
		jammo_slider_track_set_frequency_realtime(slider_track, freq);
	}
	return FALSE;
}
/////////////////////////////////






//TODO
gboolean instrument_metronome_clicked (TangleActor* actor, gpointer data){
	cem_add_to_log("Metronome-button on virtual-instrument pressed (doesn't do anything yet)",J_LOG_USER_ACTION);
	return FALSE;
}


static void end_jamming() {
	if (recording)
		jammo_sequencer_stop (JAMMO_SEQUENCER (static_sequencer));

	if (ad_hoc_slider_track) {
		g_signal_handler_disconnect (ad_hoc_slider_track,handler_for_update_marker);
		ad_hoc_slider_track=NULL;
	}

	jammo_sequencer_stop(ad_hoc_sequencer);
	g_object_unref(ad_hoc_sequencer);

	g_signal_handler_disconnect(static_sequencer, handler_for_sequencer_stop);

	//hide instrument_view
	ClutterActor* mainview;
	mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);
}



//TODO
gboolean  instrument_mentor_clicked (TangleActor* actor, gpointer data) {
	cem_add_to_log("Mentor-button on virtual-instrument pressed (doesn't do anything yet)",J_LOG_USER_ACTION);
	return FALSE;
}

gboolean instrument_gosequencer_clicked (TangleActor* actor, gpointer data){
	cem_add_to_log("BackToSequencer-button on virtual-instrument pressed",J_LOG_USER_ACTION);
	end_jamming();

	//show sequencer
	//ClutterActor* view = jammo_get_actor_by_id("fullsequencer-view");
	//clutter_actor_show(view);
	return_to_fullsequencer_gui();
	return TRUE;
}

gboolean instrument_play_clicked (TangleActor* actor, gpointer data) {
	cem_add_to_log("Play-button on virtual-instrument pressed",J_LOG_USER_ACTION);
	if (recording) {
		//printf("STOPping recording first\n");
	jammo_sequencer_stop (JAMMO_SEQUENCER (static_sequencer));
	}

	if (playing) {
		//printf("Already playing -> stop\n");
		jammo_sequencer_stop (JAMMO_SEQUENCER (static_sequencer));
		return TRUE;
	}

	//printf("Playing sequencer\n");
	playing=TRUE;
	//If user has pressed play-button during record, we need handle this manually
	tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("instrument_play")),TRUE);

	//stop ad_hoc.
	jammo_sequencer_stop (JAMMO_SEQUENCER (ad_hoc_sequencer));

	if (static_slider_track) {
		//Change this slider_track to move slider_marker.
		g_signal_handler_disconnect (ad_hoc_slider_track,handler_for_update_marker);
		handler_for_update_marker = g_signal_connect (static_slider_track, "report-current-frequency", G_CALLBACK (update_marker),NULL);
	}

	//Start real-sequencer
	jammo_sequencer_play(JAMMO_SEQUENCER(static_sequencer));

	return TRUE;
}


gboolean instrument_record_clicked (TangleButton *tanglebutton, gpointer user_data)  {
	cem_add_to_log("Record-button on virtual-instrument pressed",J_LOG_USER_ACTION);
	if (recording) {
		//printf("STOP recording\n");
		jammo_sequencer_stop (JAMMO_SEQUENCER (static_sequencer));
		return TRUE;
	}

	if (playing) {
		//printf("STOP playing and start recording\n");
		jammo_sequencer_stop (JAMMO_SEQUENCER (static_sequencer));
	}

	//printf("START recording\n");
	recording = TRUE;

	//If user has pressed record-button during play, we need handle this manually
	tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("instrument_record")),TRUE);


	//TODO, there should be some prelude, or precount with metronome


	//stop ad_hoc.
	jammo_sequencer_stop (JAMMO_SEQUENCER (ad_hoc_sequencer));

	//Slider:
	if (static_jammo_miditrack_view==NULL) {
		//clean original
		jammo_slider_track_set_event_list(JAMMO_SLIDER_TRACK(static_slider_track),NULL);

		//set to realtime
		jammo_slider_track_set_recording (JAMMO_SLIDER_TRACK(static_slider_track), TRUE);

		//Change this slider_track to move slider_marker.
		g_signal_handler_disconnect (ad_hoc_slider_track,handler_for_update_marker);
		handler_for_update_marker = g_signal_connect (static_slider_track, "report-current-frequency", G_CALLBACK (update_marker),NULL);

		cem_add_to_log("Virtual Instrument recording starts",J_LOG_INFO);
	}


	//Keyboard:
	else {
		//clean original
		JammoTrack* track;
		g_object_get(static_jammo_miditrack_view,"track",&track,NULL);
		jammo_instrument_track_set_event_list(JAMMO_INSTRUMENT_TRACK(track),NULL);

		//set to realtime
		jammo_instrument_track_set_realtime (JAMMO_INSTRUMENT_TRACK(track), TRUE);
	}

	//Now all content in sequencer will play and instrument_track is recording
	jammo_sequencer_play (JAMMO_SEQUENCER (static_sequencer));

	return TRUE;
}

//TODO: this signal should be removed when changing view.
static void instrument_gui_on_sequencer_stopped(JammoSequencer* sequencer, gpointer user_data) {
	//printf("instrument_gui_on_sequencer_stopped\n");

	//Slider:
	if (static_jammo_miditrack_view==NULL) {
		//No need to copy events like in miditrack.

		//If playback has just stopped, this is already FALSE, but it is not harm
		jammo_slider_track_set_recording (JAMMO_SLIDER_TRACK(static_slider_track), FALSE);
		//Change this slider_track to move slider_marker.
		//g_signal_handler_disconnect (static_slider_track,handler_for_update_marker); //This is not assigned
		handler_for_update_marker = g_signal_connect (ad_hoc_slider_track, "report-current-frequency", G_CALLBACK (update_marker),NULL);
	}


	if (recording) {
		recording = FALSE;

		//Keyboard
		if (static_jammo_miditrack_view){
			JammoTrack* track;
			g_object_get(static_jammo_miditrack_view,"track",&track,NULL);

			//set to non-realtime (unless we cant use jammo_miditrack_view_add_event_list)
			jammo_instrument_track_set_realtime (JAMMO_INSTRUMENT_TRACK(track), FALSE);

			//take all note-events from track
			GList* list= jammo_instrument_track_get_event_list(JAMMO_INSTRUMENT_TRACK(track));
			//Clean midi-track-view and track inside it
			jammo_miditrack_view_remove_all(static_jammo_miditrack_view);
			//And put them back via miditrack_view
			jammo_miditrack_view_add_event_list(static_jammo_miditrack_view,list);
		}
	}

	//else = playback has just stopped
	playing=FALSE;

	//Both play and rec buttons should be non-pressed
	tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("instrument_record")),FALSE);
	tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("instrument_play")),FALSE);

	//Start ad-hoc-sequencer
	jammo_sequencer_play  (JAMMO_SEQUENCER (ad_hoc_sequencer));
}

/*
Call this
a) instrument_gui_start(JAMMO_MIDITRACK_VIEW(track->track_view),sequencer, NULL);
or
b) instrument_gui_start(NULL,sequencer, TANGLE_WIDGET(track->track_view));
*/
void instrument_gui_start (JammoMiditrackView* miditrack_view, TangleWidget* slider_container)
{
	static_sequencer = jammo_sequencer_new();
	//JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));
	handler_for_sequencer_stop = g_signal_connect(static_sequencer, "stopped", G_CALLBACK(instrument_gui_on_sequencer_stopped), NULL);

	ClutterActor* mainview;
	mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);

	ClutterActor *view  = jammo_get_actor_by_id("instrument-view");
	clutter_actor_show(view);

	recording = FALSE;
	playing = FALSE;

	ad_hoc_sequencer = jammo_sequencer_new ();
	static_slider_track = NULL;
	static_jammo_miditrack_view = NULL;

	if (slider_container){
		//printf("starting slider\n");
		clutter_actor_hide(jammo_get_actor_by_id("instrument-two-rows-keyboard")); //this will also hide go_editor-button
		clutter_actor_hide(jammo_get_actor_by_id("instrument-drumset"));


		clutter_actor_show(jammo_get_actor_by_id("instrument-slider"));

		g_object_get(slider_container,"track",&static_slider_track,NULL);
		jammo_sequencer_add_track(static_sequencer,JAMMO_TRACK(static_slider_track));

		//Make ad-hoc-track using same type
		int type;
		g_object_get(static_slider_track,"slider-type",&type,NULL);
		gchar* message = g_strdup_printf("Starting instrument-gui with slider-type: %d",type);
		cem_add_to_log(message,J_LOG_DEBUG);
		g_free(message);

		ad_hoc_slider_track = jammo_slider_track_new (type);
		jammo_sequencer_add_track (ad_hoc_sequencer, JAMMO_TRACK (ad_hoc_slider_track));

		//Track must be added to sequencer before changing it recording
		jammo_slider_track_set_recording (ad_hoc_slider_track, TRUE);

		handler_for_update_marker = g_signal_connect (ad_hoc_slider_track, "report-current-frequency", G_CALLBACK (update_marker),NULL);

		//Marker = cursor for slider frequency.
		//TODO: use different marker (relating selected type)
		slider_marker =  jammo_get_actor_by_id("instrument-slider-marker");
		//After changing texture, tune anchor-point
		clutter_actor_set_anchor_point(slider_marker,clutter_actor_get_width(slider_marker)/2,0.0);
	}

	else {
		//Take miditrack_view safe to static variable
		static_jammo_miditrack_view=miditrack_view;

		JammoTrack* track;
		g_object_get(miditrack_view,"track",&track,NULL);
		g_signal_connect (track, "report-note", G_CALLBACK (visualize_playback),NULL);

		jammo_sequencer_add_track(static_sequencer,track);
		int type;
		g_object_get(track,"instrument-type",&type,NULL);
		gchar* message = g_strdup_printf("Starting instrument-gui with instrument-type: %d",type);
		cem_add_to_log(message,J_LOG_DEBUG);
		g_free(message);

		clutter_actor_hide(jammo_get_actor_by_id("instrument-slider"));
		clutter_actor_hide(jammo_get_actor_by_id("instrument-two-rows-keyboard"));
		clutter_actor_hide(jammo_get_actor_by_id("instrument-drumset"));
		if (type==JAMMO_INSTRUMENT_TYPE_DRUMKIT){
			clutter_actor_show(jammo_get_actor_by_id("instrument-drumset"));
			using_drumkit=TRUE;
		}
		else{
			clutter_actor_show(jammo_get_actor_by_id("instrument-two-rows-keyboard"));
			using_drumkit=FALSE;
		}

		ad_hoc_instrument_track = jammo_instrument_track_new (type);
		jammo_sequencer_add_track (ad_hoc_sequencer, JAMMO_TRACK (ad_hoc_instrument_track));

		//Track must be added to sequencer before changing it recording
		jammo_instrument_track_set_realtime (ad_hoc_instrument_track, TRUE);
	}


	jammo_sequencer_play (JAMMO_SEQUENCER (ad_hoc_sequencer)); //this is the non-recording ad-hoc-sequencer
	return;
}



