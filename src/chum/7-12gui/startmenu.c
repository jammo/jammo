/**sequencer.c is part of JamMo.
License: GPLv2, read more from COPYING

This file is for clutter based gui.
This is the sequencer.
 */
#include <tangle.h>
#include <glib-object.h>
#include "startmenu.h"
#include "../jammo.h"
#include "../jammo-game-level.h"
#include "sequencer.h"
#include "gamesmenu.h"
#include "communitymenu.h"

#include "../../cem/cem.h"
#include "../../configure.h"
#include <libintl.h> //For gettext

#include "../jammo-mentor.h"

static gboolean instructions_interrupted;
gboolean startmenu_goto_sequencer (TangleButton* tanglebutton, gpointer none){
	instructions_interrupted=TRUE;
	jammo_mentor_shut_up(jammo_mentor_get_default());
	gamesmenu_show(); //When level starts, it suppose that gamesmenu is also visible
	gamesmenu_start_first_task_on_this_level("level_sandbox_4loops");

	return TRUE;
}

gboolean startmenu_goto_gamesmenu (TangleButton* tanglebutton, gpointer none){
	instructions_interrupted=TRUE;
	jammo_mentor_shut_up(jammo_mentor_get_default());
	cem_add_to_log("From startmenu to gamesmenu clicked",J_LOG_USER_ACTION);
	gamesmenu_show();
	gamesmenu_start_instructions_for_level1();

	return TRUE;
}

gboolean startmenu_goto_communitymenu(TangleButton* tanglebutton, gpointer none){
	instructions_interrupted=TRUE;
	jammo_mentor_shut_up(jammo_mentor_get_default());
	cem_add_to_log("From startmenu to communitymenu clicked",J_LOG_USER_ACTION);
	start_communitymenu();

	return TRUE;
}

gboolean startmenu_mentor_clicked(TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("On startmenu mentor clicked",J_LOG_USER_ACTION);

	return TRUE;
}
static void startmenu_welcome_ready(){
	instructions_interrupted=TRUE;
	clutter_actor_show(jammo_get_actor_by_id("startmenu-mentor"));
	jammo_clutter_actor_flash(jammo_get_actor_by_id("startmenu-mentor"));
	clutter_actor_hide(CLUTTER_ACTOR(jammo_mentor_get_default()));
	clutter_actor_set_y(CLUTTER_ACTOR(jammo_mentor_get_default()), 0.0);
	clutter_actor_set_x(CLUTTER_ACTOR(jammo_mentor_get_default()), 0.0);
}

static void startmenu_welcomeE(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	startmenu_welcome_ready();
}

static void startmenu_welcomeD(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	if (instructions_interrupted) {
		startmenu_welcome_ready();
		return;
	}
	jammo_clutter_actor_flash(jammo_get_actor_by_id("start_community"));
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "here_community.spx", startmenu_welcomeE,NULL);
}

static void startmenu_welcomeC(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	if (instructions_interrupted) {
		startmenu_welcome_ready();
		return;
	}
	jammo_clutter_actor_flash(jammo_get_actor_by_id("start_sequencer"));
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "here_sequencer.spx", startmenu_welcomeD,NULL);
}

static void startmenu_welcomeB(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	if (instructions_interrupted) {
		startmenu_welcome_ready();
		return;
	}
	jammo_clutter_actor_flash(jammo_get_actor_by_id("start_games"));
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "here_games.spx", startmenu_welcomeC,NULL);
}

void startmenu_draw_version_information(TangleActor* actor, gpointer data){
	//Show version number: defined in configure.ac
	ClutterActor* version_label =  jammo_get_actor_by_id( "startmenu-version-info");
	clutter_text_set_font_name (CLUTTER_TEXT(version_label),"Luxi Mono 36");

	/* TRANSLATORS: This is used on startmenu to concatenate Version information e.g. Version-0.7.13 */
	gchar* version = g_strdup_printf("%s-%s",gettext("Version"),VERSION);
	clutter_text_set_text(CLUTTER_TEXT(version_label), version);
	g_free(version);

	instructions_interrupted=FALSE;
	clutter_actor_set_y(CLUTTER_ACTOR(jammo_mentor_get_default()), 30.0);
	clutter_actor_set_x(CLUTTER_ACTOR(jammo_mentor_get_default()), -9.0);
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "jammo_starts.spx", startmenu_welcomeB,NULL);
}

static void startmenu_quitB(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
		clutter_main_quit();
}

gboolean startmenu_quit (TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("startmenu_quit-called",J_LOG_USER_ACTION);
	clutter_actor_set_width(CLUTTER_ACTOR(jammo_mentor_get_default()),0.1);
	clutter_actor_set_height(CLUTTER_ACTOR(jammo_mentor_get_default()),0.1);
	instructions_interrupted=TRUE;
	jammo_mentor_shut_up(jammo_mentor_get_default());
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "jammo_quitted.spx", startmenu_quitB,NULL);

	return TRUE;
}

/*
Whenever any view want return back to startmenu, this function can be used
*/
gboolean startmenu_goto_startmenu (TangleButton* tanglebutton, gpointer none){
	cem_add_to_log("startmenu_goto_startmenu-called",J_LOG_USER_ACTION);
	tangle_view_unload(TANGLE_VIEW(jammo_get_actor_by_id("fullsequencer-view")));
	clutter_actor_hide(CLUTTER_ACTOR(jammo_mentor_get_default()));

	ClutterActor* mainview;
	mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);
	clutter_actor_show(jammo_get_actor_by_id("startmenu-view"));

	return TRUE;
}

