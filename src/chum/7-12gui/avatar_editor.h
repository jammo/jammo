/*
License: GPLv2, read more from COPYING

This file contains the functionality of the avatar editor.
 */

#ifndef AVATAR_EDITOR_H
#define AVATAR_EDITOR_H

#include <tangle.h>
#include "community_utilities.h"

/*
* Starts avatar editor.
*/
void start_avatar_editor();

void set_avatar(Avatar*, ClutterActor*);


gboolean sendJammoUserProfile(gchar* user_profile);

gboolean avatar_goto_communitymenu(TangleButton *tanglebutton, gpointer none);

gboolean avatar_goto_startmenu(TangleButton *tanglebutton, gpointer none);

void show_roster(gboolean editor);
void end_roster(gboolean editor);

#endif
