#include "record_cover_tool.h"
#include "../../configure.h"
#include "../../cem/cem.h"
#include <tangle.h>
#include <string.h>
#include "community_utilities.h"
#include "mysongs.h"

static ClutterContainer* imageList = 0;
static ClutterContainer* widgets = 0;
static ClutterActor* image = 0;
static ClutterActor* recordName = 0;
static ClutterActor* composerName = 0;
static TextField recordNameTextField;
static TextField composerNameTextField;
static const gchar* path;

/*
* Starts record cover tool. 
* Parameter filepath must be full path to file that contains existing record label.
*/
void start_record_cover_tool(const gchar* filepath){
		
	FILE* file;
	char coverInfo[200];

	path = filepath;

	gchar *instruments_path = g_strdup_printf("%s/communitymenu/INSTRUMENTS/", DATA_DIR); 

	cem_add_to_log("Starting record cover tool", J_LOG_DEBUG);
	
	tangle_actor_hide_animated(TANGLE_ACTOR(jammo_get_actor_by_id("mysongs-view")));
	
	ClutterActor* cover_view = jammo_get_actor_by_id("record-cover-view");
				

	if(cover_view){
		clutter_actor_show(cover_view);

		clutter_text_set_text(CLUTTER_TEXT(jammo_get_actor_by_id("record-name-info")), _("RECORD NAME:"));
		clutter_text_set_text(CLUTTER_TEXT(jammo_get_actor_by_id("composer-name-info")), _("COMPOSER:"));
		clutter_text_set_text(CLUTTER_TEXT(jammo_get_actor_by_id("record-cover-images-label")), _("CHOOSE IMAGE:"));

		imageList = CLUTTER_CONTAINER(jammo_get_actor_by_id("record-cover-images-list"));
		widgets = CLUTTER_CONTAINER(jammo_get_actor_by_id("record-cover-widgets"));

		recordName = jammo_get_actor_by_id("record-name-text");
		composerName = jammo_get_actor_by_id("composer-name-text");
		recordNameTextField.text = jammo_get_actor_by_id("record-name-textfield");
		composerNameTextField.text = jammo_get_actor_by_id("composer-name-textfield");	
		
		recordNameTextField.max_characters = 100;
		composerNameTextField.max_characters = 100;
		recordNameTextField.lineCount = 1;	
		composerNameTextField.lineCount = 1;
		
	
		recordNameTextField.handler_id = g_signal_connect_swapped(recordNameTextField.text, "text-changed", G_CALLBACK(community_utilities_limit_line_count), &recordNameTextField);
		composerNameTextField.handler_id = g_signal_connect_swapped(composerNameTextField.text, "text-changed", G_CALLBACK(community_utilities_limit_line_count), &composerNameTextField);
	
		g_signal_connect(recordNameTextField.text, "text-changed", G_CALLBACK(change_record_name), NULL);
		g_signal_connect(composerNameTextField.text, "text-changed", G_CALLBACK(change_composer_name), NULL);		
				
		if ((file = fopen(filepath, "r")) != NULL){
		
			if(fread(coverInfo, sizeof(char), 200, file) != 0){
				
			clutter_text_set_text(CLUTTER_TEXT(recordNameTextField.text), strtok(coverInfo, ","));
			
			if(strcmp(clutter_text_get_text(CLUTTER_TEXT(recordNameTextField.text)), "0") == 0)
				clutter_text_set_text(CLUTTER_TEXT(recordNameTextField.text), "");

			clutter_text_set_text(CLUTTER_TEXT(composerNameTextField.text), strtok(NULL, ","));
			
			if(strcmp(clutter_text_get_text(CLUTTER_TEXT(composerNameTextField.text)), "0") == 0)
				clutter_text_set_text(CLUTTER_TEXT(composerNameTextField.text), "");
							
			char *temp = strtok(NULL, ",");
			if(strcmp(temp, "0") != 0){
				image = tangle_texture_new(temp);
				clutter_actor_set_name(image, temp);
				clutter_actor_set_position(image, 35, 35);
				clutter_actor_set_size(image, 150, 150);
				clutter_container_add_actor(widgets, image);
				}	
			}
			
			fclose(file);
		}
			
		community_utilities_make_buttons_from_directory(instruments_path, imageList, G_CALLBACK(record_cover_change_image), 100, 100);
	}
}

/*
* For cleanupping.
*/
void end_record_cover_tool(){

	
	DESTROY_ACTOR(image);
	clutter_text_set_text(CLUTTER_TEXT(recordNameTextField.text), "");
	clutter_text_set_text(CLUTTER_TEXT(composerNameTextField.text), "");

	tangle_actor_hide_animated(TANGLE_ACTOR(jammo_get_actor_by_id("record-cover-view")));
	clutter_actor_show(jammo_get_actor_by_id("mysongs-view"));


}

/*
* Callback function that changes record label. 
* Parameter button must have full filepath to the image it is representing.
*/
void record_cover_change_image(TangleButton *button){

	if(image != 0)
		clutter_container_remove_actor(widgets, image);

	image = tangle_texture_new(clutter_actor_get_name(CLUTTER_ACTOR(button)));
	
	if(image != 0){
		clutter_actor_set_position(image, 35, 35);
		clutter_actor_set_size(image, 150, 150);
		clutter_actor_set_name(image, clutter_actor_get_name(CLUTTER_ACTOR(button)));
		clutter_container_add_actor(widgets, image);
	}

}

/*
* Handles ok button clicks.
*/
gboolean record_cover_ok_pressed(TangleActor* actor, gpointer user_data) {
	FILE* file;
	gchar *data;
	
	if((file = fopen(path, "w")) != NULL){
		
		data =  g_strdup_printf("%s,%s,%s,",
			strlen(clutter_text_get_text(CLUTTER_TEXT(recordName))) == 0 ? "0" : clutter_text_get_text(CLUTTER_TEXT(recordName)),
			strlen(clutter_text_get_text(CLUTTER_TEXT(composerName))) == 0 ? "0" : clutter_text_get_text(CLUTTER_TEXT(composerName)),
			image != NULL ? clutter_actor_get_name(image) : "0");
						 	
		fwrite(data, sizeof(char), strlen(data), file);
		
		g_free(data);
		fclose(file);
	}

	mysongs_update_label_button(path);

	end_record_cover_tool();
	return TRUE;
}

gboolean record_cover_cancel_pressed(TangleActor* actor, gpointer data){
	end_record_cover_tool();
	return TRUE;
}

/*
* Changes record label name
*/
void change_record_name(gpointer text){

		clutter_text_set_text(CLUTTER_TEXT(recordName), clutter_text_get_text(text));

}

/*
* Changes composer name
*/
void change_composer_name(gpointer text){

		clutter_text_set_text(CLUTTER_TEXT(composerName), clutter_text_get_text(text));

}




