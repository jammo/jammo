/**sequencer_track_view.c is part of JamMo.
License: GPLv2, read more from COPYING

Call this: sequencer_track_view_tune_width
 */
#include <glib-object.h>
#include <clutter/clutter.h>
#include <string.h>
#include <stdlib.h>
#include <tangle.h>


#include "../../meam/jammo-track.h"
#include "../../meam/jammo-editing-track.h"
#include "../../meam/jammo-instrument-track.h"
#include "../../meam/jammo-slider-track.h"
#include "../../meam/jammo-backing-track.h"
#include "../../meam/jammo-midi.h"
#include "../../meam/jammo-meam.h" //JAMMO_DURATION_INVALID

#include "../jammo-track-view.h"
#include "../jammo-editing-track-view.h"
#include "../jammo-miditrack-view.h"
#include "../jammo.h"
#include "../jammo-cursor.h"
#include "../file_helper.h"
#include "../jammo-mentor.h"

#include "sequencer_track_view.h"
#include "sequencer.h"
#include "midi_editor.h"
#include "instrument_gui.h"
#include "jammer_view.h"
#include "startmenu.h"

#include "backingtrack_selection.h"
#include "../../cem/cem.h"
static int width_of_slot = 40;




//We get these as parameters
static int number_of_slots = -1;
static guint64 duration_of_the_track = 0; //This is saved on files.

gboolean pressed_sequencer_button_mute (TangleButton *tanglebutton, gpointer user_data);
gboolean pressed_sequencer_button_volume (TangleButton *tanglebutton, gpointer user_data);
gboolean pressed_sequencer_button_track_label (TangleButton *tanglebutton, gpointer data);

//We want toggle this up, when next is clicked
TangleButton* last_pressed_volume_button = NULL;






gboolean sequencer_track_view_start_backingtrack_selection (ClutterActor *track_view, ClutterEvent *event, gpointer user_data) {
	cem_add_to_log(" Backing_track pressed",J_LOG_USER_ACTION);
	clutter_actor_show(jammo_get_actor_by_id("backingtrack-selection-view"));
	clutter_actor_hide(jammo_get_actor_by_id("fullsequencer-view"));
	backingtrack_selection_set_track_view(CLUTTER_ACTOR(track_view));

	return TRUE;
}

/* DEPRECATED: track label is not used on clickable button, but label is background for mute+volume*/
gboolean pressed_sequencer_button_track_label (TangleButton *tanglebutton, gpointer data) {
	ClutterActor* control_panel_for_this_track = clutter_actor_get_parent(CLUTTER_ACTOR(tanglebutton));
	const gchar* name = clutter_actor_get_name(CLUTTER_ACTOR(control_panel_for_this_track));
	TangleWidget* track_view=NULL;


	ClutterActor* container = jammo_get_actor_by_id("fullsequencer-container-for-tracks");
	TangleActorIterator actor_iterator;
	ClutterActor* child;
	
	for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(container), &actor_iterator); (child = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
		const gchar* name2 = clutter_actor_get_name(child);
		if (strcmp(name,name2)==0){
			track_view=TANGLE_WIDGET(child);
		}
	}

	if (track_view==NULL) {
		gchar* message = g_strdup_printf(" Not found track named '%s' for label-pressed",name);
		cem_add_to_log(message,J_LOG_FATAL);
		g_free(message);
		return FALSE;
	}

	cem_add_to_log("Pressed track label",J_LOG_USER_ACTION);
	const gchar* name_of_track_view = clutter_actor_get_name(CLUTTER_ACTOR(track_view));
	if (JAMMO_IS_MIDITRACK_VIEW(track_view))
	{
		cem_add_to_log("It is instrument track",J_LOG_USER_ACTION);
		tangle_actor_hide_animated(TANGLE_ACTOR(jammo_mentor_get_default()));
		//We can start realtime jamming or midi-editor.
		//Midi-editor backs always to sequencer.
		//From Jamming we can go back to sequencer or reach editor
		//TODO: some way to go straight to editor.

		//jamming
		instrument_gui_start(JAMMO_MIDITRACK_VIEW(track_view), NULL); //NULL means no slider

		//editor
		//JammoSequencer* sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));
		//midi_editor_start_with_miditrack_view(JAMMO_MIDITRACK_VIEW(track_view), sequencer);
	}
	else if (JAMMO_IS_TRACK_VIEW(track_view))
	{
		//printf("It is slider track or backing-track\n");
		TangleWidget* container2 = TANGLE_WIDGET(track_view);
		JammoTrack* track;
		g_object_get(track_view,"track",&track,NULL);
		if (JAMMO_IS_SLIDER_TRACK(track)) {
			cem_add_to_log(" Slider_track pressed",J_LOG_USER_ACTION);
			tangle_actor_hide_animated(TANGLE_ACTOR(jammo_mentor_get_default()));
			instrument_gui_start(NULL, container2); //NULL means no keyboard, but slider
		}
		else if (JAMMO_IS_BACKING_TRACK(track)){
			if (name_of_track_view && strcmp(name_of_track_view,"singing")==0)
				{
				cem_add_to_log(" Singing track pressed",J_LOG_USER_ACTION);
				clutter_actor_show(jammo_get_actor_by_id("singing-view"));
				clutter_actor_hide(jammo_get_actor_by_id("fullsequencer-view"));
				}
			else if (name_of_track_view && strcmp(name_of_track_view,"BackingTrack")==0)
			{
			cem_add_to_log(" Backing_track pressed",J_LOG_USER_ACTION);
			clutter_actor_show(jammo_get_actor_by_id("backingtrack-selection-view"));
			clutter_actor_hide(jammo_get_actor_by_id("fullsequencer-view"));
			backingtrack_selection_set_track_view(CLUTTER_ACTOR(track_view));
			}
		}

	}
	else if  (JAMMO_IS_EDITING_TRACK_VIEW(track_view)){
			cem_add_to_log(" Editing_track pressed",J_LOG_USER_ACTION);
			if (name_of_track_view && strcmp(name_of_track_view,"jammer")==0)
				{
				cem_add_to_log("  Jammer_track pressed",J_LOG_USER_ACTION);
				tangle_actor_hide_animated(TANGLE_ACTOR(jammo_mentor_get_default()));
				clutter_actor_show(jammo_get_actor_by_id("jammer-view"));
				clutter_actor_hide(jammo_get_actor_by_id("fullsequencer-view"));
				start_jammer();}
			else 
				{
				cem_add_to_log("  Do nothing",J_LOG_USER_ACTION);
				}
		}
	return TRUE;
}


/*JSON callback. TODO
*/
gboolean fullsequencer_mentor_clicked (TangleButton *tanglebutton, gpointer data)   {
	cem_add_to_log("fullsequencer_mentor_clicked",J_LOG_USER_ACTION);
	return TRUE;
}


gboolean pressed_sequencer_button_mute (TangleButton *tanglebutton, gpointer data)   {
	ClutterActor* control_panel_for_this_track = clutter_actor_get_parent(CLUTTER_ACTOR(tanglebutton));
	const gchar* name = clutter_actor_get_name(CLUTTER_ACTOR(control_panel_for_this_track));
	gchar* message = g_strdup_printf("mute clicked '%s'",name);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);

	TangleWidget* track_view=NULL;

	ClutterActor* container = jammo_get_actor_by_id("fullsequencer-container-for-tracks");
	TangleActorIterator actor_iterator;
	ClutterActor* child;
	
	for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(container), &actor_iterator); (child = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
		const gchar* name2 = clutter_actor_get_name(child);
		if (strcmp(name,name2)==0){
			track_view=TANGLE_WIDGET(child);
		}
	}

	if (track_view==NULL) {
		message = g_strdup_printf(" Not found track named '%s' for muting",name);
		cem_add_to_log(message,J_LOG_FATAL);
		g_free(message);
		return FALSE;
	}

	JammoPlayingTrack* jammotrack=NULL;
	//slider/backingtrack is inside of generic jammo_track_view, miditrack is inside of miditrack-view  and loop-track inside editing-track-view
	if (JAMMO_IS_EDITING_TRACK_VIEW(track_view) || JAMMO_IS_MIDITRACK_VIEW(track_view)  || JAMMO_IS_TRACK_VIEW(track_view) ) {
		g_object_get(track_view, "track", &jammotrack, NULL);
	} else  {
		return FALSE;
	}

	if (jammotrack)
		jammo_playing_track_set_muted(JAMMO_PLAYING_TRACK(jammotrack), tangle_button_get_selected(tanglebutton));

	return TRUE;
}

/*
When track is created from saved projects and it has starting volume, this function is called.
Also when user moves volume-slider, this is called.
*/
static void set_track_volume_and_button_texture(JammoPlayingTrack* jammotrack, TangleButton* volume_button, gfloat new_volume) {
	//printf("changing volume to %f\n",new_volume);

	//new_volume is float and can be -0 (negative zero). -0 = +0. But it looks weird on logs.
	if (new_volume==0)
		new_volume=0.0;

	jammo_playing_track_set_volume(jammotrack,new_volume);

	ClutterActor* control_panel_for_this_track = clutter_actor_get_parent(CLUTTER_ACTOR(volume_button));
	const gchar* name_of_track = clutter_actor_get_name(CLUTTER_ACTOR(control_panel_for_this_track));

	gchar* message = g_strdup_printf("Volume changed to %f for track '%s' ",new_volume,name_of_track);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);

	//Change texture of volume-button
	ClutterActor* container = tangle_button_get_normal_background_actor(volume_button);
	ClutterActor* nub = tangle_widget_get_nth_child(TANGLE_WIDGET(container),0);

	ClutterActor* container2 = tangle_button_get_selected_background_actor(volume_button);
	ClutterActor* nub2 = tangle_widget_get_nth_child(TANGLE_WIDGET(container2),0);

	gfloat height_ = 36; //clutter_actor_get_height(container) - clutter_actor_get_height(nub); //If hided. container reports it's height wrongly
	//printf("height_: %f\n",height_);

	clutter_actor_set_y(nub,height_-new_volume*height_);
	clutter_actor_set_y(nub2,height_-new_volume*height_);
}


/**
This syncs scrollers offset to tracks volume.
*/
gboolean sequencer_volumebar_used (ClutterAction* scroll_action, gpointer none){
	//printf("sequencer_volumebar_used\n");
	if (!last_pressed_volume_button)
		return FALSE;


	ClutterActor* volume_scroller = clutter_actor_meta_get_actor(CLUTTER_ACTOR_META(scroll_action));
	JammoPlayingTrack* jammotrack = g_object_get_data(G_OBJECT(volume_scroller),"track");

	gfloat height = clutter_actor_get_height(CLUTTER_ACTOR(volume_scroller))-50;

	gfloat offset;
	tangle_scroll_action_get_offsets(TANGLE_SCROLL_ACTION(scroll_action), NULL,&offset);



	gfloat new_volume = offset/height; //This is between [0...1]
	//If volume is changed
		if (jammo_playing_track_get_volume(jammotrack) != new_volume)
			set_track_volume_and_button_texture(jammotrack,last_pressed_volume_button,new_volume);


	return FALSE;
}


/*
 When volume-slider is visible stage is capturing all events.
 Next release-event will end capturing.
 */
static gboolean stage_capture_events (ClutterActor* actor, ClutterEvent* event, gpointer user_data)  {
	if (event->type == CLUTTER_BUTTON_RELEASE){
		g_signal_handlers_disconnect_by_func(actor, stage_capture_events,NULL);

		//Hide volume-slider
		ClutterActor* volume_container = jammo_get_actor_by_id("fullsequencer-volumebar-container");
		clutter_actor_hide(volume_container);

		//De-select pressed volume-button.
		tangle_button_set_selected(last_pressed_volume_button,FALSE);
		last_pressed_volume_button=NULL;
	}

	return FALSE;
}



/**
Each tracks share one volumebar.
  data is track_view when saved game is loaded (and actors are created from c)
  else name of buttons parent (=control-panel-widget) is used for jammo_get_actor_by_id (actors created from json)
*/
gboolean pressed_sequencer_button_volume (TangleButton *tanglebutton, gpointer data) {
	ClutterActor* stage = clutter_stage_get_default();
	g_signal_connect(stage, "captured-event", G_CALLBACK(stage_capture_events), NULL);

	ClutterActor* control_panel_for_this_track = clutter_actor_get_parent(CLUTTER_ACTOR(tanglebutton));
	const gchar* name = clutter_actor_get_name(CLUTTER_ACTOR(control_panel_for_this_track));
	gchar* message = g_strdup_printf("volume clicked '%s'",name);
	cem_add_to_log(message,J_LOG_USER_ACTION);
	g_free(message);

	TangleWidget* track_view=NULL;

	ClutterActor* container = jammo_get_actor_by_id("fullsequencer-container-for-tracks");
	TangleActorIterator actor_iterator;
	ClutterActor* child;
	
	for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(container), &actor_iterator); (child = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
		const gchar* name2 = clutter_actor_get_name(child);
		if (strcmp(name,name2)==0){
			track_view=TANGLE_WIDGET(child);
		}
	}

	if (track_view==NULL) {
		message = g_strdup_printf(" Not found track named '%s'",name);
		cem_add_to_log(message,J_LOG_FATAL);
		g_free(message);
		return FALSE;
	}

	ClutterActor* volume_container = jammo_get_actor_by_id("fullsequencer-volumebar-container");
	ClutterActor* volume_scroller = jammo_get_actor_by_id("fullsequencer-volumebar-scroller");
	ClutterAction* scroll_action = tangle_actor_get_action_by_type(volume_scroller,TANGLE_TYPE_SCROLL_ACTION);

	JammoPlayingTrack* jammotrack=NULL;
	//slider/backingtrack is inside of generic jammo_track_view, miditrack is inside of miditrack-view  and loop-track inside editing-track-view
	if (JAMMO_IS_EDITING_TRACK_VIEW(track_view) || JAMMO_IS_MIDITRACK_VIEW(track_view)  || JAMMO_IS_TRACK_VIEW(track_view) ) {
		g_object_get(track_view, "track", &jammotrack, NULL);
	} else  {
		return FALSE;
	}

	gfloat current_volume = jammo_playing_track_get_volume(jammotrack);
	//printf("volume of this track is :%f\n",current_volume);


	//Hide last pressed and put new value to volumebar from track
	clutter_actor_show(volume_container);
	//-50 is height of the visible marker.
	gfloat new_place = (clutter_actor_get_height(CLUTTER_ACTOR(volume_scroller))-50)*current_volume;
	//printf("new place: %f\n",new_place);
	//Disconnect notify::offset-y for a while: we are making to change offset-y
	g_signal_handlers_disconnect_by_func(scroll_action, sequencer_volumebar_used,NULL);
	g_object_set(scroll_action, "offset-y", new_place, NULL);
	g_signal_connect(scroll_action, "notify::offset-y", G_CALLBACK(sequencer_volumebar_used), NULL);
	g_object_set_data(G_OBJECT(volume_scroller),"track",jammotrack);
	if (last_pressed_volume_button)
		tangle_button_set_selected(last_pressed_volume_button,FALSE);
	last_pressed_volume_button=tanglebutton;

	return FALSE;
}




//JSON callback
void fullsequencer_on_cursor_notify_x(GObject* object, GParamSpec* param_spec, gpointer user_data) {
	ClutterAction* scroll_action;
	gfloat x, visible, offset, max;

	ClutterActor* scrolling_view = jammo_get_actor_by_id("fullsequencer-container-for-tracks-outer");
	scroll_action = tangle_actor_get_action_by_type(CLUTTER_ACTOR(scrolling_view),TANGLE_TYPE_SCROLL_ACTION);

	g_object_get(object, "x", &x, NULL);
	g_object_get(scroll_action, "page-length-x", &visible, "offset-x", &offset, "max-offset-x", &max, NULL);
	gfloat old_offset = offset;

	if (x < offset) { //Cursor is out of view somewhere too left
		offset = x - visible + 360.0;
	} else if (x > offset + visible - 80.0) { //Cursor is reaching right edge and there are need for scroll container
		offset += 360.0;
	}

	//Check Min and Max boundaries:
	if (offset < 0.0) offset = 0.0;
	if (offset > max) offset = max;

	//If there are no reason, do not do tangle_object_animate
	if (offset != old_offset)
		tangle_object_animate(G_OBJECT(scroll_action),  CLUTTER_EASE_IN_OUT_QUAD, 250, "offset-x", offset, NULL);
}

/***************************
Functions for json
**************************/

gboolean fullsequencer_play_clicked (TangleButton* tanglebutton, gpointer none){
	const gchar* message;
	if (tangle_button_get_selected(tanglebutton))
		message = "fullsequencer: Play-Button pressed";
	else
		message = "fullsequencer: Stop-Button pressed";

	cem_add_to_log(message,J_LOG_USER_ACTION);

	//Button is (tangle-)binded to sequencer, so there are nothing to do.
	return TRUE;
}


/*
Play-button is made by template. When it is taken in use, this function is called.
*/
void fullsequencer_play_loaded (TangleButton* tanglebutton, gpointer none){
	//printf("fullsequencer: Play-Stop-Button LOADED\n");
	g_signal_handlers_disconnect_by_func(tanglebutton, fullsequencer_play_loaded, NULL);
	JammoSequencer* sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));

 	//TangleBinding* binding;
 	//binding =
	tangle_binding_new(G_OBJECT(tanglebutton), "selected", G_OBJECT(sequencer), "play"); //this is never free'ed!
}



gboolean fullsequencer_community_clicked (TangleButton* tanglebutton, gpointer none){
	printf("fullsequencer: Community pressed. Doesn't do anything\n");
	return FALSE;
}




/*
Also prepare cursor.
*/

void sequencer_track_view_set_duration(guint64 duration) {
	gchar* message = g_strdup_printf("sequencer_track_view_set_duration: %" G_GUINT64_FORMAT "", duration);
	cem_add_to_log(message,J_LOG_DEBUG);
	g_free(message);

	JammoSequencer* sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));

	gint tempo_factory = 4;
	//We want operate whole time with guint64.
	guint64 temp=60*tempo_factory;
	guint64 one_second = 1000000000L;
	guint64 big = temp * one_second;
	guint64 duration_of_one_bar = big / jammo_sequencer_get_tempo(sequencer);
	guint64 duration_of_one_beat = duration_of_one_bar/4;
	gint n_slots = duration/duration_of_one_beat;
	//printf("*number_of_slots (in backing-track / for editing-track): %d\n",n_slots);


	//Set static
	number_of_slots = n_slots;
	duration_of_the_track = duration;

	//Set slot-duration for editing-track-views
	//This is used when level is started from json.
	ClutterActor* container = jammo_get_actor_by_id("fullsequencer-container-for-tracks");
	TangleActorIterator actor_iterator;
	ClutterActor* child;

	float slot_width;
	for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(container), &actor_iterator); (child = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
		if (JAMMO_IS_EDITING_TRACK_VIEW(child)) {
			g_object_set(child,"slot-duration",duration_of_one_beat,NULL);
			g_object_set(child,"n-slots",number_of_slots,NULL);
			g_object_get(child,"slot-width",&slot_width,NULL); //float
		}
	}

	//tune static variable
	width_of_slot=slot_width;

	sequencer_track_view_fix_cursor();
}





//Loading saved games
static int track_height=50;

//Creates control-panel for given track_view.
static void create_control_panel_entry(ClutterActor* track_view_actor, ClutterActor* label, const gchar* name_of_track_view, gboolean muted, gfloat volume) {

	ClutterActor* panel_widget = tangle_widget_new();
	tangle_widget_set_layout(TANGLE_WIDGET(panel_widget), tangle_box_layout_new());
	//default direction is good: "direction": "ltr-ttb"

	//This is only for log-messages
	g_object_set(panel_widget, "name",name_of_track_view, NULL);

	//Label as background
	tangle_widget_set_background_actor(TANGLE_WIDGET(panel_widget),label);

	//Mute
	ClutterActor* mute_button = tangle_button_new_selectable_with_background_actors (
       tangle_texture_new("/opt/jammo/sequencer/button_mute.png"), /* normal */
       tangle_texture_new("/opt/jammo/sequencer/button_mute_pressed.png"),  /* interactive */
       tangle_texture_new("/opt/jammo/sequencer/button_mute_pressed.png")); /* selected */
	tangle_button_set_selected(TANGLE_BUTTON(mute_button),muted);

	g_signal_connect(mute_button, "clicked", G_CALLBACK(pressed_sequencer_button_mute), track_view_actor);
	tangle_widget_add(TANGLE_WIDGET(panel_widget),mute_button,NULL);

	//Volume-button
	// It is toggle-button. Both states are tangle_widgets with background+nub
	ClutterActor* volume_background = tangle_texture_new("/opt/jammo/sequencer/small_volume_background.png");
	ClutterActor* volume_background2 = tangle_texture_new("/opt/jammo/sequencer/small_volume_background.png");
	ClutterActor* volume_background3 = tangle_texture_new("/opt/jammo/sequencer/small_volume_background.png");
	ClutterActor* volume_nub = tangle_texture_new("/opt/jammo/sequencer/small_volume_button.png");
	ClutterActor* volume_nub_selected = tangle_texture_new("/opt/jammo/sequencer/small_volume_button_selected.png");

	ClutterActor* volume_button_idle = tangle_widget_new();
	ClutterActor* volume_button_selected = tangle_widget_new();
	ClutterActor* volume_button_interactive = tangle_widget_new();

	g_object_set(volume_button_idle, "background-actor", volume_background, NULL);
	g_object_set(volume_button_selected, "background-actor", volume_background2,NULL);
	g_object_set(volume_button_interactive, "background-actor", volume_background3,NULL);

	tangle_widget_add(TANGLE_WIDGET(volume_button_idle),volume_nub,NULL);
	tangle_widget_add(TANGLE_WIDGET(volume_button_selected),volume_nub_selected,NULL);

	ClutterActor* volume_button = tangle_button_new_selectable_with_background_actors (
       volume_button_idle,/* normal */
       volume_button_interactive,  /* interactive */
       volume_button_selected); /* selected */

	g_signal_connect(volume_button, "clicked", G_CALLBACK(pressed_sequencer_button_volume), track_view_actor);
	tangle_widget_add(TANGLE_WIDGET(panel_widget),volume_button,NULL);


	ClutterActor* container;
	container = jammo_get_actor_by_id("fullsequencer-track-control-panel");
	clutter_container_add_actor(CLUTTER_CONTAINER(container),panel_widget);
	clutter_actor_set_height(container,clutter_actor_get_height(container)+track_height);

	JammoPlayingTrack* jammotrack;
	g_object_get(track_view_actor,"track",&jammotrack,NULL);
	if (jammotrack) {
		//volume
		set_track_volume_and_button_texture(jammotrack, TANGLE_BUTTON(volume_button),volume);
		//mute-state
		jammo_playing_track_set_muted(JAMMO_PLAYING_TRACK(jammotrack), muted);
	}
	else {
		cem_add_to_log("No audio-track inside saved track-view",J_LOG_FATAL);
	}
}



ClutterActor* sequencer_track_view_create_editing_track(gboolean muted, gfloat volume, gint e_type, gint slot_width, const gchar* name_of_track_view) {
	JammoSequencer* sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));
	guint64 duration_of_one_slot;

	if(number_of_slots == 0)
		duration_of_one_slot = 0;
	else	
		duration_of_one_slot = duration_of_the_track / number_of_slots; //these  are static variables

	ClutterActor *label;
	const gchar* name_of_label_button;
	JammoTrack* track;

	//MEAM-track
	track = JAMMO_TRACK(jammo_editing_track_new());
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(track));

	if (slot_width==-1)
		slot_width=width_of_slot; //static default value

	//CHUM-track-view
	ClutterActor* track_view_actor = jammo_editing_track_view_new(JAMMO_EDITING_TRACK(track),
          number_of_slots, duration_of_one_slot, slot_width, track_height);
	clutter_actor_set_reactive(track_view_actor, TRUE);
	g_object_set(track_view_actor, "line-every-nth-slot", 4, "sample-type", e_type,
        "name", name_of_track_view,
        "reactive", TRUE,
        NULL);
	//logging of this track:
	g_signal_connect(track_view_actor, "show-completed", G_CALLBACK(fullsequencer_trackview_loaded), NULL);

	//Add track-view to container
	ClutterActor* container = jammo_get_actor_by_id("fullsequencer-container-for-tracks");
	clutter_container_add_actor(CLUTTER_CONTAINER(container),track_view_actor);
	clutter_actor_set_height(container,clutter_actor_get_height(container)+(float)track_height);


	//Choose label-image and set color of track
	ClutterColor sample_color =  { 0, 0, 0, 255 };
	if (e_type == JAMMO_SAMPLE_RHYTMICAL) {
		name_of_label_button = "sequencer/label_loop_rhythm.png";
		sample_color.red   = 255;
		sample_color.green = 155;
		sample_color.blue  = 0;
	}
	else if (e_type == JAMMO_SAMPLE_MELODICAL) {
		name_of_label_button = "sequencer/label_loop_melody.png";
		sample_color.red   = 0;
		sample_color.green = 0;
		sample_color.blue  = 155;
	}
	else if (e_type == JAMMO_SAMPLE_HARMONICAL) {
		name_of_label_button = "sequencer/label_loop_harmony.png";
		sample_color.red   = 200;
		sample_color.green = 0;
		sample_color.blue  = 0;
	}
	else if (e_type == JAMMO_SAMPLE_EFFECT) {
		name_of_label_button = "sequencer/label_loop_effect.png";
		sample_color.red    = 0;
		sample_color.green  = 155;
		sample_color.blue   = 0;
	}
	else {//Some sane default value
		name_of_label_button = "sequencer/label_loop_effect.png";
		sample_color.alpha   = 0;
	}

	g_object_set(track_view_actor, "sample-color", &sample_color, NULL);

	ClutterColor background_color = { sample_color.red, sample_color.green, sample_color.blue, 68};
	g_object_set(track_view_actor, "background-color", &background_color, NULL);

	gchar* file_for_label_button = tangle_lookup_filename(name_of_label_button);
	label = tangle_button_new_with_background_actor(tangle_texture_new(file_for_label_button));
	g_free(file_for_label_button);
	g_signal_connect(label, "clicked", G_CALLBACK(pressed_sequencer_button_track_label), track_view_actor);

	create_control_panel_entry(track_view_actor, label, name_of_track_view, muted, volume);

	//tune static variable
	width_of_slot=slot_width;

	//tune cursor
	sequencer_track_view_fix_cursor();

	return track_view_actor;
}

/*
  This set width of the cursor (=width of the container it is moving)
  and height of the cursor texture
*/
void sequencer_track_view_fix_cursor(){
	TangleActorIterator actor_iterator;
	TangleActorIterator actor_iterator2;
	ClutterActor* child;
	ClutterActor* child2;
	ClutterActor*	container = jammo_get_actor_by_id("fullsequencer-container-for-tracks-outer");
	for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(container), &actor_iterator); (child = tangle_actor_iterator_get_actor(&actor_iterator)); tangle_actor_iterator_next(&actor_iterator)) {
		if (JAMMO_IS_CURSOR(child)) {
			gfloat width_of_area = number_of_slots*width_of_slot;
			clutter_actor_set_width(child, width_of_area); //width of cursor means total area for cursor to move
			ClutterActor* texture_container=NULL;
			g_object_get(child,"texture", &texture_container,NULL); //We know this cursor's texture is container
			for (tangle_widget_initialize_actor_iterator(TANGLE_WIDGET(texture_container), &actor_iterator2); (child2 = tangle_actor_iterator_get_actor(&actor_iterator2)); tangle_actor_iterator_next(&actor_iterator2)) {
				if (CLUTTER_IS_RECTANGLE(child2)) { //This is the resizable part of the texture
					clutter_actor_set_height(child2, clutter_actor_get_height(jammo_get_actor_by_id("fullsequencer-container-for-tracks"))-track_height);
				}
			}
		}
	}
}

ClutterActor* sequencer_track_view_create_backing_track(gboolean muted, gfloat volume, const gchar* audio, const gchar* image, const gchar* name_of_track_view) {
	JammoSequencer* sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));

	ClutterActor *label;
	gchar* name_of_label_button = tangle_lookup_filename("sequencer/label_backingtrack.png");
	JammoTrack* track;
	ClutterActor* track_view_actor;

	//MEAM-track
	track = JAMMO_TRACK(jammo_backing_track_new(audio));
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(track));

	//CHUM-track-view
	ClutterActor* back_actor = tangle_texture_new(image);
	g_object_set(back_actor,"repeat-x",true,"height",50.0,NULL);

	track_view_actor = jammo_track_view_new(track);
	tangle_widget_set_background_actor(TANGLE_WIDGET(track_view_actor),back_actor);

	g_object_set(track_view_actor, "name", name_of_track_view, NULL);

	//Add track-view to container
	ClutterActor* container = jammo_get_actor_by_id("fullsequencer-container-for-tracks");
	clutter_container_add_actor(CLUTTER_CONTAINER(container),track_view_actor);
	clutter_actor_set_height(container,clutter_actor_get_height(container)+(float)track_height);


	label = tangle_button_new_with_background_actor(tangle_texture_new(name_of_label_button));
	g_free(name_of_label_button);

	create_control_panel_entry(track_view_actor, label, name_of_track_view, muted, volume);

	//Long click on backingtrack => backingtrack_selection
	clutter_actor_set_reactive(track_view_actor,TRUE);
	ClutterAction* action = tangle_hold_action_new();
	g_signal_connect_swapped(action, "held", G_CALLBACK(sequencer_track_view_start_backingtrack_selection), track_view_actor);
	clutter_actor_add_action(track_view_actor, action);

	return track_view_actor;
}


ClutterActor* sequencer_track_view_create_instrument_track(gboolean muted, gfloat volume, gint instrument_type, const gchar* name_of_track_view) {
	JammoSequencer* sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));
	guint64 duration_of_tracks = jammo_sequencer_get_duration(sequencer);
	//printf("Making editing track (duration: %" G_GUINT64_FORMAT ")\n", duration_of_tracks);

	guint64 duration_of_one_slot = duration_of_tracks / number_of_slots;
	ClutterActor *label;
	const gchar* name_of_label_button;
	JammoTrack* track;
	ClutterActor* track_view_actor;

	//MEAM-track
	track = JAMMO_TRACK(jammo_instrument_track_new(instrument_type));
	jammo_sequencer_add_track(sequencer, JAMMO_TRACK(track));

	// TODO set transpose. Key of the song should be stored somewhere
	//instrument_gui_set_transpose('c'); This might be useless?

	//CHUM-track-view
	//TODO: we need to know highest_note and lowest_note for this instrument!
	track_view_actor = jammo_miditrack_view_new(JAMMO_INSTRUMENT_TRACK(track),
				number_of_slots*4,       //Miditrack-view uses more narrow slots
				duration_of_one_slot/4,
				width_of_slot, track_height, //height can be anything, we zoom it
				59,36);  //Highest-note and lowest-note. TODO: should be asked from instrumet
	jammo_miditrack_view_zoom_to_fit(JAMMO_MIDITRACK_VIEW(track_view_actor), number_of_slots*width_of_slot,track_height);
	jammo_miditrack_view_set_show_grid(JAMMO_MIDITRACK_VIEW(track_view_actor),FALSE);

	//Add track-view to container
	ClutterActor* container = jammo_get_actor_by_id("fullsequencer-container-for-tracks");
	clutter_container_add_actor(CLUTTER_CONTAINER(container),track_view_actor);
	clutter_actor_set_height(container,clutter_actor_get_height(container)+(float)track_height);

	if (instrument_type==1) //Drum
		name_of_label_button = "/opt/jammo/sequencer/label_drum.png";
	else
		name_of_label_button = "/opt/jammo/sequencer/label_keyboard.png";

	label = tangle_button_new_with_background_actors (
       tangle_texture_new(name_of_label_button), /* normal */
       tangle_texture_new(name_of_label_button));  /* interactive TODO*/
	g_signal_connect(label, "clicked", G_CALLBACK(pressed_sequencer_button_track_label), track_view_actor);


	create_control_panel_entry(track_view_actor, label, name_of_track_view, muted, volume);

	return track_view_actor;
}

