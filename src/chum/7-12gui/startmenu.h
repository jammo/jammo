/**sequencer is part of JamMo.
License: GPLv2, read more from COPYING

This file is for clutter based gui.
*/

#ifndef STARTMENU_H_
#define STARTMENU_H_

#include <tangle.h>
gboolean startmenu_goto_startmenu (TangleButton* tanglebutton, gpointer none);
gboolean startmenu_goto_communitymenu(TangleButton* tanglebutton, gpointer none);
#endif /* STARTMENU_H_ */
