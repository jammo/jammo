/**midi_editor.h is part of JamMo.
License: GPLv2, read more from COPYING

This file is for clutter based gui.
*/
#ifndef _MIDI_EDITOR_H_
#define _MIDI_EDITOR_H_
#include "../jammo-miditrack-view.h"

void midi_editor_start_with_filename(gchar* filename);
void midi_editor_start_with_miditrack_view(JammoMiditrackView* miditrack_view, JammoSequencer* sequencer);




void create_note(char name, double start_time, double end_time, gint finalNotePosX, gint finalNotePosY,gboolean from_file);




#endif /* MIDI_EDITOR_H */

