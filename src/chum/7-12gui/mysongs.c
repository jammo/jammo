/*
License: GPLv2, read more from COPYING

This file contains the functionality of mysongs menu.
 */
#include <glib-object.h>
#include <json-glib/json-glib.h>
#include <json-glib/json-gobject.h>

#include <tangle.h>
#include <string.h>
#include "communitymenu.h"
#include "startmenu.h"
#include "../jammo.h"
#include "mysongs.h"
#include "community_utilities.h"
#include "record_cover_tool.h"
#include "../../configure.h"
#include "../../cem/cem.h"
#include "../file_helper.h"
#include "../jammo-mentor.h"
#include "single_thread.h"
#include "sequencer.h" //From mysongs to sequencer 

ClutterContainer* remove_view = 0;
ClutterColor mysongs_text_color = { 255, 255, 255, 255 };
static list_type songlist = {0, 0};
static gboolean removing_song = FALSE;
static gchar* command;
ClutterActor *currentlyModifiedLabel = 0;
GList* all_songs = 0;
gint song_index = 0;

/**
 * Starts mysongs view
**/
void start_mysongs() 
{
	cem_add_to_log("Starting Mysongs", J_LOG_DEBUG);

	ClutterActor* mainview = jammo_get_actor_by_id("main-views-widget");

	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);
	
	ClutterActor* mysongs_view = jammo_get_actor_by_id("mysongs-view");

	if(mysongs_view)
	{
		clutter_actor_show(CLUTTER_ACTOR(mysongs_view));
		songlist.list = 0;
		mysongs_refresh_list();
	}

	else 	cem_add_to_log("can't find 'mysongs-view' ", J_LOG_ERROR);
}

/**
 * Ends mysongs and clears all containers
**/
void mysongs_end_mysongs(){

	community_utilities_clear_container_recursively(songlist.list);

	community_utilities_clear_songlist(all_songs);
	all_songs = NULL;

	if(removing_song){
		community_utilities_clear_container_recursively(remove_view);
		removing_song = FALSE;
	}		

}

gboolean mysongs_goto_communitymenu(TangleButton *tanglebutton, gpointer none)
{
	mysongs_end_mysongs();
	start_communitymenu();
	return TRUE;
}

gboolean mysongs_goto_startmenu(TangleButton *tanglebutton, gpointer none)
{
	mysongs_end_mysongs();
	startmenu_goto_startmenu(tanglebutton, none);
	return TRUE;
}

gboolean mysongs_mentor_clicked (TangleButton *tanglebutton, gpointer data)   
{
	cem_add_to_log("mentor clicked", J_LOG_DEBUG);
	return TRUE;
}


/**
 * Function to continue project in sequencer
**/
void start_sequencer_with_this_file(ClutterActor *actor, gpointer none) 
{
	mysongs_end_mysongs();

	const char* filename = clutter_actor_get_name(actor);
	sequencer_start_with_file(filename);
}

/*
 * Filename: contains fullpath
 * Type:
 * 0=work in progress (project)
 * 1=finalized work (no editing anymore)
 * 2=workshop in progress
 * 3=song is sent to jammosongs
 *
 * Adds files to list
 */
gboolean mysongs_add_loop_to_list(char* filename, int type, gchar* json_param)
{
	ClutterActor *colorbar 		= 0;
	ClutterActor *label 		= 0;
	ClutterActor *remove_button 	= 0;
	ClutterActor *text 		= 0;
	ClutterActor *infoButton 	= 0;
	ClutterActor *textButton 	= 0;
	ClutterActor *arrowImage 	= 0;
	ClutterContainer *listContainer = 0;
	ClutterActor *labelButton = 0;
	ClutterActor *labelBg = 0;
	gchar* path;
	char colorbar_path[80] = DATA_DIR;
	char infoButton_path[100] = DATA_DIR;

	switch (type)
	{
		case 0:
			strcat(colorbar_path, "/communitymenu/colorbar1.png");
			colorbar = tangle_texture_new(colorbar_path);
			break;
		case 1:
			strcat(colorbar_path, "/communitymenu/colorbar4.png");
			colorbar = tangle_texture_new(colorbar_path);
			break;
		case 2:
			strcat(colorbar_path, "/communitymenu/colorbar5.png");
			colorbar = tangle_texture_new(colorbar_path);
			break;
		case 3:
			strcat(colorbar_path, "/communitymenu/colorbar7.png");
			colorbar = tangle_texture_new(colorbar_path);
			break;
		default:
			strcat(colorbar_path, "/communitymenu/colorbar12.png");
			colorbar = tangle_texture_new(colorbar_path);
			break;
	}

	int i 		= 0;
	int j 		= 0;
	int type_length = 0;
	int path_length = 0;
	int userid_length = 10;
	int length	= strlen(filename);

	char temp[length];
	char filename_for_textbutton[length];
	strcpy(temp, filename);

	if(type == 0){ //projects
		path_length = strlen(configure_get_projects_directory());
		type_length = 5; //.json
	}else if(type == 1 || type == 3){ //songs
		path_length = strlen(configure_get_finalized_directory());
		type_length = 4; //.ogg
	}
		
	for(i = path_length + 1; i < (length-(type_length + userid_length)); i++)
	{
		filename_for_textbutton[j] = temp[i];
		j++;
	}
	filename_for_textbutton[j] = 0;

	//TODO: change gems_profile_manager_get_userid(NULL) to use proper user id

	command = g_strdup_printf("ls %s/label_%d_%s.csv 1> /dev/null 2> /dev/null", configure_get_jammo_directory(), 
					/*gems_profile_manager_get_userid(NULL)*/TEST_USERID, filename_for_textbutton);

	path = g_strdup_printf("%s/label_%d_%s.csv", configure_get_jammo_directory(), /*gems_profile_manager_get_userid(NULL)*/TEST_USERID, 
					filename_for_textbutton);

	gchar* labelButton_filename = g_strdup_printf("%s/communitymenu/community_cdlabel_small.png", DATA_DIR);

	if(community_utilities_file_exist(path)/*system(command) == 0*/){ // labelfile already exists
		
		label =  clutter_group_new();
		
		labelBg = clutter_rectangle_new_with_color(clutter_color_new(255,255,255,255));
		clutter_actor_set_size(labelBg, 50, 50);
		clutter_container_add_actor(CLUTTER_CONTAINER(label), labelBg);
				
		FILE *file = fopen(path, "r");
		char data[200];
		if(fread(data, sizeof(char), 200, file) != 0){
		
			strtok(data, ",");
			strtok(NULL, ",");
			char *instrument;
			instrument = strtok(NULL, ",");
		
			ClutterActor *image = tangle_texture_new(instrument);
			clutter_actor_set_size(image, 40, 40);
			clutter_actor_set_position(image, 5, 5);
		
			clutter_container_add_actor(CLUTTER_CONTAINER(label), image);
		}
		fclose(file);

		labelButton = tangle_button_new_with_background_actor(label);
		
		clutter_actor_set_size(labelButton, 50, 50);

	}else{
		labelButton = tangle_button_new_with_background_actor(tangle_texture_new(labelButton_filename));
	}

	clutter_actor_set_position(labelButton, 500, 15);
	g_free(command);
	g_free(labelButton_filename);

	gchar* remove_button_filename = g_strdup_printf("%s/backingtracks/button_cancel.png", DATA_DIR);

	remove_button = tangle_button_new_with_background_actor(tangle_texture_new(remove_button_filename));

	text = clutter_text_new_full(TEXT_NORMAL, filename_for_textbutton, &mysongs_text_color);

	textButton = tangle_button_new_with_background_actor(text);
	clutter_actor_set_name(textButton,filename);

	clutter_text_set_editable (CLUTTER_TEXT(text), FALSE);
	clutter_text_set_max_length (CLUTTER_TEXT(text), 40);
	clutter_text_set_single_line_mode (CLUTTER_TEXT(text), TRUE);

	gchar* arrowImage_filename = g_strdup_printf("%s/communitymenu/community_rightarrow.png", DATA_DIR);
	arrowImage = tangle_texture_new(arrowImage_filename);

	listContainer = CLUTTER_CONTAINER(clutter_group_new());

	//Additinally of type, there can be another icon e.g. video missing, new comments
	switch(type)
	{
		case 0:
			strcat(infoButton_path, "/communitymenu/community_backtosequencer.png");
			infoButton = tangle_button_new_with_background_actor(tangle_texture_new(infoButton_path));

			g_signal_connect(CLUTTER_ACTOR(infoButton), "clicked", G_CALLBACK (start_sequencer_with_this_file), CLUTTER_ACTOR(infoButton));
			clutter_actor_set_position(infoButton, 690, 7);
			clutter_actor_set_name(infoButton,filename);
			break;
		case 1:
			strcat(infoButton_path, "/communitymenu/community_umsic_icon.png");
			infoButton = tangle_button_new_with_background_actor(tangle_texture_new(infoButton_path));

			clutter_actor_set_position(infoButton, 690, 15);
			clutter_actor_set_name(infoButton,filename);

			g_signal_connect_swapped(CLUTTER_ACTOR(infoButton), "clicked", G_CALLBACK(mysongs_send_song), CLUTTER_ACTOR(textButton));
			
			break;
		case 3:
			strcat(infoButton_path, "/communitymenu/community_comment_icon.png");
			infoButton = tangle_button_new_with_background_actor(tangle_texture_new(infoButton_path));
			clutter_actor_set_name(infoButton,json_param);

			g_signal_connect(CLUTTER_ACTOR(infoButton), "clicked", G_CALLBACK(mysongs_get_comments), infoButton); 
			clutter_actor_set_position(infoButton, 690, 15);				
			break;
	}

	if(colorbar == NULL || infoButton == NULL || arrowImage == NULL){
		cem_add_to_log("Failed to load Colorbar, Arrowimage or infoButton", J_LOG_ERROR);
		return FALSE;	
	}

	if(songlist.listItemHeight == 0) 
		songlist.listItemHeight = clutter_actor_get_height(colorbar);

	clutter_container_add_actor(listContainer, colorbar);	//Add components inside the container

	if(labelButton != 0)
	{
		clutter_container_add_actor(listContainer, CLUTTER_ACTOR(labelButton));
		clutter_actor_raise(labelButton, colorbar);
		clutter_actor_set_name(labelButton, path);
		g_signal_connect_swapped(CLUTTER_ACTOR(labelButton), "clicked", G_CALLBACK(mysongs_label_button_clicked), CLUTTER_ACTOR(labelButton));
	}
	//clutter_actor_set_name(infoButton,filename);

	clutter_actor_set_name(remove_button,filename);
	clutter_actor_set_position(remove_button, 580, 15);
	clutter_container_add_actor(listContainer, remove_button);
	g_free(remove_button_filename);

	clutter_actor_set_position(textButton, 60, 23);		
	clutter_container_add_actor(listContainer, textButton);	
	
	clutter_actor_set_position(arrowImage, 653, 15);
	clutter_container_add_actor(listContainer, arrowImage);
	g_free(arrowImage_filename);
	
	clutter_container_add_actor(listContainer, CLUTTER_ACTOR(infoButton));


	g_signal_connect(textButton, "clicked", G_CALLBACK(song_on_clicked_function), NULL);
	g_object_set(textButton, "hold-timeout", 1500, NULL);
	g_signal_connect(textButton, "held", G_CALLBACK(song_on_held_function), NULL);


	g_signal_connect_swapped(CLUTTER_ACTOR(remove_button), "clicked", G_CALLBACK(mysongs_remove_song_view), CLUTTER_ACTOR(textButton));

	//FIXME: label is container used in label-button.
	//No point to connect clicked signal.
	//g_signal_connect_swapped(CLUTTER_ACTOR(label), "clicked", G_CALLBACK(show_song_label), filename);	

	clutter_container_add_actor(songlist.list, CLUTTER_ACTOR(listContainer));
	
	g_free(path);

	return TRUE;
}

/**
 * Views comments of chosen song
**/
void mysongs_get_comments(ClutterActor* json_)
{
	const gchar* comments_path = clutter_actor_get_name(json_); 

	if(comments_path != NULL){

		//TODO: Download messages.json from server! 

		mysongs_end_mysongs();
		ClutterActor* mainview = jammo_get_actor_by_id("main-views-widget");
		clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);
	
		start_thread_view("mysongs", comments_path, 0);
	}else{
		printf("myongs_get_comments error: bad filename\n");
	}

}


/**
 * Writes json files that are needed for sending song to server 
**/
void mysongs_send_song(ClutterActor *actor)
{
	printf("mysongs_send_song\n");

	//TODO: filenames for jsons!
	
	//TODO: Get all_songs.json from the server
	if(!community_utilities_load_file_from_server("all_songs.json")){
		printf("Couldn't download all_songs.json from server for writing!\n");
		return;
	}
	char *all_songs_filepath = g_strdup_printf("%s/all_songs.json", configure_get_temp_directory()); //Only for testing!

	FILE *commentsFile;
	ClutterActor *text = tangle_button_get_normal_background_actor(TANGLE_BUTTON(actor));
	const gchar * songname = clutter_text_get_text(CLUTTER_TEXT(text));
	
	gchar* cover =  g_strdup_printf(" "); //TODO: default?
	const char * filename = clutter_actor_get_name(actor); 
	guint32 userid = /*gems_profile_manager_get_userid(NULL)*/TEST_USERID;

	char date [80];
	cem_get_time(date);
	
	gchar* songid = g_strdup_printf("%d_%s", userid, songname);
	char * comments = g_strdup_printf("%s/messages_%s.json", configure_get_temp_directory(), songid); 
	char * label_path = g_strdup_printf("%s/label_%d_%s.csv", configure_get_jammo_directory(), userid, songname);
	
	if(community_utilities_file_exist(label_path)){
		
		if(mysongs_copy_label(label_path, userid, songname)) { //we copy the labelfile to temp directory for sending
			g_free(cover);
			cover = g_strdup_printf("%s/label_%d_%s.csv", configure_get_temp_directory(), userid, songname);
		}
	}
	g_free(label_path);
	
	//write comments file
	if ((commentsFile = fopen(comments, "w")) == NULL) {
			cem_add_to_log("Error happened while trying to write comments file", J_LOG_ERROR);
			printf("Error happened while trying to write comments file\n");
			return;
	}
	fclose(commentsFile);

	FILE * pFile;
	gboolean fileIsEmpty = FALSE;
	
	//check whether json file is empty
	if ((pFile = fopen(all_songs_filepath, "r")) == NULL) 
		//cem_add_to_log("Error happened while trying to read all_songs.json", J_LOG_ERROR);
		//printf("Error happened while trying to read a file\n");
		//return;	

		if((pFile = fopen(all_songs_filepath, "w")) == NULL){
			cem_add_to_log("Error happened while trying to write all_songs.json", J_LOG_ERROR);
			printf("Error happened while trying to write all_songs.json\n");
			return;
		}
	
	if(fgetc(pFile) == EOF)
		fileIsEmpty = TRUE;		

	fclose(pFile);

	char * dest_filepath = g_strdup_printf("%s/temp.json", configure_get_temp_directory());

	if(fileIsEmpty){

		//write a new json file
		if ((pFile = fopen(all_songs_filepath, "w")) == NULL) {
			cem_add_to_log("Error happened while trying to write into all_songs.json", J_LOG_ERROR);
			printf("Error happened while trying to write a file\n");
			return;
		}else{
			fprintf(pFile, "{\n"); 
			fprintf(pFile, "\"tracks\": [\n");
			fprintf(pFile, "{\n"); 
			fprintf(pFile, "\"songid\": \"%s\",\n", songid);
			fprintf(pFile, "\"songname\": \"%s\",\n", songname);
			fprintf(pFile, "\"userid\": \"%d\",\n", userid);
			fprintf(pFile, "\"filename\": \"%s\",\n", filename);
			//fprintf(pFile, "\"json\": \"%s\",\n", json);
			fprintf(pFile, "\"cover\": \"%s\",\n", cover);
			fprintf(pFile, "\"comments\": \"%s\",\n", comments);
			fprintf(pFile, "\"date\": \"%s\"\n", date);
       	 		fprintf(pFile, "}\n"); 		
			fprintf(pFile, "]\n"); 	
			fprintf(pFile, "}"); 
		}
		fclose(pFile);	
	}else{
		
		//update json file
		FILE *destFile;
		int c, position_found = 0;
		
		//open source file
		if((pFile = fopen(all_songs_filepath, "r")) == NULL){
			printf("Error happened while trying to open source file (all_songs.json)\n");
			return;
		}
		//open destination file
		if((destFile = fopen(dest_filepath, "w")) == NULL){
			printf("Error happened while trying to open destination file (temp.json)\n");
			return;
		}	
		//copy the file
		fprintf(destFile, "{\n"); 
		fprintf(destFile, "\"tracks\": [");
		
		do{
			c = fgetc(pFile);
			if(ferror(pFile)) {
      				printf("Error reading source file (all_songs.json)\n");
      				return;
    			}
			if(c == '[')
				position_found = 1;
			
			if(position_found == 1 && c != '[' && c != ']'){
				if(c != ']') fputc(c, destFile);
				if(ferror(destFile)) {
      					printf("Error writing destination file (temp.json)\n");
      					return;
				}
			}
		}while(c != ']');	
		fclose(pFile);	
	
		fprintf(destFile, ",\n");
		fprintf(destFile, "{\n"); 
		fprintf(destFile, "\"songid\": \"%s\",\n", songid);
		fprintf(destFile, "\"songname\": \"%s\",\n", songname);
		fprintf(destFile, "\"userid\": \"%d\",\n", userid);
		fprintf(destFile, "\"filename\": \"%s\",\n", filename);
		//fprintf(destFile, "\"json\": \"%s\",\n", json);
		fprintf(destFile, "\"cover\": \"%s\",\n", cover);
		fprintf(destFile, "\"comments\": \"%s\",\n", comments);
		fprintf(destFile, "\"date\": \"%s\"\n", date);
       		fprintf(destFile, "}\n");	      	 	
		fprintf(destFile, "]\n"); 	
		fprintf(destFile, "}"); 	 	
		fclose(destFile);
		
		if(remove(all_songs_filepath) != 0){ //We remove the original json
			printf("Error removing original all_songs.json\n");
			return;
		}			
		rename(dest_filepath, all_songs_filepath); //And rename temp file with the name of the original
		
	}//Json file is ready

	gchar * message = g_strdup_printf("Sending song \"%s\" to JamMo Songs...", filename);
	cem_add_to_log(message, J_LOG_DEBUG);
	g_free(message);
	
	if(sendJammoSong(filename, cover, comments, all_songs_filepath))
		printf("sendJammoSong ok\n");

	g_free(cover);
	
	g_free(all_songs_filepath);
	g_free(dest_filepath);
	g_free(songid);
	g_free(comments);
}


/**
 * Sends a song to server
**/
gboolean sendJammoSong(const char *song_filename, const char *song_cover, const char *comments_file, const char *all_songs_json)
{
	gboolean returnvalue = TRUE;
		
	//TODO: Send song.ogg, cover.csv(if != ""), comments.json and all_songs.json to server 
	
	//TODO: Check that files where successfully sended
	
	//TODO: Remove jsons from hd

	if(returnvalue == TRUE){
		community_utilities_clear_container_recursively(songlist.list);
		mysongs_refresh_list();
	}		

	return returnvalue;
}


/**
 * Searches a song of given name from all_songs.json. This function is used to check whether the song has been sended to JammoSongs. 
**/
gboolean find_song_by_filename(char* songName, GList* songlist_param){

	gboolean songWasFound = FALSE;
	GList* l = NULL; //Iterator

	int numberOfFoundSongs = g_list_length(songlist_param);
	int i = 0, j = 0, songlist_index = 0;
	int type_length = strlen(".ogg");
	int path_length = strlen(configure_get_finalized_directory());
	int userid_length = 10;
	int filename_length = strlen(songName);
	char temp[filename_length];
	char real_songname[filename_length];

	strcpy(temp, songName);
		
	for(i = path_length + 1; i < (filename_length-(type_length + userid_length)); i++){
		real_songname[j] = temp[i];
		j++;
	}
	real_songname[j] = 0;

	//build songid for the song
	char* songid = g_strdup_printf("%d_%s", TEST_USERID, real_songname);
	
	if(numberOfFoundSongs > 0){
	
		for (l = songlist_param; l; l = l->next){
			
			if(strcmp(((song_type*)l->data)->songid, songid) == 0){ //search with songid
				songWasFound = TRUE;
				song_index = songlist_index;
			}
			songlist_index++;		
		}//for
	}else{
		printf("songlist is empty\n");
	}
	g_free(songid);

	return songWasFound;
}


/**
 * Deletes a song from list and hard-drive 
**/
void mysongs_delete_song(ClutterActor *actor)
{
	const gchar * filename = clutter_actor_get_name(actor);
	gchar* suffix = strrchr(filename, '.');
	gchar songname[99];
	gchar* temp;
	int i;

	for(temp = strrchr(filename, '/'), i = 0; temp+1 != suffix; i++){
		temp++;
		songname[i] = *temp;
	}
	songname[i] = 0;
	
	//TODO: change gems_profile_manager_get_userid(NULL) to use proper user id
	gchar* label = g_strdup_printf("%s/label_%d_%s.csv", configure_get_jammo_directory(), /*gems_profile_manager_get_userid(NULL)*/TEST_USERID, songname);

	gchar * message = g_strdup_printf("Removing %s...", filename);
	cem_add_to_log(message, J_LOG_DEBUG);
	g_free(message);

	if(remove(filename) != 0)
    		cem_add_to_log("Error deleting file", J_LOG_ERROR);
  	else
  		cem_add_to_log("File successfully deleted", J_LOG_ERROR);
    		
    	if(remove(label) != 0)
    		cem_add_to_log("Error deleting labelfile", J_LOG_ERROR);
    	else	
    		cem_add_to_log("Labelfile succesfully deleted", J_LOG_ERROR);		
    		
	g_free(label);

	community_utilities_clear_container_recursively(songlist.list);
	community_utilities_clear_container_recursively(remove_view);
	removing_song = FALSE;
	mysongs_refresh_list();
}


/**
 * Cancels song delete
**/
void mysongs_cancel_song_delete(ClutterActor *actor)
{
	removing_song = FALSE;
	community_utilities_clear_container_recursively(remove_view);
}


/**
 * Sets up question whether to remove a song or not
**/
void mysongs_remove_song_view(ClutterActor *actor){

	if(removing_song) return;

	removing_song = TRUE;
	remove_view = CLUTTER_CONTAINER(jammo_get_actor_by_id("mysongs-container"));

	ClutterContainer *listContainer = CLUTTER_CONTAINER(clutter_group_new());
	gchar* ok_button_filename = g_strdup_printf("%s/backingtracks/button_ok.png", DATA_DIR);
	gchar* cancel_button_filename = g_strdup_printf("%s/backingtracks/button_cancel.png", DATA_DIR);

	ClutterActor *text = CLUTTER_ACTOR(clutter_text_new_full(TEXT_CAPTION, "Remove?", &mysongs_text_color));
	clutter_actor_set_position(text, 20, 60);
	clutter_container_add_actor(listContainer, text);
	
	ClutterActor *ok_button = tangle_button_new_with_background_actor(tangle_texture_new(ok_button_filename));

	ClutterActor *cancel_button = tangle_button_new_with_background_actor(tangle_texture_new(cancel_button_filename));

	clutter_actor_set_position(ok_button, 170, 40);
	clutter_container_add_actor(listContainer, ok_button);

	clutter_actor_set_position(cancel_button, 260, 60);
	clutter_container_add_actor(listContainer, cancel_button);

	g_signal_connect_swapped(CLUTTER_ACTOR(ok_button), "clicked", G_CALLBACK(mysongs_delete_song), CLUTTER_ACTOR(actor));
	g_signal_connect_swapped(CLUTTER_ACTOR(cancel_button), "clicked", G_CALLBACK(mysongs_cancel_song_delete), CLUTTER_CONTAINER(remove_view));

	clutter_container_add_actor(remove_view, CLUTTER_ACTOR(listContainer));

	g_free(ok_button_filename);
	g_free(cancel_button_filename);
}


/**
 * This function is called to start label editor
**/
void mysongs_label_button_clicked(ClutterActor *label)
{
	currentlyModifiedLabel = label;
	start_record_cover_tool(clutter_actor_get_name(label));
}

/**
 * Upadates label button after it has been changed
**/
void mysongs_update_label_button(const gchar* path){

	currentlyModifiedLabel = tangle_button_new_with_background_actor(clutter_rectangle_new_with_color(clutter_color_new(255,255, 255,255)));
	mysongs_refresh_list();
}


/**
 * Refreshes list after it has been updated
**/
void mysongs_refresh_list()
{
	int numberOfLoops = 0;
	GList* l = NULL; //Iterator
	GList* projects = NULL;
	GList* finalized = NULL;
	all_songs = 0;

	cem_add_to_log("Refreshing list...", J_LOG_DEBUG);
	
	community_utilities_clear_container_recursively(songlist.list);
	
	//TODO: download all_songs.json from server
	gchar* all_songs_path = g_strdup_printf("%s/all_songs.json", configure_get_temp_directory()); //Only for testing!
	community_utilities_parse_songs_from_file(all_songs_path, &all_songs, 1);

	projects = file_helper_get_all_files(configure_get_projects_directory()); //Project-files
	finalized = file_helper_get_all_files(configure_get_finalized_directory()); //Audio-files

	numberOfLoops += g_list_length (projects) + g_list_length (finalized);

	if(numberOfLoops > 5)
		songlist.list = CLUTTER_CONTAINER(jammo_get_actor_by_id("mysongs-songs-list-scrollable"));	

	else	songlist.list = CLUTTER_CONTAINER(jammo_get_actor_by_id("mysongs-songs-list-unscrollable"));	

	clutter_actor_lower(CLUTTER_ACTOR(songlist.list),jammo_get_actor_by_id("mysongs_down-index"));

	for (l = projects; l; l = l->next)
		mysongs_add_loop_to_list(l->data, 0, NULL); //0 = Project-file

	for (l = finalized; l; l = l->next)
		
		if(find_song_by_filename(l->data, all_songs)){ 
 
			mysongs_add_loop_to_list(l->data, 3, ((song_type*)g_list_nth_data(all_songs, song_index))->comments_path); //3 = File has been sended to JammoSongs
		}else{
			mysongs_add_loop_to_list(l->data, 1, NULL); //1 = Finalized file
		}

	if(numberOfLoops <= 6)
		clutter_actor_set_height(CLUTTER_ACTOR(songlist.list), songlist.listItemHeight * numberOfLoops);
	else	
		clutter_actor_set_height(CLUTTER_ACTOR(songlist.list), songlist.listItemHeight * 6);
	
	ClutterAction *listAction = tangle_actor_get_action_by_type(CLUTTER_ACTOR(songlist.list), TANGLE_TYPE_SCROLL_ACTION);

	if (listAction)
		g_signal_connect_swapped(listAction, "clamp-offset-y", G_CALLBACK(community_utilities_autoscroll_list), songlist.list);
	else
		printf("listAction is NULL\n");

	community_utilities_clear_songlist(all_songs);
	all_songs = NULL;
	g_free(all_songs_path);


}

/**
 * Copies a song label to jammo temp directory for sending to server
**/
gboolean mysongs_copy_label(gchar* label_param, guint32 userid_param, const gchar* songname_param){

	int c;
	FILE *pFile;
	FILE *destFile;
	gchar* dest_filepath = g_strdup_printf("%s/label_%d_%s.csv", configure_get_temp_directory(), userid_param, songname_param);

	//open source file
	if((pFile = fopen(label_param, "r")) == NULL){
		printf("Error happened while trying to open source file '%s'\n", label_param);
		return FALSE;
	}		
	//open destination file
	if((destFile = fopen(dest_filepath, "w")) == NULL){
		printf("Error happened while trying to open destination file '%s'\n", dest_filepath);
		return FALSE;
	}			
	//copy the file
	do{
		c = fgetc(pFile);
		if(ferror(pFile)) {
      			printf("Error reading source file '%s'\n", label_param);
      			return FALSE;
    		}
		if(c != EOF) fputc(c, destFile);
		if(ferror(destFile)) {
      			printf("Error writing destination file '%s'\n", dest_filepath);
      			return FALSE;
		}
			
	}while(c != EOF);
	fclose(pFile);		 	
	fclose(destFile);

	g_free(dest_filepath);
	return TRUE;
}
