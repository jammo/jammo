#ifndef PROFILE_VIEWER_H
#define PROFILE_VIEWER_H

#include <glib.h>
#include <tangle.h>

typedef struct{
	guint32 user_id;
	char parent_view[30];
}profile_view_params;

typedef struct{
	guint32 user_id;
	char *username;
	char *hobbies;
	char *gender;
}user_type;

void start_profile_view(profile_view_params *pvp);
gboolean parse_user_profile_from_file(gchar* filename);
gboolean end_profile_view(TangleActor* actor, gpointer data);

#endif
