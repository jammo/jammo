ClutterAnimation * midi_editor_animate_press(ClutterActor *actor);
ClutterAnimation * midi_editor_animate_release(ClutterActor *actor);
ClutterAnimation * midi_editor_animate_zoom(ClutterActor *actor, double scaleX, double scaleY, gfloat x, gfloat y);
ClutterAnimation * midi_editor_animate_opacity(ClutterActor *actor, guint duration, guint opacity);
ClutterColor* midi_editor_get_color(const char *table);
void draw_text(const gchar *myText, gfloat x, gfloat y, ClutterActor* myContainer, ClutterColor* myColor);
