#include "avatar_editor.h"
#include "startmenu.h"
#include "../jammo.h"
#include "communitymenu.h"
#include "community_utilities.h"
#include "string.h"
#include "../../cem/cem.h"
#include "avatar.h"

static ClutterContainer *partsList;
static ClutterContainer* avatarImage_static = 0;

static char* currentHairColor;
static char* currentHairType;

static Avatar avatar_static = {0,0,0,0};
static TextField hobbiesTextField;
static TextField nameTextField;
static TextField genderTextField;

static void end_avatar_view();

static gboolean show_instrument_dialog(TangleActor *actor, gpointer data);
static void split_hair_style_and_color(const char* para, gchar** type, gchar** color);

/*
* Starts avatar editor.
*/
void start_avatar_editor(){
	show_roster(TRUE);
}

//TODO: move this function out of avatar-editor.
void show_roster(gboolean editor){
	cem_add_to_log("Starting avatar editor", J_LOG_DEBUG);

	//Creating default user to test avatar saving!

	static int start = 1; // For testing only!!!
	
	if(start){// For testing only!!!
		#ifdef NETWORKING_ENABLED
		gems_profile_manager_create_default_user_profile("sala", 23);// For testing only!!!
		gems_profile_manager_authenticate_default_user("sala");// For testing only!!!
		#endif
		start = 0;// // For testing only!!!
	}
	//TODO: Get real userid

	guint32 avatar_id = TEST_USERID; //TODO
	gchar* profile_path = g_strdup_printf("%s/user_%d.json", configure_get_temp_directory(), avatar_id);

	//Default values
	gchar* username = g_strdup_printf("<<YOUR NAME HERE>>");
	gchar* hobbies = g_strdup_printf("<<YOUR HOBBIES HERE>>");
	gchar* gender = g_strdup_printf("<<YOUR GENDER HERE>>");
	guint32 serialized_image;
	guint32 age=7; //default

	avatar_parse_json(profile_path,&username,&hobbies,&gender,&serialized_image,&age);

	printf("username '%s', hobbies '%s', gender '%s', serialized_image '%d'\n",username, hobbies, gender,  serialized_image);

	ClutterActor* mainview;
	
	mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);
	
	ClutterActor* avatarView = jammo_get_actor_by_id("avatar-view");
	
	if(avatarView==NULL){
		cem_add_to_log("Can't start roster-view", J_LOG_FATAL);
		return;
	}
	clutter_actor_show(CLUTTER_ACTOR(avatarView));
	clutter_text_set_text(CLUTTER_TEXT(jammo_get_actor_by_id("avatar_hobbies_text")), _("HOBBIES:"));
	clutter_text_set_text(CLUTTER_TEXT(jammo_get_actor_by_id("avatar_name_text")), _("NAME:"));
	clutter_text_set_text(CLUTTER_TEXT(jammo_get_actor_by_id("avatar_gender_text")), _("GENDER:"));
	clutter_text_set_text(CLUTTER_TEXT(jammo_get_actor_by_id("avatar_instrument_text")), _("FAVOURITE INSTRUMENT:"));





	avatar_id =TEST_USERID; //TODO = gems_profile_manager_get_avatar_id()

	if(avatar_id != 0){
		avatar_unserialize_image(&avatar_static, serialized_image);

		//split hair style and color
		if(avatar_static.hair != 0){
			char *temp = strstr(clutter_actor_get_name(avatar_static.hair), "hairs/");
			split_hair_style_and_color(temp,&currentHairType,&currentHairColor);
		}
		else {
			currentHairType =  g_strdup_printf(" ");
			currentHairColor =  g_strdup_printf(" ");
		}
	}

	hobbiesTextField.text = jammo_get_actor_by_id("avatar_hobbies_textfield");
	nameTextField.text = jammo_get_actor_by_id("avatar_name_textfield");
	genderTextField.text = jammo_get_actor_by_id("avatar_gender_textfield");
	clutter_text_set_text(CLUTTER_TEXT(hobbiesTextField.text), _(hobbies));
	clutter_text_set_text(CLUTTER_TEXT(nameTextField.text), _(username));
	clutter_text_set_text(CLUTTER_TEXT(genderTextField.text), _(gender));

	//Editor/Viewer specific:
	if (editor){
		clutter_actor_show(jammo_get_actor_by_id("avatar-editor-specific-container"));
		clutter_actor_hide(jammo_get_actor_by_id("roster-viewer-specific-container"));

		partsList = CLUTTER_CONTAINER(jammo_get_actor_by_id("avatar-parts-list"));

		clutter_text_set_editable(CLUTTER_TEXT(hobbiesTextField.text),TRUE);
		hobbiesTextField.lineCount = 2;
		hobbiesTextField.max_characters = 500;

		clutter_text_set_editable(CLUTTER_TEXT(nameTextField.text),TRUE);
		nameTextField.lineCount = 1;
		nameTextField.max_characters = 500;

		clutter_text_set_editable(CLUTTER_TEXT(genderTextField.text),TRUE);
		genderTextField.lineCount = 1;
		genderTextField.max_characters = 500;


		g_free(username);
		g_free(hobbies);
		g_free(gender);

		hobbiesTextField.handler_id = g_signal_connect_swapped(hobbiesTextField.text, "text-changed", (GCallback)community_utilities_limit_line_count, &hobbiesTextField);
		nameTextField.handler_id = g_signal_connect_swapped(nameTextField.text, "text-changed", (GCallback)community_utilities_limit_line_count, &nameTextField);
		genderTextField.handler_id = g_signal_connect_swapped(genderTextField.text, "text-changed", (GCallback)community_utilities_limit_line_count, &genderTextField);

		//Assign clicked signal (pressing current instrument shows instrument-dialog)
		g_signal_connect(avatar_static.instrument, "clicked", G_CALLBACK(show_instrument_dialog), NULL);
	}
	else { //viewer only
		clutter_actor_hide(jammo_get_actor_by_id("avatar-editor-specific-container"));
		clutter_actor_show(jammo_get_actor_by_id("roster-viewer-specific-container"));


		clutter_text_set_editable(CLUTTER_TEXT(nameTextField.text),FALSE);
		clutter_text_set_editable(CLUTTER_TEXT(genderTextField.text),FALSE);
		clutter_text_set_editable(CLUTTER_TEXT(hobbiesTextField.text),FALSE);

		gchar* age_string = g_strdup_printf("%d",age);
		clutter_text_set_text(CLUTTER_TEXT(jammo_get_actor_by_id("avatar_age_text")), _("AGE: "));
		clutter_text_set_text(CLUTTER_TEXT(jammo_get_actor_by_id("avatar_age_textfield")), age_string);
		g_free(age_string);

		clutter_actor_set_reactive(avatar_static.instrument,FALSE);
	}


	avatarImage_static = CLUTTER_CONTAINER(jammo_get_actor_by_id("avatar-widgets"));
	set_avatar(&avatar_static, CLUTTER_ACTOR(avatarImage_static));
}

static void set_head(Avatar *avatar, ClutterActor* avatarImage){
	gchar* default_head_filename = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/heads/head_yellow.png", DATA_DIR);
	if(avatar->head == 0){
		avatar->head = tangle_texture_new(default_head_filename);
		clutter_actor_set_name(avatar->head, "head_yellow.png");
	}

	clutter_container_add_actor(CLUTTER_CONTAINER(avatarImage), avatar->head);
	clutter_actor_set_position(avatar->head, 92, 85);

	g_free(default_head_filename);
}

static void set_body(Avatar *avatar, ClutterActor* avatarImage){
	gchar* default_body_filename = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/bodys/body_blue.png", DATA_DIR);
	if(avatar->body == 0){
		avatar->body = tangle_texture_new(default_body_filename);
		clutter_actor_set_name(avatar->body, "body_blue.png");
	}

	clutter_container_add_actor(CLUTTER_CONTAINER(avatarImage), avatar->body);
	clutter_actor_set_position(avatar->body, 65, 170);

	g_free(default_body_filename);
}



static void set_eyes(Avatar *avatar, ClutterActor* avatarImage){
	gchar* default_eyes_filename = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/eyes/eyes1.png", DATA_DIR);
	if(avatar->eyes == 0){
		avatar->eyes= tangle_texture_new(default_eyes_filename);
		clutter_actor_set_name(avatar->eyes, "eyes1.png");
	}

	clutter_container_add_actor(CLUTTER_CONTAINER(avatarImage), avatar->eyes);
	clutter_actor_set_position(avatar->eyes, 92, 85);

	g_free(default_eyes_filename);
}

static void set_hair(Avatar *avatar, ClutterActor* avatarImage){
	if(avatar->hair != 0){
		clutter_container_add_actor(CLUTTER_CONTAINER(avatarImage), avatar->hair);
		clutter_actor_set_position(avatar->hair, 50, 35);
	}
}

static void set_instrument(Avatar *avatar, ClutterActor* avatarImage){
	if(avatar->instrument != 0){
		clutter_actor_set_position(avatar->instrument, 510, 245);
		clutter_container_add_actor(CLUTTER_CONTAINER(avatarImage), avatar->instrument);
	}
}

/*
* Sets avatar picture and other widgets
*
* parameters:
* -avatar    Avatar struct that contains avatar parts
* -avatarImage   Container where to put avatar parts
*/
void set_avatar(Avatar *avatar, ClutterActor *avatarImage){

	set_head(avatar, avatarImage);
	set_body(avatar, avatarImage);
	set_eyes(avatar, avatarImage);
	set_hair(avatar, avatarImage);

	set_instrument(avatar, avatarImage);
}


/*
* Callback function to handle clicks in parts-selection-lists. Takes button clicked
* as parameter. Button's name should be path to the image of particular avatar part. 
*
*/
static void pressed(ClutterActor *actor){

	gchar* eyes_path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/eyes/", DATA_DIR);
	gchar* bodys_path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/bodys/", DATA_DIR);
	gchar* heads_path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/heads/", DATA_DIR);
	gchar* hairs_path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/hairs/", DATA_DIR);
	gchar* instruments_path = g_strdup_printf("%s/communitymenu/INSTRUMENTS/", DATA_DIR);

	if(strstr(clutter_actor_get_name(actor), bodys_path)){
		tangle_texture_set_from_file(TANGLE_TEXTURE(avatar_static.body),clutter_actor_get_name(actor));
		clutter_actor_set_name(avatar_static.body, clutter_actor_get_name(actor));
	}

	else if(strstr(clutter_actor_get_name(actor), hairs_path)){
		tangle_texture_set_from_file(TANGLE_TEXTURE(avatar_static.hair),clutter_actor_get_name(actor));
		clutter_actor_set_name(avatar_static.hair, clutter_actor_get_name(actor));

		char* directory = strstr(clutter_actor_get_name(actor), "hairs/");
		split_hair_style_and_color(directory,&currentHairType,&currentHairColor);
	}

	else if(strstr(clutter_actor_get_name(actor), heads_path)){
		tangle_texture_set_from_file(TANGLE_TEXTURE(avatar_static.head),clutter_actor_get_name(actor));
		clutter_actor_set_name(avatar_static.head, clutter_actor_get_name(actor));
	}

	else if(strstr(clutter_actor_get_name(actor), eyes_path)){
		tangle_texture_set_from_file(TANGLE_TEXTURE(avatar_static.eyes),clutter_actor_get_name(actor));
		clutter_actor_set_name(avatar_static.eyes, clutter_actor_get_name(actor));
	}

	else if(strstr(clutter_actor_get_name(actor), instruments_path)){
		tangle_texture_set_from_file(TANGLE_TEXTURE(
           tangle_button_get_normal_background_actor(
           TANGLE_BUTTON(avatar_static.instrument))),clutter_actor_get_name(actor));
		clutter_actor_set_name(avatar_static.instrument, clutter_actor_get_name(actor));
	}

	g_free(eyes_path);
	g_free(bodys_path);
	g_free(heads_path);
	g_free(hairs_path);
	g_free(instruments_path);
}

/*
* This function is called when body color change button is clicked.
*/
gboolean show_body_color_dialog(TangleActor *actor, gpointer data){

	cem_add_to_log("show_body_color_dialog opened", J_LOG_DEBUG);

	ClutterContainer* scrolling_container = CLUTTER_CONTAINER(jammo_get_actor_by_id("avatar-parts-list"));
	community_utilities_clear_container_recursively(scrolling_container);

	gchar* path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/bodys/", DATA_DIR);
	if(!community_utilities_make_buttons_from_directory(path, scrolling_container, (GCallback)pressed, 80, 80)){
		cem_add_to_log("can't open directory containing avatar body parts", J_LOG_ERROR);
	}
	g_free(path);

	return TRUE;
}

/*
* This function is called when eye change button is clicked.
*/
gboolean show_eye_dialog(TangleActor *actor, gpointer data){

	cem_add_to_log("show_eye_dialog opened", J_LOG_DEBUG);

	ClutterContainer* scrolling_container = CLUTTER_CONTAINER(jammo_get_actor_by_id("avatar-parts-list"));
	community_utilities_clear_container_recursively(scrolling_container);

	gchar* path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/eyes/", DATA_DIR);
	if(!community_utilities_make_buttons_from_directory(path, scrolling_container, (GCallback)pressed, 80, 80)){
		cem_add_to_log("can't open directory containing avatar body parts", J_LOG_ERROR);
	}
	g_free(path);

	return TRUE;
}

/*
* This function is called when hair color change button is clicked.
*/
gboolean show_hair_color_dialog(TangleActor *actor, gpointer data){
	cem_add_to_log("show_hair_color_dialog opened", J_LOG_DEBUG);

	ClutterContainer* scrolling_container = CLUTTER_CONTAINER(jammo_get_actor_by_id("avatar-parts-list"));
	community_utilities_clear_container_recursively(scrolling_container);

	gchar* path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/hairs/", DATA_DIR);

	ClutterActor* button = NULL;

	char result[MAX_FILES_IN_DIR][MAX_FILE_NAME_LEN];
	int numberOfImages = community_utilities_open_and_arrange_dir(path, "DIR", result);
	int width=80;

	for(int i = 0; i < numberOfImages; i++){
		gchar* image_path = g_strdup_printf("%s%s/%s", path,result[i],currentHairType);
		community_utilities_add_single_button(button, scrolling_container, image_path, width, 80, (GCallback)pressed);
		g_free(image_path);
	}

	g_free(path);

	if (numberOfImages*width <= 800){
		//Do not scroll it, they fit.
		ClutterAction* scroll_action = tangle_actor_get_action_by_type(CLUTTER_ACTOR(scrolling_container),TANGLE_TYPE_SCROLL_ACTION);
		tangle_scroll_action_set_thresholds(TANGLE_SCROLL_ACTION(scroll_action), 30000.0,3000.0);

		//Adjust position
		g_object_set(G_OBJECT(scroll_action), "offset-x", 0.0, NULL);

		//If scroller is very this moment scrolling, this is needed to stop it
		tangle_object_animate(G_OBJECT(scroll_action),  CLUTTER_EASE_IN_OUT_QUAD, 50, "offset-x", 0.0, NULL);
	}

	clutter_actor_set_width(CLUTTER_ACTOR(scrolling_container),numberOfImages*width);

	return TRUE;
}


/*
* This function is called when hair type change button is clicked.
*/
gboolean show_hair_dialog(TangleActor *actor, gpointer data){
	cem_add_to_log("changing hair type", J_LOG_DEBUG);

	community_utilities_clear_container_recursively(partsList);

	gchar* path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/hairs/%s/", DATA_DIR,currentHairColor);

	if(!community_utilities_make_buttons_from_directory(path, partsList, (GCallback)pressed, 100, 100))
		cem_add_to_log("can't open directory containing avatar parts", J_LOG_ERROR);

	g_free(path);
	return TRUE;
}

/*
* This function is called when head change button is clicked.
*/
gboolean show_head_color_dialog(TangleActor *actor, gpointer data){

	cem_add_to_log("show_head_color_dialog opened", J_LOG_DEBUG);

	ClutterContainer* scrolling_container = CLUTTER_CONTAINER(jammo_get_actor_by_id("avatar-parts-list"));
	community_utilities_clear_container_recursively(scrolling_container);

	gchar* path = g_strdup_printf("%s/communitymenu/AVATAR_PARTS/heads/", DATA_DIR);
	if(!community_utilities_make_buttons_from_directory(path, scrolling_container, (GCallback)pressed, 100, 100)){
		cem_add_to_log("can't open directory containing avatar body parts", J_LOG_ERROR);
	}
	g_free(path);

	return TRUE;
}

/*
* This function is called when instrument-icon is clicked.
*/
static gboolean show_instrument_dialog(TangleActor *actor, gpointer data){

	cem_add_to_log("changing instrument", J_LOG_DEBUG);
	gchar* instruments_path = g_strdup_printf("%s/communitymenu/INSTRUMENTS/", DATA_DIR);
	
	community_utilities_clear_container_recursively(partsList);

	if(!community_utilities_make_buttons_from_directory(instruments_path, partsList, (GCallback)pressed, 100, 100))
		cem_add_to_log("can't open directory containing instruments", J_LOG_ERROR);

	g_free(instruments_path);

	return TRUE;
}



/*
* Writes avatar info into file.
*/
static gboolean write_avatar_json(guint32 id, guint32 serialized_image, const char *username, const char *hobbies, const char *gender){

	gchar* user_profile_for_others = g_strdup_printf("%s/user_%d.json", configure_get_temp_directory(), id);

	FILE *pFile;
	if ((pFile = fopen(user_profile_for_others, "w")) == NULL) {
		cem_add_to_log("Error happened while trying to write user .json file", J_LOG_ERROR);
		printf("Error happened while trying to write a file\n");
		return FALSE;
	}else{
		fprintf(pFile, "{\n"); 
		fprintf(pFile, "\"serialized_image\": %d,\n", serialized_image);
		fprintf(pFile, "\"username\": \"%s\",\n", username);
		fprintf(pFile, "\"hobbies\": \"%s\",\n", hobbies);
		fprintf(pFile, "\"gender\": \"%s\"\n", gender);
		fprintf(pFile, "}\n");
	}
	fclose(pFile);	


	if(!sendJammoUserProfile(user_profile_for_others)){
		printf("Failed to send user profile to server\n");
		return FALSE;
	} 

	g_free(user_profile_for_others);

	return TRUE;
}


/* e.g. para= 'hairs/black/hair_bush.png'
 currentHairColor-> 'black'
 currentHairType-> 'hair_bush.png'
*/
static void split_hair_style_and_color(const char* para, gchar** type, gchar** color) {
	char* directory2;
	char hair_type[50];
	char hair_color[50];

	gchar* directory = g_strdup_printf("%s",para);
	//printf("1. directory '%s'\n",directory);
	directory = strpbrk(directory, "/");      //e.g. '/black/hair_bush.png'
	//printf("2. directory '%s'\n",directory);
	directory++;                              //e.g. 'black/hair_bush.png'
	//printf("3. directory '%s'\n",directory);

	directory2 = strpbrk(directory, "/");     //e.g. '/hair_bush.png'
	//printf("4. directory2 '%s'\n",directory2);
	size_t length1 = strlen ( directory );    //e.g. 19
	size_t length2 = strlen ( directory2 );   //e.g. 14

	size_t diff = length1-length2;            //e.g. 5
	//printf("5. len %d, len %d, diff %d\n",length1,length2,diff);

	strncpy (hair_color, directory, diff);  //doesn't add '\0'
	hair_color[diff]='\0';              //e.g. 'black'

	//printf("currentHairColor '%s'\n",currentHairColor);

	directory2++;                             //e.g. 'hair_bush.png'
	strcpy(hair_type, directory2);  //This is now currentHairType

	*type = g_strdup_printf("%s",hair_type);
	*color = g_strdup_printf("%s",hair_color);
}

//For cleanupping
static void end_avatar_view() {
end_roster(TRUE);
}
void end_roster(gboolean editor){
	const char* hobbies = clutter_text_get_text(CLUTTER_TEXT(hobbiesTextField.text));
	const char* username = clutter_text_get_text(CLUTTER_TEXT(nameTextField.text));
	const char* gender = clutter_text_get_text(CLUTTER_TEXT(genderTextField.text));

	guint32 id;
	id = TEST_USERID; //TODO
	guint32 serialized_image = avatar_serialize_image(&avatar_static);
	write_avatar_json(id, serialized_image, username, hobbies, gender);

	community_utilities_clear_container_recursively(partsList);

	DESTROY_ACTOR(avatar_static.body);
	DESTROY_ACTOR(avatar_static.hair);
	DESTROY_ACTOR(avatar_static.head);
	DESTROY_ACTOR(avatar_static.instrument);
	DESTROY_ACTOR(avatar_static.eyes);

	partsList = 0;
	//avatarImage = 0;
}


gboolean sendJammoUserProfile(gchar* user_profile)
{
	gboolean returnvalue = TRUE;
	
	//TODO: Upload file to server
		
	return returnvalue;
}


gboolean avatar_goto_communitymenu(TangleButton *tanglebutton, gpointer none)
{
	end_avatar_view();
	start_communitymenu();
	return TRUE;
}

gboolean avatar_goto_startmenu(TangleButton *tanglebutton, gpointer none)
{
	end_avatar_view();
	startmenu_goto_startmenu(tanglebutton, none);
	return TRUE;
}
