/*
* File community_utilities.h contains utility functions that might (not) be useful anywhere
*
*
*/

#ifndef COMMUNITY_UTILITIES_H
#define COMMUNITY_UTILITIES_H

#include <tangle.h>
#include "../jammo.h"
#include "../../configure.h"
#include <libintl.h>
#ifdef NETWORKING_ENABLED
#include "../../gems/gems.h"
#include "../../gems/gems_profile_manager.h"
#include "../../gems/gems_utils.h"
#endif
#include "avatar.h"

#define DESTROY_ACTOR(x) {if(x==0); else{clutter_actor_destroy(x); x = 0;}}
#define TEXT_COMMENT 1
#define VOICE_COMMENT 2

#define TEXT_CAPTION "Comic Sans 24"
#define TEXT_LARGE "Comic Sans 16"
#define TEXT_NORMAL "Comic Sans 14"
#define TEXT_SMALL "Comic Sans 12"

#define TEST_USERID 202179860 //ONLY FOR TESTING
#define TEST_USERID2 18039856 //ONLY FOR TESTING


/*
* message_type structure contains info about message, sender, time etc.
*/
typedef struct{
	gint  index;
	guint32 user_id;
	gchar sender[20];	
	gchar title[100];
	gchar time[17];
	gchar *msg;
	gchar *filepath;
	int type;
}message_type;


typedef struct{
	guint32 user_id;
	gchar songname[50];	
	gchar *songid;
	gchar *filename;
	//gchar *json;
	gchar *cover;
	gchar *comments_path;
	gchar date[17];
}song_type;

typedef struct{
	guint32 user_id;
	gchar loopname[50];	
	gchar *loopid;
	gchar *filename;
	//gchar *json;
	gchar date[17];
}loop_type;

/*
* Struct used to limit line count of text field.
*
* See function limit_line_count
*/
typedef struct {
	ClutterActor *text;     // actual textfield
	int lineCount;			// number of lines
	gulong handler_id;		// id of the handler function (this is what g_signal_connect returns)
	int max_characters;		// max number of characters in field
}TextField;

typedef struct{
	char time[19];
}time_type;

typedef struct{
	ClutterContainer *list;
	gint listItemHeight;
}list_type;


/*
* Struct needed to pass several params in a single callback function, used when showing record cover view 
*/
typedef struct{
	gchar* path;
	gchar parentview[15];
}show_label_params;
	

void song_on_held_function(TangleButton* button, gpointer user_data);
void song_on_clicked_function(TangleButton* button, gpointer user_data);

gboolean community_utilities_add_single_button(ClutterActor* button, ClutterContainer* list, char* path, int width,int height, GCallback function);
void community_utilities_clear_container_recursively(ClutterContainer* listToClear);
void community_utilities_clear_songlist(GList*);
gboolean community_utilities_file_exist(const gchar* filename); //with fullpath
gboolean community_utilities_load_file_from_server(const gchar* filename);
void community_utilities_autoscroll_list(list_type *list);
void community_utilities_clear_labelparamslist(GList* paramslist);
void community_utilities_clear_paramslist(GList* paramsList);
void community_utilities_clear_messagelist(GList* messages_);
int community_utilities_open_and_arrange_dir(const char* path, const char* suffix, char result[MAX_FILES_IN_DIR][MAX_FILE_NAME_LEN]);
void community_utilities_start_label_viewer(show_label_params* params);
gboolean community_utilities_parse_songs_from_file(const gchar* filename, GList** list, int type);
void community_utilities_limit_line_count(TextField *text);
void community_utilities_play_selected_loop(ClutterActor *actor);
gboolean community_utilities_make_buttons_from_directory(const char* path, ClutterContainer *list, GCallback function, int width, int height);

#endif
