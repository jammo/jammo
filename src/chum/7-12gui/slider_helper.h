#include <glib.h>

//For slider
#define RANGE 2.0
// X coordinates of playing area
#define RIGHT_X 599.0
#define LEFT_X 0.0
#define OCTAVE_ON_SCREEN ((RIGHT_X - LEFT_X)/RANGE)
#define BASE_FREQUENCY 130.81


int freq_to_coordinate(gfloat asked_freq);
float coordinate_to_freq(gint asked_coordinate);
