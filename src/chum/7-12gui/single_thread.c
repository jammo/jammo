#include "single_thread.h"
#include <stdio.h>
#include <tangle.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <meam/jammo-meam.h>
#include <meam/jammo-sample.h>
#include "../../meam/jammo-recording-track.h"
#include "../../meam/jammo-sample.h"
#include <meam/jammo-backing-track.h>
#include "../jammo.h"
#include "../../cem/cem.h"
#include "community_utilities.h"
#include "jammosongs.h"
#include "jammosounds.h"
#include "discuss.h"
#include "mysongs.h"
#include "profile_viewer.h"
#include "avatar.h"
#include "avatar_editor.h"

static ClutterContainer *messageList = 0;
static int selected_index = 0;
GList* st_messages = 0;
extern GList* paramsList;
static gchar* static_id;
static TextField text;
gchar filepath[250];
gchar* json;
static gboolean firstLaunch = TRUE;
static gboolean isrecording_voice_comment = FALSE;
static gboolean voice_comment_ready = FALSE;
gchar* recording_filename;
JammoSequencer* sequencer;
static JammoSequencer* static_sequencer;


static void on_recording_stopped(JammoSequencer* sequencer, gpointer user_data); 
static void add_message_to_list(gpointer data, int i);
static gboolean load_messages_from_server(const gchar* filename);
static gboolean parse_messages_from_file(const gchar* filename);
static gboolean single_thread_send_message(int type_);
static gboolean sendJammoMessage(const gchar* msg_filename);
static void load_voice_comment_from_server(ClutterActor* comment);
static void record_voice_comment();
static void stop_recording();


void start_thread_view(const gchar *id_, const gchar *params, int index_)
{
	GList *l = 0; //Iterator
	selected_index 	= index_;
	static_id = g_strdup_printf("%s", id_);  //FIXME: is this leaking?
	int numberOfMessages = 0;
	st_messages = 0;

	cem_add_to_log("starting thread...", J_LOG_DEBUG);
	printf("starting thread... params: %s\n", params);

	if(firstLaunch){
		json = g_strdup_printf("%s",params);
		firstLaunch = FALSE;
	}

	ClutterActor* thread_view = jammo_get_actor_by_id("single-thread-view");

	if(thread_view)
	{
		if(!load_messages_from_server(params))
			printf("Error happened while trying to load messages from server\n");

		clutter_actor_show(thread_view);

		if(strcmp(id_, "jammosongs") == 0){
			clutter_actor_hide(jammo_get_actor_by_id("text-answer-widgets"));
			clutter_actor_show(jammo_get_actor_by_id("select-comment-type"));

		}else if((strcmp(id_, "discuss") == 0) || (strcmp(id_, "mysongs") == 0)){
			text.text = jammo_get_actor_by_id("answer-field");
			text.lineCount = 6;
			text.max_characters = 500;
			text.handler_id = g_signal_connect_swapped(text.text, "text-changed", (GCallback)community_utilities_limit_line_count, &text);
			clutter_text_set_text(CLUTTER_TEXT(text.text), "");

		}else if(strcmp(id_, "helpdesk") == 0){
			text.text = jammo_get_actor_by_id("answer-field");
			text.lineCount = 6;
			text.max_characters = 500;
			text.handler_id = g_signal_connect_swapped(text.text, "text-changed", (GCallback)community_utilities_limit_line_count, &text);
			clutter_text_set_text(CLUTTER_TEXT(text.text), "");
		}
		numberOfMessages = g_list_length(st_messages);
		
		if(numberOfMessages <= 2 && numberOfMessages > 0) {
			messageList = CLUTTER_CONTAINER(jammo_get_actor_by_id("single-thread-list-unscrollable"));		
		}else if(numberOfMessages > 2){
			//TODO: Fix the scrolling in json
			messageList = CLUTTER_CONTAINER(jammo_get_actor_by_id("single-thread-list-scrollable"));		
		}		

		for(l = st_messages; l; l = l->next){
			add_message_to_list(l->data, numberOfMessages);
		}	

		//This is just a temporary fix for scrolling problem
		ClutterAction *action = tangle_actor_get_action_by_type(CLUTTER_ACTOR(messageList), TANGLE_TYPE_SCROLL_ACTION);
		if (action)
			tangle_object_animate(G_OBJECT(action), CLUTTER_EASE_IN_OUT_QUAD, 300, "offset-y", 0, NULL);
	}
	else
	cem_add_to_log("can't find single-thread-view", J_LOG_ERROR);
}


/*********************************
List filling functions

Used when all messages of one thread are shown
**********************************/
static void add_message_to_list(gpointer data, int i)  {
	message_type *msg = data;

		//int index = 0;
		int MAX = 500;
		int h = 80;
		
		profile_view_params *params = malloc(sizeof(profile_view_params));	
		paramsList = g_list_append(paramsList, params);
		
		params->user_id = msg->user_id;
		strcpy(params->parent_view, "single-thread");
		
		Avatar avatar = {0,0,0,0};
		guint32 serialized_avatar_image = avatar_give_serialized_image_for_this_id(msg->user_id);

		avatar_unserialize_image(&avatar, serialized_avatar_image);
		
		ClutterActor* messageBackground = 0;
		ClutterContainer *listContainer = CLUTTER_CONTAINER(clutter_group_new());
	
		ClutterColor text_color = { 0, 0, 0, 255 };
		ClutterColor line_color = { 144, 148, 140, 255 };

		ClutterActor* avatarBackground = clutter_group_new();
		gchar* avatarbg_filename = g_strdup_printf("%s/communitymenu/community_avatarbackground_small.png", DATA_DIR);		

		clutter_container_add_actor(CLUTTER_CONTAINER(avatarBackground), tangle_texture_new(avatarbg_filename));
		ClutterActor* writer = tangle_button_new_with_background_actor(avatarBackground);
		
		ClutterActor* name = clutter_text_new_full(TEXT_NORMAL, msg->sender, &text_color);
		ClutterActor* date = clutter_text_new_full(TEXT_NORMAL, msg->time, &text_color);
		//ClutterActor* avatar_ = clutter_text_new_full(TEXT_SMALL, msg->avatar, &text_color);

		set_avatar(&avatar, CLUTTER_ACTOR(avatarBackground));
		avatar_shrink(&avatar);

		clutter_container_add_actor(CLUTTER_CONTAINER(listContainer), writer);
		
		//index = (index+1) % 3;

		if(strcmp(msg->title, "") != 0){
			ClutterActor* title = clutter_text_new_full(TEXT_LARGE, msg->title, &text_color);
			clutter_actor_set_position(title, 80, 10);
			clutter_container_add_actor(listContainer, title);

			clutter_actor_set_position(name, 80, 40); 		
		}else{
			clutter_actor_set_position(name, 80, 20); 
		}
		g_free(avatarbg_filename);

		clutter_actor_set_size(name,((35*10)+30), 41);
		clutter_text_set_max_length(CLUTTER_TEXT(name), 20);
		clutter_container_add_actor(listContainer, name);

		clutter_actor_set_size(date,((20*10)), 20);
		clutter_text_set_max_length(CLUTTER_TEXT(date), 16);
		clutter_actor_set_position(date, 600, 10);
		clutter_container_add_actor(listContainer, date);
		
		g_signal_connect_swapped(writer, "clicked", G_CALLBACK(start_profile_view), (gpointer)params);

		if(msg->type == TEXT_COMMENT){

			ClutterActor* message = clutter_text_new_full(TEXT_SMALL, msg->msg, &text_color);
			clutter_text_set_line_wrap_mode(CLUTTER_TEXT(message), PANGO_WRAP_WORD_CHAR);
			//clutter_actor_set_size(message,(88*7)+40, ((MAX/88)*27));
			clutter_actor_set_width(message, (88*7)+40);
			clutter_text_set_max_length(CLUTTER_TEXT(message), MAX);
			clutter_actor_set_position(message, 80, 80); 
			clutter_container_add_actor(listContainer, message);
			h = (clutter_actor_get_height(avatarBackground) + pango_layout_get_line_count(clutter_text_get_layout(CLUTTER_TEXT(message)))*25);	
		}else if(msg->type == VOICE_COMMENT){
			
			strcpy(filepath, msg->filepath);
			
			gchar* play_button_filename = g_strdup_printf("%s/buttons/play-button.png", DATA_DIR);
			ClutterActor* play_button = tangle_button_new_with_background_actor(tangle_texture_new(play_button_filename)); 
			clutter_actor_set_position(play_button, 400, 1);
			clutter_actor_set_name(play_button, msg->filepath);
			clutter_container_add_actor(listContainer, play_button);

			g_signal_connect_swapped(CLUTTER_ACTOR(play_button), "clicked", G_CALLBACK(load_voice_comment_from_server), play_button);
			g_free(play_button_filename);
		}

		messageBackground = clutter_rectangle_new_with_color(&line_color);
		clutter_actor_set_size(messageBackground, 800, 2);

		clutter_actor_set_position(avatarBackground, 10, 1);
		clutter_container_add_actor(listContainer, avatarBackground);

		clutter_actor_set_position(messageBackground, 0, h+10);
		clutter_container_add_actor(listContainer, messageBackground);

		clutter_container_add_actor(messageList, CLUTTER_ACTOR(listContainer));
}


/*********************************
Functions for json
**********************************/

gboolean add_comment(TangleActor* actor, gpointer data) {

	if(clutter_text_get_text(CLUTTER_TEXT(text.text)) == NULL || (strcmp(clutter_text_get_text(CLUTTER_TEXT(text.text)), "") == FALSE))
	{
		cem_add_to_log("No text entered\n", J_LOG_DEBUG);
		return FALSE;
	}
	
	if(!single_thread_send_message(TEXT_COMMENT)){
		printf("Failed adding new message: error happened while sending message\n");
		return FALSE;
	}

	community_utilities_clear_messagelist(st_messages);
	
	community_utilities_clear_container_recursively(messageList);
	
	start_thread_view(static_id, json, 0);

	return TRUE;
}


gboolean add_voice_comment (TangleActor* actor, gpointer data) {

	if(!isrecording_voice_comment){
		record_voice_comment();
	}else if(isrecording_voice_comment){
		stop_recording();
	
		if(voice_comment_ready){
			
			if(!single_thread_send_message(VOICE_COMMENT)){
				printf("Failed adding new message: error happened while sending message\n");
				return FALSE;
			}else{
				community_utilities_clear_messagelist(st_messages);
				community_utilities_clear_container_recursively(messageList);
	
				start_thread_view(static_id, json, 0);
			}
		}
	}
	return TRUE;
}

gboolean text_comment_view(TangleActor* actor, gpointer data) {

	clutter_actor_hide(jammo_get_actor_by_id("select-comment-type"));
	clutter_actor_show(jammo_get_actor_by_id("text-answer-widgets"));
	text.text = jammo_get_actor_by_id("answer-field");
	text.lineCount = 6;
	text.max_characters = 500;
	text.handler_id = g_signal_connect_swapped(text.text, "text-changed", (GCallback)community_utilities_limit_line_count, &text);
	clutter_text_set_text(CLUTTER_TEXT(text.text), "");

	return TRUE;
}

gboolean end_single_thread(TangleActor* actor, gpointer data){
	
	community_utilities_clear_paramslist(paramsList);
	paramsList = 0;

	community_utilities_clear_messagelist(st_messages);
	community_utilities_clear_container_recursively(messageList);

	if(isrecording_voice_comment)
		stop_recording();

	if(voice_comment_ready){
		voice_comment_ready = FALSE;
		remove(recording_filename);
	}

	g_free(json);
	g_free(recording_filename);
	firstLaunch = TRUE;

	if (strcmp(static_id, "jammosongs") == 0)
	start_jammosongs();

	else if (strcmp(static_id, "jammosounds") == 0)
	start_jammosounds();

	else if (strcmp(static_id, "discuss") == 0)
	start_discussionthreads();
	
	else if(strcmp(static_id, "mysongs") == 0)
	start_mysongs();

	else {} //do nothing

	return TRUE;
}


/*********************************
Voice comment recording functions
**********************************/

static void record_voice_comment(){
	JammoBackingTrack* backing_track;
	JammoRecordingTrack* recording_track;

	gchar* backing_track_filename = g_strdup_printf("%s/voice_comment_backing.wav", DATA_DIR);

	recording_filename = g_strdup_printf("%s/last_recorded.ogg",configure_get_temp_directory());
	
	static_sequencer = jammo_sequencer_new();
	g_signal_connect(static_sequencer, "stopped", G_CALLBACK(on_recording_stopped), NULL);

	backing_track = jammo_backing_track_new(backing_track_filename);
	jammo_sequencer_add_track(static_sequencer, JAMMO_TRACK(backing_track)); //max length 30s
	
	recording_track = jammo_recording_track_new_with_pitch_detect(recording_filename);
	jammo_sequencer_add_track(static_sequencer, JAMMO_TRACK(recording_track));

	cem_add_to_log("Recording...", J_LOG_DEBUG);
	cem_add_to_log("Max length is 30 seconds.", J_LOG_DEBUG);

	isrecording_voice_comment = TRUE;

	jammo_sequencer_play(JAMMO_SEQUENCER(static_sequencer));
	
	g_free(backing_track_filename);
}

static void stop_recording() {
	isrecording_voice_comment = FALSE;
	voice_comment_ready = TRUE;
	jammo_sequencer_stop(JAMMO_SEQUENCER(static_sequencer));
}

static void on_recording_stopped(JammoSequencer* sequencer_, gpointer user_data) 
{
	cem_add_to_log("Stopped recording.", J_LOG_DEBUG);
	voice_comment_ready = TRUE;
	isrecording_voice_comment = FALSE;
}


/*********************************
Server functions
**********************************/

static void load_voice_comment_from_server(ClutterActor* comment_) {
	const char *voice_comment_filename = clutter_actor_get_name(comment_);

	if(!community_utilities_file_exist(voice_comment_filename)){
		//TODO: load voice_comment.ogg from server
		printf("Downloading file from server...\n");
	}else{
		//clutter_actor_set_name(song, save_path);
		community_utilities_play_selected_loop(comment_);
	}
}


static gboolean load_messages_from_server(const gchar* filename) {
	gboolean returnvalue = FALSE;
	
	//TODO: json from server and check that the file was received
	if(!community_utilities_load_file_from_server(filename)){
		printf("Load messages from server error: Couldn't download %s from server!\n", filename);
		return FALSE;
	}	
	GList* l = NULL; //Iterator
	int numberOfFound = 0;
	
	if(parse_messages_from_file(filename)){
		returnvalue = TRUE;
		numberOfFound = g_list_length(st_messages);
	}else{
		returnvalue = FALSE;
	}
	
	//TODO: Download all user json files from server
	if(returnvalue == TRUE){
		//TODO: download all user json files and check that files were received
		
		if(numberOfFound > 0){
	
			for (l = st_messages; l; l = l->next){
				message_type* temp = st_messages->data;
					
				//TODO: build user profile filename!
				gchar* user_profile = g_strdup_printf("user_%d.json", temp->user_id);
				
				if(!community_utilities_load_file_from_server(user_profile)){
					printf("Load messages from server error: Couldn't download user profiles from server!\n");
					returnvalue = FALSE;
				}
				g_free(user_profile);
				
			}//for
		}
	}	
	return returnvalue;
}


static gboolean sendJammoMessage(const gchar* msg_filename) {
	gboolean returnvalue = TRUE;
	
	//TODO: Check that files where successfully sended
		
	return returnvalue;
}


/*********************************
JSON Parsers and builders
**********************************/

static gboolean parse_messages_from_file(const gchar* filename) {
	int j;
	message_type *msg = 0;
	JsonParser *parser;
	JsonNode *root;
	JsonObject *object;
	JsonNode *node;
	
	//Init JSON Parser
	parser = json_parser_new ();
	g_assert (JSON_IS_PARSER (parser));
	GError *error = NULL;
	
	//Load file
	if (!json_parser_load_from_file (parser, filename, &error)){
		g_print ("Error: %s\n", error->message);
		g_error_free (error);
		g_object_unref (parser);
		return FALSE;
	}

	if(json_parser_get_root(parser) != NULL){
		root = json_parser_get_root (parser);
	}else{
		return FALSE;
	}
	if(JSON_NODE_TYPE(root) != JSON_NODE_OBJECT)
		return FALSE;

	object = json_node_get_object (root);
	if(object == NULL)
		return FALSE;
	
	//Fill array with messages
	JsonArray* msg_array;
	guint length_msgs = 0;
	node = json_object_get_member(object, "messages");
	
	if (node != NULL && JSON_NODE_TYPE (node) == JSON_NODE_ARRAY){
		msg_array =  json_node_get_array (node);
		length_msgs = json_array_get_length(msg_array);
		printf("Found %d messages\n\n", length_msgs);
	}else {
		printf("No messages found\n"); 
		return TRUE;
	}

	//Check message properties
	for (j = 0; j < length_msgs; j++) {

		JsonNode* msg_node;
		msg_node = json_array_get_element(msg_array, j);

		if (msg_node != NULL && (JSON_NODE_TYPE(msg_node) == JSON_NODE_OBJECT)){
			
			JsonObject* sub_object = json_node_get_object(msg_node);
			JsonNode *sub_node;

			gint msg_index = j;
			guint32 userid = 0;
			const gchar* sender = "";
			const gchar* title = "";
			const gchar* date = "";
			const gchar* msg_msg = "";
			const gchar* msg_filepath = "";
			int type = 0;

			printf("Found message!\n");
				
			sub_node = json_object_get_member(sub_object, "title");
			
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				title = json_node_get_string(sub_node);
				printf("title: '%s'\n", title);
			}
			sub_node = json_object_get_member(sub_object, "userid"); 
			
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				userid = (guint32)atoi(json_node_get_string(sub_node));				
				printf("userid: '%d'\n", userid);
			}	
			sub_node = json_object_get_member(sub_object, "type"); 
			
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				type = atoi(json_node_get_string(sub_node));			
				printf("type: '%d'\n", type);
			}		
			sub_node = json_object_get_member(sub_object, "filepath");
			
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				msg_filepath = json_node_get_string(sub_node);
				printf("filepath: '%s'\n", msg_filepath);
			}		
			sub_node = json_object_get_member(sub_object, "sender"); 
			
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				sender = json_node_get_string(sub_node);
				printf("sender: '%s'\n", sender);
			}			
			sub_node = json_object_get_member(sub_object, "message");
			
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				msg_msg = json_node_get_string(sub_node);
				printf("message: '%s'\n", msg_msg);
			}
			sub_node = json_object_get_member(sub_object, "date");
			
			if (sub_node != NULL && JSON_NODE_TYPE(sub_node) == JSON_NODE_VALUE){
				date = json_node_get_string(sub_node);
				printf("date: '%s'\n", date);
			}

			//Save information into struct
			msg = malloc(sizeof(message_type));
			
			msg->msg = malloc(strlen(msg_msg)+1*sizeof(gchar)); 
			strcpy(msg->msg, msg_msg);

			msg->index = msg_index; 
			msg->user_id = userid; 		
			strcpy(msg->title, title);
			strcpy(msg->sender, sender); 
			strcpy(msg->time, date);
			msg->type = type; 

			msg->filepath = malloc(strlen(msg_filepath)+1*sizeof(gchar));
			strcpy(msg->filepath, msg_filepath);
			
		} //Message is ready
		
		//Save struct into list
		st_messages = g_list_append(st_messages, (gpointer)msg);
		printf("\n");

	} //Next message

	g_object_unref (parser);

	return TRUE;
}


static gboolean single_thread_send_message(int type_) {
	printf("Single thread send message\n");
	
	//TODO: re-download messages json because it might have changed in meanwhile
	if(!community_utilities_load_file_from_server(json)){
		printf("Single_thread_send_message error: Couldn't download json file from server for writing!\n");
		return FALSE;
	}

	gboolean returnvalue = FALSE;
	gchar* msg = g_strdup_printf(" ");
	gchar* comment_filepath = g_strdup_printf(" ");
	gchar* newname = g_strdup_printf(" ");
	//gint index = selected_index;

	if(type_ == TEXT_COMMENT) {
		g_free(msg);
		msg = g_strdup_printf("%s", clutter_text_get_text(CLUTTER_TEXT(text.text)));
	}

	gint indexOfMessage = 0;

	char date [80];
	cem_get_time(date);
	guint32 user_id = TEST_USERID; //TODO: user_id =gems_profile_manager_get_userid(NULL)
	gchar* sender = avatar_give_username_for_this_id(user_id);
	int type = type_;
	gchar *title;
	
	//TODO: Make textfield for title
	if(g_list_length(st_messages) > 0){
		title = g_strdup_printf("RE: %s", ((message_type*)g_list_first(st_messages)->data)->title);
	}else{
		title = g_strdup_printf("New comment");
	}

	guint32 id = TEST_USERID; // TODO: use gems_profile_manager_get_userid(NULL)

	if(type_ == VOICE_COMMENT){
		g_free(comment_filepath);
		comment_filepath = g_strdup_printf("%s/last_recorded.ogg", configure_get_temp_directory());
		g_free(newname);
		newname = g_strdup_printf("%s/voice_comment_%d_%s.ogg", configure_get_temp_directory(),id, date);
		rename(comment_filepath, newname);
	}

	//check whether json file is empty
	FILE * fp;
	gboolean fileIsEmpty = FALSE;

	if ((fp = fopen(json, "r")) == NULL) {
		cem_add_to_log("Error happened while trying to read json", J_LOG_ERROR);
		printf("Error happened while trying to read a file\n");
		return FALSE;	
	}else{
		if(fgetc(fp) == EOF)
			fileIsEmpty = TRUE;		
	}
	fclose(fp);

	char * dest_filepath = g_strdup_printf("%s/temp.json", configure_get_temp_directory());

	if(fileIsEmpty){

		//write a new json file
		if ((fp = fopen(json, "w")) == NULL) {
			cem_add_to_log("Error happened while trying to write into json", J_LOG_ERROR);
			printf("Error happened while trying to write a file\n");
			return FALSE;
		}else{
				fprintf(fp, "{\n"); 
				fprintf(fp, "\"messages\": [\n");
				fprintf(fp, "{\n"); 
				fprintf(fp, "\"index\": \"%d\",\n", indexOfMessage);
				fprintf(fp, "\"userid\": \"%d\",\n", user_id);
				fprintf(fp, "\"sender\": \"%s\",\n", sender);
				fprintf(fp, "\"title\": \"%s\",\n", title);
				fprintf(fp, "\"message\": \"%s\",\n", msg);

				if(type == TEXT_COMMENT){
					fprintf(fp, "\"filepath\": \"%s\",\n", comment_filepath);
				}else if((type == VOICE_COMMENT)){
					fprintf(fp, "\"filepath\": \"%s\",\n", newname);
				}
				fprintf(fp, "\"type\": \"%d\",\n", type);
				fprintf(fp, "\"date\": \"%s\"\n", date);
    				fprintf(fp, "}\n");	      	 	
				fprintf(fp, "]\n"); 	
				fprintf(fp, "}");
		}
		fclose(fp);	
	}else{	
		//update json file
		FILE * pFile;
		FILE *destFile;
		int c, position_found = 0;
		
		//open source file
		if((pFile = fopen(json, "r")) == NULL){
			printf("Error happened while trying to open source file\n");
			return FALSE;
		}
		//open destination file
		if((destFile = fopen(dest_filepath, "w")) == NULL){
			printf("Error happened while trying to open destination file\n");
			return FALSE;
		}	
		//copy the file
		fprintf(destFile, "{\n"); 
		fprintf(destFile, "\"messages\": [");
		
		do{
			c = fgetc(pFile);
			if(ferror(pFile)) {
      				printf("Error reading source file\n");
      				return FALSE;
    			}
			if(c == '[')
				position_found = 1;
			
			if(position_found == 1 && c != '[' && c != ']'){
				if(c != ']') fputc(c, destFile);
				if(ferror(destFile)) {
      					printf("Error writing destination file\n");
      					return FALSE;
				}
			}
		}while(c != ']');	
	
		fclose(pFile);
		
		fprintf(destFile, ",\n");
		fprintf(destFile, "{\n"); 
		fprintf(destFile, "\"index\": \"%d\",\n", indexOfMessage);
		fprintf(destFile, "\"userid\": \"%d\",\n", user_id);
		fprintf(destFile, "\"sender\": \"%s\",\n", sender);
		fprintf(destFile, "\"title\": \"%s\",\n", title);
		fprintf(destFile, "\"message\": \"%s\",\n", msg);

		if(type_ == TEXT_COMMENT){
			fprintf(destFile, "\"filepath\": \"%s\",\n", comment_filepath);
		}else if((type_ == VOICE_COMMENT)){
			fprintf(destFile, "\"filepath\": \"%s\",\n", newname);
		}
		fprintf(destFile, "\"type\": \"%d\",\n", type);
		fprintf(destFile, "\"date\": \"%s\"\n", date);
    		fprintf(destFile, "}\n");	      	 	
		fprintf(destFile, "]\n"); 	
		fprintf(destFile, "}"); 	 	
		fclose(destFile);
			
		if(remove(json) != 0){ //We remove the original json
			printf("Error removing original file\n");
			return FALSE;
		}			
		rename(dest_filepath, json); //And rename temp file with the name of the original

	}//Json file is ready

	cem_add_to_log("Sending message to server...", J_LOG_DEBUG);
	
	if(sendJammoMessage(json))
		returnvalue = TRUE;

	g_free(comment_filepath);
	g_free(msg);
	g_free(newname);

	g_free(dest_filepath);
	g_free(title);
	g_free(sender);

	printf("End of single thread send message\n");
	return returnvalue;
}

