#ifndef __SINGLE_THREAD_H__
#define __SINGLE_THREAD_H__

#include <glib.h>

void start_thread_view(const gchar *id_, const gchar *params, int index_);

#endif /* __SINGLE_THREAD_H__ */
