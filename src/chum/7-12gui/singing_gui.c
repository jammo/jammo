/**singing_gui.c is part of JamMo.
License: GPLv2, read more from COPYING

This file is for clutter based gui.
This is for singing-view.json
 */
#include <tangle.h>
#include <glib-object.h>
#include "../../meam/jammo-sequencer.h"
#include "../../meam/jammo-backing-track.h"
#include "../../meam/jammo-recording-track.h"
#include "startmenu.h"
#include "../jammo.h"
#include "../jammo-game-level.h"
#include "sequencer.h"
#include "gamesmenu.h"
#include "communitymenu.h"

#include "../../cem/cem.h"
#include "../../configure.h"

//State of recording
static gboolean recording=FALSE;
static gboolean playing=FALSE;
static gchar* last_recorded_filename=NULL;

/*
 When recording stops:
 -just recorded file will be file for playback-track
 -disable recordind
*/
static void singing_gui_on_sequencer_stopped_recorded(JammoSequencer* sequencer, gpointer user_data) {
	g_signal_handlers_disconnect_by_func(sequencer,G_CALLBACK(singing_gui_on_sequencer_stopped_recorded),user_data);
	//printf("singing_gui_on_sequencer_stopped_recorded\n");
	recording=FALSE;
	//Change button (it could be already)
	tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("singing_record")),FALSE);

	JammoBackingTrack* play_track  = JAMMO_BACKING_TRACK(jammo_get_object_by_id("singing-track-for-playback")); //FIXME not general
	if (play_track && last_recorded_filename) {
		g_object_set(play_track,"filename",last_recorded_filename,NULL);
	}

	JammoRecordingTrack* rec_track  = JAMMO_RECORDING_TRACK(jammo_get_object_by_id("singing-track-for-recording")); //FIXME not general
	jammo_recording_track_set_enabled(rec_track, FALSE); //disabling
}

static void singing_gui_on_sequencer_stopped_played(JammoSequencer* sequencer, gpointer user_data) {
	g_signal_handlers_disconnect_by_func(sequencer,G_CALLBACK(singing_gui_on_sequencer_stopped_played),user_data);
	playing=FALSE;
	//Change button (it could be already)
	tangle_button_set_selected(TANGLE_BUTTON(jammo_get_actor_by_id("singing_play")),FALSE);
}

gboolean singing_play_clicked(TangleActor* actor, gpointer data){
	cem_add_to_log("Play-button on singing-view pressed",J_LOG_USER_ACTION);
	JammoSequencer* static_sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));

	if (playing) {
		cem_add_to_log("Stop-button on singing-view pressed",J_LOG_USER_ACTION);
		jammo_sequencer_stop (static_sequencer); //this will trigger singing_gui_on_sequencer_stopped_played
		return TRUE;
	}

	if (recording) {
		cem_add_to_log("  during recording",J_LOG_USER_ACTION);
		jammo_sequencer_stop (static_sequencer); //this will trigger singing_gui_on_sequencer_stopped_recorded
	}

	JammoPlayingTrack* track  = JAMMO_PLAYING_TRACK(jammo_get_object_by_id("singing-track-for-playback")); //FIXME: not general
	jammo_playing_track_set_muted(track, FALSE);

	playing=TRUE;
	g_signal_connect(static_sequencer, "stopped", G_CALLBACK(singing_gui_on_sequencer_stopped_played), NULL);
	jammo_sequencer_play (static_sequencer);

	return TRUE;
}


gboolean singing_record_clicked (TangleActor* actor, gpointer data){
	cem_add_to_log("Record-button on singing-view pressed",J_LOG_USER_ACTION);
	JammoSequencer* static_sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));

	if (recording) {
		cem_add_to_log("(rec-)stop-button on singing-view pressed",J_LOG_USER_ACTION);
		jammo_sequencer_stop (static_sequencer); //this will trigger singing_gui_on_sequencer_stopped_recorded
		return TRUE;
	}

	if (playing) {
		jammo_sequencer_stop (static_sequencer); //this will trigger singing_gui_on_sequencer_stopped_played
	}

	//TODO, there should be some prelude, or precount with metronome


	//Generate filename based on timestamp
	char timestamp_now [80];
	cem_get_time(timestamp_now);

	last_recorded_filename = g_strdup_printf("%s/audio_%s.wav",configure_get_jammo_directory(), timestamp_now);
	printf("using filename '%s' for recording\n",last_recorded_filename);

	JammoRecordingTrack* rec_track  = JAMMO_RECORDING_TRACK(jammo_get_object_by_id("singing-track-for-recording")); //FIXME not general
	g_object_set(rec_track,"filename",last_recorded_filename,NULL);
	//setting filename automatically enable recording

	JammoPlayingTrack* track  = JAMMO_PLAYING_TRACK(jammo_get_object_by_id("singing-track-for-playback")); //FIXME: not general
	jammo_playing_track_set_muted(track, TRUE);

	recording=TRUE;
	g_signal_connect(static_sequencer, "stopped", G_CALLBACK(singing_gui_on_sequencer_stopped_recorded), NULL);
	jammo_sequencer_play (static_sequencer);

	return TRUE;
}


gboolean singing_gosequencer_clicked (TangleActor* actor, gpointer data) {
	cem_add_to_log("Backtoseq-button on singing-view pressed",J_LOG_USER_ACTION);

	//hide this
	ClutterActor* view;
	view = jammo_get_actor_by_id("singing-view");
	tangle_actor_hide_animated(TANGLE_ACTOR(view));

	//We want disconnect all on-stopped-handlers.
	JammoSequencer* static_sequencer = JAMMO_SEQUENCER(jammo_get_object_by_id("fullsequencer-the-sequencer"));
	jammo_sequencer_stop (static_sequencer);

	//For next time
	recording=FALSE;
	playing=FALSE;

	JammoRecordingTrack* rec_track  = JAMMO_RECORDING_TRACK(jammo_get_object_by_id("singing-track-for-recording")); //FIXME not general
	jammo_recording_track_set_enabled(rec_track, FALSE); //disabling

	return_to_fullsequencer_gui();

	return TRUE;
}

