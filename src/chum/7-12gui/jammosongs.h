/*
License: GPLv2, read more from COPYING

This file contains the functionality of JamMo Songs menu.
 */

#ifndef JAMMOSONGS_H_
#define JAMMOSONGS_H_

#include "community_utilities.h"

void start_jammosongs();
gboolean end_jammosongs(TangleButton *tanglebutton, gpointer none);
gboolean add_song_to_list(gpointer data, guint32 user_id);
void review_selected_song(ClutterActor*);
gboolean load_songs_from_server();
void load_song_from_server(ClutterActor * song);

#endif 
