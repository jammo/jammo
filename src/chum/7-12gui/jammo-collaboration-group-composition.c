/*
 * jammo-collaboration-group-composition.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 Lappeenranta University of Technology
 *
 * Authors: Mikko Gynther <mikko.gynther@lut.fi>
 *	    Tommi Kallonen <tommi.kallonen@lut.fi>
 */

#include "../jammo-collaboration-game.h"
#include "jammo-collaboration-group-composition.h"
#include "../jammo.h"
G_DEFINE_TYPE(JammoCollaborationGroupComposition, jammo_collaboration_group_composition, JAMMO_TYPE_COLLABORATION_GAME);

#include "../../meam/jammo-sequencer.h"
#include "../../meam/jammo-backing-track.h"
#include "../jammo-editing-track-view.h"
#include "../../meam/jammo-midi.h"
#include "../../meam/jammo-slider-event.h"
#include "../../meam/jammo-loop.h"
#include "../../gems/gems_definitions.h"
#include "../../gems/groupmanager.h"
#include "../../gems/gems.h"
#include "../../gems/collaboration.h"
#include "sequencer.h"
#include "sequencer_loop.h"


#define GROUPCOMPOSITIONMAXPLAYERS 4

enum {
	PROP_0/*,
	PROP_ADD_PROPERTIES_HERE*/
};

struct _JammoCollaborationGroupCompositionPrivate {
	// these have to be filled before calling group composition start function
	int number_of_players;
	guint peer_user_id[3];
	guint own_track_id;
	guint added_tracks;

	// following are used automatically and do not need to be filled
	int state;
	int viewcounter;
};

// states for group composition
enum {
	GROUPCOMPSTATE_INIT=0,
	GROUPCOMPSTATE_ERROR
};


// callbacks for GEMS
static void jammo_collaboration_group_composition_midi_remote(GObject * game, guint user_id, gint16 instrument_id, GemsEventListOperation operation, GList * list) {

	GList* temp;
	JammoMidiEvent* n;

	// TODO add list to correct track	
	printf("jammo-collaboration-group-composition midi callback stub, user id %u, instr id %d, operation %d\n", user_id, instrument_id, operation);
  
  printf("Midi list:\n");
	for (temp = list; temp; temp = temp->next) {
		n = (temp->data);
		printf("  %llu ns: note:%d ,type: %s \n", (unsigned long long)n->timestamp,n->note,n->type==0? "start":"stop");
	}
}

static void jammo_collaboration_group_composition_slider_remote(GObject * game, guint user_id, gint16 instrument_id, GemsEventListOperation operation, GList * list) {

		GList* temp;
		JammoSliderEvent* n;

	// TODO add list to correct track
		printf("jammo-collaboration-group-composition slider callback stub, user id %u, instr id %d, operation %d\n", user_id, instrument_id, operation);
	  printf("Slider event list:\n");
		for (temp = list; temp; temp = temp->next) {
			n = (temp->data);
			printf("  %llu ns: freq:%f ,type: %d \n", (unsigned long long)n->timestamp,n->freq,n->type);
		}
}

static void jammo_collaboration_group_composition_add_jammo_sample_button_remote(GObject * game, guint user_id, guint loop_id, guint slot) {


	ClutterActor* track_view = jammo_collaboration_game_user_id_to_track(JAMMO_COLLABORATION_GAME(game),user_id);
	printf("remote sample, uid %u, loop_id %u, slot %u\n", user_id, loop_id, slot);

	ClutterActor* sample_button =sequencer_loop_give_sample_button_for_this_id(loop_id);



	jammo_editing_track_view_add_jammo_sample_button(JAMMO_EDITING_TRACK_VIEW(track_view), JAMMO_SAMPLE_BUTTON(sample_button), slot);
}

static void jammo_collaboration_group_composition_remove_jammo_sample_button_remote(GObject * game, guint user_id, guint slot) {

	ClutterActor* track_view = jammo_collaboration_game_user_id_to_track(JAMMO_COLLABORATION_GAME(game),user_id);
	jammo_editing_track_view_remove_jammo_sample_button_from_slot(JAMMO_EDITING_TRACK_VIEW(track_view), slot);
}

static void jammo_collaboration_group_composition_loop_sync_remote(GObject * game, guint32 user_id, GList * list) {
	GList* temp;
	JammoLoop* n;

	// TODO clear existing sample buttons

	//TODO: loop_id --> sample_button

	//TODO: add loops
	printf("jammo-collaboration-pair-composition loop sync stub, user id %u\n", user_id);
  printf("Loop list:\n");
	for (temp = list; temp; temp = temp->next) {
		n = (temp->data);
		printf("  loop_id %u, slot %u\n", n->loop_id,n->slot);
	}
}

JammoCollaborationGroupComposition* jammo_collaboration_group_composition_new() {
	return JAMMO_COLLABORATION_GROUP_COMPOSITION(g_object_new(JAMMO_TYPE_COLLABORATION_GROUP_COMPOSITION, NULL));
}


static void jammo_collaboration_group_composition_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	//JammoCollaborationGroupComposition * collaboration_group_composition;
	//collaboration_group_composition = JAMMO_COLLABORATION_GROUP_COMPOSITION(object);

	switch (prop_id) {
		/*case PROP_BACKING_TRACK_FILENAME:
			g_assert(!collaboration_composition->priv->filename);
			collaboration_composition->priv->filename = g_strdup(g_value_get_string(value));
			break;*/
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_collaboration_group_composition_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
	//JammoCollaborationGroupComposition * collaboration_group_composition;
	//collaboration_group_composition = JAMMO_COLLABORATION_GROUP_COMPOSITION(object);

	switch (prop_id) {
		/*case PROP_FILENAME:
			g_value_set_string(value, backing_track->priv->filename);
			break;*/
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

// implementations for base class functions

// implementation for teacher exit
void jammo_collaboration_group_composition_teacher_exit(JammoCollaborationGame * collaboration_game) {
	//JammoCollaborationGroupComposition * group_composition;
	//group_composition = JAMMO_COLLABORATION_GROUP_COMPOSITION(collaboration_game);
	// TODO implement teacher exit
	printf("TODO: jammo_collaboration_group_composition_teacher_exit called\n");
}

// create_song_file
// return values 0 success, 1 can not open file
int jammo_collaboration_group_composition_create_song_file(JammoCollaborationGame * collaboration_game) {
	//JammoCollaborationGroupComposition * group_composition;
	//group_composition = JAMMO_COLLABORATION_GROUP_COMPOSITION(collaboration_game);
	// TODO implement
	printf("TODO: jammo_collaboration_group_composition_create_song_file called\n");

	return 0;
}

static GObject* jammo_collaboration_group_composition_constructor(GType type, guint n_properties, GObjectConstructParam* properties) {
	GObject* object;
	JammoCollaborationGroupComposition * collaboration_group_composition;
	
	object = G_OBJECT_CLASS(jammo_collaboration_group_composition_parent_class)->constructor(type, n_properties, properties);

	collaboration_group_composition = JAMMO_COLLABORATION_GROUP_COMPOSITION(object);
	collaboration_group_composition->priv->state=GROUPCOMPSTATE_INIT;

	return object;
}


static void jammo_collaboration_group_composition_finalize(GObject* object) {
	//JammoCollaborationGroupComposition* collaboration_group_composition;
	//collaboration_group_composition = JAMMO_COLLABORATION_GROUP_COMPOSITION(object);

	G_OBJECT_CLASS(jammo_collaboration_group_composition_parent_class)->finalize(object);
}

static void jammo_collaboration_group_composition_dispose(GObject* object) {
	G_OBJECT_CLASS(jammo_collaboration_group_composition_parent_class)->dispose(object);
}

static void jammo_collaboration_group_composition_class_init(JammoCollaborationGroupCompositionClass* collaboration_group_composition_class) {
	printf("class init\n");
	GObjectClass* gobject_class = G_OBJECT_CLASS(collaboration_group_composition_class);
	JammoCollaborationGameClass* collaboration_game_class = JAMMO_COLLABORATION_GAME_CLASS(collaboration_group_composition_class);

	gobject_class->constructor = jammo_collaboration_group_composition_constructor;
	gobject_class->finalize = jammo_collaboration_group_composition_finalize;
	gobject_class->dispose = jammo_collaboration_group_composition_dispose;
	gobject_class->set_property = jammo_collaboration_group_composition_set_property;
	gobject_class->get_property = jammo_collaboration_group_composition_get_property;

	collaboration_game_class->teacher_exit = jammo_collaboration_group_composition_teacher_exit;
	collaboration_game_class->create_song_file = jammo_collaboration_group_composition_create_song_file;

	// set callbacks for GEMS
	gems_set_callback_midi_list(jammo_collaboration_group_composition_midi_remote);
	gems_set_callback_slider_event_list(jammo_collaboration_group_composition_slider_remote);
	gems_set_callback_add_loop(jammo_collaboration_group_composition_add_jammo_sample_button_remote);
	gems_set_callback_remove_loop(jammo_collaboration_group_composition_remove_jammo_sample_button_remote);
	gems_set_callback_loop_sync(jammo_collaboration_group_composition_loop_sync_remote);

	g_type_class_add_private(gobject_class, sizeof(JammoCollaborationGroupCompositionPrivate));
}

static void jammo_collaboration_group_composition_init(JammoCollaborationGroupComposition* collaboration_group_composition) {
	collaboration_group_composition->priv = G_TYPE_INSTANCE_GET_PRIVATE(collaboration_group_composition, JAMMO_TYPE_COLLABORATION_GROUP_COMPOSITION, JammoCollaborationGroupCompositionPrivate);
}

// get and set functions

int jammo_collaboration_group_composition_get_number_of_players(JammoCollaborationGroupComposition * collaboration_group_composition) {
	return collaboration_group_composition->priv->number_of_players;
}

void jammo_collaboration_group_composition_set_number_of_players(JammoCollaborationGroupComposition * collaboration_group_composition, int number_of_players) {
	if (collaboration_group_composition->priv->state==GROUPCOMPSTATE_INIT) {
		if (number_of_players > collaboration_group_composition->priv->number_of_players && number_of_players <= GROUPCOMPOSITIONMAXPLAYERS) {
			collaboration_group_composition->priv->number_of_players=number_of_players;
		
		}
		else {
			g_warning("Could not change number of players to '%d'", number_of_players);
		}
	}
	else {
		g_warning("Can not change number of players in current state '%d'", collaboration_group_composition->priv->state);
	}
}

// functions needed in group composition
// functions for sending event lists
void jammo_collaboration_group_composition_send_midi_to_group (JammoCollaborationGroupComposition * group_composition) {
	// TODO get own track from view and send it
	GList * list =NULL;
	gems_midi_events_to_track(GEMS_EVENTLIST_REPLACE, 0, list);
}

void jammo_collaboration_group_composition_send_slider_to_group (JammoCollaborationGroupComposition * group_composition) {
	// TODO get own track from view and send it
	GList * list =NULL;
	gems_slider_events_to_track(GEMS_EVENTLIST_REPLACE, 0, list);
}

void jammo_collaboration_group_composition_loop_sync (JammoCollaborationGroupComposition * group_composition) {
	// TODO get own loops from view and send them
	GList * list =NULL;
	gems_loop_sync(list);
	jammo_loop_free_glist(&list);
}

// prototypes
void jammo_collaboration_group_composition_on_sequencer_stopped_recording(JammoSequencer* sequencer, gpointer data);
int create_mix_of_group_composition(gpointer data);
void exit_group_composition(JammoCollaborationGroupComposition * collaboration_group_composition);
static void clean_up(JammoCollaborationGroupComposition * collaboration_group_composition);

void jammo_collaboration_group_composition_on_sequencer_stopped_recording(JammoSequencer* sequencer, gpointer data) {

	// uncomment to use object
	/*JammoCollaborationGroupComposition * collaboration_group_composition = (JammoCollaborationGroupComposition *)data;*/

	// uncomment adding callback to start generating mix of group composition
	/*g_timeout_add_full(G_PRIORITY_DEFAULT,100,(GSourceFunc)create_mix_of_group_composition,collaboration_group_game,NULL);*/
	
}

int create_mix_of_group_composition(gpointer data) {
	static JammoCollaborationGroupComposition * collaboration_group_composition;

	collaboration_group_composition = (JammoCollaborationGroupComposition *)data;

	// TODO check states and do not change mode multiple times
	if (0) {
		JammoSequencer * sequencer;
		g_object_get(collaboration_group_composition,"sequencer",&sequencer, NULL);
		char * mix_location;
		g_object_get(collaboration_group_composition,"mix-location",&mix_location, NULL);		

		// change sequencer to file mode
		if (jammo_sequencer_set_output_filename(sequencer, mix_location)==0) {
			printf("sequencer changed to file mode successfully\n");
		}

		// playback will now create a file
		jammo_sequencer_play(sequencer);
	}

	if (1 /* stopped */){

		exit_group_composition(collaboration_group_composition);
		return 0;
	}

	// still generating mix file
	return 1;
}

// function for exiting group_composition
// this can be used to call main menu or something else
void exit_group_composition(JammoCollaborationGroupComposition * collaboration_group_composition) {
	// unref sequencer and tracks
	clean_up(collaboration_group_composition);
}

int jammo_collaboration_group_composition_start(JammoCollaborationGroupComposition * collaboration_group_composition) {

	// set group composition to gems
	gems_components * gems_data = gems_get_data();
	gems_data->service_collaboration->collaboration_game=G_OBJECT(collaboration_group_composition);

	// this is how to add callback functions to main loop
	/*g_timeout_add_full(G_PRIORITY_DEFAULT,100,(GSourceFunc)create_mix_of_group_composition,collaboration_group_game,NULL);*/

  return 0;
}

// with this function sequencer and tracks can be destroyed during the game without
// creating a new game
// unrefs sequencer and tracks which should cause deletion of those objects
static void clean_up(JammoCollaborationGroupComposition * collaboration_group_composition) {
	JammoSequencer * sequencer;
	g_object_get(collaboration_group_composition,"sequencer",&sequencer, NULL);

	// unref sequencer. this unrefs the tracks also and should delete all objects
	g_object_unref(sequencer);
	g_object_set(G_OBJECT(collaboration_group_composition),"sequencer",NULL, NULL);
}

void jammo_collaboration_group_composition_new_view(GObject * game, ClutterActor * view)
{
	int noviews = jammo_collaboration_game_viewcount(JAMMO_COLLABORATION_GAME(game));
	JammoCollaborationGroupComposition * collaboration_group_composition = JAMMO_COLLABORATION_GROUP_COMPOSITION(game);

	if(collaboration_group_composition->priv->viewcounter != collaboration_group_composition->priv->own_track_id)
	{
		jammo_collaboration_game_add_view(JAMMO_COLLABORATION_GAME(game), collaboration_group_composition->priv->peer_user_id[noviews], view);
		jammo_editing_track_view_set_editing_enabled(JAMMO_EDITING_TRACK_VIEW(view),FALSE);
		int n_slots;
		g_object_get(view, "n_slots", &n_slots, NULL);
		g_object_set(view, "disabled-slots-begin", n_slots, NULL);
		const ClutterColor background_color = {0,0,255,120};
		tangle_widget_set_background_color(TANGLE_WIDGET(view), &background_color);
	}
	collaboration_group_composition->priv->viewcounter++;
}

//Teacher forced a new game
void group_composition_remote_game_starter(gpointer data, gint16 track_id)
{
	gchar* json_name = NULL;
	gchar* filename = NULL;
	gint i=0;
	
	
	JammoCollaborationGroupComposition * gc = jammo_collaboration_group_composition_new();
	gc->priv->viewcounter=0;
	gems_group_info * group = (gems_group_info*)data;
	for(;i<3;i++)
	{
		if(group->peers[i] != 0)
			gc->priv->peer_user_id[i] = group->peers[i];
		else
			break;
	}
	gc->priv->number_of_players = i+1;	
	gc->priv->own_track_id = track_id;

	json_name = g_strdup_printf("%d-players.json",gc->priv->number_of_players);
	if (!(filename = tangle_lookup_filename(json_name))) {
		g_critical("Cannot find a JSON file '%s'.\n", json_name);
	}


	sequencer_set_group_composition(gc);
	sequencer_start_with_file(filename);
	jammo_collaboration_group_composition_start(gc);
	
	

	g_free(json_name);
	g_free(filename);
}

