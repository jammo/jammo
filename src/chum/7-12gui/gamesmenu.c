/**sequencer.c is part of JamMo.
License: GPLv2, read more from COPYING

This file is for clutter based gui.
This is the sequencer.
 */
#include <tangle.h>
#include <glib-object.h>
#include "gamesmenu.h"
#include "startmenu.h"
#include "sequencer.h"
#include "../jammo.h"

#include "../jammo-game-task.h"
#include "../jammo-game-level.h"
#include "../jammo-game-level-view.h"

#include "../../cem/cem.h"
#include "../jammo-mentor.h"

/*  TODO: use signal in json
//return to startmenu
static gboolean goto_startmenu(ClutterActor *actor, ClutterEvent *event, gpointer data) {
	clutter_actor_hide(gamesmenu_group);
	show_startmenu();
	return TRUE;
}*/

static gboolean instructions_interrupted=FALSE;
static void gamesmenu_welcome_ready(){
	instructions_interrupted=TRUE;
	//If interrupted, mentor can be in wrong place
	g_object_set(jammo_mentor_get_default(), "rotation-angle-y", 0.0,"x", 0.0,NULL);
}
static void gamesmenu_welcomeG(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
		gamesmenu_welcome_ready();
}

static void gamesmenu_welcomeF(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	if (instructions_interrupted) {
		gamesmenu_welcome_ready();
		return;
	}
	jammo_clutter_actor_flash(jammo_get_actor_by_id("gamesmenu-fake-score-bar"));
	//mentor: on the right side of the screen there are score bar
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "level1_startsB.spx", gamesmenu_welcomeG,NULL);
}

static void gamesmenu_welcomeE(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	if (instructions_interrupted) {
		gamesmenu_welcome_ready();
		return;
	}
	jammo_clutter_actor_flash(jammo_get_actor_by_id("start_singing_game"));
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "here_singing_game.spx", gamesmenu_welcomeF,NULL);
}

static void gamesmenu_welcomeD(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	if (instructions_interrupted) {
		gamesmenu_welcome_ready();
		return;
	}
	ClutterAnimation* animation;
	ClutterTimeline* timeline;
	//Restore mentor to own place
	animation = clutter_actor_animate(CLUTTER_ACTOR(mentor), CLUTTER_EASE_IN_QUAD, 3000, "x", 0.0, "rotation-angle-y", 0.0, NULL);

	timeline = clutter_animation_get_timeline(animation);
	g_signal_connect(timeline, "completed", G_CALLBACK(gamesmenu_welcomeE), NULL);
}

static void gamesmenu_welcomeC(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	if (instructions_interrupted) {
		gamesmenu_welcome_ready();
		return;
	}
	//Instrument game is center of the screen -> mentor moves and gives space
	clutter_actor_animate(CLUTTER_ACTOR(mentor), CLUTTER_EASE_IN_QUAD, 1000, "x", 1000.0, NULL);

	jammo_clutter_actor_flash(jammo_get_actor_by_id("start_instrument_game"));
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "here_instrument_game.spx", gamesmenu_welcomeD,NULL);
}

static void gamesmenu_welcomeB(JammoMentor* mentor, const gchar* speech, gboolean interrupted, gpointer sequencer){
	if (instructions_interrupted) {
		gamesmenu_welcome_ready();
		return;
	}
	jammo_clutter_actor_flash(jammo_get_actor_by_id("start_composition_game"));
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "here_composing_game.spx", gamesmenu_welcomeC,NULL);
}

void gamesmenu_start_instructions_for_level1() {
	clutter_actor_show(CLUTTER_ACTOR(jammo_mentor_get_default()));
	if (instructions_interrupted) {
		gamesmenu_welcome_ready();
		return;
	}
	//Start instructions with mentor flipped vertically
	g_object_set(jammo_mentor_get_default(), "rotation-angle-y", 180.0,"x", 850.0,NULL);
	jammo_mentor_speak_with_callback(jammo_mentor_get_default(), "level1_starts.spx", gamesmenu_welcomeB,NULL);
}

void gamesmenu_show() {
	cem_add_to_log("Gamesmenu starting",J_LOG_DEBUG);

	ClutterActor* mainview;

	mainview = jammo_get_actor_by_id("main-views-widget");
	clutter_container_foreach(CLUTTER_CONTAINER(mainview), CLUTTER_CALLBACK(tangle_actor_hide_animated), NULL);

	clutter_actor_show (jammo_get_actor_by_id("fullsequencer-view")); //contains only templates (show works better than load_now() )
	clutter_actor_show (jammo_get_actor_by_id("gamesmenu-view"));
}

void gamesmenu_start_first_task_on_this_level(const char* levelname) {
	gchar* message = g_strdup_printf("Starting level: '%s'",levelname);
	cem_add_to_log(message, J_LOG_DEBUG);
	g_free(message);
	
	clutter_actor_hide(jammo_get_actor_by_id("gamesmenu-view"));
	//tangle_view_unload(TANGLE_VIEW(jammo_get_actor_by_id("gamesmenu-view")));

	gchar* view_name = g_strdup_printf("%s-view",levelname);
	ClutterActor* levelview = jammo_get_actor_by_id(view_name);

	if (levelview==NULL){
		message = g_strdup_printf("levelview '%s' was NULL",view_name);
		cem_add_to_log(message, J_LOG_ERROR);
		g_free(message);
	}
	g_free(view_name);

	clutter_actor_set_name(levelview,levelname);
	start_fullsequencer_gui(levelview);

}

/*
name of button should be in format "level1".
*/
gboolean start_level(TangleButton* button, gpointer data) {
	instructions_interrupted=TRUE;
	jammo_mentor_shut_up(jammo_mentor_get_default());
	gamesmenu_start_first_task_on_this_level(clutter_actor_get_name(CLUTTER_ACTOR(button)));

	return TRUE;
}

gboolean start_instrument_game(TangleButton* button, gpointer data) {
	instructions_interrupted=TRUE;
	jammo_mentor_shut_up(jammo_mentor_get_default());

	clutter_actor_show (jammo_get_actor_by_id("instrumentmenu-view"));
	return TRUE;
}

void all_levels_completed(JammoGameLevel* game_level) {
	g_print("MENTOR: Now you have gone through all game levels! Feel free to do whatever you want now on...\n");
}
