#ifndef RECORD_COVER_TOOL_H
#define RECORD_COVER_TOOL_H

#include <tangle.h>

void start_record_cover_tool(const gchar* filepath);
gboolean record_cover_ok_pressed(TangleActor* actor, gpointer data);
gboolean record_cover_cancel_pressed(TangleActor* actor, gpointer data);
void end_record_cover_tool();
void record_cover_change_image(TangleButton *button);
void change_record_name(gpointer text);
void change_composer_name(gpointer text);

#endif
