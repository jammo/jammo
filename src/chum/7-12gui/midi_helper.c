/**midi_helper.c is part of JamMo.
License: GPLv2, read more from COPYING

This file is for midi editor
Midi editor (and co.) are using these functions, but these do not have any midi related things.
*/

#include <tangle.h>
#include <glib-object.h>
#include <ctype.h>
#include <string.h>
#include <clutter/clutter.h>

static ClutterColor color_white = { 255,255,255,255 };
static ClutterColor color_blue =  { 0, 0, 0xff, 0xff };
static ClutterColor color_light_blue = { 134, 175, 231, 255 };
static ClutterColor color_black = { 0, 0, 0, 0xff };
static ClutterColor color_green = { 0, 0xff, 0, 0xff };
static ClutterColor color_yellow = { 255, 255, 0, 255 };
static ClutterColor color_light_blue_second = { 176, 204, 255, 255 };

/**
 * Returns given color, color name is passed as char* table
 *
 * @param   table
 * @return  "color_name"
 */
ClutterColor* midi_editor_get_color(const char *table) {
	if (strcmp(table, "white")==0) {
		return &color_white;
	} else if (strncmp(table, "blue",4)==0) {
		return &color_blue;
	} else if (strncmp(table, "light_blue",10)==0) {
		return &color_light_blue;
	} else if (strncmp(table, "black",5)==0) {
		return &color_black;
	} else if (strncmp(table, "green",5)==0) {
		return &color_green;
	} else if (strncmp(table, "yellow",6)==0) {
		return &color_yellow;
	} else if (strncmp(table, "light_blue_second",17)==0) {
		return &color_light_blue_second;
	} else {
		printf("Color not found! - function midi_editor_get_color() called!");
		return &color_white; // default color
	}
}



/**
 * Scales the button back to normal size to simulate button's release
 *
 * @param actor - actor to scale
 * @return anim - scaling animation to normal size
 */
ClutterAnimation * midi_editor_animate_release(ClutterActor *actor) {
	ClutterAnimation *anim = clutter_actor_animate (actor,
			CLUTTER_LINEAR,
			100,
			"scale-x",
			1.0,
			"scale-y",
			1.0,
			"scale-gravity",
			CLUTTER_GRAVITY_CENTER,
			NULL);
	return anim;
}

/**
 * Scales the button to smaller size to simulate button pressing
 *
 * @param actor - actor to scale
 * @return anim - scaling animation to smaller size
 */
ClutterAnimation * midi_editor_animate_press(ClutterActor *actor) {

	//This is a workaround to set the default gravity to center before animating
	clutter_actor_set_scale_with_gravity(actor, 1.0, 1.0, CLUTTER_GRAVITY_CENTER);

	ClutterAnimation *anim = clutter_actor_animate (actor,
			CLUTTER_LINEAR,
			100,
			"scale-x",
			0.8,
			"scale-y",
			0.8,
			"scale-gravity",
			CLUTTER_GRAVITY_CENTER,
			NULL);
	return anim;
}



/**
 * Used to move actor to coordinates x and y while scaling the actor to values scaleX and scaleY
 *
 * @param actor
 * @param scaleX
 * @param scaleY
 * @param x
 * @param y
 * @return anim - scaling and moving animation
 */
 ClutterAnimation * midi_editor_animate_zoom(ClutterActor *actor, double scaleX, double scaleY, gfloat x, gfloat y) {
	ClutterAnimation *anim = clutter_actor_animate (CLUTTER_ACTOR(actor),
			CLUTTER_LINEAR,
			700,
			"scale-x",
			scaleX,
			"scale-y",
			scaleY,
			"x", x, "y", y,
			NULL);

	return anim;
}

/**
 * Fades the actor in or out in a given time
 *
 * @param actor
 * @param duration
 * @param opacity
 * @return anim - fading animation
 */
 ClutterAnimation * midi_editor_animate_opacity(ClutterActor *actor, guint duration, guint opacity) {
	ClutterAnimation *anim = clutter_actor_animate(CLUTTER_ACTOR(actor),
			CLUTTER_LINEAR,
			duration,
			"opacity", opacity,
			NULL);

	return anim;
}

/**
 * This is function which can draw different texts in midi editor
 *
 * @param myText
 * @param x
 * @param y
 * @param myContainer
 * @param myColor
 */
void draw_text(const gchar *myText, gfloat x, gfloat y, ClutterActor* myContainer, ClutterColor* myColor){
	/* Add a non-editable text actor to the stage: */
	ClutterActor *text = clutter_text_new ();

	/* Setup text properties */
	clutter_text_set_color (CLUTTER_TEXT (text), myColor);
	clutter_text_set_text (CLUTTER_TEXT (text), myText);
	clutter_text_set_font_name (CLUTTER_TEXT (text), "Sans 12");
	clutter_text_set_editable (CLUTTER_TEXT (text), FALSE);
	clutter_text_set_line_wrap (CLUTTER_TEXT (text), FALSE);

	clutter_actor_set_position (text, x, y);
	clutter_container_add_actor (CLUTTER_CONTAINER (myContainer), text);
	clutter_actor_show (text);

}
