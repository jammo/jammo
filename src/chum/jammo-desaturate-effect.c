/* jammo-desaturate-effect.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2011 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "jammo-desaturate-effect.h"

G_DEFINE_TYPE(JammoDesaturateEffect, jammo_desaturate_effect, CLUTTER_TYPE_SHADER_EFFECT);

enum {
	PROP_0,
	PROP_FACTOR
};

struct _JammoDesaturateEffectPrivate {
	gfloat factor;
};

/* The shader source is based on clutter-desaturation-effect.c from git.clutter-project.org.
 * GNU Lesser General Public Licence, version 2 or (at your option) any later version.
 * Copyright (C) 2010  Intel Corporation.
 */

/* the magic gray vec3 has been taken from the NTSC conversion weights
 * as defined by:
 *
 *   "OpenGL Superbible, 4th edition"
 *   -- Richard S. Wright Jr, Benjamin Lipchak, Nicholas Haemel
 *   Addison-Wesley
 */

static const gchar* shader_source =
#ifdef COGL_HAS_GLES2
	"precision mediump float;\n"
	"varying vec2 tex_coord[1];\n"
#define TEX_COORD "tex_coord[0]"
#else
#define TEX_COORD "gl_TexCoord[0]"
#endif
	"uniform sampler2D tex;\n"
	"uniform float factor;\n"
	"\n"
	"vec3 desaturate (const vec3 color, const float desaturation)\n"
	"{\n"
	"  const vec3 gray_conv = vec3 (0.299, 0.587, 0.114);\n"
	"  vec3 gray = vec3 (dot (gray_conv, color));\n"
	"  return vec3 (mix (color.rgb, gray, desaturation));\n"
	"}\n"
	"\n"
	"void main ()\n"
	"{\n"
	"  vec4 color = gl_Color * texture2D (tex, vec2 (" TEX_COORD ".xy));\n"
	"  color.rgb = desaturate (color.rgb, factor);\n"
	"  gl_FragColor = color;\n"
	"}\n";

ClutterEffect* jammo_desaturate_effect_new(gdouble factor) {

	return CLUTTER_EFFECT(g_object_new(JAMMO_TYPE_DESATURATE_EFFECT, "factor", factor, NULL));
}

void jammo_desaturate_effect_set_factor(JammoDesaturateEffect* desaturate_effect, gdouble factor) {
	ClutterActor* actor;

	g_return_if_fail(JAMMO_IS_DESATURATE_EFFECT(desaturate_effect));
	g_return_if_fail(factor >= 0.0 && factor <= 1.0);
	
	if ((gdouble)desaturate_effect->priv->factor != factor) {
		desaturate_effect->priv->factor = (gdouble)factor;
		
		if ((actor = clutter_actor_meta_get_actor(CLUTTER_ACTOR_META(desaturate_effect)))) {
			clutter_actor_queue_redraw(actor);
		}

		g_object_notify(G_OBJECT(desaturate_effect), "factor");
	}
}
gdouble jammo_desaturate_effect_get_factor(JammoDesaturateEffect* desaturate_effect) {
	g_return_val_if_fail(JAMMO_IS_DESATURATE_EFFECT(desaturate_effect), 0.0);

	return (gdouble)desaturate_effect->priv->factor;
}

static gboolean jammo_desaturate_effect_pre_paint(ClutterEffect* effect) {
	gboolean success = FALSE;
	JammoDesaturateEffect* desaturate_effect;
	ClutterShaderEffect* shader_effect;
	
	desaturate_effect = JAMMO_DESATURATE_EFFECT(effect);
	shader_effect = CLUTTER_SHADER_EFFECT(effect);
	
	if (clutter_actor_meta_get_enabled(CLUTTER_ACTOR_META(effect))) {
		clutter_shader_effect_set_shader_source(shader_effect, shader_source);
		clutter_shader_effect_set_uniform(shader_effect, "tex", G_TYPE_INT, 1, 0);
		clutter_shader_effect_set_uniform(shader_effect, "factor", G_TYPE_FLOAT, 1, desaturate_effect->priv->factor);

		success = CLUTTER_EFFECT_CLASS(jammo_desaturate_effect_parent_class)->pre_paint(effect);
	}
	
	return success;
}

static void jammo_desaturate_effect_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoDesaturateEffect* desaturate_effect;
	
	desaturate_effect = JAMMO_DESATURATE_EFFECT(object);

	switch (prop_id) {
		case PROP_FACTOR:
			jammo_desaturate_effect_set_factor(desaturate_effect, g_value_get_double(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_desaturate_effect_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoDesaturateEffect* desaturate_effect;

	desaturate_effect = JAMMO_DESATURATE_EFFECT(object);

        switch (prop_id) {
		case PROP_FACTOR:
			g_value_set_double(value, (gdouble)desaturate_effect->priv->factor);
			break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static void jammo_desaturate_effect_class_init(JammoDesaturateEffectClass* desaturate_effect_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(desaturate_effect_class);
	ClutterEffectClass* effect_class = CLUTTER_EFFECT_CLASS(desaturate_effect_class);

	gobject_class->set_property = jammo_desaturate_effect_set_property;
	gobject_class->get_property = jammo_desaturate_effect_get_property;

	effect_class->pre_paint = jammo_desaturate_effect_pre_paint;

	/**
	 * JammoDesaturateEffect:factor:
	 *
	 * The desaturation factor, between 0.0 (no desaturation) and 1.0 (full desaturation).
	 */
	g_object_class_install_property(gobject_class, PROP_FACTOR,
	                                g_param_spec_double("factor",
	                                "Factor",
	                                "The desaturation factor, between 0.0 (no desaturation) and 1.0 (full desaturation)",
	                                0.0, 1.0, 1.0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_CONSTRUCT | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	g_type_class_add_private(gobject_class, sizeof(JammoDesaturateEffectPrivate));
}

static void jammo_desaturate_effect_init(JammoDesaturateEffect* desaturate_effect) {
	desaturate_effect->priv = G_TYPE_INSTANCE_GET_PRIVATE(desaturate_effect, JAMMO_TYPE_DESATURATE_EFFECT, JammoDesaturateEffectPrivate);
}

