/*
 * jammo-game-task.c
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "jammo-game-task.h"
#include "jammo-chum.h"
#include <string.h>

static void clutter_scriptable_iface_init(ClutterScriptableIface* iface);

G_DEFINE_TYPE_WITH_CODE(JammoGameTask, jammo_game_task, TANGLE_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(CLUTTER_TYPE_SCRIPTABLE, clutter_scriptable_iface_init););

enum {
	PROP_0,
	PROP_VIEW,
	PROP_ACTS_TO_COMPLETE,
	PROP_ACTS_COMPLETED
};

enum {
	STARTED,
	COMPLETED,
	ACT_COMPLETED,
	ACT_REDONE,
	LAST_SIGNAL
};

typedef struct _Act {
	gchar* name;
	
	guint completed : 1;
} Act;

struct _JammoGameTaskPrivate {
	TangleView* view;
	GList* acts;
	guint acts_to_complete;
	
	GSList* enabled_actors;
	GSList* disabled_actors;
	
	guint completed : 1;
};

static guint signals[LAST_SIGNAL] = { 0 };
static ClutterScriptableIface* parent_scriptable_iface = NULL;

static gint act_compare_name(Act* act, const gchar* name);

static void marshal_VOID__STRING_UINT (GClosure     *closure,
                                       GValue       *return_value G_GNUC_UNUSED,
                                       guint         n_param_values,
                                       const GValue *param_values,
                                       gpointer      invocation_hint G_GNUC_UNUSED,
                                       gpointer      marshal_data);
				
JammoGameTask* jammo_game_new(TangleView* view) {

	return JAMMO_GAME_TASK(g_object_new(JAMMO_TYPE_GAME_TASK, "view", view, NULL));
}

void jammo_game_task_start(JammoGameTask* task) {
	ClutterActor* parent;
	GSList* slist_item;
	ClutterActor* actor;
	
	g_return_if_fail(JAMMO_IS_GAME_TASK(task));
	
	parent = clutter_actor_get_parent(CLUTTER_ACTOR(task->priv->view));
	g_warn_if_fail(parent != NULL);
		
	/*	//Do not hide automatically!
	if (CLUTTER_IS_CONTAINER(parent)) {
		clutter_container_foreach(CLUTTER_CONTAINER(parent), hide_actor, task->priv->view);
	}
	*/

	//clutter_container_foreach(CLUTTER_CONTAINER(task->priv->view), CLUTTER_CALLBACK(clutter_actor_hide), NULL);
	for (slist_item = task->priv->enabled_actors; slist_item; slist_item = slist_item->next) {
		actor = CLUTTER_ACTOR(slist_item->data);
		clutter_actor_show(actor);
	}
	for (slist_item = task->priv->disabled_actors; slist_item; slist_item = slist_item->next) {
		actor = CLUTTER_ACTOR(slist_item->data);
		clutter_actor_show(actor);
		jammo_chum_disable_all_actors(actor);
	}
	
	tangle_actor_show(TANGLE_ACTOR(task->priv->view));

	g_signal_emit(task, signals[STARTED], 0);
}

void jammo_game_task_stop(JammoGameTask* task) {
	GSList* slist_item;
	ClutterActor* actor;

	g_return_if_fail(JAMMO_IS_GAME_TASK(task));

	clutter_container_foreach(CLUTTER_CONTAINER(task->priv->view), CLUTTER_CALLBACK(clutter_actor_show), NULL);
	for (slist_item = task->priv->disabled_actors; slist_item; slist_item = slist_item->next) {
		actor = CLUTTER_ACTOR(slist_item->data);
		jammo_chum_enable_all_actors(actor);
	}
}

GList* jammo_game_task_get_acts(JammoGameTask* task) {
	GList* act_names = NULL;
	GList* act_in_list;
	Act* act;
	
	g_return_val_if_fail(JAMMO_IS_GAME_TASK(task), NULL);

	for (act_in_list = task->priv->acts; act_in_list; act_in_list = act_in_list->next) {
		act = (Act*)act_in_list->data;
		act_names = g_list_prepend(act_names, act->name);
	}
	
	return act_names;
}

void jammo_game_task_add_act(JammoGameTask* task, const gchar* act_name) {
	Act* act;
	
	g_return_if_fail(JAMMO_IS_GAME_TASK(task));

	if (g_list_find_custom(task->priv->acts, act_name, (GCompareFunc)act_compare_name)) {
		g_warning("An act '%s' already exists in the task.\n", act_name);
	} else {
		act = g_new0(Act, 1);
		act->name = g_strdup(act_name);
		task->priv->acts = g_list_prepend(task->priv->acts, act);
	}
}

gboolean jammo_game_task_get_act_completed(JammoGameTask* task, const gchar* act_name) {
	gboolean completed = FALSE;
	GList* act_in_list;
	Act* act;
	
	g_return_val_if_fail(JAMMO_IS_GAME_TASK(task), FALSE);

	if (!(act_in_list = g_list_find_custom(task->priv->acts, act_name, (GCompareFunc)act_compare_name))) {
		g_warning("An act '%s' does not exists in the task.\n", act_name);
	} else {
		act = (Act*)act_in_list->data;
		completed = act->completed;
	}
	
	return completed;
}

void jammo_game_task_set_act_completed(JammoGameTask* task, const gchar* act_name, gboolean is_completed) {
	GList* act_in_list;
	Act* act;
	guint acts_completed;
	
	g_return_if_fail(JAMMO_IS_GAME_TASK(task));

	if (!(act_in_list = g_list_find_custom(task->priv->acts, act_name, (GCompareFunc)act_compare_name))) {
		g_warning("An act '%s' does not exists in the task.\n", act_name);
	} else {
		act = (Act*)act_in_list->data;
		if (act->completed && is_completed &&
		    (acts_completed = jammo_game_task_get_acts_completed(task)) < task->priv->acts_to_complete) {
			g_signal_emit(task, signals[ACT_REDONE], 0, act_name, acts_completed);
		} else if (act->completed != is_completed) {
			act->completed = is_completed;

			if (act->completed && !task->priv->completed) {
				acts_completed = jammo_game_task_get_acts_completed(task);
				g_signal_emit(task, signals[ACT_COMPLETED], 0, act_name, acts_completed);

				if (acts_completed >= task->priv->acts_to_complete) {
					g_signal_emit(task, signals[COMPLETED], 0);
				}
			}
		}
	}
}

guint jammo_game_task_get_acts_to_complete(JammoGameTask* task) {
	g_return_val_if_fail(JAMMO_IS_GAME_TASK(task), 0);

	return task->priv->acts_to_complete;
}

void jammo_game_task_set_acts_to_complete(JammoGameTask* task, guint number_of_tasks) {
	g_return_if_fail(JAMMO_IS_GAME_TASK(task));

	if (task->priv->acts_to_complete != number_of_tasks) {
		task->priv->acts_to_complete = number_of_tasks;
		g_object_notify(G_OBJECT(task), "acts-to-complete");
	}
}

guint jammo_game_task_get_acts_completed(JammoGameTask* task) {
	guint number_of_completed_acts = 0;
	GList* act_in_list;
	Act* act;
	
	g_return_val_if_fail(JAMMO_IS_GAME_TASK(task), 0);

	for (act_in_list = task->priv->acts; act_in_list; act_in_list = act_in_list->next) {
		act = (Act*)act_in_list->data;
		if (act->completed) {
			number_of_completed_acts++;
		}
	}
	
	
	return number_of_completed_acts;
}

static void jammo_game_task_started(JammoGameTask* task) {
}

static void jammo_game_task_act_completed(JammoGameTask* task, const gchar* act_name, guint acts_completed) {
}

static void jammo_game_task_act_redone(JammoGameTask* task, const gchar* act_name, guint acts_completed) {
}

static void jammo_game_task_completed(JammoGameTask* task) {
}

static void jammo_game_task_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoGameTask* task;
	
	task = JAMMO_GAME_TASK(object);

	switch (prop_id) {
		case PROP_VIEW:
			g_assert(!task->priv->view);
			task->priv->view = TANGLE_VIEW(g_value_get_object(value));
			break;
		case PROP_ACTS_TO_COMPLETE:
			jammo_game_task_set_acts_to_complete(task, g_value_get_uint(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_game_task_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoGameTask* task;

	task = JAMMO_GAME_TASK(object);

        switch (prop_id) {
		case PROP_VIEW:
			g_value_set_object(value, task->priv->view);
			break;
		case PROP_ACTS_TO_COMPLETE:
			g_value_set_uint(value, task->priv->acts_to_complete);
			break;
		case PROP_ACTS_COMPLETED:
			g_value_set_uint(value, jammo_game_task_get_acts_completed(task));
			break;
	        default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static void jammo_game_task_finalize(GObject* object) {
	G_OBJECT_CLASS(jammo_game_task_parent_class)->finalize(object);
}

static void jammo_game_task_dispose(GObject* object) {
	G_OBJECT_CLASS(jammo_game_task_parent_class)->dispose(object);
}

static void jammo_game_task_class_init(JammoGameTaskClass* game_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(game_class);

	gobject_class->finalize = jammo_game_task_finalize;
	gobject_class->dispose = jammo_game_task_dispose;
	gobject_class->set_property = jammo_game_task_set_property;
	gobject_class->get_property = jammo_game_task_get_property;

	game_class->started = jammo_game_task_started;
	game_class->act_completed = jammo_game_task_act_completed;
	game_class->act_redone = jammo_game_task_act_redone;
	game_class->completed = jammo_game_task_completed;

	/**
	 * JammoGameTask:view:
	 *
	 * The TangleView that contains this task.
	 */
	g_object_class_install_property(gobject_class, PROP_VIEW,
	                                g_param_spec_object("view",
	                                "View",
	                                "The TangleView that contains this task",
	                                TANGLE_TYPE_VIEW,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoGameTask:acts-to-complete:
	 *
	 * The number of acts that has been completed for this task.
	 */
	g_object_class_install_property(gobject_class, PROP_ACTS_TO_COMPLETE,
	                                g_param_spec_uint("acts-to-complete",
	                                "Acts to complete",
	                                "The number of acts that has to be completed",
	                                0, G_MAXUINT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoGameTask:acts-completed:
	 *
	 * The number of completed acts
	 */
	g_object_class_install_property(gobject_class, PROP_ACTS_COMPLETED,
	                                g_param_spec_uint("acts-completed",
	                                "Acts completed",
	                                "The number of completed acts",
	                                0, G_MAXUINT, 0,
	                                G_PARAM_READABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	/**
	 * JammoGameTask::started:
	 * @game: the object which received the signal
	 *
	 * The ::started signal is emitted when the task has been started by calling jammo_game_task_start().
	 */
	signals[STARTED] = g_signal_new("started", G_TYPE_FROM_CLASS(gobject_class),
	                                G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(JammoGameTaskClass, started),
					NULL, NULL,
					g_cclosure_marshal_VOID__VOID,
					G_TYPE_NONE, 0);
	/**
	 * JammoGameTask::act-completed:
	 * @task: the object which received the signal
	 * @act_name: the name of the act that has been completed
	 * @acts_completed: the number of acts that has been already completed
	 *
	 * The ::act-completed signal is emitted when an act has been completed by calling jammo_game_task_set_act_completed().
	 */
	signals[ACT_COMPLETED] = g_signal_new("act-completed", G_TYPE_FROM_CLASS(gobject_class),
	                                       G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(JammoGameTaskClass, act_completed),
					       NULL, NULL,
					       marshal_VOID__STRING_UINT,
					       G_TYPE_NONE, 2,
					       G_TYPE_STRING,
					       G_TYPE_UINT);
	/**
	 * JammoGameTask::act-redone:
	 * @task: the object which received the signal
	 * @act_name: the name of the act that has been completed
	 * @acts_completed: the number of acts that has been already completed
	 *
	 * The ::act-redone signal is emitted when an act has been already completed is completed again by calling jammo_game_task_set_act_completed().
	 */
	signals[ACT_REDONE] = g_signal_new("act-redone", G_TYPE_FROM_CLASS(gobject_class),
	                                       G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(JammoGameTaskClass, act_redone),
					       NULL, NULL,
					       marshal_VOID__STRING_UINT,
					       G_TYPE_NONE, 2,
					       G_TYPE_STRING,
					       G_TYPE_UINT);
	/**
	 * JammoGameTask::completed:
	 * @task: the object which received the signal
	 *
	 * The ::completed signal is emitted when the task has been completed by calling jammo_game_task_complete().
	 */
	signals[COMPLETED] = g_signal_new("completed", G_TYPE_FROM_CLASS(gobject_class),
	                                  G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(JammoGameTaskClass, completed),
					  NULL, NULL,
				  	  g_cclosure_marshal_VOID__VOID,
				 	  G_TYPE_NONE, 0);

	g_type_class_add_private (gobject_class, sizeof (JammoGameTaskPrivate));
}

static void jammo_game_task_init(JammoGameTask* task) {
	task->priv = G_TYPE_INSTANCE_GET_PRIVATE(task, JAMMO_TYPE_GAME_TASK, JammoGameTaskPrivate);
}

static gboolean jammo_game_task_parse_custom_node(ClutterScriptable* scriptable, ClutterScript* script, GValue* value, const gchar* name, JsonNode* node) {
	gboolean retvalue = FALSE;
	//JammoGameTask* task;
	GSList* slist = NULL;
	JsonArray* array;
	gint i;
	JsonNode* n;
	GObject* object;
	
	//task = JAMMO_GAME_TASK(scriptable);

	if (!strcmp(name, "enabled-actors") || !strcmp(name, "disabled-actors")) {
		if (JSON_NODE_TYPE(node) != JSON_NODE_ARRAY) {
			g_warning("Expected JSON array for '%s', skipped.", name);
		} else {
			array = json_node_get_array(node);
			for (i = json_array_get_length(array) - 1; i >= 0; i--) {
				n = json_array_get_element(array, i);
				if (JSON_NODE_TYPE(n) != JSON_NODE_VALUE || json_node_get_value_type(n) != G_TYPE_STRING) {
					g_warning("Expected a string value in '%s' array, skipped.", name);
				} else if (!(object = clutter_script_get_object(script, json_node_get_string(n)))) {
					g_warning("A reference to non-existing object '%s' in '%s' array, skipped.", json_node_get_string(n), name);
				} else {
					slist = g_slist_prepend(slist, object);
				}
			}
					
			g_value_init(value, G_TYPE_POINTER);
			g_value_set_pointer(value, slist);

			retvalue = TRUE;
		}
	} else if (!strcmp(name, "acts")) {
		if (JSON_NODE_TYPE(node) != JSON_NODE_ARRAY) {
			g_warning("Expected JSON array for '%s', skipped.", name);
		} else {
			array = json_node_get_array(node);
			for (i = json_array_get_length(array) - 1; i >= 0; i--) {
				n = json_array_get_element(array, i);
				if (JSON_NODE_TYPE(n) != JSON_NODE_VALUE || json_node_get_value_type(n) != G_TYPE_STRING) {
					g_warning("Expected a string value in '%s' array, skipped.", name);
				} else {
					slist = g_slist_prepend(slist,json_node_dup_string(n));
				}
			}
					
			g_value_init(value, G_TYPE_POINTER);
			g_value_set_pointer(value, slist);
			
			retvalue = TRUE;
		}	
	} else if (parent_scriptable_iface->parse_custom_node) {
		retvalue = parent_scriptable_iface->parse_custom_node(scriptable, script, value, name, node);
	}
	
	return retvalue;
}

static void jammo_game_task_set_custom_property(ClutterScriptable* scriptable, ClutterScript* script, const gchar* name, const GValue* value) {
	JammoGameTask* task;
	GSList* slist;
	GSList* slist_item;
	
	task = JAMMO_GAME_TASK(scriptable);
	
	if (!strcmp(name, "enabled-actors")) {
		g_return_if_fail(!task->priv->enabled_actors);
		g_return_if_fail(G_VALUE_HOLDS(value, G_TYPE_POINTER));

		task->priv->enabled_actors = (GSList*)g_value_get_pointer(value);
	} else if (!strcmp(name, "disabled-actors")) {
		g_return_if_fail(!task->priv->disabled_actors);
		g_return_if_fail(G_VALUE_HOLDS(value, G_TYPE_POINTER));

		task->priv->disabled_actors = (GSList*)g_value_get_pointer(value);
	} else if (!strcmp(name, "acts")) {
		g_return_if_fail(G_VALUE_HOLDS(value, G_TYPE_POINTER));

		slist = (GSList*)g_value_get_pointer(value);
		for (slist_item = slist; slist_item; slist_item = slist_item->next) {
			jammo_game_task_add_act(task, (const gchar*)slist_item->data);
			g_free(slist_item->data);
		}
		g_slist_free(slist);	
	} else if (parent_scriptable_iface->set_custom_property) {
		parent_scriptable_iface->set_custom_property(scriptable, script, name, value);
	} else {
		g_object_set_property(G_OBJECT(scriptable), name, value);
	}
}

static void clutter_scriptable_iface_init(ClutterScriptableIface* iface) {
	if (!(parent_scriptable_iface = g_type_interface_peek_parent (iface))) {
		parent_scriptable_iface = g_type_default_interface_peek(CLUTTER_TYPE_SCRIPTABLE);
	}
	
	iface->parse_custom_node = jammo_game_task_parse_custom_node;
	iface->set_custom_property = jammo_game_task_set_custom_property;
}


static gint act_compare_name(Act* act, const gchar* name) {

	return strcmp(act->name, name);
}

/* Generated marshaler */

#ifdef G_ENABLE_DEBUG
#define g_marshal_value_peek_uint(v)     g_value_get_uint (v)
#define g_marshal_value_peek_string(v)   (char*) g_value_get_string (v)
#else /* !G_ENABLE_DEBUG */
/* WARNING: This code accesses GValues directly, which is UNSUPPORTED API.
 *          Do not access GValues directly in your code. Instead, use the
 *          g_value_get_*() functions
 */
#define g_marshal_value_peek_uint(v)     (v)->data[0].v_uint
#define g_marshal_value_peek_string(v)   (v)->data[0].v_pointer
#endif /* !G_ENABLE_DEBUG */

static void marshal_VOID__STRING_UINT (GClosure     *closure,
                                       GValue       *return_value G_GNUC_UNUSED,
                                       guint         n_param_values,
                                       const GValue *param_values,
                                       gpointer      invocation_hint G_GNUC_UNUSED,
                                       gpointer      marshal_data)
{
  typedef void (*GMarshalFunc_VOID__STRING_UINT) (gpointer     data1,
                                                  gpointer     arg_1,
                                                  guint        arg_2,
                                                  gpointer     data2);
  register GMarshalFunc_VOID__STRING_UINT callback;
  register GCClosure *cc = (GCClosure*) closure;
  register gpointer data1, data2;

  g_return_if_fail (n_param_values == 3);

  if (G_CCLOSURE_SWAP_DATA (closure))
    {
      data1 = closure->data;
      data2 = g_value_peek_pointer (param_values + 0);
    }
  else
    {
      data1 = g_value_peek_pointer (param_values + 0);
      data2 = closure->data;
    }
  callback = (GMarshalFunc_VOID__STRING_UINT) (marshal_data ? marshal_data : cc->callback);

  callback (data1,
            g_marshal_value_peek_string (param_values + 1),
            g_marshal_value_peek_uint (param_values + 2),
            data2);
}

