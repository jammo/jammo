/*
 * jammo-sample-button.c
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#include "jammo-sample-button.h"
#include "jammo-editing-track-view.h"
#include "../meam/jammo-sequencer.h"
#include <string.h>

G_DEFINE_TYPE(JammoSampleButton, jammo_sample_button, TANGLE_TYPE_BUTTON);

enum {
	PROP_0,
	PROP_IMAGE_FILENAME,
	PROP_SAMPLE_FILENAME,
	PROP_SAMPLE,
	PROP_SAMPLE_TYPE,
	PROP_LOOP_ID,
	PROP_TEXT,
	PROP_SEQUENCER,
	PROP_DRAG_THRESHOLD_X,
	PROP_DRAG_THRESHOLD_Y
};

enum {
	LISTENED,
	DRAG_BEGIN,
	DRAG_END,
	DROPPED,
	LAST_SIGNAL
};

struct _JammoSampleButtonPrivate {
	gchar* image_filename;
	JammoSample* sample;
	JammoSampleType sample_type;
	gint loop_id;
	JammoSequencer* sequencer;
	ClutterAction* drag_action;
	
	guint dragging : 1;
	gboolean waltz;
};

static const GEnumValue sample_type_values[] = {
	{ JAMMO_SAMPLE_UNTYPED, "JAMMO_SAMPLE_UNTYPED", "untyped" },
	{ JAMMO_SAMPLE_RHYTMICAL, "JAMMO_SAMPLE_RHYTHMICAL", "rhytmical" },
	{ JAMMO_SAMPLE_MELODICAL, "JAMMO_SAMPLE_MELODICAL", "melodical" },
	{ JAMMO_SAMPLE_HARMONICAL, "JAMMO_SAMPLE_HARMONICAL", "harmonical" },
	{ JAMMO_SAMPLE_EFFECT, "JAMMO_SAMPLE_EFFECT", "effect" },
	{ 0, NULL, NULL }
};

GType jammo_sample_type_get_type(void) {
	static GType type = 0;
	
	if (!type) {
		type = g_enum_register_static("JammoSampleType", sample_type_values);
	}
	
	return type;
}


static guint signals[LAST_SIGNAL] = { 0 };

static void on_drag_begin(ClutterDragAction* action, ClutterActor* actor, gfloat event_x, gfloat event_y, ClutterModifierType modifiers, gpointer user_data);
static void on_drag_end(ClutterDragAction* action, ClutterActor* actor, gfloat event_x, gfloat event_y, ClutterModifierType modifiers, gpointer user_data);
static gboolean on_clicked(TangleButton* button, gpointer user_data);
static void on_size_changed(TangleTexture* texture, gint width, gint height, gpointer user_data);
static void set_sequencer(JammoSampleButton* sample_button, JammoSequencer* sequencer);

ClutterActor* jammo_sample_button_new() {

	return CLUTTER_ACTOR(g_object_new(JAMMO_TYPE_SAMPLE_BUTTON, NULL));
}

ClutterActor* jammo_sample_button_new_from_files(const gchar* image_filename, const gchar* sample_filename) {

	return CLUTTER_ACTOR(g_object_new(JAMMO_TYPE_SAMPLE_BUTTON, "image-filename", image_filename, "sample-filename", sample_filename, NULL));
}

ClutterActor* jammo_sample_button_new_with_type(JammoSampleType sample_type) {

	return CLUTTER_ACTOR(g_object_new(JAMMO_TYPE_SAMPLE_BUTTON, "sample-type", sample_type, NULL));
}

ClutterActor* jammo_sample_button_new_with_type_from_files(JammoSampleType sample_type, const gchar* image_filename, const gchar* sample_filename) {

	return CLUTTER_ACTOR(g_object_new(JAMMO_TYPE_SAMPLE_BUTTON, "sample-type", sample_type, "image-filename", image_filename, "sample-filename", sample_filename, NULL));
}

ClutterActor* jammo_sample_button_new_from_existing(JammoSampleButton* sample_button) {

	ClutterActor* new_sample_button = CLUTTER_ACTOR(g_object_new(JAMMO_TYPE_SAMPLE_BUTTON, "image-filename", sample_button->priv->image_filename, "sample", sample_button->priv->sample, "sample-type", sample_button->priv->sample_type, "loop-id", sample_button->priv->loop_id, NULL));

	JAMMO_SAMPLE_BUTTON(new_sample_button)->priv->waltz = sample_button->priv->waltz;
	jammo_sample_button_set_text(JAMMO_SAMPLE_BUTTON(new_sample_button),jammo_sample_button_get_text(sample_button));

	return new_sample_button;
}

const gchar* jammo_sample_button_get_image_filename(JammoSampleButton* sample_button) {
	g_return_val_if_fail(JAMMO_IS_SAMPLE_BUTTON(sample_button), NULL);

	return sample_button->priv->image_filename;
}

const gchar* jammo_sample_button_get_sample_filename(JammoSampleButton* sample_button) {
	g_return_val_if_fail(JAMMO_IS_SAMPLE_BUTTON(sample_button), NULL);

	return jammo_sample_get_filename(sample_button->priv->sample);
}

JammoSample* jammo_sample_button_get_sample(JammoSampleButton* sample_button) {
	g_return_val_if_fail(JAMMO_IS_SAMPLE_BUTTON(sample_button), NULL);

	return sample_button->priv->sample;
}

gint jammo_sample_button_get_loop_id(JammoSampleButton* sample_button) {
	g_return_val_if_fail(JAMMO_IS_SAMPLE_BUTTON(sample_button), 0);

	return sample_button->priv->loop_id;
}

void jammo_sample_button_set_loop_id(JammoSampleButton* sample_button, gint loop_id) {
	g_return_if_fail(JAMMO_IS_SAMPLE_BUTTON(sample_button));

	sample_button->priv->loop_id = loop_id;
	g_object_notify(G_OBJECT(sample_button), "loop-id");
}

void jammo_sample_button_set_image_filename(JammoSampleButton* sample_button, const gchar* image_filename) {
	ClutterActor* texture;
	gchar* filename;
	
	g_return_if_fail(JAMMO_IS_SAMPLE_BUTTON(sample_button));

	if (!sample_button->priv->image_filename ||
	    strcmp(sample_button->priv->image_filename, image_filename)) {
		g_free(sample_button->priv->image_filename);
		sample_button->priv->image_filename = g_strdup(image_filename);

		filename = tangle_lookup_filename(image_filename);
		texture = tangle_texture_new(filename);
		g_free(filename);
		if (!texture) {
			g_warning("Could not create a texture from a file '%s', ignored.", image_filename);
			
			g_free(sample_button->priv->image_filename);
			sample_button->priv->image_filename = NULL;
		} else {
			tangle_button_set_normal_background_actor(TANGLE_BUTTON(sample_button), texture);
			g_signal_connect(texture, "size-changed", G_CALLBACK(on_size_changed), sample_button);
		}

		g_object_notify(G_OBJECT(sample_button), "image-filename");
	}
}

void jammo_sample_button_set_sample_filename(JammoSampleButton* sample_button, const gchar* sample_filename) {
	gchar* filename;
	
	g_return_if_fail(JAMMO_IS_SAMPLE_BUTTON(sample_button));

	if (!sample_button->priv->sample ||
	    !jammo_sample_get_filename(sample_button->priv->sample) ||
	    strcmp(jammo_sample_get_filename(sample_button->priv->sample), sample_filename)) {
		if (sample_button->priv->sample) {
			g_object_unref(sample_button->priv->sample);
		}
		filename = g_strdup(sample_filename);
		sample_button->priv->sample = jammo_sample_new_from_file_with_sequencer(filename, sample_button->priv->sequencer);
		g_object_ref(sample_button->priv->sample);
		g_free(filename);

		g_object_notify(G_OBJECT(sample_button), "sample-filename");
		g_object_notify(G_OBJECT(sample_button), "sample");
	}
}

void jammo_sample_button_set_sample(JammoSampleButton* sample_button, JammoSample* sample) {
	g_return_if_fail(JAMMO_IS_SAMPLE_BUTTON(sample_button));

	if (sample_button->priv->sample != sample) {
		if (sample_button->priv->sample) {
			g_object_unref(sample_button->priv->sample);
		}
		sample_button->priv->sample = jammo_sample_new_from_existing(sample);
		g_object_ref(sample_button->priv->sample);
		
		g_object_notify(G_OBJECT(sample_button), "sample-filename");
		g_object_notify(G_OBJECT(sample_button), "sample");
	
	}
}

JammoSampleType jammo_sample_button_get_sample_type(JammoSampleButton* sample_button) {
	g_return_val_if_fail(JAMMO_IS_SAMPLE_BUTTON(sample_button), JAMMO_SAMPLE_UNTYPED);

	return sample_button->priv->sample_type;
}

void jammo_sample_button_set_sample_type(JammoSampleButton* sample_button, JammoSampleType sample_type) {
	g_return_if_fail(JAMMO_IS_SAMPLE_BUTTON(sample_button));

	if (sample_button->priv->sample_type != sample_type) {
		sample_button->priv->sample_type = sample_type;
		g_object_notify(G_OBJECT(sample_button), "sample-type");
	}
}

gboolean jammo_sample_button_is_waltz(JammoSampleButton* sample_button) {
 return sample_button->priv->waltz;
}

void jammo_sample_button_set_waltz(JammoSampleButton* sample_button, gboolean state) {
 sample_button->priv->waltz=state;
}

const gchar* jammo_sample_button_get_text(JammoSampleButton* sample_button) {
	const gchar* text = NULL;
	ClutterActor* actor;
	
	g_return_val_if_fail(JAMMO_IS_SAMPLE_BUTTON(sample_button), NULL);

	if ((actor = tangle_widget_get_foreground_actor(TANGLE_WIDGET(sample_button))) &&
	    CLUTTER_IS_TEXT(actor)) {
		text = clutter_text_get_text(CLUTTER_TEXT(actor));
	}
	
	return text;
}
	
void jammo_sample_button_set_text(JammoSampleButton* sample_button, const gchar* text) {
	TangleWidget* widget;
	ClutterActor* actor;

	g_return_if_fail(JAMMO_IS_SAMPLE_BUTTON(sample_button));

	widget = TANGLE_WIDGET(sample_button);
	if ((actor = tangle_widget_get_foreground_actor(widget))) {
		g_return_if_fail(CLUTTER_IS_TEXT(actor));
		
		if (g_strcmp0(clutter_text_get_text(CLUTTER_TEXT(actor)), text)) {
			if (text) {
				clutter_text_set_text(CLUTTER_TEXT(actor), text);
				g_object_notify(G_OBJECT(sample_button), "text");
			} else {
				tangle_widget_set_foreground_actor(widget, NULL);
				g_object_notify(G_OBJECT(sample_button), "text");
			}
		}
	} else if (text) {
		tangle_widget_set_foreground_actor(widget, clutter_text_new_with_text("Sans 12", text));
		g_object_notify(G_OBJECT(sample_button), "text");
	}
}

void jammo_sample_button_on_dropped(JammoSampleButton* sample_button) {
	g_return_if_fail(JAMMO_IS_SAMPLE_BUTTON(sample_button));

	g_signal_emit(sample_button, signals[DROPPED], 0);
}

static void jammo_sample_button_constructed(GObject* object) {
	JammoSampleButton* sample_button;
	
	sample_button = JAMMO_SAMPLE_BUTTON(object);
	
	sample_button->priv->drag_action = tangle_drag_action_new();
	g_signal_connect(sample_button->priv->drag_action, "drag-end", G_CALLBACK(on_drag_end), object);
	g_signal_connect(sample_button->priv->drag_action, "drag-begin", G_CALLBACK(on_drag_begin), object);
	clutter_actor_add_action(CLUTTER_ACTOR(object), sample_button->priv->drag_action);
	
	tangle_widget_set_prefer_background_size(TANGLE_WIDGET(object), TRUE);
	g_signal_connect(object, "clicked", G_CALLBACK(on_clicked), NULL);

	sample_button->priv->waltz=FALSE;
	G_OBJECT_CLASS(jammo_sample_button_parent_class)->constructed(object);
}

static void jammo_sample_button_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec) {
	JammoSampleButton* sample_button;
	
	sample_button = JAMMO_SAMPLE_BUTTON(object);
	guint x_threshold;
	guint y_threshold;

	switch (prop_id) {
		case PROP_IMAGE_FILENAME:
			jammo_sample_button_set_image_filename(sample_button, g_value_get_string(value));
			break;
		case PROP_SAMPLE_FILENAME:
			jammo_sample_button_set_sample_filename(sample_button, g_value_get_string(value));
			break;
		case PROP_SAMPLE:
			jammo_sample_button_set_sample(sample_button, JAMMO_SAMPLE(g_value_get_object(value)));
			break;
		case PROP_SAMPLE_TYPE:
			sample_button->priv->sample_type = g_value_get_enum(value);
			break;
		case PROP_LOOP_ID:
			sample_button->priv->loop_id = g_value_get_int(value);
			break;
		case PROP_TEXT:
			jammo_sample_button_set_text(sample_button, g_value_get_string(value));
			break;
		case PROP_SEQUENCER:
			set_sequencer(sample_button, JAMMO_SEQUENCER(g_value_get_object(value)));
			break;
		case PROP_DRAG_THRESHOLD_X:
			clutter_drag_action_get_drag_threshold (CLUTTER_DRAG_ACTION(sample_button->priv->drag_action),
                                          &x_threshold,
                                          &y_threshold);

			clutter_drag_action_set_drag_threshold(CLUTTER_DRAG_ACTION(sample_button->priv->drag_action), g_value_get_uint(value),y_threshold);
			break;
		case PROP_DRAG_THRESHOLD_Y:
			clutter_drag_action_get_drag_threshold (CLUTTER_DRAG_ACTION(sample_button->priv->drag_action),
                                          &x_threshold,
                                          &y_threshold);
			clutter_drag_action_set_drag_threshold(CLUTTER_DRAG_ACTION(sample_button->priv->drag_action), x_threshold, g_value_get_uint(value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void jammo_sample_button_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec) {
        JammoSampleButton* sample_button;

	sample_button = JAMMO_SAMPLE_BUTTON(object);
	guint x_threshold;
	guint y_threshold;

        switch (prop_id) {
		case PROP_IMAGE_FILENAME:
			g_value_set_string(value, jammo_sample_button_get_image_filename(sample_button));
			break;
		case PROP_SAMPLE_FILENAME:
			g_value_set_string(value, jammo_sample_button_get_sample_filename(sample_button));
			break;
		case PROP_SAMPLE:
			g_value_set_object(value, jammo_sample_button_get_sample(sample_button));
			break;
		case PROP_SAMPLE_TYPE:
			g_value_set_enum(value, sample_button->priv->sample_type);
			break;
		case PROP_LOOP_ID:
			g_value_set_int(value, sample_button->priv->loop_id);
			break;
		case PROP_TEXT:
			g_value_set_string(value, jammo_sample_button_get_text(sample_button));
			break;
		case PROP_DRAG_THRESHOLD_X:
			clutter_drag_action_get_drag_threshold (CLUTTER_DRAG_ACTION(sample_button->priv->drag_action),
                                          &x_threshold,
                                          &y_threshold);

			g_value_set_uint(value, x_threshold);
			break;
		case PROP_DRAG_THRESHOLD_Y:
			clutter_drag_action_get_drag_threshold (CLUTTER_DRAG_ACTION(sample_button->priv->drag_action),
                                          &x_threshold,
                                          &y_threshold);
			g_value_set_uint(value, y_threshold);
			break;
		default:
		        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		        break;
        }
}

static void jammo_sample_button_finalize(GObject* object) {
	G_OBJECT_CLASS(jammo_sample_button_parent_class)->finalize(object);
}

static void jammo_sample_button_dispose(GObject* object) {
	JammoSampleButton* sample_button;
	
	sample_button = JAMMO_SAMPLE_BUTTON(object);

	if (sample_button->priv->sequencer) {
		g_object_remove_weak_pointer(G_OBJECT(sample_button->priv->sequencer), (gpointer*)&sample_button->priv->sequencer);
		sample_button->priv->sequencer = NULL;
	}

	G_OBJECT_CLASS(jammo_sample_button_parent_class)->dispose(object);
}

static void jammo_sample_button_class_init(JammoSampleButtonClass* sample_button_class) {
	GObjectClass* gobject_class = G_OBJECT_CLASS(sample_button_class);

	gobject_class->constructed = jammo_sample_button_constructed;
	gobject_class->finalize = jammo_sample_button_finalize;
	gobject_class->dispose = jammo_sample_button_dispose;
	gobject_class->set_property = jammo_sample_button_set_property;
	gobject_class->get_property = jammo_sample_button_get_property;

	/**
	 * JammoSampleButton:image-filename:
	 */
	g_object_class_install_property(gobject_class, PROP_IMAGE_FILENAME,
	                                g_param_spec_string("image-filename",
	                                "Image filename",
	                                "The filename of the displayed image",
					NULL,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSampleButton:sample-filename:
	 */
	g_object_class_install_property(gobject_class, PROP_SAMPLE_FILENAME,
	                                g_param_spec_string("sample-filename",
	                                "Sample filename",
	                                "The filename of the played sample",
					NULL,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSampleButton:sample:
	 */
	g_object_class_install_property(gobject_class, PROP_SAMPLE,
	                                g_param_spec_object ("sample",
	                                "Sample",
	                                "The JammoSample that is played when the button is interacting",
	                                JAMMO_TYPE_SAMPLE,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSampleButton:sample-type:
	 *
	 * The type of the sample (not used internally).
	 */
	g_object_class_install_property(gobject_class, PROP_SAMPLE_TYPE,
	                                g_param_spec_enum("sample-type",
	                                "Sample type",
	                                "The type of the sample (not used internally)",
	                                JAMMO_TYPE_SAMPLE_TYPE, JAMMO_SAMPLE_UNTYPED,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	/**
	 * JammoSampleButton:loop_id:
	 *
	 * The loop_id: if two sample_buttons have same image and same audio, they have same loop_id.
	 * (sample_button in another themepack can have same loop_id)
	 */
	g_object_class_install_property(gobject_class, PROP_LOOP_ID,
	                                g_param_spec_int("loop-id",
	                                "Loop ID",
	                                "Loop ID",
	                                0, G_MAXINT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	/**
	 * JammoSampleButton:text:
	 *
	 * A text that is displayed on top of the button.
	 */
	g_object_class_install_property(gobject_class, PROP_TEXT,
	                                g_param_spec_string("text",
	                                "Text",
	                                "A text that is displayed on top of the button",
	                                NULL,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSampleButton:sequencer:
	 *
	 * A sequencer that is used to track tempo and pitch until the underlying sample is added to an editing track. See JammoSample::sequencer .
	 */
	g_object_class_install_property(gobject_class, PROP_SEQUENCER,
	                                g_param_spec_object("sequencer",
	                                "Sequencer",
	                                "A sequencer that is used to track tempo and pitch until the underlying sample is added to an editing track",
	                                JAMMO_TYPE_SEQUENCER,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSampleButton:drag-threshold-x:
	 *
	 * ClutterDragAction:drag-threshold-x.
	 */
	g_object_class_install_property(gobject_class, PROP_DRAG_THRESHOLD_X,
	                                g_param_spec_uint("drag-threshold-x",
	                                "Drag Threshold X",
	                                "ClutterDragAction:drag-threshold-x",
	                                0, G_MAXUINT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));
	/**
	 * JammoSampleButton:drag-threshold-y:
	 *
	 * ClutterDragAction:drag-threshold-y.
	 */
	g_object_class_install_property(gobject_class, PROP_DRAG_THRESHOLD_Y,
	                                g_param_spec_uint("drag-threshold-y",
	                                "Drag Threshold Y",
	                                "ClutterDragAction:drag-threshold-y",
	                                0, G_MAXUINT, 0,
	                                G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK |G_PARAM_STATIC_BLURB));

	/**
	 * JammoSampleButton::listened:
	 * @sample_button: the object which received the signal
	 *
	 * The ::listened signal is emitted when the JammoSampleButton has been clicked and the associated sample has stopped.
	 */
	signals[LISTENED] = g_signal_new("listened", G_TYPE_FROM_CLASS(gobject_class),
	                                 G_SIGNAL_RUN_LAST, 0,
					 NULL, NULL,
					 g_cclosure_marshal_VOID__VOID,
					 G_TYPE_NONE, 0);

	signals[DRAG_BEGIN] = g_signal_new("drag-begin", G_TYPE_FROM_CLASS(gobject_class),
	                                 G_SIGNAL_RUN_LAST, 0,
					 NULL, NULL,
					 g_cclosure_marshal_VOID__VOID,
					 G_TYPE_NONE, 0);

	//Drag-ends happens every time dragging is ended (e.g. sample-button is dropped to stage)
	signals[DRAG_END] = g_signal_new("drag-end", G_TYPE_FROM_CLASS(gobject_class),
	                                 G_SIGNAL_RUN_LAST, 0,
					 NULL, NULL,
					 g_cclosure_marshal_VOID__VOID,
					 G_TYPE_NONE, 0);

	//dropped happens only when jammo-sample-button is dropped on jammo-editing-track-view (also drag-ends happens then)
	signals[DROPPED] = g_signal_new("dropped", G_TYPE_FROM_CLASS(gobject_class),
	                                 G_SIGNAL_RUN_LAST, 0,
					 NULL, NULL,
					 g_cclosure_marshal_VOID__VOID,
					 G_TYPE_NONE, 0);

	g_type_class_add_private(gobject_class, sizeof(JammoSampleButtonPrivate));
}

static void jammo_sample_button_init(JammoSampleButton* sample_button) {
	sample_button->priv = G_TYPE_INSTANCE_GET_PRIVATE(sample_button, JAMMO_TYPE_SAMPLE_BUTTON, JammoSampleButtonPrivate);
}

static void on_drag_begin(ClutterDragAction* action, ClutterActor* actor, gfloat event_x, gfloat event_y, ClutterModifierType modifiers, gpointer user_data) {
	JammoSampleButton* sample_button;
	
	sample_button = JAMMO_SAMPLE_BUTTON(user_data);

	sample_button->priv->dragging = TRUE;
	tangle_button_stop_highlighting(TANGLE_BUTTON(sample_button));

	jammo_sample_stop_all();

	jammo_sample_get_duration(sample_button->priv->sample);
	g_signal_emit(sample_button, signals[DRAG_BEGIN], 0);
}

static void on_drag_end(ClutterDragAction* action, ClutterActor* actor, gfloat event_x, gfloat event_y, ClutterModifierType modifiers, gpointer user_data) {
	ClutterActor* parent;
	
	parent = clutter_actor_get_parent(actor);
	if (JAMMO_IS_EDITING_TRACK_VIEW(parent)) {
		clutter_container_remove_actor(CLUTTER_CONTAINER(parent), CLUTTER_ACTOR(actor));
	}
	g_signal_emit(JAMMO_SAMPLE_BUTTON(user_data), signals[DRAG_END], 0);
}

static void on_stopped(JammoSample* sample, gpointer user_data) {
	JammoSampleButton* sample_button;
	
	sample_button = JAMMO_SAMPLE_BUTTON(user_data);
	
	g_signal_emit(sample_button, signals[LISTENED], 0);

	g_signal_handlers_disconnect_by_func(sample, G_CALLBACK(on_stopped), user_data);
}

static gboolean on_clicked(TangleButton* button, gpointer user_data) {
	JammoSampleButton* sample_button;
	
	sample_button = JAMMO_SAMPLE_BUTTON(button);

	if (!sample_button->priv->dragging) {
		jammo_sample_stop_all();
		jammo_sample_play(sample_button->priv->sample);

		g_signal_connect(sample_button->priv->sample, "stopped", G_CALLBACK(on_stopped), sample_button);
	}
	
	sample_button->priv->dragging = FALSE;

	return FALSE;
}

static void on_size_changed(TangleTexture* texture, gint width, gint height, gpointer user_data) {
	JammoSampleButton* sample_button;
	
	sample_button = JAMMO_SAMPLE_BUTTON(user_data);
	
	tangle_actor_set_aspect_ratio(TANGLE_ACTOR(sample_button), width / height);
}

static void set_sequencer(JammoSampleButton* sample_button, JammoSequencer* sequencer) {
	if (sample_button->priv->sequencer != sequencer) {
		if (sample_button->priv->sequencer) {
			g_object_remove_weak_pointer(G_OBJECT(sample_button->priv->sequencer), (gpointer*)&sample_button->priv->sequencer);
		}
		sample_button->priv->sequencer = sequencer;
		if (sample_button->priv->sequencer) {
			g_object_add_weak_pointer(G_OBJECT(sample_button->priv->sequencer), (gpointer*)&sample_button->priv->sequencer);
		}
	}
}
