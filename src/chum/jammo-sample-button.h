/*
 * jammo-sample-button.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#ifndef __JAMMO_SAMPLE_BUTTON_H__
#define __JAMMO_SAMPLE_BUTTON_H__

#include <tangle.h>

#define JAMMO_TYPE_SAMPLE_BUTTON (jammo_sample_button_get_type())
#define JAMMO_SAMPLE_BUTTON(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), JAMMO_TYPE_SAMPLE_BUTTON, JammoSampleButton))
#define JAMMO_IS_SAMPLE_BUTTON(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAMMO_TYPE_SAMPLE_BUTTON))
#define JAMMO_SAMPLE_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), JAMMO_TYPE_SAMPLE_BUTTON, JammoSampleButtonClass))
#define JAMMO_IS_SAMPLE_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), JAMMO_TYPE_SAMPLE_BUTTON))
#define JAMMO_SAMPLE_BUTTON_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), JAMMO_TYPE_SAMPLE_BUTTON, JammoSampleButtonClass))

#define JAMMO_TYPE_SAMPLE_TYPE (jammo_sample_type_get_type())

typedef struct _JammoSampleButtonPrivate JammoSampleButtonPrivate;

typedef struct _JammoSampleButton {
	TangleButton parent_instance;
	JammoSampleButtonPrivate* priv;
} JammoSampleButton;

typedef struct _JammoSampleButtonClass {
	TangleButtonClass parent_class;
} JammoSampleButtonClass;

GType jammo_sample_button_get_type(void) G_GNUC_CONST;

typedef enum _JammoSampleType {
	JAMMO_SAMPLE_UNTYPED = 0,
	JAMMO_SAMPLE_RHYTMICAL,
	JAMMO_SAMPLE_MELODICAL,
	JAMMO_SAMPLE_HARMONICAL,
	JAMMO_SAMPLE_EFFECT
} JammoSampleType;

GType jammo_sample_type_get_type(void) G_GNUC_CONST;

#include "../meam/jammo-sample.h"

ClutterActor* jammo_sample_button_new();
ClutterActor* jammo_sample_button_new_from_files(const gchar* image_filename, const gchar* sample_filename);
ClutterActor* jammo_sample_button_new_with_type(JammoSampleType sample_type);
ClutterActor* jammo_sample_button_new_with_type_from_files(JammoSampleType sample_type, const gchar* image_filename, const gchar* sample_filename);
ClutterActor* jammo_sample_button_new_from_existing(JammoSampleButton* sample_button);
const gchar* jammo_sample_button_get_image_filename(JammoSampleButton* sample_button);
const gchar* jammo_sample_button_get_sample_filename(JammoSampleButton* sample_button);
JammoSample* jammo_sample_button_get_sample(JammoSampleButton* sample_button);
void jammo_sample_button_set_image_filename(JammoSampleButton* sample_button, const gchar* image_filename);
void jammo_sample_button_set_sample_filename(JammoSampleButton* sample_button, const gchar* sample_filename);
void jammo_sample_button_set_sample(JammoSampleButton* sample_button, JammoSample* sample);
JammoSampleType jammo_sample_button_get_sample_type(JammoSampleButton* sample_button);
void jammo_sample_button_set_sample_type(JammoSampleButton* sample_button, JammoSampleType sample_type);
gboolean jammo_sample_button_is_waltz(JammoSampleButton* sample_button);
void jammo_sample_button_set_waltz(JammoSampleButton* sample_button, gboolean state);
void jammo_sample_button_get_background_color(JammoSampleButton* sample_button, ClutterColor* background_color);
gboolean jammo_sample_button_get_background_color_set(JammoSampleButton* sample_button);
void jammo_sample_button_set_background_color(JammoSampleButton* sample_button, const ClutterColor* background_color);
const gchar* jammo_sample_button_get_text(JammoSampleButton* sample_button);
void jammo_sample_button_set_text(JammoSampleButton* sample_button, const gchar* text);
gint jammo_sample_button_get_loop_id(JammoSampleButton* sample_button);
void jammo_sample_button_set_loop_id(JammoSampleButton* sample_button, gint loop_id);
void jammo_sample_button_on_dropped(JammoSampleButton* sample_button);

#endif
