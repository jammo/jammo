/*
 * jammo-miditrack-view.h
 *
 * This file is part of JamMo.
 *
 * (c) 2010 University of Oulu
 *
 * Authors: Aapo Rantalainen
 */

#ifndef __JAMMO_MIDITRACK_VIEW_H__
#define __JAMMO_MIDITRACK_VIEW_H__

#include <tangle.h>
#include "../meam/jammo-instrument-track.h"
#include "jammo-note-button.h"

#define JAMMO_TYPE_MIDITRACK_VIEW (jammo_miditrack_view_get_type())
#define JAMMO_MIDITRACK_VIEW(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), JAMMO_TYPE_MIDITRACK_VIEW, JammoMiditrackView))
#define JAMMO_IS_MIDITRACK_VIEW(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), JAMMO_TYPE_MIDITRACK_VIEW))
#define JAMMO_MIDITRACK_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), JAMMO_TYPE_MIDITRACK_VIEW, JammoMiditrackViewClass))
#define JAMMO_IS_MIDITRACK_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), JAMMO_TYPE_MIDITRACK_VIEW))
#define JAMMO_MIDITRACK_VIEW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), JAMMO_TYPE_MIDITRACK_VIEW, JammoMiditrackViewClass))

typedef struct _JammoMiditrackViewPrivate JammoMiditrackViewPrivate;

typedef struct _JammoMiditrackView {
	TangleWidget parent_instance;
	JammoMiditrackViewPrivate* priv;
} JammoMiditrackView;

typedef struct _JammoMiditrackViewClass {
	TangleWidgetClass parent_class;
} JammoMiditrackViewClass;

GType jammo_miditrack_view_get_type(void) G_GNUC_CONST;

ClutterActor* jammo_miditrack_view_new(JammoInstrumentTrack* miditrack, guint n_slots, guint64 slot_duration, gfloat slot_width, gfloat slot_height, guint h_note, guint l_note);

void jammo_miditrack_view_add_event_list(JammoMiditrackView* miditrack_view, GList* events);
ClutterActor* jammo_miditrack_view_add_note(JammoMiditrackView* miditrack_view, JammoMidiEvent* start, JammoMidiEvent* stop);
guint jammo_miditrack_view_get_note_button_slot(JammoMiditrackView* miditrack_view, JammoNoteButton* note_button);

gboolean jammo_miditrack_view_get_pen_mode(JammoMiditrackView* miditrack_view);
void jammo_miditrack_view_set_pen_mode(JammoMiditrackView* miditrack_view, gboolean mode);

gboolean jammo_miditrack_view_get_eraser_mode(JammoMiditrackView* miditrack_view);
void jammo_miditrack_view_set_eraser_mode(JammoMiditrackView* miditrack_view, gboolean mode);

gboolean jammo_miditrack_view_get_editing_enabled(JammoMiditrackView* miditrack_view);
void jammo_miditrack_view_set_editing_enabled(JammoMiditrackView* miditrack_view, gboolean editing_enabled);

gboolean jammo_miditrack_view_zoom_to_fit(JammoMiditrackView* miditrack_view, gfloat target_x, gfloat target_y);

void jammo_miditrack_view_set_show_grid(JammoMiditrackView* miditrack_view, gboolean state);
void jammo_miditrack_view_remove_all(JammoMiditrackView* miditrack_view);
#endif
