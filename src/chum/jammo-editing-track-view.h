/*
 * jammo-track-view.h
 *
 * This file is part of JamMo.
 *
 * (c) 2009-2010 University of Oulu
 *
 * Authors: Henrik Hedberg <henrik.hedberg@oulu.fi>
 */

#ifndef __JAMMO_EDITING_TRACK_VIEW_H__
#define __JAMMO_EDITING_TRACK_VIEW_H__

#include <tangle.h>
#include "../meam/jammo-editing-track.h"
#include "jammo-sample-button.h"

#define JAMMO_TYPE_EDITING_TRACK_VIEW (jammo_editing_track_view_get_type())
#define JAMMO_EDITING_TRACK_VIEW(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), JAMMO_TYPE_EDITING_TRACK_VIEW, JammoEditingTrackView))
#define JAMMO_IS_EDITING_TRACK_VIEW(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), JAMMO_TYPE_EDITING_TRACK_VIEW))
#define JAMMO_EDITING_TRACK_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), JAMMO_TYPE_EDITING_TRACK_VIEW, JammoEditingTrackViewClass))
#define JAMMO_IS_EDITING_TRACK_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), JAMMO_TYPE_EDITING_TRACK_VIEW))
#define JAMMO_EDITING_TRACK_VIEW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), JAMMO_TYPE_EDITING_TRACK_VIEW, JammoEditingTrackViewClass))

typedef struct _JammoEditingTrackViewPrivate JammoEditingTrackViewPrivate;

typedef struct _JammoEditingTrackView {
	TangleWidget parent_instance;
	JammoEditingTrackViewPrivate* priv;
} JammoEditingTrackView;

typedef struct _JammoEditingTrackViewClass {
	TangleWidgetClass parent_class;
} JammoEditingTrackViewClass;

GType jammo_editing_track_view_get_type(void) G_GNUC_CONST;

ClutterActor* jammo_editing_track_view_new(JammoEditingTrack* track, guint n_slots, guint64 slot_duration, gfloat slot_width, gfloat slot_height);

void jammo_editing_track_view_remove_jammo_sample_button_from_slot(JammoEditingTrackView* track_view, guint asked_slot);
void jammo_editing_track_view_add_jammo_sample_button(JammoEditingTrackView* track_view, JammoSampleButton* sample_button, guint slot);
guint jammo_editing_track_view_get_sample_button_slot(JammoEditingTrackView* track_view, JammoSampleButton* sample_button);

gboolean jammo_editing_track_view_get_editing_enabled(JammoEditingTrackView* track_view);
void jammo_editing_track_view_set_editing_enabled(JammoEditingTrackView* track_view, gboolean editing_enabled);

JammoSampleType jammo_editing_track_view_get_sample_type(JammoEditingTrackView* track_view);
void jammo_editing_track_view_set_sample_type(JammoEditingTrackView* track_view, JammoSampleType sample_type);

#endif
