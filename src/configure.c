#include <stdlib.h> //getenv(home)
#include "configure.h"
#include <glib.h>
#include <stdio.h>
#include <fcntl.h> //we check some files

static gchar* JAMMO_DIRECTORY=NULL;        //This is used on both games (e.g for log-files)
static gchar* COMPOSITIONS_DIRECTORY=NULL; //3-6 composings
static gchar* SINGINGS_DIRECTORY=NULL;     //3-6 singings
static gchar* PROJECTS_DIRECTORY=NULL;     //7-12 working versions
static gchar* FINALIZED_DIRECTORY=NULL;    //7-12 audio files
static gchar* LOG_DIRECTORY=NULL;    	   //Log files
static gchar* COMMUNITY_TEMP_DIRECTORY=NULL;//Used to hold community's temporary files

static void make_directory(gchar* directory) {
	gchar* cmd;

	cmd= g_strdup_printf("mkdir -p %s",directory);
	if (system(cmd)) {
		printf("Configure: Can't call '%s' \n", cmd);
	}
	g_free(cmd);
}

/*
 * TRUE = 3-6 game
 * FALSE = 7-12 game
 */
void configure_init_directories(gboolean small_game){
	gchar* home = getenv ("HOME");

	JAMMO_DIRECTORY = g_strdup_printf("%s/%s",home,USER_DIR);

	make_directory(JAMMO_DIRECTORY);


	if (small_game) {
		COMPOSITIONS_DIRECTORY = g_strdup_printf("%s/compositions",JAMMO_DIRECTORY);
		SINGINGS_DIRECTORY = g_strdup_printf("%s/singings",JAMMO_DIRECTORY);
		LOG_DIRECTORY = g_strdup_printf("%s/logs3_6",JAMMO_DIRECTORY);

		make_directory(COMPOSITIONS_DIRECTORY);
		make_directory(SINGINGS_DIRECTORY);

		gchar* recorded_waws = g_strdup_printf("%s/wavs",SINGINGS_DIRECTORY);
		make_directory(recorded_waws);
		g_free(recorded_waws);

	}

	else	{
		PROJECTS_DIRECTORY = g_strdup_printf("%s/projects",JAMMO_DIRECTORY);
		FINALIZED_DIRECTORY = g_strdup_printf("%s/finalized",JAMMO_DIRECTORY);
		LOG_DIRECTORY = g_strdup_printf("%s/logs7_12",JAMMO_DIRECTORY);
		COMMUNITY_TEMP_DIRECTORY = g_strdup_printf("%s/community_temp",JAMMO_DIRECTORY);

		make_directory(PROJECTS_DIRECTORY);
		make_directory(FINALIZED_DIRECTORY);
		make_directory(COMMUNITY_TEMP_DIRECTORY);
	}

	make_directory(LOG_DIRECTORY);

	//Check jammo.config
	gchar* setting_file = g_strdup_printf("%s/jammo.config",configure_get_jammo_directory());
	int fd = open(setting_file,O_RDONLY);
	if (fd == -1) //Settings-file not found => maybe first run
		{
			printf("Making default settings file '%s' for JamMo (maybe first run?)\n",setting_file);
			FILE* ofp;
			ofp = fopen(setting_file, "w");
			fprintf(ofp,"{\n");
			fprintf(ofp,"\"WRITE_LOG_ALSO_STDOUT\" : false,\n");
			fprintf(ofp,"\"LOG_DEBUG\" : true,\n");
			fprintf(ofp,"\"LOG_INFO\" : true,\n");
			fprintf(ofp,"\"LOG_FATAL\" : true,\n");
			fprintf(ofp,"\"LOG_ERROR\" : true,\n");
			fprintf(ofp,"\"LOG_WARN\" : true,\n");
			fprintf(ofp,"\"LOG_USER_ACTION\" : true,\n");
			fprintf(ofp,"\"LOG_NETWORK\" : true,\n");
			fprintf(ofp,"\"LOG_NETWORK_DEBUG\" : true,\n");
			fprintf(ofp,"\"LOG_MENTOR_SPEECH\" : true,\n");
			fprintf(ofp,"\"MENTOR_LANGUAGE\" : \"system\",\n");
			fprintf(ofp,"\"START_WITH_PASSIVE_MENTOR\" : false,\n");
			fprintf(ofp,"\"START_WITH_ADVANCED\" : false,\n");
			fprintf(ofp,"\"FULLSCREEN\" : false\n");
			fprintf(ofp,"}\n");
			fclose(ofp);
		}
	g_free(setting_file);
}

void configure_release_directories(){
	if (JAMMO_DIRECTORY) g_free(JAMMO_DIRECTORY);
	if (COMPOSITIONS_DIRECTORY) g_free(COMPOSITIONS_DIRECTORY);
	if (SINGINGS_DIRECTORY) g_free(SINGINGS_DIRECTORY);
	if (PROJECTS_DIRECTORY) g_free(PROJECTS_DIRECTORY);
	if (FINALIZED_DIRECTORY) g_free(FINALIZED_DIRECTORY);
	if (LOG_DIRECTORY) g_free(LOG_DIRECTORY);
	if (COMMUNITY_TEMP_DIRECTORY) g_free(COMMUNITY_TEMP_DIRECTORY);
}



const gchar* configure_get_jammo_directory(){
	return JAMMO_DIRECTORY;
}

const gchar* configure_get_compositions_directory(){
	return COMPOSITIONS_DIRECTORY;
}

const gchar* configure_get_singings_directory(){
	return SINGINGS_DIRECTORY;
}

const gchar* configure_get_projects_directory(){
	return PROJECTS_DIRECTORY;
}

const gchar* configure_get_finalized_directory(){
	return FINALIZED_DIRECTORY;
}
const gchar* configure_get_log_directory(){
	return LOG_DIRECTORY;
}
const gchar* configure_get_temp_directory(){
	return COMMUNITY_TEMP_DIRECTORY;
}
