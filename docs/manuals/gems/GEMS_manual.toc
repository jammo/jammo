\select@language {english}
\contentsline {chapter}{\numberline {1}PeerHood}{7}
\contentsline {section}{\numberline {1.1}What is PeerHood}{7}
\contentsline {section}{\numberline {1.2}PeerHood components}{7}
\contentsline {subsection}{\numberline {1.2.1}Main parts}{7}
\contentsline {subsection}{\numberline {1.2.2}Plugins}{8}
\contentsline {subsection}{\numberline {1.2.3}Listeners}{8}
\contentsline {section}{\numberline {1.3}How PeerHood is utilized in JamMo}{8}
\contentsline {section}{\numberline {1.4}Getting PeerHood}{8}
\contentsline {subsection}{\numberline {1.4.1}Downloading}{8}
\contentsline {subsection}{\numberline {1.4.2}Pre-requirements}{9}
\contentsline {subsection}{\numberline {1.4.3}Compiling and installing}{9}
\contentsline {subsection}{\numberline {1.4.4}Uninstalling}{10}
\contentsline {subsection}{\numberline {1.4.5}Configuring, starting and restarting PeerHood}{10}
\contentsline {subsection}{\numberline {1.4.6}Compiling applications using PeerHood}{12}
\contentsline {chapter}{\numberline {2}GEMS instructions }{13}
\contentsline {section}{\numberline {2.1}Service creation}{13}
\contentsline {subsection}{\numberline {2.1.1}Adding new service}{13}
\contentsline {subsection}{\numberline {2.1.2}Searching for devices with service enabled and sending data to that service}{14}
\contentsline {subsection}{\numberline {2.1.3}Processing incoming data}{17}
\contentsline {subsection}{\numberline {2.1.4}Error Messages}{19}
\contentsline {subsection}{\numberline {2.1.5}Additional notes}{19}
\contentsline {chapter}{\numberline {3}GEMS High level API}{20}
\contentsline {section}{\numberline {3.1}Connections API}{20}
\contentsline {subsection}{\numberline {3.1.1}Used structures}{20}
\contentsline {subsection}{\numberline {3.1.2}API functions}{20}
\contentsline {paragraph}{gems\_list\_jammo\_connections()}{20}
\contentsline {paragraph}{gems\_connection{*} gems\_communication\_get\_connection(GList{*} list, guint checksum)}{20}
\contentsline {paragraph}{gems\_connection{*} gems\_communication\_get\_connection\_with\_userid(GList{*} list, guint32 userid)}{21}
\contentsline {paragraph}{gems\_peer\_profile{*} gems\_get\_profile(gems\_connection{*} element)}{21}
\contentsline {paragraph}{void gems\_connection\_add\_16(gems\_connection{*} element, guint16 value)}{21}
\contentsline {paragraph}{void gems\_connection\_add\_32(gems\_connection{*} element, guint32 value)}{21}
\contentsline {paragraph}{void gems\_connection\_add\_64(gems\_connection{*} element, guint64 value)}{21}
\contentsline {paragraph}{void gems\_connection\_add\_float(gems\_connection{*} element, gfloat value)}{21}
\contentsline {paragraph}{void gems\_connection\_add\_char(gems\_connection{*} element, gchar{*} value, guint length, gboolean pad)}{21}
\contentsline {paragraph}{void gems\_connection\_add\_padding(gems\_connection{*} element)}{21}
\contentsline {paragraph}{void gems\_connection\_add\_data(gems\_connection{*} element, gchar{*} data, guint length)}{22}
\contentsline {paragraph}{guint16 gems\_connection\_get\_16(gems\_connection{*} element, guint from)}{22}
\contentsline {paragraph}{guint16 gems\_connection\_get\_32(gems\_connection{*} element, guint from)}{22}
\contentsline {paragraph}{guint64 gems\_connection\_get\_64(gems\_connection{*} element, guint from)}{22}
\contentsline {paragraph}{gfloat gems\_connection\_get\_float(gems\_connection{*} element, guint from)}{22}
\contentsline {paragraph}{gchar{*} gems\_connection\_get\_char(gems\_connection{*} element, guint from)}{22}
\contentsline {paragraph}{gchar{*} gems\_connection\_get\_data(gems\_connection{*} element, guint from, guint length)}{22}
\contentsline {paragraph}{guint gems\_connection\_partial\_data\_amount(gems\_connection{*} element)}{22}
\contentsline {section}{\numberline {3.2}GEMS Utils API}{23}
\contentsline {subsection}{\numberline {3.2.1}Used structures}{23}
\contentsline {subsection}{\numberline {3.2.2}API functions}{23}
\contentsline {paragraph}{gchar{*}{*} gems\_list\_saved\_profiles(guint{*} count)}{23}
\contentsline {paragraph}{void gems\_free\_list\_saved\_profiles()}{23}
\contentsline {paragraph}{gboolean gems\_check\_profile\_version(gchar{*} filename)}{23}
\contentsline {section}{\numberline {3.3}Group Manager API}{23}
\contentsline {subsection}{\numberline {3.3.1}Used structures}{23}
\contentsline {subsection}{\numberline {3.3.2}API functions}{24}
\contentsline {paragraph}{gboolean gems\_group\_init\_group(gint16 type, gint16 size, gint16 theme)}{24}
\contentsline {paragraph}{void gems\_reset\_group\_info()}{24}
\contentsline {paragraph}{void gems\_group\_lock\_group(gboolean locked)}{24}
\contentsline {paragraph}{gint gems\_group\_send\_to\_group(gems\_message{*} msg)}{24}
\contentsline {paragraph}{gint gems\_group\_join\_to\_group(guint32 gid)}{24}
\contentsline {paragraph}{gint gems\_group\_leave\_from\_group()}{24}
\contentsline {paragraph}{gboolean gems\_group\_is\_locked()}{24}
\contentsline {paragraph}{gboolean gems\_group\_active()}{25}
\contentsline {paragraph}{GList{*} gems\_group\_list\_other\_groups()}{25}
\contentsline {paragraph}{guint32 gems\_group\_get\_gid(gems\_group\_info{*} group)}{25}
\contentsline {paragraph}{guint32 gems\_group\_get\_owner(gems\_group\_info{*} group)}{25}
\contentsline {paragraph}{gint16 gems\_group\_get\_type(gems\_group\_info{*} group)}{25}
\contentsline {paragraph}{gint16 gems\_group\_get\_theme(gems\_group\_info{*} group)}{25}
\contentsline {paragraph}{gint16 gems\_group\_get\_size(gems\_group\_info{*} group)}{25}
\contentsline {paragraph}{gint16 gems\_group\_get\_spaces\_left(gems\_group\_info{*} group)}{25}
\contentsline {paragraph}{guint32{*} gems\_group\_get\_group\_members()}{25}
\contentsline {section}{\numberline {3.4}Profile Manager API}{25}
\contentsline {subsection}{\numberline {3.4.1}Used structures}{26}
\contentsline {subsection}{\numberline {3.4.2}API functions}{26}
\contentsline {paragraph}{gint gems\_profile\_manager\_authenticate\_default\_user(const gchar{*} passworddata)}{26}
\contentsline {paragraph}{gint gems\_profile\_manager\_authenticate\_user(const gchar{*} username, const gchar{*} passworddata)}{26}
\contentsline {paragraph}{gboolean gems\_profile\_manager\_logout()}{26}
\contentsline {paragraph}{gboolean gems\_profile\_manager\_change\_password(const gchar{*} passworddata)}{26}
\contentsline {paragraph}{gboolean gems\_profile\_manager\_create\_default\_user\_profile(const gchar{*} passworddata, guint16 age)}{27}
\contentsline {paragraph}{guint32 gems\_profile\_manager\_get\_userid(gems\_peer\_profile{*} profile)}{27}
\contentsline {paragraph}{const gchar{*} gems\_profile\_manager\_get\_username(gems\_peer\_profile{*} profile)}{27}
\contentsline {paragraph}{guint16 gems\_profile\_manager\_get\_age(gems\_peer\_profile{*} profile)}{27}
\contentsline {paragraph}{guint32 gems\_profile\_manager\_get\_avatar\_id(gems\_peer\_profile{*} profile)}{27}
\contentsline {paragraph}{gboolean gems\_profile\_manager\_set\_avatar\_id(guint32 avatarid)}{27}
\contentsline {paragraph}{gems\_peer\_profile{*} gems\_profile\_manager\_get\_profile\_of\_user(guint32 id)}{27}
\contentsline {paragraph}{const gchar{*} gems\_profile\_manager\_get\_firstname()}{27}
\contentsline {paragraph}{const gchar{*} gems\_profile\_manager\_get\_lastname()}{27}
\contentsline {paragraph}{guint32 gems\_profile\_manager\_get\_points()}{27}
\contentsline {paragraph}{gboolean gems\_profile\_manager\_add\_points(guint32 \_value)}{28}
\contentsline {paragraph}{gboolean gems\_profile\_manager\_remove\_points(guint32 \_value)}{28}
\contentsline {paragraph}{void gems\_profile\_manager\_reset\_points()}{28}
\contentsline {paragraph}{gboolean gems\_profile\_manager\_is\_authenticated()}{28}
\contentsline {paragraph}{gchar{*} gems\_profile\_manager\_get\_loaded\_username()}{28}
\contentsline {paragraph}{guint8 gems\_profile\_manager\_get\_profile\_type()}{28}
\contentsline {paragraph}{guint8 gems\_profile\_manager\_get\_profile\_version()}{28}
\contentsline {section}{\numberline {3.5}Teacher Utils API}{28}
\contentsline {subsection}{\numberline {3.5.1}Used structures}{29}
\contentsline {subsection}{\numberline {3.5.2}API functions}{29}
\contentsline {paragraph}{jammo\_profile\_container{*} gems\_teacher\_server\_new\_profile\_container()}{29}
\contentsline {paragraph}{teacher\_server\_profile\_data{*} gems\_teacher\_server\_new\_profile\_data\_container()}{29}
\contentsline {paragraph}{void gems\_teacher\_server\_clear\_profile\_container(jammo\_profile\_container{*} container)}{29}
\contentsline {paragraph}{void gems\_teacher\_server\_clear\_profile\_data\_container(teacher\_server\_profile\_data{*} data)}{29}
\contentsline {paragraph}{jammo\_profile\_container{*} gems\_teacher\_server\_create\_profile(guint8 \_type, guint32 \_userid, const gchar{*} \_username, const gchar{*} \_password, const gchar{*} \_authlevel, const gchar{*} \_firstname, const gchar{*} \_lastname, guint16 \_age, guint32 \_avatarid)}{30}
\contentsline {paragraph}{jammo\_profile\_container{*} gems\_teacher\_server\_setup\_profile(guint32 \_userid, const gchar{*} \_username, const gchar{*} \_password, const guchar{*} \_salt, const gchar{*} \_authlevel, const gchar{*} \_firstname, const gchar{*} \_lastname, guint16 \_age, guint32 \_points, guint32 \_avatarid)}{30}
\contentsline {paragraph}{jammo\_profile\_container{*} gems\_teacher\_server\_get\_profile\_for\_user(teacher\_server\_profile\_data{*} data, gchar{*} username)}{30}
\contentsline {paragraph}{jammo\_profile\_container{*} gems\_teacher\_server\_get\_profile\_for\_user\_id(teacher\_server\_profile\_data{*} data, guint32 userid)}{30}
\contentsline {paragraph}{gboolean gems\_teacher\_server\_encrypt\_profile(jammo\_profile\_container{*} container)}{31}
\contentsline {paragraph}{gboolean gems\_teacher\_server\_change\_password(jammo\_profile\_container{*} container, gchar{*} password)}{31}
\contentsline {paragraph}{gint gems\_teacher\_server\_write\_profile(jammo\_profile\_container{*} container)}{31}
\contentsline {paragraph}{gboolean gems\_teacher\_server\_save\_encrypted\_profile(gchar{*} username, gchar{*} profiledata, guint datalength)}{31}
\contentsline {paragraph}{gchar{*} gems\_teacher\_server\_serialize\_all\_profiles(GList{*} list, guint{*} length)}{31}
\contentsline {paragraph}{GList{*} gems\_teacher\_server\_deserialize\_profiles(GList{*} list, gchar{*} serialized\_data, guint length)}{31}
\contentsline {paragraph}{void gems\_teacher\_server\_clear\_profile\_list(GList{*} list)}{32}
\contentsline {paragraph}{gboolean gems\_teacher\_server\_encrypt\_profiles(teacher\_server\_profile\_data{*} data, const gchar{*} passwd)}{32}
\contentsline {paragraph}{gboolean gems\_teacher\_server\_decrypt\_profiles(teacher\_server\_profile\_data{*} data, const gchar{*} passwd)}{32}
\contentsline {paragraph}{gint gems\_teacher\_server\_write\_profile\_database(teacher\_server\_profile\_data{*} data)}{32}
\contentsline {paragraph}{gint gems\_teacher\_server\_read\_profile\_database(teacher\_server\_profile\_data{*}{*} data)}{32}
\contentsline {chapter}{\numberline {4}GEMS Low level API}{33}
\contentsline {section}{\numberline {4.1}Files and content}{33}
\contentsline {subsection}{\numberline {4.1.1}Files containing core functionality:}{33}
\contentsline {paragraph}{collaboration.h collaboration.c}{33}
\contentsline {paragraph}{communication.h communication.c}{33}
\contentsline {paragraph}{community\_server\_functions.h community\_server\_functions.c}{33}
\contentsline {paragraph}{gems\_definitions.h}{33}
\contentsline {paragraph}{gems\_message\_functions.h gems\_message\_functions.c}{33}
\contentsline {paragraph}{gems\_profile\_manager.h gems\_profile\_manager.c}{33}
\contentsline {paragraph}{gems\_security.h gems\_security.c}{33}
\contentsline {paragraph}{gems\_structures.h gems\_structures.c}{33}
\contentsline {paragraph}{gems\_teacher\_server\_utils.h gems\_teacher\_server\_utils.c}{33}
\contentsline {paragraph}{gems\_utils.h gems\_utils.c}{33}
\contentsline {paragraph}{gems.h gems.c}{33}
\contentsline {paragraph}{groupmanager.h groupmanager.c}{33}
\contentsline {paragraph}{mentor.h mentor.c}{34}
\contentsline {paragraph}{ProfileManager.h ProfileManager.cc}{34}
\contentsline {paragraph}{songbank\_comm.h songbank\_comm.c}{34}
\contentsline {paragraph}{StorageAgent.h StorageAgent.cc}{34}
\contentsline {subsection}{\numberline {4.1.2}Files of different services:}{34}
\contentsline {paragraph}{gems\_service\_collaboration.h gems\_service\_collaboration.c}{34}
\contentsline {paragraph}{gems\_service\_control.h gems\_service\_control.c}{34}
\contentsline {paragraph}{gems\_service\_group\_management.h gems\_service\_group\_management.c}{34}
\contentsline {paragraph}{gems\_service\_jammo.h gems\_service\_jammo.c}{34}
\contentsline {paragraph}{gems\_service\_mentor.h gems\_service\_mentor.c}{34}
\contentsline {paragraph}{gems\_service\_profile.h gems\_service\_profile.c}{34}
\contentsline {paragraph}{gems\_service\_songbank.h gems\_service\_songbank.c}{34}
\contentsline {section}{\numberline {4.2}Constants, message types and error messages}{34}
\contentsline {section}{\numberline {4.3}Structures}{34}
\contentsline {section}{\numberline {4.4}Functions}{34}
