#! /bin/sh

# show-git-statistics.sh
#
# (c) 2010 Henrik Hedberg <henrik.hedberg@oulu.fi_
#
# Shows the amount of lines in a directory for each author.

for d in $(find -type d | grep -v .git |grep -v .dep | grep -v .jammo-profiles); do
	echo $d
	(for f in $d/*; do git annotate $f; done) | cut -f 2 |cut -c 2- | sort | uniq -c | sed 's/^[ \t]*//' | grep -v "Not Committed Yet"
done 2> /dev/null
